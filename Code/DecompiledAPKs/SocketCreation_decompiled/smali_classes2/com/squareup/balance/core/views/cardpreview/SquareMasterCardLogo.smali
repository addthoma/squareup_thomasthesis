.class public final Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;
.super Landroid/view/View;
.source "SquareMasterCardLogo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareMasterCardLogo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareMasterCardLogo.kt\ncom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo\n+ 2 Paints.kt\ncom/squareup/util/Paints\n*L\n1#1,116:1\n7#2,4:117\n7#2,4:121\n7#2,4:125\n*E\n*S KotlinDebug\n*F\n+ 1 SquareMasterCardLogo.kt\ncom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo\n*L\n77#1,4:117\n91#1,4:121\n105#1,4:125\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0007\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u0000 ,2\u00020\u0001:\u0001,B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0019\u001a\u00020\nH\u0002J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001f\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0012\u0010 \u001a\u00020\u00072\u0008\u0008\u0001\u0010!\u001a\u00020\u0007H\u0003J\u0008\u0010\"\u001a\u00020\nH\u0002J\u0010\u0010#\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0014J\u0018\u0010$\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u0007H\u0014J\u0008\u0010\'\u001a\u00020\nH\u0002J\u0010\u0010(\u001a\u00020\u001b2\u0006\u0010)\u001a\u00020*H\u0016J\u0016\u0010(\u001a\u00020\u001b2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020*R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u001b\u0010\u000f\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\u000e\u001a\u0004\u0008\u0010\u0010\u000cR\u0014\u0010\u0012\u001a\u00020\u00138BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0018\u0010\u000e\u001a\u0004\u0008\u0017\u0010\u000c\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "centerIntersectPaint",
        "Landroid/graphics/Paint;",
        "getCenterIntersectPaint",
        "()Landroid/graphics/Paint;",
        "centerIntersectPaint$delegate",
        "Lkotlin/Lazy;",
        "leftCirclePaint",
        "getLeftCirclePaint",
        "leftCirclePaint$delegate",
        "radius",
        "",
        "getRadius",
        "()F",
        "rightCirclePaint",
        "getRightCirclePaint",
        "rightCirclePaint$delegate",
        "centerIntersectPaintFactory",
        "drawLeftCircle",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawOverlap",
        "drawRightCircle",
        "fromResource",
        "color",
        "leftCirclePaintFactory",
        "onDraw",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "rightCirclePaintFactory",
        "setEnabled",
        "enabled",
        "",
        "animate",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIDTH_SCALE:F = 1.6470588f


# instance fields
.field private final centerIntersectPaint$delegate:Lkotlin/Lazy;

.field private final leftCirclePaint$delegate:Lkotlin/Lazy;

.field private final rightCirclePaint$delegate:Lkotlin/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->Companion:Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance p1, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$leftCirclePaint$2;

    move-object p2, p0

    check-cast p2, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    invoke-direct {p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$leftCirclePaint$2;-><init>(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->leftCirclePaint$delegate:Lkotlin/Lazy;

    .line 27
    new-instance p1, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$rightCirclePaint$2;

    invoke-direct {p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$rightCirclePaint$2;-><init>(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->rightCirclePaint$delegate:Lkotlin/Lazy;

    .line 28
    new-instance p1, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$centerIntersectPaint$2;

    invoke-direct {p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$centerIntersectPaint$2;-><init>(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->centerIntersectPaint$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 18
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 19
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$centerIntersectPaintFactory(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)Landroid/graphics/Paint;
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->centerIntersectPaintFactory()Landroid/graphics/Paint;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$leftCirclePaintFactory(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)Landroid/graphics/Paint;
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->leftCirclePaintFactory()Landroid/graphics/Paint;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$rightCirclePaintFactory(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)Landroid/graphics/Paint;
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->rightCirclePaintFactory()Landroid/graphics/Paint;

    move-result-object p0

    return-object p0
.end method

.method private final centerIntersectPaintFactory()Landroid/graphics/Paint;
    .locals 2

    .line 125
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 126
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 106
    sget v1, Lcom/squareup/balance/core/R$color;->master_card_intersection:I

    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-object v0
.end method

.method private final drawLeftCircle(Landroid/graphics/Canvas;)V
    .locals 4

    .line 70
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v0

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v1

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v2

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getLeftCirclePaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private final drawOverlap(Landroid/graphics/Canvas;)V
    .locals 5

    .line 60
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 61
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 62
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v1

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v2

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 64
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 65
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v1

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v2

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getCenterIntersectPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 66
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private final drawRightCircle(Landroid/graphics/Canvas;)V
    .locals 4

    .line 74
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v1

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRadius()F

    move-result v2

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getRightCirclePaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private final fromResource(I)I
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method private final getCenterIntersectPaint()Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->centerIntersectPaint$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method

.method private final getLeftCirclePaint()Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->leftCirclePaint$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method

.method private final getRadius()F
    .locals 2

    .line 23
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method private final getRightCirclePaint()Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->rightCirclePaint$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method

.method private final leftCirclePaintFactory()Landroid/graphics/Paint;
    .locals 11

    .line 117
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 118
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getWidth()I

    move-result v4

    .line 81
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getHeight()I

    move-result v5

    const/4 v2, 0x3

    new-array v6, v2, [I

    .line 83
    sget v3, Lcom/squareup/balance/core/R$color;->master_card_left_circle_start:I

    invoke-direct {p0, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v3

    const/4 v7, 0x0

    aput v3, v6, v7

    .line 84
    sget v3, Lcom/squareup/balance/core/R$color;->master_card_left_circle_center:I

    invoke-direct {p0, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v3

    aput v3, v6, v1

    .line 85
    sget v1, Lcom/squareup/balance/core/R$color;->master_card_left_circle_end:I

    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v1

    const/4 v3, 0x2

    aput v1, v6, v3

    new-array v7, v2, [F

    .line 87
    fill-array-data v7, :array_0

    const-wide v2, 0x4046800000000000L    # 45.0

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    .line 78
    invoke-static/range {v2 .. v10}, Lcom/squareup/util/Gradients;->createLinearGradientFromAngle$default(DII[I[FLandroid/graphics/Shader$TileMode;ILjava/lang/Object;)Landroid/graphics/LinearGradient;

    move-result-object v1

    check-cast v1, Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    return-object v0

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private final rightCirclePaintFactory()Landroid/graphics/Paint;
    .locals 11

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 122
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getWidth()I

    move-result v4

    .line 95
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->getHeight()I

    move-result v5

    const/4 v2, 0x3

    new-array v6, v2, [I

    .line 97
    sget v3, Lcom/squareup/balance/core/R$color;->master_card_right_circle_start:I

    invoke-direct {p0, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v3

    const/4 v7, 0x0

    aput v3, v6, v7

    .line 98
    sget v3, Lcom/squareup/balance/core/R$color;->master_card_right_circle_center:I

    invoke-direct {p0, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v3

    aput v3, v6, v1

    .line 99
    sget v1, Lcom/squareup/balance/core/R$color;->master_card_right_circle_end:I

    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->fromResource(I)I

    move-result v1

    const/4 v3, 0x2

    aput v1, v6, v3

    new-array v7, v2, [F

    .line 101
    fill-array-data v7, :array_0

    const-wide v2, 0x4046800000000000L    # 45.0

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    .line 92
    invoke-static/range {v2 .. v10}, Lcom/squareup/util/Gradients;->createLinearGradientFromAngle$default(DII[I[FLandroid/graphics/Shader$TileMode;ILjava/lang/Object;)Landroid/graphics/LinearGradient;

    move-result-object v1

    check-cast v1, Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    return-object v0

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->drawRightCircle(Landroid/graphics/Canvas;)V

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->drawLeftCircle(Landroid/graphics/Canvas;)V

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->drawOverlap(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 34
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    int-to-float p2, p1

    const v0, 0x3fd2d2d3

    mul-float p2, p2, v0

    .line 36
    invoke-static {p2}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result p2

    invoke-virtual {p0, p2, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->setMeasuredDimension(II)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 40
    invoke-virtual {p0, p1, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->setEnabled(ZZ)V

    return-void
.end method

.method public final setEnabled(ZZ)V
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->isEnabled()Z

    move-result v0

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_1
    const v0, 0x3ecccccd    # 0.4f

    .line 49
    :goto_0
    invoke-static {p0, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 50
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method
