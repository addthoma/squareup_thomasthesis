.class public final Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;
.super Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;
.source "SquareCardProgressScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Succeeded"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardProgressScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardProgressScreen.kt\ncom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded\n*L\n1#1,74:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0008\u0013\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001BW\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u0003\u0012\u0014\u0008\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J[\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u00032\u0014\u0008\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u00d6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001J\t\u0010#\u001a\u00020\u000bH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u000e\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
        "actionBarText",
        "",
        "title",
        "message",
        "primaryButtonText",
        "secondaryButtonText",
        "helpText",
        "messageArgs",
        "",
        "",
        "(IIIIIILjava/util/Map;)V",
        "getActionBarText",
        "()I",
        "getHelpText",
        "getMessage",
        "getMessageArgs",
        "()Ljava/util/Map;",
        "getPrimaryButtonText",
        "getSecondaryButtonText",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarText:I

.field private final helpText:I

.field private final message:I

.field private final messageArgs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final primaryButtonText:I

.field private final secondaryButtonText:I

.field private final title:I


# direct methods
.method public constructor <init>(IIIIIILjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIII",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "messageArgs"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    iput p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    iput p3, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    iput p4, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    iput p5, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    iput p6, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    iput-object p7, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(IIIIIILjava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x1

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    const/4 v3, -0x1

    goto :goto_0

    :cond_0
    move v3, p1

    :goto_0
    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_1

    const/4 v4, -0x1

    goto :goto_1

    :cond_1
    move v4, p2

    :goto_1
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_2

    const/4 v5, -0x1

    goto :goto_2

    :cond_2
    move v5, p3

    :goto_2
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_3

    const/4 v7, -0x1

    goto :goto_3

    :cond_3
    move v7, p5

    :goto_3
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_4

    const/4 v8, -0x1

    goto :goto_4

    :cond_4
    move/from16 v8, p6

    :goto_4
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_5

    .line 48
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v0

    move-object v9, v0

    goto :goto_5

    :cond_5
    move-object/from16 v9, p7

    :goto_5
    move-object v2, p0

    move v6, p4

    invoke-direct/range {v2 .. v9}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;-><init>(IIIIIILjava/util/Map;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;IIIIIILjava/util/Map;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->copy(IIIIIILjava/util/Map;)Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    return v0
.end method

.method public final component7()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(IIIIIILjava/util/Map;)Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIII",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;"
        }
    .end annotation

    const-string v0, "messageArgs"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;-><init>(IIIIIILjava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    iget v1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarText()I
    .locals 1

    .line 33
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    return v0
.end method

.method public final getHelpText()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    return v0
.end method

.method public final getMessage()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    return v0
.end method

.method public final getMessageArgs()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    return-object v0
.end method

.method public final getPrimaryButtonText()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    return v0
.end method

.method public final getSecondaryButtonText()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Succeeded(actionBarText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->actionBarText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->title:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->message:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", primaryButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->primaryButtonText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", secondaryButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->secondaryButtonText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", helpText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->helpText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", messageArgs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->messageArgs:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
