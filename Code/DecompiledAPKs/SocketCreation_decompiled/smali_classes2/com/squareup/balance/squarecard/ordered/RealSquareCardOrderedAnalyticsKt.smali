.class public final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalyticsKt;
.super Ljava/lang/Object;
.source "RealSquareCardOrderedAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001e\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0017\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001d\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "ACTIVATE_CARD",
        "",
        "ACTIVATION_CANCELED",
        "ACTIVATION_CODE_ENTRY_ERROR_SCREEN",
        "ACTIVATION_CODE_ENTRY_SCREEN",
        "ACTIVATION_FINISHED_SCREEN",
        "BILLING_ADDRESS_CANCEL",
        "BILLING_ADDRESS_CONTINUE",
        "BILLING_ADDRESS_SCREEN",
        "CARD_VALIDATION_CANCEL",
        "DEPOSITS_ON_DEMAND_CANCEL",
        "DEPOSITS_ON_DEMAND_CONTINUE",
        "DEPOSITS_ON_DEMAND_LEARN_MORE",
        "DEPOSITS_ON_DEMAND_SCREEN",
        "ENTER_ACTIVATION_CODE_CANCEL",
        "ENTER_ACTIVATION_CODE_CONTINUE",
        "FINISH_ACTIVATION",
        "MANUAL_CARD_ENTRY_FAILURE",
        "MANUAL_CARD_ENTRY_SUCCESS",
        "PIN_ENTRY_CANCEL",
        "PIN_ENTRY_CONTINUE",
        "PIN_ENTRY_ERROR_SCREEN",
        "PIN_ENTRY_SCREEN",
        "PROBLEM_WITH_ACTIVATED_CARD",
        "RESEND_ACTIVATION_EMAIL",
        "SEND_ACTIVATION_EMAIL_ERROR_SCREEN",
        "SQUARE_CARD_PENDING_2FA_SCREEN",
        "SQUARE_CARD_PENDING_SCREEN",
        "SQUARE_CARD_VALIDATION_SCREEN",
        "SWIPE_CARD_ENTRY_FAILURE",
        "SWIPE_CARD_ENTRY_SUCCESS",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTIVATE_CARD:Ljava/lang/String; = "Square Card: Activate Card"

.field private static final ACTIVATION_CANCELED:Ljava/lang/String; = "Square Card: Activate Card Cancel"

.field private static final ACTIVATION_CODE_ENTRY_ERROR_SCREEN:Ljava/lang/String; = "Activate Card: Confirmation Code Error"

.field private static final ACTIVATION_CODE_ENTRY_SCREEN:Ljava/lang/String; = "Activate Card: Confirmation Code Validation"

.field private static final ACTIVATION_FINISHED_SCREEN:Ljava/lang/String; = "Activate Card: Card Now Active"

.field private static final BILLING_ADDRESS_CANCEL:Ljava/lang/String; = "Activate Card: Billing Address Cancel"

.field private static final BILLING_ADDRESS_CONTINUE:Ljava/lang/String; = "Activate Card: Billing Address Continue"

.field private static final BILLING_ADDRESS_SCREEN:Ljava/lang/String; = "Activate Card: Billing Address"

.field private static final CARD_VALIDATION_CANCEL:Ljava/lang/String; = "Activate Card: Card Validation Cancel"

.field private static final DEPOSITS_ON_DEMAND_CANCEL:Ljava/lang/String; = "Activate Card: Deposits On Demand Cancel"

.field private static final DEPOSITS_ON_DEMAND_CONTINUE:Ljava/lang/String; = "Activate Card: Deposits On Demand Continue"

.field private static final DEPOSITS_ON_DEMAND_LEARN_MORE:Ljava/lang/String; = "Activate Card: Deposits On Demand Learn More"

.field private static final DEPOSITS_ON_DEMAND_SCREEN:Ljava/lang/String; = "Activate Card: Deposits On Demand"

.field private static final ENTER_ACTIVATION_CODE_CANCEL:Ljava/lang/String; = "Activate Card: Confirmation Code Validation Cancel"

.field private static final ENTER_ACTIVATION_CODE_CONTINUE:Ljava/lang/String; = "Activate Card: Confirmation Code Validation Continue"

.field private static final FINISH_ACTIVATION:Ljava/lang/String; = "Square Card: Finish Activation"

.field private static final MANUAL_CARD_ENTRY_FAILURE:Ljava/lang/String; = "Activate Card: Manual Entry Fail"

.field private static final MANUAL_CARD_ENTRY_SUCCESS:Ljava/lang/String; = "Activate Card: Manual Entry Success"

.field private static final PIN_ENTRY_CANCEL:Ljava/lang/String; = "Activate Card: Create PIN Cancel"

.field private static final PIN_ENTRY_CONTINUE:Ljava/lang/String; = "Activate Card: Create PIN Continue"

.field private static final PIN_ENTRY_ERROR_SCREEN:Ljava/lang/String; = "Activate Card: Create PIN Error"

.field private static final PIN_ENTRY_SCREEN:Ljava/lang/String; = "Activate Card: Create PIN"

.field private static final PROBLEM_WITH_ACTIVATED_CARD:Ljava/lang/String; = "Square Card: Active Page Problem With Card"

.field private static final RESEND_ACTIVATION_EMAIL:Ljava/lang/String; = "Activate Card: Resend Activation Email"

.field private static final SEND_ACTIVATION_EMAIL_ERROR_SCREEN:Ljava/lang/String; = "Activate Card: Send Activation Email Error"

.field private static final SQUARE_CARD_PENDING_2FA_SCREEN:Ljava/lang/String; = "Square Card: Pending 2FA Page"

.field private static final SQUARE_CARD_PENDING_SCREEN:Ljava/lang/String; = "Square Card: Card Pending"

.field private static final SQUARE_CARD_VALIDATION_SCREEN:Ljava/lang/String; = "Activate Card: Card Validation"

.field private static final SWIPE_CARD_ENTRY_FAILURE:Ljava/lang/String; = "Activate Card: Swipe Card Fail"

.field private static final SWIPE_CARD_ENTRY_SUCCESS:Ljava/lang/String; = "Activate Card: Swipe Card Success"
