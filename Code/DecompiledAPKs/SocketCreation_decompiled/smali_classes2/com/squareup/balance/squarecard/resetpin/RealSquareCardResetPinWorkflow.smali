.class public final Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSquareCardResetPinWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquareCardResetPinWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquareCardResetPinWorkflow.kt\ncom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,228:1\n149#2,5:229\n85#3:234\n240#4:235\n276#5:236\n*E\n*S KotlinDebug\n*F\n+ 1 RealSquareCardResetPinWorkflow.kt\ncom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow\n*L\n192#1,5:229\n221#1:234\n221#1:235\n221#1:236\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002B\u0019\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ:\u0010\u0010\u001a\u0018\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0008j\u0008\u0012\u0004\u0012\u00020\u0011`\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017H\u0002J\u001a\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u00032\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J2\u0010\u001e\u001a\u0018\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020\u00120\u0008j\u0008\u0012\u0004\u0012\u00020\u001f`\u00132\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020\u00190\u0017H\u0002JR\u0010!\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010\u001b\u001a\u00020\u00032\u0006\u0010\"\u001a\u00020\u00042\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050$H\u0016J\u0010\u0010%\u001a\u00020\u001d2\u0006\u0010\"\u001a\u00020\u0004H\u0016J:\u0010&\u001a\u0018\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0008j\u0008\u0012\u0004\u0012\u00020\u0011`\u00132\u0006\u0010\'\u001a\u00020(2\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017H\u0002J\u001e\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u00040*2\u0006\u0010+\u001a\u00020(2\u0006\u0010,\u001a\u00020-H\u0002J2\u0010.\u001a\u0018\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0008j\u0008\u0012\u0004\u0012\u00020\u0011`\u00132\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017H\u0002J\u0012\u0010\u0014\u001a\u00020\u0015*\u0008\u0012\u0004\u0012\u0002000/H\u0002J\u0018\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000502*\u000203H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "twoFactorAuthWorkflow",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;)V",
        "errorScreen",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "isWeakPasscode",
        "",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "promptForPinScreen",
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;",
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "successScreen",
        "cardData",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "updatePin",
        "Lcom/squareup/workflow/Worker;",
        "card",
        "pin",
        "",
        "updatingPinScreen",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;",
        "retryOrFinish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final twoFactorAuthWorkflow:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;)V
    .locals 1
    .param p2    # Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;
        .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ResetPasswordTwoFactor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "twoFactorAuthWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->twoFactorAuthWorkflow:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    return-void
.end method

.method public static final synthetic access$isWeakPasscode(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->isWeakPasscode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$retryOrFinish(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->retryOrFinish(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final errorScreen(ZLkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 164
    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_weak_error_title:I

    .line 165
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_weak_error_message:I

    .line 166
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_weak_error_button_text:I

    goto :goto_0

    .line 168
    :cond_0
    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_error_title:I

    .line 169
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_generic_error_message:I

    .line 170
    sget v1, Lcom/squareup/common/strings/R$string;->okay:I

    :goto_0
    move v4, v1

    .line 173
    new-instance v8, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 174
    new-instance v9, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 175
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v1, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 176
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/ViewString;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v9

    .line 174
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v9, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 173
    invoke-direct {v8, v9, p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 180
    invoke-static {v8}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final isWeakPasscode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;",
            ">;)Z"
        }
    .end annotation

    .line 225
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;->result:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    :cond_1
    sget-object p1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED_WEAK_PASSCODE:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    if-ne v1, p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final promptForPinScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 186
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;

    .line 187
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

    .line 188
    sget v2, Lcom/squareup/common/strings/R$string;->next:I

    .line 189
    new-instance v3, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData$ActionBarConfig;

    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->reset_square_card_pin_title:I

    sget-object v5, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    invoke-direct {v3, v4, v5}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData$ActionBarConfig;-><init>(ILcom/squareup/noho/UpIcon;)V

    .line 187
    invoke-direct {v1, v2, v3}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;-><init>(ILcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData$ActionBarConfig;)V

    .line 186
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 230
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 231
    const-class v1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 232
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 230
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final retryOrFinish(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
            ">;"
        }
    .end annotation

    .line 197
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;->isWeakPassword()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 198
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 200
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult$Back;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult$Back;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final successScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 144
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 145
    new-instance v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;

    .line 146
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_reset_pin_success_title:I

    .line 147
    sget v5, Lcom/squareup/common/strings/R$string;->finish:I

    .line 148
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_reset_pin_success_message:I

    .line 149
    invoke-static {p1}, Lcom/squareup/balance/squarecard/utility/CardFormatterKt;->getFormattedBrandAndUnmaskedDigits(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "card_name"

    invoke-static {v1, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x31

    const/4 v10, 0x0

    move-object v1, v11

    .line 145
    invoke-direct/range {v1 .. v10}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;-><init>(IIIIIILjava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 144
    invoke-direct {v0, v11, p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 152
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final updatePin(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
            ">;"
        }
    .end annotation

    .line 208
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;-><init>()V

    .line 209
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;

    move-result-object p1

    .line 210
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;->passcode(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;

    move-result-object p1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;

    move-result-object p1

    .line 213
    iget-object p2, p0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->setPasscode(Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;

    move-result-object p1

    .line 214
    invoke-virtual {p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 215
    new-instance p2, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$1;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$1;-><init>(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService.setPassco\u2026())\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 235
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 236
    const-class p2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method private final updatingPinScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 135
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_reset_pin_loading:I

    invoke-direct {v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 134
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 137
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 69
    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;

    invoke-virtual {p1, p2}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->initialState(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;

    check-cast p2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->render(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
            "-",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object p2, p0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->twoFactorAuthWorkflow:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 78
    new-instance v2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 80
    sget p2, Lcom/squareup/balance/squarecard/impl/R$string;->reset_square_card_pin_title:I

    .line 78
    invoke-direct {v2, p1, p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)V

    const/4 v3, 0x0

    .line 82
    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 76
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 88
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->promptForPinScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 94
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_1
    instance-of v0, p2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPin;

    if-eqz v0, :cond_2

    .line 96
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    check-cast p2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPin;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPin;->getPin()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->updatePin(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$3;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$3;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 101
    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$4;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$4;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->updatingPinScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 106
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 108
    :cond_2
    instance-of v0, p2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;

    if-eqz v0, :cond_3

    .line 109
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;->isWeakPassword()Z

    move-result p1

    .line 110
    new-instance v0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$5;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$5;-><init>(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 108
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->errorScreen(ZLkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 116
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 117
    :cond_3
    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 118
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 119
    sget-object p2, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$6;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$6;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 117
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->successScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 125
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;->snapshotState(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
