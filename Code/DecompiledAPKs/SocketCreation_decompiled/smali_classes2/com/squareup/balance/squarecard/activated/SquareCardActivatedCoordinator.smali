.class public final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardActivatedCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivatedCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivatedCoordinator.kt\ncom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,269:1\n1103#2,7:270\n1103#2,7:277\n1103#2,7:284\n1103#2,7:291\n1103#2,7:298\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardActivatedCoordinator.kt\ncom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator\n*L\n191#1,7:270\n200#1,7:277\n209#1,7:284\n216#1,7:291\n223#1,7:298\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001:\u00013B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u000cH\u0016J\u0010\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u000cH\u0002J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010!\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010\"\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010#\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0018\u0010$\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020&2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010\'\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010(\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010)\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010*\u001a\u00020\u001c2\u0006\u0010+\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0018\u0010.\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u000c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u0010/\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u00100\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u00101\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002J\u0010\u00102\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u0005H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addToGooglePay",
        "Landroid/view/View;",
        "cardDisabled",
        "cardIsSuspended",
        "Lcom/squareup/widgets/MessageView;",
        "firstLoad",
        "",
        "getHelpWithCard",
        "notifications",
        "resetCardPin",
        "showCardDetails",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "squareCard",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;",
        "toggleCard",
        "viewBillingAddress",
        "attach",
        "",
        "view",
        "bindViews",
        "handleBack",
        "screen",
        "hidePrivateCardDetails",
        "maybeShowAddToGooglePay",
        "maybeShowCardIsDisabled",
        "maybeShowCardIsSuspended",
        "context",
        "Landroid/content/Context;",
        "maybeShowGetHelp",
        "maybeShowNotifications",
        "maybeShowResetCardPin",
        "setNameOnCard",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "showBillingAddress",
        "update",
        "updateActionBar",
        "updateCardPreviewState",
        "updateShowCardDetailsToggle",
        "updateToggleCard",
        "CardSuspendedLinkSpan",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addToGooglePay:Landroid/view/View;

.field private cardDisabled:Landroid/view/View;

.field private cardIsSuspended:Lcom/squareup/widgets/MessageView;

.field private firstLoad:Z

.field private getHelpWithCard:Landroid/view/View;

.field private notifications:Landroid/view/View;

.field private resetCardPin:Landroid/view/View;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

.field private squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

.field private toggleCard:Lcom/squareup/noho/NohoCheckableRow;

.field private viewBillingAddress:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->screens:Lio/reactivex/Observable;

    const/4 p1, 0x1

    .line 64
    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->firstLoad:Z

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->handleBack(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;Landroid/view/View;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->update(Landroid/view/View;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 249
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->get_help_with_card:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->getHelpWithCard:Landroid/view/View;

    .line 250
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->add_to_google_pay:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->addToGooglePay:Landroid/view/View;

    .line 251
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->reset_card_pin:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->resetCardPin:Landroid/view/View;

    .line 252
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->view_billing_address:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->viewBillingAddress:Landroid/view/View;

    .line 253
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->view_notifications:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->notifications:Landroid/view/View;

    .line 254
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->toggle_card_state:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    .line 255
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->toggle_show_card_details:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    .line 256
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->card_suspended_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->cardIsSuspended:Lcom/squareup/widgets/MessageView;

    .line 257
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    .line 258
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->card_disabled:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->cardDisabled:Landroid/view/View;

    .line 259
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method

.method private final handleBack(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getAllowBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$GoBack;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private final hidePrivateCardDetails(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 3

    .line 172
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    const-string v1, "squareCard"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    invoke-direct {v2, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;

    invoke-virtual {v0, v2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setPan(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->hideExpirationAndCvc()V

    return-void
.end method

.method private final maybeShowAddToGooglePay(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 3

    .line 215
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->addToGooglePay:Landroid/view/View;

    const-string v1, "addToGooglePay"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowAddToGooglePay()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 216
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->addToGooglePay:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 291
    :cond_1
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowAddToGooglePay$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowAddToGooglePay$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final maybeShowCardIsDisabled(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->cardDisabled:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "cardDisabled"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowCardDisabled()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrInvisible(Landroid/view/View;Z)V

    return-void
.end method

.method private final maybeShowCardIsSuspended(Landroid/content/Context;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 4

    .line 232
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->cardIsSuspended:Lcom/squareup/widgets/MessageView;

    const-string v1, "cardIsSuspended"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowCardIsSuspended()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 241
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->cardIsSuspended:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 233
    :cond_1
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 234
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_suspended_message:I

    const-string v3, "learn_more"

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 235
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_suspended_message_learn_more:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 237
    new-instance v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;

    .line 238
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 237
    invoke-direct {v2, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;-><init>(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/ui/LinkSpan;

    .line 236
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 241
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final maybeShowGetHelp(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 3

    .line 199
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->getHelpWithCard:Landroid/view/View;

    const-string v1, "getHelpWithCard"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowGetHelp()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 200
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->getHelpWithCard:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 277
    :cond_1
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowGetHelp$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowGetHelp$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final maybeShowNotifications(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 3

    .line 208
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->notifications:Landroid/view/View;

    const-string v1, "notifications"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowNotificationPreferences()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 209
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->notifications:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 284
    :cond_1
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowNotifications$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowNotifications$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final maybeShowResetCardPin(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 3

    .line 222
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->resetCardPin:Landroid/view/View;

    const-string v1, "resetCardPin"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowResetCardPin()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 223
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->resetCardPin:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 298
    :cond_1
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowResetCardPin$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$maybeShowResetCardPin$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setNameOnCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_0

    const-string v1, "squareCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private final showBillingAddress(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->viewBillingAddress:Landroid/view/View;

    if-nez v0, :cond_0

    const-string/jumbo v1, "viewBillingAddress"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 270
    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$showBillingAddress$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$showBillingAddress$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 1

    .line 76
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 78
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->updateActionBar(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 79
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->updateShowCardDetailsToggle(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 80
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->updateToggleCard(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 81
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->updateCardPreviewState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 83
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showBillingAddress(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 84
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->maybeShowAddToGooglePay(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 85
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->maybeShowCardIsDisabled(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 86
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->maybeShowGetHelp(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 87
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->maybeShowNotifications(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->maybeShowCardIsSuspended(Landroid/content/Context;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 89
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->maybeShowResetCardPin(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    .line 91
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->setNameOnCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    const/4 p1, 0x0

    .line 93
    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->firstLoad:Z

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 4

    .line 181
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 177
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 178
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->square_card:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 179
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 180
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getAllowBack()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 181
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateCardPreviewState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 2

    .line 103
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getToggleIsInProgress()Z

    move-result v0

    const-string v1, "squareCard"

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getPrivateDataToggleState()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;->inProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->stopAnimating()V

    .line 107
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowCardDisabled()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->firstLoad:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setEnabled(ZZ)V

    goto :goto_1

    .line 104
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->startAnimatingIndefinitely()V

    :goto_1
    return-void
.end method

.method private final updateShowCardDetailsToggle(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 4

    .line 132
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getPrivateDataToggleState()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    move-result-object v0

    .line 133
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible;

    const-string v2, "showCardDetails"

    if-eqz v1, :cond_9

    .line 134
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 136
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v3, 0x0

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 137
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    instance-of v3, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 139
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;->inProgress()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoCheckableRow;->setClickable(Z)V

    .line 140
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;->inProgress()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoCheckableRow;->setCheckableEnabled(Z)V

    .line 143
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateShowCardDetailsToggle$1;

    invoke-direct {v2, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateShowCardDetailsToggle$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 148
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loaded;

    if-eqz v1, :cond_8

    .line 150
    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loaded;->getPrivateDetails()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    move-result-object p1

    .line 151
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    const-string v1, "squareCard"

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v2, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Full;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->getFullPan()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Full;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;

    invoke-virtual {v0, v2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setPan(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 153
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->getMonth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x30

    const/4 v3, 0x2

    invoke-static {v1, v3, v2}, Lcom/squareup/util/Strings;->padStart(Ljava/lang/String;IC)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->getYear()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/text/StringsKt;->takeLast(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->getCvc()Ljava/lang/String;

    move-result-object p1

    .line 152
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setExpirationAndCvc(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_8
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->hidePrivateCardDetails(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    goto :goto_0

    .line 163
    :cond_9
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 164
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->showCardDetails:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 166
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->hidePrivateCardDetails(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    :cond_b
    :goto_0
    return-void
.end method

.method private final updateToggleCard(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V
    .locals 4

    .line 112
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getShowCardToggle()Z

    move-result v0

    const-string v1, "toggleCard"

    if-eqz v0, :cond_6

    .line 113
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 115
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v3, 0x0

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getToggleIsChecked()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 118
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getToggleIsInProgress()Z

    move-result v3

    xor-int/2addr v3, v2

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableRow;->setClickable(Z)V

    .line 119
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->getToggleIsInProgress()Z

    move-result v3

    xor-int/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setCheckableEnabled(Z)V

    .line 122
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateToggleCard$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$updateToggleCard$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    goto :goto_0

    .line 127
    :cond_6
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->toggleCard:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->bindViews(Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026iew, it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
