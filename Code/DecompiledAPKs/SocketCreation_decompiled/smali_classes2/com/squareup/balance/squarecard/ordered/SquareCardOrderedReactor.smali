.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;
.super Ljava/lang/Object;
.source "SquareCardOrderedReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;,
        Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Reactor<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardOrderedReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardOrderedReactor.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor\n*L\n1#1,658:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002BCB/\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0016\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0008\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0015H\u0002J\u0010\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0015H\u0002J\u0010\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0016\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001e2\u0006\u0010\u001f\u001a\u00020\u0015H\u0002J2\u0010 \u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\"0!2\u0006\u0010\u0012\u001a\u00020\u00022\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00030$H\u0016J\u001e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00112\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J\u001e\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00112\u0006\u0010&\u001a\u00020\'2\u0006\u0010+\u001a\u00020,H\u0002J\u0016\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00112\u0006\u0010&\u001a\u00020\'H\u0002J\u0016\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00112\u0006\u0010&\u001a\u00020\'H\u0002J\u0010\u0010/\u001a\u00020\u00022\u0006\u0010&\u001a\u00020\'H\u0002J$\u00100\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000401j\u0002`22\u0006\u00103\u001a\u00020\'J$\u00100\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000401j\u0002`22\u0006\u00104\u001a\u000205J\u001e\u00106\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00112\u0006\u0010&\u001a\u00020\'2\u0006\u00107\u001a\u00020,H\u0002J\u0012\u00108\u001a\u00020\u0015*\u0008\u0012\u0004\u0012\u00020:09H\u0002J\u0012\u0010;\u001a\u00020\u0015*\u0008\u0012\u0004\u0012\u00020<09H\u0002J\u0012\u0010=\u001a\u00020>*\u0008\u0012\u0004\u0012\u00020?09H\u0002J\u000c\u0010@\u001a\u00020A*\u00020\u0013H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006D"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "cardConverter",
        "Lcom/squareup/payment/CardConverter;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "analytics",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;",
        "cardReaderOracle",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/payment/CardConverter;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V",
        "finishCardActivationRequest",
        "Lio/reactivex/Single;",
        "state",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;",
        "isBillingDuringActivationEnabled",
        "",
        "logCardActivationFailure",
        "",
        "fromSwipe",
        "logCardActivationSuccess",
        "logCardPendingScreen",
        "cardState",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;",
        "onCardReaderStateUpdated",
        "Lrx/Observable;",
        "currentState",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "setBillingAddress",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "address",
        "Lcom/squareup/address/Address;",
        "setPin",
        "pin",
        "",
        "startCardActivation",
        "startConfirmAddress",
        "startOrSkipStartingCardActivation",
        "startWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflow;",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "verifyCardActivation",
        "token",
        "isIncorrectCardInfo",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;",
        "isWeakPasscode",
        "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;",
        "toFailureResult",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
        "toFinishCardActivationRequest",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;",
        "SquareCardOrderedEvent",
        "SquareCardOrderedState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final cardConverter:Lcom/squareup/payment/CardConverter;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/payment/CardConverter;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardConverter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderOracle"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->cardConverter:Lcom/squareup/payment/CardConverter;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    return-void
.end method

.method public static final synthetic access$finishCardActivationRequest(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;)Lio/reactivex/Single;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->finishCardActivationRequest(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    return-object p0
.end method

.method public static final synthetic access$isBillingDuringActivationEnabled(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;)Z
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->isBillingDuringActivationEnabled()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isIncorrectCardInfo(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->isIncorrectCardInfo(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isWeakPasscode(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->isWeakPasscode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$logCardActivationFailure(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Z)V
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->logCardActivationFailure(Z)V

    return-void
.end method

.method public static final synthetic access$logCardActivationSuccess(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Z)V
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->logCardActivationSuccess(Z)V

    return-void
.end method

.method public static final synthetic access$onCardReaderStateUpdated(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Z)Lrx/Observable;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->onCardReaderStateUpdated(Z)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setBillingAddress(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/address/Address;)Lio/reactivex/Single;
    .locals 0

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->setBillingAddress(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/address/Address;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setPin(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->setPin(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startCardActivation(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->startCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startConfirmAddress(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->startConfirmAddress(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startOrSkipStartingCardActivation(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->startOrSkipStartingCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toFailureResult(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->toFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$verifyCardActivation(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->verifyCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final finishCardActivationRequest(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ">;"
        }
    .end annotation

    .line 536
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getActivationToken()Ljava/lang/String;

    move-result-object v0

    .line 537
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    .line 538
    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 539
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->toFinishCardActivationRequest(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->finishCardActivation(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$FinishCardActivationStandardResponse;

    move-result-object v2

    .line 540
    invoke-virtual {v2}, Lcom/squareup/balance/core/server/bizbank/BizbankService$FinishCardActivationStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    .line 541
    new-instance v3, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;

    invoke-direct {v3, p0, p1, v1, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final isBillingDuringActivationEnabled()Z
    .locals 2

    .line 639
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_ACTIVATION_BILLING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final isIncorrectCardInfo(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;",
            ">;)Z"
        }
    .end annotation

    .line 650
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;->result:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    :cond_1
    sget-object p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INCORRECT_CARD_INFO:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    if-ne v1, p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isWeakPasscode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;",
            ">;)Z"
        }
    .end annotation

    .line 655
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;->result:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    :cond_1
    sget-object p1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED_WEAK_PASSCODE:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    if-ne v1, p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final logCardActivationFailure(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 572
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logSwipeCardEntryFailure()V

    goto :goto_0

    .line 574
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logManualEntryFailure()V

    :goto_0
    return-void
.end method

.method private final logCardActivationSuccess(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 564
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logSwipeCardEntrySuccess()V

    goto :goto_0

    .line 566
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logManualEntrySuccess()V

    :goto_0
    return-void
.end method

.method private final logCardPendingScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;)V
    .locals 1

    .line 631
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne p1, v0, :cond_0

    .line 632
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logSquareCardPending2FAScreen()V

    goto :goto_0

    .line 634
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logSquareCardPendingScreen()V

    :goto_0
    return-void
.end method

.method private final onCardReaderStateUpdated(Z)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 620
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    .line 621
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->features:Lcom/squareup/settings/server/Features;

    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 622
    sget-object v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onCardReaderStateUpdated$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onCardReaderStateUpdated$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 626
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onCardReaderStateUpdated$2;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onCardReaderStateUpdated$2;-><init>(Z)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 627
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    const-string v0, "cardReaderOracle.readerS\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final setBillingAddress(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/address/Address;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/address/Address;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ">;"
        }
    .end annotation

    .line 497
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest$Builder;-><init>()V

    .line 498
    iget-object v1, p2, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    :goto_0
    invoke-virtual {p2, v1}, Lcom/squareup/address/Address;->toGlobalAddress(Lcom/squareup/CountryCode;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest$Builder;->billing_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest$Builder;

    move-result-object p2

    .line 499
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest;

    move-result-object p2

    .line 501
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "setBillingAddressRequest"

    .line 502
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p2}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->setCardBillingAddress(Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p2

    .line 503
    invoke-virtual {p2}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p2

    .line 504
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$setBillingAddress$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$setBillingAddress$1;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService\n        .\u2026rd)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final setPin(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ">;"
        }
    .end annotation

    .line 582
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;-><init>()V

    .line 583
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;

    move-result-object v0

    .line 584
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;->passcode(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;

    move-result-object p2

    .line 585
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/SetPasscodeRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;

    move-result-object p2

    .line 586
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p2}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->setPasscode(Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;

    move-result-object p2

    .line 587
    invoke-virtual {p2}, Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p2

    .line 588
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$setPin$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$setPin$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService.setPassco\u2026())\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final startCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ">;"
        }
    .end annotation

    .line 462
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 464
    new-instance v1, Lcom/squareup/protos/client/bizbank/StartCardActivationRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/StartCardActivationRequest$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bizbank/StartCardActivationRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartCardActivationRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartCardActivationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/StartCardActivationRequest;

    move-result-object v1

    const-string v2, "StartCardActivationReque\u2026(card.card_token).build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 463
    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->startCardActivation(Lcom/squareup/protos/client/bizbank/StartCardActivationRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 466
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 467
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$startCardActivation$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$startCardActivation$1;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026rd)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final startConfirmAddress(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ">;"
        }
    .end annotation

    .line 476
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 478
    new-instance v1, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;

    move-result-object v1

    const-string v2, "GetCardBillingAddressRequest.Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 477
    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getCardBillingAddress(Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 480
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 481
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$startConfirmAddress$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$startConfirmAddress$1;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026TY)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final startOrSkipStartingCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
    .locals 2

    .line 454
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v0, v1, :cond_0

    .line 455
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_0

    .line 457
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivation;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivation;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    :goto_0
    return-object v0
.end method

.method private final toFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;"
        }
    .end annotation

    .line 644
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    if-eqz p1, :cond_1

    goto :goto_0

    .line 645
    :cond_1
    sget-object p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->FAILED:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    :goto_0
    return-object p1
.end method

.method private final toFinishCardActivationRequest(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
    .locals 3

    .line 603
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;-><init>()V

    .line 605
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getSwipeCard()Lcom/squareup/Card;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 606
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->cardConverter:Lcom/squareup/payment/CardConverter;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getSwipeCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    goto :goto_0

    .line 608
    :cond_0
    new-instance v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;-><init>()V

    .line 609
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getCvv()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;

    move-result-object v1

    .line 610
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getExpiration()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->expiration_mmyy(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;

    move-result-object v1

    .line 611
    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    move-result-object v1

    .line 612
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    .line 615
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getActivationToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->activation_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    move-result-object p1

    .line 616
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object p1

    const-string v0, "FinishCardActivationRequ\u2026onToken)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final verifyCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ">;"
        }
    .end annotation

    .line 516
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 518
    new-instance v1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest$Builder;-><init>()V

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest$Builder;->activation_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest;

    move-result-object v1

    const-string v2, "VerifyCardActivationToke\u2026tion_token(token).build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 517
    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->verifyCardActivationToken(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;

    move-result-object v0

    .line 520
    invoke-virtual {v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 521
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService\n        .\u2026  )\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public onAbandoned(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/Reactor$DefaultImpls;->onAbandoned(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onAbandoned(Ljava/lang/Object;)V
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->onAbandoned(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    return-void
.end method

.method public onReact(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
            ">;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const-string v1, "state.card.card_state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->logCardPendingScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;)V

    .line 231
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 250
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingDepositsInfo;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logDepositsOnDemandScreen()V

    .line 252
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$2;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 267
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivation;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$3;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 272
    :cond_2
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivationFailed;

    if-eqz v0, :cond_3

    .line 273
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logSendActivationEmailErrorScreen()V

    .line 274
    sget-object p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$4;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$4;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 280
    :cond_3
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;

    if-eqz v0, :cond_4

    .line 281
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logEnterActivationCodeScreen()V

    .line 282
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 297
    :cond_4
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$6;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$6;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 306
    :cond_5
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartCardConfirmAddress;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$7;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$7;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 310
    :cond_6
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    if-eqz v0, :cond_7

    .line 311
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logBillingAddressScreen()V

    .line 312
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$8;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 332
    :cond_7
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$9;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$9;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 337
    :cond_8
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$10;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$10;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 341
    :cond_9
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    if-eqz v0, :cond_a

    .line 342
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logActivationCodeErrorScreen()V

    .line 343
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$11;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$11;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 354
    :cond_a
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    if-eqz v0, :cond_b

    .line 355
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logCardValidationScreen()V

    .line 356
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$12;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$12;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 378
    :cond_b
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    if-eqz v0, :cond_c

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$13;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$13;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 388
    :cond_c
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    if-eqz v0, :cond_d

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 409
    :cond_d
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    if-eqz v0, :cond_e

    .line 410
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logPinEntryScreen()V

    .line 411
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$15;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$15;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 422
    :cond_e
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPin;

    if-eqz v0, :cond_f

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$16;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$16;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 430
    :cond_f
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    if-eqz v0, :cond_10

    .line 431
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logPinEntryErrorScreen()V

    .line 432
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$17;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$17;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 445
    :cond_10
    instance-of p1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingSquareCardActivationComplete;

    if-eqz p1, :cond_11

    .line 446
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->analytics:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logActivationFinishedScreen()V

    .line 447
    sget-object p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$18;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$18;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_11
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->onReact(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
