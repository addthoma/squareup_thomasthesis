.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateSubscriptions(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "previewData",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;)V
    .locals 3

    .line 256
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$enableControls(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Z)V

    .line 257
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;->$screen:Lcom/squareup/workflow/legacy/Screen;

    const-string v2, "previewData"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$showPreviewOrEmitError(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 80
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;->accept(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;)V

    return-void
.end method
