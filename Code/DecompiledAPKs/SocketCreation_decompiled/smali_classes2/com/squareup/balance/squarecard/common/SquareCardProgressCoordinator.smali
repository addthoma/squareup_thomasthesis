.class public final Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardProgressCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardProgressCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardProgressCoordinator.kt\ncom/squareup/balance/squarecard/common/SquareCardProgressCoordinator\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,142:1\n151#2,2:143\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardProgressCoordinator.kt\ncom/squareup/balance/squarecard/common/SquareCardProgressCoordinator\n*L\n108#1,2:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use SquareCardProgressV2Coordinator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0002J\u0016\u0010\u0015\u001a\u00020\u00122\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J(\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00102\u0016\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0010\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010 \u001a\u00020\u00122\u0006\u0010!\u001a\u00020\"H\u0002J\u001e\u0010#\u001a\u00020\u00122\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00172\u0006\u0010$\u001a\u00020%H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgressScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "loadingText",
        "Landroid/widget/TextView;",
        "resultsMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "spinnerHolder",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "handleBack",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "toggleLoadingState",
        "isLoading",
        "",
        "update",
        "screen",
        "updateFailed",
        "failed",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;",
        "updateLoading",
        "loading",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;",
        "updateSucceeded",
        "succeeded",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private loadingText:Landroid/widget/TextView;

.field private resultsMessage:Lcom/squareup/noho/NohoMessageView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private spinnerHolder:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 136
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 137
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_progress_spinner_holder:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->spinnerHolder:Landroid/view/View;

    .line 138
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_progress_spinner_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->loadingText:Landroid/widget/TextView;

    .line 139
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_progress_result:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;)V"
        }
    .end annotation

    .line 132
    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final toggleLoadingState(Z)V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->spinnerHolder:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "spinnerHolder"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 128
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "resultsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 57
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$update$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "resultsMessage"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 59
    :cond_0
    new-instance v2, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$update$2;

    invoke-direct {v2, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$update$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    const-string v3, "debounceRunnable { workf\u2026endEvent(PrimaryAction) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 61
    :cond_1
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$update$3;

    invoke-direct {v1, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$update$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    const-string v2, "debounceRunnable { workf\u2026dEvent(SecondaryAction) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 63
    iget-object p1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;

    .line 64
    instance-of p2, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;

    if-eqz p2, :cond_2

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->updateLoading(Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;)V

    goto :goto_0

    .line 65
    :cond_2
    instance-of p2, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    if-eqz p2, :cond_3

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->updateFailed(Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;)V

    goto :goto_0

    .line 66
    :cond_3
    instance-of p2, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;

    if-eqz p2, :cond_4

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;

    invoke-direct {p0, v0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->updateSucceeded(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private final updateFailed(Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;)V
    .locals 5

    const/4 v0, 0x0

    .line 77
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->toggleLoadingState(Z)V

    .line 79
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "resultsMessage"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 82
    iget-object v2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v3, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 83
    iget-object v2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v3

    const-string v4, "resource"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v2, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->getMessage()Lcom/squareup/util/ViewString;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->getPrimaryButtonText()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 87
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;->getSecondaryButtonText()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    return-void
.end method

.method private final updateLoading(Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;)V
    .locals 2

    const/4 v0, 0x1

    .line 71
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->toggleLoadingState(Z)V

    .line 72
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->loadingText:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "loadingText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;->getMessage()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private final updateSucceeded(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 94
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->toggleLoadingState(Z)V

    .line 96
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getActionBarText()I

    move-result v1

    const/4 v2, -0x1

    const-string v3, "actionBar"

    if-eq v1, v2, :cond_2

    .line 100
    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 97
    :cond_0
    new-instance v4, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 98
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getActionBarText()I

    move-result v6

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v5, Lcom/squareup/resources/TextModel;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v4

    .line 99
    sget-object v5, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v6, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$updateSucceeded$1;

    invoke-direct {v6, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$updateSucceeded$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v4, v5, v6}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    goto :goto_0

    .line 103
    :cond_2
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 106
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getMessage()I

    move-result p1

    const-string v0, "resultsMessage"

    if-eq p1, v2, :cond_7

    .line 107
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getMessage()I

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 109
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getMessageArgs()Ljava/util/Map;

    move-result-object v1

    .line 143
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 110
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto :goto_1

    .line 113
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 115
    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v1, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 118
    :cond_7
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_8

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    sget v1, Lcom/squareup/vectoricons/R$drawable;->circle_check_96:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 119
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getTitle()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_a

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getPrimaryButtonText()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 122
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_b

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getSecondaryButtonText()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 123
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_c

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Succeeded;->getHelpText()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setHelp(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->bindViews(Landroid/view/View;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardProgressCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
