.class public final Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "MaybeCancelBizbankWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMaybeCancelBizbankWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MaybeCancelBizbankWorkflow.kt\ncom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,218:1\n32#2,12:219\n*E\n*S KotlinDebug\n*F\n+ 1 MaybeCancelBizbankWorkflow.kt\ncom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow\n*L\n114#1,12:219\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001:\u0001#BU\u0008\u0007\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0007\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0007\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u001a\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00022\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J,\u0010\u001b\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u00032\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u0003H\u0016J\u000c\u0010 \u001a\u00020!*\u00020\"H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "offerToDeactivateBizbankWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
        "confirmBizbankStatusWorkflow",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
        "cancelingBizbankWorkflow",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
        "cancelBizbankSuccessWorkflow",
        "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
        "cancelBizbankFailedWorkflow",
        "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
        "analytics",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V",
        "determineInitialState",
        "isBizbankActive",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "toActionBarText",
        "",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

.field private final cancelBizbankFailedWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelBizbankSuccessWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelingBizbankWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmBizbankStatusWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final offerToDeactivateBizbankWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow;",
            ">;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "offerToDeactivateBizbankWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmBizbankStatusWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelingBizbankWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelBizbankSuccessWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelBizbankFailedWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->offerToDeactivateBizbankWorkflow:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->confirmBizbankStatusWorkflow:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->cancelingBizbankWorkflow:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->cancelBizbankSuccessWorkflow:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->cancelBizbankFailedWorkflow:Ljavax/inject/Provider;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->analytics:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->analytics:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    return-object p0
.end method

.method private final determineInitialState(Z)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;
    .locals 0

    if-eqz p1, :cond_0

    .line 169
    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$OfferingToDeactivateBizbank;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$OfferingToDeactivateBizbank;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingConfirmationScreen;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingConfirmationScreen;

    :goto_0
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;

    return-object p1
.end method

.method private final toActionBarText(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)I
    .locals 1

    .line 173
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_lost_card_action_bar:I

    goto :goto_0

    .line 174
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_stolen_card_action_bar:I

    goto :goto_0

    .line 175
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_never_received_card_action_bar:I

    goto :goto_0

    .line 176
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_action_bar:I

    :goto_0
    return p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 219
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 224
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 226
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 227
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 228
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 230
    :cond_3
    check-cast v2, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 114
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;->isBizbankActive()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->determineInitialState(Z)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->initialState(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
            ">;)",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingConfirmationScreen;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingConfirmationScreen;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->confirmBizbankStatusWorkflow:Ljavax/inject/Provider;

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    const-string v0, "confirmBizbankStatusWorkflow.get()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p2

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 124
    new-instance v3, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;

    .line 125
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->toActionBarText(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)I

    move-result p1

    .line 124
    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusProps;-><init>(I)V

    const/4 v4, 0x0

    .line 127
    new-instance p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 122
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    goto/16 :goto_0

    .line 133
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$OfferingToDeactivateBizbank;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$OfferingToDeactivateBizbank;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->offerToDeactivateBizbankWorkflow:Ljavax/inject/Provider;

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    const-string v0, "offerToDeactivateBizbankWorkflow.get()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p2

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 135
    new-instance v3, Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankProps;

    .line 136
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->toActionBarText(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)I

    move-result p1

    .line 135
    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankProps;-><init>(I)V

    const/4 v4, 0x0

    .line 138
    new-instance p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$2;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$2;-><init>(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 133
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    goto/16 :goto_0

    .line 146
    :cond_1
    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$DeactivatingBizbank;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$DeactivatingBizbank;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->cancelingBizbankWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "cancelingBizbankWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$3;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$3;-><init>(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    goto :goto_0

    .line 153
    :cond_2
    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingFailure;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->cancelBizbankFailedWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "cancelBizbankFailedWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$4;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$4;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    goto :goto_0

    .line 156
    :cond_3
    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingSuccess;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 157
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->cancelBizbankSuccessWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "cancelBizbankSuccessWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$5;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$5;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;

    check-cast p2, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->render(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
