.class public abstract Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;
.super Ljava/lang/Object;
.source "SquareCardActivatedState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ScreenState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$StartResetPin;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayBillingAddress;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayNotificationPreferences;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u0003\u0004\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0008\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;",
        "",
        "()V",
        "DisplayBillingAddress",
        "DisplayNotificationPreferences",
        "DisplayingPrivateDataTwoFactor",
        "GooglePayResult",
        "ShowingCardDetails",
        "ShowingLearnMoreAboutSuspendedCardDialog",
        "ShowingLoadingGooglePay",
        "StartResetPin",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$StartResetPin;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayBillingAddress;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayNotificationPreferences;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;-><init>()V

    return-void
.end method
