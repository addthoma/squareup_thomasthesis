.class public abstract Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;
.super Ljava/lang/Object;
.source "SquareCardActivatedScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ToggleState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible;,
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;",
        "",
        "()V",
        "inProgress",
        "",
        "Hidden",
        "Visible",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;-><init>()V

    return-void
.end method


# virtual methods
.method public final inProgress()Z
    .locals 1

    .line 29
    instance-of v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loading;

    return v0
.end method
