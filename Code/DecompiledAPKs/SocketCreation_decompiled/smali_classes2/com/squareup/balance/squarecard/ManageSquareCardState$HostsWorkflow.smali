.class public abstract Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;
.super Lcom/squareup/balance/squarecard/ManageSquareCardState;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HostsWorkflow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0003:\u0002\u0014\u0015B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013RR\u0010\u0005\u001aB\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0000\u0012&\u0012$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\u000b0\u0006j\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0000`\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u0082\u0001\u0002\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;",
        "O",
        "",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "()V",
        "subWorkflow",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/workflow/legacyintegration/PosScreenWorkflowAdapter;",
        "getSubWorkflow",
        "()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "wrapDelegateSnapshotWithOptionalCard",
        "Lcom/squareup/workflow/Snapshot;",
        "subSnapshot",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "OrderingCard",
        "WithCard",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 90
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;-><init>()V

    return-void
.end method

.method public static synthetic wrapDelegateSnapshotWithOptionalCard$default(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;Lcom/squareup/workflow/Snapshot;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ILjava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 119
    check-cast p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;->wrapDelegateSnapshotWithOptionalCard(Lcom/squareup/workflow/Snapshot;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: wrapDelegateSnapshotWithOptionalCard"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abstract getSubWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "*TO;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end method

.method public final wrapDelegateSnapshotWithOptionalCard(Lcom/squareup/workflow/Snapshot;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "subSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$wrapDelegateSnapshotWithOptionalCard$1;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;Lcom/squareup/workflow/Snapshot;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
