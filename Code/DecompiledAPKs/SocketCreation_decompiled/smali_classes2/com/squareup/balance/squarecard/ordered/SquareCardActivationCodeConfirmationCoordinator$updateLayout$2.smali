.class final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;
.super Ljava/lang/Object;
.source "SquareCardActivationCodeConfirmationCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->updateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event$CodeConfirmationData;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$2;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->access$getCodeConfirmationInput$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;)Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event$CodeConfirmationData;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
