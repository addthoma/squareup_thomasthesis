.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromStampsGallery;
.super Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;
.source "OrderSquareCardReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnBackFromStampsGallery"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromStampsGallery;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "stamp",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "(Lcom/squareup/protos/client/bizbank/Stamp;)V",
        "getStamp",
        "()Lcom/squareup/protos/client/bizbank/Stamp;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final stamp:Lcom/squareup/protos/client/bizbank/Stamp;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bizbank/Stamp;)V
    .locals 1

    const/4 v0, 0x0

    .line 239
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromStampsGallery;->stamp:Lcom/squareup/protos/client/bizbank/Stamp;

    return-void
.end method


# virtual methods
.method public final getStamp()Lcom/squareup/protos/client/bizbank/Stamp;
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromStampsGallery;->stamp:Lcom/squareup/protos/client/bizbank/Stamp;

    return-object v0
.end method
