.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;
.super Ljava/lang/Object;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Reactor<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardReactor.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardReactor\n*L\n1#1,579:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u000234B/\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001c\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0016\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00152\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0015H\u0002J\"\u0010\u0018\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00110\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001a\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0016J2\u0010!\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00110\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00030#H\u0016J\u0010\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020&H\u0002J\u0016\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00152\u0006\u0010(\u001a\u00020)H\u0002J$\u0010*\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040+j\u0002`,2\u0006\u0010-\u001a\u00020&J$\u0010*\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040+j\u0002`,2\u0006\u0010.\u001a\u00020/J\u000c\u00100\u001a\u000201*\u000202H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "stateFactory",
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "unique",
        "Lcom/squareup/util/Unique;",
        "analytics",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;",
        "stampsRequester",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
        "(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/util/Unique;Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;)V",
        "backToDepositsInfoOrFinish",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "skippedInitialScreens",
        "",
        "fetchCustomizationSettings",
        "Lio/reactivex/Single;",
        "fetchStamps",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "finishOrderingSquareCard",
        "Lrx/Single;",
        "state",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;",
        "loadingStampsState",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;",
        "onAbandoned",
        "",
        "onReact",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "onStart",
        "input",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
        "sendSignature",
        "sendingSignature",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;",
        "startWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardWorkflow;",
        "startArg",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toCustomizationSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
        "OrderSquareCardEvent",
        "OrderSquareCardWorkflowState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;

.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final stampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

.field private final stateFactory:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

.field private final unique:Lcom/squareup/util/Unique;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/util/Unique;Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "stateFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bizbankService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unique"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsRequester"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->stateFactory:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->unique:Lcom/squareup/util/Unique;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->analytics:Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->stampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

    return-void
.end method

.method public static final synthetic access$backToDepositsInfoOrFinish(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Z)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->backToDepositsInfoOrFinish(Z)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fetchCustomizationSettings(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Z)Lio/reactivex/Single;
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->fetchCustomizationSettings(Z)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fetchStamps(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)Lio/reactivex/Single;
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->fetchStamps()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->analytics:Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getStateFactory$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->stateFactory:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    return-object p0
.end method

.method public static final synthetic access$loadingStampsState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->loadingStampsState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$sendSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;)Lio/reactivex/Single;
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->sendSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toCustomizationSettings(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->toCustomizationSettings(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object p0

    return-object p0
.end method

.method private final backToDepositsInfoOrFinish(Z)Lcom/squareup/workflow/legacy/Reaction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 464
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 466
    :cond_0
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1
.end method

.method private final fetchCustomizationSettings(Z)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            ">;"
        }
    .end annotation

    .line 472
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 473
    new-instance v1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest;

    move-result-object v1

    const-string v2, "Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getCustomizationSettings(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 474
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 475
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$fetchCustomizationSettings$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final fetchStamps()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
            ">;"
        }
    .end annotation

    .line 577
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->stampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;->stamps()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final finishOrderingSquareCard(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;>;"
        }
    .end annotation

    .line 528
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSubWorkflow()Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/workflow/rx1/Workflow;->getResult()Lrx/Single;

    move-result-object v0

    .line 529
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$finishOrderingSquareCard$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string v0, "state.subWorkflow.result\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final loadingStampsState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;
    .locals 8

    .line 451
    new-instance v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    .line 452
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSkippedInitialScreens()Z

    move-result v1

    .line 453
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v2

    .line 454
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v4

    .line 455
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v3

    .line 457
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getStampsToRestore()Ljava/util/List;

    move-result-object v6

    const/4 v5, 0x0

    move-object v0, v7

    .line 451
    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    return-object v7
.end method

.method private final onStart(Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->stampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;->fetchStamps()V

    .line 264
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardInput$SkipInitialScreens;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardInput$SkipInitialScreens;

    if-ne p1, v0, :cond_0

    .line 265
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettings;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettings;-><init>(Z)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto :goto_0

    .line 267
    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    :goto_0
    return-object p1
.end method

.method private final sendSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            ">;"
        }
    .end annotation

    .line 548
    new-instance v0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;-><init>()V

    .line 549
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->unique:Lcom/squareup/util/Unique;

    invoke-interface {v1}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    move-result-object v0

    .line 550
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSignaturePayload()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;->getByteString()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->image_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    move-result-object v0

    .line 551
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSignaturePayload()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->mime_type(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    move-result-object v0

    .line 552
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    move-result-object v0

    .line 553
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v2, "createCustomizationRequest"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->createCardCustomization(Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 554
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 555
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService.createCar\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toCustomizationSettings(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 14

    .line 495
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/IdvState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 505
    :goto_0
    sget-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->REJECTED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    :goto_1
    move-object v2, v0

    goto :goto_3

    .line 499
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_3

    sget-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->REJECTED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    goto :goto_1

    .line 500
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :cond_4
    if-eqz v1, :cond_5

    sget-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE_NEEDS_SSN:Lcom/squareup/balance/squarecard/order/ApprovalState;

    goto :goto_1

    .line 501
    :cond_5
    sget-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE:Lcom/squareup/balance/squarecard/order/ApprovalState;

    goto :goto_1

    .line 496
    :cond_6
    sget-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->APPROVED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    goto :goto_1

    .line 510
    :goto_3
    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    const-string v0, "owner_name"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 511
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v4, 0x0

    if-eqz v1, :cond_7

    sget-object v5, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    invoke-virtual {v5, v1}, Lcom/squareup/address/Address$Companion;->fromGlobalAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object v1

    move-object v5, v1

    goto :goto_4

    :cond_7
    move-object v5, v4

    .line 512
    :goto_4
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_8

    invoke-static {v1}, Lcom/squareup/util/ProtoDates;->getLocalDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    move-object v6, v1

    goto :goto_5

    :cond_8
    move-object v6, v4

    :goto_5
    const/4 v7, 0x0

    .line 508
    new-instance v13, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-object v1, v13

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;-><init>(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    .line 516
    new-instance v1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    .line 517
    iget-object v9, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 518
    iget-object v10, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    const-string v0, "business_name"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 519
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    const-string v2, "min_ink_coverage"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 520
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    const-string v0, "max_ink_coverage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v12

    move-object v8, v1

    .line 516
    invoke-direct/range {v8 .. v13}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;-><init>(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    return-object v1
.end method


# virtual methods
.method public onAbandoned(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 447
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;->getSubWorkflow()Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/squareup/workflow/rx1/Workflow;->abandon()V

    :cond_1
    return-void
.end method

.method public bridge synthetic onAbandoned(Ljava/lang/Object;)V
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->onAbandoned(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    return-void
.end method

.method public onReact(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
            ">;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->analytics:Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;->logOrderSquareCardScreen()V

    .line 277
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 293
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettings;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 303
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    if-eqz v0, :cond_2

    .line 304
    move-object p2, p1

    check-cast p2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->getSubWorkflow()Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/workflow/rx1/Workflow;->getResult()Lrx/Single;

    move-result-object p2

    .line 305
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p2, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string p2, "state.subWorkflow.result\u2026            }\n          }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :cond_2
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 334
    :cond_3
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$5;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 350
    :cond_4
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$6;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 399
    :cond_5
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 415
    :cond_6
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$8;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$8;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 430
    :cond_7
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$9;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$9;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 435
    :cond_8
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    if-eqz v0, :cond_9

    .line 436
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->analytics:Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;->logSignatureErrorScreen()V

    .line 437
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$10;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$10;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 443
    :cond_9
    instance-of p2, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    if-eqz p2, :cond_a

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->finishOrderingSquareCard(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->onReact(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->onStart(Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->stateFactory:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
