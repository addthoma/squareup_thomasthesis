.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "SquareCardOrderedWorkflowViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "squareCardConfirmationCoordinatorFactory",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;",
        "(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;)V
    .locals 18
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "squareCardConfirmationCoordinatorFactory"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x9

    new-array v1, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 15
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    sget-object v3, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered;

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 17
    sget v4, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_ordered_view:I

    .line 18
    new-instance v17, Lcom/squareup/workflow/ScreenHint;

    const-class v9, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1f7

    const/16 v16, 0x0

    move-object/from16 v5, v17

    invoke-direct/range {v5 .. v16}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 19
    sget-object v5, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$1;

    move-object v7, v5

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object/from16 v5, v17

    .line 15
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 21
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 22
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 23
    sget v6, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_ordered_deposits_info_view:I

    .line 24
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$2;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$2;

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0xc

    .line 21
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 26
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 27
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 28
    sget v6, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_code_confirmation_view:I

    .line 29
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$3;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$3;

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 26
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 31
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 32
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 33
    sget v6, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_activation_card_confirmation_view:I

    .line 34
    new-instance v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$4;

    invoke-direct {v2, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$4;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;)V

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 31
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x3

    aput-object v0, v1, v2

    .line 36
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 37
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;->getSQUARE_CARD_PROGRESS_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 38
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_progress_view:I

    .line 39
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$5;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$5;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    .line 36
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x4

    aput-object v0, v1, v2

    .line 41
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 42
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 43
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_activation_create_pin_view:I

    .line 44
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$6;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$6;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 41
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x5

    aput-object v0, v1, v2

    .line 46
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 47
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 48
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_activation_complete_view:I

    .line 49
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$7;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$7;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 46
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x6

    aput-object v0, v1, v2

    .line 51
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 52
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 53
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_confirm_address_view:I

    .line 54
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$8;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$8;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 51
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x7

    aput-object v0, v1, v2

    .line 56
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 57
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 58
    sget-object v3, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$9;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory$9;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 56
    invoke-virtual {v0, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v2, 0x8

    aput-object v0, v1, v2

    move-object/from16 v0, p0

    .line 14
    invoke-direct {v0, v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
