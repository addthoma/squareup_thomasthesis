.class public final Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;
.super Ljava/lang/Object;
.source "RealSquareCardDataRequester.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/SquareCardDataRequester;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquareCardDataRequester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquareCardDataRequester.kt\ncom/squareup/balance/squarecard/RealSquareCardDataRequester\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,132:1\n250#2,2:133\n*E\n*S KotlinDebug\n*F\n+ 1 RealSquareCardDataRequester.kt\ncom/squareup/balance/squarecard/RealSquareCardDataRequester\n*L\n116#1,2:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0016J\u0016\u0010\r\u001a\u00020\u000c2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000fH\u0002J\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0011H\u0016J\u0016\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000b2\u0006\u0010\u0016\u001a\u00020\u0007H\u0016J\u0010\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0018\u001a\u00020\u0007H\u0016J\u0010\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00020\u0007H\u0002J\u000e\u0010\u0018\u001a\u0004\u0018\u00010\u0007*\u00020\u000cH\u0002J\u000c\u0010\u001b\u001a\u00020\u001c*\u00020\u001dH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;",
        "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
        "bizBankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "cardDataRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "toggleDisposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "fetchCardStatus",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "handleCardStatusFetchSuccess",
        "cards",
        "",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "singleNewCardData",
        "currentCardData",
        "toggleCard",
        "cardData",
        "updateData",
        "data",
        "isActiveDisabledOrSuspended",
        "",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final cardDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation
.end field

.field private toggleDisposable:Lio/reactivex/disposables/SerialDisposable;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizBankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 39
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->toggleDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 40
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<CardData>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->cardDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$cardData(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;Lcom/squareup/balance/squarecard/CardStatus;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->cardData(Lcom/squareup/balance/squarecard/CardStatus;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleCardStatusFetchSuccess(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;Ljava/util/List;)Lcom/squareup/balance/squarecard/CardStatus;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->handleCardStatusFetchSuccess(Ljava/util/List;)Lcom/squareup/balance/squarecard/CardStatus;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateData(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->updateData(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-void
.end method

.method private final cardData(Lcom/squareup/balance/squarecard/CardStatus;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 95
    instance-of v0, p1, Lcom/squareup/balance/squarecard/CardStatus$Activated;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus$Activated;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/CardStatus$Activated;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/CardStatus$Ordered;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus$Ordered;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/CardStatus$Ordered;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final handleCardStatusFetchSuccess(Ljava/util/List;)Lcom/squareup/balance/squarecard/CardStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;)",
            "Lcom/squareup/balance/squarecard/CardStatus;"
        }
    .end annotation

    .line 106
    sget-object v0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;->INSTANCE:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;

    .line 116
    check-cast p1, Ljava/lang/Iterable;

    .line 133
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    .line 116
    sget-object v2, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;->INSTANCE:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;

    invoke-virtual {v2, v1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;->invoke(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 134
    :goto_0
    check-cast v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    if-eqz v0, :cond_3

    .line 123
    iget-object p1, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const-string v1, "relevantCard.card_state"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->isActiveDisabledOrSuspended(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 124
    new-instance p1, Lcom/squareup/balance/squarecard/CardStatus$Ordered;

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/CardStatus$Ordered;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    return-object p1

    .line 126
    :cond_2
    new-instance p1, Lcom/squareup/balance/squarecard/CardStatus$Activated;

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/CardStatus$Activated;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    return-object p1

    .line 117
    :cond_3
    sget-object p1, Lcom/squareup/balance/squarecard/CardStatus$Ordering;->INSTANCE:Lcom/squareup/balance/squarecard/CardStatus$Ordering;

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    return-object p1
.end method

.method private final isActiveDisabledOrSuspended(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;)Z
    .locals 1

    .line 130
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final updateData(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->cardDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public fetchCardStatus()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/CardStatus;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 79
    new-instance v1, Lcom/squareup/protos/client/bizbank/ListCardsRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/ListCardsRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ListCardsRequest;

    move-result-object v1

    const-string v2, "ListCardsRequest.Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->listCards(Lcom/squareup/protos/client/bizbank/ListCardsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$1;-><init>(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 87
    new-instance v1, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$fetchCardStatus$2;-><init>(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bizBankService\n        .\u2026t(::updateData)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->toggleDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method

.method public singleNewCardData(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation

    const-string v0, "currentCardData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->cardDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 52
    new-instance v1, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$singleNewCardData$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$singleNewCardData$1;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "cardDataRelay\n        //\u2026}\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public toggleCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 4

    const-string v0, "cardData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;-><init>()V

    .line 58
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;

    move-result-object v0

    .line 59
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    sget-object v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->enable_card(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->toggleDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 63
    iget-object v2, p0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v3, "request"

    .line 64
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->toggleCardFreeze(Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 66
    new-instance v2, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$toggleCard$1;

    invoke-direct {v2, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$toggleCard$1;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 73
    new-instance v0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$toggleCard$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;

    invoke-direct {v0, v2}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$toggleCard$2;-><init>(Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v0}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 62
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
