.class final Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2;
.super Ljava/lang/Object;
.source "CardOrderingAuthMissingFieldDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;)Landroid/app/AlertDialog;
    .locals 3

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->getTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->getMessage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 32
    sget v1, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 31
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 35
    sget v1, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 34
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 37
    sget v1, Lcom/squareup/common/strings/R$string;->okay:I

    new-instance v2, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2$1;

    invoke-direct {v2, p1}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2$1;-><init>(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2$2;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2$2;-><init>(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogFactory$create$2;->apply(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldDialogRendering;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
