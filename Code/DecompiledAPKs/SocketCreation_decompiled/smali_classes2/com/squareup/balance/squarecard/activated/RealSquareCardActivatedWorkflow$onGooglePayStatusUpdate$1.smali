.class final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$1;
.super Ljava/lang/Object;
.source "SquareCardActivatedWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->onGooglePayStatusUpdate(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_onGooglePayStatusUpdate:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$1;->$this_onGooglePayStatusUpdate:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;->getComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;->isAvailable()Z

    move-result p1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$1;->$this_onGooglePayStatusUpdate:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;->isGooglePayAvailable()Z

    move-result v0

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$1;->test(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;)Z

    move-result p1

    return p1
.end method
