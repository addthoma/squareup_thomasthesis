.class final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;
.super Ljava/lang/Object;
.source "RealOrderSquareCardScreenWorkflowStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ScreenInput"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\tR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\tR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\tR\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\t\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;",
        "",
        "reactor",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "(Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "forBusinessName",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
        "getForBusinessName",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "forProgress",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "getForProgress",
        "forSignature",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "getForSignature",
        "forSplash",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
        "getForSplash",
        "forStamps",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
        "getForStamps",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final forBusinessName:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forProgress:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forSignature:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forSplash:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forStamps:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forProgress$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forProgress$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forProgress:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 109
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSplash$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSplash$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forSplash:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 117
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forBusinessName$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forBusinessName$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forBusinessName:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 124
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forSignature:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 135
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forStamps$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forStamps$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forStamps:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public final getForBusinessName()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$Event;",
            ">;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forBusinessName:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forProgress:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForSignature()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forSignature:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForSplash()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forSplash:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForStamps()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->forStamps:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method
