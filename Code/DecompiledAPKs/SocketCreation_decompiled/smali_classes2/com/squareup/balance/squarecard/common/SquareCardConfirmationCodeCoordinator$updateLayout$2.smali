.class final Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;
.super Ljava/lang/Object;
.source "SquareCardConfirmationCodeCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->updateLayout(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;->this$0:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;->$screen:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 88
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;->$screen:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event$CodeConfirmationData;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator$updateLayout$2;->this$0:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;->access$getCodeConfirmationInput$p(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeCoordinator;)Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Event$CodeConfirmationData;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
