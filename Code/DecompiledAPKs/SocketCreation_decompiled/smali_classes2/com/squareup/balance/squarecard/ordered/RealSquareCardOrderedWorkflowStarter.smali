.class public final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;
.super Ljava/lang/Object;
.source "RealSquareCardOrderedWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;,
        Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquareCardOrderedWorkflowStarter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquareCardOrderedWorkflowStarter.kt\ncom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter\n*L\n1#1,435:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 R2\u00020\u0001:\u0002RSB\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0002`\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0002J*\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0002`\u00112\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0013H\u0002JD\u0010\u0014\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0018\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\u00190\u00170\u0016\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u0015j\u0002`\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016JD\u0010\u0014\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0018\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\u00190\u00170\u0016\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u0015j\u0002`\u001c2\u0006\u0010\u001f\u001a\u00020 H\u0016J \u0010!\u001a\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0002`\u00112\u0006\u0010\u000c\u001a\u00020\rH\u0002J(\u0010\"\u001a\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0002`\u00112\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010#\u001a\u00020$H\u0002J(\u0010%\u001a\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0002`\u00112\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010&\u001a\u00020$H\u0002J4\u0010\'\u001a\u0012\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020)0\u0008j\u0002`*2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0001\u0010+\u001a\u00020\u00132\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0013H\u0002J@\u0010,\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020\u0018\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\u00190\u0017j\u0008\u0012\u0004\u0012\u00020\u0018`-0\u00162\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020\rH\u0002J(\u00101\u001a\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0002`\u00112\u0006\u0010\u000c\u001a\u00020\r2\u0006\u00102\u001a\u000203H\u0002J$\u00104\u001a\u0012\u0012\u0004\u0012\u000205\u0012\u0004\u0012\u0002060\u0008j\u0002`7*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\rH\u0002J,\u00108\u001a\u0012\u0012\u0004\u0012\u000209\u0012\u0004\u0012\u00020:0\u0008j\u0002`;*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010.\u001a\u00020<H\u0002J$\u0010=\u001a\u0012\u0012\u0004\u0012\u00020>\u0012\u0004\u0012\u00020?0\u0008j\u0002`@*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\rH\u0002J,\u0010A\u001a\u0012\u0012\u0004\u0012\u00020B\u0012\u0004\u0012\u00020C0\u0008j\u0002`D*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010E\u001a\u00020FH\u0002J6\u0010G\u001a$\u0012\u0004\u0012\u00020\u0018\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\u00190\u0017j\u0008\u0012\u0004\u0012\u00020\u0018`-*\u00020/2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u001c\u0010H\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008*\u00020/2\u0006\u0010\u000c\u001a\u00020\rH\u0002J,\u0010I\u001a\u0012\u0012\u0004\u0012\u00020J\u0012\u0004\u0012\u00020K0\u0008j\u0002`L*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010M\u001a\u00020$H\u0002J$\u0010N\u001a\u0012\u0012\u0004\u0012\u00020O\u0012\u0004\u0012\u00020P0\u0008j\u0002`Q*\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\rH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006T"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
        "reactor",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/util/Device;)V",
        "depositsInfoScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoScreen;",
        "input",
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
        "loadingScreen",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgressScreen;",
        "message",
        "",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflow;",
        "startArg",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "startingCardActivationFailedScreen",
        "toSettingPinFailed",
        "isWeakPasscode",
        "",
        "toVerifyingSquareCardInfoFailed",
        "incorrectCardInfo",
        "toWarningDialogScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$DialogData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarningScreen;",
        "title",
        "toWorkflowState",
        "Lcom/squareup/workflow/LayeredScreen;",
        "state",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "screenInput",
        "verifyingActivationCodeFailedScreen",
        "reason",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;",
        "toActivationCompleteScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$DialogDismissed;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCompleteScreen;",
        "toCardConfirmationScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationScreen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;",
        "toCodeConfirmationScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationScreen;",
        "toConfirmAddressScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressScreen;",
        "address",
        "Lcom/squareup/address/Address;",
        "toScreenStack",
        "toSheet",
        "toSquareCardCreatePinScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinScreen;",
        "isBillingEnabled",
        "toSquareCardOrderedScreen",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreen;",
        "Companion",
        "ScreenInput",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;

.field private static final SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final reactor:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;

    .line 107
    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardProgress;

    sget-object v1, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$Companion;

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->createKey(Ljava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static final synthetic access$getSQUARE_CARD_PROGRESS_KEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static final synthetic access$toWorkflowState(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/ScreenState;
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toWorkflowState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/ScreenState;

    move-result-object p0

    return-object p0
.end method

.method private final depositsInfoScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;"
        }
    .end annotation

    .line 350
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForDepositsInfo()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoScreenKt;->SquareCardOrderedDepositsInfoScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "I)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 271
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 272
    sget-object v1, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 273
    new-instance v2, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;

    invoke-direct {v2, p2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;-><init>(I)V

    .line 274
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 271
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method private final startingCardActivationFailedScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 279
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 280
    sget-object v1, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 281
    new-instance v9, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 282
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_generic_error_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v3, v2

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 283
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_generic_error_message:I

    invoke-direct {v2, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 284
    sget v5, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v9

    .line 281
    invoke-direct/range {v2 .. v8}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 286
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 279
    invoke-direct {v0, v1, v9, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method private final toActivationCompleteScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$DialogDismissed;",
            ">;"
        }
    .end annotation

    .line 413
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$ScreenData;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    .line 414
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForActivationComplete()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 412
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCompleteScreenKt;->SquareCardActivationCompleteScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toCardConfirmationScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;"
        }
    .end annotation

    .line 335
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;

    .line 337
    invoke-virtual {p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getExpiration()Ljava/lang/String;

    move-result-object v1

    .line 338
    invoke-virtual {p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCvv()Ljava/lang/String;

    move-result-object v2

    .line 339
    invoke-virtual {p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getReaderConnected()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 340
    sget p3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_card_message_with_reader:I

    goto :goto_0

    .line 342
    :cond_0
    sget p3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_card_message_no_reader:I

    .line 335
    :goto_0
    invoke-direct {v0, p1, v1, v2, p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;I)V

    .line 345
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForCardConfirmation()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 334
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationScreenKt;->SquareCardActivationCardConfirmationScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toCodeConfirmationScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;"
        }
    .end annotation

    .line 325
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    .line 326
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForConfirmation()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 324
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationScreenKt;->SquareCardActivationCodeConfirmationScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toConfirmAddressScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Lcom/squareup/address/Address;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "Lcom/squareup/address/Address;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;"
        }
    .end annotation

    .line 423
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;

    invoke-direct {v0, p1, p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/address/Address;)V

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForAddressConfirm()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 422
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressScreenKt;->SquareCardConfirmAddressScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toScreenStack(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 196
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSquareCardOrderedScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 197
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 198
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSquareCardOrderedScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    const/4 v3, 0x0

    .line 199
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getPreviousState()Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSheet(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    .line 200
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getTitle()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getMessage()I

    move-result p1

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toWarningDialogScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;II)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 197
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 202
    :cond_1
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 203
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSquareCardOrderedScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v1

    const/4 v2, 0x0

    .line 204
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSheet(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 202
    invoke-static/range {v0 .. v5}, Lcom/squareup/container/PosLayering$Companion;->sheetStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final toSettingPinFailed(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Z)Lcom/squareup/workflow/legacy/Screen;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "Z)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 391
    new-instance v7, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 392
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_weak_error_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 393
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_weak_error_message:I

    invoke-direct {v0, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 394
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_weak_error_button_text:I

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    .line 391
    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 397
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 398
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_error_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v9, v1

    check-cast v9, Lcom/squareup/util/ViewString;

    .line 399
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_create_pin_error_message:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/ViewString;

    .line 400
    sget v11, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v12, 0x0

    const/16 v13, 0x8

    const/4 v14, 0x0

    move-object v8, v0

    .line 397
    invoke-direct/range {v8 .. v14}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 403
    :goto_0
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 404
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 406
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 403
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v1
.end method

.method private final toSheet(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 210
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSquareCardOrderedScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 211
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingDepositsInfo;

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->depositsInfoScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 212
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivation;

    if-eqz v0, :cond_2

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_sending_email:I

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 213
    :cond_2
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;

    if-eqz v0, :cond_3

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirming_code:I

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 214
    :cond_3
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivationFailed;

    if-eqz v0, :cond_4

    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->startingCardActivationFailedScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 215
    :cond_4
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;->getReason()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->verifyingActivationCodeFailedScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 216
    :cond_5
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toCodeConfirmationScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 217
    :cond_6
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    if-eqz v0, :cond_7

    .line 218
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->isIncorrectCardInfo()Z

    move-result p1

    .line 217
    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toVerifyingSquareCardInfoFailed(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 220
    :cond_7
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toCardConfirmationScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 221
    :cond_8
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    if-eqz v0, :cond_9

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirming_card:I

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 222
    :cond_9
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;->isBillingEnabled()Z

    move-result p1

    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSquareCardCreatePinScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 223
    :cond_a
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPin;

    if-eqz v0, :cond_b

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_creating_pin:I

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 224
    :cond_b
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    if-eqz v0, :cond_c

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;->isWeakPasscode()Z

    move-result p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toSettingPinFailed(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 225
    :cond_c
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingSquareCardActivationComplete;

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toActivationCompleteScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 227
    :cond_d
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartCardConfirmAddress;

    if-eqz v0, :cond_e

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_creating_pin:I

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 228
    :cond_e
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;

    if-eqz v0, :cond_f

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirming_card:I

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->loadingScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;I)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 229
    :cond_f
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toConfirmAddressScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Lcom/squareup/address/Address;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 230
    :cond_10
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    if-eqz v0, :cond_11

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getTitle()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getMessage()I

    move-result p1

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toWarningDialogScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;II)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_11
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toSquareCardCreatePinScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Z)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "Z)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
            ">;"
        }
    .end annotation

    .line 381
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;

    invoke-direct {v0, p1, p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Z)V

    .line 382
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForActivationCreatePin()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 380
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinScreenKt;->SquareCardActivationCreatePinScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toSquareCardOrderedScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;"
        }
    .end annotation

    .line 235
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v3

    .line 236
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eqz v0, :cond_2

    sget-object v1, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 255
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    .line 258
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_message_pending_2fa:I

    .line 259
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_activate_button_text_pending_2fa:I

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, v0

    move-object v2, p1

    .line 255
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)V

    goto :goto_0

    .line 247
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    .line 250
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_message_shipped:I

    .line 251
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_activate_button_text_shipped:I

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, v0

    move-object v2, p1

    .line 247
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)V

    goto :goto_0

    .line 239
    :cond_1
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    .line 242
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_message_issued:I

    .line 243
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_activate_button_text_shipped:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 239
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZIIZZ)V

    .line 264
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForSquareCardOrdered()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenKt;->SquareCardOrderedScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1

    .line 263
    :cond_2
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected card state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final toVerifyingSquareCardInfoFailed(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Z)Lcom/squareup/workflow/legacy/Screen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "Z)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 357
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 358
    sget-object v1, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 359
    new-instance v9, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    if-eqz p2, :cond_0

    .line 361
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_card_incorrect_info_error_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    goto :goto_0

    .line 363
    :cond_0
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_generic_error_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 360
    :goto_0
    move-object v3, v2

    check-cast v3, Lcom/squareup/util/ViewString;

    if-eqz p2, :cond_1

    .line 366
    new-instance p2, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_card_incorrect_info_error_message:I

    invoke-direct {p2, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    goto :goto_1

    .line 368
    :cond_1
    new-instance p2, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_generic_error_message:I

    invoke-direct {p2, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 365
    :goto_1
    move-object v4, p2

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 370
    sget v5, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v9

    .line 359
    invoke-direct/range {v2 .. v8}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 372
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 357
    invoke-direct {v0, v1, v9, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method private final toWarningDialogScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;II)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "II)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$DialogData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$Event;",
            ">;"
        }
    .end annotation

    .line 432
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$DialogData;

    invoke-direct {v0, p2, p3}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$DialogData;-><init>(II)V

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForMissingInfoDialog()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarningScreenKt;->SquareCardOrderedWarningScreen(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$DialogData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toWorkflowState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/ScreenState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 124
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->toScreenStack(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Ljava/util/Map;

    move-result-object p2

    invoke-static {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializerKt;->toSnapshot(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method private final verifyingActivationCodeFailedScreen(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;)Lcom/squareup/workflow/legacy/Screen;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
            "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 295
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_1

    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    .line 305
    new-instance p2, Lkotlin/Pair;

    .line 306
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_generic_error_title:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 307
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_generic_error_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 305
    invoke-direct {p2, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 301
    :cond_1
    new-instance p2, Lkotlin/Pair;

    .line 302
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_expired_token_error_title:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 303
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_expired_token_error_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 301
    invoke-direct {p2, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 297
    :cond_2
    new-instance p2, Lkotlin/Pair;

    .line 298
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_invalid_token_error_title:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 299
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_activation_confirm_code_invalid_token_error_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 297
    invoke-direct {p2, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 295
    :goto_0
    invoke-virtual {p2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p2}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    .line 310
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 311
    sget-object v2, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 312
    new-instance v10, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 313
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v3, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v4, v3

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 314
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v0, p2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/ViewString;

    .line 315
    sget v6, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v10

    .line 312
    invoke-direct/range {v3 .. v9}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 317
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 310
    invoke-direct {v1, v2, v10, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v1

    .line 296
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Only failures should be routed to this method."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 96
    invoke-static {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$DefaultImpls;->adapter(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v0

    return-object v0
.end method

.method public start(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->startWorkflow(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 112
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;-><init>(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 117
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$2;-><init>(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
