.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;
.super Ljava/lang/Object;
.source "RealOrderSquareCardAnalytics.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0019\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0008\u0010\u0008\u001a\u00020\u0006H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010\u000e\u001a\u00020\u0006H\u0016J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016J\u0008\u0010\u0010\u001a\u00020\u0006H\u0016J\u0008\u0010\u0011\u001a\u00020\u0006H\u0016J\u0008\u0010\u0012\u001a\u00020\u0006H\u0016J\u0008\u0010\u0013\u001a\u00020\u0006H\u0016J\u0008\u0010\u0014\u001a\u00020\u0006H\u0016J\u0008\u0010\u0015\u001a\u00020\u0006H\u0016J\u0008\u0010\u0016\u001a\u00020\u0006H\u0016J\u0008\u0010\u0017\u001a\u00020\u0006H\u0016J\u0008\u0010\u0018\u001a\u00020\u0006H\u0016J\u0008\u0010\u0019\u001a\u00020\u0006H\u0016J\u0008\u0010\u001a\u001a\u00020\u0006H\u0016J\u0008\u0010\u001b\u001a\u00020\u0006H\u0016J\u0008\u0010\u001c\u001a\u00020\u0006H\u0016J\u0008\u0010\u001d\u001a\u00020\u0006H\u0016J\u0008\u0010\u001e\u001a\u00020\u0006H\u0016J\u0008\u0010\u001f\u001a\u00020\u0006H\u0016J\u0008\u0010 \u001a\u00020\u0006H\u0016J\u0008\u0010!\u001a\u00020\u0006H\u0016J\u0008\u0010\"\u001a\u00020\u0006H\u0016J\u0008\u0010#\u001a\u00020\u0006H\u0016J\u0008\u0010$\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;",
        "squareCardAnalyticsLogger",
        "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
        "(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V",
        "logBusinessInfoErrorScreen",
        "",
        "logBusinessInfoScreen",
        "logCancelFromOrderSplash",
        "logCardCustomizationScreen",
        "logClick",
        "description",
        "",
        "logImpression",
        "logNoOwnerNameBusinessInfoErrorScreen",
        "logOrderSquareCardClick",
        "logOrderSquareCardScreen",
        "logOrderSquareCardTermsOfServiceClick",
        "logPersonalizeCardClick",
        "logShippingDetailsContinueClick",
        "logShippingDetailsCorrectedAddressAlertScreen",
        "logShippingDetailsCorrectedAddressOriginalSubmitted",
        "logShippingDetailsCorrectedAddressRecommendedSubmitted",
        "logShippingDetailsCorrectedAddressScreen",
        "logShippingDetailsMissingInfoErrorScreen",
        "logShippingDetailsScreen",
        "logShippingDetailsServerErrorScreen",
        "logShippingDetailsUnverifiedAddressErrorScreen",
        "logShippingDetailsUnverifiedAddressProceedClick",
        "logShippingDetailsUnverifiedAddressReenterClick",
        "logShippingDetailsUnverifiedAddressScreen",
        "logShippingOrderConfirmedScreen",
        "logShippingOrderConfirmedWithSignatureOnly",
        "logShippingOrderConfirmedWithStampsAndSignature",
        "logShippingOrderConfirmedWithStampsOnly",
        "logShippingOrderErrorScreen",
        "logSignatureErrorScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "squareCardAnalyticsLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logImpression(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public logBusinessInfoErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Business Info Error"

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logBusinessInfoScreen()V
    .locals 1

    const-string v0, "Create Card: Business Info"

    .line 107
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logCancelFromOrderSplash()V
    .locals 1

    const-string v0, "Square Card: Create Card Cancel"

    .line 59
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logCardCustomizationScreen()V
    .locals 1

    const-string v0, "Create Card: Card Customization"

    .line 111
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logNoOwnerNameBusinessInfoErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Business Info No Owner Name Error"

    .line 103
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logOrderSquareCardClick()V
    .locals 1

    const-string v0, "Square Card: Create My Square Card"

    .line 51
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logOrderSquareCardScreen()V
    .locals 1

    const-string v0, "Square Card: No Card Page"

    .line 95
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logOrderSquareCardTermsOfServiceClick()V
    .locals 1

    const-string v0, "Square Card: No Card Page Terms Of Service"

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logPersonalizeCardClick()V
    .locals 1

    const-string v0, "Create Card: Personalize Card"

    .line 63
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsContinueClick()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Continue"

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressAlertScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Corrected Address Alert"

    .line 135
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressOriginalSubmitted()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Continue With Old Address"

    .line 79
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressRecommendedSubmitted()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Continue With Recommended Address"

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Address Modified"

    .line 71
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsMissingInfoErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Missing Info"

    .line 127
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details"

    .line 119
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsServerErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Server Error"

    .line 123
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Unverified Address"

    .line 131
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressProceedClick()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Unrecognized Address Continue"

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressReenterClick()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Unrecognized Address Reenter"

    .line 91
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressScreen()V
    .locals 1

    const-string v0, "Create Card: Shipping Details Unrecognized Address"

    .line 83
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingOrderConfirmedScreen()V
    .locals 1

    const-string v0, "Create Card: Order Confirmed"

    .line 139
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingOrderConfirmedWithSignatureOnly()V
    .locals 1

    const-string v0, "Create Card: Order Confirmed Signature Only"

    .line 143
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingOrderConfirmedWithStampsAndSignature()V
    .locals 1

    const-string v0, "Create Card: Order Confirmed Stamps And Signature"

    .line 151
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingOrderConfirmedWithStampsOnly()V
    .locals 1

    const-string v0, "Create Card: Order Confirmed Stamps Only"

    .line 147
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logShippingOrderErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Order Confirmation Server Error"

    .line 155
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSignatureErrorScreen()V
    .locals 1

    const-string v0, "Create Card: Signature Error"

    .line 115
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method
