.class public abstract Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;
.super Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;
.source "SquareCardPrivateDataState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "InitialState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;,
        Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$DoNotShowPrivateDetails;,
        Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00032\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0006\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;",
        "()V",
        "Companion",
        "DoNotShowPrivateDetails",
        "ShowInitialPrivateDetails",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$DoNotShowPrivateDetails;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;->Companion:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;-><init>()V

    return-void
.end method
