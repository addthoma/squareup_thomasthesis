.class public final Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;
.super Ljava/lang/Object;
.source "BusinessNameFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBusinessNameFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BusinessNameFormatter.kt\ncom/squareup/balance/squarecard/order/BusinessNameFormatter\n*L\n1#1,32:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "formatBusinessName",
        "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;",
        "businessName",
        "",
        "FormatResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final formatBusinessName(Ljava/lang/String;)Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;
    .locals 2

    const-string v0, "businessName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1a

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    .line 19
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult$Truncated;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult$Truncated;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;

    return-object v0

    .line 22
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult$Unchanged;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult$Unchanged;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;

    return-object v0
.end method
