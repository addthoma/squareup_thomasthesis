.class final Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AuthSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromMissingFieldDialog;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "+",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "it",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromMissingFieldDialog;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromMissingFieldDialog;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromMissingFieldDialog;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 211
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 210
    :cond_1
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromMissingFieldDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4$1;->invoke(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromMissingFieldDialog;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
