.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateSubscriptions(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$createCardSignaturePreview(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 248
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getMinInkLevel()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v1, v1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getMaxInkLevel()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 249
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->$view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/balance/squarecard/impl/R$color;->square_card_signature_preview_signature_background:I

    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 247
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->checkInkLevel(Landroid/graphics/Bitmap;Ljava/lang/Float;Ljava/lang/Float;I)Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$createSignatureJson(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/lang/String;

    move-result-object v1

    .line 252
    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;-><init>(Landroid/graphics/Bitmap;Lcom/squareup/cardcustomizations/signature/InkLevel;Ljava/lang/String;)V

    return-object v2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 80
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;->apply(Lkotlin/Unit;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;

    move-result-object p1

    return-object p1
.end method
