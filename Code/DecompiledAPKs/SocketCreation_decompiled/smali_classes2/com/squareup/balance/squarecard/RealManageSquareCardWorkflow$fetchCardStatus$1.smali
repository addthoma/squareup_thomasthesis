.class final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;
.super Ljava/lang/Object;
.source "RealManageSquareCardWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->fetchCardStatus(Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "status",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/squarecard/CardStatus;)Lcom/squareup/balance/squarecard/ManageSquareCardState;
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    instance-of v0, p1, Lcom/squareup/balance/squarecard/CardStatus$Activated;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$getStateFactory$p(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object v0

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus$Activated;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/CardStatus$Activated;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->startCardActivated(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    goto :goto_0

    .line 272
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/CardStatus$Ordered;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$getStateFactory$p(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object v0

    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus$Ordered;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/CardStatus$Ordered;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->startCardOrdered(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    goto :goto_0

    .line 273
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/CardStatus$Ordering;->INSTANCE:Lcom/squareup/balance/squarecard/CardStatus$Ordering;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$getStateFactory$p(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->startOrderingCard(Z)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    goto :goto_0

    .line 274
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/CardStatus$Failure;->INSTANCE:Lcom/squareup/balance/squarecard/CardStatus$Failure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;->apply(Lcom/squareup/balance/squarecard/CardStatus;)Lcom/squareup/balance/squarecard/ManageSquareCardState;

    move-result-object p1

    return-object p1
.end method
