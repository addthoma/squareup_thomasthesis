.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardStampsDialogFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->populateRecycler(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardStampsDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardStampsDialogFactory.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,184:1\n1360#2:185\n1429#2,3:186\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSquareCardStampsDialogFactory.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1\n*L\n124#1:185\n124#1,3:186\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $stamps:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1;->$stamps:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1;->$stamps:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 185
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 186
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 187
    check-cast v2, Lcom/squareup/protos/client/bizbank/Stamp;

    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;

    .line 124
    invoke-direct {v3, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;-><init>(Lcom/squareup/protos/client/bizbank/Stamp;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 188
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 125
    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method
