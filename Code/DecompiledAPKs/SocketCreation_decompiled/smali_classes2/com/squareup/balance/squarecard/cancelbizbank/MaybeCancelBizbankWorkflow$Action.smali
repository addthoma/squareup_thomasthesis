.class abstract Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action;
.super Ljava/lang/Object;
.source "MaybeCancelBizbankWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbortFromOfferScreen;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivateBizbank;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankFailed;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromSuccess;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromError;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbort;,
        Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankSucceeded;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0008\u0008\t\n\u000b\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007H\u0016\u0082\u0001\u0008\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "ConfirmedCurrentBizbankStatus",
        "DeactivateBizbank",
        "DeactivatingBizbankFailed",
        "DeactivatingBizbankSucceeded",
        "FinishedFromError",
        "FinishedFromSuccess",
        "OnAbort",
        "OnAbortFromOfferScreen",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbortFromOfferScreen;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivateBizbank;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankFailed;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromSuccess;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromError;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbort;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankSucceeded;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 180
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 180
    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromSuccess;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithSuccess;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 184
    :cond_0
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;

    if-eqz v0, :cond_1

    .line 185
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logGenericDeactivatedOkay()V

    .line 186
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithoutAction;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithoutAction;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :cond_1
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbort;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$Aborted;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$Aborted;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 189
    :cond_2
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbortFromOfferScreen;

    if-eqz v0, :cond_3

    .line 190
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbortFromOfferScreen;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbortFromOfferScreen;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logGenericDeactivatedQuit()V

    .line 191
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$Aborted;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$Aborted;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 193
    :cond_3
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivateBizbank;

    if-eqz v0, :cond_4

    .line 194
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivateBizbank;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivateBizbank;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logSwitchToRegularDeposits()V

    .line 195
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$DeactivatingBizbank;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$DeactivatingBizbank;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 197
    :cond_4
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankFailed;

    if-eqz v0, :cond_5

    .line 198
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankFailed;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logSwitchToDepositsError()V

    .line 199
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingFailure;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 201
    :cond_5
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromError;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$FinishedFromError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithFailure;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 202
    :cond_6
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankSucceeded;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$DeactivatingBizbankSucceeded;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 203
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState$ShowingDeactivatingSuccess;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :cond_7
    :goto_0
    return-void
.end method
