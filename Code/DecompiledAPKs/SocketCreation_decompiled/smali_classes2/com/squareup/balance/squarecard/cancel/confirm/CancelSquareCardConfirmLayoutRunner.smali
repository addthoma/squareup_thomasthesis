.class public final Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;
.super Ljava/lang/Object;
.source "CancelSquareCardConfirmLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00122\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0002H\u0002J\u0010\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "messageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "kotlin.jvm.PlatformType",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateGlyphMessage",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final messageView:Lcom/squareup/noho/NohoMessageView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->view:Landroid/view/View;

    .line 34
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 35
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->cancel_square_card_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V
    .locals 4

    .line 57
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 54
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 55
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;->getActionBarTitle()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 56
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateGlyphMessage(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;->getSorryMessage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    .line 50
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$updateGlyphMessage$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$updateGlyphMessage$1;-><init>(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounceRunnable { rendering.onCancelCard() }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->updateActionBar(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->updateGlyphMessage(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
