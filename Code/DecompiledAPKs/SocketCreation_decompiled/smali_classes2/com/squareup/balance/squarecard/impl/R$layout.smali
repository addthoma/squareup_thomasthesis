.class public final Lcom/squareup/balance/squarecard/impl/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final auth_square_card_personal_info_view:I = 0x7f0d006d

.field public static final auth_square_card_ssn_info_view:I = 0x7f0d006e

.field public static final cancel_square_card_reasons_view:I = 0x7f0d00ac

.field public static final cancel_square_card_view:I = 0x7f0d00ad

.field public static final canceled_card_feedback_view:I = 0x7f0d00ae

.field public static final notification_preferences_contents_layout:I = 0x7f0d03a1

.field public static final notification_preferences_error_layout:I = 0x7f0d03a2

.field public static final notification_preferences_layout:I = 0x7f0d03a3

.field public static final notification_preferences_loading_layout:I = 0x7f0d03a4

.field public static final order_square_card_business_name_view:I = 0x7f0d03eb

.field public static final order_square_card_signature_button_bar:I = 0x7f0d03ec

.field public static final order_square_card_signature_view:I = 0x7f0d03ed

.field public static final order_square_card_splash_view:I = 0x7f0d03ee

.field public static final order_square_card_stamps_gallery:I = 0x7f0d03ef

.field public static final square_card_activated_google_pay_view:I = 0x7f0d04ed

.field public static final square_card_activated_view:I = 0x7f0d04ee

.field public static final square_card_activation_card_confirmation_view:I = 0x7f0d04ef

.field public static final square_card_activation_complete_view:I = 0x7f0d04f0

.field public static final square_card_activation_create_pin_view:I = 0x7f0d04f1

.field public static final square_card_code_confirmation_view:I = 0x7f0d04f2

.field public static final square_card_confirm_address_view:I = 0x7f0d04f3

.field public static final square_card_enter_phone_number_view:I = 0x7f0d04f4

.field public static final square_card_order_stamp_view:I = 0x7f0d04f8

.field public static final square_card_ordered_deposits_info_view:I = 0x7f0d04f9

.field public static final square_card_ordered_view:I = 0x7f0d04fa

.field public static final square_card_progress_view:I = 0x7f0d04ff

.field public static final square_card_view_billing_address_layout:I = 0x7f0d0500


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
