.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;
.super Ljava/lang/Object;
.source "OrderSquareCardServiceHelper.kt"

# interfaces
.implements Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J6\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u00082\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J\n\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u001c\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\t0\u00082\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\n\u0010\u0018\u001a\u0004\u0018\u00010\u0013H\u0016J\u0008\u0010\u0019\u001a\u00020\u0013H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;",
        "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "generalWarning",
        "Lcom/squareup/widgets/warning/WarningIds;",
        "placeOrder",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        "itemToken",
        "",
        "verifiedAddressToken",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "globalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "placeOrderServerError",
        "Lcom/squareup/widgets/warning/Warning;",
        "verifyShippingAddress",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        "body",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        "verifyShippingAddressServerError",
        "verifyShippingAddressServiceError",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final generalWarning:Lcom/squareup/widgets/warning/WarningIds;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 37
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_generic_error_title:I

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_generic_error_message:I

    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->generalWarning:Lcom/squareup/widgets/warning/WarningIds;

    return-void
.end method


# virtual methods
.method public placeOrder(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/mailorder/ContactInfo;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifiedAddressToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance p3, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;-><init>()V

    .line 103
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->card_issue_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;

    move-result-object p2

    .line 104
    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;

    move-result-object p2

    .line 105
    invoke-static {p1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->customization_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;

    move-result-object p1

    .line 107
    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->confirmIssueCard(Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 109
    sget-object p2, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService.confirmIs\u2026  )\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public placeOrderServerError()Lcom/squareup/widgets/warning/Warning;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->generalWarning:Lcom/squareup/widgets/warning/WarningIds;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    return-object v0
.end method

.method public verifyShippingAddress(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 49
    new-instance v1, Lcom/squareup/protos/client/bizbank/StartIssueCardRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardRequest$Builder;-><init>()V

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bizbank/StartIssueCardRequest$Builder;->shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/StartIssueCardRequest$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/StartIssueCardRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/StartIssueCardRequest;

    move-result-object p1

    const-string v1, "StartIssueCardRequest.Bu\u2026shipping_address).build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->startIssueCard(Lcom/squareup/protos/client/bizbank/StartIssueCardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 52
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService.startIssu\u2026d()\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public verifyShippingAddressServerError()Lcom/squareup/widgets/warning/Warning;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->generalWarning:Lcom/squareup/widgets/warning/WarningIds;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    return-object v0
.end method

.method public verifyShippingAddressServiceError()Lcom/squareup/widgets/warning/Warning;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;->generalWarning:Lcom/squareup/widgets/warning/WarningIds;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    return-object v0
.end method
