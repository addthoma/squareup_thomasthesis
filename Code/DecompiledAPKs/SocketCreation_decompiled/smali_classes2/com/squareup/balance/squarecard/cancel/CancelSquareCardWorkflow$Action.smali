.class abstract Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action;
.super Ljava/lang/Object;
.source "CancelSquareCardWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$Exit;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithSuccess;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithFailure;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$BackToSelectReasons;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitAndReorder;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;,
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledFailed;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\n\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007H\u0016\u0082\u0001\n\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "BackToSelectReasons",
        "CardCanceledFailed",
        "CardCanceledSuccessfully",
        "Exit",
        "ExitAndReorder",
        "ExitWithFailure",
        "ExitWithSuccess",
        "ReadyForFeedback",
        "ReadyToDeactivateCard",
        "ReasonSelected",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$Exit;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithSuccess;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithFailure;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$BackToSelectReasons;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitAndReorder;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledFailed;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 198
    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$Exit;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Back;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Back;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 202
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithSuccess;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccess;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 203
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithFailure;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Error;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Error;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 204
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$BackToSelectReasons;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$BackToSelectReasons;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingCancelReasons;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingCancelReasons;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 207
    :cond_3
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitAndReorder;

    if-eqz v0, :cond_4

    .line 208
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitAndReorder;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitAndReorder;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitAndReorder;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/balance/squarecard/cancel/AnalyticsLoggingHelperKt;->logReplaceCard(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    .line 209
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccessWithReorder;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccessWithReorder;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 211
    :cond_4
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;

    if-eqz v0, :cond_5

    .line 212
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFeedback;

    move-object v1, p0

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;->getFeedbackSource()Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFeedback;-><init>(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 214
    :cond_5
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;

    if-eqz v0, :cond_6

    .line 215
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/cancel/AnalyticsLoggingHelperKt;->logCancelReasonChosen(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    .line 216
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ConfirmingCancelCard;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 218
    :cond_6
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;

    if-eqz v0, :cond_7

    .line 219
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/cancel/AnalyticsLoggingHelperKt;->logCanceledCard(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    .line 221
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    sget-object v2, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 222
    new-instance v2, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$CancelingCard;

    .line 223
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyToDeactivateCard;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v0

    .line 222
    invoke-direct {v2, v0, v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$CancelingCard;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_1

    .line 227
    :cond_7
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;

    if-eqz v0, :cond_9

    .line 228
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/cancel/AnalyticsLoggingHelperKt;->logCanceledCardSuccess(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    .line 230
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v1

    .line 231
    sget-object v2, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    .line 232
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v2

    .line 233
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;->isBizBankEnabled()Z

    move-result v0

    .line 231
    invoke-direct {v1, v2, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)V

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    goto :goto_0

    .line 235
    :cond_8
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccess;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledSuccessfully;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccess;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)V

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;

    .line 230
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_1

    .line 238
    :cond_9
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledFailed;

    if-eqz v0, :cond_a

    .line 239
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledFailed;->getAnalytics()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;->logCancelCardErrorScreen()V

    .line 240
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFailure;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$CardCanceledFailed;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingFailure;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :cond_a
    :goto_1
    return-void
.end method
