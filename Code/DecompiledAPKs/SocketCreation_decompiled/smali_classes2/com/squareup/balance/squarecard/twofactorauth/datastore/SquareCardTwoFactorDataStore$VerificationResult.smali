.class public final enum Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;
.super Ljava/lang/Enum;
.source "SquareCardTwoFactorDataStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VerificationResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;",
        "",
        "(Ljava/lang/String;I)V",
        "SUCCESS",
        "INCORRECT_TOKEN",
        "TOKEN_EXPIRED",
        "UNKNOWN_FAILURE",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

.field public static final enum INCORRECT_TOKEN:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

.field public static final enum SUCCESS:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

.field public static final enum TOKEN_EXPIRED:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

.field public static final enum UNKNOWN_FAILURE:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    new-instance v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    const/4 v2, 0x0

    const-string v3, "SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->SUCCESS:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    const/4 v2, 0x1

    const-string v3, "INCORRECT_TOKEN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->INCORRECT_TOKEN:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    const/4 v2, 0x2

    const-string v3, "TOKEN_EXPIRED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->TOKEN_EXPIRED:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    const/4 v2, 0x3

    const-string v3, "UNKNOWN_FAILURE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->UNKNOWN_FAILURE:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->$VALUES:[Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;
    .locals 1

    const-class v0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;
    .locals 1

    sget-object v0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->$VALUES:[Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    invoke-virtual {v0}, [Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    return-object v0
.end method
