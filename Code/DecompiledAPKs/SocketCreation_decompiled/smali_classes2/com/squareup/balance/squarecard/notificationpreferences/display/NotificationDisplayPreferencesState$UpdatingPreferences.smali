.class public final Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;
.super Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;
.source "NotificationDisplayPreferencesState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdatingPreferences"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\u000c\u001a\u00020\rH\u00d6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\rH\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0019\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\rH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;",
        "preferences",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "desiredPreferences",
        "(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)V",
        "getDesiredPreferences",
        "()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "getPreferences",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

.field private final preferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)V
    .locals 1

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "desiredPreferences"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->preferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->copy(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    return-object v0
.end method

.method public final copy(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;
    .locals 1

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "desiredPreferences"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDesiredPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    return-object v0
.end method

.method public getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->preferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdatingPreferences(preferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", desiredPreferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->preferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->desiredPreferences:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
