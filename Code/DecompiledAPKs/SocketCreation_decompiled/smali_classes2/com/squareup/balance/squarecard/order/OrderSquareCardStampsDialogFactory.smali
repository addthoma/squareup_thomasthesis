.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;
.super Ljava/lang/Object;
.source "OrderSquareCardStampsDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardStampsDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardStampsDialogFactory.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n*L\n1#1,184:1\n49#2:185\n50#2,3:191\n53#2:206\n599#3,4:186\n601#3:190\n310#4,3:194\n313#4,3:203\n35#5,6:197\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSquareCardStampsDialogFactory.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory\n*L\n143#1:185\n143#1,3:191\n143#1:206\n143#1,4:186\n143#1:190\n143#1,3:194\n143#1,3:203\n143#1,6:197\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0003*+,B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0018\u0010\u0014\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u000c0\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J(\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\u00172\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u001e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u000eH\u0002J\u0016\u0010 \u001a\u00020\u001e2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\"H\u0002J\u0010\u0010#\u001a\u00020\u001e2\u0006\u0010$\u001a\u00020%H\u0002J\u0016\u0010&\u001a\u00020\u001e2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00060(H\u0002J \u0010)\u001a\u00020\u001e2\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogScreen;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V",
        "bottomSheet",
        "Lcom/google/android/material/bottomsheet/BottomSheetDialog;",
        "chosenStamp",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "loadingProgress",
        "Landroid/widget/ProgressBar;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;",
        "create",
        "Lio/reactivex/Single;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "screen",
        "initRecycler",
        "contentLayout",
        "Landroid/view/View;",
        "onClick",
        "",
        "stamp",
        "populateRecycler",
        "stamps",
        "",
        "showOrHideLoading",
        "showLoading",
        "",
        "updateDismissListener",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "updateUi",
        "Factory",
        "StampItem",
        "StampsBottomSheetDialogCallback",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

.field private chosenStamp:Lcom/squareup/protos/client/bizbank/Stamp;

.field private loadingProgress:Landroid/widget/ProgressBar;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;>;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen;)Lcom/google/android/material/bottomsheet/BottomSheetDialog;
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen;)Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChosenStamp$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;)Lcom/squareup/protos/client/bizbank/Stamp;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->chosenStamp:Lcom/squareup/protos/client/bizbank/Stamp;

    return-object p0
.end method

.method public static final synthetic access$onClick(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/squareup/protos/client/bizbank/Stamp;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->onClick(Lcom/squareup/protos/client/bizbank/Stamp;)V

    return-void
.end method

.method public static final synthetic access$setChosenStamp$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/squareup/protos/client/bizbank/Stamp;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->chosenStamp:Lcom/squareup/protos/client/bizbank/Stamp;

    return-void
.end method

.method public static final synthetic access$updateUi(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->updateUi(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen;)Lcom/google/android/material/bottomsheet/BottomSheetDialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;)",
            "Lcom/google/android/material/bottomsheet/BottomSheetDialog;"
        }
    .end annotation

    .line 97
    sget v0, Lcom/squareup/balance/squarecard/impl/R$layout;->order_square_card_stamps_gallery:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string v1, "contentView"

    .line 98
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->initRecycler(Landroid/content/Context;Landroid/view/View;)Lcom/squareup/cycler/Recycler;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->recycler:Lcom/squareup/cycler/Recycler;

    .line 99
    sget v1, Lcom/squareup/balance/squarecard/impl/R$id;->stamps_loading_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "contentView.findViewById(R.id.stamps_loading_view)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->loadingProgress:Landroid/widget/ProgressBar;

    .line 101
    new-instance v1, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-direct {v1, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 102
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    const-string v2, "bottomSheet"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/google/android/material/R$id;->design_bottom_sheet:I

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "bottomSheet.findViewById\u2026id.design_bottom_sheet)!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    check-cast v0, Landroid/view/ViewGroup;

    .line 106
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/balance/squarecard/impl/R$dimen;->square_card_stamps_peek_height:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setPeekHeight(I)V

    const/4 p1, 0x4

    .line 110
    invoke-virtual {v0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    .line 111
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p1, p0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampsBottomSheetDialogCallback;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V

    check-cast p1, Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;

    invoke-virtual {v0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->addBottomSheetCallback(Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;)V

    .line 115
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->updateUi(Lcom/squareup/workflow/legacy/Screen;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    if-nez p1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    return-object p1
.end method

.method private final initRecycler(Landroid/content/Context;Landroid/view/View;)Lcom/squareup/cycler/Recycler;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$StampItem;",
            ">;"
        }
    .end annotation

    .line 133
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stamps_recycler_view:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .line 134
    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    .line 135
    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setClipToPadding(Z)V

    .line 137
    new-instance v0, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/balance/squarecard/impl/R$integer;->order_square_card_stamp_gallery_grid_count:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 137
    invoke-direct {v0, p1, v1}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    const-string v0, "recyclerView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 186
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 191
    invoke-virtual {p1}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 192
    invoke-virtual {p1}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 195
    new-instance p1, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p1, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 145
    sget v1, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_order_stamp_view:I

    .line 197
    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$initRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v2, v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$initRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 195
    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 194
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 189
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 186
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final onClick(Lcom/squareup/protos/client/bizbank/Stamp;)V
    .locals 1

    .line 158
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->chosenStamp:Lcom/squareup/protos/client/bizbank/Stamp;

    .line 159
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    if-nez p1, :cond_0

    const-string v0, "bottomSheet"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->dismiss()V

    return-void
.end method

.method private final populateRecycler(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            ">;)V"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "recycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$populateRecycler$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final showOrHideLoading(Z)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->loadingProgress:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const-string v1, "loadingProgress"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final updateDismissListener(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;)V"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    if-nez v0, :cond_0

    const-string v1, "bottomSheet"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method private final updateUi(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;",
            ">;)V"
        }
    .end annotation

    .line 68
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;

    .line 70
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->updateDismissListener(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 71
    instance-of p1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Loading;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->showOrHideLoading(Z)V

    .line 73
    instance-of p1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Display;

    if-eqz p1, :cond_0

    .line 74
    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Display;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Display;->getStamps()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->populateRecycler(Ljava/util/List;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/google/android/material/bottomsheet/BottomSheetDialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 59
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v1

    const-string v2, "MortarScope.getScope(context)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$create$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;

    invoke-direct {v2, v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$create$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 62
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->screens:Lio/reactivex/Observable;

    .line 63
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$create$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$create$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .firstOr\u2026Dialog(context, screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
