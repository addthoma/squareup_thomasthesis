.class public final Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;
.super Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OrderingCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001BM\u0012>\u0010\u0003\u001a:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0004j\u0002`\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u0013\u001a\u00020\u0014H\u0016R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010RL\u0010\u0003\u001a:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0004j\u0002`\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "subWorkflow",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardAdaptedWorkflow;",
        "skipInitialScreens",
        "",
        "(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Z)V",
        "getSkipInitialScreens",
        "()Z",
        "getSubWorkflow",
        "()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final skipInitialScreens:Z

.field private final subWorkflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;Z)V"
        }
    .end annotation

    const-string v0, "subWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 96
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;->subWorkflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    iput-boolean p2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;->skipInitialScreens:Z

    return-void
.end method


# virtual methods
.method public final getSkipInitialScreens()Z
    .locals 1

    .line 95
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;->skipInitialScreens:Z

    return v0
.end method

.method public getSubWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;->subWorkflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    return-object v0
.end method

.method public toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 97
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard$toSnapshot$1;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
