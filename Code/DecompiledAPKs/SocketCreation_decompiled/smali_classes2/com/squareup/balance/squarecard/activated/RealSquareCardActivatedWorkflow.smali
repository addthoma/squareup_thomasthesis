.class public final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SquareCardActivatedWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;,
        Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;,
        Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivatedWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivatedWorkflow.kt\ncom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,509:1\n85#2:510\n85#2:528\n41#2:531\n56#2,2:532\n240#3:511\n240#3:529\n276#4:512\n276#4:530\n276#4:534\n149#5,5:513\n149#5,5:518\n149#5,5:523\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardActivatedWorkflow.kt\ncom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow\n*L\n198#1:510\n376#1:528\n444#1:531\n444#1,2:532\n198#1:511\n376#1:529\n198#1:512\n376#1:530\n444#1:534\n288#1,5:513\n300#1,5:518\n345#1,5:523\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0089\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001(\u0018\u00002\u00020\u000128\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\"\u0012 \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n0\u0002:\u0003\\]^Bw\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018\u0012\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0018\u0012\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u0018\u0012\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u0018\u0012\u0006\u0010 \u001a\u00020!\u00a2\u0006\u0002\u0010\"J\\\u0010,\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010-\u001a\u00020\u00032\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0002\u00100\u001a\u0002012\u0006\u00102\u001a\u0002032\u0012\u00104\u001a\u000e\u0012\u0004\u0012\u000206\u0012\u0004\u0012\u00020705H\u0002J\u0016\u00108\u001a\u0008\u0012\u0004\u0012\u00020:092\u0006\u0010.\u001a\u00020/H\u0002J\u0010\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020:H\u0002JL\u0010>\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0002\u0010?\u001a\u00020@2\u0012\u00104\u001a\u000e\u0012\u0004\u0012\u00020A\u0012\u0004\u0012\u00020705H\u0002J\u001c\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050C2\u0006\u0010D\u001a\u00020\u0004H\u0002J\u001a\u0010E\u001a\u00020\u00042\u0006\u0010F\u001a\u00020\u00032\u0008\u0010G\u001a\u0004\u0018\u00010HH\u0016J\u0008\u0010I\u001a\u000201H\u0002JJ\u0010J\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010F\u001a\u00020\u00032\u0006\u0010D\u001a\u00020\u00042\u0012\u0010K\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050LH\u0016J\u0008\u0010M\u001a\u00020NH\u0002J\u0010\u0010O\u001a\u00020H2\u0006\u0010D\u001a\u00020\u0004H\u0016J:\u0010P\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0012\u00104\u001a\u000e\u0012\u0004\u0012\u00020Q\u0012\u0004\u0012\u00020705H\u0002J\u000c\u0010R\u001a\u000207*\u00020:H\u0002J\u000c\u0010S\u001a\u00020\u0004*\u00020\u0004H\u0002J\u0012\u0010T\u001a\u0008\u0012\u0004\u0012\u00020109*\u00020NH\u0002J\u0014\u0010U\u001a\u00020\u0004*\u00020V2\u0006\u0010W\u001a\u00020\u0004H\u0002J\u000c\u0010X\u001a\u00020@*\u00020YH\u0002J\u000c\u0010Z\u001a\u00020[*\u000203H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010#\u001a\u0010\u0012\u000c\u0012\n &*\u0004\u0018\u00010%0%0$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010)R\u0014\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006_"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowScreen;",
        "secureScopeManager",
        "Lcom/squareup/secure/SecureScopeManager;",
        "analytics",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
        "device",
        "Lcom/squareup/util/Device;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "googlePayHelper",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
        "squareCardDataRequester",
        "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
        "squareCardResetPinWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
        "viewBillingAddressWorkflow",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
        "notificationPreferencesWorkflow",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
        "enablePrivateDataWorkflow",
        "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
        "privateDetailsDataStore",
        "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
        "(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)V",
        "googlePayCapabilities",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;",
        "kotlin.jvm.PlatformType",
        "googlePayWorker",
        "com/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;",
        "privateDataWorker",
        "Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;",
        "activatedCardDetailsScreen",
        "input",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "isToggleRequestInProgress",
        "",
        "privateCardDetailsState",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
        "",
        "addCardToGooglePay",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
        "googlePayResponseState",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;",
        "googlePayResult",
        "googlePayScreen",
        "status",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Event;",
        "handleCardDetailsToggle",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "isGooglePayAvailable",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showingCardDetails",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;",
        "snapshotState",
        "suspendedDialogScreen",
        "Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen$DialogDismissed;",
        "logResult",
        "maybeResetPrivateCardDataState",
        "onGooglePayStatusUpdate",
        "toNextState",
        "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;",
        "previousState",
        "toStatus",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;",
        "toToggleState",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;",
        "GooglePayStatus",
        "LogScreen",
        "OnProgressToggleUpdate",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

.field private final device:Lcom/squareup/util/Device;

.field private final enablePrivateDataWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final googlePayCapabilities:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final googlePayHelper:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

.field private final googlePayWorker:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;

.field private final notificationPreferencesWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final privateDataWorker:Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;

.field private final privateDetailsDataStore:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;

.field private final squareCardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

.field private final squareCardResetPinWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final viewBillingAddressWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/secure/SecureScopeManager;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "secureScopeManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "googlePayHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardDataRequester"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardResetPinWorkflow"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewBillingAddressWorkflow"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationPreferencesWorkflow"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enablePrivateDataWorkflow"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "privateDetailsDataStore"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->analytics:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayHelper:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->squareCardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iput-object p7, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->squareCardResetPinWorkflow:Ljavax/inject/Provider;

    iput-object p8, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->viewBillingAddressWorkflow:Ljavax/inject/Provider;

    iput-object p9, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->notificationPreferencesWorkflow:Ljavax/inject/Provider;

    iput-object p10, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->enablePrivateDataWorkflow:Ljavax/inject/Provider;

    iput-object p11, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->privateDetailsDataStore:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;

    .line 124
    new-instance p2, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;

    const/4 p3, 0x0

    const/4 p4, 0x3

    const/4 p5, 0x0

    invoke-direct {p2, p3, p3, p4, p5}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;-><init>(ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 123
    invoke-static {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    const-string p3, "BehaviorRelay.createDefa\u2026    GooglePayStatus()\n  )"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayCapabilities:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 127
    new-instance p2, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayWorker:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;

    .line 149
    new-instance p2, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;

    invoke-direct {p2, p1}, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;-><init>(Lcom/squareup/secure/SecureScopeManager;)V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->privateDataWorker:Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;
    .locals 0

    .line 107
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->analytics:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 107
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getGooglePayCapabilities$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 107
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayCapabilities:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getGooglePayHelper$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;
    .locals 0

    .line 107
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayHelper:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    return-object p0
.end method

.method public static final synthetic access$getSquareCardDataRequester$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/SquareCardDataRequester;
    .locals 0

    .line 107
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->squareCardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    return-object p0
.end method

.method public static final synthetic access$googlePayResponseState(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;
    .locals 0

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayResponseState(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleCardDetailsToggle(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->handleCardDetailsToggle(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$logResult(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)V
    .locals 0

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->logResult(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)V

    return-void
.end method

.method public static final synthetic access$maybeResetPrivateCardDataState(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 0

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->maybeResetPrivateCardDataState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showingCardDetails(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->showingCardDetails()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toNextState(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 0

    .line 107
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->toNextState(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p0

    return-object p0
.end method

.method private final activatedCardDetailsScreen(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZLcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Z",
            "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p2

    .line 311
    iget-object v1, v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v4, 0x0

    if-eqz p3, :cond_0

    .line 315
    sget-object v5, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v1, v5, :cond_1

    goto :goto_0

    .line 317
    :cond_0
    sget-object v5, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eq v1, v5, :cond_1

    :goto_0
    const/4 v11, 0x1

    goto :goto_1

    :cond_1
    const/4 v11, 0x0

    :goto_1
    if-nez p3, :cond_2

    .line 320
    iget-object v5, v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    sget-object v6, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v5, v6, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    .line 322
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;->isToggleCardFeatureEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v5, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eq v1, v5, :cond_3

    sget-object v5, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v1, v5, :cond_4

    :cond_3
    const/4 v5, 0x1

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    .line 324
    :goto_3
    sget-object v7, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v1, v7, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->isGooglePayAvailable()Z

    move-result v7

    if-eqz v7, :cond_5

    if-nez p3, :cond_5

    const/4 v8, 0x1

    goto :goto_4

    :cond_5
    const/4 v8, 0x0

    :goto_4
    if-eqz v6, :cond_6

    .line 326
    sget-object v7, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;

    check-cast v7, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    goto :goto_5

    :cond_6
    move-object/from16 v7, p4

    invoke-direct {v0, v7}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->toToggleState(Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    move-result-object v7

    :goto_5
    move-object v13, v7

    .line 329
    new-instance v14, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;

    .line 330
    new-instance v15, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    .line 332
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;->isResetCardPinEnabled()Z

    move-result v9

    .line 336
    sget-object v7, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eq v1, v7, :cond_8

    sget-object v7, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v1, v7, :cond_7

    goto :goto_6

    :cond_7
    const/4 v7, 0x0

    goto :goto_7

    :cond_8
    :goto_6
    const/4 v7, 0x1

    .line 338
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;->isNotificationPreferencesEnabled()Z

    move-result v10

    .line 339
    iget-object v12, v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v12}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v12

    .line 340
    sget-object v3, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v1, v3, :cond_9

    const/16 v16, 0x1

    goto :goto_8

    :cond_9
    const/16 v16, 0x0

    :goto_8
    move-object v1, v15

    move-object/from16 v2, p2

    move v3, v12

    move v4, v7

    move/from16 v7, v16

    move/from16 v12, p3

    .line 330
    invoke-direct/range {v1 .. v13}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;)V

    move-object/from16 v1, p5

    .line 329
    invoke-direct {v14, v15, v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v14, Lcom/squareup/workflow/legacy/V2Screen;

    .line 524
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 525
    const-class v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 526
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 524
    invoke-direct {v1, v2, v14, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 346
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method static synthetic activatedCardDetailsScreen$default(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZLcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/util/Map;
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, p3

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 307
    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->activatedCardDetailsScreen(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZLcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
            ">;"
        }
    .end annotation

    .line 374
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayHelper:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    .line 375
    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;

    move-result-object p1

    .line 528
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$addCardToGooglePay$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$addCardToGooglePay$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 529
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 530
    const-class v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final googlePayResponseState(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;
    .locals 3

    .line 387
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$Success;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$Success;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 388
    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->add_to_google_pay_success_title:I

    .line 389
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->add_to_google_pay_success_message:I

    const/4 v1, 0x1

    goto :goto_0

    .line 392
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$CancelledFlow;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$CancelledFlow;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->add_to_google_pay_cancelled_title:I

    .line 394
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->add_to_google_pay_cancelled_message:I

    goto :goto_0

    .line 397
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$UnexpectedError;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$UnexpectedError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 398
    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->add_to_google_pay_error_title:I

    .line 399
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->add_to_google_pay_error_message:I

    .line 404
    :goto_0
    new-instance v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    invoke-direct {v2, v1, p1, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;-><init>(ZII)V

    check-cast v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    return-object v2

    .line 400
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final googlePayScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 297
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;

    .line 298
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$ScreenData;

    invoke-direct {v1, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;)V

    .line 297
    invoke-direct {v0, v1, p3}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 519
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 520
    const-class p2, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 521
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 519
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 301
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method static synthetic googlePayScreen$default(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/util/Map;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 294
    sget-object p2, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$Loading;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$Loading;

    check-cast p2, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final handleCardDetailsToggle(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
            ">;"
        }
    .end annotation

    .line 352
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getPrivateDataState()Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    move-result-object v0

    .line 353
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$ErrorFetchingPrivateData;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$ErrorFetchingPrivateData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    .line 354
    :cond_0
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;

    move-object v7, v1

    check-cast v7, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v4 .. v9}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    invoke-static {v0, p1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 355
    :cond_1
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$RequiresTwoFactorAuth;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$RequiresTwoFactorAuth;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;

    check-cast v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {p1, v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    invoke-static {v0, p1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 356
    :cond_2
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Disabled;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 357
    new-instance v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Disabled;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Disabled;->getDetails()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;-><init>(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;)V

    move-object v7, v4

    check-cast v7, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v4 .. v9}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    .line 356
    invoke-static {v1, p1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 359
    :cond_3
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 360
    new-instance v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Disabled;

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;->getDetails()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Disabled;-><init>(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;)V

    move-object v7, v4

    check-cast v7, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v4 .. v9}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    .line 359
    invoke-static {v1, p1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1

    .line 363
    :cond_4
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$DoNotShowPrivateDetails;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$DoNotShowPrivateDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 364
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 365
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterTwoFactor;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterTwoFactor;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_6
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getPrivateDataState()Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " toggle state is not allowed"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final isGooglePayAvailable()Z
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayCapabilities:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;->isAvailable()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final logResult(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)V
    .locals 1

    .line 431
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$Success;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$Success;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->analytics:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;->logSquareCardAddToGooglePaySuccessScreen()V

    goto :goto_0

    .line 432
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$CancelledFlow;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$CancelledFlow;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->analytics:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;->logSquareCardAddToGooglePayCancelClick()V

    goto :goto_0

    .line 433
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$UnexpectedError;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult$UnexpectedError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->analytics:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;->logSquareCardAddToGooglePayErrorScreen()V

    :cond_2
    :goto_0
    return-void
.end method

.method private final maybeResetPrivateCardDataState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 7

    .line 476
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getPrivateDataState()Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;

    if-nez v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 477
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;

    move-object v4, v0

    check-cast v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private final onGooglePayStatusUpdate(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 441
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayCapabilities:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 442
    new-instance v1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 443
    sget-object v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$2;->INSTANCE:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$onGooglePayStatusUpdate$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "googlePayCapabilities\n  \u2026  .map { it.isAvailable }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 531
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 533
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 534
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1

    .line 533
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final showingCardDetails()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;
    .locals 7

    .line 281
    new-instance v6, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->isGooglePayAvailable()Z

    move-result v3

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;-><init>(ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v6
.end method

.method private final suspendedDialogScreen(Lkotlin/jvm/functions/Function1;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen$DialogDismissed;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 286
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 514
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 515
    const-class v1, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 516
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 514
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 289
    sget-object v0, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final toNextState(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 7

    .line 465
    sget-object v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$TwoFactorAuthRequired;->INSTANCE:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$TwoFactorAuthRequired;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 466
    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$RequiresTwoFactorAuth;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$RequiresTwoFactorAuth;

    move-object v4, p1

    check-cast v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    sget-object p2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;

    check-cast p2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    goto :goto_0

    .line 468
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$CouldNotRetrieveData;->INSTANCE:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$CouldNotRetrieveData;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$ErrorFetchingPrivateData;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$ErrorFetchingPrivateData;

    move-object v4, p1

    check-cast v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    goto :goto_0

    .line 470
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$DataRetrieved;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    check-cast p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$DataRetrieved;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$DataRetrieved;->getDetails()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;-><init>(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toStatus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;)Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;
    .locals 2

    .line 412
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$CardAddedSuccessfully;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->getTitle()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->getMessage()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$CardAddedSuccessfully;-><init>(II)V

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;

    goto :goto_0

    .line 415
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$UnknownError;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->getTitle()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->getMessage()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$UnknownError;-><init>(II)V

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;

    :goto_0
    return-object v0
.end method

.method private final toToggleState(Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;
    .locals 1

    .line 449
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$DoNotShowPrivateDetails;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$DoNotShowPrivateDetails;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Hidden;

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    goto :goto_2

    .line 450
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loaded;

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;->getDetails()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loaded;-><init>(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    goto :goto_2

    .line 451
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterTwoFactor;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterTwoFactor;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 452
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_0
    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loading;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Enabled$Loading;

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    goto :goto_2

    .line 453
    :cond_3
    instance-of v0, p1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Disabled;

    if-eqz v0, :cond_4

    goto :goto_1

    .line 454
    :cond_4
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$RequiresTwoFactorAuth;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$RequiresTwoFactorAuth;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_1

    .line 455
    :cond_5
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$ShowInitialPrivateDetails;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_1

    .line 456
    :cond_6
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$ErrorFetchingPrivateData;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$ErrorFetchingPrivateData;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    :goto_1
    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Disabled;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState$Visible$Disabled;

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    :goto_2
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 6

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 156
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;

    invoke-virtual {v0, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 157
    :cond_0
    new-instance p2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;-><init>(ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    .line 158
    :goto_0
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    .line 159
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 161
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;->Companion:Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->features:Lcom/squareup/settings/server/Features;

    invoke-virtual {v1, v2}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState$Companion;->fromFeatureFlags(Lcom/squareup/settings/server/Features;)Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$InitialState;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    .line 158
    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;)V

    return-object v0
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->initialState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;

    check-cast p2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->render(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            "-",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getScreenState()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-result-object v0

    .line 171
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$StartResetPin;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$StartResetPin;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->squareCardResetPinWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "squareCardResetPinWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 173
    new-instance v3, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    const/4 v4, 0x0

    .line 174
    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 171
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 180
    :cond_0
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    if-eqz v1, :cond_3

    .line 181
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayWorker:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;

    check-cast v1, Lcom/squareup/workflow/Worker;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p3, v1, v3, v2, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 182
    new-instance v1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v4

    invoke-direct {v1, p0, v4}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    invoke-static {p3, v1, v3, v2, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 183
    move-object v1, v0

    check-cast v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-direct {p0, v1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->onGooglePayStatusUpdate(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;)Lcom/squareup/workflow/Worker;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v4, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;

    invoke-direct {v4, p2, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V

    move-object v7, v4

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 187
    new-instance v4, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    move-object v7, v4

    check-cast v7, Lcom/squareup/workflow/Worker;

    const/4 v8, 0x0

    new-instance v4, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$3;

    invoke-direct {v4, p2, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$3;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V

    move-object v9, v4

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v6, p3

    invoke-static/range {v6 .. v11}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 196
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getPrivateDataState()Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    move-result-object v4

    .line 197
    instance-of v5, v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;

    if-eqz v5, :cond_1

    .line 198
    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->privateDetailsDataStore:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;->privateCardDetailsState(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;

    move-result-object v2

    .line 510
    sget-object v4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v4, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v4, v2, v3}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 511
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 512
    const-class v3, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 199
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getPrivateDataState()Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    move-result-object v2

    check-cast v2, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 200
    new-instance v3, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$4;

    invoke-direct {v3, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$4;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 197
    invoke-interface {p3, v4, v2, v3}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 201
    :cond_1
    instance-of v4, v4, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$LoadedState$Enabled;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->privateDataWorker:Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;

    check-cast v4, Lcom/squareup/workflow/Worker;

    invoke-static {p3, v4, v3, v2, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 205
    :cond_2
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v7

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;->isToggleInProgress()Z

    move-result v8

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getPrivateDataState()Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;

    move-result-object v9

    .line 206
    new-instance v1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;

    invoke-direct {v1, p0, p2, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v10

    move-object v5, p0

    move-object v6, p1

    .line 204
    invoke-direct/range {v5 .. v10}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->activatedCardDetailsScreen(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZLcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 232
    :cond_3
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayBillingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayBillingAddress;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 234
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->viewBillingAddressWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string/jumbo v0, "viewBillingAddressWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 235
    new-instance v3, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    const/4 v4, 0x0

    .line 236
    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$6;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$6;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 233
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 239
    :cond_4
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayNotificationPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayNotificationPreferences;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 241
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->notificationPreferencesWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "notificationPreferencesWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 242
    sget-object v3, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v4, 0x0

    .line 243
    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$7;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$7;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 240
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 246
    :cond_5
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 247
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 252
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v7

    const/4 v8, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$9;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$9;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v9

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v6, p0

    invoke-static/range {v6 .. v11}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayScreen$default(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 256
    :cond_6
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    if-eqz v1, :cond_7

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->toStatus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;)Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;

    move-result-object v0

    new-instance v1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$10;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$10;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->googlePayScreen(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 259
    :cond_7
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$11;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$11;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->suspendedDialogScreen(Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 262
    :cond_8
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayingPrivateDataTwoFactor;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 263
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->enablePrivateDataWorkflow:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "enablePrivateDataWorkflow.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 264
    new-instance v4, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    invoke-direct {v4, p1}, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    const/4 v5, 0x0

    .line 265
    new-instance p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$12;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$12;-><init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 262
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_1
    return-object p1

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getScreenState()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;->snapshotState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
