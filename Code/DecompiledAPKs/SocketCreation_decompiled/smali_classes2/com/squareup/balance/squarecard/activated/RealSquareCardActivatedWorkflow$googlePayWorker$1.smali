.class public final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "SquareCardActivatedWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;-><init>(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivatedWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivatedWorkflow.kt\ncom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1\n*L\n1#1,509:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0005H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "googlePayDisposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "onStarted",
        "",
        "onStopped",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private googlePayDisposable:Lio/reactivex/disposables/SerialDisposable;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 127
    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    .line 128
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->googlePayDisposable:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getFeatures$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/settings/server/Features;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_GOOGLE_PAY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getGooglePayCapabilities$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$GooglePayStatus;->getComplete()Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getGooglePayHelper$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->isGooglePayAvailable()Lio/reactivex/Single;

    move-result-object v0

    .line 135
    sget-object v1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1$onStarted$1;->INSTANCE:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1$onStarted$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getGooglePayCapabilities$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->googlePayDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    :cond_1
    return-void
.end method

.method public onStopped()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$googlePayWorker$1;->googlePayDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method
