.class public abstract Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;
.super Ljava/lang/Object;
.source "CancelingBizbankWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput$Abort;,
        Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput$Success;,
        Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput$Failure;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;",
        "",
        "()V",
        "Abort",
        "Failure",
        "Success",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput$Abort;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput$Success;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput$Failure;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;-><init>()V

    return-void
.end method
