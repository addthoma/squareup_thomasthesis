.class final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;
.super Lcom/squareup/ui/LinkSpan;
.source "SquareCardActivatedCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CardSuspendedLinkSpan"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;",
        "Lcom/squareup/ui/LinkSpan;",
        "context",
        "Landroid/content/Context;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
        "",
        "(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "onClick",
        "widget",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    sget v0, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 264
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedCoordinator$CardSuspendedLinkSpan;->onEvent:Lkotlin/jvm/functions/Function1;

    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$LearnMoreAboutSuspendedCard;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$LearnMoreAboutSuspendedCard;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
