.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/RenderSignatureScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/RenderSignatureScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/RenderSignatureScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
            ">;)",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 49
    new-instance v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;
    .locals 8

    .line 55
    new-instance v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;
    .locals 7

    .line 42
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/balance/core/RenderSignatureScheduler;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->newInstance(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator_Factory_Factory;->get()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
