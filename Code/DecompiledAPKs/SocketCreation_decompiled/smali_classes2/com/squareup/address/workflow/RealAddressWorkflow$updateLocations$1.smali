.class final Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddressWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/workflow/RealAddressWorkflow;->updateLocations(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/address/workflow/AddressState;",
        "Lcom/squareup/address/workflow/AddressState;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddressWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddressWorkflow.kt\ncom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,245:1\n310#2,7:246\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddressWorkflow.kt\ncom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1\n*L\n192#1,7:246\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/address/workflow/AddressState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $locations:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;->$locations:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/address/workflow/AddressState;
    .locals 14

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;->$locations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 191
    iget-object v9, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;->$locations:Ljava/util/List;

    .line 247
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 248
    check-cast v10, Lcom/squareup/server/address/PostalLocation;

    .line 192
    iget-boolean v10, v10, Lcom/squareup/server/address/PostalLocation;->default_city:Z

    if-eqz v10, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    invoke-static {v2, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v10

    const/4 v11, 0x0

    const/16 v12, 0x13f

    const/4 v13, 0x0

    move-object v2, p1

    .line 190
    invoke-static/range {v2 .. v13}, Lcom/squareup/address/workflow/AddressState;->copy$default(Lcom/squareup/address/workflow/AddressState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;->$locations:Ljava/util/List;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/address/PostalLocation;

    iget-object v3, v0, Lcom/squareup/server/address/PostalLocation;->city:Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;->$locations:Ljava/util/List;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/address/PostalLocation;

    iget-object v4, v0, Lcom/squareup/server/address/PostalLocation;->state_abbreviation:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 188
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1b3

    const/4 v11, 0x0

    move-object v0, p1

    .line 185
    invoke-static/range {v0 .. v11}, Lcom/squareup/address/workflow/AddressState;->copy$default(Lcom/squareup/address/workflow/AddressState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 183
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1b3

    const/4 v11, 0x0

    const-string v3, ""

    const-string v4, ""

    move-object v0, p1

    .line 180
    invoke-static/range {v0 .. v11}, Lcom/squareup/address/workflow/AddressState;->copy$default(Lcom/squareup/address/workflow/AddressState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/AddressState;

    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;->invoke(Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    return-object p1
.end method
