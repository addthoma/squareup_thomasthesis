.class final Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddressWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/workflow/RealAddressWorkflow;->render(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/address/workflow/AddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/address/workflow/StateInfo;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "stateInfo",
        "Lcom/squareup/address/workflow/StateInfo;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;->this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;

    iput-object p2, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/StateInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;->invoke(Lcom/squareup/address/workflow/StateInfo;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/address/workflow/StateInfo;)V
    .locals 2

    const-string v0, "stateInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;->this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;

    invoke-static {v1, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow;->access$onStatePicked(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/address/workflow/StateInfo;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
