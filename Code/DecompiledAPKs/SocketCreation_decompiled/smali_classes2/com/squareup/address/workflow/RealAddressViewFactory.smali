.class public final Lcom/squareup/address/workflow/RealAddressViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealAddressViewFactory.kt"

# interfaces
.implements Lcom/squareup/address/workflow/AddressViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/address/workflow/RealAddressViewFactory;",
        "Lcom/squareup/address/workflow/AddressViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    sget-object v1, Lcom/squareup/address/workflow/AddressLayoutRunner;->Companion:Lcom/squareup/address/workflow/AddressLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 10
    sget-object v1, Lcom/squareup/address/workflow/ChooseStateDialogFactory;->Companion:Lcom/squareup/address/workflow/ChooseStateDialogFactory$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 11
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 12
    const-class v1, Lcom/squareup/address/workflow/AddressScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v4, 0x0

    invoke-static {v1, v4, v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 13
    sget-object v1, Lcom/squareup/address/workflow/RealAddressViewFactory$1;->INSTANCE:Lcom/squareup/address/workflow/RealAddressViewFactory$1;

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function4;

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    .line 11
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindViewBuilder$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
