.class public Lcom/squareup/address/StateScrubber;
.super Ljava/lang/Object;
.source "StateScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# static fields
.field private static final NON_UPPERCASE_LETTER:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[^A-Z]"

    .line 13
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/address/StateScrubber;->NON_UPPERCASE_LETTER:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 19
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    .line 20
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 22
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/address/StateScrubber;->NON_UPPERCASE_LETTER:Ljava/util/regex/Pattern;

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
