.class Lcom/squareup/address/StatePickerScreen$CountryStates;
.super Ljava/lang/Object;
.source "StatePickerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/StatePickerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CountryStates"
.end annotation


# instance fields
.field private final abbreviations:[Ljava/lang/String;

.field private final states:[Ljava/lang/String;


# direct methods
.method private constructor <init>([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/squareup/address/StatePickerScreen$CountryStates;->states:[Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/squareup/address/StatePickerScreen$CountryStates;->abbreviations:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/address/StatePickerScreen$CountryStates;)[Ljava/lang/String;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/address/StatePickerScreen$CountryStates;->states:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/address/StatePickerScreen$CountryStates;)[Ljava/lang/String;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/address/StatePickerScreen$CountryStates;->abbreviations:[Ljava/lang/String;

    return-object p0
.end method

.method static forCountryCode(Lcom/squareup/CountryCode;Lcom/squareup/util/Res;)Lcom/squareup/address/StatePickerScreen$CountryStates;
    .locals 2

    .line 87
    sget-object v0, Lcom/squareup/address/StatePickerScreen$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 93
    new-instance p0, Lcom/squareup/address/StatePickerScreen$CountryStates;

    sget v0, Lcom/squareup/address/R$array;->us_states:I

    .line 94
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/squareup/address/R$array;->us_state_abbrevs:I

    .line 95
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/address/StatePickerScreen$CountryStates;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    return-object p0

    .line 97
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported country code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 89
    :cond_1
    new-instance p0, Lcom/squareup/address/StatePickerScreen$CountryStates;

    sget v0, Lcom/squareup/address/R$array;->ca_provinces:I

    .line 90
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/squareup/address/R$array;->ca_province_abbrevs:I

    .line 91
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/address/StatePickerScreen$CountryStates;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    return-object p0
.end method
