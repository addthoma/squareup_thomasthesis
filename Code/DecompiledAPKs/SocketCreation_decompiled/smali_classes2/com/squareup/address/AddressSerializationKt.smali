.class public final Lcom/squareup/address/AddressSerializationKt;
.super Ljava/lang/Object;
.source "AddressSerialization.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddressSerialization.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddressSerialization.kt\ncom/squareup/address/AddressSerializationKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,30:1\n140#2:31\n*E\n*S KotlinDebug\n*F\n+ 1 AddressSerialization.kt\ncom/squareup/address/AddressSerializationKt\n*L\n27#1:31\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "readAddress",
        "Lcom/squareup/address/Address;",
        "Lokio/BufferedSource;",
        "writeAddress",
        "",
        "Lokio/BufferedSink;",
        "address",
        "address_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final readAddress(Lokio/BufferedSource;)Lcom/squareup/address/Address;
    .locals 8

    const-string v0, "$this$readAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    .line 23
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 24
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v4

    .line 25
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v5

    .line 26
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v6

    .line 31
    sget-object v0, Lcom/squareup/address/AddressSerializationKt$readAddress$$inlined$readOptionalEnumByOrdinal$1;->INSTANCE:Lcom/squareup/address/AddressSerializationKt$readAddress$$inlined$readOptionalEnumByOrdinal$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Enum;

    move-object v7, p0

    check-cast v7, Lcom/squareup/CountryCode;

    .line 21
    new-instance p0, Lcom/squareup/address/Address;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object p0
.end method

.method public static final writeAddress(Lokio/BufferedSink;Lcom/squareup/address/Address;)V
    .locals 1

    const-string v0, "$this$writeAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p1, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 13
    iget-object v0, p1, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 14
    iget-object v0, p1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 15
    iget-object v0, p1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 16
    iget-object v0, p1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 17
    iget-object p1, p1, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    check-cast p1, Ljava/lang/Enum;

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeOptionalEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    return-void
.end method
