.class public Lcom/squareup/address/StatePickerScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "StatePickerScreen.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/address/StatePickerScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/StatePickerScreen$CountryStates;,
        Lcom/squareup/address/StatePickerScreen$DialogBuilder;,
        Lcom/squareup/address/StatePickerScreen$Factory;,
        Lcom/squareup/address/StatePickerScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/address/StatePickerScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/address/-$$Lambda$StatePickerScreen$0IuXZXdKD5_IqSuGYwuidZc-Uos;->INSTANCE:Lcom/squareup/address/-$$Lambda$StatePickerScreen$0IuXZXdKD5_IqSuGYwuidZc-Uos;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/address/StatePickerScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/CountryCode;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/address/StatePickerScreen;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/address/StatePickerScreen;)Lcom/squareup/CountryCode;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/address/StatePickerScreen;->countryCode:Lcom/squareup/CountryCode;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/address/StatePickerScreen;
    .locals 1

    .line 73
    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v0, p0

    .line 74
    new-instance v0, Lcom/squareup/address/StatePickerScreen;

    invoke-direct {v0, p0}, Lcom/squareup/address/StatePickerScreen;-><init>(Lcom/squareup/CountryCode;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 68
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 69
    iget-object p2, p0, Lcom/squareup/address/StatePickerScreen;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {p2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
