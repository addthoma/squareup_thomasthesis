.class public abstract Lcom/squareup/android/activity/AndroidConfigurationChangeMonitorModule;
.super Ljava/lang/Object;
.source "AndroidConfigurationChangeMonitorModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindAndroidConfigurationChangeMonitor(Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;)Lcom/squareup/util/AndroidConfigurationChangeMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
