.class public abstract Lcom/squareup/CommonAppModuleProdWithoutServer;
.super Ljava/lang/Object;
.source "CommonAppModuleProdWithoutServer.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/account/AccountModule$Prod;,
        Lcom/squareup/server/account/AccountServiceReleaseModule;,
        Lcom/squareup/server/account/AccountStatusServiceReleaseModule;,
        Lcom/squareup/server/address/AddressServiceReleaseModule;,
        Lcom/squareup/cogs/CogsServiceReleaseModule;,
        Lcom/squareup/CommonAppModule$Real;,
        Lcom/squareup/communications/CommunicationsReleaseModule;,
        Lcom/squareup/server/api/ConnectServiceReleaseModule;,
        Lcom/squareup/server/crm/CrmServicesReleaseModule;,
        Lcom/squareup/customreport/data/service/CustomReportServiceReleaseModule;,
        Lcom/squareup/depositschedule/DepositScheduleServiceReleaseModule;,
        Lcom/squareup/server/analytics/EventStreamModule$Prod;,
        Lcom/squareup/experiments/ExperimentsModule$Prod;,
        Lcom/squareup/cardreader/dipper/FirmwareUpdateModule;,
        Lcom/squareup/http/HttpReleaseModule;,
        Lcom/squareup/instantdeposit/InstantDepositsServiceReleaseModule;,
        Lcom/squareup/debitcard/LinkDebitCardServiceReleaseModule;,
        Lcom/squareup/log/LogModule$Prod;,
        Lcom/squareup/merchantprofile/MerchantProfileServiceReleaseModule;,
        Lcom/squareup/api/multipassauth/MultipassServiceReleaseModule;,
        Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictionsReleaseModule;,
        Lcom/squareup/ms/ReleaseMinesweeperModule;,
        Lcom/squareup/server/RestAdapterReleaseModule;,
        Lcom/squareup/cardreader/SecureSessionModule;,
        Lcom/squareup/api/ServicesReleaseModule;,
        Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsServiceReleaseModule;,
        Lcom/squareup/ThumborProdModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideJobNotificationManager()Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/backgroundjob/notification/NoOpBackgroundJobNotificationManager;

    invoke-direct {v0}, Lcom/squareup/backgroundjob/notification/NoOpBackgroundJobNotificationManager;-><init>()V

    return-object v0
.end method

.method static providePicasso(Landroid/app/Application;Lcom/squareup/picasso/Cache;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;)Lcom/squareup/picasso/Picasso;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 96
    new-instance v0, Lcom/squareup/picasso/Picasso$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/picasso/Picasso$Builder;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso$Builder;->memoryCache(Lcom/squareup/picasso/Cache;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object p0

    const/4 p1, 0x0

    .line 98
    invoke-virtual {p0, p1}, Lcom/squareup/picasso/Picasso$Builder;->indicatorsEnabled(Z)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object p0

    .line 99
    invoke-virtual {p0, p2}, Lcom/squareup/picasso/Picasso$Builder;->requestTransformer(Lcom/squareup/picasso/Picasso$RequestTransformer;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object p0

    .line 100
    invoke-virtual {p0}, Lcom/squareup/picasso/Picasso$Builder;->build()Lcom/squareup/picasso/Picasso;

    move-result-object p0

    return-object p0
.end method

.method static provideRealNfcUtils()Lcom/squareup/util/NfcAdapterShim;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/util/NfcAdapterShim$RealNfcUtils;

    invoke-direct {v0}, Lcom/squareup/util/NfcAdapterShim$RealNfcUtils;-><init>()V

    return-object v0
.end method

.method static provideViewHierarchyBuilder()Lcom/squareup/radiography/FocusedActivityScanner;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 87
    new-instance v0, Lcom/squareup/radiography/FocusedActivityScanner;

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {v0, v1}, Lcom/squareup/radiography/FocusedActivityScanner;-><init>([I)V

    return-object v0
.end method


# virtual methods
.method abstract bindFileThreadExecutor(Lcom/squareup/thread/executor/SerialExecutor;)Ljava/util/concurrent/Executor;
    .param p1    # Lcom/squareup/thread/executor/SerialExecutor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBleAutoConnector(Lcom/squareup/cardreader/ble/RealBleAutoConnector;)Lcom/squareup/cardreader/ble/BleAutoConnector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSwipeEventLogger(Lcom/squareup/swipe/AnalyticsSwipeEventLogger;)Lcom/squareup/logging/SwipeEventLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
