.class public final Lcom/squareup/common/bootstrap/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/bootstrap/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final about_device_setting_container:I = 0x7f0a00f5

.field public static final about_device_settings_location:I = 0x7f0a00f6

.field public static final about_device_settings_phone:I = 0x7f0a00f7

.field public static final about_device_settings_storage:I = 0x7f0a00f8

.field public static final enable_device_settings_learn_more:I = 0x7f0a0700

.field public static final enable_device_settings_location_button:I = 0x7f0a0701

.field public static final enable_device_settings_phone_button:I = 0x7f0a0702

.field public static final enable_device_settings_storage_button:I = 0x7f0a0703

.field public static final image:I = 0x7f0a0821

.field public static final location_settings_button:I = 0x7f0a095e

.field public static final message:I = 0x7f0a09d3

.field public static final permission_denied_body:I = 0x7f0a0c13

.field public static final permission_denied_button:I = 0x7f0a0c14

.field public static final permission_denied_glyph:I = 0x7f0a0c15

.field public static final permission_denied_title:I = 0x7f0a0c16

.field public static final pos_api_progress_bar:I = 0x7f0a0c3d

.field public static final title:I = 0x7f0a103f

.field public static final why_message:I = 0x7f0a111b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
