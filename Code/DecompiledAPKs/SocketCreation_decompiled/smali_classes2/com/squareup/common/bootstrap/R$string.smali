.class public final Lcom/squareup/common/bootstrap/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/bootstrap/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final about_device_settings_device_storage:I = 0x7f12001d

.field public static final about_device_settings_location_services:I = 0x7f12001e

.field public static final about_device_settings_phone_access:I = 0x7f12001f

.field public static final accept_credit_cards_offline:I = 0x7f120023

.field public static final complete_payment_in_progress_message:I = 0x7f12046f

.field public static final complete_payment_in_progress_title:I = 0x7f120470

.field public static final coupon_redeem_reward:I = 0x7f1205a8

.field public static final enable_device_settings_description:I = 0x7f120a71

.field public static final enable_device_settings_learn_more:I = 0x7f120a72

.field public static final enable_device_settings_title:I = 0x7f120a73

.field public static final location_go_to_location_settings:I = 0x7f120eef

.field public static final location_off_message:I = 0x7f120ef0

.field public static final location_off_title:I = 0x7f120ef1

.field public static final location_use_gps_satellites:I = 0x7f120ef2

.field public static final location_use_wireless_networks:I = 0x7f120ef3

.field public static final location_waiting_message:I = 0x7f120ef4

.field public static final location_waiting_speed_up_message:I = 0x7f120ef5

.field public static final location_waiting_title:I = 0x7f120ef6

.field public static final location_why:I = 0x7f120ef7

.field public static final offline_mode_warning:I = 0x7f1210de


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
