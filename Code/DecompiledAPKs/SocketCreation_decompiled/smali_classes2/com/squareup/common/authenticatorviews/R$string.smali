.class public final Lcom/squareup/common/authenticatorviews/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/authenticatorviews/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final account_status_error_message:I = 0x7f12003a

.field public static final account_status_error_title:I = 0x7f12003b

.field public static final back:I = 0x7f12011b

.field public static final device_code_auth_failed_message:I = 0x7f120833

.field public static final device_code_hint:I = 0x7f120834

.field public static final device_code_hint_link_text:I = 0x7f120835

.field public static final device_code_login_failed_message:I = 0x7f120836

.field public static final device_code_login_password_hint:I = 0x7f120837

.field public static final device_code_sign_in_to_square:I = 0x7f120838

.field public static final device_code_url:I = 0x7f120839

.field public static final forgot_password:I = 0x7f120ad9

.field public static final forgot_password_subtitle:I = 0x7f120add

.field public static final forgot_password_title:I = 0x7f120ade

.field public static final login_email_hint:I = 0x7f120ef8

.field public static final login_failed_message:I = 0x7f120ef9

.field public static final login_failed_title:I = 0x7f120efa

.field public static final login_new_account:I = 0x7f120efb

.field public static final login_password_hint:I = 0x7f120efc

.field public static final mobile_phone_number:I = 0x7f120ffa

.field public static final select_a_business:I = 0x7f12179c

.field public static final select_a_business_subtext:I = 0x7f12179d

.field public static final select_a_location:I = 0x7f12179e

.field public static final select_a_location_subtext:I = 0x7f12179f

.field public static final sign_in:I = 0x7f1217f5

.field public static final sign_in_to_square:I = 0x7f1217f7

.field public static final two_factor_auth_url:I = 0x7f121ab6

.field public static final two_factor_contact_support_url:I = 0x7f121ab7

.field public static final two_factor_details_picker_authentication_app:I = 0x7f121ab8

.field public static final two_factor_details_picker_sms:I = 0x7f121ab9

.field public static final two_factor_enroll_google_auth_code_hint:I = 0x7f121aba

.field public static final two_factor_enroll_google_auth_code_subtitle:I = 0x7f121abb

.field public static final two_factor_enroll_google_auth_code_title:I = 0x7f121abc

.field public static final two_factor_enroll_google_auth_learn_more:I = 0x7f121abd

.field public static final two_factor_enroll_google_auth_learn_more_text:I = 0x7f121abe

.field public static final two_factor_enroll_google_auth_qr_hint:I = 0x7f121abf

.field public static final two_factor_enroll_google_auth_qr_subtitle:I = 0x7f121ac0

.field public static final two_factor_enroll_hint:I = 0x7f121ac1

.field public static final two_factor_enroll_skip:I = 0x7f121ac2

.field public static final two_factor_enroll_skip_for_now:I = 0x7f121ac3

.field public static final two_factor_enroll_sms_hint:I = 0x7f121ac4

.field public static final two_factor_enroll_subtitle:I = 0x7f121ac5

.field public static final two_factor_enroll_title:I = 0x7f121ac6

.field public static final two_factor_google_auth_cant_scan_barcode:I = 0x7f121ac7

.field public static final two_factor_google_auth_copy_code:I = 0x7f121ac8

.field public static final two_factor_resend_code:I = 0x7f121ac9

.field public static final two_factor_resend_code_pending:I = 0x7f121aca

.field public static final two_factor_resend_code_toast:I = 0x7f121acb

.field public static final two_factor_sms_picker_subtitle:I = 0x7f121acc

.field public static final two_factor_sms_picker_title:I = 0x7f121acd

.field public static final two_factor_use_sms:I = 0x7f121ad1

.field public static final two_factor_verification_code:I = 0x7f121ad2

.field public static final two_factor_verification_google_auth_contact_support:I = 0x7f121ad3

.field public static final two_factor_verification_google_auth_hint:I = 0x7f121ad4

.field public static final two_factor_verification_google_auth_subtitle:I = 0x7f121ad5

.field public static final two_factor_verification_google_auth_support:I = 0x7f121ad6

.field public static final two_factor_verification_google_auth_title:I = 0x7f121ad7

.field public static final two_factor_verification_remember_this_device:I = 0x7f121ad8

.field public static final two_factor_verification_sms_hint:I = 0x7f121ad9

.field public static final two_factor_verification_sms_subtitle:I = 0x7f121ada

.field public static final two_factor_verification_sms_title:I = 0x7f121adb

.field public static final two_factor_verify:I = 0x7f121adc

.field public static final use_a_device_code:I = 0x7f121b8f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
