.class Lcom/squareup/applet/AppletsDrawerLayout$2;
.super Landroid/view/View$AccessibilityDelegate;
.source "AppletsDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerLayout;->announceAppletNameForAccessibility(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerLayout;

.field final synthetic val$appletName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerLayout;Ljava/lang/String;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout$2;->this$0:Lcom/squareup/applet/AppletsDrawerLayout;

    iput-object p2, p0, Lcom/squareup/applet/AppletsDrawerLayout$2;->val$appletName:Ljava/lang/String;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .line 150
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    .line 152
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 153
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object p2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/squareup/applet/AppletsDrawerLayout$2;->val$appletName:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_0
    return p1
.end method
