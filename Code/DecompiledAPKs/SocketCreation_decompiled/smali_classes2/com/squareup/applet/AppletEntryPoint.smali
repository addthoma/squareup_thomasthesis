.class public Lcom/squareup/applet/AppletEntryPoint;
.super Ljava/lang/Object;
.source "AppletEntryPoint.java"


# instance fields
.field private final allSections:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/applet/AppletSection;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultSection:Lcom/squareup/applet/AppletSection;

.field private final gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final selectedSectionSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/applet/AppletSection;",
            "[",
            "Lcom/squareup/applet/AppletSection;",
            ")V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/applet/AppletEntryPoint;->selectedSectionSetting:Lcom/squareup/settings/LocalSetting;

    .line 34
    iput-object p2, p0, Lcom/squareup/applet/AppletEntryPoint;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 35
    iput-object p3, p0, Lcom/squareup/applet/AppletEntryPoint;->defaultSection:Lcom/squareup/applet/AppletSection;

    .line 37
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 38
    invoke-interface {p1, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    invoke-static {p1, p4}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 40
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/applet/AppletEntryPoint;->allSections:Ljava/util/Set;

    return-void
.end method

.method private canAccess(Lcom/squareup/applet/AppletSection;)Z
    .locals 1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 116
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PermissionGatekeeper;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private getPreferredSection()Lcom/squareup/applet/AppletSection;
    .locals 4

    .line 88
    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->selectedSectionSetting:Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/applet/AppletEntryPoint;->defaultSection:Lcom/squareup/applet/AppletSection;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/squareup/applet/AppletEntryPoint;->allSections:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/applet/AppletSection;

    .line 90
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->defaultSection:Lcom/squareup/applet/AppletSection;

    return-object v0
.end method


# virtual methods
.method public getAllSections()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/applet/AppletSection;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->allSections:Ljava/util/Set;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 69
    invoke-direct {p0}, Lcom/squareup/applet/AppletEntryPoint;->getPreferredSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    .line 70
    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletEntryPoint;->canAccess(Lcom/squareup/applet/AppletSection;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->allSections:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/AppletSection;

    .line 76
    invoke-direct {p0, v1}, Lcom/squareup/applet/AppletEntryPoint;->canAccess(Lcom/squareup/applet/AppletSection;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    invoke-virtual {p0, v1}, Lcom/squareup/applet/AppletEntryPoint;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    .line 78
    invoke-virtual {v1}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0

    .line 84
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Applet has no visible and allowed sections!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getInitialScreenWithoutAccessChecking()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 57
    invoke-direct {p0}, Lcom/squareup/applet/AppletEntryPoint;->getPreferredSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getRedirect(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 99
    invoke-static {p1}, Lcom/squareup/container/Masters;->getSection(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Class;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 102
    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->allSections:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/AppletSection;

    .line 103
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 104
    invoke-direct {p0, v1}, Lcom/squareup/applet/AppletEntryPoint;->canAccess(Lcom/squareup/applet/AppletSection;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/applet/AppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public setSelectedSection(Lcom/squareup/applet/AppletSection;)V
    .locals 0

    .line 48
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletEntryPoint;->setSelectedSection(Ljava/lang/Class;)V

    return-void
.end method

.method public setSelectedSection(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/applet/AppletSection;",
            ">;)V"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/applet/AppletEntryPoint;->selectedSectionSetting:Lcom/squareup/settings/LocalSetting;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method
