.class public Lcom/squareup/applet/RealAppletSelection;
.super Ljava/lang/Object;
.source "RealAppletSelection.java"

# interfaces
.implements Lcom/squareup/applet/AppletSelection;
.implements Lmortar/Scoped;


# instance fields
.field private final applets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedApplet:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/applet/Applets;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/applet/RealAppletSelection;->selectedApplet:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 25
    invoke-interface {p1}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/applet/RealAppletSelection;->applets:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$RealAppletSelection(Lcom/squareup/applet/Applet;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 36
    iget-object p2, p0, Lcom/squareup/applet/RealAppletSelection;->selectedApplet:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    if-ne p2, p1, :cond_0

    return-void

    .line 39
    :cond_0
    iget-object p2, p0, Lcom/squareup/applet/RealAppletSelection;->selectedApplet:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 29
    iget-object v0, p0, Lcom/squareup/applet/RealAppletSelection;->applets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/Applet;

    .line 30
    invoke-virtual {v1}, Lcom/squareup/applet/Applet;->onSelected()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/applet/-$$Lambda$RealAppletSelection$V5kwkO6p8Yj_eIdDWyuNE_0stDc;

    invoke-direct {v3, p0, v1}, Lcom/squareup/applet/-$$Lambda$RealAppletSelection$V5kwkO6p8Yj_eIdDWyuNE_0stDc;-><init>(Lcom/squareup/applet/RealAppletSelection;Lcom/squareup/applet/Applet;)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public selectedApplet()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/applet/RealAppletSelection;->selectedApplet:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
