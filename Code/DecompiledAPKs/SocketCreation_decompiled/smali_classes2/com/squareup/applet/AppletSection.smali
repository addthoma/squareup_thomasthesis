.class public abstract Lcom/squareup/applet/AppletSection;
.super Ljava/lang/Object;
.source "AppletSection.java"


# instance fields
.field public final accessControl:Lcom/squareup/applet/SectionAccess;


# direct methods
.method protected constructor <init>(Lcom/squareup/applet/SectionAccess;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/applet/AppletSection;->accessControl:Lcom/squareup/applet/SectionAccess;

    return-void
.end method


# virtual methods
.method public getAccessControl()Lcom/squareup/applet/SectionAccess;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/applet/AppletSection;->accessControl:Lcom/squareup/applet/SectionAccess;

    return-object v0
.end method

.method public abstract getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
.end method

.method public isDevSection()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRestricted()Z
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
