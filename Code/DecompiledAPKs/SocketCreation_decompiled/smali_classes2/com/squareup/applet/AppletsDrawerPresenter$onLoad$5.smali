.class final Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;
.super Lkotlin/jvm/internal/Lambda;
.source "AppletsDrawerPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppletsDrawerPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppletsDrawerPresenter.kt\ncom/squareup/applet/AppletsDrawerPresenter$onLoad$5\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,254:1\n57#2,4:255\n*E\n*S KotlinDebug\n*F\n+ 1 AppletsDrawerPresenter.kt\ncom/squareup/applet/AppletsDrawerPresenter$onLoad$5\n*L\n150#1,4:255\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 150
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {v0}, Lcom/squareup/applet/AppletsDrawerPresenter;->access$getDrawerLocked$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    check-cast v0, Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {v1}, Lcom/squareup/applet/AppletsDrawerPresenter;->access$getDrawerIsReady$p(Lcom/squareup/applet/AppletsDrawerPresenter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    check-cast v1, Lio/reactivex/Observable;

    .line 256
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 257
    new-instance v2, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5$$special$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5$$special$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 255
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5$2;

    invoke-direct {v1, p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5$2;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(drawerLock\u2026ribe { updateDrawer(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$5;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
