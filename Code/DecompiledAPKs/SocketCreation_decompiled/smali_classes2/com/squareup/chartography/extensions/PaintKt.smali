.class public final Lcom/squareup/chartography/extensions/PaintKt;
.super Ljava/lang/Object;
.source "Paint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"+\u0010\u0002\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018@@@X\u0080\u000e\u00f8\u0001\u0000\u00a2\u0006\u000c\u001a\u0004\u0008\u0004\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0008"
    }
    d2 = {
        "value",
        "Lcom/squareup/chartography/util/ColorResource;",
        "colorResource",
        "Landroid/graphics/Paint;",
        "getColorResource",
        "(Landroid/graphics/Paint;)I",
        "setColorResource-9Ee6Eao",
        "(Landroid/graphics/Paint;I)V",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getColorResource(Landroid/graphics/Paint;)I
    .locals 1

    const-string v0, "$this$colorResource"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p0}, Landroid/graphics/Paint;->getColor()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/chartography/util/ColorResource;->constructor-impl(I)I

    move-result p0

    return p0
.end method

.method public static final setColorResource-9Ee6Eao(Landroid/graphics/Paint;I)V
    .locals 1

    const-string v0, "$this$colorResource"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method
