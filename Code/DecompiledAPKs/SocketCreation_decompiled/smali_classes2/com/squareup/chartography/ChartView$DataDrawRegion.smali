.class public final Lcom/squareup/chartography/ChartView$DataDrawRegion;
.super Ljava/lang/Object;
.source "ChartView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/chartography/ChartView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DataDrawRegion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0018\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BK\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003JO\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/chartography/ChartView$DataDrawRegion;",
        "",
        "x",
        "",
        "y",
        "width",
        "height",
        "xStep",
        "yStep",
        "maxXLabelWidth",
        "(FFFFFFF)V",
        "getHeight",
        "()F",
        "getMaxXLabelWidth",
        "getWidth",
        "getX",
        "getXStep",
        "getY",
        "getYStep",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final height:F

.field private final maxXLabelWidth:F

.field private final width:F

.field private final x:F

.field private final xStep:F

.field private final y:F

.field private final yStep:F


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/squareup/chartography/ChartView$DataDrawRegion;-><init>(FFFFFFFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(FFFFFFF)V
    .locals 0

    .line 466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    iput p2, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    iput p3, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    iput p4, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    iput p5, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    iput p6, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    iput p7, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    return-void
.end method

.method public synthetic constructor <init>(FFFFFFFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p9, p8, 0x1

    const/4 v0, 0x0

    if-eqz p9, :cond_0

    const/4 p9, 0x0

    goto :goto_0

    :cond_0
    move p9, p1

    :goto_0
    and-int/lit8 p1, p8, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    and-int/lit8 p1, p8, 0x4

    if-eqz p1, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    move v2, p3

    :goto_2
    and-int/lit8 p1, p8, 0x8

    if-eqz p1, :cond_3

    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    move v3, p4

    :goto_3
    and-int/lit8 p1, p8, 0x10

    if-eqz p1, :cond_4

    const/4 v4, 0x0

    goto :goto_4

    :cond_4
    move v4, p5

    :goto_4
    and-int/lit8 p1, p8, 0x20

    if-eqz p1, :cond_5

    const/4 v5, 0x0

    goto :goto_5

    :cond_5
    move v5, p6

    :goto_5
    and-int/lit8 p1, p8, 0x40

    if-eqz p1, :cond_6

    const/4 p8, 0x0

    goto :goto_6

    :cond_6
    move p8, p7

    :goto_6
    move-object p1, p0

    move p2, p9

    move p3, v1

    move p4, v2

    move p5, v3

    move p6, v4

    move p7, v5

    .line 473
    invoke-direct/range {p1 .. p8}, Lcom/squareup/chartography/ChartView$DataDrawRegion;-><init>(FFFFFFF)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/chartography/ChartView$DataDrawRegion;FFFFFFFILjava/lang/Object;)Lcom/squareup/chartography/ChartView$DataDrawRegion;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget p1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget p2, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget p7, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    :cond_6
    move v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->copy(FFFFFFF)Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    return v0
.end method

.method public final component2()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    return v0
.end method

.method public final component4()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    return v0
.end method

.method public final component5()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    return v0
.end method

.method public final component6()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    return v0
.end method

.method public final component7()F
    .locals 1

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    return v0
.end method

.method public final copy(FFFFFFF)Lcom/squareup/chartography/ChartView$DataDrawRegion;
    .locals 9

    new-instance v8, Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-object v0, v8

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/squareup/chartography/ChartView$DataDrawRegion;-><init>(FFFFFFF)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    iget v1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    iget v1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    iget v1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    iget v1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    iget v1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    iget v1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    iget p1, p1, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHeight()F
    .locals 1

    .line 470
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    return v0
.end method

.method public final getMaxXLabelWidth()F
    .locals 1

    .line 473
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    return v0
.end method

.method public final getWidth()F
    .locals 1

    .line 469
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    return v0
.end method

.method public final getX()F
    .locals 1

    .line 467
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    return v0
.end method

.method public final getXStep()F
    .locals 1

    .line 471
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    return v0
.end method

.method public final getY()F
    .locals 1

    .line 468
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    return v0
.end method

.method public final getYStep()F
    .locals 1

    .line 472
    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    invoke-static {v0}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataDrawRegion(x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->width:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->height:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", xStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->xStep:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", yStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->yStep:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", maxXLabelWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/chartography/ChartView$DataDrawRegion;->maxXLabelWidth:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
