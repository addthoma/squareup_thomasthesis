.class public abstract Lcom/squareup/chartography/ChartView;
.super Landroid/view/View;
.source "ChartView.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/chartography/ChartView$DataDrawRegion;,
        Lcom/squareup/chartography/ChartView$ScrubbingListener;,
        Lcom/squareup/chartography/ChartView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChartView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChartView.kt\ncom/squareup/chartography/ChartView\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,487:1\n9368#2:488\n9701#2,3:489\n37#3,2:492\n*E\n*S KotlinDebug\n*F\n+ 1 ChartView.kt\ncom/squareup/chartography/ChartView\n*L\n240#1:488\n240#1,3:489\n240#1,2:492\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008&\u0018\u0000 \u008b\u00012\u00020\u00012\u00020\u0002:\u0006\u008b\u0001\u008c\u0001\u008d\u0001B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010j\u001a\u00020kH\u0002J\u0008\u0010l\u001a\u00020\u0008H\u0002J\u0010\u0010m\u001a\u00020k2\u0006\u0010n\u001a\u00020oH\u0016J\u0010\u0010p\u001a\u00020k2\u0006\u0010n\u001a\u00020oH\u0002J\u0008\u0010q\u001a\u00020kH\u0002J\"\u0010r\u001a\u00020\"2\u0006\u0010s\u001a\u00020t2\u0008\u0008\u0001\u0010u\u001a\u00020\u00082\u0006\u0010v\u001a\u00020\u0008H\u0002J\u0010\u0010w\u001a\u00020k2\u0006\u0010n\u001a\u00020oH$J0\u0010x\u001a\u00020k2\u0006\u0010y\u001a\u0002002\u0006\u0010z\u001a\u00020\u00082\u0006\u0010{\u001a\u00020\u00082\u0006\u0010|\u001a\u00020\u00082\u0006\u0010}\u001a\u00020\u0008H\u0014J\u0019\u0010~\u001a\u00020k2\u0006\u0010\u007f\u001a\u00020\u00082\u0007\u0010\u0080\u0001\u001a\u00020\u0008H\u0014J \u0010\u0081\u0001\u001a\u0002002\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u00012\n\u0010\u0083\u0001\u001a\u0005\u0018\u00010\u0084\u0001H\u0016J\u0012\u0010\u0085\u0001\u001a\u00020\u001b2\u0007\u0010\u0086\u0001\u001a\u00020gH\u0002J\u0012\u0010\u0087\u0001\u001a\u00020k2\u0007\u0010\u0088\u0001\u001a\u00020\u001bH\u0002J\u001b\u0010\u0089\u0001\u001a\u00020k2\u0007\u0010\u0088\u0001\u001a\u00020\u001b2\u0007\u0010\u008a\u0001\u001a\u00020\u001bH\u0002R,\u0010\u000c\u001a\u0006\u0012\u0002\u0008\u00030\u000b2\n\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u000b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\'\u0010\u0012\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u00118F@FX\u0086\u000e\u00f8\u0001\u0000\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0018X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR$\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\n\u001a\u00020\u001b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u001a\u0010!\u001a\u00020\"X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010$\"\u0004\u0008%\u0010&R\u001a\u0010\'\u001a\u00020(X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008)\u0010*\"\u0004\u0008+\u0010,R\u0014\u0010-\u001a\u00020\u001bX\u0094D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010\u001eR\u001a\u0010/\u001a\u000200X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00081\u00102\"\u0004\u00083\u00104R\u000e\u00105\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00106\u001a\u000207X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00088\u00109R$\u0010:\u001a\u00020\u001b2\u0006\u0010\n\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008;\u0010\u001e\"\u0004\u0008<\u0010 R\u001c\u0010=\u001a\u0004\u0018\u00010>X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008?\u0010@\"\u0004\u0008A\u0010BR\u000e\u0010C\u001a\u000200X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010D\u001a\u00020\"X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008E\u0010$\"\u0004\u0008F\u0010&R$\u0010G\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0008@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008H\u0010\u0014\"\u0004\u0008I\u0010\u0016R\'\u0010J\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u00118F@FX\u0086\u000e\u00f8\u0001\u0000\u00a2\u0006\u000c\u001a\u0004\u0008K\u0010\u0014\"\u0004\u0008L\u0010\u0016R\u0014\u0010M\u001a\u00020\u0018X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008N\u0010\u001aR\u001a\u0010O\u001a\u00020\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008P\u0010\u001e\"\u0004\u0008Q\u0010 R\u0010\u0010R\u001a\u0004\u0018\u00010SX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010T\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008U\u0010\u0014\"\u0004\u0008V\u0010\u0016R\u001a\u0010W\u001a\u00020\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008X\u0010\u001e\"\u0004\u0008Y\u0010 R$\u0010Z\u001a\u00020\u001b2\u0006\u0010\n\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008[\u0010\u001e\"\u0004\u0008\\\u0010 R\u001a\u0010]\u001a\u00020\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008^\u0010\u001e\"\u0004\u0008_\u0010 R$\u0010`\u001a\u00020\u001b2\u0006\u0010\n\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008a\u0010\u001e\"\u0004\u0008b\u0010 R$\u0010c\u001a\u00020\u001b2\u0006\u0010\n\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008d\u0010\u001e\"\u0004\u0008e\u0010 R\u0018\u0010f\u001a\u00020\u001b*\u00020g8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008h\u0010i\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u008e\u0001"
    }
    d2 = {
        "Lcom/squareup/chartography/ChartView;",
        "Landroid/view/View;",
        "Landroid/view/View$OnTouchListener;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "value",
        "Lcom/squareup/chartography/ChartAdapter;",
        "adapter",
        "getAdapter",
        "()Lcom/squareup/chartography/ChartAdapter;",
        "setAdapter",
        "(Lcom/squareup/chartography/ChartAdapter;)V",
        "Lcom/squareup/chartography/util/ColorResource;",
        "axisColor",
        "getAxisColor",
        "()I",
        "setAxisColor-OeUeViQ",
        "(I)V",
        "axisPaint",
        "Landroid/graphics/Paint;",
        "getAxisPaint",
        "()Landroid/graphics/Paint;",
        "",
        "axisStrokeWidth",
        "getAxisStrokeWidth",
        "()F",
        "setAxisStrokeWidth",
        "(F)V",
        "colorWheel",
        "Lcom/squareup/chartography/util/ColorWheel;",
        "getColorWheel$public_release",
        "()Lcom/squareup/chartography/util/ColorWheel;",
        "setColorWheel$public_release",
        "(Lcom/squareup/chartography/util/ColorWheel;)V",
        "dataDrawRegion",
        "Lcom/squareup/chartography/ChartView$DataDrawRegion;",
        "getDataDrawRegion",
        "()Lcom/squareup/chartography/ChartView$DataDrawRegion;",
        "setDataDrawRegion",
        "(Lcom/squareup/chartography/ChartView$DataDrawRegion;)V",
        "headerSpacing",
        "getHeaderSpacing",
        "hideFirstYLabel",
        "",
        "getHideFirstYLabel",
        "()Z",
        "setHideFirstYLabel",
        "(Z)V",
        "initialScrubRegion",
        "pixel",
        "Lcom/squareup/chartography/util/Pixel;",
        "getPixel$public_release",
        "()Lcom/squareup/chartography/util/Pixel;",
        "rangeScalingFactor",
        "getRangeScalingFactor",
        "setRangeScalingFactor",
        "scrubbingListener",
        "Lcom/squareup/chartography/ChartView$ScrubbingListener;",
        "getScrubbingListener",
        "()Lcom/squareup/chartography/ChartView$ScrubbingListener;",
        "setScrubbingListener",
        "(Lcom/squareup/chartography/ChartView$ScrubbingListener;)V",
        "scrubbingLock",
        "selectedColorWheel",
        "getSelectedColorWheel$public_release",
        "setSelectedColorWheel$public_release",
        "selectedDomain",
        "getSelectedDomain",
        "setSelectedDomain",
        "textColor",
        "getTextColor",
        "setTextColor-OeUeViQ",
        "textPaint",
        "getTextPaint",
        "tickSize",
        "getTickSize",
        "setTickSize",
        "touchOrigin",
        "Landroid/graphics/PointF;",
        "xLabelFrquency",
        "getXLabelFrquency",
        "setXLabelFrquency",
        "xLabelMinGap",
        "getXLabelMinGap",
        "setXLabelMinGap",
        "xLabelSize",
        "getXLabelSize",
        "setXLabelSize",
        "xLabelTickSpacing",
        "getXLabelTickSpacing",
        "setXLabelTickSpacing",
        "yLabelSize",
        "getYLabelSize",
        "setYLabelSize",
        "yLabelSpacing",
        "getYLabelSpacing",
        "setYLabelSpacing",
        "descentPercentage",
        "Landroid/graphics/Paint$FontMetrics;",
        "getDescentPercentage",
        "(Landroid/graphics/Paint$FontMetrics;)F",
        "calculateDrawRegion",
        "",
        "calculatedXLabelFrequency",
        "draw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawAxes",
        "endScrubbing",
        "loadColorWheel",
        "typedArray",
        "Landroid/content/res/TypedArray;",
        "index",
        "defaultValue",
        "onDrawData",
        "onLayout",
        "changed",
        "left",
        "top",
        "right",
        "bottom",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "onTouch",
        "v",
        "event",
        "Landroid/view/MotionEvent;",
        "scaledYLabelSize",
        "fontMetrics",
        "selectDomainAtPosition",
        "x",
        "startScrubbing",
        "y",
        "Companion",
        "DataDrawRegion",
        "ScrubbingListener",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final AUTO_X_LABEL_FREQUENCY:I = -0x1

.field public static final Companion:Lcom/squareup/chartography/ChartView$Companion;

.field public static final NO_SELECTION:I = -0x1


# instance fields
.field private adapter:Lcom/squareup/chartography/ChartAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/chartography/ChartAdapter<",
            "*>;"
        }
    .end annotation
.end field

.field private final axisPaint:Landroid/graphics/Paint;

.field private colorWheel:Lcom/squareup/chartography/util/ColorWheel;

.field private dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

.field private final headerSpacing:F

.field private hideFirstYLabel:Z

.field private final initialScrubRegion:F

.field private final pixel:Lcom/squareup/chartography/util/Pixel;

.field private rangeScalingFactor:F

.field private scrubbingListener:Lcom/squareup/chartography/ChartView$ScrubbingListener;

.field private scrubbingLock:Z

.field private selectedColorWheel:Lcom/squareup/chartography/util/ColorWheel;

.field private selectedDomain:I

.field private final textPaint:Landroid/graphics/Paint;

.field private tickSize:F

.field private touchOrigin:Landroid/graphics/PointF;

.field private xLabelFrquency:I

.field private xLabelMinGap:F

.field private xLabelSize:F

.field private xLabelTickSpacing:F

.field private yLabelSize:F

.field private yLabelSpacing:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/chartography/ChartView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/chartography/ChartView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/chartography/ChartView;->Companion:Lcom/squareup/chartography/ChartView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/chartography/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/chartography/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 16

    move-object/from16 v1, p0

    const-string v0, "context"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct/range {p0 .. p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Lcom/squareup/chartography/EmptyAdapter;

    invoke-direct {v0}, Lcom/squareup/chartography/EmptyAdapter;-><init>()V

    check-cast v0, Lcom/squareup/chartography/ChartAdapter;

    iput-object v0, v1, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    const/high16 v0, 0x41000000    # 8.0f

    .line 99
    iput v0, v1, Lcom/squareup/chartography/ChartView;->xLabelSize:F

    .line 108
    iput v0, v1, Lcom/squareup/chartography/ChartView;->yLabelSize:F

    const/high16 v3, 0x3f800000    # 1.0f

    .line 136
    iput v3, v1, Lcom/squareup/chartography/ChartView;->rangeScalingFactor:F

    const/4 v3, -0x1

    .line 147
    iput v3, v1, Lcom/squareup/chartography/ChartView;->selectedDomain:I

    .line 161
    new-instance v3, Lcom/squareup/chartography/util/Pixel;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "context.resources"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/squareup/chartography/util/Pixel;-><init>(Landroid/content/res/Resources;)V

    iput-object v3, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    .line 176
    new-instance v3, Lcom/squareup/chartography/ChartView$DataDrawRegion;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x40

    const/4 v15, 0x0

    move-object v6, v3

    invoke-direct/range {v6 .. v15}, Lcom/squareup/chartography/ChartView$DataDrawRegion;-><init>(FFFFFFFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v3, v1, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    .line 177
    new-instance v3, Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/graphics/Paint;-><init>(I)V

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iput-object v3, v1, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    .line 178
    new-instance v3, Landroid/graphics/Paint;

    const/16 v5, 0x81

    invoke-direct {v3, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/chartography/ChartView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v5, v6}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 178
    iput-object v3, v1, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    .line 196
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 198
    sget-object v3, Lcom/squareup/chartography/R$styleable;->ChartView:[I

    const/4 v5, 0x0

    move-object/from16 v6, p2

    move/from16 v7, p3

    .line 196
    invoke-virtual {v2, v6, v3, v7, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 203
    :try_start_0
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_xLabelSize:I

    iget-object v6, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    invoke-virtual {v6, v0}, Lcom/squareup/chartography/util/Pixel;->sp(F)F

    move-result v6

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/squareup/chartography/ChartView;->setXLabelSize(F)V

    .line 204
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_yLabelSize:I

    iget-object v6, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    invoke-virtual {v6, v0}, Lcom/squareup/chartography/util/Pixel;->sp(F)F

    move-result v6

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/squareup/chartography/ChartView;->setYLabelSize(F)V

    .line 205
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_textColor:I

    const v6, -0x777778

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-static {v3}, Lcom/squareup/chartography/util/ColorResource;->constructor-impl(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/squareup/chartography/ChartView;->setTextColor-OeUeViQ(I)V

    .line 206
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_axisColor:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-static {v3}, Lcom/squareup/chartography/util/ColorResource;->constructor-impl(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/squareup/chartography/ChartView;->setAxisColor-OeUeViQ(I)V

    .line 208
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_axisStrokeWidth:I

    const/4 v6, 0x0

    .line 207
    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/squareup/chartography/ChartView;->setAxisStrokeWidth(F)V

    .line 210
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_tickSize:I

    iget-object v6, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    const/high16 v7, 0x40c00000    # 6.0f

    invoke-virtual {v6, v7}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v6

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, v1, Lcom/squareup/chartography/ChartView;->tickSize:F

    .line 211
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_xLabelTickSpacing:I

    iget-object v6, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-virtual {v6, v7}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v6

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, v1, Lcom/squareup/chartography/ChartView;->xLabelTickSpacing:F

    .line 212
    sget v3, Lcom/squareup/chartography/R$styleable;->ChartView_xLabelMinGap:I

    iget-object v6, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    invoke-virtual {v6, v0}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, v1, Lcom/squareup/chartography/ChartView;->xLabelMinGap:F

    .line 213
    sget v0, Lcom/squareup/chartography/R$styleable;->ChartView_xLabelFrequency:I

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, v1, Lcom/squareup/chartography/ChartView;->xLabelFrquency:I

    .line 214
    sget v0, Lcom/squareup/chartography/R$styleable;->ChartView_hideFirstYLabel:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, v1, Lcom/squareup/chartography/ChartView;->hideFirstYLabel:Z

    .line 215
    sget v0, Lcom/squareup/chartography/R$styleable;->ChartView_yLabelSpacing:I

    iget-object v3, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    invoke-virtual {v3, v7}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/chartography/ChartView;->setYLabelSpacing(F)V

    const-string v0, "this"

    .line 218
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/chartography/R$styleable;->ChartView_dataColors:I

    sget v3, Lcom/squareup/chartography/R$array;->defaultChartViewColors:I

    .line 217
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/chartography/ChartView;->loadColorWheel(Landroid/content/res/TypedArray;II)Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/chartography/ChartView;->colorWheel:Lcom/squareup/chartography/util/ColorWheel;

    .line 221
    sget v0, Lcom/squareup/chartography/R$styleable;->ChartView_dataSelectedColors:I

    .line 222
    sget v3, Lcom/squareup/chartography/R$array;->defaultChartViewSelectedColors:I

    .line 220
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/chartography/ChartView;->loadColorWheel(Landroid/content/res/TypedArray;II)Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/chartography/ChartView;->selectedColorWheel:Lcom/squareup/chartography/util/ColorWheel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 229
    iget-object v0, v1, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v0, v2}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v0

    iput v0, v1, Lcom/squareup/chartography/ChartView;->initialScrubRegion:F

    .line 230
    move-object v0, v1

    check-cast v0, Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v0}, Lcom/squareup/chartography/ChartView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :catchall_0
    move-exception v0

    .line 225
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 33
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 34
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/chartography/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final calculateDrawRegion()V
    .locals 13

    .line 339
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/chartography/ChartView;->yLabelSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 341
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getRangeNumSteps()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    .line 342
    iget-object v5, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v5, v3}, Lcom/squareup/chartography/ChartAdapter;->rangeStepValue(I)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/squareup/chartography/ChartAdapter;->rangeLabel(D)Ljava/lang/String;

    move-result-object v5

    .line 343
    iget-object v6, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/squareup/chartography/ChartView;->xLabelSize:F

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 348
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v0

    const/4 v1, 0x0

    const/4 v12, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 349
    iget-object v3, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v3, v1}, Lcom/squareup/chartography/ChartAdapter;->domainLabel(I)Ljava/lang/String;

    move-result-object v3

    .line 350
    iget-object v5, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3, v12}, Ljava/lang/Math;->max(FF)F

    move-result v12

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v1, v2}, Lcom/squareup/chartography/ChartAdapter;->domainLabel(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    const/4 v1, 0x2

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 356
    iget-object v2, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    iget v3, p0, Lcom/squareup/chartography/ChartView;->tickSize:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/squareup/chartography/ChartView;->xLabelTickSpacing:F

    add-float/2addr v2, v3

    .line 357
    iget v3, p0, Lcom/squareup/chartography/ChartView;->yLabelSpacing:F

    mul-float v3, v3, v1

    add-float/2addr v4, v3

    .line 358
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v2

    .line 359
    iget-object v5, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v5}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Lcom/squareup/chartography/ChartAdapter;->domainLabel(I)Ljava/lang/String;

    move-result-object v5

    .line 361
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v0

    sub-float/2addr v6, v4

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingLeft()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingRight()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    .line 362
    iget-object v7, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    div-float/2addr v5, v1

    sub-float v8, v6, v5

    .line 366
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getAxisStrokeWidth()F

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getHeaderSpacing()F

    move-result v2

    sub-float v9, v1, v2

    .line 368
    new-instance v1, Lcom/squareup/chartography/ChartView$DataDrawRegion;

    .line 369
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v4

    add-float v6, v2, v0

    .line 370
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingBottom()I

    move-result v0

    int-to-float v0, v0

    sub-float v7, v3, v0

    .line 373
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v0

    int-to-float v0, v0

    div-float v10, v8, v0

    .line 374
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getRangeNumSteps()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    div-float v11, v9, v0

    move-object v5, v1

    .line 368
    invoke-direct/range {v5 .. v12}, Lcom/squareup/chartography/ChartView$DataDrawRegion;-><init>(FFFFFFF)V

    iput-object v1, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    return-void
.end method

.method private final calculatedXLabelFrequency()I
    .locals 4

    .line 444
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getMaxXLabelWidth()F

    move-result v0

    iget v1, p0, Lcom/squareup/chartography/ChartView;->xLabelMinGap:F

    add-float/2addr v0, v1

    .line 446
    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v1

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    float-to-double v0, v0

    .line 449
    iget-object v2, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v2}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    :goto_0
    return v0
.end method

.method private final drawAxes(Landroid/graphics/Canvas;)V
    .locals 12

    .line 381
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    const-string v2, "textPaint.fontMetrics"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/chartography/ChartView;->scaledYLabelSize(Landroid/graphics/Paint$FontMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 382
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingLeft()I

    move-result v0

    int-to-float v6, v0

    .line 383
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getY()F

    move-result v0

    .line 385
    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartAdapter;->getRangeNumSteps()I

    move-result v7

    const/4 v8, 0x0

    move v10, v0

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v7, :cond_2

    .line 386
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    sub-float v3, v0, v1

    iget-object v5, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v6

    move v2, v10

    move v4, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 387
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0, v9}, Lcom/squareup/chartography/ChartAdapter;->rangeStepValue(I)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/chartography/ChartAdapter;->rangeLabel(D)Ljava/lang/String;

    move-result-object v0

    if-nez v9, :cond_0

    .line 388
    iget-boolean v1, p0, Lcom/squareup/chartography/ChartView;->hideFirstYLabel:Z

    if-nez v1, :cond_1

    .line 390
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/squareup/chartography/ChartView;->yLabelSpacing:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    add-float/2addr v2, v10

    iget v3, p0, Lcom/squareup/chartography/ChartView;->yLabelSpacing:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    .line 389
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getYStep()F

    move-result v0

    sub-float/2addr v10, v0

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 397
    :cond_2
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/chartography/ChartView;->xLabelSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 398
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v1

    const/4 v2, 0x2

    int-to-float v6, v2

    div-float/2addr v1, v6

    add-float/2addr v0, v1

    .line 399
    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getY()F

    move-result v7

    .line 402
    iget v1, p0, Lcom/squareup/chartography/ChartView;->xLabelFrquency:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    invoke-direct {p0}, Lcom/squareup/chartography/ChartView;->calculatedXLabelFrequency()I

    move-result v1

    :cond_3
    move v9, v1

    .line 403
    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v10

    move v11, v0

    :goto_1
    if-ge v8, v10, :cond_5

    .line 404
    iget v0, p0, Lcom/squareup/chartography/ChartView;->tickSize:F

    add-float v4, v7, v0

    iget-object v5, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v11

    move v2, v7

    move v3, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 405
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0, v8}, Lcom/squareup/chartography/ChartAdapter;->domainLabel(I)Ljava/lang/String;

    move-result-object v0

    .line 406
    rem-int v1, v8, v9

    if-nez v1, :cond_4

    .line 408
    iget-object v1, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    div-float/2addr v1, v6

    sub-float v1, v11, v1

    .line 409
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getPaddingBottom()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float/2addr v2, v3

    .line 410
    iget-object v3, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    .line 407
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 413
    :cond_4
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v0

    add-float/2addr v11, v0

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_5
    return-void
.end method

.method private final endScrubbing()V
    .locals 2

    const/4 v0, 0x0

    .line 319
    check-cast v0, Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/squareup/chartography/ChartView;->touchOrigin:Landroid/graphics/PointF;

    .line 320
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    const/4 v0, -0x1

    .line 321
    invoke-virtual {p0, v0}, Lcom/squareup/chartography/ChartView;->setSelectedDomain(I)V

    .line 322
    iput-boolean v1, p0, Lcom/squareup/chartography/ChartView;->scrubbingLock:Z

    return-void
.end method

.method private final getDescentPercentage(Landroid/graphics/Paint$FontMetrics;)F
    .locals 2

    .line 437
    iget v0, p1, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v1, p1, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget p1, p1, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v1, p1

    div-float/2addr v0, v1

    return v0
.end method

.method private final loadColorWheel(Landroid/content/res/TypedArray;II)Lcom/squareup/chartography/util/ColorWheel;
    .locals 3

    .line 238
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    .line 239
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object p1

    const-string p2, "resources.getIntArray(colorsId)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 488
    new-instance p2, Ljava/util/ArrayList;

    array-length p3, p1

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 489
    array-length p3, p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p3, :cond_0

    aget v2, p1, v1

    .line 240
    invoke-static {v2}, Lcom/squareup/chartography/util/ColorResource;->constructor-impl(I)I

    move-result v2

    invoke-static {v2}, Lcom/squareup/chartography/util/ColorResource;->box-impl(I)Lcom/squareup/chartography/util/ColorResource;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 491
    :cond_0
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/util/Collection;

    new-array p1, v0, [Lcom/squareup/chartography/util/ColorResource;

    .line 493
    invoke-interface {p2, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, [Lcom/squareup/chartography/util/ColorResource;

    .line 240
    new-instance p2, Lcom/squareup/chartography/util/ColorWheel;

    invoke-direct {p2, p1}, Lcom/squareup/chartography/util/ColorWheel;-><init>([Lcom/squareup/chartography/util/ColorResource;)V

    return-object p2

    .line 493
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final scaledYLabelSize(Landroid/graphics/Paint$FontMetrics;)F
    .locals 3

    .line 421
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getYStep()F

    move-result v0

    iget v1, p0, Lcom/squareup/chartography/ChartView;->yLabelSpacing:F

    const/4 v2, 0x2

    int-to-float v2, v2

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    .line 424
    iget v1, p0, Lcom/squareup/chartography/ChartView;->yLabelSize:F

    invoke-direct {p0, p1}, Lcom/squareup/chartography/ChartView;->getDescentPercentage(Landroid/graphics/Paint$FontMetrics;)F

    move-result v2

    mul-float v2, v2, v1

    add-float/2addr v1, v2

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    .line 427
    invoke-direct {p0, p1}, Lcom/squareup/chartography/ChartView;->getDescentPercentage(Landroid/graphics/Paint$FontMetrics;)F

    move-result p1

    mul-float p1, p1, v0

    sub-float/2addr v0, p1

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto :goto_0

    .line 429
    :cond_0
    iget p1, p0, Lcom/squareup/chartography/ChartView;->yLabelSize:F

    :goto_0
    return p1
.end method

.method private final selectDomainAtPosition(F)V
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v0

    div-float/2addr p1, v0

    float-to-int p1, p1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v0

    if-lt p1, v0, :cond_1

    iget-object p1, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {p1}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    .line 335
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/chartography/ChartView;->setSelectedDomain(I)V

    return-void
.end method

.method private final startScrubbing(FF)V
    .locals 1

    .line 313
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/squareup/chartography/ChartView;->touchOrigin:Landroid/graphics/PointF;

    .line 314
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 315
    :cond_0
    iget-object p2, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {p2}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result p2

    sub-float/2addr p1, p2

    invoke-direct {p0, p1}, Lcom/squareup/chartography/ChartView;->selectDomainAtPosition(F)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 271
    invoke-direct {p0, p1}, Lcom/squareup/chartography/ChartView;->drawAxes(Landroid/graphics/Canvas;)V

    .line 272
    invoke-virtual {p0, p1}, Lcom/squareup/chartography/ChartView;->onDrawData(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final getAdapter()Lcom/squareup/chartography/ChartAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/chartography/ChartAdapter<",
            "*>;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    return-object v0
.end method

.method public final getAxisColor()I
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/chartography/util/ColorResource;->constructor-impl(I)I

    move-result v0

    return v0
.end method

.method protected final getAxisPaint()Landroid/graphics/Paint;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public final getAxisStrokeWidth()F
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public final getColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->colorWheel:Lcom/squareup/chartography/util/ColorWheel;

    return-object v0
.end method

.method protected final getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    return-object v0
.end method

.method protected getHeaderSpacing()F
    .locals 1

    .line 456
    iget v0, p0, Lcom/squareup/chartography/ChartView;->headerSpacing:F

    return v0
.end method

.method public final getHideFirstYLabel()Z
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/squareup/chartography/ChartView;->hideFirstYLabel:Z

    return v0
.end method

.method public final getPixel$public_release()Lcom/squareup/chartography/util/Pixel;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->pixel:Lcom/squareup/chartography/util/Pixel;

    return-object v0
.end method

.method public final getRangeScalingFactor()F
    .locals 1

    .line 136
    iget v0, p0, Lcom/squareup/chartography/ChartView;->rangeScalingFactor:F

    return v0
.end method

.method public final getScrubbingListener()Lcom/squareup/chartography/ChartView$ScrubbingListener;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->scrubbingListener:Lcom/squareup/chartography/ChartView$ScrubbingListener;

    return-object v0
.end method

.method public final getSelectedColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->selectedColorWheel:Lcom/squareup/chartography/util/ColorWheel;

    return-object v0
.end method

.method public final getSelectedDomain()I
    .locals 1

    .line 147
    iget v0, p0, Lcom/squareup/chartography/ChartView;->selectedDomain:I

    return v0
.end method

.method public final getTextColor()I
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/chartography/util/ColorResource;->constructor-impl(I)I

    move-result v0

    return v0
.end method

.method protected final getTextPaint()Landroid/graphics/Paint;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public final getTickSize()F
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/chartography/ChartView;->tickSize:F

    return v0
.end method

.method public final getXLabelFrquency()I
    .locals 1

    .line 82
    iget v0, p0, Lcom/squareup/chartography/ChartView;->xLabelFrquency:I

    return v0
.end method

.method public final getXLabelMinGap()F
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/chartography/ChartView;->xLabelMinGap:F

    return v0
.end method

.method public final getXLabelSize()F
    .locals 1

    .line 99
    iget v0, p0, Lcom/squareup/chartography/ChartView;->xLabelSize:F

    return v0
.end method

.method public final getXLabelTickSpacing()F
    .locals 1

    .line 59
    iget v0, p0, Lcom/squareup/chartography/ChartView;->xLabelTickSpacing:F

    return v0
.end method

.method public final getYLabelSize()F
    .locals 1

    .line 108
    iget v0, p0, Lcom/squareup/chartography/ChartView;->yLabelSize:F

    return v0
.end method

.method public final getYLabelSpacing()F
    .locals 1

    .line 69
    iget v0, p0, Lcom/squareup/chartography/ChartView;->yLabelSpacing:F

    return v0
.end method

.method protected abstract onDrawData(Landroid/graphics/Canvas;)V
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 265
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 266
    invoke-direct {p0}, Lcom/squareup/chartography/ChartView;->calculateDrawRegion()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .line 247
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 248
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 253
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 252
    invoke-virtual {p0, v0, v1}, Lcom/squareup/chartography/ChartView;->setMeasuredDimension(II)V

    .line 255
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .line 287
    iget-object p1, p0, Lcom/squareup/chartography/ChartView;->touchOrigin:Landroid/graphics/PointF;

    if-eqz p2, :cond_0

    .line 288
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-nez v0, :cond_1

    goto :goto_1

    .line 289
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/chartography/ChartView;->startScrubbing(FF)V

    goto :goto_5

    :cond_2
    :goto_1
    const/4 v2, 0x2

    if-nez v0, :cond_3

    goto :goto_2

    .line 290
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v2, :cond_6

    if-eqz p1, :cond_a

    .line 292
    iget-boolean v0, p0, Lcom/squareup/chartography/ChartView;->scrubbingLock:Z

    if-nez v0, :cond_4

    iget v0, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/squareup/chartography/ChartView;->initialScrubRegion:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    .line 293
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 296
    :cond_4
    iget p1, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sub-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    iget v0, p0, Lcom/squareup/chartography/ChartView;->initialScrubRegion:F

    cmpl-float p1, p1, v0

    if-lez p1, :cond_5

    .line 297
    iput-boolean v1, p0, Lcom/squareup/chartography/ChartView;->scrubbingLock:Z

    .line 300
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iget-object p2, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    invoke-virtual {p2}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result p2

    sub-float/2addr p1, p2

    invoke-direct {p0, p1}, Lcom/squareup/chartography/ChartView;->selectDomainAtPosition(F)V

    goto :goto_5

    :cond_6
    :goto_2
    if-nez v0, :cond_7

    goto :goto_3

    .line 303
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v1, :cond_8

    goto :goto_4

    :cond_8
    :goto_3
    const/4 p1, 0x3

    if-nez v0, :cond_9

    goto :goto_5

    .line 304
    :cond_9
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    if-ne p2, p1, :cond_a

    :goto_4
    invoke-direct {p0}, Lcom/squareup/chartography/ChartView;->endScrubbing()V

    :cond_a
    :goto_5
    return v1
.end method

.method public final setAdapter(Lcom/squareup/chartography/ChartAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/chartography/ChartAdapter<",
            "*>;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->requestLayout()V

    return-void
.end method

.method public final setAxisColor-OeUeViQ(I)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    invoke-static {v0, p1}, Lcom/squareup/chartography/extensions/PaintKt;->setColorResource-9Ee6Eao(Landroid/graphics/Paint;I)V

    return-void
.end method

.method public final setAxisStrokeWidth(F)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->axisPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 130
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->requestLayout()V

    return-void
.end method

.method public final setColorWheel$public_release(Lcom/squareup/chartography/util/ColorWheel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    iput-object p1, p0, Lcom/squareup/chartography/ChartView;->colorWheel:Lcom/squareup/chartography/util/ColorWheel;

    return-void
.end method

.method protected final setDataDrawRegion(Lcom/squareup/chartography/ChartView$DataDrawRegion;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iput-object p1, p0, Lcom/squareup/chartography/ChartView;->dataDrawRegion:Lcom/squareup/chartography/ChartView$DataDrawRegion;

    return-void
.end method

.method public final setHideFirstYLabel(Z)V
    .locals 0

    .line 86
    iput-boolean p1, p0, Lcom/squareup/chartography/ChartView;->hideFirstYLabel:Z

    return-void
.end method

.method public final setRangeScalingFactor(F)V
    .locals 0

    .line 138
    iput p1, p0, Lcom/squareup/chartography/ChartView;->rangeScalingFactor:F

    .line 139
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->invalidate()V

    return-void
.end method

.method public final setScrubbingListener(Lcom/squareup/chartography/ChartView$ScrubbingListener;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/chartography/ChartView;->scrubbingListener:Lcom/squareup/chartography/ChartView$ScrubbingListener;

    return-void
.end method

.method public final setSelectedColorWheel$public_release(Lcom/squareup/chartography/util/ColorWheel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iput-object p1, p0, Lcom/squareup/chartography/ChartView;->selectedColorWheel:Lcom/squareup/chartography/util/ColorWheel;

    return-void
.end method

.method public final setSelectedDomain(I)V
    .locals 1

    .line 149
    iget v0, p0, Lcom/squareup/chartography/ChartView;->selectedDomain:I

    if-eq p1, v0, :cond_3

    .line 150
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->adapter:Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v0

    if-gez p1, :cond_0

    goto :goto_0

    :cond_0
    if-le v0, p1, :cond_1

    .line 151
    iput p1, p0, Lcom/squareup/chartography/ChartView;->selectedDomain:I

    .line 152
    iget-object p1, p0, Lcom/squareup/chartography/ChartView;->scrubbingListener:Lcom/squareup/chartography/ChartView$ScrubbingListener;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/squareup/chartography/ChartView;->selectedDomain:I

    invoke-interface {p1, v0}, Lcom/squareup/chartography/ChartView$ScrubbingListener;->onScrub(I)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    .line 154
    iput p1, p0, Lcom/squareup/chartography/ChartView;->selectedDomain:I

    .line 155
    iget-object p1, p0, Lcom/squareup/chartography/ChartView;->scrubbingListener:Lcom/squareup/chartography/ChartView$ScrubbingListener;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/squareup/chartography/ChartView$ScrubbingListener;->onScrubEnd()V

    .line 157
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->invalidate()V

    :cond_3
    return-void
.end method

.method public final setTextColor-OeUeViQ(I)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/chartography/ChartView;->textPaint:Landroid/graphics/Paint;

    invoke-static {v0, p1}, Lcom/squareup/chartography/extensions/PaintKt;->setColorResource-9Ee6Eao(Landroid/graphics/Paint;I)V

    return-void
.end method

.method public final setTickSize(F)V
    .locals 0

    .line 54
    iput p1, p0, Lcom/squareup/chartography/ChartView;->tickSize:F

    return-void
.end method

.method public final setXLabelFrquency(I)V
    .locals 0

    .line 82
    iput p1, p0, Lcom/squareup/chartography/ChartView;->xLabelFrquency:I

    return-void
.end method

.method public final setXLabelMinGap(F)V
    .locals 0

    .line 64
    iput p1, p0, Lcom/squareup/chartography/ChartView;->xLabelMinGap:F

    return-void
.end method

.method public final setXLabelSize(F)V
    .locals 0

    .line 101
    iput p1, p0, Lcom/squareup/chartography/ChartView;->xLabelSize:F

    .line 102
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->requestLayout()V

    return-void
.end method

.method public final setXLabelTickSpacing(F)V
    .locals 0

    .line 59
    iput p1, p0, Lcom/squareup/chartography/ChartView;->xLabelTickSpacing:F

    return-void
.end method

.method public final setYLabelSize(F)V
    .locals 0

    .line 110
    iput p1, p0, Lcom/squareup/chartography/ChartView;->yLabelSize:F

    .line 111
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->requestLayout()V

    return-void
.end method

.method public final setYLabelSpacing(F)V
    .locals 0

    .line 71
    iput p1, p0, Lcom/squareup/chartography/ChartView;->yLabelSpacing:F

    .line 72
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->requestLayout()V

    return-void
.end method
