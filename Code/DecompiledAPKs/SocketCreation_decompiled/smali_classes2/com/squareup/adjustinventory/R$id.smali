.class public final Lcom/squareup/adjustinventory/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/adjustinventory/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final adjust_inventory_select_reason_help_text:I = 0x7f0a018b

.field public static final adjust_stock_container:I = 0x7f0a0192

.field public static final adjust_stock_reason_section:I = 0x7f0a0193

.field public static final adjust_stock_specify_number_field:I = 0x7f0a0194

.field public static final adjust_stock_specify_number_help_text_inventory_tracking:I = 0x7f0a0195

.field public static final adjust_stock_specify_number_help_text_stock_count:I = 0x7f0a0196

.field public static final adjust_stock_unit_cost_field:I = 0x7f0a0197

.field public static final inventory_adjustment_save_spinner:I = 0x7f0a086e

.field public static final inventory_adjustment_save_spinner_text:I = 0x7f0a086f

.field public static final reason_list:I = 0x7f0a0cfb

.field public static final save_spinner_container:I = 0x7f0a0e06


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
