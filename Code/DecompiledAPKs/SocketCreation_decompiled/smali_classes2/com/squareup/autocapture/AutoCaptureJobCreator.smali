.class public Lcom/squareup/autocapture/AutoCaptureJobCreator;
.super Lcom/squareup/backgroundjob/BackgroundJobCreator;
.source "AutoCaptureJobCreator.java"


# instance fields
.field private final autoCaptureJobProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJob;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/backgroundjob/BackgroundJobManager;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJob;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/backgroundjob/BackgroundJobCreator;-><init>(Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 25
    iput-object p2, p0, Lcom/squareup/autocapture/AutoCaptureJobCreator;->autoCaptureJobProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->QUICK_AUTO_CAPTURE_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/autocapture/AutoCaptureJob;->SLOW_AUTO_CAPTURE_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return-object p1

    .line 30
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJobCreator;->autoCaptureJobProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/android/job/Job;

    return-object p1
.end method
