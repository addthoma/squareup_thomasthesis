.class Lcom/squareup/autocapture/AutoCaptureJob$KeyMismatch;
.super Ljava/lang/RuntimeException;
.source "AutoCaptureJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/autocapture/AutoCaptureJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyMismatch"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/autocapture/AutoCaptureJob;


# direct methods
.method private constructor <init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 172
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJob$KeyMismatch;->this$0:Lcom/squareup/autocapture/AutoCaptureJob;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 174
    invoke-static {p1}, Lcom/squareup/autocapture/AutoCaptureJob;->access$200(Lcom/squareup/autocapture/AutoCaptureJob;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const/4 p1, 0x2

    aput-object p3, v0, p1

    const-string p1, "%s auto capture of %s failed, found %s in cart"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 173
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureJob$1;)V
    .locals 0

    .line 171
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/autocapture/AutoCaptureJob$KeyMismatch;-><init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
