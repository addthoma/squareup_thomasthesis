.class public final Lcom/squareup/autocapture/AutoCaptureJob_Factory;
.super Ljava/lang/Object;
.source "AutoCaptureJob_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/autocapture/AutoCaptureJob;",
        ">;"
    }
.end annotation


# instance fields
.field private final appDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final autoCaptureControlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;"
        }
    .end annotation
.end field

.field private final autoVoidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;"
        }
    .end annotation
.end field

.field private final backgroundJobNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->busProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->backgroundJobNotificationManagerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->autoVoidProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/autocapture/AutoCaptureJob_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;)",
            "Lcom/squareup/autocapture/AutoCaptureJob_Factory;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/autocapture/AutoCaptureJob_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/autocapture/AutoCaptureJob_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/payment/AutoVoid;Ldagger/Lazy;Lcom/squareup/AppDelegate;)Lcom/squareup/autocapture/AutoCaptureJob;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            "Lcom/squareup/payment/AutoVoid;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Lcom/squareup/AppDelegate;",
            ")",
            "Lcom/squareup/autocapture/AutoCaptureJob;"
        }
    .end annotation

    .line 80
    new-instance v10, Lcom/squareup/autocapture/AutoCaptureJob;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/autocapture/AutoCaptureJob;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/payment/AutoVoid;Ldagger/Lazy;Lcom/squareup/AppDelegate;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/autocapture/AutoCaptureJob;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->backgroundJobNotificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/autocapture/AutoCaptureControl;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->autoVoidProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/AutoVoid;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v8

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/AppDelegate;

    invoke-static/range {v1 .. v9}, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/payment/AutoVoid;Ldagger/Lazy;Lcom/squareup/AppDelegate;)Lcom/squareup/autocapture/AutoCaptureJob;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureJob_Factory;->get()Lcom/squareup/autocapture/AutoCaptureJob;

    move-result-object v0

    return-object v0
.end method
