.class public final Lcom/squareup/bugreport/DefaultVeryLargeTelescopeLayoutConfig;
.super Ljava/lang/Object;
.source "DefaultVeryLargeTelescopeLayoutConfig.kt"

# interfaces
.implements Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/bugreport/DefaultVeryLargeTelescopeLayoutConfig;",
        "Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;",
        "twoFingerBugReporter",
        "Lcom/squareup/bugreport/TwoFingerBugReporter;",
        "(Lcom/squareup/bugreport/TwoFingerBugReporter;)V",
        "onCapture",
        "",
        "activity",
        "Landroid/app/Activity;",
        "screenshot",
        "Ljava/io/File;",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final twoFingerBugReporter:Lcom/squareup/bugreport/TwoFingerBugReporter;


# direct methods
.method public constructor <init>(Lcom/squareup/bugreport/TwoFingerBugReporter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "twoFingerBugReporter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/bugreport/DefaultVeryLargeTelescopeLayoutConfig;->twoFingerBugReporter:Lcom/squareup/bugreport/TwoFingerBugReporter;

    return-void
.end method


# virtual methods
.method public getMode()Lcom/mattprecious/telescope/ScreenshotMode;
    .locals 1

    .line 7
    invoke-static {p0}, Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig$DefaultImpls;->getMode(Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;)Lcom/mattprecious/telescope/ScreenshotMode;

    move-result-object v0

    return-object v0
.end method

.method public onCapture(Landroid/app/Activity;Ljava/io/File;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/squareup/bugreport/DefaultVeryLargeTelescopeLayoutConfig;->twoFingerBugReporter:Lcom/squareup/bugreport/TwoFingerBugReporter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/bugreport/TwoFingerBugReporter;->showDialogAndReport(Landroid/app/Activity;Ljava/io/File;)V

    return-void
.end method
