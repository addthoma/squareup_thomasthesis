.class final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;
.super Ljava/lang/Object;
.source "AccessiblePinTutorialDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "wrapper",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;

    iput-object p2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 4

    const-string/jumbo v0, "wrapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;

    .line 44
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;

    invoke-static {v0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;->access$getBuyerLocaleOverride$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;)Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->exit_tutorial_dialog_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 48
    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;

    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->exit_tutorial_dialog_title:I

    invoke-static {v2, v3}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;->access$textForAccessibility(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitleContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 49
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->exit_tutorial_dialog_message:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 51
    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;

    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->exit_tutorial_dialog_message:I

    invoke-static {v2, v3}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;->access$textForAccessibility(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory;I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 50
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessageContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 53
    sget v2, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 54
    sget v2, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 55
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->skip:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    new-instance v3, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1$1;

    invoke-direct {v3, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1$1;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 58
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->go_back:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v2, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1$2;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 61
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
