.class public interface abstract Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;
.super Ljava/lang/Object;
.source "CheckoutLinkShareSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000cJ0\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
        "",
        "openShareSheetForCheckoutLink",
        "",
        "context",
        "Landroid/content/Context;",
        "shareSheetTitle",
        "",
        "subject",
        "checkoutLink",
        "shareContext",
        "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;",
        "ShareContext",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract openShareSheetForCheckoutLink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;)V
.end method
