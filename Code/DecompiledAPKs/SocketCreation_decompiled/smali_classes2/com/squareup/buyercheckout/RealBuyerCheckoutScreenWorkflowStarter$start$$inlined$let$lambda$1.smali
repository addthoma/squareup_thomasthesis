.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "RealBuyerCheckoutScreenWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->start(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0002j\u0002`\u00030\u00012\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\u008a@\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ScreenState;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "it",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$2$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/Workflow;

.field final synthetic $workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

.field label:I

.field private p$0:Lcom/squareup/buyercheckout/BuyerCheckoutState;

.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/coroutines/Continuation;Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;Lcom/squareup/workflow/legacy/WorkflowPool;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;

    iput-object p4, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->$workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;

    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;

    iget-object v3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->$workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/coroutines/Continuation;Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    iput-object p1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->p$0:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 36
    iget v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->p$0:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 37
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    .line 38
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->access$getBuyerCheckoutRenderer$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;)Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;

    move-result-object v1

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowInput;

    iget-object v3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;->$workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-interface {v1, p1, v2, v3}, Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object v1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 37
    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0

    .line 40
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
