.class public final Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;
.super Ljava/lang/Object;
.source "BuyerCheckoutScreenKeys.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u0007H\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;",
        "",
        "()V",
        "isInBuyerCheckoutWorkflow",
        "",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;

    invoke-direct {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;-><init>()V

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isInBuyerCheckoutWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "key"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCart;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCart;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCart;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 16
    :cond_0
    sget-object v0, Lcom/squareup/buyercheckout/PaymentPrompt;->INSTANCE:Lcom/squareup/buyercheckout/PaymentPrompt;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/PaymentPrompt;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 17
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 18
    :cond_2
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipInputScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    :goto_0
    const/4 p0, 0x1

    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    :goto_1
    return p0
.end method
