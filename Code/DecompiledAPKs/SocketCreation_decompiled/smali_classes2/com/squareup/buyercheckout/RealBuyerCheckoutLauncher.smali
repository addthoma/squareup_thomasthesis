.class public final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBuyerCheckoutLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/RealBuyerCheckoutLauncher\n+ 2 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,641:1\n357#2:642\n402#2:643\n357#2:644\n402#2:645\n1529#3,3:646\n704#3:649\n777#3,2:650\n*E\n*S KotlinDebug\n*F\n+ 1 RealBuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/RealBuyerCheckoutLauncher\n*L\n137#1:642\n137#1:643\n138#1:644\n138#1:645\n617#1,3:646\n622#1:649\n622#1,2:650\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001DB\u00a5\u0001\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u0012\u0006\u0010!\u001a\u00020\"\u0012\u0006\u0010#\u001a\u00020$\u0012\u0006\u0010%\u001a\u00020&\u0012\u0006\u0010\'\u001a\u00020(\u00a2\u0006\u0002\u0010)J.\u00109\u001a\u0018\u0012\u0004\u0012\u00020;\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020=0:j\u0002`>2\u0006\u0010?\u001a\u00020;2\u0006\u0010@\u001a\u00020AH\u0016J\u0008\u0010B\u001a\u00020CH\u0007R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000RH\u0010*\u001a<\u0012(\u0012&\u0012\u0004\u0012\u00020-\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020/\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u000300j\u0002`10.0,\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u0002020+j\u0002`3X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R^\u00104\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020-\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u000205000,\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u0002060+j&\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u000206\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020500j\u0006\u0012\u0002\u0008\u0003`8`7X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006E"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
        "paymentInputHandlerProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "tenderCompleter",
        "Lcom/squareup/tenderpayment/TenderCompleter;",
        "customerCheckoutSettings",
        "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "buyerCartFormatter",
        "Lcom/squareup/ui/cart/BuyerCartFormatter;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "statusBarEventManager",
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "paymentHudToaster",
        "Lcom/squareup/payment/PaymentHudToaster;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "billTipWorkflow",
        "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "swipeValidator",
        "Lcom/squareup/swipe/SwipeValidator;",
        "buyerLanguageSelectionWorkflow",
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
        "(Ljavax/inject/Provider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;)V",
        "buyerLanguageSelectionLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/buyer/language/Exit;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionLegacyLauncher;",
        "tipLauncher",
        "",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutWorkflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "onlyHaveOneCustomItemInCart",
        "",
        "BuyerCheckoutReactor",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

.field private final buyerLanguageSelectionLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lkotlin/Unit;",
            "Lcom/squareup/buyer/language/Exit;",
            ">;"
        }
    .end annotation
.end field

.field private final customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

.field private final paymentInputHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

.field private final swipeValidator:Lcom/squareup/swipe/SwipeValidator;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tipLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/cart/BuyerCartFormatter;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/payment/PaymentHudToaster;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/swipe/SwipeValidator;",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p17

    const-string v0, "paymentInputHandlerProvider"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderCompleter"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerCheckoutSettings"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCartFormatter"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statusBarEventManager"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialCore"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentHudToaster"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hudToaster"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billTipWorkflow"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeValidator"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLanguageSelectionWorkflow"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p17

    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    iput-object v2, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v3, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object v4, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    iput-object v5, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    iput-object v6, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object v7, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    iput-object v8, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object v9, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object v10, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    iput-object v11, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object v12, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iput-object v13, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v14, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->features:Lcom/squareup/settings/server/Features;

    iput-object v15, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    .line 130
    invoke-interface/range {p16 .. p16}, Lcom/squareup/ui/buyer/tip/BillTipWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tipLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 131
    invoke-interface/range {p19 .. p19}, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->buyerLanguageSelectionLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getBuyerCartFormatter$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/ui/cart/BuyerCartFormatter;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    return-object p0
.end method

.method public static final synthetic access$getCustomerCheckoutSettings$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/buyercheckout/CustomerCheckoutSettings;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getHudToaster$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/hudtoaster/HudToaster;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-object p0
.end method

.method public static final synthetic access$getOfflineModeMonitor$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/OfflineModeMonitor;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    return-object p0
.end method

.method public static final synthetic access$getPaymentHudToaster$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/PaymentHudToaster;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    return-object p0
.end method

.method public static final synthetic access$getPermissionGatekeeper$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/permissions/PermissionGatekeeper;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$getSwipeValidator$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/swipe/SwipeValidator;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    return-object p0
.end method

.method public static final synthetic access$getTenderCompleter$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/tenderpayment/TenderCompleter;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    return-object p0
.end method

.method public static final synthetic access$getTenderFactory$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/tender/TenderFactory;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    return-object p0
.end method

.method public static final synthetic access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$getTutorialCore$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method


# virtual methods
.method public launch(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->tipLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 643
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lkotlin/Unit;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/ui/buyer/tip/BillTipResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 642
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->buyerLanguageSelectionLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 645
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lkotlin/Unit;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/buyer/language/Exit;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 644
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    .line 144
    new-instance v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    const-string v2, "paymentInputHandler"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    .line 145
    invoke-virtual {v1, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 146
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$1;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 156
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;

    move-result-object v1

    .line 159
    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    invoke-interface {v2, v1}, Lcom/squareup/statusbar/event/StatusBarEventManager;->enterFullscreenMode(Lio/reactivex/Completable;)V

    .line 163
    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$2$paymentInputSub$1;

    invoke-direct {v3, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$2$paymentInputSub$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 168
    new-instance v3, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;

    invoke-direct {v3, v2, p0, v0, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;-><init>(Lio/reactivex/disposables/Disposable;Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v3, Lio/reactivex/functions/Action;

    invoke-virtual {v1, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 108
    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->launch(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public final onlyHaveOneCustomItemInCart()Z
    .locals 10

    .line 617
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "transaction.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 646
    instance-of v2, v0, Ljava/util/Collection;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 647
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    const-string v5, "it"

    .line 617
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    return v4

    .line 622
    :cond_3
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 649
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 650
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/checkout/CartItem;

    .line 622
    iget-object v5, v5, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    const-wide/16 v6, 0x0

    if-eqz v5, :cond_5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    goto :goto_2

    :cond_5
    move-wide v8, v6

    :goto_2
    cmp-long v5, v8, v6

    if-lez v5, :cond_6

    const/4 v5, 0x1

    goto :goto_3

    :cond_6
    const/4 v5, 0x0

    :goto_3
    if-eqz v5, :cond_4

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 651
    :cond_7
    check-cast v1, Ljava/util/List;

    .line 623
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    return v4

    .line 627
    :cond_8
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_9

    return v4

    .line 631
    :cond_9
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->single(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 632
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->hasAppliedAdditiveTaxes()Z

    move-result v1

    if-nez v1, :cond_b

    .line 633
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->hasAppliedDiscounts()Z

    move-result v0

    if-eqz v0, :cond_a

    goto :goto_4

    :cond_a
    return v3

    :cond_b
    :goto_4
    return v4
.end method
