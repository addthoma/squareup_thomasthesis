.class public final Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;
.super Ljava/lang/Object;
.source "BuyerCheckoutState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;",
        "source",
        "Lokio/BufferedSource;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSnapshot(Lokio/BufferedSource;)Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;
    .locals 2

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    .line 95
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v1

    .line 96
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 94
    invoke-direct {v0, v1, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;-><init>(ZZ)V

    return-object v0
.end method
