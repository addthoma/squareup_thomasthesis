.class public final Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutV2Workflow.kt"

# interfaces
.implements Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J2\u0010\u000e\u001a,\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\u0008H\u0016R8\u0010\u0007\u001a,\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
        "buyerCheckoutLauncher",
        "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
        "buyerCheckoutRenderer",
        "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;",
        "(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)V",
        "workflow",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "asStatefulWorkflow",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final workflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerCheckoutLauncher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCheckoutRenderer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 14
    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    .line 15
    sget-object v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow$workflow$1;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow$workflow$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 16
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow$workflow$2;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow$workflow$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 13
    invoke-static {p1, p2, v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt;->asV2Workflow(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;->workflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-void
.end method


# virtual methods
.method public bridge synthetic asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;->asStatefulWorkflow()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object v0
.end method

.method public asStatefulWorkflow()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;->workflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-object v0
.end method
