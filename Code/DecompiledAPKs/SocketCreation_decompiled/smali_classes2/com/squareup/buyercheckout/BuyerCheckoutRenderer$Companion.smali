.class public final Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;
.super Ljava/lang/Object;
.source "BuyerCheckoutRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;",
        "",
        "()V",
        "isRenderable",
        "",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;

    invoke-direct {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;-><init>()V

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;->$$INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutRenderer$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final isRenderable(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Z
    .locals 1

    const-string v0, "$this$isRenderable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    if-eqz p1, :cond_2

    :goto_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
