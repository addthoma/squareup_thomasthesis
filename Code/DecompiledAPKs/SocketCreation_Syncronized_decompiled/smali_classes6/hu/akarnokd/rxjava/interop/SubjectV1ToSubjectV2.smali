.class final Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;
.super Lio/reactivex/subjects/Subject;
.source "SubjectV1ToSubjectV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/subjects/Subject<",
        "TT;>;"
    }
.end annotation


# instance fields
.field error:Ljava/lang/Throwable;

.field final source:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation
.end field

.field volatile terminated:Z


# direct methods
.method constructor <init>(Lrx/subjects/Subject;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/subjects/Subject<",
            "TT;TT;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Lio/reactivex/subjects/Subject;-><init>()V

    .line 32
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->source:Lrx/subjects/Subject;

    return-void
.end method


# virtual methods
.method public getThrowable()Ljava/lang/Throwable;
    .locals 1

    .line 101
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->error:Ljava/lang/Throwable;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasComplete()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->error:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasObservers()Z
    .locals 1

    .line 86
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/subjects/Subject;->hasObservers()Z

    move-result v0

    return v0
.end method

.method public hasThrowable()Z
    .locals 1

    .line 96
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->error:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onComplete()V
    .locals 1

    .line 69
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 70
    iput-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    .line 71
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/subjects/Subject;->onCompleted()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 55
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    .line 57
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Throwable was null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 59
    :cond_0
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->error:Ljava/lang/Throwable;

    const/4 v0, 0x1

    .line 60
    iput-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    .line 61
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0, p1}, Lrx/subjects/Subject;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 63
    :cond_1
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 44
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    .line 46
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 48
    :cond_0
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0, p1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onSubscribe(Lio/reactivex/disposables/Disposable;)V
    .locals 1

    .line 37
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->terminated:Z

    if-eqz v0, :cond_0

    .line 38
    invoke-interface {p1}, Lio/reactivex/disposables/Disposable;->dispose()V

    :cond_0
    return-void
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer<",
            "-TT;>;)V"
        }
    .end annotation

    .line 77
    new-instance v0, Lhu/akarnokd/rxjava/interop/ObservableV1ToObservableV2$ObservableSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/ObservableV1ToObservableV2$ObservableSubscriber;-><init>(Lio/reactivex/Observer;)V

    .line 79
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 81
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;->source:Lrx/subjects/Subject;

    invoke-virtual {p1, v0}, Lrx/subjects/Subject;->unsafeSubscribe(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
