.class final Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;
.super Lrx/Scheduler$Worker;
.source "SchedulerV2ToSchedulerV1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "WorkerV2ToWorkerV1"
.end annotation


# instance fields
.field final v2Worker:Lio/reactivex/Scheduler$Worker;


# direct methods
.method constructor <init>(Lio/reactivex/Scheduler$Worker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lrx/Scheduler$Worker;-><init>()V

    .line 62
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    return-void
.end method


# virtual methods
.method public isUnsubscribed()Z
    .locals 1

    .line 95
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    invoke-virtual {v0}, Lio/reactivex/Scheduler$Worker;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public now()J
    .locals 2

    .line 85
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lio/reactivex/Scheduler$Worker;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public schedule(Lrx/functions/Action0;)Lrx/Subscription;
    .locals 1

    .line 67
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;-><init>(Lrx/functions/Action0;)V

    .line 68
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    invoke-virtual {p1, v0}, Lio/reactivex/Scheduler$Worker;->schedule(Ljava/lang/Runnable;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public schedule(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 1

    .line 73
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;-><init>(Lrx/functions/Action0;)V

    .line 74
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    invoke-virtual {p1, v0, p2, p3, p4}, Lio/reactivex/Scheduler$Worker;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public schedulePeriodically(Lrx/functions/Action0;JJLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 7

    .line 79
    new-instance v1, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;

    invoke-direct {v1, p1}, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;-><init>(Lrx/functions/Action0;)V

    .line 80
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lio/reactivex/Scheduler$Worker;->schedulePeriodically(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public unsubscribe()V
    .locals 1

    .line 90
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;->v2Worker:Lio/reactivex/Scheduler$Worker;

    invoke-virtual {v0}, Lio/reactivex/Scheduler$Worker;->dispose()V

    return-void
.end method
