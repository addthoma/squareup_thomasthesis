.class final Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;
.super Lio/reactivex/Scheduler$Worker;
.source "SchedulerV1ToSchedulerV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "WorkerV1ToWorkerV2"
.end annotation


# instance fields
.field final v1Worker:Lrx/Scheduler$Worker;


# direct methods
.method constructor <init>(Lrx/Scheduler$Worker;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lio/reactivex/Scheduler$Worker;-><init>()V

    .line 63
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 91
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    invoke-virtual {v0}, Lrx/Scheduler$Worker;->unsubscribe()V

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 96
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    invoke-virtual {v0}, Lrx/Scheduler$Worker;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public now(Ljava/util/concurrent/TimeUnit;)J
    .locals 3

    .line 86
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    invoke-virtual {v0}, Lrx/Scheduler$Worker;->now()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public schedule(Ljava/lang/Runnable;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 68
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;-><init>(Ljava/lang/Runnable;)V

    .line 69
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    invoke-virtual {p1, v0}, Lrx/Scheduler$Worker;->schedule(Lrx/functions/Action0;)Lrx/Subscription;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 74
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;-><init>(Ljava/lang/Runnable;)V

    .line 75
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    invoke-virtual {p1, v0, p2, p3, p4}, Lrx/Scheduler$Worker;->schedule(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public schedulePeriodically(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/disposables/Disposable;
    .locals 7

    .line 80
    new-instance v1, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;

    invoke-direct {v1, p1}, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;-><init>(Ljava/lang/Runnable;)V

    .line 81
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;->v1Worker:Lrx/Scheduler$Worker;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lrx/Scheduler$Worker;->schedulePeriodically(Lrx/functions/Action0;JJLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
