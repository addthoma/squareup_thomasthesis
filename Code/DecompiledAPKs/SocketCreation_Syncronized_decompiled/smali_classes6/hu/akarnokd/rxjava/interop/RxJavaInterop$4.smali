.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$4;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lio/reactivex/CompletableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Completable$Transformer;)Lio/reactivex/CompletableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$transformer:Lrx/Completable$Transformer;


# direct methods
.method constructor <init>(Lrx/Completable$Transformer;)V
    .locals 0

    .line 272
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$4;->val$transformer:Lrx/Completable$Transformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Completable;)Lio/reactivex/Completable;
    .locals 1

    .line 275
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$4;->val$transformer:Lrx/Completable$Transformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Completable(Lio/reactivex/CompletableSource;)Lrx/Completable;

    move-result-object p1

    invoke-interface {v0, p1}, Lrx/Completable$Transformer;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Completable;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Completable;)Lio/reactivex/CompletableSource;
    .locals 0

    .line 272
    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$4;->apply(Lio/reactivex/Completable;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method
