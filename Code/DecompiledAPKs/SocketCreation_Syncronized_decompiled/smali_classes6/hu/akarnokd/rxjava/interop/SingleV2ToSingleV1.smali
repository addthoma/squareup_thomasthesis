.class final Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1;
.super Ljava/lang/Object;
.source "SingleV2ToSingleV1.java"

# interfaces
.implements Lrx/Single$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1$SourceSingleObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Single$OnSubscribe<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final source:Lio/reactivex/SingleSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/SingleSource<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/SingleSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleSource<",
            "TT;>;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1;->source:Lio/reactivex/SingleSource;

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lrx/SingleSubscriber;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1;->call(Lrx/SingleSubscriber;)V

    return-void
.end method

.method public call(Lrx/SingleSubscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/SingleSubscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 36
    new-instance v0, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1$SourceSingleObserver;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1$SourceSingleObserver;-><init>(Lrx/SingleSubscriber;)V

    .line 37
    invoke-virtual {p1, v0}, Lrx/SingleSubscriber;->add(Lrx/Subscription;)V

    .line 38
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1;->source:Lio/reactivex/SingleSource;

    invoke-interface {p1, v0}, Lio/reactivex/SingleSource;->subscribe(Lio/reactivex/SingleObserver;)V

    return-void
.end method
