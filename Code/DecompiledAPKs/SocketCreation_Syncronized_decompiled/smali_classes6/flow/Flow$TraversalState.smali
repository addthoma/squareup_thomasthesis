.class final enum Lflow/Flow$TraversalState;
.super Ljava/lang/Enum;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TraversalState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lflow/Flow$TraversalState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lflow/Flow$TraversalState;

.field public static final enum DISPATCHED:Lflow/Flow$TraversalState;

.field public static final enum ENQUEUED:Lflow/Flow$TraversalState;

.field public static final enum FINISHED:Lflow/Flow$TraversalState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 239
    new-instance v0, Lflow/Flow$TraversalState;

    const/4 v1, 0x0

    const-string v2, "ENQUEUED"

    invoke-direct {v0, v2, v1}, Lflow/Flow$TraversalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflow/Flow$TraversalState;->ENQUEUED:Lflow/Flow$TraversalState;

    .line 244
    new-instance v0, Lflow/Flow$TraversalState;

    const/4 v2, 0x1

    const-string v3, "DISPATCHED"

    invoke-direct {v0, v3, v2}, Lflow/Flow$TraversalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflow/Flow$TraversalState;->DISPATCHED:Lflow/Flow$TraversalState;

    .line 248
    new-instance v0, Lflow/Flow$TraversalState;

    const/4 v3, 0x2

    const-string v4, "FINISHED"

    invoke-direct {v0, v4, v3}, Lflow/Flow$TraversalState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflow/Flow$TraversalState;->FINISHED:Lflow/Flow$TraversalState;

    const/4 v0, 0x3

    new-array v0, v0, [Lflow/Flow$TraversalState;

    .line 237
    sget-object v4, Lflow/Flow$TraversalState;->ENQUEUED:Lflow/Flow$TraversalState;

    aput-object v4, v0, v1

    sget-object v1, Lflow/Flow$TraversalState;->DISPATCHED:Lflow/Flow$TraversalState;

    aput-object v1, v0, v2

    sget-object v1, Lflow/Flow$TraversalState;->FINISHED:Lflow/Flow$TraversalState;

    aput-object v1, v0, v3

    sput-object v0, Lflow/Flow$TraversalState;->$VALUES:[Lflow/Flow$TraversalState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 237
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflow/Flow$TraversalState;
    .locals 1

    .line 237
    const-class v0, Lflow/Flow$TraversalState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lflow/Flow$TraversalState;

    return-object p0
.end method

.method public static values()[Lflow/Flow$TraversalState;
    .locals 1

    .line 237
    sget-object v0, Lflow/Flow$TraversalState;->$VALUES:[Lflow/Flow$TraversalState;

    invoke-virtual {v0}, [Lflow/Flow$TraversalState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflow/Flow$TraversalState;

    return-object v0
.end method
