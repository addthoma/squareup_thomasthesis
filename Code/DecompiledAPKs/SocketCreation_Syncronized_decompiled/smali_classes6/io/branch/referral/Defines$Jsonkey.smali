.class public final enum Lio/branch/referral/Defines$Jsonkey;
.super Ljava/lang/Enum;
.source "Defines.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/branch/referral/Defines;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Jsonkey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/Defines$Jsonkey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AAID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressCity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressCountry:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressPostalCode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressRegion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressStreet:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Affiliation:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Amount:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidAppLinkURL:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidDeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidPushIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidPushNotificationKey:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AppIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AppVersion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BeginAfterID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchIdentity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchKey:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchLinkUsed:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewAction:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewHtml:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewNumOfUse:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewUrl:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Branch_Instrumentation:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Branch_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Brand:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Bucket:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CalculationType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CanonicalIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CanonicalUrl:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ClickedReferrerTimeStamp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Clicked_Branch_Link:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CommerceData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Condition:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentActionView:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentAnalyticsMode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentDesc:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentDiscovery:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentEvents:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentExpiryTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentImgUrl:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentItems:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentKeyWords:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentNavPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentSchema:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentTitle:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Country:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Coupon:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CreationSource:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CreationTimestamp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Currency:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CustomData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Data:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Debug:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DefaultBucket:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Description:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeveloperIdentity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeviceFingerprintID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Direction:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Environment:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Event:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum EventData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Expiration:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum External_Intent_Extra:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum External_Intent_URI:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum FaceBookAppLinkChecked:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum FirstInstallTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ForceNewBranchSession:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum GoogleAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum GooglePlayInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum GoogleSearchInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareIDType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareIDTypeRandom:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareIDTypeVendor:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Identity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IdentityID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ImageCaptions:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstallBeginTimeStamp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstantApp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsFirstSession:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsFullAppConv:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsHardwareIDReal:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsReferrable:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LATVal:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Language:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LastUpdateTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Last_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Latitude:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Length:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LimitedAdTracking:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Link:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LinkClickID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LinkIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LocalIP:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LocallyIndexable:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Location:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Longitude:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Metadata:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Model:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Name:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum NativeApp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OS:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OSVersion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OriginalInstallTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Params:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Path:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Prefix:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PreviousUpdateTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Price:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PriceCurrency:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductBrand:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductCategory:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductName:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductVariant:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PublicallyIndexable:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Quantity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Queue_Wait_Time:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Rating:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RatingAverage:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RatingCount:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RatingMax:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferralCode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferralLink:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferringBranchIdentity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferringData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferringLink:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Revenue:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SDK:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SKU:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ScreenDpi:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ScreenHeight:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ScreenWidth:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SdkVersion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SearchQuery:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SessionID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ShareError:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SharedLink:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Shipping:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Tax:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Total:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum TrackingDisabled:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum TransactionID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Type:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UIMode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum URIScheme:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UnidentifiedDevice:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Unique:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Update:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UserAgent:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UserData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ViewList:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum WiFi:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum limitFacebookTracking:Lio/branch/referral/Defines$Jsonkey;


# instance fields
.field private key:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x0

    const-string v2, "IdentityID"

    const-string v3, "identity_id"

    invoke-direct {v0, v2, v1, v3}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IdentityID:Lio/branch/referral/Defines$Jsonkey;

    .line 13
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v2, 0x1

    const-string v3, "Identity"

    const-string v4, "identity"

    invoke-direct {v0, v3, v2, v4}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Identity:Lio/branch/referral/Defines$Jsonkey;

    .line 14
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v3, 0x2

    const-string v4, "DeviceFingerprintID"

    const-string v5, "device_fingerprint_id"

    invoke-direct {v0, v4, v3, v5}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeviceFingerprintID:Lio/branch/referral/Defines$Jsonkey;

    .line 15
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v4, 0x3

    const-string v5, "SessionID"

    const-string v6, "session_id"

    invoke-direct {v0, v5, v4, v6}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SessionID:Lio/branch/referral/Defines$Jsonkey;

    .line 16
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v5, 0x4

    const-string v6, "LinkClickID"

    const-string v7, "link_click_id"

    invoke-direct {v0, v6, v5, v7}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LinkClickID:Lio/branch/referral/Defines$Jsonkey;

    .line 17
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v6, 0x5

    const-string v7, "GoogleSearchInstallReferrer"

    const-string v8, "google_search_install_referrer"

    invoke-direct {v0, v7, v6, v8}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->GoogleSearchInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 18
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v7, 0x6

    const-string v8, "GooglePlayInstallReferrer"

    const-string v9, "install_referrer_extras"

    invoke-direct {v0, v8, v7, v9}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->GooglePlayInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 19
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v8, 0x7

    const-string v9, "ClickedReferrerTimeStamp"

    const-string v10, "clicked_referrer_ts"

    invoke-direct {v0, v9, v8, v10}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ClickedReferrerTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    .line 20
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v9, 0x8

    const-string v10, "InstallBeginTimeStamp"

    const-string v11, "install_begin_ts"

    invoke-direct {v0, v10, v9, v11}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstallBeginTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    .line 21
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v10, 0x9

    const-string v11, "FaceBookAppLinkChecked"

    const-string v12, "facebook_app_link_checked"

    invoke-direct {v0, v11, v10, v12}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->FaceBookAppLinkChecked:Lio/branch/referral/Defines$Jsonkey;

    .line 22
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v11, 0xa

    const-string v12, "BranchLinkUsed"

    const-string v13, "branch_used"

    invoke-direct {v0, v12, v11, v13}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchLinkUsed:Lio/branch/referral/Defines$Jsonkey;

    .line 23
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v12, 0xb

    const-string v13, "ReferringBranchIdentity"

    const-string v14, "referring_branch_identity"

    invoke-direct {v0, v13, v12, v14}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferringBranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 24
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v13, 0xc

    const-string v14, "BranchIdentity"

    const-string v15, "branch_identity"

    invoke-direct {v0, v14, v13, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 25
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v14, 0xd

    const-string v15, "BranchKey"

    const-string v13, "branch_key"

    invoke-direct {v0, v15, v14, v13}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchKey:Lio/branch/referral/Defines$Jsonkey;

    .line 26
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v13, 0xe

    const-string v15, "BranchData"

    const-string v14, "branch_data"

    invoke-direct {v0, v15, v13, v14}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchData:Lio/branch/referral/Defines$Jsonkey;

    .line 28
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v14, "Bucket"

    const/16 v15, 0xf

    const-string v13, "bucket"

    invoke-direct {v0, v14, v15, v13}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Bucket:Lio/branch/referral/Defines$Jsonkey;

    .line 29
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "DefaultBucket"

    const/16 v14, 0x10

    const-string v15, "default"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DefaultBucket:Lio/branch/referral/Defines$Jsonkey;

    .line 30
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Amount"

    const/16 v14, 0x11

    const-string v15, "amount"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Amount:Lio/branch/referral/Defines$Jsonkey;

    .line 31
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CalculationType"

    const/16 v14, 0x12

    const-string v15, "calculation_type"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CalculationType:Lio/branch/referral/Defines$Jsonkey;

    .line 32
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Location"

    const/16 v14, 0x13

    const-string v15, "location"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Location:Lio/branch/referral/Defines$Jsonkey;

    .line 33
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Type"

    const/16 v14, 0x14

    const-string v15, "type"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Type:Lio/branch/referral/Defines$Jsonkey;

    .line 34
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CreationSource"

    const/16 v14, 0x15

    const-string v15, "creation_source"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CreationSource:Lio/branch/referral/Defines$Jsonkey;

    .line 35
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Prefix"

    const/16 v14, 0x16

    const-string v15, "prefix"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Prefix:Lio/branch/referral/Defines$Jsonkey;

    .line 36
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Expiration"

    const/16 v14, 0x17

    const-string v15, "expiration"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Expiration:Lio/branch/referral/Defines$Jsonkey;

    .line 37
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Event"

    const/16 v14, 0x18

    const-string v15, "event"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Event:Lio/branch/referral/Defines$Jsonkey;

    .line 38
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Metadata"

    const/16 v14, 0x19

    const-string v15, "metadata"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Metadata:Lio/branch/referral/Defines$Jsonkey;

    .line 39
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CommerceData"

    const/16 v14, 0x1a

    const-string v15, "commerce_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CommerceData:Lio/branch/referral/Defines$Jsonkey;

    .line 40
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ReferralCode"

    const/16 v14, 0x1b

    const-string v15, "referral_code"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferralCode:Lio/branch/referral/Defines$Jsonkey;

    .line 41
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Total"

    const/16 v14, 0x1c

    const-string v15, "total"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Total:Lio/branch/referral/Defines$Jsonkey;

    .line 42
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Unique"

    const/16 v14, 0x1d

    const-string v15, "unique"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Unique:Lio/branch/referral/Defines$Jsonkey;

    .line 43
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Length"

    const/16 v14, 0x1e

    const-string v15, "length"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Length:Lio/branch/referral/Defines$Jsonkey;

    .line 44
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Direction"

    const/16 v14, 0x1f

    const-string v15, "direction"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Direction:Lio/branch/referral/Defines$Jsonkey;

    .line 45
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BeginAfterID"

    const/16 v14, 0x20

    const-string v15, "begin_after_id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BeginAfterID:Lio/branch/referral/Defines$Jsonkey;

    .line 46
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Link"

    const/16 v14, 0x21

    const-string v15, "link"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Link:Lio/branch/referral/Defines$Jsonkey;

    .line 47
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ReferringData"

    const/16 v14, 0x22

    const-string v15, "referring_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferringData:Lio/branch/referral/Defines$Jsonkey;

    .line 48
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ReferringLink"

    const/16 v14, 0x23

    const-string v15, "referring_link"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferringLink:Lio/branch/referral/Defines$Jsonkey;

    .line 49
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "IsFullAppConv"

    const/16 v14, 0x24

    const-string v15, "is_full_app_conversion"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsFullAppConv:Lio/branch/referral/Defines$Jsonkey;

    .line 50
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Data"

    const/16 v14, 0x25

    const-string v15, "data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Data:Lio/branch/referral/Defines$Jsonkey;

    .line 51
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "OS"

    const/16 v14, 0x26

    const-string v15, "os"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OS:Lio/branch/referral/Defines$Jsonkey;

    .line 52
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "HardwareID"

    const/16 v14, 0x27

    const-string v15, "hardware_id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareID:Lio/branch/referral/Defines$Jsonkey;

    .line 53
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AndroidID"

    const/16 v14, 0x28

    const-string v15, "android_id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidID:Lio/branch/referral/Defines$Jsonkey;

    .line 54
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "UnidentifiedDevice"

    const/16 v14, 0x29

    const-string v15, "unidentified_device"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UnidentifiedDevice:Lio/branch/referral/Defines$Jsonkey;

    .line 55
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "HardwareIDType"

    const/16 v14, 0x2a

    const-string v15, "hardware_id_type"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareIDType:Lio/branch/referral/Defines$Jsonkey;

    .line 56
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "HardwareIDTypeVendor"

    const/16 v14, 0x2b

    const-string v15, "vendor_id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeVendor:Lio/branch/referral/Defines$Jsonkey;

    .line 57
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "HardwareIDTypeRandom"

    const/16 v14, 0x2c

    const-string v15, "random"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeRandom:Lio/branch/referral/Defines$Jsonkey;

    .line 58
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "IsHardwareIDReal"

    const/16 v14, 0x2d

    const-string v15, "is_hardware_id_real"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsHardwareIDReal:Lio/branch/referral/Defines$Jsonkey;

    .line 59
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AppVersion"

    const/16 v14, 0x2e

    const-string v15, "app_version"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AppVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 60
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "OSVersion"

    const/16 v14, 0x2f

    const-string v15, "os_version"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OSVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 61
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Country"

    const/16 v14, 0x30

    const-string v15, "country"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Country:Lio/branch/referral/Defines$Jsonkey;

    .line 62
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Language"

    const/16 v14, 0x31

    const-string v15, "language"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Language:Lio/branch/referral/Defines$Jsonkey;

    .line 63
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "IsReferrable"

    const/16 v14, 0x32

    const-string v15, "is_referrable"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsReferrable:Lio/branch/referral/Defines$Jsonkey;

    .line 64
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Update"

    const/16 v14, 0x33

    const-string v15, "update"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Update:Lio/branch/referral/Defines$Jsonkey;

    .line 65
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "OriginalInstallTime"

    const/16 v14, 0x34

    const-string v15, "first_install_time"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OriginalInstallTime:Lio/branch/referral/Defines$Jsonkey;

    .line 66
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "FirstInstallTime"

    const/16 v14, 0x35

    const-string v15, "latest_install_time"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->FirstInstallTime:Lio/branch/referral/Defines$Jsonkey;

    .line 67
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "LastUpdateTime"

    const/16 v14, 0x36

    const-string v15, "latest_update_time"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LastUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    .line 68
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "PreviousUpdateTime"

    const/16 v14, 0x37

    const-string v15, "previous_update_time"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PreviousUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    .line 69
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "URIScheme"

    const/16 v14, 0x38

    const-string v15, "uri_scheme"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->URIScheme:Lio/branch/referral/Defines$Jsonkey;

    .line 70
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AppIdentifier"

    const/16 v14, 0x39

    const-string v15, "app_identifier"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AppIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 71
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "LinkIdentifier"

    const/16 v14, 0x3a

    const-string v15, "link_identifier"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LinkIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 72
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "GoogleAdvertisingID"

    const/16 v14, 0x3b

    const-string v15, "google_advertising_id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->GoogleAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

    .line 73
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AAID"

    const/16 v14, 0x3c

    const-string v15, "aaid"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AAID:Lio/branch/referral/Defines$Jsonkey;

    .line 74
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "LATVal"

    const/16 v14, 0x3d

    const-string v15, "lat_val"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LATVal:Lio/branch/referral/Defines$Jsonkey;

    .line 75
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "LimitedAdTracking"

    const/16 v14, 0x3e

    const-string v15, "limit_ad_tracking"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LimitedAdTracking:Lio/branch/referral/Defines$Jsonkey;

    .line 76
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "limitFacebookTracking"

    const/16 v14, 0x3f

    const-string v15, "limit_facebook_tracking"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->limitFacebookTracking:Lio/branch/referral/Defines$Jsonkey;

    .line 77
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Debug"

    const/16 v14, 0x40

    const-string v15, "debug"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Debug:Lio/branch/referral/Defines$Jsonkey;

    .line 78
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Brand"

    const/16 v14, 0x41

    const-string v15, "brand"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Brand:Lio/branch/referral/Defines$Jsonkey;

    .line 79
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Model"

    const/16 v14, 0x42

    const-string v15, "model"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Model:Lio/branch/referral/Defines$Jsonkey;

    .line 80
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ScreenDpi"

    const/16 v14, 0x43

    const-string v15, "screen_dpi"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ScreenDpi:Lio/branch/referral/Defines$Jsonkey;

    .line 81
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ScreenHeight"

    const/16 v14, 0x44

    const-string v15, "screen_height"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ScreenHeight:Lio/branch/referral/Defines$Jsonkey;

    .line 82
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ScreenWidth"

    const/16 v14, 0x45

    const-string v15, "screen_width"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ScreenWidth:Lio/branch/referral/Defines$Jsonkey;

    .line 83
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "WiFi"

    const/16 v14, 0x46

    const-string v15, "wifi"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->WiFi:Lio/branch/referral/Defines$Jsonkey;

    .line 84
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "LocalIP"

    const/16 v14, 0x47

    const-string v15, "local_ip"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LocalIP:Lio/branch/referral/Defines$Jsonkey;

    .line 85
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "UserData"

    const/16 v14, 0x48

    const-string v15, "user_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UserData:Lio/branch/referral/Defines$Jsonkey;

    .line 86
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "DeveloperIdentity"

    const/16 v14, 0x49

    const-string v15, "developer_identity"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeveloperIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 87
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "UserAgent"

    const/16 v14, 0x4a

    const-string v15, "user_agent"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UserAgent:Lio/branch/referral/Defines$Jsonkey;

    .line 88
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "SDK"

    const/16 v14, 0x4b

    const-string v15, "sdk"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SDK:Lio/branch/referral/Defines$Jsonkey;

    .line 89
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "SdkVersion"

    const/16 v14, 0x4c

    const-string v15, "sdk_version"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SdkVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 90
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "UIMode"

    const/16 v14, 0x4d

    const-string v15, "ui_mode"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UIMode:Lio/branch/referral/Defines$Jsonkey;

    .line 92
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Clicked_Branch_Link"

    const/16 v14, 0x4e

    const-string v15, "+clicked_branch_link"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Clicked_Branch_Link:Lio/branch/referral/Defines$Jsonkey;

    .line 93
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "IsFirstSession"

    const/16 v14, 0x4f

    const-string v15, "+is_first_session"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsFirstSession:Lio/branch/referral/Defines$Jsonkey;

    .line 94
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AndroidDeepLinkPath"

    const/16 v14, 0x50

    const-string v15, "$android_deeplink_path"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidDeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    .line 95
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "DeepLinkPath"

    const/16 v14, 0x51

    const-string v15, "$deeplink_path"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    .line 97
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AndroidAppLinkURL"

    const/16 v14, 0x52

    const-string v15, "android_app_link_url"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidAppLinkURL:Lio/branch/referral/Defines$Jsonkey;

    .line 98
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AndroidPushNotificationKey"

    const/16 v14, 0x53

    const-string v15, "branch"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidPushNotificationKey:Lio/branch/referral/Defines$Jsonkey;

    .line 99
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AndroidPushIdentifier"

    const/16 v14, 0x54

    const-string v15, "push_identifier"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidPushIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 100
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ForceNewBranchSession"

    const/16 v14, 0x55

    const-string v15, "branch_force_new_session"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ForceNewBranchSession:Lio/branch/referral/Defines$Jsonkey;

    .line 102
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CanonicalIdentifier"

    const/16 v14, 0x56

    const-string v15, "$canonical_identifier"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CanonicalIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 103
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentTitle"

    const/16 v14, 0x57

    const-string v15, "$og_title"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentTitle:Lio/branch/referral/Defines$Jsonkey;

    .line 104
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentDesc"

    const/16 v14, 0x58

    const-string v15, "$og_description"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentDesc:Lio/branch/referral/Defines$Jsonkey;

    .line 105
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentImgUrl"

    const/16 v14, 0x59

    const-string v15, "$og_image_url"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentImgUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 106
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CanonicalUrl"

    const/16 v14, 0x5a

    const-string v15, "$canonical_url"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CanonicalUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 108
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentType"

    const/16 v14, 0x5b

    const-string v15, "$content_type"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentType:Lio/branch/referral/Defines$Jsonkey;

    .line 109
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "PublicallyIndexable"

    const/16 v14, 0x5c

    const-string v15, "$publicly_indexable"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PublicallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    .line 110
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "LocallyIndexable"

    const/16 v14, 0x5d

    const-string v15, "$locally_indexable"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LocallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    .line 111
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentKeyWords"

    const/16 v14, 0x5e

    const-string v15, "$keywords"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentKeyWords:Lio/branch/referral/Defines$Jsonkey;

    .line 112
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentExpiryTime"

    const/16 v14, 0x5f

    const-string v15, "$exp_date"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentExpiryTime:Lio/branch/referral/Defines$Jsonkey;

    .line 113
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Params"

    const/16 v14, 0x60

    const-string v15, "params"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Params:Lio/branch/referral/Defines$Jsonkey;

    .line 114
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "SharedLink"

    const/16 v14, 0x61

    const-string v15, "$shared_link"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SharedLink:Lio/branch/referral/Defines$Jsonkey;

    .line 115
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ShareError"

    const/16 v14, 0x62

    const-string v15, "$share_error"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ShareError:Lio/branch/referral/Defines$Jsonkey;

    .line 118
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "External_Intent_URI"

    const/16 v14, 0x63

    const-string v15, "external_intent_uri"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->External_Intent_URI:Lio/branch/referral/Defines$Jsonkey;

    .line 119
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "External_Intent_Extra"

    const/16 v14, 0x64

    const-string v15, "external_intent_extra"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->External_Intent_Extra:Lio/branch/referral/Defines$Jsonkey;

    .line 120
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Last_Round_Trip_Time"

    const/16 v14, 0x65

    const-string v15, "lrtt"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Last_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 121
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Branch_Round_Trip_Time"

    const/16 v14, 0x66

    const-string v15, "brtt"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Branch_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 122
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Branch_Instrumentation"

    const/16 v14, 0x67

    const-string v15, "instrumentation"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Branch_Instrumentation:Lio/branch/referral/Defines$Jsonkey;

    .line 123
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Queue_Wait_Time"

    const/16 v14, 0x68

    const-string v15, "qwt"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Queue_Wait_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 124
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "InstantDeepLinkSession"

    const/16 v14, 0x69

    const-string v15, "instant_dl_session"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

    .line 126
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BranchViewData"

    const/16 v14, 0x6a

    const-string v15, "branch_view_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewData:Lio/branch/referral/Defines$Jsonkey;

    .line 127
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BranchViewID"

    const/16 v14, 0x6b

    const-string v15, "id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewID:Lio/branch/referral/Defines$Jsonkey;

    .line 128
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BranchViewAction"

    const/16 v14, 0x6c

    const-string v15, "action"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewAction:Lio/branch/referral/Defines$Jsonkey;

    .line 129
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BranchViewNumOfUse"

    const/16 v14, 0x6d

    const-string v15, "number_of_use"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewNumOfUse:Lio/branch/referral/Defines$Jsonkey;

    .line 130
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BranchViewUrl"

    const/16 v14, 0x6e

    const-string v15, "url"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 131
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "BranchViewHtml"

    const/16 v14, 0x6f

    const-string v15, "html"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewHtml:Lio/branch/referral/Defines$Jsonkey;

    .line 133
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Path"

    const/16 v14, 0x70

    const-string v15, "path"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Path:Lio/branch/referral/Defines$Jsonkey;

    .line 134
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ViewList"

    const/16 v14, 0x71

    const-string v15, "view_list"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ViewList:Lio/branch/referral/Defines$Jsonkey;

    .line 135
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentActionView"

    const/16 v14, 0x72

    const-string v15, "view"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentActionView:Lio/branch/referral/Defines$Jsonkey;

    .line 136
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentPath"

    const/16 v14, 0x73

    const-string v15, "content_path"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentPath:Lio/branch/referral/Defines$Jsonkey;

    .line 137
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentNavPath"

    const/16 v14, 0x74

    const-string v15, "content_nav_path"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentNavPath:Lio/branch/referral/Defines$Jsonkey;

    .line 138
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ReferralLink"

    const/16 v14, 0x75

    const-string v15, "referral_link"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferralLink:Lio/branch/referral/Defines$Jsonkey;

    .line 139
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentData"

    const/16 v14, 0x76

    const-string v15, "content_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentData:Lio/branch/referral/Defines$Jsonkey;

    .line 140
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentEvents"

    const/16 v14, 0x77

    const-string v15, "events"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentEvents:Lio/branch/referral/Defines$Jsonkey;

    .line 141
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentAnalyticsMode"

    const/16 v14, 0x78

    const-string v15, "content_analytics_mode"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentAnalyticsMode:Lio/branch/referral/Defines$Jsonkey;

    .line 142
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentDiscovery"

    const/16 v14, 0x79

    const-string v15, "cd"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentDiscovery:Lio/branch/referral/Defines$Jsonkey;

    .line 143
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Environment"

    const/16 v14, 0x7a

    const-string v15, "environment"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Environment:Lio/branch/referral/Defines$Jsonkey;

    .line 144
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "InstantApp"

    const/16 v14, 0x7b

    const-string v15, "INSTANT_APP"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstantApp:Lio/branch/referral/Defines$Jsonkey;

    .line 145
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "NativeApp"

    const/16 v14, 0x7c

    const-string v15, "FULL_APP"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->NativeApp:Lio/branch/referral/Defines$Jsonkey;

    .line 147
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "TransactionID"

    const/16 v14, 0x7d

    const-string v15, "transaction_id"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->TransactionID:Lio/branch/referral/Defines$Jsonkey;

    .line 148
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Currency"

    const/16 v14, 0x7e

    const-string v15, "currency"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Currency:Lio/branch/referral/Defines$Jsonkey;

    .line 149
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Revenue"

    const/16 v14, 0x7f

    const-string v15, "revenue"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Revenue:Lio/branch/referral/Defines$Jsonkey;

    .line 150
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Shipping"

    const/16 v14, 0x80

    const-string v15, "shipping"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Shipping:Lio/branch/referral/Defines$Jsonkey;

    .line 151
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Tax"

    const/16 v14, 0x81

    const-string v15, "tax"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Tax:Lio/branch/referral/Defines$Jsonkey;

    .line 152
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Coupon"

    const/16 v14, 0x82

    const-string v15, "coupon"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Coupon:Lio/branch/referral/Defines$Jsonkey;

    .line 153
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Affiliation"

    const/16 v14, 0x83

    const-string v15, "affiliation"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Affiliation:Lio/branch/referral/Defines$Jsonkey;

    .line 154
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Description"

    const/16 v14, 0x84

    const-string v15, "description"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Description:Lio/branch/referral/Defines$Jsonkey;

    .line 155
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "SearchQuery"

    const/16 v14, 0x85

    const-string v15, "search_query"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SearchQuery:Lio/branch/referral/Defines$Jsonkey;

    .line 157
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Name"

    const/16 v14, 0x86

    const-string v15, "name"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Name:Lio/branch/referral/Defines$Jsonkey;

    .line 158
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CustomData"

    const/16 v14, 0x87

    const-string v15, "custom_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CustomData:Lio/branch/referral/Defines$Jsonkey;

    .line 159
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "EventData"

    const/16 v14, 0x88

    const-string v15, "event_data"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->EventData:Lio/branch/referral/Defines$Jsonkey;

    .line 160
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentItems"

    const/16 v14, 0x89

    const-string v15, "content_items"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentItems:Lio/branch/referral/Defines$Jsonkey;

    .line 161
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ContentSchema"

    const/16 v14, 0x8a

    const-string v15, "$content_schema"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentSchema:Lio/branch/referral/Defines$Jsonkey;

    .line 162
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Price"

    const/16 v14, 0x8b

    const-string v15, "$price"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Price:Lio/branch/referral/Defines$Jsonkey;

    .line 163
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "PriceCurrency"

    const/16 v14, 0x8c

    const-string v15, "$currency"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PriceCurrency:Lio/branch/referral/Defines$Jsonkey;

    .line 164
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Quantity"

    const/16 v14, 0x8d

    const-string v15, "$quantity"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Quantity:Lio/branch/referral/Defines$Jsonkey;

    .line 165
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "SKU"

    const/16 v14, 0x8e

    const-string v15, "$sku"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SKU:Lio/branch/referral/Defines$Jsonkey;

    .line 166
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ProductName"

    const/16 v14, 0x8f

    const-string v15, "$product_name"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductName:Lio/branch/referral/Defines$Jsonkey;

    .line 167
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ProductBrand"

    const/16 v14, 0x90

    const-string v15, "$product_brand"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductBrand:Lio/branch/referral/Defines$Jsonkey;

    .line 168
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ProductCategory"

    const/16 v14, 0x91

    const-string v15, "$product_category"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductCategory:Lio/branch/referral/Defines$Jsonkey;

    .line 169
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ProductVariant"

    const/16 v14, 0x92

    const-string v15, "$product_variant"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductVariant:Lio/branch/referral/Defines$Jsonkey;

    .line 170
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Rating"

    const/16 v14, 0x93

    const-string v15, "$rating"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Rating:Lio/branch/referral/Defines$Jsonkey;

    .line 171
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "RatingAverage"

    const/16 v14, 0x94

    const-string v15, "$rating_average"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RatingAverage:Lio/branch/referral/Defines$Jsonkey;

    .line 172
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "RatingCount"

    const/16 v14, 0x95

    const-string v15, "$rating_count"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RatingCount:Lio/branch/referral/Defines$Jsonkey;

    .line 173
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "RatingMax"

    const/16 v14, 0x96

    const-string v15, "$rating_max"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RatingMax:Lio/branch/referral/Defines$Jsonkey;

    .line 174
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AddressStreet"

    const/16 v14, 0x97

    const-string v15, "$address_street"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressStreet:Lio/branch/referral/Defines$Jsonkey;

    .line 175
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AddressCity"

    const/16 v14, 0x98

    const-string v15, "$address_city"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressCity:Lio/branch/referral/Defines$Jsonkey;

    .line 176
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AddressRegion"

    const/16 v14, 0x99

    const-string v15, "$address_region"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressRegion:Lio/branch/referral/Defines$Jsonkey;

    .line 177
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AddressCountry"

    const/16 v14, 0x9a

    const-string v15, "$address_country"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressCountry:Lio/branch/referral/Defines$Jsonkey;

    .line 178
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "AddressPostalCode"

    const/16 v14, 0x9b

    const-string v15, "$address_postal_code"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressPostalCode:Lio/branch/referral/Defines$Jsonkey;

    .line 179
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Latitude"

    const/16 v14, 0x9c

    const-string v15, "$latitude"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Latitude:Lio/branch/referral/Defines$Jsonkey;

    .line 180
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Longitude"

    const/16 v14, 0x9d

    const-string v15, "$longitude"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Longitude:Lio/branch/referral/Defines$Jsonkey;

    .line 181
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "ImageCaptions"

    const/16 v14, 0x9e

    const-string v15, "$image_captions"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ImageCaptions:Lio/branch/referral/Defines$Jsonkey;

    .line 182
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "Condition"

    const/16 v14, 0x9f

    const-string v15, "$condition"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Condition:Lio/branch/referral/Defines$Jsonkey;

    .line 183
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "CreationTimestamp"

    const/16 v14, 0xa0

    const-string v15, "$creation_timestamp"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CreationTimestamp:Lio/branch/referral/Defines$Jsonkey;

    .line 184
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v13, "TrackingDisabled"

    const/16 v14, 0xa1

    const-string v15, "tracking_disabled"

    invoke-direct {v0, v13, v14, v15}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->TrackingDisabled:Lio/branch/referral/Defines$Jsonkey;

    const/16 v0, 0xa2

    new-array v0, v0, [Lio/branch/referral/Defines$Jsonkey;

    .line 10
    sget-object v13, Lio/branch/referral/Defines$Jsonkey;->IdentityID:Lio/branch/referral/Defines$Jsonkey;

    aput-object v13, v0, v1

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Identity:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->DeviceFingerprintID:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v3

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->SessionID:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v4

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LinkClickID:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v5

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->GoogleSearchInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v6

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->GooglePlayInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v7

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ClickedReferrerTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v8

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->InstallBeginTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v9

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->FaceBookAppLinkChecked:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v10

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchLinkUsed:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v11

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ReferringBranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    aput-object v1, v0, v12

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchKey:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Bucket:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->DefaultBucket:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Amount:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CalculationType:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Location:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Type:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CreationSource:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Prefix:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Expiration:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Event:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Metadata:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CommerceData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ReferralCode:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Total:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Unique:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Length:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Direction:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BeginAfterID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Link:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ReferringData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ReferringLink:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->IsFullAppConv:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Data:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->OS:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->HardwareID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AndroidID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->UnidentifiedDevice:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->HardwareIDType:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeVendor:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeRandom:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->IsHardwareIDReal:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AppVersion:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->OSVersion:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Country:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Language:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->IsReferrable:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Update:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->OriginalInstallTime:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->FirstInstallTime:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LastUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->PreviousUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->URIScheme:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AppIdentifier:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LinkIdentifier:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->GoogleAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AAID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LATVal:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LimitedAdTracking:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->limitFacebookTracking:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Debug:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Brand:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Model:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ScreenDpi:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ScreenHeight:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ScreenWidth:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->WiFi:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LocalIP:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->UserData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->DeveloperIdentity:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->UserAgent:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->SDK:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->SdkVersion:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->UIMode:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Clicked_Branch_Link:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->IsFirstSession:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AndroidDeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->DeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AndroidAppLinkURL:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AndroidPushNotificationKey:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AndroidPushIdentifier:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ForceNewBranchSession:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CanonicalIdentifier:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentTitle:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentDesc:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentImgUrl:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CanonicalUrl:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentType:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->PublicallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->LocallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentKeyWords:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentExpiryTime:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Params:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->SharedLink:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ShareError:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->External_Intent_URI:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->External_Intent_Extra:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Last_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Branch_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Branch_Instrumentation:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Queue_Wait_Time:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchViewData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchViewID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchViewAction:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchViewNumOfUse:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchViewUrl:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->BranchViewHtml:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Path:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ViewList:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentActionView:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentPath:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentNavPath:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ReferralLink:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentEvents:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentAnalyticsMode:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentDiscovery:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Environment:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->InstantApp:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->NativeApp:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->TransactionID:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Currency:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Revenue:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Shipping:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Tax:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Coupon:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Affiliation:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Description:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->SearchQuery:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Name:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CustomData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->EventData:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentItems:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ContentSchema:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Price:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->PriceCurrency:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Quantity:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->SKU:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ProductName:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ProductBrand:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ProductCategory:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ProductVariant:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Rating:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->RatingAverage:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->RatingCount:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->RatingMax:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AddressStreet:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AddressCity:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AddressRegion:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AddressCountry:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->AddressPostalCode:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Latitude:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Longitude:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->ImageCaptions:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Condition:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->CreationTimestamp:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->TrackingDisabled:Lio/branch/referral/Defines$Jsonkey;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->$VALUES:[Lio/branch/referral/Defines$Jsonkey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 188
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const-string p1, ""

    .line 186
    iput-object p1, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    .line 189
    iput-object p3, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/Defines$Jsonkey;
    .locals 1

    .line 10
    const-class v0, Lio/branch/referral/Defines$Jsonkey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lio/branch/referral/Defines$Jsonkey;

    return-object p0
.end method

.method public static values()[Lio/branch/referral/Defines$Jsonkey;
    .locals 1

    .line 10
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->$VALUES:[Lio/branch/referral/Defines$Jsonkey;

    invoke-virtual {v0}, [Lio/branch/referral/Defines$Jsonkey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/branch/referral/Defines$Jsonkey;

    return-object v0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .line 193
    iget-object v0, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 198
    iget-object v0, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    return-object v0
.end method
