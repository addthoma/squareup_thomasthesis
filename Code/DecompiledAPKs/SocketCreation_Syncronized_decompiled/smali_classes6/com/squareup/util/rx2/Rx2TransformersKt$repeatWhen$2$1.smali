.class final Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "T",
        "",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;


# direct methods
.method constructor <init>(Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-object p1, p1, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$refreshRequested:Lio/reactivex/Observable;

    .line 244
    sget-object v0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1$1;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 245
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-wide v0, v0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$minIdleTime:J

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-object v2, v2, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-object v3, v3, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/Observable;->delaySubscription(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    .line 246
    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-wide v5, p1, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$maxIdleTime:J

    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-object v7, p1, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;

    iget-object v8, p1, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$scheduler:Lio/reactivex/Scheduler;

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    move-object v9, p1

    check-cast v9, Lio/reactivex/ObservableSource;

    invoke-virtual/range {v4 .. v9}, Lio/reactivex/Observable;->timeout(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 247
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;->apply(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
