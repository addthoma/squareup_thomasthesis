.class public final Lcom/squareup/util/rx2/Maybes;
.super Ljava/lang/Object;
.source "RxKotlin.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002Jh\u0010\u0003\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00080\u00050\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\nH\u0007J\u0083\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2 \u0008\u0004\u0010\u000e\u001a\u001a\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\r0\u000fH\u0087\u0008J\u0086\u0001\u0010\u0003\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u00110\u00100\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\nH\u0007J\u00a1\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2&\u0008\u0004\u0010\u000e\u001a \u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\r0\u0013H\u0087\u0008J\u00a4\u0001\u0010\u0003\u001a&\u0012\"\u0012 \u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u00150\u00140\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\nH\u0007J\u00bf\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u0001\"\u0008\u0008\u0005\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\n2,\u0008\u0004\u0010\u000e\u001a&\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u0002H\r0\u0017H\u0087\u0008J\u00c2\u0001\u0010\u0003\u001a,\u0012(\u0012&\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u0002H\u00190\u00180\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u0001\"\u0008\u0008\u0005\u0010\u0019*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\n2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u00190\nH\u0007J\u00dd\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u0001\"\u0008\u0008\u0005\u0010\u0019*\u00020\u0001\"\u0008\u0008\u0006\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\n2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u00190\n22\u0008\u0004\u0010\u000e\u001a,\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u0002H\u0019\u0012\u0004\u0012\u0002H\r0\u001bH\u0087\u0008J\u00fb\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u0001\"\u0008\u0008\u0005\u0010\u0019*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001c*\u00020\u0001\"\u0008\u0008\u0007\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\n2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u00190\n2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u0002H\u001c0\n28\u0008\u0004\u0010\u000e\u001a2\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u0002H\u0019\u0012\u0004\u0012\u0002H\u001c\u0012\u0004\u0012\u0002H\r0\u001eH\u0087\u0008J\u0099\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u0001\"\u0008\u0008\u0005\u0010\u0019*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001c*\u00020\u0001\"\u0008\u0008\u0007\u0010\u001f*\u00020\u0001\"\u0008\u0008\u0008\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\n2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u00190\n2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u0002H\u001c0\n2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u0002H\u001f0\n2>\u0008\u0004\u0010\u000e\u001a8\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u0002H\u0019\u0012\u0004\u0012\u0002H\u001c\u0012\u0004\u0012\u0002H\u001f\u0012\u0004\u0012\u0002H\r0!H\u0087\u0008J\u00b7\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0015*\u00020\u0001\"\u0008\u0008\u0005\u0010\u0019*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001c*\u00020\u0001\"\u0008\u0008\u0007\u0010\u001f*\u00020\u0001\"\u0008\u0008\u0008\u0010\"*\u00020\u0001\"\u0008\u0008\t\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\n2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\n2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u00190\n2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u0002H\u001c0\n2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u0002H\u001f0\n2\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\"0\n2D\u0008\u0004\u0010\u000e\u001a>\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u0002H\u0019\u0012\u0004\u0012\u0002H\u001c\u0012\u0004\u0012\u0002H\u001f\u0012\u0004\u0012\u0002H\"\u0012\u0004\u0012\u0002H\r0$H\u0087\u0008JJ\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H&\u0012\u0004\u0012\u0002H\'0%0\u0004\"\u0008\u0008\u0000\u0010&*\u00020\u0001\"\u0008\u0008\u0001\u0010\'*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H&0\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\'0\nH\u0007Je\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\r0\u0004\"\u0008\u0008\u0000\u0010&*\u00020\u0001\"\u0008\u0008\u0001\u0010\'*\u00020\u0001\"\u0008\u0008\u0002\u0010\r*\u00020\u00012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H&0\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\'0\n2\u001a\u0008\u0004\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u0002H&\u0012\u0004\u0012\u0002H\'\u0012\u0004\u0012\u0002H\r0(H\u0087\u0008\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/util/rx2/Maybes;",
        "",
        "()V",
        "zip",
        "Lio/reactivex/Maybe;",
        "Lkotlin/Triple;",
        "T1",
        "T2",
        "T3",
        "s1",
        "Lio/reactivex/MaybeSource;",
        "s2",
        "s3",
        "R",
        "zipper",
        "Lkotlin/Function3;",
        "Lcom/squareup/util/tuple/Quartet;",
        "T4",
        "s4",
        "Lkotlin/Function4;",
        "Lcom/squareup/util/tuple/Quintuple;",
        "T5",
        "s5",
        "Lkotlin/Function5;",
        "Lcom/squareup/util/tuple/Sextuple;",
        "T6",
        "s6",
        "Lkotlin/Function6;",
        "T7",
        "s7",
        "Lkotlin/Function7;",
        "T8",
        "s8",
        "Lkotlin/Function8;",
        "T9",
        "s9",
        "Lkotlin/Function9;",
        "Lkotlin/Pair;",
        "T",
        "U",
        "Lkotlin/Function2;",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/rx2/Maybes;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1256
    new-instance v0, Lcom/squareup/util/rx2/Maybes;

    invoke-direct {v0}, Lcom/squareup/util/rx2/Maybes;-><init>()V

    sput-object v0, Lcom/squareup/util/rx2/Maybes;->INSTANCE:Lcom/squareup/util/rx2/Maybes;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;",
            "Lio/reactivex/MaybeSource<",
            "TU;>;)",
            "Lio/reactivex/Maybe<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1273
    sget-object v0, Lcom/squareup/util/rx2/Maybes$zip$2;->INSTANCE:Lcom/squareup/util/rx2/Maybes$zip$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-static {p1, p2, v0}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, BiFunction(::Pair))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;)",
            "Lio/reactivex/Maybe<",
            "Lkotlin/Triple<",
            "TT1;TT2;TT3;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1293
    sget-object v0, Lcom/squareup/util/rx2/Maybes$zip$4;->INSTANCE:Lcom/squareup/util/rx2/Maybes$zip$4;

    check-cast v0, Lkotlin/jvm/functions/Function3;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function3$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function3$0;-><init>(Lkotlin/jvm/functions/Function3;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Function3;

    invoke-static {p1, p2, p3, v0}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, s3, Function3(::Triple))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;)",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TT1;TT2;TT3;TT4;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1316
    sget-object v0, Lcom/squareup/util/rx2/Maybes$zip$6;->INSTANCE:Lcom/squareup/util/rx2/Maybes$zip$6;

    check-cast v0, Lkotlin/jvm/functions/Function4;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function4$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Function4;

    invoke-static {p1, p2, p3, p4, v0}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, s3, s4, Function4(::Quartet))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;)",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/util/tuple/Quintuple<",
            "TT1;TT2;TT3;TT4;TT5;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1341
    sget-object v0, Lcom/squareup/util/rx2/Maybes$zip$8;->INSTANCE:Lcom/squareup/util/rx2/Maybes$zip$8;

    check-cast v0, Lkotlin/jvm/functions/Function5;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function5$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function5$0;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object v0, v1

    :cond_0
    move-object v6, v0

    check-cast v6, Lio/reactivex/functions/Function5;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v1 .. v6}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, s3, s4\u2026, Function5(::Quintuple))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;",
            "Lio/reactivex/MaybeSource<",
            "TT6;>;)",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/util/tuple/Sextuple<",
            "TT1;TT2;TT3;TT4;TT5;TT6;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1370
    sget-object v0, Lcom/squareup/util/rx2/Maybes$zip$10;->INSTANCE:Lcom/squareup/util/rx2/Maybes$zip$10;

    check-cast v0, Lkotlin/jvm/functions/Function6;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function6$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object v0, v1

    :cond_0
    move-object v7, v0

    check-cast v7, Lio/reactivex/functions/Function6;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v1 .. v7}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function6;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, s3, s4\u20266, Function6(::Sextuple))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function9;)Lio/reactivex/Maybe;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            "T9:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;",
            "Lio/reactivex/MaybeSource<",
            "TT6;>;",
            "Lio/reactivex/MaybeSource<",
            "TT7;>;",
            "Lio/reactivex/MaybeSource<",
            "TT8;>;",
            "Lio/reactivex/MaybeSource<",
            "TT9;>;",
            "Lkotlin/jvm/functions/Function9<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;-TT8;-TT9;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    move-object/from16 v0, p10

    const-string v1, "s1"

    move-object v2, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s2"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s3"

    move-object v4, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s4"

    move-object/from16 v5, p4

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s5"

    move-object/from16 v6, p5

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s6"

    move-object/from16 v7, p6

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s7"

    move-object/from16 v8, p7

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s8"

    move-object/from16 v9, p8

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s9"

    move-object/from16 v10, p9

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "zipper"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1419
    new-instance v1, Lcom/squareup/util/rx2/Maybes$zip$13;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/Maybes$zip$13;-><init>(Lkotlin/jvm/functions/Function9;)V

    move-object v11, v1

    check-cast v11, Lio/reactivex/functions/Function9;

    .line 1418
    invoke-static/range {v2 .. v11}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function9;)Lio/reactivex/Maybe;

    move-result-object v0

    const-string v1, "Maybe.zip(\n      s1, s2,\u2026t6, t7, t8, t9\n    )\n  })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function8;)Lio/reactivex/Maybe;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;",
            "Lio/reactivex/MaybeSource<",
            "TT6;>;",
            "Lio/reactivex/MaybeSource<",
            "TT7;>;",
            "Lio/reactivex/MaybeSource<",
            "TT8;>;",
            "Lkotlin/jvm/functions/Function8<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;-TT8;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    move-object/from16 v0, p9

    const-string v1, "s1"

    move-object v2, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s2"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s3"

    move-object v4, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s4"

    move-object v5, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s5"

    move-object/from16 v6, p5

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s6"

    move-object/from16 v7, p6

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s7"

    move-object/from16 v8, p7

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s8"

    move-object/from16 v9, p8

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "zipper"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1402
    new-instance v1, Lcom/squareup/util/rx2/Maybes$zip$12;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/Maybes$zip$12;-><init>(Lkotlin/jvm/functions/Function8;)V

    move-object v10, v1

    check-cast v10, Lio/reactivex/functions/Function8;

    .line 1400
    invoke-static/range {v2 .. v10}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function8;)Lio/reactivex/Maybe;

    move-result-object v0

    const-string v1, "Maybe.zip(\n      s1, s2,\u20263, t4, t5, t6, t7, t8) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function7;)Lio/reactivex/Maybe;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;",
            "Lio/reactivex/MaybeSource<",
            "TT6;>;",
            "Lio/reactivex/MaybeSource<",
            "TT7;>;",
            "Lkotlin/jvm/functions/Function7<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    move-object/from16 v0, p8

    const-string v1, "s1"

    move-object v2, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s2"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s3"

    move-object v4, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s4"

    move-object v5, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s5"

    move-object v6, p5

    invoke-static {p5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s6"

    move-object/from16 v7, p6

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "s7"

    move-object/from16 v8, p7

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "zipper"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1385
    new-instance v1, Lcom/squareup/util/rx2/Maybes$zip$11;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/Maybes$zip$11;-><init>(Lkotlin/jvm/functions/Function7;)V

    move-object v9, v1

    check-cast v9, Lio/reactivex/functions/Function7;

    .line 1383
    invoke-static/range {v2 .. v9}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function7;)Lio/reactivex/Maybe;

    move-result-object v0

    const-string v1, "Maybe.zip(\n      s1, s2,\u20262, t3, t4, t5, t6, t7) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function6;)Lio/reactivex/Maybe;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;",
            "Lio/reactivex/MaybeSource<",
            "TT6;>;",
            "Lkotlin/jvm/functions/Function6<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1355
    new-instance v0, Lcom/squareup/util/rx2/Maybes$zip$9;

    invoke-direct {v0, p7}, Lcom/squareup/util/rx2/Maybes$zip$9;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object v7, v0

    check-cast v7, Lio/reactivex/functions/Function6;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 1353
    invoke-static/range {v1 .. v7}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function6;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(\n      s1, s2,\u20261, t2, t3, t4, t5, t6) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function5;)Lio/reactivex/Maybe;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lio/reactivex/MaybeSource<",
            "TT5;>;",
            "Lkotlin/jvm/functions/Function5<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1328
    new-instance v0, Lcom/squareup/util/rx2/Maybes$zip$7;

    invoke-direct {v0, p6}, Lcom/squareup/util/rx2/Maybes$zip$7;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object v6, v0

    check-cast v6, Lio/reactivex/functions/Function5;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1327
    invoke-static/range {v1 .. v6}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(\n      s1, s2,\u2026ke(t1, t2, t3, t4, t5) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function4;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lio/reactivex/MaybeSource<",
            "TT4;>;",
            "Lkotlin/jvm/functions/Function4<",
            "-TT1;-TT2;-TT3;-TT4;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1304
    new-instance v0, Lcom/squareup/util/rx2/Maybes$zip$5;

    invoke-direct {v0, p5}, Lcom/squareup/util/rx2/Maybes$zip$5;-><init>(Lkotlin/jvm/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Function4;

    invoke-static {p1, p2, p3, p4, v0}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, s3, s4\u2026invoke(t1, t2, t3, t4) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function3;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT1;>;",
            "Lio/reactivex/MaybeSource<",
            "TT2;>;",
            "Lio/reactivex/MaybeSource<",
            "TT3;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1282
    new-instance v0, Lcom/squareup/util/rx2/Maybes$zip$3;

    invoke-direct {v0, p4}, Lcom/squareup/util/rx2/Maybes$zip$3;-><init>(Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Function3;

    invoke-static {p1, p2, p3, v0}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, s3, Fu\u2026per.invoke(t1, t2, t3) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;",
            "Lio/reactivex/MaybeSource<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "s1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "s2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1263
    new-instance v0, Lcom/squareup/util/rx2/Maybes$zip$1;

    invoke-direct {v0, p3}, Lcom/squareup/util/rx2/Maybes$zip$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-static {p1, p2, v0}, Lio/reactivex/Maybe;->zip(Lio/reactivex/MaybeSource;Lio/reactivex/MaybeSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string p2, "Maybe.zip(s1, s2, BiFunc\u2026-> zipper.invoke(t, u) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
