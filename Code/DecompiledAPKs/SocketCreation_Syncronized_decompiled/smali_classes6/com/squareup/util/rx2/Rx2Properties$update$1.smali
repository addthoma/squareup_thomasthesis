.class final Lcom/squareup/util/rx2/Rx2Properties$update$1;
.super Ljava/lang/Object;
.source "Rx2Properties.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Properties;->update(Ljava/io/File;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/util/Properties;",
        "Lio/reactivex/CompletableSource;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRx2Properties.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Rx2Properties.kt\ncom/squareup/util/rx2/Rx2Properties$update$1\n*L\n1#1,84:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "props",
        "Ljava/util/Properties;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic $update:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Ljava/io/File;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Properties$update$1;->$file:Ljava/io/File;

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2Properties$update$1;->$update:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/Properties;)Lio/reactivex/Completable;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/squareup/util/rx2/Rx2Properties;->INSTANCE:Lcom/squareup/util/rx2/Rx2Properties;

    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2Properties$update$1;->$file:Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2Properties$update$1;->$update:Lkotlin/jvm/functions/Function1;

    invoke-interface {v2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/util/rx2/Rx2Properties;->save(Ljava/io/File;Ljava/util/Properties;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/util/Properties;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2Properties$update$1;->apply(Ljava/util/Properties;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method
