.class public Lcom/squareup/util/ProtoGlyphs;
.super Ljava/lang/Object;
.source "ProtoGlyphs.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p0, :cond_0

    .line 111
    invoke-static {p1}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    return-object p0

    .line 114
    :cond_0
    sget-object v0, Lcom/squareup/util/ProtoGlyphs$1;->$SwitchMap$com$squareup$Card$Brand:[I

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 134
    invoke-static {p1}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    return-object p0

    .line 132
    :pswitch_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 130
    :pswitch_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_CUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 128
    :pswitch_2
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 126
    :pswitch_3
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_MC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 124
    :pswitch_4
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_JCB:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 122
    :pswitch_5
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_INTERAC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 120
    :pswitch_6
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_DISCOVER_DINERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 118
    :pswitch_7
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_DISCOVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 116
    :pswitch_8
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static cash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    if-eqz p0, :cond_1

    .line 11
    sget-object v0, Lcom/squareup/util/ProtoGlyphs$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    .line 17
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOLLAR_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 13
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->YEN_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 20
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOLLAR_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static circleCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    if-eqz p0, :cond_1

    .line 47
    sget-object v0, Lcom/squareup/util/ProtoGlyphs$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 53
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 50
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 56
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static splitTender(ZZLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    if-eqz p0, :cond_0

    .line 98
    invoke-static {p2}, Lcom/squareup/util/ProtoGlyphs;->splitTenderCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz p1, :cond_1

    .line 100
    invoke-static {p2}, Lcom/squareup/util/ProtoGlyphs;->splitTenderCash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p0

    return-object p0

    .line 102
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_OTHER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static splitTenderCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    if-eqz p0, :cond_1

    .line 77
    sget-object v0, Lcom/squareup/util/ProtoGlyphs$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 83
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 80
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 86
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static splitTenderCash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    if-eqz p0, :cond_2

    .line 61
    sget-object v0, Lcom/squareup/util/ProtoGlyphs$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    goto :goto_0

    .line 66
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_DOLLAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 63
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_YEN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 69
    :cond_2
    :goto_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_DOLLAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-eqz p0, :cond_1

    .line 30
    sget-object v0, Lcom/squareup/util/ProtoGlyphs$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 36
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_BACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 33
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 39
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_BACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method
