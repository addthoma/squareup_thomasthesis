.class public Lcom/squareup/util/ShortTimes;
.super Ljava/lang/Object;
.source "ShortTimes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/ShortTimes$MaxUnit;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendAgo(Lcom/squareup/util/Res;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .line 141
    sget v0, Lcom/squareup/utilities/R$string;->time_ago:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "time"

    .line 142
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 143
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static shortTimeSince(Lcom/squareup/util/Res;JJZLcom/squareup/util/ShortTimes$MaxUnit;)Ljava/lang/String;
    .locals 6

    sub-long v1, p1, p3

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p5

    move-object v4, p6

    .line 27
    invoke-static/range {v0 .. v5}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static shortTimeSince(Lcom/squareup/util/Res;JJZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;
    .locals 6

    sub-long v1, p1, p3

    move-object v0, p0

    move v3, p5

    move-object v4, p6

    move v5, p7

    .line 33
    invoke-static/range {v0 .. v5}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static shortTimeSince(Lcom/squareup/util/Res;JZ)Ljava/lang/String;
    .locals 6

    .line 37
    sget-object v4, Lcom/squareup/util/ShortTimes$MaxUnit;->WEEK:Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    .line 47
    invoke-static/range {v0 .. v5}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    .line 52
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1e

    const-wide/16 v6, 0x3c

    if-eqz p3, :cond_0

    move-wide v8, v4

    goto :goto_0

    :cond_0
    move-wide v8, v6

    .line 56
    :goto_0
    sget-object v10, Lcom/squareup/util/ShortTimes$MaxUnit;->SECOND:Lcom/squareup/util/ShortTimes$MaxUnit;

    if-ne v1, v10, :cond_2

    .line 57
    sget v1, Lcom/squareup/utilities/R$string;->time_sec:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 58
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "second"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz p5, :cond_1

    .line 60
    invoke-static {v0, v1}, Lcom/squareup/util/ShortTimes;->appendAgo(Lcom/squareup/util/Res;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-wide/16 v10, 0x0

    cmp-long v12, v2, v10

    if-ltz v12, :cond_3

    cmp-long v12, v2, v8

    if-gez v12, :cond_3

    .line 62
    sget v1, Lcom/squareup/utilities/R$string;->time_just_now:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    long-to-double v8, v2

    const-wide/high16 v12, 0x404e000000000000L    # 60.0

    div-double/2addr v8, v12

    const/4 v15, 0x0

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    cmpl-double v18, v8, v16

    if-ltz v18, :cond_4

    const/16 v18, 0x1

    goto :goto_1

    :cond_4
    const/16 v18, 0x0

    :goto_1
    if-eqz p3, :cond_5

    if-eqz v18, :cond_5

    .line 67
    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto :goto_2

    :cond_5
    div-long/2addr v2, v6

    :goto_2
    if-eqz p3, :cond_6

    goto :goto_3

    :cond_6
    move-wide v4, v6

    :goto_3
    const-string v8, "minute"

    cmp-long v9, v2, v10

    if-lez v9, :cond_7

    cmp-long v9, v2, v4

    if-ltz v9, :cond_8

    .line 72
    :cond_7
    sget-object v4, Lcom/squareup/util/ShortTimes$MaxUnit;->MINUTE:Lcom/squareup/util/ShortTimes$MaxUnit;

    if-ne v1, v4, :cond_a

    .line 73
    :cond_8
    sget v1, Lcom/squareup/utilities/R$string;->time_min:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 74
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v8, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz p5, :cond_9

    .line 76
    invoke-static {v0, v1}, Lcom/squareup/util/ShortTimes;->appendAgo(Lcom/squareup/util/Res;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_9
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_a
    long-to-double v4, v2

    div-double/2addr v4, v12

    cmpl-double v9, v4, v16

    if-ltz v9, :cond_b

    const/4 v9, 0x1

    goto :goto_4

    :cond_b
    const/4 v9, 0x0

    :goto_4
    if-eqz p3, :cond_c

    if-eqz v9, :cond_c

    .line 81
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    goto :goto_5

    :cond_c
    div-long v4, v2, v6

    :goto_5
    const-wide/16 v12, 0x18

    if-eqz p3, :cond_d

    const-wide/16 v18, 0xc

    goto :goto_6

    :cond_d
    move-wide/from16 v18, v12

    :goto_6
    cmp-long v20, v4, v10

    if-lez v20, :cond_e

    cmp-long v20, v4, v18

    if-ltz v20, :cond_f

    .line 86
    :cond_e
    sget-object v14, Lcom/squareup/util/ShortTimes$MaxUnit;->HOUR:Lcom/squareup/util/ShortTimes$MaxUnit;

    if-ne v1, v14, :cond_13

    :cond_f
    if-eqz p3, :cond_10

    if-eqz v9, :cond_10

    move-wide v2, v10

    goto :goto_7

    .line 87
    :cond_10
    rem-long/2addr v2, v6

    .line 93
    :goto_7
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    .line 94
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    cmp-long v5, v2, v10

    if-eqz v5, :cond_11

    .line 97
    sget-object v2, Lcom/squareup/util/ShortTimes$MaxUnit;->HOUR:Lcom/squareup/util/ShortTimes$MaxUnit;

    if-eq v1, v2, :cond_11

    .line 98
    sget v1, Lcom/squareup/utilities/R$string;->time_hour_min:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v8, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    goto :goto_8

    .line 101
    :cond_11
    sget v1, Lcom/squareup/utilities/R$string;->time_hour:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    :goto_8
    const-string v2, "hour"

    .line 103
    invoke-virtual {v1, v2, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz p5, :cond_12

    .line 104
    invoke-static {v0, v1}, Lcom/squareup/util/ShortTimes;->appendAgo(Lcom/squareup/util/Res;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_12
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_13
    long-to-double v2, v4

    const-wide/high16 v6, 0x4038000000000000L    # 24.0

    div-double/2addr v2, v6

    cmpl-double v6, v2, v16

    if-lez v6, :cond_14

    const/4 v6, 0x1

    goto :goto_9

    :cond_14
    const/4 v6, 0x0

    :goto_9
    if-eqz p3, :cond_15

    if-eqz v6, :cond_15

    .line 109
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto :goto_a

    :cond_15
    div-long v2, v4, v12

    :goto_a
    const-wide/16 v4, 0x7

    if-eqz p3, :cond_16

    const-wide/16 v6, 0x4

    goto :goto_b

    :cond_16
    move-wide v6, v4

    :goto_b
    cmp-long v8, v2, v10

    if-lez v8, :cond_17

    cmp-long v8, v2, v6

    if-ltz v8, :cond_18

    .line 114
    :cond_17
    sget-object v6, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    if-ne v1, v6, :cond_1b

    .line 115
    :cond_18
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v4, 0x1

    cmp-long v6, v2, v4

    if-gtz v6, :cond_19

    .line 118
    sget v1, Lcom/squareup/utilities/R$string;->time_day:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_c

    .line 120
    :cond_19
    sget v2, Lcom/squareup/utilities/R$string;->time_days:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "days"

    .line 121
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 122
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    :goto_c
    if-eqz p5, :cond_1a

    .line 124
    invoke-static {v0, v1}, Lcom/squareup/util/ShortTimes;->appendAgo(Lcom/squareup/util/Res;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_1a
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1b
    long-to-double v6, v2

    const-wide/high16 v8, 0x401c000000000000L    # 7.0

    div-double/2addr v6, v8

    cmpl-double v1, v6, v16

    if-lez v1, :cond_1c

    const/4 v15, 0x1

    :cond_1c
    if-eqz p3, :cond_1d

    if-eqz v15, :cond_1d

    .line 129
    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    goto :goto_d

    :cond_1d
    div-long v1, v2, v4

    .line 133
    :goto_d
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 134
    sget v2, Lcom/squareup/utilities/R$string;->time_week:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "week"

    .line 135
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz p5, :cond_1e

    .line 137
    invoke-static {v0, v1}, Lcom/squareup/util/ShortTimes;->appendAgo(Lcom/squareup/util/Res;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_1e
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static shortTimeSince(Lcom/squareup/util/Res;JZZ)Ljava/lang/String;
    .locals 6

    .line 42
    sget-object v4, Lcom/squareup/util/ShortTimes$MaxUnit;->WEEK:Lcom/squareup/util/ShortTimes$MaxUnit;

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
