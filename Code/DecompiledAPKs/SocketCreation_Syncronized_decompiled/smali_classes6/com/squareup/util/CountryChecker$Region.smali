.class final Lcom/squareup/util/CountryChecker$Region;
.super Ljava/lang/Object;
.source "CountryChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/CountryChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Region"
.end annotation


# static fields
.field private static final METERS_PER_DEGREE_LATITUDE:D = 111000.0


# instance fields
.field private final polygonEdges:[[Lcom/squareup/util/CountryChecker$LineSegment;


# direct methods
.method private varargs constructor <init>([[D)V
    .locals 3

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    array-length v0, p1

    new-array v0, v0, [[Lcom/squareup/util/CountryChecker$LineSegment;

    iput-object v0, p0, Lcom/squareup/util/CountryChecker$Region;->polygonEdges:[[Lcom/squareup/util/CountryChecker$LineSegment;

    const/4 v0, 0x0

    .line 151
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/squareup/util/CountryChecker$Region;->polygonEdges:[[Lcom/squareup/util/CountryChecker$LineSegment;

    aget-object v2, p1, v0

    invoke-static {v2}, Lcom/squareup/util/CountryChecker$Region;->getEdges([D)[Lcom/squareup/util/CountryChecker$LineSegment;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>([[DLcom/squareup/util/CountryChecker$1;)V
    .locals 0

    .line 142
    invoke-direct {p0, p1}, Lcom/squareup/util/CountryChecker$Region;-><init>([[D)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/util/CountryChecker$Region;Lcom/squareup/util/CountryChecker$Point;D)Z
    .locals 0

    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/util/CountryChecker$Region;->roughlyContains(Lcom/squareup/util/CountryChecker$Point;D)Z

    move-result p0

    return p0
.end method

.method private contains(Lcom/squareup/util/CountryChecker$Point;)Z
    .locals 5

    .line 188
    iget-object v0, p0, Lcom/squareup/util/CountryChecker$Region;->polygonEdges:[[Lcom/squareup/util/CountryChecker$LineSegment;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 189
    invoke-static {v4, p1}, Lcom/squareup/util/CountryChecker$Region;->contains([Lcom/squareup/util/CountryChecker$LineSegment;Lcom/squareup/util/CountryChecker$Point;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private static contains([Lcom/squareup/util/CountryChecker$LineSegment;Lcom/squareup/util/CountryChecker$Point;)Z
    .locals 19

    move-object/from16 v0, p0

    .line 211
    invoke-static/range {p1 .. p1}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v1

    .line 212
    invoke-static/range {p1 .. p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v3

    .line 213
    array-length v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_0
    if-ge v6, v5, :cond_1

    aget-object v8, v0, v6

    .line 214
    invoke-static {v8}, Lcom/squareup/util/CountryChecker$LineSegment;->access$400(Lcom/squareup/util/CountryChecker$LineSegment;)Lcom/squareup/util/CountryChecker$Point;

    move-result-object v9

    invoke-static {v9}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v9

    .line 215
    invoke-static {v8}, Lcom/squareup/util/CountryChecker$LineSegment;->access$400(Lcom/squareup/util/CountryChecker$LineSegment;)Lcom/squareup/util/CountryChecker$Point;

    move-result-object v11

    invoke-static {v11}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v11

    .line 216
    invoke-static {v8}, Lcom/squareup/util/CountryChecker$LineSegment;->access$500(Lcom/squareup/util/CountryChecker$LineSegment;)Lcom/squareup/util/CountryChecker$Point;

    move-result-object v13

    invoke-static {v13}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v13

    .line 217
    invoke-static {v8}, Lcom/squareup/util/CountryChecker$LineSegment;->access$500(Lcom/squareup/util/CountryChecker$LineSegment;)Lcom/squareup/util/CountryChecker$Point;

    move-result-object v8

    invoke-static {v8}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v15

    cmpg-double v8, v11, v3

    if-gtz v8, :cond_0

    cmpg-double v8, v3, v15

    if-gez v8, :cond_0

    sub-double v17, v1, v9

    sub-double/2addr v15, v11

    mul-double v17, v17, v15

    sub-double/2addr v13, v9

    sub-double v8, v3, v11

    mul-double v13, v13, v8

    cmpg-double v8, v17, v13

    if-gez v8, :cond_0

    xor-int/lit8 v7, v7, 0x1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    return v7
.end method

.method private static getEdges([D)[Lcom/squareup/util/CountryChecker$LineSegment;
    .locals 11

    .line 157
    array-length v0, p0

    const/4 v1, 0x1

    shr-int/2addr v0, v1

    .line 158
    new-array v2, v0, [Lcom/squareup/util/CountryChecker$LineSegment;

    .line 159
    new-instance v3, Lcom/squareup/util/CountryChecker$Point;

    aget-wide v4, p0, v1

    const/4 v1, 0x0

    aget-wide v6, p0, v1

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/squareup/util/CountryChecker$Point;-><init>(DD)V

    :goto_0
    if-ge v1, v0, :cond_0

    add-int/lit8 v4, v1, 0x1

    .line 161
    rem-int v5, v4, v0

    .line 162
    new-instance v6, Lcom/squareup/util/CountryChecker$Point;

    mul-int/lit8 v5, v5, 0x2

    add-int/lit8 v7, v5, 0x1

    aget-wide v7, p0, v7

    aget-wide v9, p0, v5

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/squareup/util/CountryChecker$Point;-><init>(DD)V

    .line 163
    new-instance v5, Lcom/squareup/util/CountryChecker$LineSegment;

    invoke-direct {v5, v3, v6}, Lcom/squareup/util/CountryChecker$LineSegment;-><init>(Lcom/squareup/util/CountryChecker$Point;Lcom/squareup/util/CountryChecker$Point;)V

    aput-object v5, v2, v1

    move v1, v4

    move-object v3, v6

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private static intersects([Lcom/squareup/util/CountryChecker$LineSegment;Lcom/squareup/util/CountryChecker$Circle;)Z
    .locals 4

    .line 235
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p0, v2

    .line 236
    invoke-virtual {v3, p1}, Lcom/squareup/util/CountryChecker$LineSegment;->intersects(Lcom/squareup/util/CountryChecker$Circle;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private static metersToLatitude(D)D
    .locals 2

    const-wide v0, 0x40fb198000000000L    # 111000.0

    div-double/2addr p0, v0

    return-wide p0
.end method

.method private static metersToLongitude(DD)D
    .locals 2

    const-wide v0, 0x400921fb54442d18L    # Math.PI

    mul-double p2, p2, v0

    const-wide v0, 0x4066800000000000L    # 180.0

    div-double/2addr p2, v0

    .line 203
    invoke-static {p2, p3}, Ljava/lang/Math;->cos(D)D

    move-result-wide p2

    const-wide v0, 0x3f50624dd2f1a9fcL    # 0.001

    .line 205
    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide p2

    const-wide v0, 0x40fb198000000000L    # 111000.0

    mul-double p2, p2, v0

    div-double/2addr p0, p2

    return-wide p0
.end method

.method private roughlyContains(Lcom/squareup/util/CountryChecker$Point;D)Z
    .locals 6

    .line 170
    invoke-direct {p0, p1}, Lcom/squareup/util/CountryChecker$Region;->contains(Lcom/squareup/util/CountryChecker$Point;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 174
    :cond_0
    invoke-static {p2, p3}, Lcom/squareup/util/CountryChecker$Region;->metersToLatitude(D)D

    move-result-wide v2

    .line 175
    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v4

    invoke-static {p2, p3, v4, v5}, Lcom/squareup/util/CountryChecker$Region;->metersToLongitude(DD)D

    move-result-wide p2

    .line 176
    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->max(DD)D

    move-result-wide p2

    .line 177
    new-instance v0, Lcom/squareup/util/CountryChecker$Circle;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/util/CountryChecker$Circle;-><init>(Lcom/squareup/util/CountryChecker$Point;D)V

    .line 179
    iget-object p1, p0, Lcom/squareup/util/CountryChecker$Region;->polygonEdges:[[Lcom/squareup/util/CountryChecker$LineSegment;

    array-length p2, p1

    const/4 p3, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_2

    aget-object v3, p1, v2

    .line 180
    invoke-static {v3, v0}, Lcom/squareup/util/CountryChecker$Region;->intersects([Lcom/squareup/util/CountryChecker$LineSegment;Lcom/squareup/util/CountryChecker$Circle;)Z

    move-result v3

    if-eqz v3, :cond_1

    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return p3
.end method
