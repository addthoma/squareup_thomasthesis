.class final Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;
.super Ljava/lang/Object;
.source "RxContentObserver.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RxContentObserverKt;->listenForChanges(Landroid/content/ContentResolver;Landroid/net/Uri;ZLrx/Emitter$BackpressureMode;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lrx/Emitter<",
        "TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lrx/Emitter;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contentUri:Landroid/net/Uri;

.field final synthetic $notifyForDescendants:Z

.field final synthetic $this_listenForChanges:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->$this_listenForChanges:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->$contentUri:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->$notifyForDescendants:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Emitter;

    invoke-virtual {p0, p1}, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->call(Lrx/Emitter;)V

    return-void
.end method

.method public final call(Lrx/Emitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1$observer$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1$observer$1;-><init>(Lrx/Emitter;Landroid/os/Handler;)V

    .line 39
    new-instance v1, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1$1;-><init>(Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;Lcom/squareup/util/RxContentObserverKt$listenForChanges$1$observer$1;)V

    check-cast v1, Lrx/functions/Cancellable;

    invoke-interface {p1, v1}, Lrx/Emitter;->setCancellation(Lrx/functions/Cancellable;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->$this_listenForChanges:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->$contentUri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;->$notifyForDescendants:Z

    check-cast v0, Landroid/database/ContentObserver;

    invoke-virtual {p1, v1, v2, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method
