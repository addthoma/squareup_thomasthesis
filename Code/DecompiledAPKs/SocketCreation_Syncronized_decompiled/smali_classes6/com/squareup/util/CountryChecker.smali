.class public Lcom/squareup/util/CountryChecker;
.super Ljava/lang/Object;
.source "CountryChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/CountryChecker$LineSegment;,
        Lcom/squareup/util/CountryChecker$Circle;,
        Lcom/squareup/util/CountryChecker$Point;,
        Lcom/squareup/util/CountryChecker$Region;
    }
.end annotation


# static fields
.field private static final CANADA:Lcom/squareup/util/CountryChecker$Region;

.field private static final CANADA_POINTS:[D

.field private static final JAPAN:Lcom/squareup/util/CountryChecker$Region;

.field private static final JAPAN_POINTS:[D

.field private static final USA:Lcom/squareup/util/CountryChecker$Region;

.field private static final USA_ALASKA_POINTS:[D

.field private static final USA_ALASKA_WEST_POINTS:[D

.field private static final USA_HAWAII_POINTS:[D

.field private static final USA_LOWER_48_POINTS:[D


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x84

    new-array v0, v0, [D

    .line 13
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/util/CountryChecker;->CANADA_POINTS:[D

    const/16 v0, 0x2c

    new-array v0, v0, [D

    .line 35
    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/util/CountryChecker;->JAPAN_POINTS:[D

    const/16 v0, 0xc2

    new-array v0, v0, [D

    .line 45
    fill-array-data v0, :array_2

    sput-object v0, Lcom/squareup/util/CountryChecker;->USA_LOWER_48_POINTS:[D

    const/16 v0, 0x8

    new-array v0, v0, [D

    .line 91
    fill-array-data v0, :array_3

    sput-object v0, Lcom/squareup/util/CountryChecker;->USA_HAWAII_POINTS:[D

    const/16 v0, 0x28

    new-array v0, v0, [D

    .line 97
    fill-array-data v0, :array_4

    sput-object v0, Lcom/squareup/util/CountryChecker;->USA_ALASKA_POINTS:[D

    const/16 v0, 0xa

    new-array v0, v0, [D

    .line 107
    fill-array-data v0, :array_5

    sput-object v0, Lcom/squareup/util/CountryChecker;->USA_ALASKA_WEST_POINTS:[D

    .line 112
    new-instance v0, Lcom/squareup/util/CountryChecker$Region;

    const/4 v1, 0x1

    new-array v2, v1, [[D

    sget-object v3, Lcom/squareup/util/CountryChecker;->CANADA_POINTS:[D

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/squareup/util/CountryChecker$Region;-><init>([[DLcom/squareup/util/CountryChecker$1;)V

    sput-object v0, Lcom/squareup/util/CountryChecker;->CANADA:Lcom/squareup/util/CountryChecker$Region;

    .line 113
    new-instance v0, Lcom/squareup/util/CountryChecker$Region;

    new-array v2, v1, [[D

    sget-object v5, Lcom/squareup/util/CountryChecker;->JAPAN_POINTS:[D

    aput-object v5, v2, v4

    invoke-direct {v0, v2, v3}, Lcom/squareup/util/CountryChecker$Region;-><init>([[DLcom/squareup/util/CountryChecker$1;)V

    sput-object v0, Lcom/squareup/util/CountryChecker;->JAPAN:Lcom/squareup/util/CountryChecker$Region;

    .line 114
    new-instance v0, Lcom/squareup/util/CountryChecker$Region;

    const/4 v2, 0x4

    new-array v2, v2, [[D

    sget-object v5, Lcom/squareup/util/CountryChecker;->USA_LOWER_48_POINTS:[D

    aput-object v5, v2, v4

    sget-object v4, Lcom/squareup/util/CountryChecker;->USA_HAWAII_POINTS:[D

    aput-object v4, v2, v1

    sget-object v1, Lcom/squareup/util/CountryChecker;->USA_ALASKA_POINTS:[D

    const/4 v4, 0x2

    aput-object v1, v2, v4

    sget-object v1, Lcom/squareup/util/CountryChecker;->USA_ALASKA_WEST_POINTS:[D

    const/4 v4, 0x3

    aput-object v1, v2, v4

    invoke-direct {v0, v2, v3}, Lcom/squareup/util/CountryChecker$Region;-><init>([[DLcom/squareup/util/CountryChecker$1;)V

    sput-object v0, Lcom/squareup/util/CountryChecker;->USA:Lcom/squareup/util/CountryChecker$Region;

    return-void

    :array_0
    .array-data 8
        -0x3f9e558000000000L    # -141.328125
        0x40516818c197e564L    # 69.626511
        -0x3f9e558000000000L    # -141.328125
        0x404e191ba3ca7504L    # 60.196156
        -0x3f9ecd0800eae18bL    # -137.592773
        0x404d5992428d434aL    # 58.699776
        -0x3f9f0c5000c9539cL    # -135.615234
        0x404dbeaf5fd47beeL    # 59.489727
        -0x3f9fb50fffbce421L    # -130.341797
        0x404b93aea3161a1eL    # 55.153767
        -0x3f9f4057ff9b5632L    # -133.989258
        0x404b253b9f127f5fL    # 54.290882
        -0x3f9f6a8800eae18bL    # -132.670898
        0x4049ee7442c7fbadL    # 51.862923
        -0x3fa0d4effe2a3ceaL    # -124.672852
        0x404836effe2a3ceaL    # 48.429199
        -0x3fa12eeffe2a3ceaL    # -123.266602
        0x404811819d2391d5L    # 48.136768
        -0x3fa1483ffef39086L    # -122.871094
        0x40482f780346dc5dL    # 48.37085
        -0x3fa14de0008637bdL    # -122.783203
        0x404879c65c70435fL    # 48.951366
        -0x3fa84230014f8b59L    # -94.96582
        0x404876148344c37eL    # 48.922501
        -0x3fa8531001d5c316L    # -94.702148
        0x40484992c061847fL    # 48.574791
        -0x3fa99c1fff79c843L    # -89.560547
        0x4047dcbde3fbbd7bL    # 47.724545
        -0x3fa9edafff36ac64L    # -88.286133
        0x404802793dd97f63L    # 48.019325
        -0x3fab4a7000431bdfL    # -82.836914
        0x40469b43bf727137L    # 45.213005
        -0x3fab5b5000c9539cL    # -82.573242
        0x4045ce5d206c8712L    # 43.612217
        -0x3fab64cdfefbf402L    # -82.424927
        0x40458431832fcac9L    # 43.032761
        -0x3fab48db0142f61fL    # -82.861633
        0x4045303b3e9a6f82L    # 42.376808
        -0x3fab39e9ff0cbaf9L    # -83.095093
        0x4045282d1f1cfbb9L    # 42.313877
        -0x3fab3692ff4ba51aL    # -83.147278
        0x40451c358298cc14L    # 42.220383
        -0x3fab352b020c49baL    # -83.16925
        0x404502647fdc5932L    # 42.018692
        -0x3fab3b52007dd441L    # -83.07312
        0x4044e7723ee1bd1fL    # 41.808174
        -0x3fab55afff36ac64L    # -82.661133
        0x4044d3819d2391d5L    # 41.652393
        -0x3fab6ce3ffef3908L    # -82.298584
        0x4044d55842b734b5L    # 41.666756
        -0x3fac45a1016ce78aL    # -78.912048
        0x4045679ddc1e7968L    # 42.809505
        -0x3fac45cdfefbf402L    # -78.909302
        0x40457a2a23bff8a9L    # 42.954411
        -0x3fac3709fe86833cL    # -79.140015
        0x4045b976a2f48c2eL    # 43.448933
        -0x3facd214013ec461L    # -76.717529
        0x4045cbd15f02c4d6L    # 43.592327
        -0x3faceee7ff583a54L    # -76.26709
        0x404612473cd57364L    # 44.142799
        -0x3fad499bfdf7e804L    # -74.849854
        0x40467d7be121ee67L    # 44.980343
        -0x3fae206a012599edL    # -71.49353
        0x40467dfb1e18efbbL    # 44.984226
        -0x3fae2c5dfeb8d823L    # -71.306763
        0x40469cc01e68a0d3L    # 45.224613
        -0x3fae4b4e0114d2f6L    # -70.823364
        0x40469a45fe111277L    # 45.205261
        -0x3fae5ac60029f16bL    # -70.581665
        0x4046b37ec3547e07L    # 45.402306
        -0x3faeb2aa00192a73L    # -69.208374
        0x4047b46aa087ca64L    # 47.409504
        -0x3faebc8201cd5f9aL    # -69.054565
        0x404794effe2a3ceaL    # 47.163574
        -0x3faec7c200c0f020L    # -68.878784
        0x404790a23bff8a8fL    # 47.129951
        -0x3faef08a009f6230L    # -68.241577
        0x4047a4b1feeb2d0aL    # 47.286682
        -0x3faf0b41fea8112cL    # -67.824097
        0x404784aa00192a73L    # 47.036438
        -0x3faf09da0168b5ccL    # -67.846069
        0x4046d3059ea57215L    # 45.648609
        -0x3faf200000000000L    # -67.5
        0x4046c4c0df58c08bL    # 45.537136
        -0x3faf1b14013ec461L    # -67.576904
        0x4046bbe1fc08fa7bL    # 45.467834
        -0x3faf216801711948L    # -67.478027
        0x4046af0cfe154435L    # 45.367584
        -0x3faf1c21ff2e48e9L    # -67.560425
        0x4046a3ae1cde5d18L    # 45.278751
        -0x3faf243800218defL    # -67.434082
        0x40468de0bd44998dL    # 45.108421
        -0x3faf3464020817fcL    # -67.181396
        0x40468be4a3832767L    # 45.092915
        -0x3faf3d8800eae18bL    # -67.038574
        0x40466e8c3f3e0371L    # 44.863655
        -0x3faf3c79fec99f1bL    # -67.055054
        0x404575463cfb3312L    # 42.916206
        -0x3fafbee7ff583a54L    # -65.01709
        0x404560223e186983L    # 42.751045
        -0x3fb60ce0008637bdL    # -51.899414
        0x40466592641b328bL    # 44.79353
        -0x3fb6eddffc5479d5L    # -50.141602
        0x40480dc044284dfdL    # 48.10743
        -0x3fb3914003254e6eL    # -56.865234
        0x404efd7963992c8cL    # 61.980267
        -0x3fb0187ffde7210cL    # -63.808594
        0x405254fba01eeed9L    # 73.327858
        -0x3fad4d1fff79c843L    # -74.794922
        0x40530fdc4007570cL    # 76.247818
        -0x3fadbda00192a737L    # -73.037109
        0x4053bd177ea1c68fL    # 78.954559
        -0x3faecba00192a737L    # -68.818359
        0x4053f8f1800a7c5bL    # 79.88974
        -0x3faf14c0010c6f7aL    # -67.675781
        0x40542df6be37de94L    # 80.718185
        -0x3fb0727ffde7210cL    # -63.105469
        0x40546056de33269eL    # 81.505302
        -0x3fb0d7c0010c6f7aL    # -62.314453
        0x405477e9c0229a5fL    # 81.873642
        -0x3fb24b000431bde8L    # -59.414062
        0x405499c13fd0d068L    # 82.40242
        -0x3fb045800218def4L    # -63.457031
        0x4054bef01fb82c2cL    # 82.983406
        -0x3fae287ffde7210cL    # -71.367188
        0x4054d0743e963dc5L    # 83.257095
        -0x3fab5e1fff79c843L    # -82.529297
        0x4054c7c8605681edL    # 83.121605
        -0x3fa0c6e0008637bdL    # -124.892578
        0x40532262fdfc19c1L    # 76.537292
        -0x3f9e558000000000L    # -141.328125
        0x40516818c197e564L    # 69.626511
    .end array-data

    :array_1
    .array-data 8
        0x405ea6e0008637bdL    # 122.607422
        0x4038f755043e5322L    # 24.966141
        0x405e98cffeb074a7L    # 122.387695
        0x4037a4ff865d7cb3L    # 23.644524
        0x405fc88fffbce421L    # 127.133789
        0x4037c3e6a337a80dL    # 23.765238
        0x40609de7ff583a54L    # 132.93457
        0x403efa44a6223e18L    # 30.97761
        0x40610de100607810L    # 136.433716
        0x404036923e5b8562L    # 32.426338
        0x40617ee7ff583a54L    # 139.96582
        0x40402260029f16b1L    # 32.268555
        0x4061abe7ff583a54L    # 141.37207
        0x40411e047d3d4281L    # 34.234512
        0x4062318000000000L    # 145.546875
        0x4044bce25c810a57L    # 41.475658
        0x4062793800218defL    # 147.788086
        0x4045be0e6299524cL    # 43.484814
        0x4062a20000000000L    # 149.0625
        0x40466f8b827fa1a1L    # 44.871445
        0x40629f2fff36ac64L    # 148.974609
        0x4047112c83ec892bL    # 46.134171
        0x40619b0800eae18bL    # 140.844727
        0x4046da642396073eL    # 45.706181
        0x40612ec0010c6f7aL    # 137.460938
        0x4043af23c42a66dcL    # 39.368279
        0x4060fef000431bdfL    # 135.966797
        0x40428b8cfbfc6541L    # 37.09024
        0x4060768800eae18bL    # 131.704102
        0x4042088ede54b48dL    # 36.06686
        0x4060287c0096feb5L    # 129.265137
        0x40415d315d701d9fL    # 34.728069
        0x405fdf1001d5c316L    # 127.485352
        0x404070963dc486adL    # 32.879585
        0x40600662003ab863L    # 128.199463
        0x403fe90342aa9f7bL    # 31.910206
        0x4060238fffbce421L    # 129.111328
        0x403fb0d72799a1fdL    # 31.690783
        0x406019b800218defL    # 128.803711
        0x403d8904e618ce2dL    # 29.535231
        0x405f8aafff36ac64L    # 126.166992
        0x403a8c99e0e73605L    # 26.549223
        0x405ea6e0008637bdL    # 122.607422
        0x4038f755043e5322L    # 24.966141
    .end array-data

    :array_2
    .array-data 8
        -0x3fa996801f75104dL    # -89.64843
        0x403c9d0afe60c38fL    # 28.613449
        -0x3faa7feffe2a3ceaL    # -86.000977
        0x403d1f955f78359cL    # 29.123373
        -0x3fab86e7ff583a54L    # -81.89209
        0x40384832873bc904L    # 24.282021
        -0x3fac0c801f75104dL    # -79.80468
        0x4038497a24894c44L    # 24.28702
        -0x3fac51681ecd4aa1L    # -78.72802
        0x403fb0d7060bb2bcL    # 31.690781
        -0x3fad9be0008637bdL    # -73.564453
        0x404155ad42c3c9efL    # 34.66935
        -0x3fadb80000000000L    # -73.125
        0x4043e32b5e529baeL    # 39.774761
        -0x3fae93601ffb480aL    # -69.697258
        0x404485ea3df6db94L    # 41.046211
        -0x3faeaf800218def4L    # -69.257812
        0x4045b1cd5f99c38bL    # 43.38908
        -0x3faf4a6bd86cfd1cL    # -66.83716763836452
        0x40466e398270e982L    # 44.86113005173958
        -0x3faf26ab2e4c8a15L    # -67.39580194974376
        0x4046cd6b81f65af3L    # 45.60484337357584
        -0x3faf0f576429ab39L    # -67.76029106074121
        0x4046d727579c4d19L    # 45.68088812953847
        -0x3faf0e88e60f9b79L    # -67.77289436794136
        0x40478eb500215c36L    # 47.11489869717518
        -0x3faef2b27923c3c1L    # -68.20785686023099
        0x4047ad54c70ff518L    # 47.35414970664186
        -0x3faea2779cb36a81L    # -69.46144945601371
        0x4047c37ded76062aL    # 47.52728050482649
        -0x3fae6fc08868085dL    # -70.25387372818527
        0x4047223d2e3217bdL    # 46.26749207921919
        -0x3fae4cb079386fa0L    # -70.80172891129178
        0x4046b20e1e539089L    # 45.39105586128523
        -0x3fae256315c73c74L    # -71.41582732718797
        0x4046a403695b9a56L    # 45.28135411236569
        -0x3fae201eea3d2661L    # -71.49811309841469
        0x404681d1474b510eL    # 45.01419917275972
        -0x3fad4a4f78cc5333L    # -74.83889942213428
        0x40467dcb7d7831e6L    # 44.98277252549069
        -0x3fad357b48d33b21L    # -75.16435031291938
        0x404671e59f547cc1L    # 44.88982001901014
        -0x3fad1b17d3aea9cdL    # -75.5766707224182
        0x40465287fc0f04d6L    # 44.64477492078156
        -0x3facd11b9e426c4fL    # -76.73268931878444
        0x4045ed79c3cbd9e3L    # 43.85527846769016
        -0x3fac39652a8c1cf4L    # -79.10320030514794
        0x4045b0aab4643f9cL    # 43.3802094926352
        -0x3fac3cb1759d76e7L    # -79.05166873571979
        0x40459aaca5bd5b24L    # 43.20839378114604
        -0x3fac3b666031c8ecL    # -79.07187647951315
        0x40458a24140dd495L    # 43.07922602343857
        -0x3fac3ec3345c7f4aL    # -79.01933566062038
        0x404586b7432cde91L    # 43.05246772471322
        -0x3fac3e04607579c0L    # -79.03098286178374
        0x40457f7ccf2b2424L    # 42.99599637609961
        -0x3fac460b94582b29L    # -78.90554324521814
        0x404574bdf992656fL    # 42.91204757354978
        -0x3fac442b6aa8ad84L    # -78.93485005881843
        0x40456a1f584dff18L    # 42.82908157165929
        -0x3fab5e2c8db614c1L    # -82.52853066652914
        0x4044ec6321df6b2aL    # 41.84677527817833
        -0x3fab38ffed1e5d02L    # -83.10937950167497
        0x4044ff2b6b862d20L    # 41.99351257373405
        -0x3fab37c9cb512e49L    # -83.12830845901668
        0x40451e4ee7f32ce9L    # 42.23678302167702
        -0x3fab3a8e201a4a77L    # -83.08507535393268
        0x404526fc754d7d9fL    # 42.3045794132588
        -0x3fab3ccfb4a9399fL    # -83.04982264970887
        0x40452916a5c45077L    # 42.3210036476675
        -0x3fab42b0cec6f9dfL    # -82.95795851296181
        0x40452b81ad9df3fbL    # 42.33989496433784
        -0x3fab4adffa4f78c2L    # -82.83007948149768
        0x40452fb0f6939d52L    # 42.3725879879736
        -0x3fab5f064d523e5dL    # -82.51524035423658
        0x40454e8bc64c8de2L    # 42.61364058244705
        -0x3fab5f5976689a9eL    # -82.51016464028137
        0x4045556716f1e843L    # 42.6672085457981
        -0x3fab62269bf6e037L    # -82.4663934792617
        0x404561f2b17d3df1L    # 42.76521891227083
        -0x3fab613429078d6fL    # -82.48119138967947
        0x4045677c89367049L    # 42.80848803671012
        -0x3fab627a88d2e580L    # -82.46127108959809
        0x40457612aa52c633L    # 42.92244462053022
        -0x3fab652a93ba45f2L    # -82.41927630242591
        0x40457b543538e34eL    # 42.96350732114534
        -0x3fab566bdca30c0cL    # -82.64966663434444
        0x4046b5a0d3ea0cabL    # 45.41897057464788
        -0x3fab1bc4f65791c5L    # -83.56610337684917
        0x4046e82e37007da3L    # 45.81391036533321
        -0x3fab26790fbd56bdL    # -83.39886099347954
        0x404701cabc5eecbaL    # 46.01399950633045
        -0x3fab17686d79fc32L    # -83.63425124251737
        0x404711192e012471L    # 46.13358092361239
        -0x3fab0257702ec5c9L    # -83.96341319496501
        0x40470768cf207457L    # 46.05788601956072
        -0x3faaf719f57441e4L    # -84.13904060026874
        0x4047313528916cecL    # 46.38443476773122
        -0x3faaf9b6c8872530L    # -84.09821879198284
        0x404743e3b04e1250L    # 46.53038600742423
        -0x3faade3fdc1db9b9L    # -84.52735230538484
        0x40473dd699c92b6aL    # 46.48311159441694
        -0x3faad4e54aedf31fL    # -84.67350508463598
        0x404746eeed9e9f94L    # 46.55416651006013
        -0x3faaca08d80426fdL    # -84.84321021646979
        0x404771a3078a95e8L    # 46.88778776423334
        -0x3fa9e6adb29d4dfcL    # -88.39564833296032
        0x404820e6f8703d6fL    # 48.25704865913837
        -0x3fa99259edae802cL    # -89.71326120337409
        0x4048014fcbabef20L    # 48.0102476682207
        -0x3fa9483a9717dbcdL    # -90.871423937512
        0x404821629a57117aL    # 48.26082162143116
        -0x3fa922823e2c2ea3L    # -91.46080060658774
        0x40480b65bb6bdb07L    # 48.08904211775194
        -0x3fa8f5e66ddd8258L    # -92.1578107201409
        0x40482f39a89e2d39L    # 48.36894710277051
        -0x3fa8e7696392a7b3L    # -92.38419256857124
        0x40482225175609a8L    # 48.26675693226872
        -0x3fa8e4268ecda3f8L    # -92.43514661710662
        0x40483864573e9645L    # 48.44056215816503
        -0x3fa8ca0775301a0bL    # -92.84329481413427
        0x40484e4c4dadbf3dL    # 48.61170359596006
        -0x3fa8b0c7ebd120baL    # -93.23779778077468
        0x40485436d147f0a1L    # 48.65792289745992
        -0x3fa8a0e814fff664L    # -93.4858348369948
        0x40484a5ef6ed00a1L    # 48.58102308819185
        -0x3fa872928b954a1cL    # -94.20980558796833
        0x404859aa042cf3b0L    # 48.70050098605077
        -0x3fa855a35bf33ad4L    # -94.66190434691515
        0x4048649755ad6709L    # 48.78586836783331
        -0x3fa84c9985c1dd18L    # -94.80312973087314
        0x4048aa3255500689L    # 49.32966104896065
        -0x3fa835c7303d4eeeL    # -95.15971750271663
        0x4048b1c0ac93f369L    # 49.38869244789277
        -0x3fa835f8eafa4994L    # -95.15668225820417
        0x40487fa96c461ce8L    # 48.99735787795527
        -0x3fa82cb70ee7e669L    # -95.3013270125724
        0x40488190a11c86a3L    # 49.01222623722904
        -0x3fa12b657fb69985L    # -123.32193
        0x404880350d2806afL    # 49.001619
        -0x3fa13f785f8d2e51L    # -123.008278
        0x40486a5c358afc48L    # 48.830939
        -0x3fa13ff2394d41fcL    # -123.0008408303766
        0x40486269728a3d0dL    # 48.76884299993444
        -0x3fa12eff800197a0L    # -123.2656555160952
        0x40485932c5488487L    # 48.69686189690497
        -0x3fa132dcffb10e5cL    # -123.205261303991
        0x4048482340e8321cL    # 48.56357585275347
        -0x3fa135b53fa4f494L    # -123.1608124627216
        0x4048345e019d50d4L    # 48.40911884480889
        -0x3fa12fb1e10e8ec8L    # -123.2547681196055
        0x404824f349508b39L    # 48.28867451126866
        -0x3fa11088b51612c5L    # -123.7416560444298
        0x40481e85e5252d5eL    # 48.23846115412992
        -0x3fa0ecee070d7749L    # -124.2979719513413
        0x40482cce3aa7fd89L    # 48.35004361345916
        -0x3fa0cc45699649d9L    # -124.8082634003009
        0x40483b9bc26d951cL    # 48.46569090223986
        -0x3fa0d7c01e68a0d3L    # -124.628899
        0x404720bf5d78811bL    # 46.25584
        -0x3fa0ad90214ad363L    # -125.288078
        0x4043f471fff79c84L    # 39.909729
        -0x3fa1457021d10b20L    # -122.915031
        0x4041f65aa2e3c537L    # 35.924641
        -0x3fa1d6a2014727ddL    # -120.646362
        0x4040ce15c209246cL    # 33.610039
        -0x3fa299c01e68a0d3L    # -117.597649
        0x40402805a2d72ffdL    # 32.312672
        -0x3fa2b434a01abd1bL    # -117.184288
        0x4040432d01c0ca60L    # 32.524811
        -0x3fa2bed71f36262dL    # -117.01812
        0x404045401c4fc1dfL    # 32.541019
        -0x3fa332adff822bbfL    # -115.20813
        0x40404d1a21ea3593L    # 32.60236
        -0x3fa416d81f106680L    # -111.643059
        0x403f50ebdd334c5eL    # 31.316099
        -0x3fa50eb220791c4cL    # -107.770378
        0x403f4e847b24638dL    # 31.306709
        -0x3fa525e62131a8efL    # -107.407829
        0x403fbdff822bbecbL    # 31.74218
        -0x3fa552e5ffa3b9aeL    # -106.704712
        0x403f98e560418937L    # 31.59725
        -0x3fa58a71fff79c84L    # -105.836792
        0x403f14c6e6d9be4dL    # 31.08116
        -0x3fa5fd681ecd4aa1L    # -104.04052
        0x403c9d0afe60c38fL    # 28.613449
        -0x3fa696b020c49ba6L    # -101.6455
        0x403cf5c7bcc2938eL    # 28.960079
        -0x3fa6b1681ecd4aa1L    # -101.22802
        0x403b1926d8b1dd5dL    # 27.098249
        -0x3fa83f601ffb480aL    # -95.009758
        0x40389b6837f7be12L    # 24.607059
        -0x3fa996801f75104dL    # -89.64843
        0x403c9d0afe60c38fL    # 28.613449
    .end array-data

    :array_3
    .array-data 8
        -0x3f9b54fc0096feb5L    # -165.344238
        0x4038d8b87bdcf030L    # 24.846565
        -0x3f9dfce7ff583a54L    # -144.09668
        0x403943baba7b9171L    # 25.264568
        -0x3f9c7ae3ffef3908L    # -156.159668
        0x402d76fb4c3c18b5L    # 14.732386
        -0x3f9b54fc0096feb5L    # -165.344238
        0x4038d8b87bdcf030L    # 24.846565
    .end array-data

    :array_4
    .array-data 8
        -0x3f9fd12fff36ac64L    # -129.462891
        0x404adfd5c31593e6L    # 53.748711
        -0x3f9fbaafff36ac64L    # -130.166016
        0x404cacd45e9185cfL    # 57.350231
        -0x3f9ef5d0203e63e9L    # -136.318344
        0x404e5bac1d29dc72L    # 60.71619
        -0x3f9e7b77ff151e75L    # -140.141602
        0x404e77185cee17a0L    # 60.930431
        -0x3f9e75d7ff9b5632L    # -140.317383
        0x4051e7d12018a43cL    # 71.622139
        -0x3f9b10c7ffde7211L    # -167.475586
        0x4051cebbe0157eedL    # 71.230217
        -0x3f9ac63ffef39086L    # -169.804688
        0x4050dc3cff64cf8dL    # 67.441223
        -0x3f9af3b19a415f46L    # -168.38457
        0x405058a747d805e6L    # 65.38521
        -0x3f9a6a1800a7c5acL    # -172.68457
        0x404ff2963dc486adL    # 63.89521
        -0x3f9a4a1800a7c5acL    # -173.68457
        0x404e0c2fd75e2047L    # 60.09521
        -0x3f9ac00ccbc05d53L    # -169.998438
        0x404be95629d8409eL    # 55.822942
        -0x3f9a150fffbce421L    # -175.341797
        0x404a6a27418d690aL    # 52.829323
        -0x3f9980000218def4L    # -179.999999
        0x404a9d5a74c09c3dL    # 53.229323
        -0x3f9980000218def4L    # -179.999999
        0x404a2a27418d690aL    # 52.329323
        -0x3f9982bccd0fe8abL    # -179.914453
        0x404981c27e953155L    # 51.013748
        -0x3f9a25f000431bdfL    # -174.814453
        0x404981c27e953155L    # 51.013748
        -0x3f9b60f000431bdfL    # -164.970703
        0x404a9ce5bc87db2bL    # 53.225761
        -0x3f9db69800a7c5acL    # -146.293945
        0x404cafdd00f776c5L    # 57.373932
        -0x3f9e163800218defL    # -143.305664
        0x404cef1ee2435697L    # 57.86813
        -0x3f9fd12fff36ac64L    # -129.462891
        0x404adfd5c31593e6L    # 53.748711
    .end array-data

    :array_5
    .array-data 8
        0x40667ffffde7210cL    # 179.999999
        0x404a9d5a74c09c3dL    # 53.229323
        0x40658af000431bdfL    # 172.341797
        0x404a9d5a74c09c3dL    # 53.229323
        0x40658af000431bdfL    # 172.341797
        0x404a000000000000L    # 52.0
        0x40667ffffde7210cL    # 179.999999
        0x4049800000000000L    # 51.0
        0x40667ffffde7210cL    # 179.999999
        0x404a9d5a74c09c3dL    # 53.229323
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static country(Ljava/lang/String;)Lcom/squareup/util/CountryChecker$Region;
    .locals 4

    .line 129
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 130
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x85e

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x946

    if-eq v0, v1, :cond_1

    const/16 v1, 0xa9e

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "US"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "JP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    const-string v0, "CA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_5

    if-ne v0, v2, :cond_4

    .line 136
    sget-object p0, Lcom/squareup/util/CountryChecker;->JAPAN:Lcom/squareup/util/CountryChecker$Region;

    return-object p0

    .line 138
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No region data for country "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_5
    sget-object p0, Lcom/squareup/util/CountryChecker;->CANADA:Lcom/squareup/util/CountryChecker$Region;

    return-object p0

    .line 132
    :cond_6
    sget-object p0, Lcom/squareup/util/CountryChecker;->USA:Lcom/squareup/util/CountryChecker$Region;

    return-object p0
.end method

.method public static inCountry(Ljava/lang/String;DDD)Z
    .locals 1

    .line 125
    invoke-static {p0}, Lcom/squareup/util/CountryChecker;->country(Ljava/lang/String;)Lcom/squareup/util/CountryChecker$Region;

    move-result-object p0

    new-instance v0, Lcom/squareup/util/CountryChecker$Point;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/util/CountryChecker$Point;-><init>(DD)V

    invoke-static {p0, v0, p5, p6}, Lcom/squareup/util/CountryChecker$Region;->access$100(Lcom/squareup/util/CountryChecker$Region;Lcom/squareup/util/CountryChecker$Point;D)Z

    move-result p0

    return p0
.end method
