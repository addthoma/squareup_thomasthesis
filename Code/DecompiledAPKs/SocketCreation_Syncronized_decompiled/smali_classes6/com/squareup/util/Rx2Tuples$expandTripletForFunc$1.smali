.class final Lcom/squareup/util/Rx2Tuples$expandTripletForFunc$1;
.super Ljava/lang/Object;
.source "Rx2Tuples.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Rx2Tuples;->expandTripletForFunc(Lio/reactivex/functions/Function3;)Lio/reactivex/functions/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lcom/squareup/util/tuple/Triplet<",
        "TA;TB;TC;>;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0001*\u00020\u00032\u0018\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0007H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "R",
        "A",
        "",
        "B",
        "C",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Triplet;",
        "apply",
        "(Lcom/squareup/util/tuple/Triplet;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_expandTripletForFunc:Lio/reactivex/functions/Function3;


# direct methods
.method constructor <init>(Lio/reactivex/functions/Function3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/Rx2Tuples$expandTripletForFunc$1;->$this_expandTripletForFunc:Lio/reactivex/functions/Function3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/tuple/Triplet;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;)TR;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component1()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component2()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component3()Ljava/lang/Object;

    move-result-object p1

    .line 53
    iget-object v2, p0, Lcom/squareup/util/Rx2Tuples$expandTripletForFunc$1;->$this_expandTripletForFunc:Lio/reactivex/functions/Function3;

    invoke-interface {v2, v0, v1, p1}, Lio/reactivex/functions/Function3;->apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/util/tuple/Triplet;

    invoke-virtual {p0, p1}, Lcom/squareup/util/Rx2Tuples$expandTripletForFunc$1;->apply(Lcom/squareup/util/tuple/Triplet;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
