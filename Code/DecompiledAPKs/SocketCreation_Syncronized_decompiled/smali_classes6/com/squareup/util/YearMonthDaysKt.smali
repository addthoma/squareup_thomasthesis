.class public final Lcom/squareup/util/YearMonthDaysKt;
.super Ljava/lang/Object;
.source "YearMonthDays.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0000\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0086\u0002\u001a\u0017\u0010\u0004\u001a\u00020\u0005*\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0086\u0004\u001a\u0017\u0010\u0006\u001a\u00020\u0005*\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0086\u0004\u001a\u0017\u0010\u0007\u001a\u00020\u0005*\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0086\u0004\u001a\u0015\u0010\u0008\u001a\u00020\t*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0086\u0002\u00a8\u0006\n"
    }
    d2 = {
        "compareTo",
        "",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "other",
        "isAfter",
        "",
        "isBefore",
        "isSameDayAs",
        "minus",
        "",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final compareTo(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I
    .locals 1

    const-string v0, "$this$compareTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-static {p0, p1}, Lcom/squareup/util/ProtoDates;->compareYmd(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result p0

    return p0
.end method

.method public static final isAfter(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 11
    invoke-static {p0, p1}, Lcom/squareup/util/YearMonthDaysKt;->compareTo(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 13
    invoke-static {p0, p1}, Lcom/squareup/util/YearMonthDaysKt;->compareTo(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result p0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isSameDayAs(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final minus(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J
    .locals 1

    const-string v0, "$this$minus"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p1, p0}, Lcom/squareup/util/ProtoDates;->countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide p0

    return-wide p0
.end method
