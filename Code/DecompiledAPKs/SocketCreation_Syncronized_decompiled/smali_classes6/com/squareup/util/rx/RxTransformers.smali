.class public final Lcom/squareup/util/rx/RxTransformers;
.super Ljava/lang/Object;
.source "RxTransformers.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static adaptiveSample(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 66
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    return-object v0
.end method

.method public static currentValueOrDefault(Ljava/lang/Object;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 87
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$uG64W1dkykULMPl63lVH2fTzUr8;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$uG64W1dkykULMPl63lVH2fTzUr8;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static debounceWithHysteresis(ILrx/functions/Func1;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lrx/functions/Func1<",
            "TT;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 198
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$G-WEgh28hQp7uLRRaGQAjgKvqfg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$G-WEgh28hQp7uLRRaGQAjgKvqfg;-><init>(ILrx/functions/Func1;)V

    return-object v0
.end method

.method public static distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 101
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$i7sWVhCYm0DEIyJe0ihqHzXQ_QQ;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$i7sWVhCYm0DEIyJe0ihqHzXQ_QQ;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static gateBy(Lrx/Observable;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 179
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$6JA03FvmrTM5bRQOPn7lSImEYmU;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$6JA03FvmrTM5bRQOPn7lSImEYmU;-><init>(Lrx/Observable;)V

    return-object v0
.end method

.method private static isDifferent(Ljava/lang/Object;Ljava/lang/Object;ILrx/functions/Func1;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;I",
            "Lrx/functions/Func1<",
            "TT;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 209
    invoke-interface {p3, p0}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-interface {p3, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sub-int/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result p0

    if-lt p0, p2, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$adaptiveSample$3(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 66
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$cIZvhTkjOjqDGz01RizVymvNKY4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$cIZvhTkjOjqDGz01RizVymvNKY4;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    .line 67
    invoke-virtual {p4, v0}, Lrx/Observable;->publish(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$currentValueOrDefault$4(Ljava/lang/Object;Lrx/Observable;)Lrx/Observable;
    .locals 0

    .line 87
    invoke-static {p0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    invoke-static {p1, p0}, Lrx/Observable;->amb(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    .line 88
    invoke-virtual {p0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$debounceWithHysteresis$18(ILrx/functions/Func1;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 198
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$DTBsvOQoJw5kvzldDgtO-S3d86Y;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$DTBsvOQoJw5kvzldDgtO-S3d86Y;-><init>(ILrx/functions/Func1;)V

    invoke-virtual {p2, v0}, Lrx/Observable;->scan(Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p0

    .line 200
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$distinctUntilChangedWithFirstValueToSkip$5(Ljava/lang/Object;Lrx/Observable;)Lrx/Observable;
    .locals 0

    .line 102
    invoke-virtual {p1, p0}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    .line 103
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    const/4 p1, 0x1

    .line 104
    invoke-virtual {p0, p1}, Lrx/Observable;->skip(I)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$gateBy$16(Lrx/Observable;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 180
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$WXw60eu327Zj390lBCcnPKsxFww;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$WXw60eu327Zj390lBCcnPKsxFww;-><init>(Lrx/Observable;)V

    .line 181
    invoke-virtual {p0, v0}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Ljava/lang/Object;)Lkotlin/Unit;
    .locals 0

    .line 69
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$null$1(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lkotlin/Unit;)Lrx/Observable;
    .locals 0

    const/4 p5, 0x1

    .line 72
    invoke-virtual {p0, p5}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object p5

    .line 73
    invoke-virtual {p0, p1, p2, p3, p4}, Lrx/Observable;->sample(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p0

    .line 71
    invoke-static {p5, p0}, Lrx/Observable;->concat(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$10(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;JLjava/lang/Void;)Lrx/Observable;
    .locals 6

    .line 154
    sget-object p7, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$ZEBQPnulvzlBrzpxe490dkr_zhc;->INSTANCE:Lcom/squareup/util/rx/-$$Lambda$RxTransformers$ZEBQPnulvzlBrzpxe490dkr_zhc;

    .line 155
    invoke-virtual {p0, p7}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 156
    invoke-virtual {p0, p1, p2, p3, p4}, Lrx/Observable;->delaySubscription(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 157
    invoke-static {p0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v4

    move-wide v1, p5

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lrx/Observable;->timeout(JLjava/util/concurrent/TimeUnit;Lrx/Observable;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p0

    const/4 p1, 0x1

    .line 158
    invoke-virtual {p0, p1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$11(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;JLrx/Observable;)Lrx/Observable;
    .locals 9

    .line 153
    new-instance v8, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;

    move-object v0, v8

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;-><init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;J)V

    move-object/from16 v0, p7

    .line 154
    invoke-virtual {v0, v8}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$13(Lrx/Observable;Lkotlin/Unit;)Lrx/Observable;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$15(Lrx/Observable;Ljava/lang/Boolean;)Lrx/Observable;
    .locals 0

    .line 181
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lrx/Observable;->never()Lrx/Observable;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$null$17(ILrx/functions/Func1;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 199
    invoke-static {p2, p3, p0, p1}, Lcom/squareup/util/rx/RxTransformers;->isDifferent(Ljava/lang/Object;Ljava/lang/Object;ILrx/functions/Func1;)Z

    move-result p0

    if-eqz p0, :cond_0

    move-object p2, p3

    :cond_0
    return-object p2
.end method

.method static synthetic lambda$null$2(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;
    .locals 8

    .line 68
    invoke-virtual {p4, p0, p1, p2, p3}, Lrx/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$EXP6quIk8P3iDCxvqDiZVV-Djr4;->INSTANCE:Lcom/squareup/util/rx/-$$Lambda$RxTransformers$EXP6quIk8P3iDCxvqDiZVV-Djr4;

    .line 69
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 70
    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v7, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;

    move-object v1, v7

    move-object v2, p4

    move-wide v3, p0

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$gVcPj1j6yItDzAg1ZVmcq1U41E4;-><init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    .line 71
    invoke-virtual {v0, v7}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$6(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Ljava/lang/Object;)Lrx/Observable;
    .locals 0

    const/4 p5, 0x1

    .line 124
    invoke-virtual {p0, p5}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object p0

    const/4 p5, 0x0

    .line 125
    invoke-virtual {p0, p5}, Lrx/observables/ConnectableObservable;->autoConnect(I)Lrx/Observable;

    move-result-object p0

    .line 126
    invoke-virtual {p0, p1, p2, p3, p4}, Lrx/Observable;->delaySubscription(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p0

    .line 127
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/util/rx/RxTransformers;->adaptiveSample(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable$Transformer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$7(Lrx/Observable;Lkotlin/Unit;)Lrx/Observable;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$9(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$refreshWhen$8(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;
    .locals 7

    .line 122
    new-instance v6, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$BKcdrTAy2RNiI5WSkK1Vx9uTtXQ;

    move-object v0, v6

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$BKcdrTAy2RNiI5WSkK1Vx9uTtXQ;-><init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    .line 123
    invoke-virtual {p5, v6}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 128
    invoke-virtual {p0, p1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    new-instance p1, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$LdLAZ3wUqTMXKGQxRQ2O_lrYznE;

    invoke-direct {p1, p5}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$LdLAZ3wUqTMXKGQxRQ2O_lrYznE;-><init>(Lrx/Observable;)V

    .line 129
    invoke-virtual {p0, p1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$repeatWhen$12(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;JLrx/Observable;)Lrx/Observable;
    .locals 9

    .line 152
    new-instance v8, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$FSYsT2UuoRt1JmfL8XZEHHLON6A;

    move-object v0, v8

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$FSYsT2UuoRt1JmfL8XZEHHLON6A;-><init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;J)V

    move-object/from16 v0, p7

    .line 153
    invoke-virtual {v0, v8}, Lrx/Observable;->repeatWhen(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$resubscribeWhen$14(Lrx/Observable;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 169
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 170
    invoke-virtual {p0, v0}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$YZ_Z_2rTZzqjGvyrBWz0tnSOnbw;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$YZ_Z_2rTZzqjGvyrBWz0tnSOnbw;-><init>(Lrx/Observable;)V

    .line 171
    invoke-virtual {p0, v0}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static refreshWhen(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable$Transformer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 122
    new-instance v6, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;

    move-object v0, v6

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$Q5uH9dC6dOQQ2aNP3aNM3jFy0gM;-><init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    return-object v6
.end method

.method public static repeatWhen(Lrx/Observable;JJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable$Transformer;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;JJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 152
    new-instance v8, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$1txbC6liumKUsWstg4N6FmEGyBw;

    move-object v0, v8

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p5

    move-object v5, p6

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$1txbC6liumKUsWstg4N6FmEGyBw;-><init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;J)V

    return-object v8
.end method

.method public static resubscribeWhen(Lrx/Observable;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 169
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$uKzZeJQF8O8TBsqVQADP5XHp3Ig;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$uKzZeJQF8O8TBsqVQADP5XHp3Ig;-><init>(Lrx/Observable;)V

    return-object v0
.end method
