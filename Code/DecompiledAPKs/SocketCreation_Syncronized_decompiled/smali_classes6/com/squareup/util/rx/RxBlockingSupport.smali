.class public final Lcom/squareup/util/rx/RxBlockingSupport;
.super Ljava/lang/Object;
.source "RxBlockingSupport.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a)\u0010\u0004\u001a\u0002H\u0005\"\u0004\u0008\u0000\u0010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u00012\u0006\u0010\u0007\u001a\u0002H\u0005H\u0007\u00a2\u0006\u0002\u0010\u0008\u001a-\u0010\t\u001a\u0002H\u0005\"\u0004\u0008\u0000\u0010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u00012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u000b\u00a2\u0006\u0002\u0010\u000c\u001a!\u0010\r\u001a\u0002H\u0005\"\u0004\u0008\u0000\u0010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0001H\u0007\u00a2\u0006\u0002\u0010\u000e\"2\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "NO_ITEM_AVAILABLE_OBSERVABLE",
        "Lrx/Observable;",
        "Lcom/squareup/util/rx/NoItemAvailable;",
        "kotlin.jvm.PlatformType",
        "getValueOrDefault",
        "T",
        "source",
        "defaultValue",
        "(Lrx/Observable;Ljava/lang/Object;)Ljava/lang/Object;",
        "getValueOrProvideDefault",
        "defaultValueProvider",
        "Lkotlin/Function0;",
        "(Lrx/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "getValueOrThrow",
        "(Lrx/Observable;)Ljava/lang/Object;",
        "pure"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final NO_ITEM_AVAILABLE_OBSERVABLE:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/util/rx/NoItemAvailable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/util/rx/NoItemAvailable;->INSTANCE:Lcom/squareup/util/rx/NoItemAvailable;

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/rx/RxBlockingSupport;->NO_ITEM_AVAILABLE_OBSERVABLE:Lrx/Observable;

    return-void
.end method

.method public static final getValueOrDefault(Lrx/Observable;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Write properly reactive code instead."
    .end annotation

    const-string v0, "source"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v0, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrDefault$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrDefault$1;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrProvideDefault(Lrx/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final getValueOrProvideDefault(Lrx/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValueProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object v0, Lcom/squareup/util/rx/RxBlockingSupport;->NO_ITEM_AVAILABLE_OBSERVABLE:Lrx/Observable;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lrx/Observable;->ambWith(Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    .line 68
    new-instance v0, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrProvideDefault$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrProvideDefault$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p0, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 69
    invoke-virtual {p0}, Lrx/Observable;->toBlocking()Lrx/observables/BlockingObservable;

    move-result-object p0

    .line 70
    invoke-virtual {p0}, Lrx/observables/BlockingObservable;->first()Ljava/lang/Object;

    move-result-object p0

    return-object p0

    .line 67
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type rx.Observable<T>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getValueOrThrow(Lrx/Observable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)TT;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Write properly reactive code instead."
    .end annotation

    const-string v0, "source"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;->INSTANCE:Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrProvideDefault(Lrx/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
