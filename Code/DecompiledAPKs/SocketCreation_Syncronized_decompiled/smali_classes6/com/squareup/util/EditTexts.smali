.class public final Lcom/squareup/util/EditTexts;
.super Ljava/lang/Object;
.source "EditTexts.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "disableActionsExceptPaste",
        "",
        "Landroid/widget/EditText;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final disableActionsExceptPaste(Landroid/widget/EditText;)V
    .locals 1

    const-string v0, "$this$disableActionsExceptPaste"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/util/EditTexts$disableActionsExceptPaste$1;

    invoke-direct {v0}, Lcom/squareup/util/EditTexts$disableActionsExceptPaste$1;-><init>()V

    check-cast v0, Landroid/view/ActionMode$Callback;

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    return-void
.end method
