.class public final Lcom/squareup/util/DeviceScreenSizeInfo;
.super Ljava/lang/Object;
.source "DeviceScreenSizeInfo.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDeviceScreenSizeInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DeviceScreenSizeInfo.kt\ncom/squareup/util/DeviceScreenSizeInfo\n*L\n1#1,115:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0016\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J;\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\tR\u0011\u0010\n\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u000b\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\tR\u0011\u0010\u000c\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\tR\u0011\u0010\r\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\tR\u0011\u0010\u000e\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\tR\u0011\u0010\u000f\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\tR\u0011\u0010\u0010\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\t\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "",
        "isPortrait",
        "",
        "isTablet",
        "isTabletAtLeast10Inches",
        "isWidescreen",
        "isAndroidOWithBadAspectRatio",
        "(ZZZZZ)V",
        "()Z",
        "isLandscape",
        "isLandscapeLongTablet",
        "isLongTablet",
        "isMasterDetail",
        "isPhone",
        "isPhoneOrPortraitLessThan10Inches",
        "isTabletLessThan10Inches",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isAndroidOWithBadAspectRatio:Z

.field private final isPortrait:Z

.field private final isTablet:Z

.field private final isTabletAtLeast10Inches:Z

.field private final isWidescreen:Z


# direct methods
.method public constructor <init>(ZZZZZ)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    iput-boolean p2, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    iput-boolean p3, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    iput-boolean p4, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    iput-boolean p5, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    .line 37
    iget-boolean p1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_2

    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "isTablet==false, expected isTabletAtLeast10Inches to also be false."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static synthetic copy$default(Lcom/squareup/util/DeviceScreenSizeInfo;ZZZZZILjava/lang/Object;)Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move p3, p1

    move p4, p7

    move p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/util/DeviceScreenSizeInfo;->copy(ZZZZZ)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    return v0
.end method

.method public final copy(ZZZZZ)Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 7

    new-instance v6, Lcom/squareup/util/DeviceScreenSizeInfo;

    move-object v0, v6

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/util/DeviceScreenSizeInfo;-><init>(ZZZZZ)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/util/DeviceScreenSizeInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/util/DeviceScreenSizeInfo;

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    iget-boolean v1, p1, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    iget-boolean v1, p1, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    iget-boolean v1, p1, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    iget-boolean v1, p1, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    iget-boolean p1, p1, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    if-eqz v2, :cond_4

    goto :goto_0

    :cond_4
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final isAndroidOWithBadAspectRatio()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    return v0
.end method

.method public final isLandscape()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final isLandscapeLongTablet()Z
    .locals 1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isLongTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isLongTablet()Z
    .locals 1

    .line 64
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isMasterDetail()Z
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final isPhone()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final isPhoneOrPortraitLessThan10Inches()Z
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhone()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final isPortrait()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    return v0
.end method

.method public final isTablet()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    return v0
.end method

.method public final isTabletAtLeast10Inches()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    return v0
.end method

.method public final isTabletLessThan10Inches()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isWidescreen()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceScreenSizeInfo(isPortrait="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isTablet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isTabletAtLeast10Inches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isWidescreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isWidescreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isAndroidOWithBadAspectRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
