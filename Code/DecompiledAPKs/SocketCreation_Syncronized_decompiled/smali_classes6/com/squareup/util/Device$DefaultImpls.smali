.class public final Lcom/squareup/util/Device$DefaultImpls;
.super Ljava/lang/Object;
.source "Device.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static isAndroidOWithBadAspectRatio(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 90
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isAndroidOWithBadAspectRatio()Z

    move-result p0

    return p0
.end method

.method public static synthetic isAndroidOWithBadAspectRatio$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isLandscape(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 48
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isLandscape()Z

    move-result p0

    return p0
.end method

.method public static synthetic isLandscape$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isLandscapeLongTablet(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 78
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isLandscapeLongTablet()Z

    move-result p0

    return p0
.end method

.method public static synthetic isLandscapeLongTablet$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isLongTablet(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 84
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isLongTablet()Z

    move-result p0

    return p0
.end method

.method public static synthetic isLongTablet$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isMasterDetail(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 96
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p0

    return p0
.end method

.method public static synthetic isMasterDetail$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isPhone(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 60
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhone()Z

    move-result p0

    return p0
.end method

.method public static synthetic isPhone$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isPhoneOrPortraitLessThan10Inches(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 36
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p0

    return p0
.end method

.method public static synthetic isPhoneOrPortraitLessThan10Inches$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isPortrait(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 42
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait()Z

    move-result p0

    return p0
.end method

.method public static synthetic isPortrait$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isTablet(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 54
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet()Z

    move-result p0

    return p0
.end method

.method public static synthetic isTablet$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isTabletAtLeast10Inches(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 66
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletAtLeast10Inches()Z

    move-result p0

    return p0
.end method

.method public static synthetic isTabletAtLeast10Inches$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method

.method public static isTabletLessThan10Inches(Lcom/squareup/util/Device;)Z
    .locals 0

    .line 72
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletLessThan10Inches()Z

    move-result p0

    return p0
.end method

.method public static synthetic isTabletLessThan10Inches$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use screenSize or currentScreenSize instead."
    .end annotation

    return-void
.end method
