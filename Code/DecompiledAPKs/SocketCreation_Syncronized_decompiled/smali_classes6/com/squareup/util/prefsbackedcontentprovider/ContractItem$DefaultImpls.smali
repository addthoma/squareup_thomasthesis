.class public final Lcom/squareup/util/prefsbackedcontentprovider/ContractItem$DefaultImpls;
.super Ljava/lang/Object;
.source "PrefsBackedContentProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static getPath(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
            "TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 35
    invoke-static {p0}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem$DefaultImpls;->requireName(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getPref(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
            "TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 38
    invoke-interface {p0}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;->getPath()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static requireName(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
            "TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    const-string v0, "this::class.java.simpleName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "$"

    move-object v2, v1

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v0, v2, v5, v3, v4}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    invoke-static {p0, v1, v4, v3, v4}, Lkotlin/text/StringsKt;->substringAfterLast$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-static {v0}, Lkotlin/text/StringsKt;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Anonymous classes must specify path"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_1
    :goto_0
    return-object p0
.end method
