.class public final Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;
.super Ljava/lang/Object;
.source "PrefsHelper.kt"

# interfaces
.implements Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrefsHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrefsHelper.kt\ncom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper\n*L\n1#1,91:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B3\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00028\u0000\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\r\u0010\u0018\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010\u0019J\u0008\u0010\u001a\u001a\u00020\u001bH\u0016J\u0019\u0010\u001c\u001a\u00020\u001d*\u00020\u001d2\u0006\u0010\u001e\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010\u001fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00028\u0000X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\rR\u000e\u0010\u0008\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;",
        "T",
        "",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
        "context",
        "Landroid/content/Context;",
        "fileName",
        "",
        "pref",
        "default",
        "strategy",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;",
        "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;)V",
        "Ljava/lang/Object;",
        "prefs",
        "Landroid/content/SharedPreferences;",
        "doQuery",
        "Landroid/database/Cursor;",
        "doUpdate",
        "",
        "uri",
        "Landroid/net/Uri;",
        "values",
        "Landroid/content/ContentValues;",
        "getPref",
        "()Ljava/lang/Object;",
        "hasPref",
        "",
        "put",
        "Landroid/content/SharedPreferences$Editor;",
        "value",
        "(Landroid/content/SharedPreferences$Editor;Ljava/lang/Object;)Landroid/content/SharedPreferences$Editor;",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final default:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final pref:Ljava/lang/String;

.field private final prefs:Landroid/content/SharedPreferences;

.field private final strategy:Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pref"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "default"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strategy"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->context:Landroid/content/Context;

    iput-object p3, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->pref:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->default:Ljava/lang/Object;

    iput-object p5, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->strategy:Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    .line 48
    iget-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->context:Landroid/content/Context;

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string p2, "context.getSharedPrefere\u2026s(fileName, MODE_PRIVATE)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->prefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method private final getPref()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->strategy:Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    iget-object v1, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->prefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->pref:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->default:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;->getPref(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private final put(Landroid/content/SharedPreferences$Editor;Ljava/lang/Object;)Landroid/content/SharedPreferences$Editor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences$Editor;",
            "TT;)",
            "Landroid/content/SharedPreferences$Editor;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->strategy:Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    iget-object v1, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->pref:Ljava/lang/String;

    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;->putPref(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Object;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public doQuery()Landroid/database/Cursor;
    .locals 5

    .line 53
    new-instance v0, Landroid/database/MatrixCursor;

    const-string v1, "column"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 54
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->strategy:Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    invoke-direct {p0}, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->getPref()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;->getValueForCursor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 53
    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method public doUpdate(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 3

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "values"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    .line 66
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->strategy:Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v1

    const-string v2, "values.keySet()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "values.keySet().first()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, p2, v1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;->getValueFromValues(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->hasPref()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->getPref()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "prefs.edit()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, v0, p2}, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->put(Landroid/content/SharedPreferences$Editor;Ljava/lang/Object;)Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    .line 81
    invoke-interface {p2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 82
    iget-object p2, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->context:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    const-string v0, "context.contentResolver"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1}, Lcom/squareup/util/ContentResolversKt;->notifyChange(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-void

    .line 66
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "A value must be supplied."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 63
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Pass exactly one value in values."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public hasPref()Z
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->prefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;->pref:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
