.class Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;
.super Ljava/lang/Object;
.source "CheckableGroup.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/CheckableGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PassThroughHierarchyChangeListener"
.end annotation


# instance fields
.field private mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field final synthetic this$0:Lcom/squareup/widgets/CheckableGroup;


# direct methods
.method private constructor <init>(Lcom/squareup/widgets/CheckableGroup;)V
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/widgets/CheckableGroup;Lcom/squareup/widgets/CheckableGroup$1;)V
    .locals 0

    .line 571
    invoke-direct {p0, p1}, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;-><init>(Lcom/squareup/widgets/CheckableGroup;)V

    return-void
.end method

.method static synthetic access$202(Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;Landroid/view/ViewGroup$OnHierarchyChangeListener;)Landroid/view/ViewGroup$OnHierarchyChangeListener;
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    return-object p1
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 576
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    if-eq p1, v0, :cond_0

    .line 577
    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$900(Lcom/squareup/widgets/CheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    .line 578
    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$1000(Lcom/squareup/widgets/CheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-ne p1, v0, :cond_5

    :cond_0
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_5

    .line 580
    move-object v0, p2

    check-cast v0, Landroid/widget/Checkable;

    .line 581
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 582
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->access$302(Lcom/squareup/widgets/CheckableGroup;Z)Z

    .line 584
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$500(Lcom/squareup/widgets/CheckableGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 586
    iget-object v2, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v2, v0, v1}, Lcom/squareup/widgets/CheckableGroup;->access$600(Lcom/squareup/widgets/CheckableGroup;IZ)Z

    .line 589
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->access$302(Lcom/squareup/widgets/CheckableGroup;Z)Z

    .line 590
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->access$700(Lcom/squareup/widgets/CheckableGroup;I)V

    .line 593
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 597
    invoke-static {}, Lcom/squareup/util/Views;->generateViewId()I

    move-result v0

    .line 598
    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 601
    :cond_3
    instance-of v0, p2, Landroid/widget/CompoundButton;

    if-eqz v0, :cond_4

    .line 602
    move-object v0, p2

    check-cast v0, Landroid/widget/CompoundButton;

    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v1}, Lcom/squareup/widgets/CheckableGroup;->access$1100(Lcom/squareup/widgets/CheckableGroup;)Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 605
    :cond_4
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$1100(Lcom/squareup/widgets/CheckableGroup;)Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 609
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    if-eqz v0, :cond_6

    .line 610
    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    :cond_6
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 616
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    if-eq p1, v0, :cond_0

    .line 617
    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$900(Lcom/squareup/widgets/CheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    .line 618
    invoke-static {v0}, Lcom/squareup/widgets/CheckableGroup;->access$1000(Lcom/squareup/widgets/CheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 619
    :cond_0
    instance-of v0, p2, Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 620
    move-object v0, p2

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 621
    :cond_1
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_2

    .line 622
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 625
    :cond_2
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    .line 626
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v1}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 627
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/squareup/widgets/CheckableGroup;->access$600(Lcom/squareup/widgets/CheckableGroup;IZ)Z

    .line 628
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/widgets/CheckableGroup;

    invoke-static {v1}, Lcom/squareup/widgets/CheckableGroup;->access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 632
    :cond_3
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    if-eqz v0, :cond_4

    .line 633
    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V

    :cond_4
    return-void
.end method
