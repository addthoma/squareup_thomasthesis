.class Lcom/squareup/widgets/EqualizingTextView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "EqualizingTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;
    }
.end annotation


# static fields
.field private static final MAX_LAYOUT_ATTEMPTS:I = 0x14

.field private static final MIN_CHARACTER_COUNT:I = 0x4

.field private static final MIN_LINE_WIDTH_PERCENTAGE:F = 0.75f


# instance fields
.field private equalizationDisabled:Z

.field private final maxAdditionalLines:I


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/R$integer;->equalizing_text_view_max_additional_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 30
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/EqualizingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput p3, p0, Lcom/squareup/widgets/EqualizingTextView;->maxAdditionalLines:I

    return-void
.end method

.method static calculateAverageCharacterWidth(Landroid/text/Layout;)F
    .locals 4

    .line 78
    invoke-virtual {p0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v2, 0x0

    .line 84
    :goto_0
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 85
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getLineMax(I)F

    move-result v3

    add-float/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    int-to-float p0, v0

    div-float/2addr v1, p0

    return v1
.end method

.method static calculateLayoutDelta(Landroid/text/Layout;)Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;
    .locals 8

    .line 49
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 51
    new-instance p0, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;

    invoke-direct {p0, v1, v1}, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;-><init>(FF)V

    return-object p0

    :cond_0
    const/4 v2, 0x0

    .line 55
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getLineMax(I)F

    move-result v3

    move v5, v3

    move v6, v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 59
    invoke-virtual {p0, v3}, Landroid/text/Layout;->getLineMax(I)F

    move-result v7

    add-float/2addr v4, v7

    .line 61
    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 62
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    int-to-float v3, v0

    div-float/2addr v4, v3

    :goto_1
    if-ge v2, v0, :cond_2

    .line 69
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getLineMax(I)F

    move-result v7

    sub-float/2addr v7, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    add-float/2addr v1, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 73
    :cond_2
    new-instance p0, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;

    div-float/2addr v1, v3

    sub-float/2addr v6, v5

    invoke-direct {p0, v1, v6}, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;-><init>(FF)V

    return-object p0
.end method

.method static findMaximumLineWidth(Landroid/text/Layout;)F
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 93
    :goto_0
    invoke-virtual {p0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 94
    invoke-virtual {p0, v1}, Landroid/text/Layout;->getLineMax(I)F

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method


# virtual methods
.method applyFinalMeasureSpec(II)V
    .locals 0

    .line 197
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->onMeasure(II)V

    .line 204
    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->getGravity()I

    move-result p1

    and-int/lit8 p1, p1, 0x7

    const/4 p2, 0x3

    if-ne p1, p2, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->getLayout()Landroid/text/Layout;

    move-result-object p1

    .line 206
    invoke-static {p1}, Lcom/squareup/widgets/EqualizingTextView;->findMaximumLineWidth(Landroid/text/Layout;)F

    move-result p1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->getPaddingLeft()I

    move-result p2

    int-to-float p2, p2

    add-float/2addr p1, p2

    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->getPaddingRight()I

    move-result p2

    int-to-float p2, p2

    add-float/2addr p1, p2

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p1, p1

    .line 208
    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/EqualizingTextView;->setMeasuredDimension(II)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 18

    move-object/from16 v0, p0

    move/from16 v1, p2

    .line 105
    invoke-virtual/range {p0 .. p2}, Lcom/squareup/widgets/EqualizingTextView;->recalculateLayoutWithMeasureSpec(II)Landroid/text/Layout;

    move-result-object v2

    .line 110
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v4, v3, :cond_0

    return-void

    .line 125
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/widgets/EqualizingTextView;->getMeasuredWidth()I

    move-result v4

    .line 126
    iget-boolean v5, v0, Lcom/squareup/widgets/EqualizingTextView;->equalizationDisabled:Z

    if-nez v5, :cond_7

    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_7

    .line 127
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v5

    iget v7, v0, Lcom/squareup/widgets/EqualizingTextView;->maxAdditionalLines:I

    add-int/2addr v5, v7

    .line 128
    invoke-static {v2}, Lcom/squareup/widgets/EqualizingTextView;->calculateAverageCharacterWidth(Landroid/text/Layout;)F

    move-result v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {v8, v7}, Ljava/lang/Math;->max(FF)F

    move-result v7

    const/high16 v9, 0x40000000    # 2.0f

    div-float v10, v7, v9

    .line 129
    invoke-static {v8, v10}, Ljava/lang/Math;->max(FF)F

    move-result v8

    mul-float v9, v9, v7

    .line 131
    invoke-static {v2}, Lcom/squareup/widgets/EqualizingTextView;->findMaximumLineWidth(Landroid/text/Layout;)F

    move-result v10

    const/high16 v11, 0x40800000    # 4.0f

    mul-float v7, v7, v11

    const/high16 v11, 0x3f400000    # 0.75f

    mul-float v11, v11, v10

    .line 133
    invoke-static {v7, v11}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 135
    invoke-static {v2}, Lcom/squareup/widgets/EqualizingTextView;->calculateLayoutDelta(Landroid/text/Layout;)Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;

    move-result-object v11

    .line 136
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    move v13, v4

    move v4, v2

    const/4 v2, 0x0

    :goto_0
    const/16 v14, 0x14

    if-ge v2, v14, :cond_8

    .line 138
    iget v14, v11, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;->average:F

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_8

    .line 141
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/widgets/EqualizingTextView;->getPaddingLeft()I

    move-result v14

    int-to-float v14, v14

    add-float/2addr v14, v10

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/widgets/EqualizingTextView;->getPaddingRight()I

    move-result v15

    int-to-float v15, v15

    add-float/2addr v14, v15

    sub-float/2addr v14, v8

    float-to-int v14, v14

    if-gtz v14, :cond_1

    goto :goto_2

    .line 146
    :cond_1
    invoke-static {v14, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 145
    invoke-virtual {v0, v15, v1}, Lcom/squareup/widgets/EqualizingTextView;->recalculateLayoutWithMeasureSpec(II)Landroid/text/Layout;

    move-result-object v15

    .line 149
    invoke-virtual {v15}, Landroid/text/Layout;->getLineCount()I

    move-result v6

    .line 150
    invoke-static {v15}, Lcom/squareup/widgets/EqualizingTextView;->findMaximumLineWidth(Landroid/text/Layout;)F

    move-result v16

    if-le v6, v4, :cond_2

    const/16 v17, 0x1

    goto :goto_1

    :cond_2
    const/16 v17, 0x0

    :goto_1
    if-le v6, v5, :cond_3

    goto :goto_2

    :cond_3
    cmpl-float v10, v16, v10

    if-nez v10, :cond_4

    goto :goto_2

    :cond_4
    if-eqz v17, :cond_5

    .line 158
    iget v10, v11, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;->max:F

    cmpg-float v10, v10, v9

    if-lez v10, :cond_8

    cmpg-float v10, v16, v7

    if-gez v10, :cond_5

    goto :goto_2

    .line 166
    :cond_5
    invoke-static {v15}, Lcom/squareup/widgets/EqualizingTextView;->calculateLayoutDelta(Landroid/text/Layout;)Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;

    move-result-object v10

    .line 167
    iget v15, v10, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;->average:F

    iget v12, v11, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;->average:F

    cmpg-float v12, v15, v12

    if-gez v12, :cond_6

    move v4, v6

    move-object v11, v10

    move v13, v14

    :cond_6
    add-int/lit8 v2, v2, 0x1

    move/from16 v10, v16

    const/4 v6, 0x1

    goto :goto_0

    :cond_7
    move v13, v4

    :cond_8
    :goto_2
    const/high16 v2, -0x80000000

    if-ne v2, v3, :cond_9

    .line 180
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 181
    invoke-static {v2, v13}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 183
    :cond_9
    invoke-static {v13, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 184
    invoke-virtual {v0, v2, v1}, Lcom/squareup/widgets/EqualizingTextView;->applyFinalMeasureSpec(II)V

    return-void
.end method

.method recalculateLayoutWithMeasureSpec(II)Landroid/text/Layout;
    .locals 0

    .line 190
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->onMeasure(II)V

    .line 191
    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->getLayout()Landroid/text/Layout;

    move-result-object p1

    return-object p1
.end method

.method public setEqualizationDisabled(Z)V
    .locals 0

    .line 100
    iput-boolean p1, p0, Lcom/squareup/widgets/EqualizingTextView;->equalizationDisabled:Z

    .line 101
    invoke-virtual {p0}, Lcom/squareup/widgets/EqualizingTextView;->invalidate()V

    return-void
.end method
