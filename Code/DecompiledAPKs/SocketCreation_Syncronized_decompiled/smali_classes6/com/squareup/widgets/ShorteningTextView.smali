.class public Lcom/squareup/widgets/ShorteningTextView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "ShorteningTextView.java"


# instance fields
.field private ellipsizeShortText:Z

.field private measuring:Z

.field private shortText:Ljava/lang/CharSequence;

.field private shortened:Z

.field private showingNothing:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/ShorteningTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010084

    .line 29
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/ShorteningTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    sget-object v0, Lcom/squareup/widgets/R$styleable;->ShorteningTextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 38
    sget p2, Lcom/squareup/widgets/R$styleable;->ShorteningTextView_shortText:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/widgets/ShorteningTextView;->shortText:Ljava/lang/CharSequence;

    .line 39
    sget p2, Lcom/squareup/widgets/R$styleable;->ShorteningTextView_ellipsizeShortText:I

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/widgets/ShorteningTextView;->ellipsizeShortText:Z

    .line 41
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    sget-object p1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 44
    invoke-virtual {p0, p3}, Lcom/squareup/widgets/ShorteningTextView;->setSingleLine(Z)V

    return-void
.end method

.method private isEllipsized()Z
    .locals 2

    .line 116
    invoke-virtual {p0}, Lcom/squareup/widgets/ShorteningTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private reset()V
    .locals 1

    const/4 v0, 0x0

    .line 119
    iput-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->shortened:Z

    .line 120
    iput-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->showingNothing:Z

    return-void
.end method


# virtual methods
.method public isShortened()Z
    .locals 1

    .line 108
    iget-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->shortened:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->showingNothing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isShowingNothing()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->showingNothing:Z

    return v0
.end method

.method protected onMeasure(II)V
    .locals 2

    .line 74
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->onMeasure(II)V

    .line 77
    iget-object v0, p0, Lcom/squareup/widgets/ShorteningTextView;->shortText:Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 81
    iput-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->measuring:Z

    .line 84
    iget-boolean v1, p0, Lcom/squareup/widgets/ShorteningTextView;->shortened:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/widgets/ShorteningTextView;->isEllipsized()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    iput-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->shortened:Z

    .line 87
    iget-object v1, p0, Lcom/squareup/widgets/ShorteningTextView;->shortText:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-boolean v1, p0, Lcom/squareup/widgets/ShorteningTextView;->ellipsizeShortText:Z

    if-nez v1, :cond_1

    .line 92
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->onMeasure(II)V

    .line 97
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/widgets/ShorteningTextView;->ellipsizeShortText:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/squareup/widgets/ShorteningTextView;->shortened:Z

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/squareup/widgets/ShorteningTextView;->showingNothing:Z

    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/squareup/widgets/ShorteningTextView;->isEllipsized()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 100
    iput-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->showingNothing:Z

    const-string p1, ""

    .line 101
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const/4 p1, 0x0

    .line 104
    iput-boolean p1, p0, Lcom/squareup/widgets/ShorteningTextView;->measuring:Z

    return-void
.end method

.method public setShortText(Ljava/lang/CharSequence;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/widgets/ShorteningTextView;->reset()V

    .line 60
    iput-object p1, p0, Lcom/squareup/widgets/ShorteningTextView;->shortText:Ljava/lang/CharSequence;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/widgets/ShorteningTextView;->requestLayout()V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/widgets/ShorteningTextView;->invalidate()V

    return-void
.end method

.method public setShowShortTextWhenEllipsized(Z)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/squareup/widgets/ShorteningTextView;->reset()V

    .line 68
    iput-boolean p1, p0, Lcom/squareup/widgets/ShorteningTextView;->ellipsizeShortText:Z

    .line 69
    invoke-virtual {p0}, Lcom/squareup/widgets/ShorteningTextView;->requestLayout()V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/widgets/ShorteningTextView;->invalidate()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/squareup/widgets/ShorteningTextView;->measuring:Z

    if-nez v0, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/squareup/widgets/ShorteningTextView;->reset()V

    .line 54
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method
