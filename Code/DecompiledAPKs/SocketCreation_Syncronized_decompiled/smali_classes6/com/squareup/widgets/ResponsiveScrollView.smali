.class public Lcom/squareup/widgets/ResponsiveScrollView;
.super Landroid/widget/ScrollView;
.source "ResponsiveScrollView.java"


# instance fields
.field private final controller:Lcom/squareup/widgets/ResponsiveViewController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    invoke-virtual {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    sget v0, Lcom/squareup/widgets/R$id;->responsive_scroll_view:I

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/ResponsiveScrollView;->setId(I)V

    .line 16
    :cond_0
    new-instance v0, Lcom/squareup/widgets/ResponsiveViewController;

    invoke-direct {v0, p1, p2, p0}, Lcom/squareup/widgets/ResponsiveViewController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v0, p0, Lcom/squareup/widgets/ResponsiveScrollView;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    const/4 p1, 0x0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ResponsiveScrollView;->setClipToPadding(Z)V

    const/high16 p1, 0x2000000

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ResponsiveScrollView;->setScrollBarStyle(I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/widgets/ResponsiveScrollView;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/ResponsiveViewController;->measure(II)V

    .line 23
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->onMeasure(II)V

    return-void
.end method
