.class final Lcom/squareup/widgets/warning/WarningIds$1;
.super Ljava/lang/Object;
.source "WarningIds.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/warning/WarningIds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/widgets/warning/WarningIds;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/widgets/warning/WarningIds;
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/warning/WarningIds$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/widgets/warning/WarningIds;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/widgets/warning/WarningIds;
    .locals 0

    .line 51
    new-array p1, p1, [Lcom/squareup/widgets/warning/WarningIds;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/warning/WarningIds$1;->newArray(I)[Lcom/squareup/widgets/warning/WarningIds;

    move-result-object p1

    return-object p1
.end method
