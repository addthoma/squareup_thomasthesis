.class public Lcom/squareup/widgets/CheckablePreservedLabelRow;
.super Landroid/widget/FrameLayout;
.source "CheckablePreservedLabelRow.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field isChecked:Z

.field private final preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/CheckablePreservedLabelRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 18
    sget v0, Lcom/squareup/widgets/R$attr;->checkablePreservedLabelRowStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/CheckablePreservedLabelRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    sget p2, Lcom/squareup/widgets/R$layout;->checkable_preserved_label_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/widgets/CheckablePreservedLabelRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 24
    sget p1, Lcom/squareup/widgets/R$id;->preserved_label:I

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckablePreservedLabelRow;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PreservedLabelView;

    iput-object p1, p0, Lcom/squareup/widgets/CheckablePreservedLabelRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    return-void
.end method


# virtual methods
.method public getPreservedLabelView()Lcom/squareup/widgets/PreservedLabelView;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/widgets/CheckablePreservedLabelRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/squareup/widgets/CheckablePreservedLabelRow;->isChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0

    .line 28
    iput-boolean p1, p0, Lcom/squareup/widgets/CheckablePreservedLabelRow;->isChecked:Z

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckablePreservedLabelRow;->setSelected(Z)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/widgets/CheckablePreservedLabelRow;->isChecked:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/CheckablePreservedLabelRow;->setChecked(Z)V

    return-void
.end method
