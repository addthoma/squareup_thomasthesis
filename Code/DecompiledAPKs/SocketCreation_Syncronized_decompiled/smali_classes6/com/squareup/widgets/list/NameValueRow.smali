.class public Lcom/squareup/widgets/list/NameValueRow;
.super Landroid/widget/LinearLayout;
.source "NameValueRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/list/NameValueRow$Builder;
    }
.end annotation


# instance fields
.field protected final nameView:Lcom/squareup/widgets/ShorteningTextView;

.field protected showPercent:Z

.field protected final valueView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/list/NameValueRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 67
    sget v0, Lcom/squareup/widgets/R$attr;->nameValueRowStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/list/NameValueRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    sget v0, Lcom/squareup/widgets/R$layout;->name_value_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/widgets/list/NameValueRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 74
    sget v0, Lcom/squareup/widgets/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ShorteningTextView;

    iput-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    .line 75
    sget v0, Lcom/squareup/widgets/R$id;->value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->valueView:Landroid/widget/TextView;

    .line 77
    sget-object v0, Lcom/squareup/widgets/R$styleable;->NameValueRow:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 78
    sget p3, Lcom/squareup/widgets/R$styleable;->NameValueRow_showPercent:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/widgets/list/NameValueRow;->showPercent:Z

    .line 79
    sget p3, Lcom/squareup/widgets/R$styleable;->NameValueRow_value:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p3

    .line 80
    sget v0, Lcom/squareup/widgets/R$styleable;->NameValueRow_android_text:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 81
    sget v1, Lcom/squareup/widgets/R$styleable;->NameValueRow_shortText:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 83
    sget v2, Lcom/squareup/widgets/R$styleable;->NameValueRow_nameValueRowNameStyle:I

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 85
    iget-object v4, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v4, p1, v2}, Lcom/squareup/widgets/ShorteningTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 87
    :cond_0
    sget v2, Lcom/squareup/widgets/R$styleable;->NameValueRow_nameValueRowValueStyle:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 89
    iget-object v3, p0, Lcom/squareup/widgets/list/NameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v3, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 92
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/widgets/list/NameValueRow;->showPercent:Z

    invoke-virtual {p0, v0, v1, p3, p1}, Lcom/squareup/widgets/list/NameValueRow;->update(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 93
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public enableName(Z)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setEnabled(Z)V

    return-void
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/ShorteningTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 136
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 137
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 122
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 127
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueRow;->nameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/ShorteningTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;)V
    .locals 2

    .line 98
    iget-boolean v0, p0, Lcom/squareup/widgets/list/NameValueRow;->showPercent:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/widgets/list/NameValueRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "value"

    .line 100
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 107
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/widgets/list/NameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public update(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 0

    .line 158
    iput-boolean p4, p0, Lcom/squareup/widgets/list/NameValueRow;->showPercent:Z

    .line 159
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p0, p3}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public update(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 0

    .line 151
    iput-boolean p3, p0, Lcom/squareup/widgets/list/NameValueRow;->showPercent:Z

    .line 152
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 153
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method
