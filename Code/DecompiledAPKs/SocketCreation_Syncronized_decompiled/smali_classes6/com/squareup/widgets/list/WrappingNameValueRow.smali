.class public Lcom/squareup/widgets/list/WrappingNameValueRow;
.super Landroid/widget/LinearLayout;
.source "WrappingNameValueRow.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;
    }
.end annotation


# instance fields
.field private isChecked:Z

.field private final nameView:Lcom/squareup/widgets/ScalingTextView;

.field private showPercent:Z

.field private final valueView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/list/WrappingNameValueRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 72
    sget v0, Lcom/squareup/widgets/R$attr;->wrappingNameValueRowStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/list/WrappingNameValueRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .line 76
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    sget v0, Lcom/squareup/widgets/R$layout;->wrapping_name_value_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/widgets/list/WrappingNameValueRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 79
    sget v0, Lcom/squareup/widgets/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    .line 80
    sget v0, Lcom/squareup/widgets/R$id;->value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->valueView:Landroid/widget/TextView;

    .line 82
    sget-object v0, Lcom/squareup/widgets/R$styleable;->WrappingNameValueRow:[I

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 84
    sget p2, Lcom/squareup/widgets/R$styleable;->NameValueRow_showPercent:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->showPercent:Z

    .line 85
    sget p2, Lcom/squareup/widgets/R$styleable;->NameValueRow_value:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 86
    sget p3, Lcom/squareup/widgets/R$styleable;->NameValueRow_android_text:I

    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p3

    .line 87
    sget v0, Lcom/squareup/widgets/R$styleable;->ScalingTextView_minTextSize:I

    .line 88
    invoke-virtual {p0}, Lcom/squareup/widgets/list/WrappingNameValueRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/widgets/R$dimen;->wrapping_name_value_row_minimum_font_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 87
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 90
    invoke-virtual {p0, p3}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 92
    invoke-direct {p0, v0}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setMinTextSize(F)V

    .line 93
    invoke-direct {p0, v1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setSingleLine(Z)V

    const/4 p2, 0x2

    .line 94
    invoke-direct {p0, p2}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setMaxLines(I)V

    .line 95
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/list/WrappingNameValueRow;Z)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setShowPercent(Z)V

    return-void
.end method

.method private setMaxLines(I)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setMaxLines(I)V

    .line 128
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    return-void
.end method

.method private setMinTextSize(F)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setMinTextSize(F)V

    return-void
.end method

.method private setShowPercent(Z)V
    .locals 0

    .line 136
    iput-boolean p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->showPercent:Z

    return-void
.end method

.method private setSingleLine(Z)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setSingleLine(Z)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/ScalingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .line 159
    iget-boolean v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->isChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0

    .line 154
    iput-boolean p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->isChecked:Z

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setSelected(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 140
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 141
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setEnabled(Z)V

    .line 142
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 118
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->nameView:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/ScalingTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;)V
    .locals 2

    .line 100
    iget-boolean v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->showPercent:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/widgets/list/WrappingNameValueRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "value"

    .line 102
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 103
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 109
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->valueView:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 163
    iget-boolean v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow;->isChecked:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setChecked(Z)V

    return-void
.end method
