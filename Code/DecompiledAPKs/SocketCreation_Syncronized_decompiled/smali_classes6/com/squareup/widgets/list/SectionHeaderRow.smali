.class public Lcom/squareup/widgets/list/SectionHeaderRow;
.super Lcom/squareup/marketfont/MarketTextView;
.source "SectionHeaderRow.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .line 11
    sget v0, Lcom/squareup/widgets/R$attr;->sectionHeaderRowStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/list/SectionHeaderRow;->setText(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 2

    .line 16
    sget v0, Lcom/squareup/widgets/R$attr;->sectionHeaderRowStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/list/SectionHeaderRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
