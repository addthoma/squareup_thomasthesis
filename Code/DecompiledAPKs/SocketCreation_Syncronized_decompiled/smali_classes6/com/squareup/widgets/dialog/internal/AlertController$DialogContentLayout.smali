.class public final enum Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;
.super Ljava/lang/Enum;
.source "AlertController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/dialog/internal/AlertController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialogContentLayout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

.field public static final enum DEFAULT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

.field public static final enum MATCH_DIALOG:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

.field public static final enum WRAP_CONTENT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;


# instance fields
.field public final matchParent:Z

.field public final wrapContent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 539
    new-instance v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "DEFAULT"

    invoke-direct {v0, v3, v1, v2, v2}, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->DEFAULT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    .line 541
    new-instance v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    const-string v3, "WRAP_CONTENT"

    invoke-direct {v0, v3, v2, v1, v2}, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->WRAP_CONTENT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    .line 543
    new-instance v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    const/4 v3, 0x2

    const-string v4, "MATCH_DIALOG"

    invoke-direct {v0, v4, v3, v2, v1}, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->MATCH_DIALOG:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    .line 537
    sget-object v4, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->DEFAULT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->WRAP_CONTENT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->MATCH_DIALOG:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->$VALUES:[Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .line 545
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 546
    iput-boolean p3, p0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->matchParent:Z

    .line 547
    iput-boolean p4, p0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->wrapContent:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;
    .locals 1

    .line 537
    const-class v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    return-object p0
.end method

.method public static values()[Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;
    .locals 1

    .line 537
    sget-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->$VALUES:[Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    invoke-virtual {v0}, [Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    return-object v0
.end method
