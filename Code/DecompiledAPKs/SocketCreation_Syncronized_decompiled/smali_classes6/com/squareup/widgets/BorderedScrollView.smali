.class public Lcom/squareup/widgets/BorderedScrollView;
.super Lcom/squareup/widgets/ResponsiveScrollView;
.source "BorderedScrollView.java"


# instance fields
.field private final paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    sget-object v0, Lcom/squareup/widgets/R$styleable;->BorderedScrollView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 16
    sget v0, Lcom/squareup/widgets/R$styleable;->BorderedScrollView_borderColor:I

    .line 17
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x106000c

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 16
    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    .line 18
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 19
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/widgets/BorderedScrollView;->paint:Landroid/graphics/Paint;

    .line 20
    iget-object p2, p0, Lcom/squareup/widgets/BorderedScrollView;->paint:Landroid/graphics/Paint;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 21
    iget-object p2, p0, Lcom/squareup/widgets/BorderedScrollView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 25
    invoke-super {p0, p1}, Lcom/squareup/widgets/ResponsiveScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/widgets/BorderedScrollView;->getScrollY()I

    move-result v0

    if-eqz v0, :cond_0

    int-to-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    add-float v6, v0, v1

    const/4 v3, 0x0

    .line 29
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v5, v0

    iget-object v7, p0, Lcom/squareup/widgets/BorderedScrollView;->paint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method
