.class public Lcom/squareup/widgets/PreservedLabelView;
.super Landroid/view/ViewGroup;
.source "PreservedLabelView.java"


# instance fields
.field private final allowMultiLine:Z

.field private availableContentWidth:I

.field private label:Lcom/squareup/marketfont/MarketTextView;

.field private layoutUsingTwoRows:Z

.field private final spacing:I

.field private title:Lcom/squareup/marketfont/MarketTextView;

.field private final verticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 81
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    sget-object v0, Lcom/squareup/widgets/R$styleable;->PreservedLabelView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 83
    sget p2, Lcom/squareup/widgets/R$styleable;->PreservedLabelView_spacing:I

    .line 84
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/R$dimen;->marin_gap_tiny:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 83
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/PreservedLabelView;->spacing:I

    .line 85
    sget p2, Lcom/squareup/widgets/R$styleable;->PreservedLabelView_verticalSpacing:I

    .line 86
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/R$dimen;->marin_gap_tiny:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 85
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/PreservedLabelView;->verticalSpacing:I

    .line 87
    sget p2, Lcom/squareup/widgets/R$styleable;->PreservedLabelView_allowMultiLine:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/widgets/PreservedLabelView;->allowMultiLine:Z

    .line 88
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private static getTop(II)I
    .locals 2

    sub-int/2addr p0, p1

    int-to-double p0, p0

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    div-double/2addr p0, v0

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    add-double/2addr p0, v0

    double-to-int p0, p0

    return p0
.end method


# virtual methods
.method public getPreservedLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getPreservedLabelView()Landroid/widget/TextView;
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleView()Landroid/widget/TextView;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    return-object v0
.end method

.method public hidePreservedLabel()V
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 241
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method public hideTitle()V
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 251
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 94
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/squareup/widgets/R$layout;->preserved_label_layout:I

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_0
    const/4 v0, 0x0

    .line 101
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PreservedLabelView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 102
    invoke-virtual {p0, v1}, Lcom/squareup/widgets/PreservedLabelView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    .line 103
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .line 144
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingTop()I

    move-result p2

    sub-int/2addr p1, p2

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingBottom()I

    move-result p2

    sub-int/2addr p1, p2

    .line 145
    iget-object p2, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result p2

    .line 146
    iget-object p3, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p3}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result p3

    .line 149
    invoke-static {p1, p2}, Lcom/squareup/widgets/PreservedLabelView;->getTop(II)I

    move-result p4

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingTop()I

    move-result p5

    add-int/2addr p4, p5

    .line 152
    iget-object p5, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p5}, Lcom/squareup/marketfont/MarketTextView;->getVisibility()I

    move-result p5

    const/16 v0, 0x8

    if-eq p5, v0, :cond_1

    .line 157
    iget-boolean p5, p0, Lcom/squareup/widgets/PreservedLabelView;->layoutUsingTwoRows:Z

    if-eqz p5, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingTop()I

    move-result p4

    .line 161
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingTop()I

    move-result p1

    add-int/2addr p1, p2

    iget p5, p0, Lcom/squareup/widgets/PreservedLabelView;->verticalSpacing:I

    add-int/2addr p1, p5

    .line 162
    iget-object p5, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p5}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result p5

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingBottom()I

    move-result v0

    add-int/2addr p5, v0

    add-int/2addr p5, p1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingLeft()I

    move-result v0

    goto :goto_0

    .line 166
    :cond_0
    iget-object p5, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p5}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result p5

    invoke-static {p1, p5}, Lcom/squareup/widgets/PreservedLabelView;->getTop(II)I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingTop()I

    move-result p5

    add-int/2addr p1, p5

    .line 167
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingLeft()I

    move-result p5

    add-int/2addr p5, p3

    iget v0, p0, Lcom/squareup/widgets/PreservedLabelView;->spacing:I

    add-int/2addr v0, p5

    .line 168
    iget-object p5, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p5}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result p5

    add-int/2addr p5, p1

    .line 170
    :goto_0
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v0, p1, v2, p5}, Lcom/squareup/marketfont/MarketTextView;->layout(IIII)V

    .line 172
    :cond_1
    iget-object p1, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingLeft()I

    move-result p5

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p3

    add-int/2addr p2, p4

    invoke-virtual {p1, p5, p4, v0, p2}, Lcom/squareup/marketfont/MarketTextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .line 107
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingLeft()I

    move-result p2

    .line 108
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingRight()I

    move-result v0

    .line 109
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p2

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/squareup/widgets/PreservedLabelView;->availableContentWidth:I

    .line 112
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/4 v2, 0x0

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/squareup/marketfont/MarketTextView;->measure(II)V

    .line 113
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-ne v1, v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v1

    iget v3, p0, Lcom/squareup/widgets/PreservedLabelView;->spacing:I

    add-int/2addr v1, v3

    .line 116
    :goto_0
    iget-object v3, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/squareup/marketfont/MarketTextView;->measure(II)V

    .line 117
    iget-object v3, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v3}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v4}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 120
    iget v4, p0, Lcom/squareup/widgets/PreservedLabelView;->availableContentWidth:I

    sub-int/2addr v4, v1

    .line 123
    iget-boolean v5, p0, Lcom/squareup/widgets/PreservedLabelView;->allowMultiLine:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v5}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v5

    if-le v5, v4, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    iput-boolean v5, p0, Lcom/squareup/widgets/PreservedLabelView;->layoutUsingTwoRows:Z

    .line 126
    iget-boolean v5, p0, Lcom/squareup/widgets/PreservedLabelView;->layoutUsingTwoRows:Z

    const/high16 v6, -0x80000000

    if-eqz v5, :cond_2

    .line 128
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v3}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, p0, Lcom/squareup/widgets/PreservedLabelView;->verticalSpacing:I

    add-int/2addr v3, v1

    .line 129
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    iget v4, p0, Lcom/squareup/widgets/PreservedLabelView;->availableContentWidth:I

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 130
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 129
    invoke-virtual {v1, v4, v2}, Lcom/squareup/marketfont/MarketTextView;->measure(II)V

    .line 131
    iget-object v1, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_2

    .line 134
    :cond_2
    iget-object v5, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v5, v4, v2}, Lcom/squareup/marketfont/MarketTextView;->measure(II)V

    .line 135
    iget-object v2, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketTextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    .line 137
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, -0x2

    if-ne v2, v4, :cond_3

    add-int/2addr v1, p2

    add-int/2addr v1, v0

    goto :goto_3

    .line 138
    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 139
    :goto_3
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingTop()I

    move-result p1

    add-int/2addr p1, v3

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getPaddingBottom()I

    move-result p2

    add-int/2addr p1, p2

    .line 140
    invoke-virtual {p0, v1, p1}, Lcom/squareup/widgets/PreservedLabelView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setPreservedLabel(Ljava/lang/CharSequence;)V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method public setPreservedLabel(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 210
    iget-object p2, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2}, Lcom/squareup/marketfont/MarketTextView;->getVisibility()I

    move-result p2

    if-eqz p2, :cond_0

    .line 211
    iget-object p2, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 213
    :cond_0
    iget-object p2, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 219
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    return-void
.end method

.method public setTextSize(F)V
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    return-void
.end method

.method public setTextSize(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 227
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 196
    iget-object p2, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method public setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public showPreservedLabel()V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 236
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method

.method public showTitle()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/widgets/PreservedLabelView;->title:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 246
    invoke-virtual {p0}, Lcom/squareup/widgets/PreservedLabelView;->requestLayout()V

    return-void
.end method
