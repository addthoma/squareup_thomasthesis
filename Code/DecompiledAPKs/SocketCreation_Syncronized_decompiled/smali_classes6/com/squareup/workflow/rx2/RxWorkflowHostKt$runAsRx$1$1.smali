.class public final Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;
.super Ljava/lang/Object;
.source "RxWorkflowHost.kt"

# interfaces
.implements Lcom/squareup/workflow/rx2/RxWorkflowHost;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;->invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
        "TO;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0001J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u001c\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\"\u0010\u0006\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00080\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\r"
    }
    d2 = {
        "com/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "outputs",
        "Lio/reactivex/Flowable;",
        "getOutputs",
        "()Lio/reactivex/Flowable;",
        "renderingsAndSnapshots",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "getRenderingsAndSnapshots",
        "()Lio/reactivex/Observable;",
        "cancel",
        "",
        "runtime-extensions"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $session:Lcom/squareup/workflow/WorkflowSession;

.field final synthetic $this_launchWorkflowIn:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;->$this_launchWorkflowIn:Lkotlinx/coroutines/CoroutineScope;

    iput-object p2, p0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;->$session:Lcom/squareup/workflow/WorkflowSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;->$this_launchWorkflowIn:Lkotlinx/coroutines/CoroutineScope;

    const/4 v1, 0x0

    const-string v2, "RxWorkflowHost was cancelled"

    const/4 v3, 0x2

    invoke-static {v0, v2, v1, v3, v1}, Lkotlinx/coroutines/CoroutineScopeKt;->cancel$default(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)V

    return-void
.end method

.method public getOutputs()Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "+TO;>;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;->$session:Lcom/squareup/workflow/WorkflowSession;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowSession;->getOutputs()Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    invoke-static {v0}, Lkotlinx/coroutines/rx2/RxConvertKt;->from(Lkotlinx/coroutines/flow/Flow;)Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public getRenderingsAndSnapshots()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TR;>;>;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;->$session:Lcom/squareup/workflow/WorkflowSession;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowSession;->getRenderingsAndSnapshots()Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    invoke-static {v0}, Lkotlinx/coroutines/rx2/RxConvertKt;->from(Lkotlinx/coroutines/flow/Flow;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
