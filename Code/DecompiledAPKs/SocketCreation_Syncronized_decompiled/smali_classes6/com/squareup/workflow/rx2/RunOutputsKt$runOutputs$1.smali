.class final Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;
.super Ljava/lang/Object;
.source "RunOutputs.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx2/RunOutputsKt;->runOutputs(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lorg/reactivestreams/Publisher<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u0002 \u0003*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Flowable;",
        "OutputT",
        "kotlin.jvm.PlatformType",
        "InputT",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lkotlin/coroutines/CoroutineContext;

.field final synthetic $inputs:Lkotlinx/coroutines/flow/Flow;

.field final synthetic $snapshot:Lcom/squareup/workflow/Snapshot;

.field final synthetic $workflow:Lcom/squareup/workflow/Workflow;


# direct methods
.method constructor <init>(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$inputs:Lkotlinx/coroutines/flow/Flow;

    iput-object p2, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$workflow:Lcom/squareup/workflow/Workflow;

    iput-object p3, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$snapshot:Lcom/squareup/workflow/Snapshot;

    iput-object p4, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$context:Lkotlin/coroutines/CoroutineContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lio/reactivex/Flowable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "+TOutputT;>;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$inputs:Lkotlinx/coroutines/flow/Flow;

    iget-object v1, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$workflow:Lcom/squareup/workflow/Workflow;

    iget-object v2, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$snapshot:Lcom/squareup/workflow/Snapshot;

    iget-object v3, p0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->$context:Lkotlin/coroutines/CoroutineContext;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/workflow/rx2/RxWorkflowHostKt;->runAsRx(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/rx2/RxWorkflowHost;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->getOutputs()Lio/reactivex/Flowable;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1$1;

    invoke-direct {v2, v0}, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1$1;-><init>(Lcom/squareup/workflow/rx2/RxWorkflowHost;)V

    check-cast v2, Lio/reactivex/functions/Action;

    invoke-virtual {v1, v2}, Lio/reactivex/Flowable;->doOnCancel(Lio/reactivex/functions/Action;)Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;->call()Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method
