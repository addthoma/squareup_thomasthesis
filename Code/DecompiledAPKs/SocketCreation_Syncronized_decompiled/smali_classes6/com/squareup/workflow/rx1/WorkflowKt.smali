.class public final Lcom/squareup/workflow/rx1/WorkflowKt;
.super Ljava/lang/Object;
.source "Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Workflow.kt\ncom/squareup/workflow/rx1/WorkflowKt\n*L\n1#1,161:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001aj\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0006*\u00020\u0005\"\u0008\u0008\u0003\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00040\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0008\u001aj\u0010\t\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b0\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\n*\u00020\u0005\"\u0008\u0008\u0002\u0010\u000c*\u00020\u0005\"\u0008\u0008\u0003\u0010\u000b*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000c0\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u000b0\u0008\u001aj\u0010\r\u001a\u0014\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u000f*\u00020\u0005\"\u0008\u0008\u0001\u0010\u000e*\u00020\u0005\"\u0008\u0008\u0002\u0010\n*\u00020\u0005\"\u0008\u0008\u0003\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u0008\u001ar\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u000f*\u00020\u0005\"\u0008\u0008\u0001\u0010\u000e*\u00020\u0005\"\u0008\u0008\u0002\u0010\n*\u00020\u0005\"\u0008\u0008\u0003\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u00012\u001a\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u0002H\u000f\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u0002H\u000e0\u00110\u0008\u001aL\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u0013\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\n*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u0001\u001a\u001e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u0015\"\u0004\u0008\u0000\u0010\u0016*\u0008\u0012\u0004\u0012\u0002H\u00160\u0017H\u0002\u001aL\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\n*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00040\u0013\u00a8\u0006\u0019"
    }
    d2 = {
        "adaptEvents",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "S",
        "E2",
        "O",
        "",
        "E1",
        "transform",
        "Lkotlin/Function1;",
        "mapResult",
        "E",
        "O2",
        "O1",
        "mapState",
        "S2",
        "S1",
        "switchMapState",
        "Lrx/Observable;",
        "toCoreWorkflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "toDeferred",
        "Lkotlinx/coroutines/Deferred;",
        "T",
        "Lrx/Single;",
        "toRx1Workflow",
        "pure-rx1"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E2:",
            "Ljava/lang/Object;",
            "E1:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS;-TE1;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TE2;+TE1;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE2;TO;>;"
        }
    .end annotation

    const-string v0, "$this$adaptEvents"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->adaptEvents(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final mapResult(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O1:",
            "Ljava/lang/Object;",
            "O2:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS;-TE;+TO1;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TO1;+TO2;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE;TO2;>;"
        }
    .end annotation

    const-string v0, "$this$mapResult"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    new-instance v0, Lcom/squareup/workflow/rx1/WorkflowKt$mapResult$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/rx1/WorkflowKt$mapResult$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapResult(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S1:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS1;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TS1;+TS2;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS2;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$mapState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    new-instance v0, Lcom/squareup/workflow/rx1/WorkflowKt$mapState$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/rx1/WorkflowKt$mapState$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final switchMapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S1:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS1;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TS1;+",
            "Lrx/Observable<",
            "+TS2;>;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS2;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$switchMapState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    new-instance v0, Lcom/squareup/workflow/rx1/WorkflowKt$switchMapState$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/rx1/WorkflowKt$switchMapState$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS;-TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$toCoreWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-interface {p0}, Lcom/squareup/workflow/rx1/Workflow;->getResult()Lrx/Single;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toDeferred(Lrx/Single;)Lkotlinx/coroutines/Deferred;

    move-result-object v0

    .line 134
    invoke-interface {p0}, Lcom/squareup/workflow/rx1/Workflow;->getState()Lrx/Observable;

    move-result-object v1

    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 138
    new-instance v2, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$1;

    invoke-direct {v2, p0}, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$1;-><init>(Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v2}, Lkotlinx/coroutines/Deferred;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    .line 145
    new-instance v2, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$2;-><init>(Lcom/squareup/workflow/rx1/Workflow;Lio/reactivex/Observable;Lkotlinx/coroutines/Deferred;)V

    check-cast v2, Lcom/squareup/workflow/legacy/Workflow;

    return-object v2
.end method

.method private static final toDeferred(Lrx/Single;)Lkotlinx/coroutines/Deferred;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "TT;>;)",
            "Lkotlinx/coroutines/Deferred<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 150
    invoke-static {v0, v1, v0}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v0

    .line 151
    new-instance v1, Lcom/squareup/workflow/rx1/WorkflowKt$toDeferred$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt$toDeferred$1$1;-><init>(Lkotlinx/coroutines/CompletableDeferred;)V

    check-cast v1, Lrx/SingleSubscriber;

    invoke-virtual {p0, v1}, Lrx/Single;->subscribe(Lrx/SingleSubscriber;)Lrx/Subscription;

    move-result-object p0

    .line 159
    new-instance v1, Lcom/squareup/workflow/rx1/WorkflowKt$toDeferred$1$2$1;

    invoke-direct {v1, p0}, Lcom/squareup/workflow/rx1/WorkflowKt$toDeferred$1$2$1;-><init>(Lrx/Subscription;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v1}, Lkotlinx/coroutines/CompletableDeferred;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    .line 150
    check-cast v0, Lkotlinx/coroutines/Deferred;

    return-object v0
.end method

.method public static final toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;-TE;+TO;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$toRx1Workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance v0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    check-cast v0, Lcom/squareup/workflow/rx1/Workflow;

    return-object v0
.end method
