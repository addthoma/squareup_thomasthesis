.class public final Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;
.super Ljava/lang/Object;
.source "Workflow.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Workflow;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Workflow<",
        "TS;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0015\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u0011J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016RB\u0010\u0002\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0004*\u0004\u0018\u00018\u00028\u0002 \u0004*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0004*\u0004\u0018\u00018\u00028\u0002\u0018\u00010\u00030\u0003X\u0096\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0005\u0010\u0006\u001a\u0004\u0008\u0007\u0010\u0008R<\u0010\t\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0004*\u0004\u0018\u00018\u00008\u0000 \u0004*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0004*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\n0\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0014"
    }
    d2 = {
        "com/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "result",
        "Lrx/Single;",
        "kotlin.jvm.PlatformType",
        "result$annotations",
        "()V",
        "getResult",
        "()Lrx/Single;",
        "state",
        "Lrx/Observable;",
        "getState",
        "()Lrx/Observable;",
        "abandon",
        "",
        "sendEvent",
        "event",
        "(Ljava/lang/Object;)V",
        "toCompletable",
        "Lrx/Completable;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

.field private final result:Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Single<",
            "+TO;>;"
        }
    .end annotation
.end field

.field private final state:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "+TS;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;-TE;+TO;>;)V"
        }
    .end annotation

    .line 107
    iput-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->$this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iget-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->$this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt;->getState(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    sget-object v0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->state:Lrx/Observable;

    .line 110
    iget-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->$this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt;->getResult(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Maybe;

    move-result-object p1

    invoke-static {}, Lio/reactivex/Single;->never()Lio/reactivex/Single;

    move-result-object v0

    check-cast v0, Lio/reactivex/SingleSource;

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->switchIfEmpty(Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->result:Lrx/Single;

    return-void
.end method

.method public static synthetic result$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public abandon()V
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->$this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    return-void
.end method

.method public getResult()Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "+TO;>;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->result:Lrx/Single;

    return-object v0
.end method

.method public getState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "+TS;>;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->state:Lrx/Observable;

    return-object v0
.end method

.method public sendEvent(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->$this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public toCompletable()Lrx/Completable;
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toRx1Workflow$1;->$this_toRx1Workflow:Lcom/squareup/workflow/legacy/Workflow;

    check-cast v0, Lkotlinx/coroutines/Job;

    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    check-cast v1, Lkotlin/coroutines/CoroutineContext;

    invoke-static {v0, v1}, Lkotlinx/coroutines/rx2/RxConvertKt;->asCompletable(Lkotlinx/coroutines/Job;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/Completable;

    move-result-object v0

    check-cast v0, Lio/reactivex/CompletableSource;

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Completable(Lio/reactivex/CompletableSource;)Lrx/Completable;

    move-result-object v0

    const-string v1, "toV1Completable(asComple\u2026(Dispatchers.Unconfined))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
