.class public interface abstract Lcom/squareup/workflow/rx1/Reactor;
.super Ljava/lang/Object;
.source "Reactor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/rx1/Reactor$DefaultImpls;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008g\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u00020\u0002J\u0015\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0008J7\u0010\t\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u000b0\n2\u0006\u0010\u0007\u001a\u00028\u00002\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\rH&\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/workflow/rx1/Reactor;",
        "S",
        "",
        "E",
        "O",
        "onAbandoned",
        "",
        "state",
        "(Ljava/lang/Object;)V",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onAbandoned(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation
.end method

.method public abstract onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "TE;>;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "TS;TO;>;>;"
        }
    .end annotation
.end method
