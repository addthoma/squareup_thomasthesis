.class public interface abstract Lcom/squareup/workflow/rx1/WorkflowLauncher;
.super Ljava/lang/Object;
.source "WorkflowPools.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.legacy.WorkflowPool.Launcher"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008g\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u00020\u0002J/\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00062\u0006\u0010\u0007\u001a\u00028\u00002\u0006\u0010\u0008\u001a\u00020\tH&\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/workflow/rx1/WorkflowLauncher;",
        "S",
        "",
        "E",
        "O",
        "launch",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation
.end method
