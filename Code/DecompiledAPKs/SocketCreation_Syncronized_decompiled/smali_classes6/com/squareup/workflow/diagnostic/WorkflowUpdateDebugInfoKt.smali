.class public final Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfoKt;
.super Ljava/lang/Object;
.source "WorkflowUpdateDebugInfo.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowUpdateDebugInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowUpdateDebugInfo.kt\ncom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfoKt\n*L\n1#1,168:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u0001*\u00060\u0002j\u0002`\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "writeUpdate",
        "",
        "Ljava/lang/StringBuilder;",
        "Lkotlin/text/StringBuilder;",
        "update",
        "Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$writeUpdate(Ljava/lang/StringBuilder;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfoKt;->writeUpdate(Ljava/lang/StringBuilder;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V

    return-void
.end method

.method private static final writeUpdate(Ljava/lang/StringBuilder;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V
    .locals 4

    .line 135
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->getWorkflowType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    .line 136
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 138
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->getKind()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    move-result-object p1

    .line 139
    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;

    const-string v1, "\u21b3 "

    const-string v2, "append(value)"

    if-eqz v0, :cond_2

    const-string v0, "updated from "

    .line 140
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;->getSource()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;

    move-result-object p1

    .line 142
    sget-object v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Sink;->INSTANCE:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Sink;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "sink"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 143
    :cond_0
    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Worker;

    const-string v3, "]: "

    if-eqz v0, :cond_1

    const-string v0, "worker[key="

    .line 144
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Worker;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Worker;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Worker;->getOutput()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 149
    :cond_1
    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;

    if-eqz v0, :cond_3

    const-string v0, "workflow[key="

    .line 150
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;->getOutput()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/text/StringsKt;->appendln(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;->getChildInfo()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->toDescriptionString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 159
    :cond_2
    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Passthrough;

    if-eqz v0, :cond_3

    const-string v0, "passing through from workflow[key="

    .line 160
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Passthrough;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Passthrough;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    .line 162
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/text/StringsKt;->appendln(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Passthrough;->getChildInfo()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->toDescriptionString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_0
    return-void
.end method
