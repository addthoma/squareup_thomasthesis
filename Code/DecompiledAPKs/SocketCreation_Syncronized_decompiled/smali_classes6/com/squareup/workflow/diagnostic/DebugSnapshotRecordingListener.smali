.class public final Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;
.super Ljava/lang/Object;
.source "DebugSnapshotRecordingListener.kt"

# interfaces
.implements Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;,
        Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDebugSnapshotRecordingListener.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DebugSnapshotRecordingListener.kt\ncom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener\n*L\n1#1,215:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001:\u000212B!\u0012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u0010\u001a\u00020\u00062\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\u001a\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u000b2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J$\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u000b2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0012H\u0016J\u0008\u0010\u0018\u001a\u00020\u0006H\u0016J \u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u000b2\u000e\u0010\u001a\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001bH\u0016J \u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020\u0012H\u0016J(\u0010 \u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\"H\u0016J\u0018\u0010$\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u000bH\u0016J>\u0010%\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u000b2\u000e\u0010\u001a\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001b2\u0008\u0010&\u001a\u0004\u0018\u00010\u00122\u0008\u0010\'\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0012H\u0016JK\u0010(\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u000b2\u0008\u0010)\u001a\u0004\u0018\u00010\u000b2\u0006\u0010*\u001a\u00020\"2\u0006\u0010!\u001a\u00020\"2\u0008\u0010+\u001a\u0004\u0018\u00010\u00122\u0008\u0010,\u001a\u0004\u0018\u00010\u00122\u0006\u0010-\u001a\u00020.H\u0016\u00a2\u0006\u0002\u0010/J\u0010\u00100\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u000bH\u0016R\u0010\u0010\u0008\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0012\u0004\u0012\u00020\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000f0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "debugger",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;",
        "Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;",
        "",
        "(Lkotlin/jvm/functions/Function2;)V",
        "currentUpdate",
        "recordersById",
        "",
        "",
        "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;",
        "rootRecorder",
        "workersById",
        "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;",
        "onAfterRenderPass",
        "rendering",
        "",
        "onAfterWorkflowRendered",
        "workflowId",
        "onBeforeWorkflowRendered",
        "props",
        "state",
        "onRuntimeStopped",
        "onSinkReceived",
        "action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "onWorkerOutput",
        "workerId",
        "parentWorkflowId",
        "output",
        "onWorkerStarted",
        "key",
        "",
        "description",
        "onWorkerStopped",
        "onWorkflowAction",
        "oldState",
        "newState",
        "onWorkflowStarted",
        "parentId",
        "workflowType",
        "initialProps",
        "initialState",
        "restoredFromSnapshot",
        "",
        "(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V",
        "onWorkflowStopped",
        "WorkerRecorder",
        "WorkflowRecorder",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

.field private final debugger:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;",
            "Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final recordersById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private rootRecorder:Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

.field private final workersById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;",
            "-",
            "Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "debugger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->debugger:Lkotlin/jvm/functions/Function2;

    .line 75
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    .line 76
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->workersById:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public onAfterRenderPass(Ljava/lang/Object;)V
    .locals 2

    .line 138
    iget-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->rootRecorder:Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->workersById:Ljava/util/Map;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->snapshot(Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    move-result-object p1

    .line 139
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->debugger:Lkotlin/jvm/functions/Function2;

    iget-object v1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    invoke-interface {v0, p1, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    .line 140
    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    return-void
.end method

.method public onAfterSnapshotPass()V
    .locals 0

    .line 34
    invoke-static {p0}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;->onAfterSnapshotPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    return-void
.end method

.method public onAfterWorkflowRendered(JLjava/lang/Object;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    invoke-virtual {p1, p3}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->setRendering(Ljava/lang/Object;)V

    return-void
.end method

.method public onBeforeRenderPass(Ljava/lang/Object;)V
    .locals 0

    .line 34
    invoke-static {p0, p1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;->onBeforeRenderPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Ljava/lang/Object;)V

    return-void
.end method

.method public onBeforeSnapshotPass()V
    .locals 0

    .line 34
    invoke-static {p0}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;->onBeforeSnapshotPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    return-void
.end method

.method public onBeforeWorkflowRendered(JLjava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 123
    check-cast p1, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 124
    invoke-virtual {p1, p3}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->setProps(Ljava/lang/Object;)V

    .line 125
    invoke-virtual {p1, p4}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->setState(Ljava/lang/Object;)V

    return-void
.end method

.method public onPropsChanged(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 34
    invoke-static/range {p0 .. p5}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;->onPropsChanged(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onRuntimeStarted(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V
    .locals 1

    const-string v0, "workflowScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rootWorkflowType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;->onRuntimeStarted(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V

    return-void
.end method

.method public onRuntimeStopped()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public onSinkReceived(JLcom/squareup/workflow/WorkflowAction;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;)V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    iget-object p3, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p3, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 186
    new-instance p2, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    .line 187
    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getType()Ljava/lang/String;

    move-result-object p1

    .line 188
    new-instance p3, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;

    sget-object v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Sink;->INSTANCE:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Sink;

    check-cast v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;

    invoke-direct {p3, v0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;)V

    check-cast p3, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    .line 186
    invoke-direct {p2, p1, p3}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V

    iput-object p2, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    return-void
.end method

.method public onWorkerOutput(JJLjava/lang/Object;)V
    .locals 1

    const-string v0, "output"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->workersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;

    .line 174
    iget-object p2, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-static {p2, p3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 175
    new-instance p3, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    .line 176
    invoke-virtual {p2}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getType()Ljava/lang/String;

    move-result-object p2

    .line 177
    new-instance p4, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;

    new-instance v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Worker;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p5}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Worker;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;

    invoke-direct {p4, v0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;)V

    check-cast p4, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    .line 175
    invoke-direct {p3, p2, p4}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V

    iput-object p3, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    return-void
.end method

.method public onWorkerStarted(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "key"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;

    invoke-direct {v0, p5, p6}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object p5, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->workersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p6

    invoke-interface {p5, p6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object p5, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-static {p5, p3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    invoke-virtual {p3}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getWorkerIds()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/util/Collection;

    .line 156
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onWorkerStopped(JJ)V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->workersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-static {v0, p3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    invoke-virtual {p3}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getWorkerIds()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/util/Collection;

    .line 165
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onWorkflowAction(JLcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    const-string p4, "action"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    iget-object p3, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p3, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 202
    iget-object p2, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getParentId()Ljava/lang/Long;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    if-eqz p2, :cond_2

    .line 204
    iget-object p3, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    if-nez p3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 205
    :cond_0
    new-instance p4, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    .line 206
    invoke-virtual {p2}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getType()Ljava/lang/String;

    move-result-object p2

    if-nez p6, :cond_1

    .line 208
    new-instance p5, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Passthrough;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p5, p1, p3}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Passthrough;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V

    check-cast p5, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    goto :goto_0

    .line 210
    :cond_1
    new-instance p5, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;

    new-instance v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;

    invoke-virtual {p1}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p6, p3}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source$Subtree;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V

    check-cast v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;

    invoke-direct {p5, v0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind$Updated;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;)V

    check-cast p5, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    .line 205
    :goto_0
    invoke-direct {p4, p2, p5}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V

    iput-object p4, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->currentUpdate:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    :cond_2
    return-void
.end method

.method public onWorkflowStarted(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0

    const-string p6, "workflowType"

    invoke-static {p4, p6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p6, "key"

    invoke-static {p5, p6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance p6, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    invoke-direct {p6, p3, p4, p5}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object p4, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    invoke-interface {p4, p5, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    .line 98
    iget-object p4, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p4, p3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 99
    invoke-virtual {p3}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getChildrenIds()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/util/Collection;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 101
    :cond_0
    iget-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->rootRecorder:Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 102
    iput-object p6, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->rootRecorder:Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    :goto_1
    return-void

    .line 101
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Failed requirement."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onWorkflowStopped(J)V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 111
    invoke-virtual {v0}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getParentId()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 112
    iget-object v1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;->recordersById:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getParentId()Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 113
    invoke-virtual {v0}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->getChildrenIds()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method
