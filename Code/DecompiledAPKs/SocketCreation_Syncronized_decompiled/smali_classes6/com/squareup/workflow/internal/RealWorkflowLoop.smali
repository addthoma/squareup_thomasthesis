.class public Lcom/squareup/workflow/internal/RealWorkflowLoop;
.super Ljava/lang/Object;
.source "WorkflowLoop.kt"

# interfaces
.implements Lcom/squareup/workflow/internal/WorkflowLoop;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0010\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002Jq\u0010\u0003\u001a\u0002H\u0004\"\u0004\u0008\u0000\u0010\u0005\"\u0004\u0008\u0001\u0010\u0006\"\u0008\u0008\u0002\u0010\u0007*\u00020\u0008\"\u0004\u0008\u0003\u0010\u00042\u001e\u0010\t\u001a\u001a\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00040\n2\u001e\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00040\u000c2\u0006\u0010\r\u001a\u0002H\u0005H\u0014\u00a2\u0006\u0002\u0010\u000eJ\u00cf\u0001\u0010\u000f\u001a\u00020\u0010\"\u0004\u0008\u0000\u0010\u0005\"\u0004\u0008\u0001\u0010\u0006\"\u0008\u0008\u0002\u0010\u0007*\u00020\u0008\"\u0004\u0008\u0003\u0010\u00042\u001e\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00040\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u0001H\u00062\u0006\u0010\u0015\u001a\u00020\u00162(\u0010\u0017\u001a$\u0008\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00040\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u00080\u00182\"\u0010\u001c\u001a\u001e\u0008\u0001\u0012\u0004\u0012\u0002H\u0007\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u00080\u00182\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001f\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/workflow/internal/RealWorkflowLoop;",
        "Lcom/squareup/workflow/internal/WorkflowLoop;",
        "()V",
        "doRender",
        "RenderingT",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "rootNode",
        "Lcom/squareup/workflow/internal/WorkflowNode;",
        "workflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "props",
        "(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;",
        "runWorkflowLoop",
        "",
        "Lkotlinx/coroutines/flow/Flow;",
        "initialSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "initialState",
        "workerContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "onRendering",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "Lkotlin/coroutines/Continuation;",
        "",
        "onOutput",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "(Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic runWorkflowLoop$suspendImpl(Lcom/squareup/workflow/internal/RealWorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 12

    .line 69
    new-instance v11, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v2, p2

    move-object/from16 v3, p8

    move-object v4, p1

    move-object v5, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v10}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;-><init>(Lcom/squareup/workflow/internal/RealWorkflowLoop;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/StatefulWorkflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)V

    check-cast v11, Lkotlin/jvm/functions/Function2;

    move-object/from16 v0, p9

    invoke-static {v11, v0}, Lkotlinx/coroutines/CoroutineScopeKt;->coroutineScope(Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected doRender(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/internal/WorkflowNode<",
            "TPropsT;TStateT;TOutputT;TRenderingT;>;",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;TPropsT;)TRenderingT;"
        }
    .end annotation

    const-string v0, "rootNode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p1, p2, p3}, Lcom/squareup/workflow/internal/WorkflowNode;->render(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public runWorkflowLoop(Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TPropsT;>;",
            "Lcom/squareup/workflow/Snapshot;",
            "TStateT;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "+TRenderingT;>;-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-TOutputT;-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-static/range {p0 .. p9}, Lcom/squareup/workflow/internal/RealWorkflowLoop;->runWorkflowLoop$suspendImpl(Lcom/squareup/workflow/internal/RealWorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
