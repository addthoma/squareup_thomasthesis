.class public final Lcom/squareup/workflow/internal/WorkflowNode;
.super Ljava/lang/Object;
.source "WorkflowNode.kt"

# interfaces
.implements Lkotlinx/coroutines/CoroutineScope;
.implements Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "StateT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner<",
        "TStateT;TOutputT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowNode.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowNode.kt\ncom/squareup/workflow/internal/WorkflowNode\n+ 2 ActiveStagingList.kt\ncom/squareup/workflow/internal/ActiveStagingList\n+ 3 InlineLinkedList.kt\ncom/squareup/workflow/internal/InlineLinkedList\n+ 4 IdCounter.kt\ncom/squareup/workflow/diagnostic/IdCounterKt\n*L\n1#1,290:1\n90#2:291\n61#2:298\n62#2,2:323\n85#2:325\n73#2:332\n76#2,5:339\n106#3,6:292\n76#3,24:299\n106#3,6:326\n106#3,6:333\n14#4:344\n14#4:345\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowNode.kt\ncom/squareup/workflow/internal/WorkflowNode\n*L\n147#1:291\n154#1:298\n154#1,2:323\n175#1:325\n240#1:332\n240#1,5:339\n147#1,6:292\n154#1,24:299\n175#1,6:326\n240#1,6:333\n276#1:344\n78#1:345\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u0002*\u0008\u0008\u0002\u0010\u0003*\u00020\u0004*\u0004\u0008\u0003\u0010\u00052\u00020\u00062\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0007B\u00ab\u0001\u0012\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\t\u0012\u001e\u0010\n\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b\u0012\u0006\u0010\u000c\u001a\u00028\u0000\u0012\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0016\u0008\u0002\u0010\u0011\u001a\u0010\u0012\u0004\u0012\u00028\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0012\u0012\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u0012\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u0012\n\u0008\u0002\u0010\u0019\u001a\u0004\u0018\u00018\u0001\u0012\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u001bJ-\u0010/\u001a\u0004\u0018\u0001H0\"\u0008\u0008\u0004\u00100*\u00020\u00042\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020$H\u0002\u00a2\u0006\u0002\u00102J\u0006\u00103\u001a\u000204JV\u00105\u001a\u0014\u0012\u0004\u0012\u0002H0\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020.\"\u0004\u0008\u0004\u001002\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u0002H0072\u0006\u00108\u001a\u0002092\u001e\u0010:\u001a\u001a\u0012\u0004\u0012\u0002H0\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020$0\u0012H\u0002J1\u0010;\u001a\u00028\u00032\u001c\u0010\n\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b2\u0006\u0010<\u001a\u00028\u0000\u00a2\u0006\u0002\u0010=J5\u0010>\u001a\u00028\u00032\u001e\u0010\n\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b2\u0006\u0010?\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010=JD\u0010@\u001a\u000204\"\u0004\u0008\u0004\u001002\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u0002H0072\u0006\u00108\u001a\u0002092\u001e\u0010:\u001a\u001a\u0012\u0004\u0012\u0002H0\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020$0\u0012H\u0016J\u001e\u0010\r\u001a\u00020A2\u0016\u0010\n\u001a\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bJ \u0010B\u001a\u000204\"\u0008\u0008\u0004\u00100*\u00020\u00042\u000e\u0010C\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H00DJ5\u0010E\u001a\u0002042\u001e\u0010\n\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b2\u0006\u0010F\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010GJ\u000e\u0010H\u001a\u0004\u0018\u00010A*\u00020\u000eH\u0002R\u0014\u0010\u001c\u001a\u00020\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0014\u0010\u001f\u001a\u00020\u0014X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\u0004\u0012\u00028\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\"\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020$0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\'\u001a\u00028\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010(R\u0010\u0010)\u001a\u00028\u0001X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010(R\u001a\u0010*\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010,\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030.0-X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006I"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/WorkflowNode;",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;",
        "id",
        "Lcom/squareup/workflow/internal/WorkflowId;",
        "workflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "initialProps",
        "snapshot",
        "Lokio/ByteString;",
        "baseContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "emitOutputToParent",
        "Lkotlin/Function1;",
        "parentDiagnosticId",
        "",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "idCounter",
        "Lcom/squareup/workflow/diagnostic/IdCounter;",
        "initialState",
        "workerContext",
        "(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;)V",
        "coroutineContext",
        "getCoroutineContext",
        "()Lkotlin/coroutines/CoroutineContext;",
        "diagnosticId",
        "getDiagnosticId$workflow_runtime",
        "()J",
        "eventActionsChannel",
        "Lkotlinx/coroutines/channels/Channel;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "getId",
        "()Lcom/squareup/workflow/internal/WorkflowId;",
        "lastProps",
        "Ljava/lang/Object;",
        "state",
        "subtreeManager",
        "Lcom/squareup/workflow/internal/SubtreeManager;",
        "workers",
        "Lcom/squareup/workflow/internal/ActiveStagingList;",
        "Lcom/squareup/workflow/internal/WorkerChildNode;",
        "applyAction",
        "T",
        "action",
        "(Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;",
        "cancel",
        "",
        "createWorkerNode",
        "worker",
        "Lcom/squareup/workflow/Worker;",
        "key",
        "",
        "handler",
        "render",
        "input",
        "(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;",
        "renderWithStateType",
        "props",
        "runningWorker",
        "Lcom/squareup/workflow/Snapshot;",
        "tick",
        "selector",
        "Lkotlinx/coroutines/selects/SelectBuilder;",
        "updatePropsAndState",
        "newProps",
        "(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)V",
        "restoreState",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final coroutineContext:Lkotlin/coroutines/CoroutineContext;

.field private final diagnosticId:J

.field private final diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

.field private final emitOutputToParent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TOutputT;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final eventActionsChannel:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;>;"
        }
    .end annotation
.end field

.field private final id:Lcom/squareup/workflow/internal/WorkflowId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "TPropsT;TOutputT;TRenderingT;>;"
        }
    .end annotation
.end field

.field private final idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

.field private lastProps:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPropsT;"
        }
    .end annotation
.end field

.field private state:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TStateT;"
        }
    .end annotation
.end field

.field private final subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/SubtreeManager<",
            "TStateT;TOutputT;>;"
        }
    .end annotation
.end field

.field private final workerContext:Lkotlin/coroutines/CoroutineContext;

.field private final workers:Lcom/squareup/workflow/internal/ActiveStagingList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/ActiveStagingList<",
            "Lcom/squareup/workflow/internal/WorkerChildNode<",
            "***>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "-TPropsT;TOutputT;+TRenderingT;>;",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;TPropsT;",
            "Lokio/ByteString;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/jvm/functions/Function1<",
            "-TOutputT;+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            "Lcom/squareup/workflow/diagnostic/IdCounter;",
            "TStateT;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v7, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p11

    const-string v8, "id"

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "workflow"

    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "baseContext"

    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "emitOutputToParent"

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "workerContext"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->id:Lcom/squareup/workflow/internal/WorkflowId;

    iput-object v5, v0, Lcom/squareup/workflow/internal/WorkflowNode;->emitOutputToParent:Lkotlin/jvm/functions/Function1;

    move-object/from16 v1, p8

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

    iput-object v6, v0, Lcom/squareup/workflow/internal/WorkflowNode;->workerContext:Lkotlin/coroutines/CoroutineContext;

    .line 71
    sget-object v1, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v1, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {v4, v1}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object v1

    check-cast v1, Lkotlinx/coroutines/Job;

    invoke-static {v1}, Lkotlinx/coroutines/JobKt;->Job(Lkotlinx/coroutines/Job;)Lkotlinx/coroutines/CompletableJob;

    move-result-object v1

    check-cast v1, Lkotlin/coroutines/CoroutineContext;

    invoke-interface {v4, v1}, Lkotlin/coroutines/CoroutineContext;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object v1

    new-instance v4, Lkotlinx/coroutines/CoroutineName;

    iget-object v5, v0, Lcom/squareup/workflow/internal/WorkflowNode;->id:Lcom/squareup/workflow/internal/WorkflowId;

    invoke-virtual {v5}, Lcom/squareup/workflow/internal/WorkflowId;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lkotlinx/coroutines/CoroutineName;-><init>(Ljava/lang/String;)V

    check-cast v4, Lkotlin/coroutines/CoroutineContext;

    invoke-interface {v1, v4}, Lkotlin/coroutines/CoroutineContext;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->coroutineContext:Lkotlin/coroutines/CoroutineContext;

    .line 78
    iget-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

    if-eqz v1, :cond_0

    .line 345
    invoke-virtual {v1}, Lcom/squareup/workflow/diagnostic/IdCounter;->createId()J

    move-result-wide v4

    goto :goto_0

    :cond_0
    const-wide/16 v4, 0x0

    :goto_0
    iput-wide v4, v0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    .line 80
    new-instance v1, Lcom/squareup/workflow/internal/SubtreeManager;

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/workflow/internal/WorkflowNode;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v9

    new-instance v4, Lcom/squareup/workflow/internal/WorkflowNode$subtreeManager$1;

    move-object v5, v0

    check-cast v5, Lcom/squareup/workflow/internal/WorkflowNode;

    invoke-direct {v4, v5}, Lcom/squareup/workflow/internal/WorkflowNode$subtreeManager$1;-><init>(Lcom/squareup/workflow/internal/WorkflowNode;)V

    move-object v10, v4

    check-cast v10, Lkotlin/jvm/functions/Function1;

    iget-wide v11, v0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    iget-object v13, v0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iget-object v14, v0, Lcom/squareup/workflow/internal/WorkflowNode;->idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

    iget-object v15, v0, Lcom/squareup/workflow/internal/WorkflowNode;->workerContext:Lkotlin/coroutines/CoroutineContext;

    move-object v8, v1

    .line 80
    invoke-direct/range {v8 .. v15}, Lcom/squareup/workflow/internal/SubtreeManager;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;JLcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Lkotlin/coroutines/CoroutineContext;)V

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;

    .line 84
    new-instance v1, Lcom/squareup/workflow/internal/ActiveStagingList;

    invoke-direct {v1}, Lcom/squareup/workflow/internal/ActiveStagingList;-><init>()V

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->workers:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 88
    iput-object v7, v0, Lcom/squareup/workflow/internal/WorkflowNode;->lastProps:Ljava/lang/Object;

    const v1, 0x7fffffff

    .line 90
    invoke-static {v1}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->eventActionsChannel:Lkotlinx/coroutines/channels/Channel;

    const/4 v1, 0x0

    if-eqz p10, :cond_1

    move-object/from16 v2, p10

    const/4 v9, 0x0

    goto :goto_2

    :cond_1
    if-eqz v3, :cond_2

    .line 97
    invoke-direct {v0, v3}, Lcom/squareup/workflow/internal/WorkflowNode;->restoreState(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object v3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_3

    const/4 v1, 0x1

    .line 101
    :cond_3
    invoke-virtual {v2, v7, v3}, Lcom/squareup/workflow/StatefulWorkflow;->initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;

    move-result-object v2

    move v9, v1

    .line 94
    :goto_2
    iput-object v2, v0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    .line 104
    iget-object v1, v0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v1, :cond_5

    .line 106
    iget-wide v2, v0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    iget-object v4, v0, Lcom/squareup/workflow/internal/WorkflowNode;->id:Lcom/squareup/workflow/internal/WorkflowId;

    invoke-virtual {v4}, Lcom/squareup/workflow/internal/WorkflowId;->getTypeDebugString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, v0, Lcom/squareup/workflow/internal/WorkflowNode;->id:Lcom/squareup/workflow/internal/WorkflowId;

    invoke-virtual {v4}, Lcom/squareup/workflow/internal/WorkflowId;->getName$workflow_runtime()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    move-object/from16 v4, p7

    move-object/from16 v7, p3

    .line 105
    invoke-interface/range {v1 .. v9}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkflowStarted(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/workflow/internal/WorkflowNode;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v1

    sget-object v2, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v2, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {v1, v2}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast v1, Lkotlinx/coroutines/Job;

    new-instance v2, Lcom/squareup/workflow/internal/WorkflowNode$2;

    invoke-direct {v2, v0}, Lcom/squareup/workflow/internal/WorkflowNode$2;-><init>(Lcom/squareup/workflow/internal/WorkflowNode;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, v2}, Lkotlinx/coroutines/Job;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    :cond_5
    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 14

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_0

    .line 59
    sget-object v1, Lcom/squareup/workflow/internal/WorkflowNode$1;->INSTANCE:Lcom/squareup/workflow/internal/WorkflowNode$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    move-object v8, v1

    goto :goto_0

    :cond_0
    move-object/from16 v8, p6

    :goto_0
    and-int/lit8 v1, v0, 0x40

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 60
    move-object v1, v2

    check-cast v1, Ljava/lang/Long;

    move-object v9, v1

    goto :goto_1

    :cond_1
    move-object/from16 v9, p7

    :goto_1
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_2

    .line 61
    move-object v1, v2

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-object v10, v1

    goto :goto_2

    :cond_2
    move-object/from16 v10, p8

    :goto_2
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_3

    .line 62
    move-object v1, v2

    check-cast v1, Lcom/squareup/workflow/diagnostic/IdCounter;

    move-object v11, v1

    goto :goto_3

    :cond_3
    move-object/from16 v11, p9

    :goto_3
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_4

    move-object v12, v2

    goto :goto_4

    :cond_4
    move-object/from16 v12, p10

    :goto_4
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    .line 64
    sget-object v0, Lkotlin/coroutines/EmptyCoroutineContext;->INSTANCE:Lkotlin/coroutines/EmptyCoroutineContext;

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    move-object v13, v0

    goto :goto_5

    :cond_5
    move-object/from16 v13, p11

    :goto_5
    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v13}, Lcom/squareup/workflow/internal/WorkflowNode;-><init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;)V

    return-void
.end method

.method public static final synthetic access$applyAction(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/workflow/internal/WorkflowNode;->applyAction(Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDiagnosticListener$p(Lcom/squareup/workflow/internal/WorkflowNode;)Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-object p0
.end method

.method private final applyAction(Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;)TT;"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/squareup/workflow/WorkflowActionKt;->applyTo(Lcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    iget-object v5, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    move-object v4, p1

    move-object v6, v8

    move-object v7, v0

    invoke-interface/range {v1 .. v7}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkflowAction(JLcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 264
    :cond_0
    iput-object v8, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 266
    iget-object p1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->emitOutputToParent:Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final createWorkerNode(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkerChildNode;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Worker<",
            "+TT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)",
            "Lcom/squareup/workflow/internal/WorkerChildNode<",
            "TT;TStateT;TOutputT;>;"
        }
    .end annotation

    move-object/from16 v9, p0

    .line 275
    iget-object v0, v9, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, v9, Lcom/squareup/workflow/internal/WorkflowNode;->idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {v0}, Lcom/squareup/workflow/diagnostic/IdCounter;->createId()J

    move-result-wide v1

    .line 277
    :cond_0
    iget-object v10, v9, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iget-wide v13, v9, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    move-wide v11, v1

    move-object/from16 v15, p2

    invoke-interface/range {v10 .. v16}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkerStarted(JJLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-wide v3, v1

    .line 280
    iget-wide v5, v9, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    iget-object v7, v9, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iget-object v8, v9, Lcom/squareup/workflow/internal/WorkflowNode;->workerContext:Lkotlin/coroutines/CoroutineContext;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static/range {v0 .. v8}, Lcom/squareup/workflow/internal/WorkersKt;->launchWorker(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Worker;Ljava/lang/String;JJLcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v13

    .line 281
    new-instance v0, Lcom/squareup/workflow/internal/WorkerChildNode;

    const/4 v14, 0x0

    const/16 v16, 0x8

    const/16 v17, 0x0

    move-object v10, v0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v15, p3

    invoke-direct/range {v10 .. v17}, Lcom/squareup/workflow/internal/WorkerChildNode;-><init>(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final renderWithStateType(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;TPropsT;)TRenderingT;"
        }
    .end annotation

    .line 226
    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/internal/WorkflowNode;->updatePropsAndState(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)V

    .line 228
    new-instance v0, Lcom/squareup/workflow/internal/RealRenderContext;

    .line 229
    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;

    check-cast v1, Lcom/squareup/workflow/internal/RealRenderContext$Renderer;

    .line 230
    move-object v2, p0

    check-cast v2, Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;

    .line 231
    iget-object v3, p0, Lcom/squareup/workflow/internal/WorkflowNode;->eventActionsChannel:Lkotlinx/coroutines/channels/Channel;

    check-cast v3, Lkotlinx/coroutines/channels/SendChannel;

    .line 228
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/workflow/internal/RealRenderContext;-><init>(Lcom/squareup/workflow/internal/RealRenderContext$Renderer;Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;Lkotlinx/coroutines/channels/SendChannel;)V

    .line 233
    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    iget-object v4, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    invoke-interface {v1, v2, v3, p2, v4}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onBeforeWorkflowRendered(JLjava/lang/Object;Ljava/lang/Object;)V

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/RenderContext;

    invoke-virtual {p1, p2, v1, v2}, Lcom/squareup/workflow/StatefulWorkflow;->render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;

    move-result-object p1

    .line 235
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/RealRenderContext;->freeze()V

    .line 236
    iget-object p2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz p2, :cond_1

    iget-wide v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    invoke-interface {p2, v0, v1, p1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onAfterWorkflowRendered(JLjava/lang/Object;)V

    .line 239
    :cond_1
    iget-object p2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;

    invoke-virtual {p2}, Lcom/squareup/workflow/internal/SubtreeManager;->commitRenderedChildren()V

    .line 240
    iget-object p2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->workers:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 332
    invoke-static {p2}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 333
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 335
    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/internal/WorkerChildNode;

    .line 240
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/WorkerChildNode;->getChannel()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v3, v2, v3}, Lkotlinx/coroutines/channels/ReceiveChannel$DefaultImpls;->cancel$default(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 336
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    .line 339
    :cond_2
    invoke-static {p2}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 340
    invoke-static {p2}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$setActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V

    .line 341
    invoke-static {p2, v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$setStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V

    .line 342
    invoke-static {p2}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/internal/InlineLinkedList;->clear()V

    return-object p1
.end method

.method private final restoreState(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 285
    invoke-static {p1}, Lcom/squareup/workflow/internal/TreeSnapshotsKt;->parseTreeSnapshot(Lokio/ByteString;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokio/ByteString;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 286
    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;

    invoke-virtual {v1, p1}, Lcom/squareup/workflow/internal/SubtreeManager;->restoreChildrenFromSnapshots(Ljava/util/List;)V

    if-eqz v0, :cond_0

    .line 287
    sget-object p1, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final updatePropsAndState(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;TPropsT;)V"
        }
    .end annotation

    .line 249
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->lastProps:Ljava/lang/Object;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->lastProps:Ljava/lang/Object;

    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    invoke-virtual {p1, v0, p2, v1}, Lcom/squareup/workflow/StatefulWorkflow;->onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 251
    iget-object v2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v2, :cond_0

    iget-wide v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/workflow/internal/WorkflowNode;->lastProps:Ljava/lang/Object;

    iget-object v6, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    move-object v5, p2

    move-object v7, p1

    invoke-interface/range {v2 .. v7}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onPropsChanged(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 252
    :cond_0
    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    .line 254
    :cond_1
    iput-object p2, p0, Lcom/squareup/workflow/internal/WorkflowNode;->lastProps:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 3

    .line 215
    invoke-virtual {p0}, Lcom/squareup/workflow/internal/WorkflowNode;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lkotlinx/coroutines/JobKt;->cancel$default(Lkotlin/coroutines/CoroutineContext;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    return-void
.end method

.method public getCoroutineContext()Lkotlin/coroutines/CoroutineContext;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->coroutineContext:Lkotlin/coroutines/CoroutineContext;

    return-object v0
.end method

.method public final getDiagnosticId$workflow_runtime()J
    .locals 2

    .line 78
    iget-wide v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->diagnosticId:J

    return-wide v0
.end method

.method public final getId()Lcom/squareup/workflow/internal/WorkflowId;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "TPropsT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->id:Lcom/squareup/workflow/internal/WorkflowId;

    return-object v0
.end method

.method public final render(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;*+TOutputT;+TRenderingT;>;TPropsT;)TRenderingT;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/internal/WorkflowNode;->renderWithStateType(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Worker<",
            "+TT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)V"
        }
    .end annotation

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->workers:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 291
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 294
    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/internal/WorkerChildNode;

    .line 148
    invoke-virtual {v1, p1, p2}, Lcom/squareup/workflow/internal/WorkerChildNode;->matches(Lcom/squareup/workflow/Worker;Ljava/lang/String;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 295
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_0
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected keys to be unique for "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ": key="

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 148
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->workers:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 298
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v1

    .line 299
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v2

    const/4 v3, 0x0

    .line 300
    move-object v4, v3

    check-cast v4, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-object v5, v4

    :goto_1
    if-eqz v2, :cond_5

    .line 303
    move-object v6, v2

    check-cast v6, Lcom/squareup/workflow/internal/WorkerChildNode;

    .line 155
    invoke-virtual {v6, p1, p2}, Lcom/squareup/workflow/internal/WorkerChildNode;->matches(Lcom/squareup/workflow/Worker;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-nez v5, :cond_2

    .line 307
    invoke-interface {v2}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/workflow/internal/InlineLinkedList;->setHead(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    goto :goto_2

    .line 309
    :cond_2
    invoke-interface {v2}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v3

    invoke-interface {v5, v3}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 311
    :goto_2
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->getTail()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 312
    invoke-virtual {v1, v5}, Lcom/squareup/workflow/internal/InlineLinkedList;->setTail(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 314
    :cond_3
    invoke-interface {v2, v4}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    goto :goto_3

    .line 319
    :cond_4
    invoke-interface {v2}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v5

    move-object v7, v5

    move-object v5, v2

    move-object v2, v7

    goto :goto_1

    :cond_5
    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_6

    goto :goto_4

    .line 156
    :cond_6
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/workflow/internal/WorkflowNode;->createWorkerNode(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkerChildNode;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    .line 323
    :goto_4
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/internal/InlineLinkedList;->plusAssign(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 154
    check-cast v2, Lcom/squareup/workflow/internal/WorkerChildNode;

    .line 158
    invoke-virtual {v2, p3}, Lcom/squareup/workflow/internal/WorkerChildNode;->setHandler(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final snapshot(Lcom/squareup/workflow/StatefulWorkflow;)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "****>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;

    invoke-virtual {v0}, Lcom/squareup/workflow/internal/SubtreeManager;->createChildSnapshots()Ljava/util/List;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode;->state:Ljava/lang/Object;

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/StatefulWorkflow;->snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 135
    invoke-static {p1, v0}, Lcom/squareup/workflow/internal/TreeSnapshotsKt;->createTreeSnapshot(Lcom/squareup/workflow/Snapshot;Ljava/util/List;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public final tick(Lkotlinx/coroutines/selects/SelectBuilder;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/selects/SelectBuilder<",
            "-TT;>;)V"
        }
    .end annotation

    const-string v0, "selector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->subtreeManager:Lcom/squareup/workflow/internal/SubtreeManager;

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/internal/SubtreeManager;->tickChildren(Lkotlinx/coroutines/selects/SelectBuilder;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->workers:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 325
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 328
    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/internal/WorkerChildNode;

    .line 177
    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkerChildNode;->getTombstone()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 180
    :cond_0
    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkerChildNode;->getChannel()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v3

    invoke-interface {v3}, Lkotlinx/coroutines/channels/ReceiveChannel;->getOnReceive()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;

    invoke-direct {v4, v1, v2, p0, p1}, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/internal/WorkerChildNode;Lcom/squareup/workflow/internal/WorkflowNode;Lkotlinx/coroutines/selects/SelectBuilder;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-interface {p1, v3, v4}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 329
    :goto_1
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode;->eventActionsChannel:Lkotlinx/coroutines/channels/Channel;

    invoke-interface {v0}, Lkotlinx/coroutines/channels/Channel;->getOnReceive()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v0

    new-instance v2, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$with$lambda$1;

    invoke-direct {v2, v1, p0}, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$with$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/internal/WorkflowNode;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-interface {p1, v0, v2}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
