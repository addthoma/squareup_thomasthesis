.class public final Lcom/squareup/workflow/internal/ValueOrDone$Companion;
.super Ljava/lang/Object;
.source "Workers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/internal/ValueOrDone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004J\u001f\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\u0004\"\u0004\u0008\u0001\u0010\u00082\u0006\u0010\u0007\u001a\u0002H\u0008\u00a2\u0006\u0002\u0010\tR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/ValueOrDone$Companion;",
        "",
        "()V",
        "Done",
        "Lcom/squareup/workflow/internal/ValueOrDone;",
        "",
        "done",
        "value",
        "T",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/internal/ValueOrDone;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/workflow/internal/ValueOrDone$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final done()Lcom/squareup/workflow/internal/ValueOrDone;
    .locals 1

    .line 123
    invoke-static {}, Lcom/squareup/workflow/internal/ValueOrDone;->access$getDone$cp()Lcom/squareup/workflow/internal/ValueOrDone;

    move-result-object v0

    return-object v0
.end method

.method public final value(Ljava/lang/Object;)Lcom/squareup/workflow/internal/ValueOrDone;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/workflow/internal/ValueOrDone<",
            "TT;>;"
        }
    .end annotation

    .line 122
    new-instance v0, Lcom/squareup/workflow/internal/ValueOrDone;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/internal/ValueOrDone;-><init>(Ljava/lang/Object;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
