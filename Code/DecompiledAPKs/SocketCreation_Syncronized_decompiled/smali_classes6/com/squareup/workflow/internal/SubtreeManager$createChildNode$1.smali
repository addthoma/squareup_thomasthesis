.class final Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SubtreeManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/SubtreeManager;->createChildNode(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkflowChildNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TChildOutputT;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0008\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0001\"\u0004\u0008\u0002\u0010\u0004\"\u0004\u0008\u0003\u0010\u0005\"\u0008\u0008\u0004\u0010\u0006*\u00020\u00012\u0006\u0010\u0007\u001a\u0002H\u0003H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "acceptChildOutput",
        "",
        "ChildPropsT",
        "ChildOutputT",
        "ChildRenderingT",
        "StateT",
        "OutputT",
        "output",
        "invoke",
        "(Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $node:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic this$0:Lcom/squareup/workflow/internal/SubtreeManager;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/internal/SubtreeManager;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;->this$0:Lcom/squareup/workflow/internal/SubtreeManager;

    iput-object p2, p0, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;->$node:Lkotlin/jvm/internal/Ref$ObjectRef;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TChildOutputT;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;->$node:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-nez v0, :cond_0

    const-string v1, "node"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/squareup/workflow/internal/WorkflowChildNode;

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/internal/WorkflowChildNode;->acceptChildOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 190
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;->this$0:Lcom/squareup/workflow/internal/SubtreeManager;

    invoke-static {v0}, Lcom/squareup/workflow/internal/SubtreeManager;->access$getEmitActionToParent$p(Lcom/squareup/workflow/internal/SubtreeManager;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
