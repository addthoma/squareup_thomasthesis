.class public final Lcom/squareup/workflow/internal/WorkflowIdKt;
.super Ljava/lang/Object;
.source "WorkflowId.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowId.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowId.kt\ncom/squareup/workflow/internal/WorkflowIdKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,70:1\n180#2:71\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowId.kt\ncom/squareup/workflow/internal/WorkflowIdKt\n*L\n63#1:71\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u001a\u001c\u0010\u0000\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u001a_\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0001\"\u001a\u0008\u0000\u0010\u0008*\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\t\"\u0004\u0008\u0001\u0010\u0005\"\u0008\u0008\u0002\u0010\u0006*\u00020\n\"\u0004\u0008\u0003\u0010\u0007*\u0002H\u00082\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0010\r\u001a\u0018\u0010\u000e\u001a\u00020\u0003*\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0001H\u0000*$\u0008\u0000\u0010\u000f\"\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0001\u00a8\u0006\u0010"
    }
    d2 = {
        "restoreId",
        "Lcom/squareup/workflow/internal/WorkflowId;",
        "bytes",
        "Lokio/ByteString;",
        "id",
        "I",
        "O",
        "R",
        "W",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "key",
        "",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Lcom/squareup/workflow/internal/WorkflowId;",
        "toByteString",
        "AnyId",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final id(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Lcom/squareup/workflow/internal/WorkflowId;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<W::",
            "Lcom/squareup/workflow/Workflow<",
            "-TI;+TO;+TR;>;I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(TW;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "TI;TO;TR;>;"
        }
    .end annotation

    const-string v0, "$this$id"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/squareup/workflow/internal/WorkflowId;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/internal/WorkflowId;-><init>(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic id$default(Lcom/squareup/workflow/Workflow;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/internal/WorkflowId;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, ""

    .line 54
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/workflow/internal/WorkflowIdKt;->id(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object p0

    return-object p0
.end method

.method public static final restoreId(Lokio/ByteString;)Lcom/squareup/workflow/internal/WorkflowId;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokio/ByteString;",
            ")",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;"
        }
    .end annotation

    const-string v0, "bytes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p0}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p0

    check-cast p0, Lokio/BufferedSource;

    .line 64
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    .line 68
    new-instance v1, Lcom/squareup/workflow/internal/WorkflowId;

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lcom/squareup/workflow/internal/WorkflowId;-><init>(Lkotlin/reflect/KClass;Ljava/lang/String;)V

    return-object v1

    .line 66
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.Class<out com.squareup.workflow.Workflow<kotlin.Nothing, kotlin.Any, kotlin.Any>>"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final toByteString(Lcom/squareup/workflow/internal/WorkflowId;)Lokio/ByteString;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;)",
            "Lokio/ByteString;"
        }
    .end annotation

    const-string v0, "$this$toByteString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 58
    move-object v1, v0

    check-cast v1, Lokio/BufferedSink;

    invoke-virtual {p0}, Lcom/squareup/workflow/internal/WorkflowId;->getType$workflow_runtime()Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "type.java.name"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/workflow/internal/WorkflowId;->getName$workflow_runtime()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 61
    invoke-virtual {v0}, Lokio/Buffer;->readByteString()Lokio/ByteString;

    move-result-object p0

    return-object p0
.end method
