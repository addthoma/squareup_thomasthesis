.class public final enum Lcom/squareup/workflow/MainAndModal;
.super Ljava/lang/Enum;
.source "LayeredScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/MainAndModal$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/workflow/MainAndModal;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u0000 \u00052\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/workflow/MainAndModal;",
        "",
        "(Ljava/lang/String;I)V",
        "MAIN",
        "MODAL",
        "Companion",
        "pure"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/workflow/MainAndModal;

.field public static final Companion:Lcom/squareup/workflow/MainAndModal$Companion;

.field public static final enum MAIN:Lcom/squareup/workflow/MainAndModal;

.field public static final enum MODAL:Lcom/squareup/workflow/MainAndModal;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/MainAndModal;

    new-instance v1, Lcom/squareup/workflow/MainAndModal;

    const/4 v2, 0x0

    const-string v3, "MAIN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/workflow/MainAndModal;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/workflow/MainAndModal;

    const/4 v2, 0x1

    const-string v3, "MODAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/workflow/MainAndModal;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/workflow/MainAndModal;->$VALUES:[Lcom/squareup/workflow/MainAndModal;

    new-instance v0, Lcom/squareup/workflow/MainAndModal$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/MainAndModal$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static final onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/workflow/MainAndModal;
    .locals 1

    const-class v0, Lcom/squareup/workflow/MainAndModal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/MainAndModal;

    return-object p0
.end method

.method public static values()[Lcom/squareup/workflow/MainAndModal;
    .locals 1

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->$VALUES:[Lcom/squareup/workflow/MainAndModal;

    invoke-virtual {v0}, [Lcom/squareup/workflow/MainAndModal;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/workflow/MainAndModal;

    return-object v0
.end method
