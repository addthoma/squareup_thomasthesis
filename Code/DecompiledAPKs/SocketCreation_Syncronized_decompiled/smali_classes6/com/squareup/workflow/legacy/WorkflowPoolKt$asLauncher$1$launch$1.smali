.class final Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowPool.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function4<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlinx/coroutines/channels/SendChannel<",
        "-TI;>;",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "Lkotlin/coroutines/Continuation<",
        "-TO;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0001*\u00020\u0003*\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u008a@\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "O",
        "I",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "<anonymous parameter 0>",
        "Lkotlinx/coroutines/channels/SendChannel;",
        "<anonymous parameter 1>",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.WorkflowPoolKt$asLauncher$1$launch$1"
    f = "WorkflowPool.kt"
    i = {
        0x0,
        0x0,
        0x0
    }
    l = {
        0x1a3
    }
    m = "invokeSuspend"
    n = {
        "$this$workflow",
        "<anonymous parameter 0>",
        "<anonymous parameter 1>"
    }
    s = {
        "L$0",
        "L$1",
        "L$2"
    }
.end annotation


# instance fields
.field final synthetic $initialState:Ljava/lang/Object;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field private p$0:Lkotlinx/coroutines/channels/SendChannel;

.field private p$1:Lkotlinx/coroutines/channels/ReceiveChannel;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;Ljava/lang/Object;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->$initialState:Ljava/lang/Object;

    const/4 p1, 0x4

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/SendChannel;Lkotlinx/coroutines/channels/ReceiveChannel;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lkotlinx/coroutines/channels/SendChannel<",
            "-TI;>;",
            "Lkotlinx/coroutines/channels/ReceiveChannel;",
            "Lkotlin/coroutines/Continuation<",
            "-TO;>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$create"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 1>"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "continuation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;

    iget-object v2, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->$initialState:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p4}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;Ljava/lang/Object;Lkotlin/coroutines/Continuation;)V

    iput-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iput-object p2, v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->p$0:Lkotlinx/coroutines/channels/SendChannel;

    iput-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->p$1:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lkotlinx/coroutines/channels/SendChannel;

    check-cast p3, Lkotlinx/coroutines/channels/ReceiveChannel;

    check-cast p4, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->create(Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/SendChannel;Lkotlinx/coroutines/channels/ReceiveChannel;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 418
    iget v1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->L$2:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->L$1:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/channels/SendChannel;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->p$0:Lkotlinx/coroutines/channels/SendChannel;

    iget-object v3, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->p$1:Lkotlinx/coroutines/channels/ReceiveChannel;

    .line 419
    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;

    iget-object v4, v4, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;->$this_asLauncher:Lcom/squareup/workflow/legacy/Worker;

    iget-object v5, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->$initialState:Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->L$1:Ljava/lang/Object;

    iput-object v3, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->L$2:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1$launch$1;->label:I

    invoke-interface {v4, v5, p0}, Lcom/squareup/workflow/legacy/Worker;->call(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method
