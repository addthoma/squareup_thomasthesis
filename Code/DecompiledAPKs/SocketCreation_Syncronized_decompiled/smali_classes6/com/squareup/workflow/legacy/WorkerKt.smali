.class public final Lcom/squareup/workflow/legacy/WorkerKt;
.super Ljava/lang/Object;
.source "Worker.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aT\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u0006\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00052\"\u0010\u000c\u001a\u001e\u0008\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00040\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u00050\rH\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u001a(\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u0002H\u00120\u0006\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0005*\u0008\u0012\u0004\u0012\u0002H\u00120\u0013H\u0007\"U\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0005\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u00020\u0005*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u00068\u00c0\u0002X\u0081\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\t\u0010\n\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0014"
    }
    d2 = {
        "workflowType",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "I",
        "",
        "O",
        "",
        "Lcom/squareup/workflow/legacy/Worker;",
        "workflowType$annotations",
        "(Lcom/squareup/workflow/legacy/Worker;)V",
        "getWorkflowType",
        "(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "worker",
        "block",
        "Lkotlin/Function2;",
        "Lkotlin/coroutines/Continuation;",
        "(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Worker;",
        "asWorker",
        "",
        "T",
        "Lkotlinx/coroutines/Deferred;",
        "legacy-workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asWorker(Lkotlinx/coroutines/Deferred;)Lcom/squareup/workflow/legacy/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/Deferred<",
            "+TT;>;)",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/workflow/legacy/WorkerKt$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/workflow/legacy/WorkerKt$asWorker$1;-><init>(Lkotlinx/coroutines/Deferred;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkerKt;->worker(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic getWorkflowType(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPool$Type;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TI;*TO;>;"
        }
    .end annotation

    const-string v0, "$this$workflowType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v0, 0x4

    const-string v1, "I"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Ljava/lang/Void;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "O"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    return-object p0
.end method

.method public static final worker(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function2<",
            "-TI;-",
            "Lkotlin/coroutines/Continuation<",
            "-TO;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Worker<",
            "TI;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/workflow/legacy/WorkerKt$worker$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/legacy/WorkerKt$worker$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Worker;

    return-object v0
.end method

.method public static synthetic workflowType$annotations(Lcom/squareup/workflow/legacy/Worker;)V
    .locals 0

    return-void
.end method
