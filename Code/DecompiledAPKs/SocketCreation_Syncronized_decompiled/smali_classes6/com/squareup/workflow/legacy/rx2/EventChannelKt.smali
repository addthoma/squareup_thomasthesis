.class public final Lcom/squareup/workflow/legacy/rx2/EventChannelKt;
.super Ljava/lang/Object;
.source "EventChannel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "asEventChannel",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "E",
        "",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "legacy-workflow-rx2"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asEventChannel(Lkotlinx/coroutines/channels/ReceiveChannel;)Lcom/squareup/workflow/legacy/rx2/EventChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TE;>;)",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "TE;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$asEventChannel"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;-><init>(Lkotlinx/coroutines/channels/ReceiveChannel;)V

    check-cast v0, Lcom/squareup/workflow/legacy/rx2/EventChannel;

    return-object v0
.end method
