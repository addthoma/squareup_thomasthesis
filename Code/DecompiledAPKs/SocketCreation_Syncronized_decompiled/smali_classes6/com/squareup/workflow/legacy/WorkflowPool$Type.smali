.class public final Lcom/squareup/workflow/legacy/WorkflowPool$Type;
.super Ljava/lang/Object;
.source "WorkflowPool.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/WorkflowPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowPool.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Type\n*L\n1#1,422:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\n\u0008\u0001\u0010\u0003 \u0000*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u00020\u0002B/\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0006\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u0006\u00a2\u0006\u0002\u0010\tJ\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0002H\u0096\u0002J\u0008\u0010\n\u001a\u00020\u000bH\u0016J$\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00132\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u0015H\u0001R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000c\u001a\u0010\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u00060\rX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000e\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "S",
        "",
        "E",
        "O",
        "stateType",
        "Lkotlin/reflect/KClass;",
        "eventType",
        "outputType",
        "(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V",
        "hashCode",
        "",
        "types",
        "",
        "[Lkotlin/reflect/KClass;",
        "equals",
        "",
        "other",
        "makeWorkflowId",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Id;",
        "name",
        "",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hashCode:I

.field private final types:[Lkotlin/reflect/KClass;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KClass<",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/KClass<",
            "TS;>;",
            "Lkotlin/reflect/KClass<",
            "TE;>;",
            "Lkotlin/reflect/KClass<",
            "TO;>;)V"
        }
    .end annotation

    const-string v0, "stateType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/KClass;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const/4 p1, 0x2

    aput-object p3, v0, p1

    .line 55
    iput-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->types:[Lkotlin/reflect/KClass;

    .line 58
    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->types:[Lkotlin/reflect/KClass;

    invoke-static {p1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result p1

    iput p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->hashCode:I

    return-void
.end method

.method public static synthetic makeWorkflowId$default(Lcom/squareup/workflow/legacy/WorkflowPool$Type;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, ""

    .line 69
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 73
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 74
    :cond_0
    instance-of v0, p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->types:[Lkotlin/reflect/KClass;

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->types:[Lkotlin/reflect/KClass;

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 71
    iget v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->hashCode:I

    return v0
.end method

.method public final makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    invoke-direct {v0, p1, p0}, Lcom/squareup/workflow/legacy/WorkflowPool$Id;-><init>(Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    return-object v0
.end method
