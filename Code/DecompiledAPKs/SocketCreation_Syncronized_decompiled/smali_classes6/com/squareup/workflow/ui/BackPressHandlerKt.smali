.class public final Lcom/squareup/workflow/ui/BackPressHandlerKt;
.super Ljava/lang/Object;
.source "BackPressHandler.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackPressHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackPressHandler.kt\ncom/squareup/workflow/ui/BackPressHandlerKt\n*L\n1#1,83:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u000f\u0010\u000e\u001a\u0004\u0018\u00010\u000f*\u00020\u0010H\u0086\u0010\"D\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0001j\u0004\u0018\u0001`\u0003*\u00020\u00052\u0014\u0010\u0000\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0001j\u0004\u0018\u0001`\u00038F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\"\u001a\u0010\n\u001a\u0004\u0018\u00010\u000b*\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r*\u0016\u0010\u0011\"\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a8\u0006\u0012"
    }
    d2 = {
        "value",
        "Lkotlin/Function0;",
        "",
        "Lcom/squareup/workflow/ui/BackPressHandler;",
        "backPressedHandler",
        "Landroid/view/View;",
        "getBackPressedHandler",
        "(Landroid/view/View;)Lkotlin/jvm/functions/Function0;",
        "setBackPressedHandler",
        "(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V",
        "handlerWrapperOrNull",
        "Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;",
        "getHandlerWrapperOrNull",
        "(Landroid/view/View;)Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;",
        "onBackPressedDispatcherOwnerOrNull",
        "Landroidx/activity/OnBackPressedDispatcherOwner;",
        "Landroid/content/Context;",
        "BackPressHandler",
        "core-android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getBackPressedHandler(Landroid/view/View;)Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$backPressedHandler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p0}, Lcom/squareup/workflow/ui/BackPressHandlerKt;->getHandlerWrapperOrNull(Landroid/view/View;)Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->getHandler()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private static final getHandlerWrapperOrNull(Landroid/view/View;)Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;
    .locals 1

    .line 35
    sget v0, Lcom/squareup/workflow/ui/R$id;->view_back_handler:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;

    return-object p0
.end method

.method public static final onBackPressedDispatcherOwnerOrNull(Landroid/content/Context;)Landroidx/activity/OnBackPressedDispatcherOwner;
    .locals 2

    :goto_0
    const-string v0, "$this$onBackPressedDispatcherOwnerOrNull"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    instance-of v0, p0, Landroidx/activity/OnBackPressedDispatcherOwner;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Landroidx/activity/OnBackPressedDispatcherOwner;

    goto :goto_1

    .line 81
    :cond_0
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-nez v0, :cond_1

    move-object p0, v1

    :cond_1
    check-cast p0, Landroid/content/ContextWrapper;

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    if-eqz p0, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    return-object v1
.end method

.method public static final setBackPressedHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$backPressedHandler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-static {p0}, Lcom/squareup/workflow/ui/BackPressHandlerKt;->getHandlerWrapperOrNull(Landroid/view/View;)Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->stop()V

    :cond_0
    if-eqz p1, :cond_1

    .line 29
    new-instance v0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;-><init>(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->start()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 31
    :goto_0
    sget p1, Lcom/squareup/workflow/ui/R$id;->view_back_handler:I

    invoke-virtual {p0, p1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method
