.class public final Lcom/squareup/workflow/ui/WorkflowRunner$Config;
.super Ljava/lang/Object;
.source "WorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/WorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000*\u0004\u0008\u0001\u0010\u0001*\u0008\u0008\u0002\u0010\u0002*\u00020\u00032\u00020\u0003B?\u0008\u0016\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00028\u0001\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bB?\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u000c\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\rR\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R#\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
        "PropsT",
        "OutputT",
        "",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "props",
        "dispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V",
        "Lkotlinx/coroutines/flow/Flow;",
        "(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V",
        "getDiagnosticListener",
        "()Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "getDispatcher",
        "()Lkotlinx/coroutines/CoroutineDispatcher;",
        "getProps",
        "()Lkotlinx/coroutines/flow/Flow;",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

.field private final dispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final props:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "TPropsT;>;"
        }
    .end annotation
.end field

.field private final workflow:Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+",
            "Ljava/lang/Object;",
            ">;TPropsT;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            ")V"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->flowOf(Ljava/lang/Object;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;-><init>(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    .line 75
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getMain()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object p3

    invoke-virtual {p3}, Lkotlinx/coroutines/MainCoroutineDispatcher;->getImmediate()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object p3

    check-cast p3, Lkotlinx/coroutines/CoroutineDispatcher;

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    .line 76
    check-cast p4, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;-><init>(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TPropsT;>;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            ")V"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->workflow:Lcom/squareup/workflow/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->props:Lkotlinx/coroutines/flow/Flow;

    iput-object p3, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->dispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    iput-object p4, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-void
.end method


# virtual methods
.method public final getDiagnosticListener()Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-object v0
.end method

.method public final getDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->dispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public final getProps()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "TPropsT;>;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->props:Lkotlinx/coroutines/flow/Flow;

    return-object v0
.end method

.method public final getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->workflow:Lcom/squareup/workflow/Workflow;

    return-object v0
.end method
