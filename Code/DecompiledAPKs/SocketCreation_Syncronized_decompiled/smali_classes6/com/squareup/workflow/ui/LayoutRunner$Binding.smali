.class public final Lcom/squareup/workflow/ui/LayoutRunner$Binding;
.super Ljava/lang/Object;
.source "LayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/ViewBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/LayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Binding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/ViewBinding<",
        "TRenderingT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LayoutRunner.kt\ncom/squareup/workflow/ui/LayoutRunner$Binding\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0001\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B7\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\t\u00a2\u0006\u0002\u0010\u000cJ/\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00028\u00012\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016\u00a2\u0006\u0002\u0010\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/LayoutRunner$Binding;",
        "RenderingT",
        "",
        "Lcom/squareup/workflow/ui/ViewBinding;",
        "type",
        "Lkotlin/reflect/KClass;",
        "layoutId",
        "",
        "runnerConstructor",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "(Lkotlin/reflect/KClass;ILkotlin/jvm/functions/Function1;)V",
        "getType",
        "()Lkotlin/reflect/KClass;",
        "buildView",
        "initialRendering",
        "initialContainerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final layoutId:I

.field private final runnerConstructor:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TRenderingT;>;>;"
        }
    .end annotation
.end field

.field private final type:Lkotlin/reflect/KClass;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/KClass<",
            "TRenderingT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/KClass;ILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/KClass<",
            "TRenderingT;>;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "+",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TRenderingT;>;>;)V"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runnerConstructor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;->type:Lkotlin/reflect/KClass;

    iput p2, p0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;->layoutId:I

    iput-object p3, p0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;->runnerConstructor:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "initialRendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialContainerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    .line 72
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, p3

    :goto_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 73
    invoke-virtual {v0, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p3

    .line 74
    iget v0, p0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;->layoutId:I

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 79
    new-instance p4, Lcom/squareup/workflow/ui/LayoutRunner$Binding$buildView$1$1;

    iget-object v0, p0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;->runnerConstructor:Lkotlin/jvm/functions/Function1;

    const-string v1, "this"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/ui/LayoutRunner;

    invoke-direct {p4, v0}, Lcom/squareup/workflow/ui/LayoutRunner$Binding$buildView$1$1;-><init>(Lcom/squareup/workflow/ui/LayoutRunner;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    .line 76
    invoke-static {p3, p1, p2, p4}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->bindShowRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    const-string p1, "LayoutInflater.from(cont\u2026            )\n          }"

    .line 75
    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p3
.end method

.method public getType()Lkotlin/reflect/KClass;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/KClass<",
            "TRenderingT;>;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;->type:Lkotlin/reflect/KClass;

    return-object v0
.end method
