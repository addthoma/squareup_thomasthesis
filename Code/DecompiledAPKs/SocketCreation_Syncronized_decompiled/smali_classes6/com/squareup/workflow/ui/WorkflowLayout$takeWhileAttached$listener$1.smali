.class public final Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;
.super Ljava/lang/Object;
.source "WorkflowLayout.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/WorkflowLayout;->takeWhileAttached(Landroid/view/View;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016J\u0012\u0010\u000c\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\r"
    }
    d2 = {
        "com/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1",
        "Landroid/view/View$OnAttachStateChangeListener;",
        "sub",
        "Lio/reactivex/disposables/SerialDisposable;",
        "getSub",
        "()Lio/reactivex/disposables/SerialDisposable;",
        "setSub",
        "(Lio/reactivex/disposables/SerialDisposable;)V",
        "onViewAttachedToWindow",
        "",
        "v",
        "Landroid/view/View;",
        "onViewDetachedFromWindow",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $source:Lio/reactivex/Observable;

.field final synthetic $update:Lkotlin/jvm/functions/Function1;

.field private sub:Lio/reactivex/disposables/SerialDisposable;


# direct methods
.method constructor <init>(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->$source:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->$update:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->sub:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method


# virtual methods
.method public final getSub()Lio/reactivex/disposables/SerialDisposable;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->sub:Lio/reactivex/disposables/SerialDisposable;

    return-object v0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2

    .line 143
    iget-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->sub:Lio/reactivex/disposables/SerialDisposable;

    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->$source:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1$onViewAttachedToWindow$1;

    invoke-direct {v1, p0}, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1$onViewAttachedToWindow$1;-><init>(Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/disposables/SerialDisposable;->replace(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .line 147
    iget-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->sub:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {p1}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method

.method public final setSub(Lio/reactivex/disposables/SerialDisposable;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;->sub:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method
