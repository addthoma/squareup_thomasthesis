.class final Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ScreenWorkflowAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/ScreenState<",
        "+TRenderingT;>;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
        "TOutputT;TRenderingT;>;+TOutputT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0002\u0010\u0004*\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;",
        "OutputT",
        "RenderingT",
        "InputT",
        "",
        "it",
        "Lcom/squareup/workflow/ScreenState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$1;->$state:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>;TOutputT;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$1;->$state:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, p1, v3, v2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->copy$default(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;ILjava/lang/Object;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    move-result-object p1

    const/4 v1, 0x2

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/workflow/ScreenState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$1;->invoke(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
