.class public final Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;
.super Ljava/lang/Object;
.source "LegacyWorkflowAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LegacyWorkflowState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        "EventT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0005\u0010\u0001*\u00020\u0002*\u0008\u0008\u0006\u0010\u0003*\u00020\u0002*\u0008\u0008\u0007\u0010\u0004*\u00020\u00022\u00020\u0002B?\u0012\u0018\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u00070\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00050\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00018\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001b\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u00070\u0006H\u00c6\u0003J\u000f\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00028\u00050\u0008H\u00c6\u0003J\u0010\u0010\u0018\u001a\u0004\u0018\u00018\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u0019\u001a\u00020\u000bH\u00c6\u0003Jb\u0010\u001a\u001a\u0014\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u00070\u00002\u001a\u0008\u0002\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u00070\u00062\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00050\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00018\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001\u00a2\u0006\u0002\u0010\u001bJ\u0013\u0010\u001c\u001a\u00020\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u0015\u0010\t\u001a\u0004\u0018\u00018\u0005\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\u0008\r\u0010\u000eR#\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0005\u0012\u0004\u0012\u00028\u0006\u0012\u0004\u0012\u00028\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00050\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;",
        "StateT",
        "",
        "EventT",
        "OutputT",
        "runningWorkflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "stateSubscription",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "lastLegacyState",
        "workflowPool",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)V",
        "getLastLegacyState",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getRunningWorkflow",
        "()Lcom/squareup/workflow/legacy/Workflow;",
        "getStateSubscription",
        "()Lkotlinx/coroutines/channels/ReceiveChannel;",
        "getWorkflowPool",
        "()Lcom/squareup/workflow/legacy/WorkflowPool;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final lastLegacyState:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TStateT;"
        }
    .end annotation
.end field

.field private final runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation
.end field

.field private final stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "TStateT;>;"
        }
    .end annotation
.end field

.field private final workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TStateT;-TEventT;+TOutputT;>;",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TStateT;>;TStateT;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")V"
        }
    .end annotation

    const-string v0, "runningWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stateSubscription"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflowPool"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    iput-object p3, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    iput-object p4, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;ILjava/lang/Object;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->copy(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    return-object v0
.end method

.method public final component2()Lkotlinx/coroutines/channels/ReceiveChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "TStateT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method

.method public final component3()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TStateT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    return-object v0
.end method

.method public final component4()Lcom/squareup/workflow/legacy/WorkflowPool;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TStateT;-TEventT;+TOutputT;>;",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TStateT;>;TStateT;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "runningWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stateSubscription"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflowPool"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    iget-object v1, p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v1, p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object p1, p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLastLegacyState()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TStateT;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    return-object v0
.end method

.method public final getRunningWorkflow()Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    return-object v0
.end method

.method public final getStateSubscription()Lkotlinx/coroutines/channels/ReceiveChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "TStateT;>;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method

.method public final getWorkflowPool()Lcom/squareup/workflow/legacy/WorkflowPool;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LegacyWorkflowState(runningWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->runningWorkflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stateSubscription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->stateSubscription:Lkotlinx/coroutines/channels/ReceiveChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastLegacyState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->lastLegacyState:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", workflowPool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->workflowPool:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
