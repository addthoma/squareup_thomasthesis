.class public abstract Lcom/squareup/x2/NoSquareDeviceRootModule;
.super Ljava/lang/Object;
.source "NoSquareDeviceRootModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/x2/NoX2AppModule;,
        Lcom/squareup/cardreader/squid/common/NoSpeModule;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static noDiagnosticsReporter()Lcom/squareup/payment/ledger/DiagnosticsReporter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    sget-object v0, Lcom/squareup/x2/-$$Lambda$Phrh0aTz_3OwnltUW3tErom8zZg;->INSTANCE:Lcom/squareup/x2/-$$Lambda$Phrh0aTz_3OwnltUW3tErom8zZg;

    return-object v0
.end method


# virtual methods
.method abstract provideUsbPortMapper(Lcom/squareup/usb/UsbPortMapper$NoOpUsbPortMapper;)Lcom/squareup/usb/UsbPortMapper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
