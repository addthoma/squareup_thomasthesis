.class public Lcom/squareup/x2/MaybeX2SellerScreenRunner$NoOpForProd;
.super Ljava/lang/Object;
.source "MaybeX2SellerScreenRunner.java"

# interfaces
.implements Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/x2/MaybeX2SellerScreenRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOpForProd"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public activitySearchCardRead()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public badIsHodorCheck()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public branReady()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 473
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public cancelProcessingDippedCardForCardOnFile()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public cancelSaveCard()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public checkingGiftCardBalance()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public configuringGiftCard(Lcom/squareup/protos/common/Money;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public continueConfirmation(Lcom/squareup/comms/protos/common/PostTransactionAction;)V
    .locals 0

    return-void
.end method

.method public disableContactlessField()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dismissGiftCardActivation()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dismissGiftCardBalanceCheck()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dismissSaveCustomer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public displayCardOnFileAuth(Lcom/squareup/ui/main/RegisterTreeKey;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public displayCustomerDetails(Lcom/squareup/protos/client/rolodex/Contact;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public displayEGiftCardActivation(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public displayGiftCardActivation(Lcom/squareup/protos/common/Money;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public displayGiftCardBalance(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public displayGiftCardBalanceCheck()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enableContactlessField()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ensurePipScreen(Lcom/squareup/x2/ui/tender/PipTenderScreen;)V
    .locals 0

    return-void
.end method

.method public enterCartForRetail()V
    .locals 0

    return-void
.end method

.method public enteringActivitySearch()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enteringCustomersApplet()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enteringOrderTicketName()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enteringSaveCardOnFile()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enteringSaveCustomer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public errorCheckingGiftCardBalance()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public exitingActivitySearch()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public exitingCustomersApplet()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public firstPaymentDialog()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 2

    .line 733
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Should not request firstPaymentDialog from non-X2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getStateForSaveCard()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public goBackInCustomerFlow()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public invoiceCreated()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBranReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCardInsertedOnCartMonitor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isOnPostTransactionMonitor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isOnSignatureMonitor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isOnTipMonitor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public loadingGiftCard()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onBuyerContactEntered()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/x2/customers/CustomerInfoWithState;",
            ">;"
        }
    .end annotation

    .line 589
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onCanDisplayDetailsToBuyer()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 597
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onCardOnFileResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;",
            ">;"
        }
    .end annotation

    .line 641
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOrEditCustomer(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/x2/customers/CustomerInfoWithState$State;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onOrderTicketNameResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/x2/tender/X2OrderTicketNameResult;",
            ">;"
        }
    .end annotation

    .line 567
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onPaymentMethodSelected()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 497
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onPipOrTenderDismissed()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 477
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public pipTenderScope()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 481
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Should not request pipTenderScope from non-X2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public promptForPayment()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public promptForPayment(Lcom/squareup/payment/tender/BaseTender$Builder;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public removeCompletedTender(Lcom/squareup/protos/common/Money;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public saveCustomerCardOnFile(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public saveCustomerContact(Lcom/squareup/protos/client/rolodex/Contact;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public savingCardOnFileCanceled(Z)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public sellerCreatingCustomer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showActivateAccountConfirmation()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showPipScreen(Lcom/squareup/x2/ui/tender/PipTenderScreen;)V
    .locals 0

    return-void
.end method

.method public splitTenderInstrumentOnFile(Lcom/squareup/payment/tender/InstrumentTender$Builder;Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public startOnboarding()V
    .locals 0

    return-void
.end method

.method public startProcessingDippedCardForCardOnFile()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public tenderCash(Lcom/squareup/payment/tender/CashTender$Builder;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public tenderFlowCanceled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public tenderInstrumentOnFile(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public tenderKeyedCard(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public tenderOther(Lcom/squareup/payment/tender/OtherTender$Builder;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public tipMonitorOnSellerSkippingTip()V
    .locals 0

    return-void
.end method
