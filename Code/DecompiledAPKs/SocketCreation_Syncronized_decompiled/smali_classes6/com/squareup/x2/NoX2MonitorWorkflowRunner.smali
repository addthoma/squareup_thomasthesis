.class final Lcom/squareup/x2/NoX2MonitorWorkflowRunner;
.super Ljava/lang/Object;
.source "X2MonitorWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/x2/X2MonitorWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nX2MonitorWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 X2MonitorWorkflowRunner.kt\ncom/squareup/x2/NoX2MonitorWorkflowRunner\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,28:1\n151#2,2:29\n*E\n*S KotlinDebug\n*F\n+ 1 X2MonitorWorkflowRunner.kt\ncom/squareup/x2/NoX2MonitorWorkflowRunner\n*L\n17#1,2:29\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004H\u0096\u0001J\u0015\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u0004H\u0096\u0001J\u0010\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000bH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/x2/NoX2MonitorWorkflowRunner;",
        "Lcom/squareup/x2/X2MonitorWorkflowRunner;",
        "()V",
        "onResult",
        "Lio/reactivex/Observable;",
        "",
        "onUpdateScreens",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "registerServices",
        "scopeBuilder",
        "Lmortar/MortarScope$Builder;",
        "x2-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/x2/NoX2MonitorWorkflowRunner;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/x2/X2MonitorWorkflowRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;

    invoke-direct {v0}, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;-><init>()V

    sput-object v0, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;->INSTANCE:Lcom/squareup/x2/NoX2MonitorWorkflowRunner;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 29
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 30
    const-class v2, Lcom/squareup/x2/X2MonitorWorkflowRunner;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/X2MonitorWorkflowRunner;

    iput-object v0, p0, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;->$$delegate_0:Lcom/squareup/x2/X2MonitorWorkflowRunner;

    return-void
.end method


# virtual methods
.method public onResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;->$$delegate_0:Lcom/squareup/x2/X2MonitorWorkflowRunner;

    invoke-interface {v0}, Lcom/squareup/x2/X2MonitorWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onUpdateScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;->$$delegate_0:Lcom/squareup/x2/X2MonitorWorkflowRunner;

    invoke-interface {v0}, Lcom/squareup/x2/X2MonitorWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public registerServices(Lmortar/MortarScope$Builder;)V
    .locals 1

    const-string v0, "scopeBuilder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
