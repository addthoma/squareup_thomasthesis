.class public Lcom/squareup/x2/customers/CustomerInformationHelper;
.super Ljava/lang/Object;
.source "CustomerInformationHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private toBirthday(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->year(Ljava/lang/Integer;)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    .line 81
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->month(I)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    .line 82
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->day(I)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->build()Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public toCustomerInformation(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/comms/protos/common/CustomerInformation;
    .locals 3

    if-eqz p1, :cond_c

    .line 20
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 24
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 25
    new-instance v0, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;-><init>()V

    .line 27
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 28
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 31
    :cond_1
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 32
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 35
    :cond_2
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 36
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->email(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 39
    :cond_3
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 40
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 43
    :cond_4
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 44
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->company(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 47
    :cond_5
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_a

    .line 50
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 51
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->street_address(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 54
    :cond_6
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 55
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->apartment(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 58
    :cond_7
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 59
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->city(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 62
    :cond_8
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 63
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->state(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 66
    :cond_9
    iget-object v2, v1, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 67
    iget-object v1, v1, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 71
    :cond_a
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_b

    .line 72
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-direct {p0, p1}, Lcom/squareup/x2/customers/CustomerInformationHelper;->toBirthday(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->birthday(Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;)Lcom/squareup/comms/protos/common/CustomerInformation$Builder;

    .line 75
    :cond_b
    invoke-virtual {v0}, Lcom/squareup/comms/protos/common/CustomerInformation$Builder;->build()Lcom/squareup/comms/protos/common/CustomerInformation;

    move-result-object p1

    return-object p1

    :cond_c
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method
