.class public final Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;
.super Ljava/lang/Object;
.source "LegacyLcrAudioModule_ProvideAudioBackendFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioBackendBridgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            ">;"
        }
    .end annotation
.end field

.field private final audioPlayerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderBridgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ">;"
        }
    .end annotation
.end field

.field private final eventDataListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final outputSampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final sampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->sampleRateProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->outputSampleRateProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->audioPlayerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->sessionProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->eventDataListenerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->audioBackendBridgeProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->cardReaderBridgeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;"
        }
    .end annotation

    .line 64
    new-instance v9, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideAudioBackend(Ljavax/inject/Provider;Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioPlayer;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/squarewave/EventDataListener;Lcom/squareup/wavpool/swipe/AudioBackendBridge;Lcom/squareup/wavpool/swipe/CardReaderBridge;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/squarewave/EventDataListener;",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ")",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;"
        }
    .end annotation

    .line 71
    invoke-static/range {p0 .. p7}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule;->provideAudioBackend(Ljavax/inject/Provider;Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioPlayer;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/squarewave/EventDataListener;Lcom/squareup/wavpool/swipe/AudioBackendBridge;Lcom/squareup/wavpool/swipe/CardReaderBridge;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioBackendLegacy;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->sampleRateProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->outputSampleRateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->audioPlayerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/wavpool/swipe/AudioPlayer;

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    iget-object v4, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->sessionProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->eventDataListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/squarewave/EventDataListener;

    iget-object v6, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->audioBackendBridgeProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v7, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->cardReaderBridgeProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/wavpool/swipe/CardReaderBridge;

    invoke-static/range {v0 .. v7}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->provideAudioBackend(Ljavax/inject/Provider;Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioPlayer;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/squarewave/EventDataListener;Lcom/squareup/wavpool/swipe/AudioBackendBridge;Lcom/squareup/wavpool/swipe/CardReaderBridge;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->get()Lcom/squareup/wavpool/swipe/AudioBackendLegacy;

    move-result-object v0

    return-object v0
.end method
