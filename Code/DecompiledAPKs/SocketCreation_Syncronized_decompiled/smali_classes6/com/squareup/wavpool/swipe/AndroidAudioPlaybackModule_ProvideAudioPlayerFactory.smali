.class public final Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;
.super Ljava/lang/Object;
.source "AndroidAudioPlaybackModule_ProvideAudioPlayerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioPlayer;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioBackendNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final audioRunningProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final audioTrackFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final outputSampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->outputSampleRateProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->audioRunningProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->audioTrackFinisherProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->sessionProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->audioBackendNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;"
        }
    .end annotation

    .line 68
    new-instance v9, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideAudioPlayer(Ljava/util/concurrent/ExecutorService;Ljava/lang/Integer;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioPlayer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/lang/Integer;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ")",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;"
        }
    .end annotation

    .line 76
    invoke-static/range {p0 .. p7}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;->provideAudioPlayer(Ljava/util/concurrent/ExecutorService;Ljava/lang/Integer;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioPlayer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/AudioPlayer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioPlayer;
    .locals 9

    .line 57
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->outputSampleRateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->audioRunningProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->audioTrackFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    iget-object v5, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->sessionProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->audioBackendNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-static/range {v1 .. v8}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->provideAudioPlayer(Ljava/util/concurrent/ExecutorService;Ljava/lang/Integer;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioPlayer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->get()Lcom/squareup/wavpool/swipe/AudioPlayer;

    move-result-object v0

    return-object v0
.end method
