.class public final Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;
.super Ljava/lang/Object;
.source "AndroidHeadsetConnectionListener.kt"

# interfaces
.implements Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAndroidHeadsetConnectionListener.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AndroidHeadsetConnectionListener.kt\ncom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener\n*L\n1#1,45:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\"\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0002J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R)\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u00070\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
        "application",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "onHeadsetStateChangedSource",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        "kotlin.jvm.PlatformType",
        "getOnHeadsetStateChangedSource",
        "()Lio/reactivex/Observable;",
        "onHeadsetStateChangedSource$delegate",
        "Lkotlin/Lazy;",
        "log",
        "",
        "action",
        "",
        "state",
        "",
        "microphone",
        "onHeadsetStateChanged",
        "headset_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final onHeadsetStateChangedSource$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->application:Landroid/app/Application;

    .line 15
    new-instance p1, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;

    invoke-direct {p1, p0}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;-><init>(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->onHeadsetStateChangedSource$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getApplication$p(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Landroid/app/Application;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->application:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic access$log(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;Ljava/lang/String;II)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->log(Ljava/lang/String;II)V

    return-void
.end method

.method private final getOnHeadsetStateChangedSource()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->onHeadsetStateChangedSource$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method private final log(Ljava/lang/String;II)V
    .locals 2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeadsetReceiver action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", state="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", microphone="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onHeadsetStateChanged()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->getOnHeadsetStateChangedSource()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onHeadsetStateChangedSource"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
