.class public final Lcom/squareup/wavpool/swipe/AudioEventKt;
.super Ljava/lang/Object;
.source "AudioEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0004\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u001e\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0006\u001a\u001e\u0010\t\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\u0006\u001a\u000e\u0010\r\u001a\u00020\u00012\u0006\u0010\u000e\u001a\u00020\u000f\u001a&\u0010\u0010\u001a\u00020\u00012\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0012\u00a8\u0006\u0016"
    }
    d2 = {
        "audioEvent",
        "Lcom/squareup/wavpool/swipe/AudioEvent;",
        "event",
        "Lcom/squareup/analytics/AudioEventName;",
        "audiotrackNotCreatedEvent",
        "audioTrackRetryCount",
        "",
        "audioTrackState",
        "audioTrackNumSamples",
        "initialAudioStateEvent",
        "initialVolume",
        "initialUnusedStreamVolume",
        "initialRingerType",
        "setVolumeErrorEvent",
        "errorMessage",
        "",
        "setVolumeEvent",
        "systemStreamActualVolume",
        "",
        "systemStreamAttemptedVolume",
        "musicStreamActualVolume",
        "musicStreamAttemptedVolume",
        "dipper_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;
    .locals 16

    const-string v0, "event"

    move-object/from16 v1, p0

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioEvent;

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/analytics/AudioEventName;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xffe

    const/4 v15, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v15}, Lcom/squareup/wavpool/swipe/AudioEvent;-><init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final audiotrackNotCreatedEvent(III)Lcom/squareup/wavpool/swipe/AudioEvent;
    .locals 16

    .line 70
    new-instance v15, Lcom/squareup/wavpool/swipe/AudioEvent;

    .line 71
    sget-object v0, Lcom/squareup/analytics/AudioEventName;->AUDIOTRACK_NOT_CREATED:Lcom/squareup/analytics/AudioEventName;

    invoke-virtual {v0}, Lcom/squareup/analytics/AudioEventName;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 73
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 74
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xf1e

    const/4 v14, 0x0

    move-object v0, v15

    .line 70
    invoke-direct/range {v0 .. v14}, Lcom/squareup/wavpool/swipe/AudioEvent;-><init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v15
.end method

.method public static final initialAudioStateEvent(III)Lcom/squareup/wavpool/swipe/AudioEvent;
    .locals 16

    .line 35
    new-instance v15, Lcom/squareup/wavpool/swipe/AudioEvent;

    .line 36
    sget-object v0, Lcom/squareup/analytics/AudioEventName;->SET_INITIAL_AUDIO_STATE:Lcom/squareup/analytics/AudioEventName;

    invoke-virtual {v0}, Lcom/squareup/analytics/AudioEventName;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 37
    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 38
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 39
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v13, 0x1fe

    const/4 v14, 0x0

    move-object v0, v15

    .line 35
    invoke-direct/range {v0 .. v14}, Lcom/squareup/wavpool/swipe/AudioEvent;-><init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v15
.end method

.method public static final setVolumeErrorEvent(Ljava/lang/String;)Lcom/squareup/wavpool/swipe/AudioEvent;
    .locals 16

    const-string v0, "errorMessage"

    move-object/from16 v10, p0

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioEvent;

    .line 60
    sget-object v1, Lcom/squareup/analytics/AudioEventName;->SET_VOLUME_ERROR:Lcom/squareup/analytics/AudioEventName;

    invoke-virtual {v1}, Lcom/squareup/analytics/AudioEventName;->getEventValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xefe

    const/4 v15, 0x0

    move-object v1, v0

    .line 59
    invoke-direct/range {v1 .. v15}, Lcom/squareup/wavpool/swipe/AudioEvent;-><init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final setVolumeEvent(FFFF)Lcom/squareup/wavpool/swipe/AudioEvent;
    .locals 16

    .line 49
    new-instance v15, Lcom/squareup/wavpool/swipe/AudioEvent;

    .line 50
    sget-object v0, Lcom/squareup/analytics/AudioEventName;->SET_VOLUME:Lcom/squareup/analytics/AudioEventName;

    invoke-virtual {v0}, Lcom/squareup/analytics/AudioEventName;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-static/range {p0 .. p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 52
    invoke-static/range {p1 .. p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 53
    invoke-static/range {p2 .. p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 54
    invoke-static/range {p3 .. p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfe0

    const/4 v14, 0x0

    move-object v0, v15

    .line 49
    invoke-direct/range {v0 .. v14}, Lcom/squareup/wavpool/swipe/AudioEvent;-><init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v15
.end method
