.class public Lcom/squareup/wavpool/swipe/AudioStartAndStopper;
.super Ljava/lang/Object;
.source "AudioStartAndStopper.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;
.implements Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/AudioStartAndStopper$ForceStopRecording;
    }
.end annotation


# static fields
.field private static final NO_FLAGS:I = 0x0

.field private static final STREAM_TYPE:I = 0x3

.field private static final UNUSED_STREAM_TYPE:I = 0x1


# instance fields
.field private final audioManager:Landroid/media/AudioManager;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final headset:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private initialRingerMode:I

.field private initialUnusedStreamVolume:I

.field private initialVolume:I

.field private final isRunning:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrBackend:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

.field private final maxVolume:I

.field private final r6CardReaderAwakener:Lcom/squareup/cardreader/R6CardReaderAwakener;

.field private final recorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;"
        }
    .end annotation
.end field

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Landroid/media/AudioManager;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/loader/LibraryLoader;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/R6CardReaderAwakener;FLcom/squareup/cardreader/CardReaderListeners;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Landroid/media/AudioManager;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            "F",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ")V"
        }
    .end annotation

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 71
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    .line 72
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 73
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->bus:Lcom/squareup/badbus/BadBus;

    .line 74
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->headset:Ljavax/inject/Provider;

    .line 75
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->isRunning:Ljavax/inject/Provider;

    .line 76
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->lcrBackend:Ljavax/inject/Provider;

    .line 77
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    .line 78
    iput-object p9, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->r6CardReaderAwakener:Lcom/squareup/cardreader/R6CardReaderAwakener;

    .line 79
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->recorderProvider:Ljavax/inject/Provider;

    const/4 p1, 0x3

    .line 80
    invoke-virtual {p3, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result p1

    int-to-float p1, p1

    mul-float p1, p1, p10

    float-to-int p1, p1

    iput p1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->maxVolume:I

    .line 81
    iput-object p11, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 82
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->setInitialAudioState()V

    return-void
.end method

.method private forceStopRecording()V
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->recorderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Recorder;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/Recorder;->stop()V

    return-void
.end method

.method private logVolumeEvent(IIII)V
    .locals 5

    .line 258
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    int-to-float v1, p3

    int-to-float v2, p4

    int-to-float v3, p1

    int-to-float v4, p2

    .line 260
    invoke-static {v1, v2, v3, v4}, Lcom/squareup/wavpool/swipe/AudioEventKt;->setVolumeEvent(FFFF)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v1

    .line 259
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 261
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 262
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v1, p2

    const-string p1, "AudioStartAndStopper#initialAudioState(stream %d, %d) (wanted %d)"

    .line 261
    invoke-static {p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v0, v0, [Ljava/lang/Object;

    .line 264
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 265
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v0, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v0, p2

    .line 263
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private maybeSetRingerMode(I)V
    .locals 2

    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 246
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/AudioEventName;->SET_RINGER_ERROR:Lcom/squareup/analytics/AudioEventName;

    .line 247
    invoke-static {v1}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 248
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    const-string v0, "Unable to set ringer mode."

    .line 249
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Unable to set ringer mode, but it\'s probably not a big deal."

    .line 251
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private setInitialAudioState()V
    .locals 7

    .line 88
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget v2, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->maxVolume:I

    const/4 v3, 0x2

    div-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialVolume:I

    .line 89
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialUnusedStreamVolume:I

    .line 90
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    iput v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialRingerMode:I

    .line 92
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget v4, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialVolume:I

    iget v5, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialUnusedStreamVolume:I

    iget v6, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialRingerMode:I

    .line 93
    invoke-static {v4, v5, v6}, Lcom/squareup/wavpool/swipe/AudioEventKt;->initialAudioStateEvent(III)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    new-array v0, v3, [Ljava/lang/Object;

    .line 95
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x0

    aput-object v1, v0, v4

    iget v1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialVolume:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "AudioStartAndStopper: Initial volume for stream %d = %d"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v0, v3, [Ljava/lang/Object;

    .line 96
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    iget v1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialUnusedStreamVolume:I

    .line 97
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "AudioStartAndStopper: Initial volume for unused stream %d = %d"

    .line 96
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private setVolume()V
    .locals 7

    .line 205
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isVolumeFixed()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    const-string v3, "Device Does Not Support Volume Change"

    .line 207
    invoke-static {v3}, Lcom/squareup/wavpool/swipe/AudioEventKt;->setVolumeErrorEvent(Ljava/lang/String;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Device does not support volume change"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    new-array v1, v1, [Ljava/lang/Object;

    .line 209
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Cannot change device volume: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "Volume change not possible"

    .line 210
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->isRunning:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->headset:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 216
    iget v3, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->maxVolume:I

    goto :goto_1

    :cond_2
    iget v3, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialVolume:I

    :goto_1
    if-eqz v0, :cond_3

    const/4 v4, 0x0

    goto :goto_2

    .line 217
    :cond_3
    iget v4, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialUnusedStreamVolume:I

    :goto_2
    if-eqz v0, :cond_4

    const/4 v5, 0x2

    goto :goto_3

    .line 218
    :cond_4
    iget v5, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialRingerMode:I

    .line 223
    :goto_3
    :try_start_0
    iget-object v6, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    iget v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialUnusedStreamVolume:I

    :goto_4
    invoke-virtual {v6, v1, v0, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 225
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    const/4 v6, 0x3

    invoke-virtual {v0, v6, v3, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 226
    invoke-direct {p0, v5}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->maybeSetRingerMode(I)V

    .line 229
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 230
    iget-object v5, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v5, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 231
    invoke-direct {p0, v0, v3, v1, v4}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->logVolumeEvent(IIII)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    .line 233
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v1

    .line 234
    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/wavpool/swipe/AudioEventKt;->setVolumeErrorEvent(Ljava/lang/String;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to set volume :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_5
    return-void
.end method

.method private sync()V
    .locals 3

    .line 170
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->headset:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result v0

    .line 173
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v1}, Lcom/squareup/cardreader/loader/LibraryLoader;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->addLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 179
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->isRunning:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v2, Lcom/squareup/analytics/AudioEventName;->START_RECORDING:Lcom/squareup/analytics/AudioEventName;

    .line 181
    invoke-static {v2}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "AudioStartAndStopper#sync(), connected and running, starting recording"

    .line 182
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->recorderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Recorder;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/Recorder;->start()V

    goto :goto_0

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v2, Lcom/squareup/analytics/AudioEventName;->STOP_RECORDING_NOT_RUNNING:Lcom/squareup/analytics/AudioEventName;

    .line 186
    invoke-static {v2}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "AudioStartAndStopper#sync(), scope is not running, stopping the recorder"

    .line 187
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->recorderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Recorder;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/Recorder;->stop()V

    goto :goto_0

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v2, Lcom/squareup/analytics/AudioEventName;->STOP_RECORDING_NOT_CONNECTED:Lcom/squareup/analytics/AudioEventName;

    .line 192
    invoke-static {v2}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "AudioStartAndStopper#sync(), headset removed, stopping the recorder"

    .line 193
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->recorderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Recorder;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/Recorder;->stop()V

    .line 197
    :goto_0
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->setVolume()V

    return-void
.end method


# virtual methods
.method getInitialVolume()I
    .locals 1

    .line 269
    iget v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->initialVolume:I

    return v0
.end method

.method public synthetic lambda$onStart$0$AudioStartAndStopper(Lcom/squareup/wavpool/swipe/AudioStartAndStopper$ForceStopRecording;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->forceStopRecording()V

    return-void
.end method

.method onHeadsetChanged(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "AudioStartAndStopper#onHeadsetChanged(%s)"

    .line 145
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->headset:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result p1

    if-nez p1, :cond_0

    .line 152
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->sync()V

    return-void
.end method

.method public onLibrariesFailedToLoad(Ljava/lang/String;)V
    .locals 0

    .line 141
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    return-void
.end method

.method public onLibrariesLoaded()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "AudioStartAndStopper#onLibrariesLoaded"

    .line 136
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->sync()V

    return-void
.end method

.method public onStart()V
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/AudioEventName;->FW_UPDATE_ALREADY_AWAKE:Lcom/squareup/analytics/AudioEventName;

    .line 104
    invoke-static {v1}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "AudioStartAndStopper#onStart() - already awake for firmware update."

    .line 105
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 110
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->bus:Lcom/squareup/badbus/BadBus;

    const-class v2, Lcom/squareup/wavpool/swipe/AudioStartAndStopper$ForceStopRecording;

    invoke-virtual {v1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/wavpool/swipe/-$$Lambda$AudioStartAndStopper$1vWSbNDK7EZhHspLzec4Q2edJfk;

    invoke-direct {v2, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$AudioStartAndStopper$1vWSbNDK7EZhHspLzec4Q2edJfk;-><init>(Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)V

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 111
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->bus:Lcom/squareup/badbus/BadBus;

    const-class v2, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {v1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/wavpool/swipe/-$$Lambda$eCrCfuHrgNicPt8g9Wxui86u_1Q;

    invoke-direct {v2, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$eCrCfuHrgNicPt8g9Wxui86u_1Q;-><init>(Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)V

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 112
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->r6CardReaderAwakener:Lcom/squareup/cardreader/R6CardReaderAwakener;

    invoke-virtual {v0}, Lcom/squareup/cardreader/R6CardReaderAwakener;->resume()V

    .line 115
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->headset:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->setInitialAudioState()V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->headset:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->onHeadsetChanged(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->lcrBackend:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/LcrBackend;

    invoke-interface {v0}, Lcom/squareup/cardreader/LcrBackend;->onResume()V

    return-void
.end method

.method public onStop()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/AudioEventName;->FW_UPDATE_STAY_AWAKE:Lcom/squareup/analytics/AudioEventName;

    .line 127
    invoke-static {v1}, Lcom/squareup/wavpool/swipe/AudioEventKt;->audioEvent(Lcom/squareup/analytics/AudioEventName;)Lcom/squareup/wavpool/swipe/AudioEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "AudioStartAndStopper#onStop() - staying awake for firmware update."

    .line 128
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->stop()V

    return-void
.end method

.method public stop()V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->r6CardReaderAwakener:Lcom/squareup/cardreader/R6CardReaderAwakener;

    invoke-virtual {v0}, Lcom/squareup/cardreader/R6CardReaderAwakener;->stop()V

    .line 161
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->recorderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Recorder;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/Recorder;->stop()V

    .line 162
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method
