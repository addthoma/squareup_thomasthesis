.class public final Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;
.super Ljava/lang/Object;
.source "AndroidDeviceParamsModule_ProvideAudioVolumeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private final androidDeviceParamsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;->androidDeviceParamsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAudioVolume(Lcom/squareup/wavpool/swipe/AndroidDeviceParams;)F
    .locals 0

    .line 34
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;->provideAudioVolume(Lcom/squareup/wavpool/swipe/AndroidDeviceParams;)F

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Float;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;->androidDeviceParamsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;->provideAudioVolume(Lcom/squareup/wavpool/swipe/AndroidDeviceParams;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;->get()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
