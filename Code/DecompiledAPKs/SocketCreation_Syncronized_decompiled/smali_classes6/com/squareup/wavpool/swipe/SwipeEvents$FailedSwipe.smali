.class public Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;
.super Lcom/squareup/wavpool/swipe/SwipeEvents;
.source "SwipeEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/SwipeEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FailedSwipe"
.end annotation


# instance fields
.field public final swipeStraight:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/wavpool/swipe/SwipeEvents;-><init>(Lcom/squareup/wavpool/swipe/SwipeEvents$1;)V

    .line 33
    iput-boolean p1, p0, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->swipeStraight:Z

    return-void
.end method

.method public static fromDemodResult(Lcom/squareup/squarewave/decode/DemodResult;)Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    invoke-virtual {p0}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;-><init>(Z)V

    return-object v0
.end method

.method public static fromSwipeStraight(Ljava/lang/Boolean;)Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;-><init>(Z)V

    return-object v0
.end method
