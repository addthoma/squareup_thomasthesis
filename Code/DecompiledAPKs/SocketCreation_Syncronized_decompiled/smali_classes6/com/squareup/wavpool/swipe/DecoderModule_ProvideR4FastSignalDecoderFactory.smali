.class public final Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;
.super Ljava/lang/Object;
.source "DecoderModule_ProvideR4FastSignalDecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/SignalDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final sqLinkSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;->sqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideR4FastSignalDecoder(Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;)Lcom/squareup/squarewave/SignalDecoder;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/DecoderModule;->provideR4FastSignalDecoder(Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;)Lcom/squareup/squarewave/SignalDecoder;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/SignalDecoder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/SignalDecoder;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;->sqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;->provideR4FastSignalDecoder(Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;)Lcom/squareup/squarewave/SignalDecoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;->get()Lcom/squareup/squarewave/SignalDecoder;

    move-result-object v0

    return-object v0
.end method
