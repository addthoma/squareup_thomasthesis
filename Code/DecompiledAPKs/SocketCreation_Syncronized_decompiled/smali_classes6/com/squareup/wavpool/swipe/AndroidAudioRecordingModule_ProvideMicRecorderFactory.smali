.class public final Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;
.super Ljava/lang/Object;
.source "AndroidAudioRecordingModule_ProvideMicRecorderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/MicRecorder;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceParamsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final sampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final telephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->crashnadoProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->sampleRateProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->deviceParamsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->telephonyManagerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->busProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->audioFilterProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->headsetListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;"
        }
    .end annotation

    .line 65
    new-instance v9, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideMicRecorder(Lcom/squareup/crashnado/Crashnado;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AndroidDeviceParams;Lcom/squareup/thread/executor/MainThread;Landroid/telephony/TelephonyManager;Lcom/squareup/badbus/BadEventSink;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/MicRecorder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crashnado/Crashnado;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Landroid/telephony/TelephonyManager;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ")",
            "Lcom/squareup/wavpool/swipe/MicRecorder;"
        }
    .end annotation

    .line 72
    invoke-static/range {p0 .. p7}, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule;->provideMicRecorder(Lcom/squareup/crashnado/Crashnado;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AndroidDeviceParams;Lcom/squareup/thread/executor/MainThread;Landroid/telephony/TelephonyManager;Lcom/squareup/badbus/BadEventSink;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/MicRecorder;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/MicRecorder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/MicRecorder;
    .locals 9

    .line 56
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->crashnadoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/crashnado/Crashnado;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->sampleRateProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->deviceParamsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->telephonyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/badbus/BadEventSink;

    iget-object v7, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->audioFilterProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->headsetListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-static/range {v1 .. v8}, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->provideMicRecorder(Lcom/squareup/crashnado/Crashnado;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AndroidDeviceParams;Lcom/squareup/thread/executor/MainThread;Landroid/telephony/TelephonyManager;Lcom/squareup/badbus/BadEventSink;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/MicRecorder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->get()Lcom/squareup/wavpool/swipe/MicRecorder;

    move-result-object v0

    return-object v0
.end method
