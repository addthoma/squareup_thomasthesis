.class public Lcom/squareup/wavpool/swipe/Headset;
.super Ljava/lang/Object;
.source "Headset.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/Headset$Listener;
    }
.end annotation


# instance fields
.field private volatile currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

.field private final headsetConnectionListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

.field private final headsetListener:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    .line 19
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 24
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/Headset;->headsetConnectionListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    .line 25
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/Headset;->headsetListener:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public currentState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    return-object v0
.end method

.method public destroy()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public initialize()V
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/Headset;->headsetConnectionListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-interface {v1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;->onHeadsetStateChanged()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/wavpool/swipe/-$$Lambda$Headset$4h--GuT40yCqs372dTvfgmG37YM;

    invoke-direct {v2, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$Headset$4h--GuT40yCqs372dTvfgmG37YM;-><init>(Lcom/squareup/wavpool/swipe/Headset;)V

    .line 33
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public synthetic lambda$initialize$0$Headset(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 33
    iget-boolean v0, p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    invoke-virtual {p0, v0, p1}, Lcom/squareup/wavpool/swipe/Headset;->setState(ZZ)V

    return-void
.end method

.method public setState(ZZ)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v0, v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v0, v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    if-eq v0, p2, :cond_1

    .line 46
    :cond_0
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/Headset;->currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    .line 47
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/Headset;->headsetListener:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/wavpool/swipe/Headset$Listener;

    iget-object p2, p0, Lcom/squareup/wavpool/swipe/Headset;->currentState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-interface {p1, p2}, Lcom/squareup/wavpool/swipe/Headset$Listener;->onHeadsetConnectionChanged(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V

    :cond_1
    return-void
.end method
