.class final Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2$1;
.super Ljava/lang/Object;
.source "AndroidHeadsetConnectionListener.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;->invoke()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        "intent",
        "Landroid/content/Intent;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2$1;->this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Landroid/content/Intent;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 4

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v1, "state"

    .line 18
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "microphone"

    .line 19
    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 20
    iget-object v3, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2$1;->this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;

    iget-object v3, v3, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;->this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v1, v2}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->access$log(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;Ljava/lang/String;II)V

    const/4 p1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 24
    :goto_1
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-direct {v0, v1, p1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2$1;->apply(Landroid/content/Intent;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object p1

    return-object p1
.end method
