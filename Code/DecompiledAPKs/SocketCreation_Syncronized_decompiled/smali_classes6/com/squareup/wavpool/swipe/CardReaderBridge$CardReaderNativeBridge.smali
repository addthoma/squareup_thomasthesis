.class public Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;
.super Ljava/lang/Object;
.source "CardReaderBridge.java"

# interfaces
.implements Lcom/squareup/wavpool/swipe/CardReaderBridge;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/CardReaderBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardReaderNativeBridge"
.end annotation


# instance fields
.field private final cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    return-void
.end method


# virtual methods
.method public cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    return-object p1
.end method

.method public cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public process_rpc_callback()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-interface {v0}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->process_rpc_callback()V

    return-void
.end method
