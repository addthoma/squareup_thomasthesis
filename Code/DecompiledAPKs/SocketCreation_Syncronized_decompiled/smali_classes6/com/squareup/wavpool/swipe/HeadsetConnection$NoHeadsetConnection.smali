.class public final Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;
.super Ljava/lang/Object;
.source "HeadsetConnection.kt"

# interfaces
.implements Lcom/squareup/wavpool/swipe/HeadsetConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/HeadsetConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoHeadsetConnection"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
        "()V",
        "isReaderConnected",
        "",
        "headset_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;

    invoke-direct {v0}, Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;-><init>()V

    sput-object v0, Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;->INSTANCE:Lcom/squareup/wavpool/swipe/HeadsetConnection$NoHeadsetConnection;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isReaderConnected()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
