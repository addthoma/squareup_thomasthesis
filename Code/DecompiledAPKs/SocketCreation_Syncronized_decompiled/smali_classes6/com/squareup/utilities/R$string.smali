.class public final Lcom/squareup/utilities/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/utilities/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name_displayed_in_app:I = 0x7f1200d1

.field public static final day_and_date:I = 0x7f1207d8

.field public static final list_pattern_long_final_separator:I = 0x7f120edb

.field public static final list_pattern_long_nonfinal_separator:I = 0x7f120edc

.field public static final list_pattern_long_two_separator:I = 0x7f120edd

.field public static final list_pattern_short:I = 0x7f120ede

.field public static final no_duration:I = 0x7f121082

.field public static final receipt_felica_suica_terminal_id:I = 0x7f1215c4

.field public static final receipt_tender_felica_id:I = 0x7f1215ce

.field public static final receipt_tender_felica_qp:I = 0x7f1215d1

.field public static final receipt_tender_felica_suica:I = 0x7f1215d2

.field public static final send:I = 0x7f1217be

.field public static final short_form_date_time_template:I = 0x7f1217ec

.field public static final subtotal:I = 0x7f1218d7

.field public static final system_permission_contacts_body_invoices:I = 0x7f1218fd

.field public static final system_permission_settings_button:I = 0x7f121909

.field public static final time_ago:I = 0x7f121971

.field public static final time_day:I = 0x7f121972

.field public static final time_days:I = 0x7f121974

.field public static final time_hour:I = 0x7f121976

.field public static final time_hour_long:I = 0x7f121977

.field public static final time_hour_min:I = 0x7f121978

.field public static final time_hours_long:I = 0x7f121979

.field public static final time_just_now:I = 0x7f12197a

.field public static final time_min:I = 0x7f12197b

.field public static final time_mins:I = 0x7f12197c

.field public static final time_sec:I = 0x7f12197d

.field public static final time_week:I = 0x7f121980

.field public static final tip:I = 0x7f1219ae

.field public static final today:I = 0x7f1219e8

.field public static final total:I = 0x7f1219e9

.field public static final yesterday:I = 0x7f121bf1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
