.class public final Lcom/squareup/utilities/ui/RealDevice;
.super Ljava/lang/Object;
.source "RealDevice.kt"

# interfaces
.implements Lcom/squareup/util/Device;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u001d\u0008\u0000\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nR\u0014\u0010\u000b\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/utilities/ui/RealDevice;",
        "Lcom/squareup/util/Device;",
        "context",
        "Landroid/app/Application;",
        "configurationChangeMonitor",
        "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
        "(Landroid/app/Application;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V",
        "screenInfoProvider",
        "Lkotlin/Function0;",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "(Lcom/squareup/util/AndroidConfigurationChangeMonitor;Lkotlin/jvm/functions/Function0;)V",
        "currentScreenSize",
        "getCurrentScreenSize",
        "()Lcom/squareup/util/DeviceScreenSizeInfo;",
        "screenSize",
        "Lio/reactivex/Observable;",
        "getScreenSize",
        "()Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screenInfoProvider:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final screenSize:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configurationChangeMonitor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/utilities/ui/RealDevice$1;

    invoke-direct {v0, p1}, Lcom/squareup/utilities/ui/RealDevice$1;-><init>(Landroid/app/Application;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, p2, v0}, Lcom/squareup/utilities/ui/RealDevice;-><init>(Lcom/squareup/util/AndroidConfigurationChangeMonitor;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/AndroidConfigurationChangeMonitor;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ">;)V"
        }
    .end annotation

    const-string v0, "configurationChangeMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenInfoProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/utilities/ui/RealDevice;->screenInfoProvider:Lkotlin/jvm/functions/Function0;

    .line 24
    invoke-interface {p1}, Lcom/squareup/util/AndroidConfigurationChangeMonitor;->getOnConfigurationMaybeChanged()Lio/reactivex/Observable;

    move-result-object p1

    .line 25
    new-instance p2, Lcom/squareup/utilities/ui/RealDevice$screenSize$1;

    invoke-direct {p2, p0}, Lcom/squareup/utilities/ui/RealDevice$screenSize$1;-><init>(Lcom/squareup/utilities/ui/RealDevice;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "configurationChangeMonit\u2026 { screenInfoProvider() }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance p2, Lcom/squareup/utilities/ui/RealDevice$screenSize$2;

    invoke-direct {p2, p0}, Lcom/squareup/utilities/ui/RealDevice$screenSize$2;-><init>(Lcom/squareup/utilities/ui/RealDevice;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Kt;->startWithLazy(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Observable;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 28
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "configurationChangeMonit\u2026ay(1)\n        .refCount()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/utilities/ui/RealDevice;->screenSize:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getScreenInfoProvider$p(Lcom/squareup/utilities/ui/RealDevice;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/utilities/ui/RealDevice;->screenInfoProvider:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method


# virtual methods
.method public getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 2

    .line 34
    invoke-virtual {p0}, Lcom/squareup/utilities/ui/RealDevice;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/utilities/ui/RealDevice;->screenInfoProvider:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/DeviceScreenSizeInfo;

    return-object v0
.end method

.method public getScreenSize()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/utilities/ui/RealDevice;->screenSize:Lio/reactivex/Observable;

    return-object v0
.end method

.method public isAndroidOWithBadAspectRatio()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isAndroidOWithBadAspectRatio(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isLandscape()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isLandscape(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isLandscapeLongTablet()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isLandscapeLongTablet(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isLongTablet()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isLongTablet(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isMasterDetail()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isMasterDetail(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isPhone()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isPhone(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isPhoneOrPortraitLessThan10Inches()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isPhoneOrPortraitLessThan10Inches(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isPortrait()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isPortrait(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isTablet()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isTablet(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isTabletAtLeast10Inches()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isTabletAtLeast10Inches(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method

.method public isTabletLessThan10Inches()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/util/Device$DefaultImpls;->isTabletLessThan10Inches(Lcom/squareup/util/Device;)Z

    move-result v0

    return v0
.end method
