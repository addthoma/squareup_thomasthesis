.class final synthetic Lcom/squareup/utilities/ui/RealDevice$1;
.super Lkotlin/jvm/internal/PropertyReference0;
.source "RealDevice.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/PropertyReference0;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/utilities/ui/RealDevice$1;->receiver:Ljava/lang/Object;

    check-cast v0, Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    .line 21
    invoke-static {v0}, Lcom/squareup/util/DeviceScreenSizeInfoKt;->getDeviceScreenSizeInfo(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceScreenSizeInfo"

    return-object v0
.end method

.method public getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 2

    const-class v0, Lcom/squareup/util/DeviceScreenSizeInfoKt;

    const-string v1, "impl_release"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "getDeviceScreenSizeInfo(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;"

    return-object v0
.end method
