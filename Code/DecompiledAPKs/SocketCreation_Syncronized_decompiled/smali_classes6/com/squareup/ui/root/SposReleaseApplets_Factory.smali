.class public final Lcom/squareup/ui/root/SposReleaseApplets_Factory;
.super Ljava/lang/Object;
.source "SposReleaseApplets_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/root/SposReleaseApplets;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final balanceAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final customersAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationCenterAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->itemsAppletProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->reportsAppletProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->balanceAppletProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->orderHubAppletProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->activityAppletProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->helpAppletProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->settingsAppletProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->customersAppletProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->invoicesAppletProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p11, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->notificationCenterAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/root/SposReleaseApplets_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;",
            ">;)",
            "Lcom/squareup/ui/root/SposReleaseApplets_Factory;"
        }
    .end annotation

    .line 82
    new-instance v12, Lcom/squareup/ui/root/SposReleaseApplets_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/root/SposReleaseApplets_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryApplet;Lcom/squareup/ui/items/ItemsApplet;Lcom/squareup/reports/applet/ReportsApplet;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/ui/help/HelpApplet;Lcom/squareup/ui/settings/SettingsApplet;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;)Lcom/squareup/ui/root/SposReleaseApplets;
    .locals 13

    .line 90
    new-instance v12, Lcom/squareup/ui/root/SposReleaseApplets;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/root/SposReleaseApplets;-><init>(Lcom/squareup/orderentry/OrderEntryApplet;Lcom/squareup/ui/items/ItemsApplet;Lcom/squareup/reports/applet/ReportsApplet;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/ui/help/HelpApplet;Lcom/squareup/ui/settings/SettingsApplet;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/root/SposReleaseApplets;
    .locals 12

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/orderentry/OrderEntryApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->itemsAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/items/ItemsApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->reportsAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/reports/applet/ReportsApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->balanceAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/balance/BalanceApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->orderHubAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/orderhub/OrderHubApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->activityAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/activity/ActivityApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->helpAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/help/HelpApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->settingsAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/settings/SettingsApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->customersAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/crm/applet/CustomersApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->invoicesAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/invoicesappletapi/InvoicesApplet;

    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->notificationCenterAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryApplet;Lcom/squareup/ui/items/ItemsApplet;Lcom/squareup/reports/applet/ReportsApplet;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/ui/help/HelpApplet;Lcom/squareup/ui/settings/SettingsApplet;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;)Lcom/squareup/ui/root/SposReleaseApplets;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/root/SposReleaseApplets_Factory;->get()Lcom/squareup/ui/root/SposReleaseApplets;

    move-result-object v0

    return-object v0
.end method
