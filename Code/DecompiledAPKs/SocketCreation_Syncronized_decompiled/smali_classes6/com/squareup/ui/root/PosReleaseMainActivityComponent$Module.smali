.class public abstract Lcom/squareup/ui/root/PosReleaseMainActivityComponent$Module;
.super Ljava/lang/Object;
.source "PosReleaseMainActivityComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/main/PosMainActivityComponent$Module;,
        Lcom/squareup/notificationcenterdata/NotificationsReleaseModule;,
        Lcom/squareup/tmn/TmnProdModule;,
        Lcom/squareup/scales/ScaleTrackerModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/root/PosReleaseMainActivityComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
