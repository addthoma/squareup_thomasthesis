.class public final Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;
.super Ljava/lang/Object;
.source "O1ReminderLauncher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
        ">;"
    }
.end annotation


# instance fields
.field private final dayNumLocalSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final trackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->trackerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->dayNumLocalSettingProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;)Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lflow/Flow;",
            ")",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;-><init>(Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->trackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;

    iget-object v1, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->dayNumLocalSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/LocalSetting;

    iget-object v2, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v3, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/Flow;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->newInstance(Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;)Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->get()Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

    move-result-object v0

    return-object v0
.end method
