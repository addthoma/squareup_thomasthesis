.class Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;
.super Ljava/lang/Object;
.source "OpenTicketsSettingsPresenter.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketGroupQueryCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        ">;"
    }
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 62
    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->canceled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ">;)V"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$000(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Lcom/squareup/tickets/TicketGroupsCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/tickets/TicketGroupsCache;->update(Lcom/squareup/shared/catalog/CatalogResult;)V

    .line 70
    iget-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->canceled:Z

    if-nez p1, :cond_0

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$000(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Lcom/squareup/tickets/TicketGroupsCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupEntries()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$102(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Ljava/util/List;)Ljava/util/List;

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$202(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$300(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Ljava/util/List;

    move-result-object p1

    .line 81
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$400(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Ljava/util/List;)V

    .line 85
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$500(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->access$600(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    :cond_2
    return-void
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 65
    iput-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$TicketGroupQueryCallback;->canceled:Z

    return-void
.end method
