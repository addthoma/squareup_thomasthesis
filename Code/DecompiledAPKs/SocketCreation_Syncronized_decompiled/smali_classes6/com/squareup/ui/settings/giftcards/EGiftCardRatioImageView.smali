.class public final Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;
.super Landroid/widget/ImageView;
.source "CropCustomDesignSettingsCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;",
        "Landroid/widget/ImageView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "onMeasure",
        "",
        "widthMeasureSpec",
        "",
        "heightMeasureSpec",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 8

    .line 128
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;->getMeasuredWidth()I

    move-result p1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;->getMeasuredHeight()I

    move-result p2

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/high16 v2, 0x3fe4000000000000L    # 0.625

    if-lez p1, :cond_1

    if-lez p2, :cond_1

    int-to-double v4, p2

    int-to-double v6, p1

    div-double/2addr v4, v6

    cmpl-double v6, v4, v2

    if-lez v6, :cond_2

    goto :goto_0

    :cond_1
    if-lez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    int-to-double v0, p1

    mul-double v0, v0, v2

    double-to-int p2, v0

    goto :goto_1

    :cond_3
    int-to-double v0, p2

    div-double/2addr v0, v2

    double-to-int p1, v0

    .line 147
    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;->setMeasuredDimension(II)V

    return-void
.end method
