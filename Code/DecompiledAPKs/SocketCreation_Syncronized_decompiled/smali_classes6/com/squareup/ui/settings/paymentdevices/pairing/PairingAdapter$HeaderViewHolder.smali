.class Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PairingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HeaderViewHolder"
.end annotation


# instance fields
.field private final content:Landroid/view/View;

.field private final helpView:Landroid/view/View;

.field private final retryingView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 199
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 202
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_screen_content_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->helpView:Landroid/view/View;

    .line 203
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_retrying_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->retryingView:Landroid/view/View;

    .line 204
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_header_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->content:Landroid/view/View;

    return-void
.end method


# virtual methods
.method setContentMinimumHeight(I)V
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->content:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setMinimumHeight(I)V

    return-void
.end method

.method setShowMultipleReaders(Z)V
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->helpView:Landroid/view/View;

    xor-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->retryingView:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
