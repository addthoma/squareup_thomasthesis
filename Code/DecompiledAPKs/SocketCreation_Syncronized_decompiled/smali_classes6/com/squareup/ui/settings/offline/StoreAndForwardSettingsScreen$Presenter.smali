.class public Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "StoreAndForwardSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final flow:Lflow/Flow;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 71
    iput-object p5, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 72
    iput-object p6, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    .line 73
    iput-object p7, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/money/MaxMoneyScrubber;
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->buildMaxAmountScrubber(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/money/MaxMoneyScrubber;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/util/Res;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method private buildMaxAmountScrubber(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/money/MaxMoneyScrubber;
    .locals 5

    .line 154
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMaximum()J

    move-result-wide v0

    .line 155
    new-instance p1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p1, v2, v3, v0}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    return-object p1
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/offline/OfflineSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$0$StoreAndForwardSettingsScreen$Presenter(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 83
    iget-object v1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 84
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v1, 0x1

    .line 107
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method onEnabledToggled(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;Z)V
    .locals 2

    .line 127
    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->saveSettings()V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 131
    invoke-virtual {p1, v0, v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->setStoreAndForwardEnabled(ZZ)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    sget-object v1, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->DISABLED:Lcom/squareup/analytics/StoreAndForwardAnalytics$State;

    invoke-virtual {p2, v1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logSettingState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V

    .line 135
    iget-object p2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object p2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->setEnabled(Ljava/lang/Boolean;)V

    .line 136
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->setTransactionLimitSectionVisible(Z)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 78
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    .line 81
    new-instance v0, Lcom/squareup/ui/settings/offline/-$$Lambda$StoreAndForwardSettingsScreen$Presenter$PdHdwdEm6a_opJC-fRVHSLfzJPc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/offline/-$$Lambda$StoreAndForwardSettingsScreen$Presenter$PdHdwdEm6a_opJC-fRVHSLfzJPc;-><init>(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onUpPressed()Z
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->saveSettings()V

    const/4 v0, 0x0

    return v0
.end method

.method protected saveSettings()V
    .locals 3

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 144
    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->getTransactionLimit()Ljava/lang/CharSequence;

    move-result-object v1

    .line 145
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getSingleTransactionLimit()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/Money;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->setTransactionLimit(Lcom/squareup/protos/common/Money;)V

    :cond_1
    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 113
    const-class v0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;

    return-object v0
.end method
