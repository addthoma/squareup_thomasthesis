.class public Lcom/squareup/ui/settings/taxes/tax/TaxScope$Module;
.super Ljava/lang/Object;
.source "TaxScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideTaxState(Lcom/google/gson/Gson;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/settings/taxes/tax/TaxState;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxState;-><init>(Lcom/google/gson/Gson;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v0
.end method
