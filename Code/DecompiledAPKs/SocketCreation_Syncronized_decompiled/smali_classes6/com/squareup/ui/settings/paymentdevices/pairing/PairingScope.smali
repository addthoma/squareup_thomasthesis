.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "PairingScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Component;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$ParentComponent;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;

    .line 52
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method
