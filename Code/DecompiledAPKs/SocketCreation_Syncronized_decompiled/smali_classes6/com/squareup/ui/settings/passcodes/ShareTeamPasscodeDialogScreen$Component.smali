.class public interface abstract Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;
.super Ljava/lang/Object;
.source "ShareTeamPasscodeDialogScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract flow()Lflow/Flow;
.end method

.method public abstract res()Lcom/squareup/util/Res;
.end method

.method public abstract settingsAppletEntryPoint()Lcom/squareup/ui/settings/SettingsAppletEntryPoint;
.end method
