.class public abstract Lcom/squareup/ui/settings/SettingsAppletScope$Module;
.super Ljava/lang/Object;
.source "SettingsAppletScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCoreSettingsSectionParams(Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/badbus/BadBus;Lcom/squareup/account/LegacyAuthenticator;)Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    invoke-interface {p3}, Lcom/squareup/account/LegacyAuthenticator;->onLoggingOut()Lio/reactivex/Observable;

    move-result-object p3

    invoke-direct {v0, p1, p3, p0, p2}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;-><init>(Lflow/Flow;Lio/reactivex/Observable;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;)V

    return-object v0
.end method

.method static provideDefaultSettingsAppletServices(Lcom/squareup/ui/settings/SettingsAppletServices;)Ljava/util/Set;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/ElementsIntoSet;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsAppletServices;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 129
    invoke-interface {p0}, Lcom/squareup/ui/settings/SettingsAppletServices;->getServices()Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract provideAppletSections(Lcom/squareup/ui/settings/SettingsAppletSectionsList;)Lcom/squareup/applet/AppletSectionsList;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmailCollectionDialogController(Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;)Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmailCollectionSettingsController(Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;)Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideScreenTypeMap()Ljava/util/Map;
    .annotation runtime Ldagger/multibindings/Multibinds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/x2/settings/ScreenType;",
            "Lcom/squareup/coordinators/Coordinator;",
            ">;"
        }
    .end annotation
.end method
