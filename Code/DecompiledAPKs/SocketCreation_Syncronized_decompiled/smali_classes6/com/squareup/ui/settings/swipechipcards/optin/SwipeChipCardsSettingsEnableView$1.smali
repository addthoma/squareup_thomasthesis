.class Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SwipeChipCardsSettingsEnableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView$1;->this$0:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView$1;->this$0:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;

    iget-object p1, p1, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->presenter:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->enableSwipeChipCards()V

    return-void
.end method
