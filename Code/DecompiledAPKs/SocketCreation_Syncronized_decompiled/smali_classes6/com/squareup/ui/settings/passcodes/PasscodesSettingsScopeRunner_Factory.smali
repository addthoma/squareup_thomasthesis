.class public final Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "PasscodesSettingsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodesSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAppletEntryPointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p5, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p6, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->employeesProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p7, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->employeesServiceProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p8, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p9, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p10, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p11, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p12, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p13, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->settingsAppletEntryPointProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p14, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;"
        }
    .end annotation

    .line 102
    new-instance v15, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/permissions/PasscodesSettings;Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/Employees;Lcom/squareup/server/employees/EmployeesService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
    .locals 16

    .line 112
    new-instance v15, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/Employees;Lcom/squareup/server/employees/EmployeesService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/settings/server/Features;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
    .locals 15

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/permissions/PasscodesSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->employeesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/Employees;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->employeesServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/server/employees/EmployeesService;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/accountstatus/AccountStatusProvider;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->settingsAppletEntryPointProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->newInstance(Lcom/squareup/permissions/PasscodesSettings;Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/Employees;Lcom/squareup/server/employees/EmployeesService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner_Factory;->get()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method
