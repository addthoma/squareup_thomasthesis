.class public final Lcom/squareup/ui/settings/instantdeposits/VerifyCardChangeBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "VerifyCardChangeBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/VerifyCardChangeBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "props",
        "Lcom/squareup/debitcard/VerifyCardChangeProps;",
        "(Lcom/squareup/debitcard/VerifyCardChangeProps;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final props:Lcom/squareup/debitcard/VerifyCardChangeProps;


# direct methods
.method public constructor <init>(Lcom/squareup/debitcard/VerifyCardChangeProps;)V
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/VerifyCardChangeBootstrapScreen;->props:Lcom/squareup/debitcard/VerifyCardChangeProps;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;->Companion:Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner$Companion;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/VerifyCardChangeBootstrapScreen;->props:Lcom/squareup/debitcard/VerifyCardChangeProps;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/debitcard/VerifyCardChangeProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 2

    .line 13
    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeScope;

    sget-object v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/VerifyCardChangeScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method
