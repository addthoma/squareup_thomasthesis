.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "EditTicketGroupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;",
        ">;"
    }
.end annotation


# static fields
.field private static final EDIT_STATE_KEY:Ljava/lang/String; = "edit_state"

.field private static final TITLE_CREATE:I

.field private static final TITLE_EDIT:I


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private awaitingCogsCallback:Z

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

.field private final onChangeTicketNamingMethodConfirmed:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

.field private ticketGroupCount:I

.field private ticketGroupId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    sget v0, Lcom/squareup/settingsapplet/R$string;->edit_ticket_group:I

    sput v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->TITLE_EDIT:I

    .line 54
    sget v0, Lcom/squareup/settingsapplet/R$string;->create_ticket_group:I

    sput v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->TITLE_CREATE:I

    return-void
.end method

.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lflow/Flow;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cogs/Cogs;",
            "Lflow/Flow;",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0, p1, p5}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 74
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 75
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->res:Lcom/squareup/util/Res;

    .line 76
    iput-object p4, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 77
    iput-object p6, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    .line 78
    iput-object p7, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 79
    iput-object p8, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onChangeTicketNamingMethodConfirmed:Lrx/Observable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    return-object p0
.end method

.method private deleteTicketGroup()V
    .locals 3

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    new-instance v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$Y5280gF_crqYRqorWIR0XAfSGm4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$Y5280gF_crqYRqorWIR0XAfSGm4;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private getActionbarGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 141
    iget v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupCount:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object v0
.end method

.method private getDefaultTicketTemplateCount()I
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->getDefaultTicketTemplateCount()I

    move-result v0

    return v0
.end method

.method private initEmptyTicketGroup()V
    .locals 3

    .line 338
    new-instance v0, Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TicketGroup$Builder;-><init>()V

    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/TicketGroup$NamingMethod;->AUTOMATIC_NUMBERING:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    .line 339
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method(Lcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    const-string v1, ""

    .line 340
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupCount:I

    .line 341
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    .line 343
    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;-><init>(Lcom/squareup/api/items/TicketGroup;Ljava/util/List;)V

    iput-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getDefaultTicketTemplateCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->setInitialAutomaticTemplateCount(I)V

    return-void
.end method

.method private isNewTicketGroup()Z
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$closeCard$1(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 277
    invoke-virtual {p0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 278
    const-class v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/RegisterTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const-class v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->closeCardAndDialog(Lflow/History;Ljava/lang/Class;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0

    .line 282
    :cond_0
    const-class v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->closeCard(Lflow/History;Ljava/lang/Class;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method private loadTicketGroupFromCogs()V
    .locals 3

    const/4 v0, 0x1

    .line 349
    iput-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->awaitingCogsCallback:Z

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$zHCY1aMmnWWs7YWvIcNuMEfFZBc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$zHCY1aMmnWWs7YWvIcNuMEfFZBc;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    new-instance v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$8pMSK3vF_Jp4uEhdg_C1Y0NQbvE;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$8pMSK3vF_Jp4uEhdg_C1Y0NQbvE;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private removeEmptyTicketTemplate()V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->removeEmptyTicketTemplateState()V

    return-void
.end method

.method private restoreTicketGroup(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "edit_state"

    .line 334
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    return-void
.end method

.method private saveChanges()V
    .locals 3

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$gZiQEbCmMQw6iHetcOCA5nmzFWo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$gZiQEbCmMQw6iHetcOCA5nmzFWo;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    new-instance v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$vyPkNLk4D_mFh2LsmQDCGvwBhJU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$vyPkNLk4D_mFh2LsmQDCGvwBhJU;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private updateActionBarPrimaryButton()V
    .locals 2

    .line 314
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method private updateAutomaticTicketCount()V
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getAutomaticTemplateCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setAutomaticTemplateCount(I)V

    :cond_0
    return-void
.end method

.method private updateDraggableItems()V
    .locals 2

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setDraggableItems(Ljava/util/List;)V

    goto :goto_0

    .line 325
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getTicketTemplateStates()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setDraggableItems(Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method private updateName()V
    .locals 2

    .line 310
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private updateTicketNameSections()V
    .locals 2

    .line 318
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setNamingMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V

    return-void
.end method

.method private updateView()V
    .locals 2

    .line 295
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateName()V

    .line 296
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateAutomaticTicketCount()V

    .line 297
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateTicketNameSections()V

    .line 298
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateDraggableItems()V

    .line 299
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateActionBarPrimaryButton()V

    .line 300
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isNewTicketGroup()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setDeleteButtonVisible(Z)V

    return-void
.end method


# virtual methods
.method addEmptyTicketTemplate()V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->addEmptyTicketTemplateState()V

    return-void
.end method

.method public buildActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 3

    .line 126
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 127
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$QfU1nfvXXpaXIl1sJOEDFcTz0aM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$QfU1nfvXXpaXIl1sJOEDFcTz0aM;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    .line 128
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getActionbarGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getActionbarText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$IMuSyzWNuWthcnKaylo5rKI-BY4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$IMuSyzWNuWthcnKaylo5rKI-BY4;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    .line 132
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public closeCard()V
    .locals 3

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    sget-object v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$ugIeov3t_tNmih10GbEYLKKy1Po;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$ugIeov3t_tNmih10GbEYLKKy1Po;

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isNewTicketGroup()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->TITLE_CREATE:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->TITLE_EDIT:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getAutomaticTemplateCount()I
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getAutomaticTemplateCount()I

    move-result v0

    return v0
.end method

.method getCurrentGroupName()Ljava/lang/String;
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getMaxTicketGroupSize()I
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketTemplatesPerGroup()I

    move-result v0

    return v0
.end method

.method getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v0

    return-object v0
.end method

.method getTicketGroupSizeLimitReachedMessage()Ljava/lang/String;
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_ticket_template_limit_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 242
    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketTemplatesPerGroup()I

    move-result v1

    const-string v2, "limit"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method isLastTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isLastTicketTemplate(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result p1

    return p1
.end method

.method isTicketGroupSizeLimitReached()Z
    .locals 2

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getTicketTemplateCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 237
    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketTemplatesPerGroup()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$deleteTicketGroup$4$EditTicketGroupPresenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method public synthetic lambda$loadTicketGroupFromCogs$2$EditTicketGroupPresenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;
    .locals 3

    .line 352
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupId:Ljava/lang/String;

    .line 353
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 358
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupId:Ljava/lang/String;

    .line 359
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findTicketTemplates(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 360
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 361
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 362
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->object()Lcom/squareup/wire/Message;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 364
    :cond_1
    new-instance p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    invoke-direct {p1, v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;-><init>(Lcom/squareup/api/items/TicketGroup;Ljava/util/List;)V

    return-object p1
.end method

.method public synthetic lambda$loadTicketGroupFromCogs$3$EditTicketGroupPresenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2

    .line 367
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;

    if-nez p1, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 371
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->closeCard()V

    goto :goto_0

    .line 374
    :cond_0
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    iget-object v1, p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;->ticketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object p1, p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;->ticketTempaltes:Ljava/util/List;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;-><init>(Lcom/squareup/api/items/TicketGroup;Ljava/util/List;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    .line 375
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 376
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setContentVisible(Z)V

    .line 377
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateView()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 380
    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->awaitingCogsCallback:Z

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$EditTicketGroupPresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 89
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->saveChanges()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$saveChanges$5$EditTicketGroupPresenter(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Boolean;
    .locals 3

    .line 423
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isNewTicketGroup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 426
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 427
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    .line 428
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketGroupCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 p1, 0x0

    .line 430
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 434
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 435
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 438
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isNewTicketGroup()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasTicketGroupChanged()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 439
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->buildTicketGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->create(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->appendTemplateChanges(Ljava/util/List;Ljava/util/List;)V

    .line 443
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p1, 0x1

    .line 444
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$saveChanges$6$EditTicketGroupPresenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 4

    .line 446
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 447
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {p1}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object p1

    invoke-static {}, Lcom/squareup/shared/catalog/CatalogResults;->empty()Lcom/squareup/shared/catalog/CatalogResult;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    .line 448
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getTicketGroupId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/opentickets/PredefinedTickets;->updateAvailableTemplateCountForGroup(Ljava/lang/String;)V

    .line 451
    iget p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupCount:I

    if-nez p1, :cond_0

    .line 452
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/squareup/tickets/OpenTicketsSettings;->setPredefinedTicketsEnabled(Z)V

    .line 454
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/log/tickets/TicketGroupCreated;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    .line 455
    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    .line 456
    invoke-virtual {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getTicketTemplateCount()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/log/tickets/TicketGroupCreated;-><init>(Ljava/lang/String;ZI)V

    .line 454
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 457
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->closeCard()V

    goto :goto_0

    .line 459
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->GROUP_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method onAutomaticTicketCountChanged(I)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->setAutomaticTemplateCount(I)V

    .line 196
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateAutomaticTicketCount()V

    return-void
.end method

.method onDeleteTicketGroup()V
    .locals 2

    .line 209
    iget v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/tickets/OpenTicketsSettings;->setPredefinedTicketsEnabled(Z)V

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->deleteTicketGroup()V

    .line 213
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->closeCard()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 83
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 84
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->screen:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->screen:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupId:Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->screen:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    iget v0, v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupCount:I

    iput v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->ticketGroupCount:I

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onChangeTicketNamingMethodConfirmed:Lrx/Observable;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$nqTUTzF-xSURSFCrCqaLMxjYeJs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupPresenter$nqTUTzF-xSURSFCrCqaLMxjYeJs;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    .line 88
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 87
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method onLastTemplateNameChanged(Lcom/squareup/api/items/TicketTemplate$Builder;I)V
    .locals 2

    .line 184
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isLastTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->addEmptyTicketTemplate()V

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->notifyItemInserted(I)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 188
    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 189
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->removeEmptyTicketTemplate()V

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->notifyItemRemoved(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 96
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    if-nez v0, :cond_3

    if-eqz p1, :cond_0

    const-string v0, "edit_state"

    .line 98
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->restoreTicketGroup(Landroid/os/Bundle;)V

    goto :goto_0

    .line 100
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isNewTicketGroup()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->initEmptyTicketGroup()V

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->setContentVisible(Z)V

    .line 105
    iget-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->awaitingCogsCallback:Z

    if-nez p1, :cond_2

    .line 106
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->loadTicketGroupFromCogs()V

    :cond_2
    return-void

    .line 111
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateView()V

    return-void
.end method

.method onNameChanged(Ljava/lang/String;)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->setName(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateActionBarPrimaryButton()V

    return-void
.end method

.method onNamingMethodChanged(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->setNamingMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateDraggableItems()V

    .line 152
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateTicketNameSections()V

    return-void
.end method

.method public onSave()V
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getTicketTemplateCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 218
    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketTemplatesPerGroup()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->TEMPLATE_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 223
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isNewTicketGroup()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isConvertingFromCustomToAutomaticTemplates()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 228
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->saveChanges()V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 115
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onSave(Landroid/os/Bundle;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    if-eqz v0, :cond_0

    const-string v1, "edit_state"

    .line 117
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method onTemplateLostFocus(Lcom/squareup/api/items/TicketTemplate$Builder;I)V
    .locals 1

    .line 167
    iget-object v0, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    .line 168
    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isNewTicketTemplate(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isLastTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->removeTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->notifyItemRemoved(I)V

    :cond_0
    return-void
.end method

.method onTicketTemplateDeleted(Lcom/squareup/api/items/TicketTemplate$Builder;I)V
    .locals 2

    .line 156
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isTicketGroupSizeLimitReached()Z

    move-result v0

    .line 157
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->removeTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result p1

    if-eqz p1, :cond_1

    if-eqz v0, :cond_0

    .line 159
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->updateDraggableItems()V

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->notifyItemRemoved(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onUpClicked()V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasTicketGroupChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->editState:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasTicketTemplatesChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    goto :goto_1

    .line 201
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 122
    const-class v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    return-object v0
.end method
