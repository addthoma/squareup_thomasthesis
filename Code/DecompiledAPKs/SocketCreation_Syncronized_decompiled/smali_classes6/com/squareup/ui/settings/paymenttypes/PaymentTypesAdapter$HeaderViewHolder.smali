.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HeaderViewHolder"
.end annotation


# instance fields
.field private textView:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 374
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 375
    sget v0, Lcom/squareup/settingsapplet/R$id;->payment_types_settings_header_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;->textView:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;)Lcom/squareup/marketfont/MarketTextView;
    .locals 0

    .line 369
    iget-object p0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;->textView:Lcom/squareup/marketfont/MarketTextView;

    return-object p0
.end method
