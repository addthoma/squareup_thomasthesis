.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;
.super Ljava/lang/Object;
.source "PrinterStationScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final autoNumberingInEditRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final builderState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

.field private final flow:Lflow/Flow;

.field private scopeKey:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->builderState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->flow:Lflow/Flow;

    .line 30
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->autoNumberingInEditRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method


# virtual methods
.method public confirmNameOption(Lcom/squareup/register/widgets/ConfirmationStrings;)V
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->scopeKey:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;Lcom/squareup/register/widgets/Confirmation;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public getAutoNumberingInEditRelay()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->autoNumberingInEditRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goToHardwarePrinterSelect()V
    .locals 3

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->scopeKey:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onDialogResponse(Ljava/lang/Boolean;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->autoNumberingInEditRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 34
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->builderState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 35
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->scopeKey:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public showWarning(Lcom/squareup/widgets/warning/WarningStrings;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
