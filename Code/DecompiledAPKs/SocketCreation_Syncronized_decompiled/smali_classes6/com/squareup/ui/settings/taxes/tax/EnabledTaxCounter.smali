.class public Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;
.super Ljava/lang/Object;
.source "EnabledTaxCounter.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private count:I


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->badBus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method public static synthetic lambda$xGE_YUbCJvPcyv_RyGhzwV95k9w(Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;Lcom/squareup/settings/server/FeesUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->onFeesUpdate(Lcom/squareup/settings/server/FeesUpdate;)V

    return-void
.end method

.method private onFeesUpdate(Lcom/squareup/settings/server/FeesUpdate;)V
    .locals 1

    const/4 v0, 0x0

    .line 32
    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->count:I

    .line 33
    iget-object p1, p1, Lcom/squareup/settings/server/FeesUpdate;->taxes:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 34
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->count:I

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->count:I

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/settings/server/FeesUpdate;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$EnabledTaxCounter$xGE_YUbCJvPcyv_RyGhzwV95k9w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$EnabledTaxCounter$xGE_YUbCJvPcyv_RyGhzwV95k9w;-><init>(Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
