.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;
.super Ljava/lang/Object;
.source "GiftCardsSettingsHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsHelper.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,91:1\n1360#2:92\n1429#2,3:93\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardsSettingsHelper.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper\n*L\n47#1:92\n47#1,3:93\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013J.\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u00182\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0018H\u0002R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\r\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;",
        "",
        "unitToken",
        "",
        "accountSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;)V",
        "maxMaxLoadAmount",
        "Lcom/squareup/protos/common/Money;",
        "getMaxMaxLoadAmount",
        "()Lcom/squareup/protos/common/Money;",
        "minMinLoadAmount",
        "getMinMinLoadAmount",
        "enableSellEGiftCardInPos",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "state",
        "enabled",
        "",
        "ensureDefaultsForSellInPos",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "currentConfig",
        "all_egift_themes",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "denomination_amounts",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

.field private final minMinLoadAmount:Lcom/squareup/protos/common/Money;

.field private final unitToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "unitToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->unitToken:Ljava/lang/String;

    .line 24
    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object p1

    const-string v0, "accountSettings.giftCardSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMinimum()J

    move-result-wide v1

    invoke-static {v1, v2, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->minMinLoadAmount:Lcom/squareup/protos/common/Money;

    .line 26
    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMaximum()J

    move-result-wide p1

    invoke-static {p1, p2, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method private final ensureDefaultsForSellInPos(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->newBuilder()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;-><init>()V

    :goto_0
    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_in_store(Ljava/lang/Boolean;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    .line 37
    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_online:Ljava/lang/Boolean;

    const/4 v2, 0x1

    if-nez v1, :cond_1

    .line 39
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_online(Ljava/lang/Boolean;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    .line 42
    :cond_1
    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->host_unit_token:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    if-eqz v0, :cond_4

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->unitToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->host_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    .line 46
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens:Ljava/util/List;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    :cond_5
    if-eqz v2, :cond_7

    .line 47
    check-cast p2, Ljava/lang/Iterable;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 93
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 94
    check-cast v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    .line 47
    iget-object v1, v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;->read_only_token:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 95
    :cond_6
    check-cast v0, Ljava/util/List;

    .line 47
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    .line 50
    :cond_7
    iget-object p2, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->denomination_amounts:Ljava/util/List;

    if-eqz p2, :cond_8

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x4

    if-eq p2, v0, :cond_9

    .line 52
    :cond_8
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->denomination_amounts(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    .line 55
    :cond_9
    iget-object p2, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->min_load_amount:Lcom/squareup/protos/common/Money;

    if-nez p2, :cond_a

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->minMinLoadAmount:Lcom/squareup/protos/common/Money;

    iput-object p2, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->min_load_amount:Lcom/squareup/protos/common/Money;

    .line 59
    :cond_a
    iget-object p2, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->max_load_amount:Lcom/squareup/protos/common/Money;

    if-nez p2, :cond_b

    .line 60
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

    iput-object p2, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->max_load_amount:Lcom/squareup/protos/common/Money;

    .line 63
    :cond_b
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->build()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object p1

    const-string p2, "newConfig.build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final enableSellEGiftCardInPos(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Z)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 9

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 83
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->newBuilder()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object v0

    xor-int/lit8 p2, p2, 0x1

    .line 84
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_in_store(Ljava/lang/Boolean;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object p2

    .line 85
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->build()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x13

    const/4 v8, 0x0

    move-object v1, p1

    .line 82
    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->copy$default(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZILjava/lang/Object;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 77
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getAll_egift_themes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getDenomination_amounts()Ljava/util/List;

    move-result-object v3

    .line 76
    invoke-direct {p0, p2, v0, v3}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->ensureDefaultsForSellInPos(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x13

    const/4 v7, 0x0

    move-object v0, p1

    .line 75
    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->copy$default(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZILjava/lang/Object;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public final getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMinMinLoadAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->minMinLoadAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
