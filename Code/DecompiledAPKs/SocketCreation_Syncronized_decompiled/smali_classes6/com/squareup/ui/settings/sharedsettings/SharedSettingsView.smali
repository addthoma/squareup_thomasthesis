.class public Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;
.super Landroid/widget/LinearLayout;
.source "SharedSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private sharedSettingsMessage:Lcom/squareup/noho/NohoMessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 51
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 31
    sget v0, Lcom/squareup/settingsapplet/R$id;->shared_setting_message_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->sharedSettingsMessage:Lcom/squareup/noho/NohoMessageView;

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->sharedSettingsMessage:Lcom/squareup/noho/NohoMessageView;

    sget v1, Lcom/squareup/settingsapplet/R$string;->shared_settings_empty_state_dashboard_link_text:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setLinkText(I)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->sharedSettingsMessage:Lcom/squareup/noho/NohoMessageView;

    new-instance v1, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView$1;-><init>(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setLinkTextOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->sharedSettingsMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoMessageView;->showLinkText()V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->presenter:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsView;->presenter:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method
