.class Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$6;
.super Ljava/lang/Object;
.source "LearnMoreReaderView.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->showBluetoothRequiredDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$6;->this$0:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    return-void
.end method

.method public enableBle()V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$6;->this$0:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->presenter:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->enableBleAndGo()V

    return-void
.end method
