.class public final Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;
.super Ljava/lang/Object;
.source "PrinterStationScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final builderStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;->builderStateProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/Object;Lflow/Flow;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    check-cast p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;->builderStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Flow;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;->newInstance(Ljava/lang/Object;Lflow/Flow;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner_Factory;->get()Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    move-result-object v0

    return-object v0
.end method
