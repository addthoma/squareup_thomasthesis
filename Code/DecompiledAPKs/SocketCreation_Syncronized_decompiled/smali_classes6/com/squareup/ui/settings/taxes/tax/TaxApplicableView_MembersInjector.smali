.class public final Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;
.super Ljava/lang/Object;
.source "TaxApplicableView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->injectAnalytics(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V

    return-void
.end method
