.class public Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;
.super Ljava/lang/Object;
.source "TileAppearanceAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$TileAppearanceToggleAction;,
        Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$ImageTileDialogCanceled;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public confirmationDialogCanceled()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$ImageTileDialogCanceled;

    invoke-direct {v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$ImageTileDialogCanceled;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public imageTileSelected()V
    .locals 3

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$TileAppearanceToggleAction;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$TileAppearanceToggleAction;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public textTileSelected()V
    .locals 3

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$TileAppearanceToggleAction;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$TileAppearanceToggleAction;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
