.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;
.super Ljava/lang/Object;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Header"
.end annotation


# instance fields
.field nextCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field prevCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
    .locals 0

    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->title:Ljava/lang/String;

    .line 410
    iput-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->prevCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 411
    iput-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->nextCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V
    .locals 0

    .line 403
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    return-void
.end method
