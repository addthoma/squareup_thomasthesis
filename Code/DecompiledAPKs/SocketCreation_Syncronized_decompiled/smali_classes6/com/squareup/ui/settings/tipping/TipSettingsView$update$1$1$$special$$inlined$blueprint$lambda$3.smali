.class final Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$3;
.super Lkotlin/jvm/internal/Lambda;
.source "TipSettingsView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;->invoke(Lcom/squareup/mosaic/components/VerticalScrollUiModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$1$2$2$2$1",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$marinRow$lambda$3",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$section$lambda$3",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$vertical$lambda$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_blueprint$inlined:Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

.field final synthetic this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/mosaic/BlueprintUiModel;Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$3;->$this_blueprint$inlined:Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$3;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$3;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;

    iget-object v0, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;

    iget-object v0, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;->$rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getOnCustomTipClicked()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
