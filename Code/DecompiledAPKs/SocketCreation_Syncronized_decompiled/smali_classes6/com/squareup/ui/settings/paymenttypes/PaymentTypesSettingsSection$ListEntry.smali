.class public Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "PaymentTypesSettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

.field private final tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 6

    .line 54
    sget v3, Lcom/squareup/settingsapplet/R$string;->payment_types_title:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    .line 56
    iput-object p5, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 57
    iput-object p6, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    return-void
.end method

.method private getCount()I
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 72
    invoke-interface {v1}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    .line 73
    invoke-interface {v2}, Lcom/squareup/tenderpayment/InvoiceTenderSetting;->canUseInvoiceAsTender()Z

    move-result v2

    .line 71
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderSettings(ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    .line 75
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 76
    iget-object v0, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method


# virtual methods
.method public getShortValueText()Ljava/lang/String;
    .locals 1

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->payment_types_active:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;->getCount()I

    move-result v1

    const-string v2, "number"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
