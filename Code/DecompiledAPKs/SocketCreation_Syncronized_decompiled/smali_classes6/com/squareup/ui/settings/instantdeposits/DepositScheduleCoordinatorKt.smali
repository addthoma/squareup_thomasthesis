.class public final Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinatorKt;
.super Ljava/lang/Object;
.source "DepositScheduleCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "ARRIVAL_DAY_TIME_FORMAT",
        "",
        "CLOSE_OF_DAY_FORMAT",
        "FIVE_AM_NEXT_DAY",
        "",
        "FRIDAY",
        "PROCESSING_TIME_MINS",
        "SATURDAY",
        "SIX_PM_CURRENT_DAY",
        "settings-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ARRIVAL_DAY_TIME_FORMAT:Ljava/lang/String; = "EEEE, hh:mm a"

.field private static final CLOSE_OF_DAY_FORMAT:Ljava/lang/String; = "EEEE, ha z"

.field private static final FIVE_AM_NEXT_DAY:I = 0x5

.field public static final FRIDAY:I = 0x4

.field private static final PROCESSING_TIME_MINS:I = 0xf

.field private static final SATURDAY:I = 0x5

.field private static final SIX_PM_CURRENT_DAY:I = 0x12
