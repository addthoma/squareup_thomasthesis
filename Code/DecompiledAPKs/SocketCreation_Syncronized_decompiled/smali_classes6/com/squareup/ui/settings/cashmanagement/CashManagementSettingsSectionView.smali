.class public Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;
.super Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;
.source "CashManagementSettingsSectionView.java"


# instance fields
.field presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Component;->inject(Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;)V

    return-void
.end method


# virtual methods
.method public enableAutoEmailToggled(Z)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->enableAutoEmailToggled(Z)V

    return-void
.end method

.method public enableCashManagementToggled(Z)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->enableCashManagementToggled(Z)V

    return-void
.end method

.method protected getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method protected getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method protected getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 37
    invoke-super {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->onBackPressed()Z

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 43
    invoke-super {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 24
    invoke-super {p0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->onFinishInflate()V

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;->presenter:Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
