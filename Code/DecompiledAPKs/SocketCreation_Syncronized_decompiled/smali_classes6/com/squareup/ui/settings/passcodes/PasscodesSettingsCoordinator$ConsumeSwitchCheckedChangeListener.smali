.class abstract Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;
.super Ljava/lang/Object;
.source "PasscodesSettingsCoordinator.java"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "ConsumeSwitchCheckedChangeListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 269
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;->invoke(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public invoke(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 1

    const/4 v0, 0x0

    .line 273
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 274
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 275
    invoke-virtual {p1, p0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 277
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
