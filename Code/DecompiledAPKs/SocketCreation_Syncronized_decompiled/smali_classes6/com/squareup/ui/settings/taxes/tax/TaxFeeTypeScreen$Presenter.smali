.class Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "TaxFeeTypeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;",
        ">;"
    }
.end annotation


# instance fields
.field private availableTypes:Lcom/squareup/server/account/FeeTypes;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private static updateTaxName(Lcom/squareup/server/account/protos/FeeType;Lcom/squareup/server/account/protos/FeeType;Lcom/squareup/shared/catalog/models/CatalogTax$Builder;)V
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 123
    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    .line 124
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 126
    :cond_1
    iget-object p0, p1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    invoke-virtual {p2, p0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    :cond_2
    return-void
.end method

.method private updateView()V
    .locals 5

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {v1}, Lcom/squareup/server/account/FeeTypes;->getTypes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 115
    iget-object v3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {v3}, Lcom/squareup/server/account/FeeTypes;->getTypes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FeeType;

    iget-object v3, v3, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;

    invoke-virtual {v4, v2, v3}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->setFeeTypeRowChecked(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method feeTypeRowClicked(I)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {v0}, Lcom/squareup/server/account/FeeTypes;->getTypes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/FeeType;

    .line 106
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->setSelectedType(Lcom/squareup/server/account/protos/FeeType;)V

    .line 107
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->updateView()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 56
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->tax_applicable_items:I

    .line 58
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 59
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 57
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getFeeTypes()Lcom/squareup/server/account/FeeTypes;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {v1}, Lcom/squareup/server/account/FeeTypes;->getTypes()Ljava/util/List;

    move-result-object v1

    .line 69
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/FeeType;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;

    iget-object v2, v2, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->createFeeTypeRow(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 75
    invoke-static {v0, v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->updateTaxName(Lcom/squareup/server/account/protos/FeeType;Lcom/squareup/server/account/protos/FeeType;Lcom/squareup/shared/catalog/models/CatalogTax$Builder;)V

    goto :goto_1

    .line 77
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    invoke-virtual {p1}, Lcom/squareup/server/account/FeeTypes;->getDefault()Lcom/squareup/server/account/protos/FeeType;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->setSelectedType(Lcom/squareup/server/account/protos/FeeType;)V

    .line 80
    :goto_1
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->updateView()V

    return-void
.end method

.method setSelectedType(Lcom/squareup/server/account/protos/FeeType;)V
    .locals 4

    if-eqz p1, :cond_2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v1

    .line 90
    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->availableTypes:Lcom/squareup/server/account/FeeTypes;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 92
    :cond_0
    invoke-virtual {v2, v1}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object v1

    :goto_0
    if-eq p1, v1, :cond_1

    .line 96
    iget-object v2, p1, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setFeeTypeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    .line 97
    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setFeeTypeName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    .line 98
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setCalculationPhase(I)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 101
    :cond_1
    invoke-static {v1, p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->updateTaxName(Lcom/squareup/server/account/protos/FeeType;Lcom/squareup/server/account/protos/FeeType;Lcom/squareup/shared/catalog/models/CatalogTax$Builder;)V

    return-void

    .line 85
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "newType"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
