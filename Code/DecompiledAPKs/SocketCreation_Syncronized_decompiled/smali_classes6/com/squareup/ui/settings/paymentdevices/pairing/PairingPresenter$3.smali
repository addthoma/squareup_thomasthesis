.class Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$3;
.super Lcom/squareup/flowlegacy/NoResultPopupPresenter;
.source "PairingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->buildFailurePopup()Lcom/squareup/flowlegacy/NoResultPopupPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
        "Lcom/squareup/widgets/warning/Warning;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$3;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-direct {p0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 275
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$3;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 0

    .line 277
    invoke-super {p0, p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->onPopupResult(Ljava/lang/Void;)V

    .line 278
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$3;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->onFailurePopupResult()V

    return-void
.end method
