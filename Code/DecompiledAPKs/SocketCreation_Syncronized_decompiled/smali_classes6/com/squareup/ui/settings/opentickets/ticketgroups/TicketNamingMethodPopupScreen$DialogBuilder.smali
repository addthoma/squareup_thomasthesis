.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;
.super Ljava/lang/Object;
.source "TicketNamingMethodPopupScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DialogBuilder"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;Landroid/content/Context;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public build()Landroid/app/Dialog;
    .locals 3

    .line 46
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_change_ticket_naming:I

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_change_ticket_naming_message:I

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketNamingMethodPopupScreen$DialogBuilder$aqfJoJglK47nOd6BLpgo2LZcChM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketNamingMethodPopupScreen$DialogBuilder$aqfJoJglK47nOd6BLpgo2LZcChM;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;)V

    .line 49
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketNamingMethodPopupScreen$DialogBuilder$GKJwKe6s4GHJlrLeQjlWLtvzTGo;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketNamingMethodPopupScreen$DialogBuilder$GKJwKe6s4GHJlrLeQjlWLtvzTGo;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;)V

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 53
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$build$0$TicketNamingMethodPopupScreen$DialogBuilder(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-static {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;)Lio/reactivex/subjects/PublishSubject;

    move-result-object p1

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$build$1$TicketNamingMethodPopupScreen$DialogBuilder(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-static {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;)Lio/reactivex/subjects/PublishSubject;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
