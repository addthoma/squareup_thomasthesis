.class public final Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;
.super Ljava/lang/Object;
.source "PasscodesSettingsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->serverProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/http/Server;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/http/Server;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/http/Server;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/http/Server;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator_Factory;->get()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method
