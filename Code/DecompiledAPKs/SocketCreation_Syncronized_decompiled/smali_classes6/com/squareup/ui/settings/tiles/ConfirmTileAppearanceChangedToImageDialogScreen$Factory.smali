.class public Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmTileAppearanceChangedToImageDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/settings/SettingsAppletScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x1

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->changeTileAppearanceToImage(Z)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/settings/SettingsAppletScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->changeTileAppearanceToImage(Z)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 25
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 26
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 27
    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->settingsAppletScopeRunner()Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/settingsapplet/R$string;->item_appeareance_dialog_confirm_title:I

    .line 29
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->item_appeareance_dialog_confirm_message:I

    .line 30
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->item_appeareance_dialog_yes:I

    new-instance v2, Lcom/squareup/ui/settings/tiles/-$$Lambda$ConfirmTileAppearanceChangedToImageDialogScreen$Factory$2D_VZmcPSxnjEYSAp5oYcJiKEgM;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/tiles/-$$Lambda$ConfirmTileAppearanceChangedToImageDialogScreen$Factory$2D_VZmcPSxnjEYSAp5oYcJiKEgM;-><init>(Lcom/squareup/ui/settings/SettingsAppletScopeRunner;)V

    .line 31
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 33
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->item_appeareance_dialog_cancel:I

    new-instance v2, Lcom/squareup/ui/settings/tiles/-$$Lambda$ConfirmTileAppearanceChangedToImageDialogScreen$Factory$r0K56oJVm58DSBV0Gva5zIapuTY;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/tiles/-$$Lambda$ConfirmTileAppearanceChangedToImageDialogScreen$Factory$r0K56oJVm58DSBV0Gva5zIapuTY;-><init>(Lcom/squareup/ui/settings/SettingsAppletScopeRunner;)V

    .line 34
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 28
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
