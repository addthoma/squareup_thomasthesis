.class public final Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "TaxApplicableScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final taxScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final taxStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->taxStateProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->taxScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/settings/taxes/tax/TaxState;Ldagger/Lazy;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;",
            ")",
            "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;"
        }
    .end annotation

    .line 60
    new-instance v7, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;Ldagger/Lazy;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->taxStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->taxScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/settings/taxes/tax/TaxState;Ldagger/Lazy;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
