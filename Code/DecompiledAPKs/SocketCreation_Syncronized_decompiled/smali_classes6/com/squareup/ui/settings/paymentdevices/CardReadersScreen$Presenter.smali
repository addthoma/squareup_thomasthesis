.class Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "CardReadersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final readerStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 65
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 67
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    .line 68
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 69
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 70
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 71
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->flow:Lflow/Flow;

    .line 72
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    return-void
.end method

.method private hasBluetoothPermission()Z
    .locals 2

    .line 167
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.BLUETOOTH"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)I
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/squareup/util/Objects;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p0

    return p0
.end method

.method private showReaderTypeSelectionCardOrBluetoothRequiredDialog()V
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->showReaderTypeSelectionCardOrPairingCard()V

    goto :goto_0

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->showBluetoothRequiredDialog()V

    :goto_0
    return-void
.end method

.method private showReaderTypeSelectionCardOrPairingCard()V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 183
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "R12 is not enabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method enableBleAndGo()V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->enable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->showReaderTypeSelectionCardOrPairingCard()V

    :cond_0
    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$CardReadersScreen$Presenter(Ljava/util/Collection;)V
    .locals 6

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreen$Presenter$qfpKm-US31TcAD_9LAmmVCWFWDE;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreen$Presenter$qfpKm-US31TcAD_9LAmmVCWFWDE;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->displayHeader(Z)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->updateCardReaderList(Ljava/util/List;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 96
    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v2}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 98
    :goto_0
    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    .line 99
    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->DISPLAY_LEARN_MORE_R4:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 102
    :goto_1
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->hasBluetoothPermission()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    .line 101
    :goto_2
    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->setConnectReaderVisible(Z)V

    .line 103
    invoke-virtual {p1, v0, v4}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->displayFooter(ZZ)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$CardReadersScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreen$Presenter$fn2eq2nG-hlYMXKLdtvcZ3bYam8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreen$Presenter$fn2eq2nG-hlYMXKLdtvcZ3bYam8;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;)V

    .line 85
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method onConnectReaderClicked()V
    .locals 0

    .line 153
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->showReaderTypeSelectionCardOrBluetoothRequiredDialog()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 76
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatusForAllReaders()V

    return-void
.end method

.method onLearnMoreClicked(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;-><init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 81
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreen$Presenter$jTS5FwvYFyf17YA7RcszdWW9dhs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreen$Presenter$jTS5FwvYFyf17YA7RcszdWW9dhs;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onReaderClicked(I)V
    .locals 3

    const/4 v0, 0x1

    sub-int/2addr p1, v0

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 125
    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v1, v2, :cond_0

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/AudioPermissionScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 130
    :cond_0
    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 146
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 148
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "Unknown reader type %s, should we open a detail screen?"

    .line 147
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 143
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;-><init>(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 110
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    return-object v0
.end method
