.class public final Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;
.super Lcom/squareup/ui/settings/orderhub/OrderHubSettingsSectionAccess;
.source "OrderHubQuickActionsSettingsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsSectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "device",
        "Lcom/squareup/util/Device;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "determineVisibility",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OrderHubSettings;->getCanShowQuickActionsSetting()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$Access;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
