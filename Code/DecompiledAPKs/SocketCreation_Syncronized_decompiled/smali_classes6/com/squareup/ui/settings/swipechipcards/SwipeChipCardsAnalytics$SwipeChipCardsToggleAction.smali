.class Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics$SwipeChipCardsToggleAction;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SwipeChipCardsAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SwipeChipCardsToggleAction"
.end annotation


# instance fields
.field private final detail:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->INSTANT_DEPOSIT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics$SwipeChipCardsToggleAction;->detail:Ljava/lang/String;

    return-void
.end method
