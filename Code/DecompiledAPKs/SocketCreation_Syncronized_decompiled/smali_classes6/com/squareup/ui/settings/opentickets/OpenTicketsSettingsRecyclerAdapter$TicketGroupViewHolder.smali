.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;
.super Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;
.source "OpenTicketsSettingsRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "TicketGroupViewHolder"
.end annotation


# instance fields
.field modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

.field presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

.field ticketGroupEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V
    .locals 1

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$BasicViewHolder;-><init>(Landroid/view/View;)V

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    .line 84
    check-cast p1, Lcom/squareup/ui/library/ModifierListRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/ModifierListRow;->setDraggableDisplay(Z)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

    invoke-virtual {p1, v0, v0}, Lcom/squareup/ui/library/ModifierListRow;->setHorizontalBorders(ZZ)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder$1;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/ModifierListRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;
    .locals 4

    .line 74
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;

    .line 75
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/squareup/widgets/pos/R$layout;->items_applet_modifiers_list_row:I

    const/4 v3, 0x0

    .line 76
    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;)V

    return-object v0
.end method


# virtual methods
.method public bindData(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/util/Res;)V
    .locals 3

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->ticketGroupEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 100
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getTicketTemplateCount()I

    move-result v0

    .line 101
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    sget v0, Lcom/squareup/settingsapplet/R$string;->predefined_ticket_group_one_ticket:I

    .line 103
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    sget v2, Lcom/squareup/settingsapplet/R$string;->predefined_ticket_group_many_tickets:I

    .line 104
    invoke-interface {p2, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string v2, "number"

    .line 105
    invoke-virtual {p2, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 106
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 101
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/squareup/ui/library/ModifierListRow;->setContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getDragHandle()Landroid/view/View;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter$TicketGroupViewHolder;->modifierListRow:Lcom/squareup/ui/library/ModifierListRow;

    invoke-virtual {v0}, Lcom/squareup/ui/library/ModifierListRow;->getDragHandle()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    return-object v0
.end method
