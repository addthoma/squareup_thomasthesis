.class public abstract Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InR12PairingScope.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0003R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parentPath",
        "",
        "(Ljava/lang/Object;)V",
        "getParentPath",
        "()Ljava/lang/Object;",
        "getParentKey",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentPath:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    const-string v0, "parentPath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;->parentPath:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;->parentPath:Ljava/lang/Object;

    return-object v0
.end method

.method public final getParentPath()Ljava/lang/Object;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;->parentPath:Ljava/lang/Object;

    return-object v0
.end method
