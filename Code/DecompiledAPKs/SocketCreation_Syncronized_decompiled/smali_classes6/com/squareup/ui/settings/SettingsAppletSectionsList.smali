.class public Lcom/squareup/ui/settings/SettingsAppletSectionsList;
.super Lcom/squareup/applet/AppletSectionsList;
.source "SettingsAppletSectionsList.java"


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/ui/settings/SettingsAppletSections;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletSectionsList;-><init>(Lcom/squareup/applet/AppletEntryPoint;)V

    .line 28
    invoke-interface {p2}, Lcom/squareup/ui/settings/SettingsAppletSections;->orderedEntries()Ljava/util/List;

    move-result-object p1

    .line 29
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/applet/AppletSectionsListEntry;

    .line 30
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->isVisible(Lcom/squareup/applet/AppletSectionsListEntry;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->visibleEntries:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private isVisible(Lcom/squareup/applet/AppletSectionsListEntry;)Z
    .locals 0

    .line 37
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    return p1
.end method
