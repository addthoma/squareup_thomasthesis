.class public Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "CardReaderDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

.field private mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

.field private noReadersRemain:Z

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

.field private final watchdog:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/util/RxWatchdog;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/ReaderEventLogger;",
            "Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 105
    invoke-direct {p0, p1, p6}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 106
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 107
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 108
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 109
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 110
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 111
    iput-object p8, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 112
    iput-object p9, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    .line 113
    iput-object p10, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    .line 114
    iput-object p11, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 115
    iput-object p12, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;)Z
    .locals 0

    .line 82
    iget-boolean p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->noReadersRemain:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;)V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->closeCardAndParent()V

    return-void
.end method

.method static synthetic access$302(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;)Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;
    .locals 0

    .line 82
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->checkSlowAudioConnectionTimeout(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    return-void
.end method

.method private checkSlowAudioConnectionTimeout(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 4

    .line 252
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne v0, v1, :cond_0

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    const-wide/16 v1, 0xf

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    goto :goto_0

    .line 255
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {p1}, Lcom/squareup/util/RxWatchdog;->cancel()V

    :goto_0
    return-void
.end method

.method private closeCardAndParent()V
    .locals 3

    .line 231
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->noReadersRemain:Z

    const-string v1, "There should be no connected card readers"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->handleReaderSettingsCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$Z81_X7NNn7xbKFM8pRh-s8KTcx8;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$Z81_X7NNn7xbKFM8pRh-s8KTcx8;

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$closeCardAndParent$5(Lflow/History;)Lcom/squareup/container/Command;
    .locals 5

    .line 240
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 243
    invoke-virtual {v0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v1

    .line 244
    instance-of v2, v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object p0, v3, v1

    const-string p0, "%s is not an instance of CardReadersScreen, history: %s"

    invoke-static {v2, p0, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 247
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method forgetReader()V
    .locals 6

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_FORGET_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 207
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->destroyWirelessAutoConnect(Ljava/lang/String;)V

    goto :goto_1

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    if-nez v1, :cond_1

    .line 216
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v5, v5, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 218
    invoke-virtual {v5}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v5

    iget v5, v5, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "CardReaderDetailScreen#forgetReader() on reader that doesn\'t exist (id=%d)"

    .line 216
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :cond_1
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->forget()V

    .line 222
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 224
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/BluetoothUtils;->unpairDevice(Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->getPairingEventListener()Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 227
    invoke-interface {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;->failedPairing(Ljava/lang/String;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    return-object v0
.end method

.method identifyReader()V
    .locals 5

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_IDENTIFY_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 192
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 195
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v4, v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 199
    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v4

    iget v4, v4, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "CardReaderDetailScreen#identifyReader() on reader that doesn\'t exist (id=%d)"

    .line 197
    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_0
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->identify()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$0$CardReaderDetailScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    if-ne v0, p2, :cond_0

    .line 131
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 132
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    .line 131
    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->audioConnectionTakingLongTime(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$2$CardReaderDetailScreen$Presenter(Ljava/util/Collection;)V
    .locals 0

    .line 137
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->noReadersRemain:Z

    return-void
.end method

.method public synthetic lambda$onLoad$1$CardReaderDetailScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$OpobjzQSmpEMGMopBlTFlSJV6uc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$OpobjzQSmpEMGMopBlTFlSJV6uc;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    .line 129
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$CardReaderDetailScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)Lrx/Subscription;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$AG9aJ_3z-2gcUGbd2s4Y0VymPfE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$AG9aJ_3z-2gcUGbd2s4Y0VymPfE;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;)V

    .line 137
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 138
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->filterByReaderAddress(Ljava/lang/String;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    .line 139
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$CardReaderDetailScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 171
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->getNickName()Ljava/lang/String;

    move-result-object p1

    .line 170
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->updateStoredReaderName(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_NAME_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/log/ReaderEventLogger;->logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->closeCard()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 119
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;

    .line 120
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;->access$000(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 124
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;

    .line 127
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$LzSBNj01ZlqhKkpVe0OJ4nkPVdg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$LzSBNj01ZlqhKkpVe0OJ4nkPVdg;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 136
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$Fw05dY2cL5KVGdH9hOwPt-4aRs0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$Fw05dY2cL5KVGdH9hOwPt-4aRs0;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->buildActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->canEditReaderNickname(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 167
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$usbxI1krX7wRxDc9Bp2nPKwmUPw;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreen$Presenter$usbxI1krX7wRxDc9Bp2nPKwmUPw;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    .line 168
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 179
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 183
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;

    return-object v0
.end method
