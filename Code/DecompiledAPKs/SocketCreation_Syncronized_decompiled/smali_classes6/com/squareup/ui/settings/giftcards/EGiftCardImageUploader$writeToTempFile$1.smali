.class final Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;
.super Ljava/lang/Object;
.source "EGiftCardImageUploader.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->writeToTempFile(Landroid/graphics/Bitmap;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEGiftCardImageUploader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EGiftCardImageUploader.kt\ncom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Ljava/io/File;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_writeToTempFile:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;->this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;->$this_writeToTempFile:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/io/File;
    .locals 6

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;->this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->access$getTempPhotoDir$p(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)Ljava/io/File;

    move-result-object v0

    const-string v1, "TEMP"

    const-string v2, ".jpg"

    invoke-static {v1, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    const-string v1, "File.createTempFile(\"TEMP\", \".jpg\", tempPhotoDir)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 71
    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;->$this_writeToTempFile:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object v4, v1

    check-cast v4, Ljava/io/OutputStream;

    const/16 v5, 0x64

    invoke-virtual {v2, v3, v5, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 72
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 73
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$writeToTempFile$1;->call()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
