.class public Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "LearnMoreReaderScreen.java"

# interfaces
.implements Lcom/squareup/pauses/PausesAndResumes;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;",
        ">;",
        "Lcom/squareup/pauses/PausesAndResumes;"
    }
.end annotation


# static fields
.field private static final R12_VIDEO_CLICK_TIME_KEY:Ljava/lang/String;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final flow:Lflow/Flow;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private r12VideoClickTime:Ljava/lang/Long;

.field private readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;

    .line 92
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".r12VideoClickTime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->R12_VIDEO_CLICK_TIME_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/util/Res;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 97
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 100
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 101
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    .line 102
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 103
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private showPairingCard()V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method enableBleAndGo()V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->showPairingCard()V

    :cond_0
    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 158
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->square_reader_magstripe:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/R$string;->square_reader_bluetooth:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getR12VideoClickTime()Ljava/lang/Long;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    return-object v0
.end method

.method onCheckCompatibilityClicked()V
    .locals 3

    .line 180
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->check_compatibility_url:I

    .line 181
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-static {v0, v1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method onConnectReaderClicked()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->showPairingCard()V

    goto :goto_0

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->showBluetoothRequiredDialog()V

    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 107
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 108
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;

    .line 109
    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;->access$000(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0, p1, p0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 114
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    .line 116
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->R12_VIDEO_CLICK_TIME_KEY:Ljava/lang/String;

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    cmp-long p1, v3, v1

    if-eqz p1, :cond_0

    .line 118
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    .line 123
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->getActionbarText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$NGIWMoIDa6_ZpXCz23MUWbaz7Ok;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$NGIWMoIDa6_ZpXCz23MUWbaz7Ok;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->show(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getVideoUrlSettings()Lcom/squareup/settings/server/VideoUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/VideoUrlSettings;->hasR12GettingStartedVideoInfo()Z

    move-result v0

    .line 132
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->setVideoTutorialLinkVisible(Z)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v0

    .line 136
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->setConnectReaderVisible(Z)V

    xor-int/lit8 v0, v0, 0x1

    .line 137
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->setCheckCompatibilityVisible(Z)V

    return-void
.end method

.method onOrderReaderClicked()V
    .locals 3

    .line 167
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 168
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderReaderUrl(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-static {v0, v1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 7

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->R12_MEET_THE_READER_MODAL:Lcom/squareup/analytics/RegisterViewName;

    .line 211
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "After Finished with Introductory Video"

    invoke-direct {v1, v2, v4, v3}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/Long;)V

    .line 208
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    const/4 v0, 0x0

    .line 214
    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, -0x1

    .line 154
    :goto_0
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->R12_VIDEO_CLICK_TIME_KEY:Ljava/lang/String;

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method onTutorialVideoLinkClicked()V
    .locals 3

    .line 185
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->r12VideoClickTime:Ljava/lang/Long;

    .line 187
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    .line 188
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 189
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getVideoUrlSettings()Lcom/squareup/settings/server/VideoUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/VideoUrlSettings;->getR12GettingStartedVideoYoutubeId()Ljava/lang/String;

    move-result-object v1

    .line 188
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->launchYouTubeMovie(Ljava/lang/String;)V

    return-void
.end method

.method onUpClicked()V
    .locals 3

    .line 141
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->R12_MEET_THE_READER_MODAL_DISMISSED_BY_USER:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method
