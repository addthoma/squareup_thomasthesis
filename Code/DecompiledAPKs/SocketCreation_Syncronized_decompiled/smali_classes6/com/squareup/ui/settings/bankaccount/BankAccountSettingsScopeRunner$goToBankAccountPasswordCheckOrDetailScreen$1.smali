.class final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountPasswordCheckOrDetailScreen$1;
.super Ljava/lang/Object;
.source "BankAccountSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->goToBankAccountPasswordCheckOrDetailScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "bankSettingsState",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountPasswordCheckOrDetailScreen$1;->this$0:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/banklinking/BankAccountSettings$State;)V
    .locals 4

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountPasswordCheckOrDetailScreen$1;->this$0:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->access$getFlow$p(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)Lflow/Flow;

    move-result-object v0

    .line 253
    new-instance v1, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;

    .line 254
    iget-boolean v2, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    .line 255
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v3, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 256
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 253
    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    .line 252
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountPasswordCheckOrDetailScreen$1;->accept(Lcom/squareup/banklinking/BankAccountSettings$State;)V

    return-void
.end method
