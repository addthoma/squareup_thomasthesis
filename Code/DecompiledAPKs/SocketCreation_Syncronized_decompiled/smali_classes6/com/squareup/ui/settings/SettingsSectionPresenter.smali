.class public abstract Lcom/squareup/ui/settings/SettingsSectionPresenter;
.super Lmortar/ViewPresenter;
.source "SettingsSectionPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/ViewGroup;",
        ":",
        "Lcom/squareup/ui/HasActionBar;",
        ">",
        "Lmortar/ViewPresenter<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u000c\u0008\u0000\u0010\u0001*\u00020\u0002*\u00020\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u0004:\u0001(B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0017\u001a\u00020\u0018H\u0002J\u0015\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001eH\u0014J\u0012\u0010\u001f\u001a\u00020\u00162\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0014J\u0008\u0010\"\u001a\u00020\u0013H\u0014J\u0008\u0010#\u001a\u00020\u0016H\u0014J\u0008\u0010$\u001a\u00020\u0016H$J\u0010\u0010%\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\'0&H$R\u0012\u0010\u0008\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\rX\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter;",
        "T",
        "Landroid/view/ViewGroup;",
        "Lcom/squareup/ui/HasActionBar;",
        "Lmortar/ViewPresenter;",
        "coreParameters",
        "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
        "(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V",
        "actionbarText",
        "",
        "getActionbarText",
        "()Ljava/lang/String;",
        "device",
        "Lcom/squareup/util/Device;",
        "getDevice",
        "()Lcom/squareup/util/Device;",
        "flow",
        "Lflow/Flow;",
        "loggingOut",
        "",
        "onLoggingOut",
        "Lio/reactivex/Observable;",
        "",
        "buildDefaultConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
        "dropView",
        "view",
        "(Landroid/view/ViewGroup;)V",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onUpPressed",
        "onUpPressedAction",
        "saveSettings",
        "screenForAssertion",
        "Ljava/lang/Class;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "CoreParameters",
        "settings-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private loggingOut:Z

.field private final onLoggingOut:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V
    .locals 1

    const-string v0, "coreParameters"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 40
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getDevice()Lcom/squareup/util/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->device:Lcom/squareup/util/Device;

    .line 41
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->flow:Lflow/Flow;

    .line 42
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getOnLoggingOut()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoggingOut:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getLoggingOut$p(Lcom/squareup/ui/settings/SettingsSectionPresenter;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->loggingOut:Z

    return p0
.end method

.method public static final synthetic access$setLoggingOut$p(Lcom/squareup/ui/settings/SettingsSectionPresenter;Z)V
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->loggingOut:Z

    return-void
.end method

.method private final buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 96
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 97
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const-string v1, "Builder()\n        .hideP\u2026   .hideSecondaryButton()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public dropView(Landroid/view/ViewGroup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-boolean v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->loggingOut:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->saveSettings()V

    .line 77
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->dropView(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public abstract getActionbarText()Ljava/lang/String;
.end method

.method protected final getDevice()Lcom/squareup/util/Device;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->device:Lcom/squareup/util/Device;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoggingOut:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->getActionbarText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 50
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 51
    new-instance v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$onLoad$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter$onLoad$1;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->getActionbarText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 61
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const-string v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/HasActionBar;

    invoke-interface {v0}, Lcom/squareup/ui/HasActionBar;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "view.actionBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method protected onUpPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onUpPressedAction()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter;->flow:Lflow/Flow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->screenForAssertion()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method protected abstract saveSettings()V
.end method

.method protected abstract screenForAssertion()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation
.end method
