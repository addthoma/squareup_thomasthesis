.class public Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;
.super Ljava/lang/Object;
.source "LoyaltySettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenData"
.end annotation


# instance fields
.field final frontOfTransactionEnabled:Z

.field final frontOfTransactionToggleDisplayed:Z

.field final loyaltyScreensEnabled:Z

.field final showNonQualifyingEvents:Z

.field final timeout30:Z

.field final timeout60:Z

.field final timeout90:Z


# direct methods
.method public constructor <init>(ZZZZZZZ)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->loyaltyScreensEnabled:Z

    .line 33
    iput-boolean p2, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->showNonQualifyingEvents:Z

    .line 34
    iput-boolean p3, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->timeout30:Z

    .line 35
    iput-boolean p4, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->timeout60:Z

    .line 36
    iput-boolean p5, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->timeout90:Z

    .line 37
    iput-boolean p6, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->frontOfTransactionToggleDisplayed:Z

    .line 38
    iput-boolean p7, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->frontOfTransactionEnabled:Z

    return-void
.end method
