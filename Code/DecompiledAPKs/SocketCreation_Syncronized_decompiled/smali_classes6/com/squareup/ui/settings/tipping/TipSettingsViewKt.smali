.class public final Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;
.super Ljava/lang/Object;
.source "TipSettingsView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTipSettingsView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TipSettingsView.kt\ncom/squareup/ui/settings/tipping/TipSettingsViewKt\n+ 2 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt\n+ 3 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n+ 4 RowUiModel.kt\ncom/squareup/mosaic/components/RowUiModelKt\n*L\n1#1,370:1\n129#2:371\n141#2,2:372\n130#2:374\n15#3,8:375\n20#3,4:383\n15#3,8:387\n20#3,2:395\n23#3:399\n15#3,8:400\n20#3,4:408\n18#4,2:397\n*E\n*S KotlinDebug\n*F\n+ 1 TipSettingsView.kt\ncom/squareup/ui/settings/tipping/TipSettingsViewKt\n*L\n296#1:371\n296#1,2:372\n296#1:374\n308#1,8:375\n308#1,4:383\n322#1,8:387\n322#1,2:395\n322#1:399\n339#1,8:400\n339#1,4:408\n322#1,2:397\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a,\u0010\u0002\u001a\u00020\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005*\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u0002H\u00040\u00062\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0002\u001a0\u0010\n\u001a\u00020\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005*\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u0002H\u00040\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u0002\u001a@\u0010\u000f\u001a\u00020\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005*\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u0002H\u00040\u00062\u001b\u0010\u0010\u001a\u0017\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0012\u0012\u0004\u0012\u00020\u00030\u0011\u00a2\u0006\u0002\u0008\u0013H\u0082\u0008\u001aF\u0010\u0014\u001a\u00020\u0003\"\u0008\u0008\u0000\u0010\u0015*\u00020\u0016*\u000c\u0012\u0004\u0012\u0002H\u0015\u0012\u0002\u0008\u00030\u000b2#\u0010\u0010\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0015\u0012\u0004\u0012\u00020\u00030\u000b\u0012\u0004\u0012\u00020\u00030\u0011\u00a2\u0006\u0002\u0008\u0013H\u0082\u0008\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "MarinSwitchType",
        "Lcom/squareup/noho/CheckType$Custom;",
        "description",
        "",
        "P",
        "",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "id",
        "",
        "marinHeading",
        "Lcom/squareup/blueprint/VerticalBlock;",
        "textModel",
        "Lcom/squareup/resources/TextModel;",
        "",
        "marinRow",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/mosaic/components/RowUiModel;",
        "Lkotlin/ExtensionFunctionType;",
        "section",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "settings-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MarinSwitchType:Lcom/squareup/noho/CheckType$Custom;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 355
    new-instance v0, Lcom/squareup/noho/CheckType$Custom;

    sget-object v1, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lcom/squareup/noho/CheckType$Custom;-><init>(ZLkotlin/jvm/functions/Function2;)V

    sput-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->MarinSwitchType:Lcom/squareup/noho/CheckType$Custom;

    return-void
.end method

.method public static final synthetic access$description(Lcom/squareup/blueprint/BlueprintContext;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->description(Lcom/squareup/blueprint/BlueprintContext;I)V

    return-void
.end method

.method public static final synthetic access$getMarinSwitchType$p()Lcom/squareup/noho/CheckType$Custom;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->MarinSwitchType:Lcom/squareup/noho/CheckType$Custom;

    return-object v0
.end method

.method public static final synthetic access$marinHeading(Lcom/squareup/blueprint/VerticalBlock;Lcom/squareup/resources/TextModel;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->marinHeading(Lcom/squareup/blueprint/VerticalBlock;Lcom/squareup/resources/TextModel;)V

    return-void
.end method

.method public static final synthetic access$marinRow(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->marinRow(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$section(Lcom/squareup/blueprint/VerticalBlock;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->section(Lcom/squareup/blueprint/VerticalBlock;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private static final description(Lcom/squareup/blueprint/BlueprintContext;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;I)V"
        }
    .end annotation

    .line 404
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 408
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 404
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 340
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    new-instance v2, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$description$$inlined$model$lambda$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$description$$inlined$model$lambda$1;-><init>(I)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/LabelUiModelKt;->label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    .line 344
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 404
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private static final marinHeading(Lcom/squareup/blueprint/VerticalBlock;Lcom/squareup/resources/TextModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/VerticalBlock<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 307
    new-instance v0, Lcom/squareup/resources/ResourceDimen;

    sget v1, Lcom/squareup/settingsapplet/R$dimen;->tipping_screen_heading_extra_top:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/DimenModel;

    invoke-virtual {p0, v0}, Lcom/squareup/blueprint/VerticalBlock;->spacing(Lcom/squareup/resources/DimenModel;)V

    .line 308
    check-cast p0, Lcom/squareup/blueprint/BlueprintContext;

    .line 379
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 383
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 379
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 309
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    new-instance v2, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$$inlined$model$lambda$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$$inlined$model$lambda$1;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/LabelUiModelKt;->label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    .line 313
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 379
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private static final marinRow(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 391
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 395
    invoke-interface/range {p0 .. p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 391
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 323
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    .line 397
    new-instance v15, Lcom/squareup/mosaic/components/RowUiModel;

    invoke-interface {v1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3fe

    const/4 v14, 0x0

    move-object v2, v15

    invoke-direct/range {v2 .. v14}, Lcom/squareup/mosaic/components/RowUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/mosaic/components/RowUiModel$IconModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/noho/AccessoryType;Lcom/squareup/noho/CheckType;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 326
    sget-object v2, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinRow$1$1$1;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinRow$1$1$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v15, v2}, Lcom/squareup/mosaic/components/RowUiModel;->customOnce(Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v2, p1

    .line 330
    invoke-interface {v2, v15}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    check-cast v15, Lcom/squareup/mosaic/core/UiModel;

    .line 397
    invoke-interface {v1, v15}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    .line 332
    check-cast v0, Lcom/squareup/blueprint/Block;

    move-object/from16 v1, p0

    .line 391
    invoke-interface {v1, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private static final section(Lcom/squareup/blueprint/VerticalBlock;Lkotlin/jvm/functions/Function1;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            ">(",
            "Lcom/squareup/blueprint/VerticalBlock<",
            "TC;*>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/VerticalBlock<",
            "TC;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 296
    check-cast p0, Lcom/squareup/blueprint/BlueprintContext;

    .line 297
    new-instance v0, Lcom/squareup/resources/ResourceDimen;

    sget v1, Lcom/squareup/settingsapplet/R$dimen;->tipping_screen_section_padding_horizontal:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/resources/DimenModel;

    .line 298
    new-instance v0, Lcom/squareup/resources/ResourceDimen;

    sget v1, Lcom/squareup/settingsapplet/R$dimen;->tipping_screen_section_padding_vertical:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    move-object v5, v0

    check-cast v5, Lcom/squareup/resources/DimenModel;

    .line 371
    new-instance v0, Lcom/squareup/blueprint/InsetBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xf8

    const/4 v12, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v12}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 300
    move-object v1, v0

    check-cast v1, Lcom/squareup/blueprint/BlueprintContext;

    .line 372
    new-instance v8, Lcom/squareup/blueprint/VerticalBlock;

    invoke-interface {v1}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/blueprint/VerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v8}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v8, Lcom/squareup/blueprint/Block;

    invoke-interface {v1, v8}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 301
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 371
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method
