.class public final Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;
.super Ljava/lang/Object;
.source "StoredCardReaders_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderMessagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;"
        }
    .end annotation
.end field

.field private final savedCardReaderStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;>;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->savedCardReaderStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;>;)",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;)",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->savedCardReaderStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->newInstance(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->get()Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    move-result-object v0

    return-object v0
.end method
