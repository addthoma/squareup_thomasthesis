.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$ComponentFactory;
.super Ljava/lang/Object;
.source "PrinterStationScope.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 130
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 131
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 132
    check-cast p2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    .line 133
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    .line 134
    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->printerStation(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;

    move-result-object p1

    return-object p1
.end method
