.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;
.super Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;
.source "EditTicketGroupScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final ticketGroupCount:I

.field final ticketGroupId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupScreen$uRBVDxCjRzCE6VX3gWffOIBk74A;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupScreen$uRBVDxCjRzCE6VX3gWffOIBk74A;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupId:Ljava/lang/String;

    .line 28
    iput p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupCount:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupId:Ljava/lang/String;

    .line 34
    iput p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupCount:I

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;
    .locals 2

    .line 58
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;-><init>(I)V

    return-object v0

    .line 63
    :cond_0
    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;-><init>(Ljava/lang/String;I)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 46
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 47
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;->ticketGroupCount:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EDIT_TICKET_GROUP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 67
    sget v0, Lcom/squareup/settingsapplet/R$layout;->edit_ticket_group_view:I

    return v0
.end method
