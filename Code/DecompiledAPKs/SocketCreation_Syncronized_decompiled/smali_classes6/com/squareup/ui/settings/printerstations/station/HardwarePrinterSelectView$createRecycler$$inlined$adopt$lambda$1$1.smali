.class final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "HardwarePrinterSelectView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "pos",
        "",
        "printerRow",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
        "invoke",
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$3$1$1",
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$row$lambda$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;Lcom/squareup/cycler/StandardRowSpec$Creator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;

    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;->invoke(ILcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V
    .locals 2

    const-string v0, "printerRow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/RadioButtonListRow;

    invoke-static {v0, p1, v1, p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->access$bindPrinterRow(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;ILcom/squareup/ui/RadioButtonListRow;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V

    return-void
.end method
