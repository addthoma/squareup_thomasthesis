.class public final Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;
.super Ljava/lang/Object;
.source "CardReaderOracleFilters.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lrx/Observable$Transformer<",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;>;"
        }
    .end annotation

    .line 72
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$MQJJZgSEnmfzmY3RmGwD4BGGUKY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$MQJJZgSEnmfzmY3RmGwD4BGGUKY;-><init>(Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method private static containsReader(Ljava/util/Collection;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .line 61
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 62
    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static containsReaderWithAddress(Ljava/lang/String;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/functions/Func1<",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 56
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$n_kq6Mozo2E8bj3aEWDzz35WZfc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$n_kq6Mozo2E8bj3aEWDzz35WZfc;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static filterByReaderAddress(Ljava/lang/String;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable$Transformer<",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$zsb1-1fZxWFMU9ujFIyporLEfBo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$zsb1-1fZxWFMU9ujFIyporLEfBo;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static isReaderTypeSupported(Lcom/squareup/settings/server/Features;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lrx/functions/Func1<",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 85
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$65mkwTwomNz2RL_LPHfxccn5rgM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$65mkwTwomNz2RL_LPHfxccn5rgM;-><init>(Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method static synthetic lambda$asCapabilities$6(Lcom/squareup/settings/server/Features;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 72
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$cf6Fdy7DTCJuuBLB-64oJoE3gAk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$cf6Fdy7DTCJuuBLB-64oJoE3gAk;-><init>(Lcom/squareup/settings/server/Features;)V

    .line 73
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 81
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$containsReaderWithAddress$4(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/Boolean;
    .locals 0

    .line 56
    invoke-static {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->containsReader(Ljava/util/Collection;Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$filterByReaderAddress$2(Ljava/lang/String;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 43
    invoke-static {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->notContainsReaderWithAddress(Ljava/lang/String;)Lrx/functions/Func1;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->skipWhile(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 44
    invoke-static {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->containsReaderWithAddress(Ljava/lang/String;)Lrx/functions/Func1;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->takeWhile(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$sxsDz83BWQE6C_tU8cgGvyf-egw;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$sxsDz83BWQE6C_tU8cgGvyf-egw;

    .line 45
    invoke-virtual {p1, v0}, Lrx/Observable;->flatMapIterable(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$vZfWRzaYB2c6n8RTTPXOP8AwTKg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$vZfWRzaYB2c6n8RTTPXOP8AwTKg;-><init>(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$isReaderTypeSupported$7(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/Boolean;
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    .line 88
    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p1, v0, :cond_3

    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    .line 89
    invoke-interface {p0, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 90
    :cond_2
    sget-boolean p0, Lcom/squareup/cardreader/CardReaderHubUtils;->READER_TYPE_IS_IGNORED:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    .line 93
    :cond_3
    sget-boolean p0, Lcom/squareup/cardreader/CardReaderHubUtils;->READER_TYPE_IS_SUPPORTED:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$notContainsReaderWithAddress$3(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/Boolean;
    .locals 0

    .line 51
    invoke-static {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->containsReader(Ljava/util/Collection;Ljava/lang/String;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Ljava/util/Collection;)Ljava/lang/Iterable;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$null$1(Ljava/lang/String;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Ljava/lang/Boolean;
    .locals 0

    .line 46
    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$5(Lcom/squareup/settings/server/Features;Ljava/util/Collection;)Ljava/util/EnumSet;
    .locals 2

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 75
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 76
    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_0
    invoke-static {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->isReaderTypeSupported(Lcom/squareup/settings/server/Features;)Lrx/functions/Func1;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/CardReaderHubUtils;->connectedReaderCapabilities(Ljava/util/Collection;Lrx/functions/Func1;)Ljava/util/EnumSet;

    move-result-object p0

    return-object p0
.end method

.method private static notContainsReaderWithAddress(Ljava/lang/String;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/functions/Func1<",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$Z_iDbMHSPuVDYP3AibcZh3qxP5o;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracleFilters$Z_iDbMHSPuVDYP3AibcZh3qxP5o;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
