.class Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;
.super Ljava/lang/Object;
.source "TileAppearanceSettings.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 74
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->access$000(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "COGS_CONFIG_CACHE"

    .line 79
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    .line 84
    invoke-static {p1}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    move-result-object p1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->access$000(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->access$000(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->access$000(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->toByteArray()[B

    move-result-object v0

    const-string v1, "COGS_CONFIG_CACHE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    return-void
.end method
