.class public Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;
.super Landroid/widget/LinearLayout;
.source "CardReadersScreenView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private bleFooter:Landroid/view/View;

.field cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private connectReader:Landroid/widget/Button;

.field private header:Landroid/view/View;

.field private learnMoreContactless:Lcom/squareup/widgets/list/NameValueRow;

.field private learnMoreFooter:Landroid/view/View;

.field private final learnMoreListener:Landroid/view/View$OnClickListener;

.field private learnMoreMagstripe:Lcom/squareup/widgets/list/NameValueRow;

.field private listView:Landroid/widget/ListView;

.field presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance p2, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreListener:Landroid/view/View$OnClickListener;

    .line 63
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Component;->inject(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V

    return-void
.end method


# virtual methods
.method displayFooter(ZZ)V
    .locals 4

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->bleFooter:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreContactless:Lcom/squareup/widgets/list/NameValueRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreMagstripe:Lcom/squareup/widgets/list/NameValueRow;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    new-instance p2, Lcom/squareup/marin/widgets/HorizontalRuleView;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, v1}, Lcom/squareup/marin/widgets/HorizontalRuleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreFooter:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 149
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method displayHeader(Z)V
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    if-eqz p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->header:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_0
    if-nez p1, :cond_1

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result p1

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->header:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 123
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 165
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$CardReadersScreenView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->onReaderClicked(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 109
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 115
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 68
    sget v0, Lcom/squareup/settingsapplet/R$id;->reader_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    .line 69
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreenView$s_51ZALRB2POCm5e-XPO8u9t704;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersScreenView$s_51ZALRB2POCm5e-XPO8u9t704;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$layout;->cardreaders_header_view:I

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    const/4 v3, 0x0

    .line 76
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->header:Landroid/view/View;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$layout;->cardreaders_ble_footer_view:I

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    .line 79
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->bleFooter:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->bleFooter:Landroid/view/View;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->device_contactless_help:I

    .line 82
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 83
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/cardreader/ui/R$string;->square_reader_help:I

    const-string v4, "square_shop"

    .line 84
    invoke-virtual {v1, v2, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 85
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderHardwareUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->square_shop:I

    .line 86
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/util/AppNameFormatterKt;->putAppName(Lcom/squareup/phrase/Phrase;Landroid/content/res/Resources;)Lcom/squareup/phrase/Phrase;

    .line 89
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->bleFooter:Landroid/view/View;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->connect_reader_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->connectReader:Landroid/widget/Button;

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->connectReader:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$layout;->cardreaders_learn_more:I

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreFooter:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreFooter:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->learn_more_r12:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreContactless:Lcom/squareup/widgets/list/NameValueRow;

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreFooter:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->learn_more_magstripe:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreMagstripe:Lcom/squareup/widgets/list/NameValueRow;

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreContactless:Lcom/squareup/widgets/list/NameValueRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreMagstripe:Lcom/squareup/widgets/list/NameValueRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->learnMoreListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method setConnectReaderVisible(Z)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->connectReader:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showBluetoothRequiredDialog()V
    .locals 2

    .line 153
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$3;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog;->show(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;)V

    return-void
.end method

.method updateCardReaderList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->updateReaderList(Ljava/util/List;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->notifyDataSetChanged()V

    return-void
.end method
