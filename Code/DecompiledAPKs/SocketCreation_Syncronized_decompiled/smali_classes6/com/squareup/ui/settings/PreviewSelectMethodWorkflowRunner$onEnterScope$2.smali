.class final Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "PreviewSelectMethodWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPreviewSelectMethodWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PreviewSelectMethodWorkflowRunner.kt\ncom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,63:1\n152#2:64\n*E\n*S KotlinDebug\n*F\n+ 1 PreviewSelectMethodWorkflowRunner.kt\ncom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2\n*L\n52#1:64\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2;->accept(Lkotlin/Unit;)V

    return-void
.end method

.method public final accept(Lkotlin/Unit;)V
    .locals 3

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->access$getContainer$p(Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 64
    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    return-void
.end method
