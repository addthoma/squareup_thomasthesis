.class public final enum Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;
.super Ljava/lang/Enum;
.source "ReaderState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/ReaderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_BATTERY_DEAD:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_BLUETOOTH_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_CHECKING_SERVER_FOR_FWUP:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_COMMS_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_FWUP_ERROR:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_KEY_INJECTION_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_KEY_INJECTION_STARTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_SS_ESTABLISHING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_STILL_BLUETOOTH_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_TAMPERED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public static final enum READER_TMS_INVALID:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;


# instance fields
.field public final level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 75
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v2, 0x0

    const-string v3, "READER_CHECKING_SERVER_FOR_FWUP"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_CHECKING_SERVER_FOR_FWUP:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 76
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v3, 0x1

    const-string v4, "READER_KEY_INJECTION_STARTED"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_STARTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 77
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v4, 0x2

    const-string v5, "READER_IS_FWUP_REBOOTING"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 79
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v5, 0x3

    const-string v6, "READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 80
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v6, 0x4

    const-string v7, "READER_APPLYING_BLOCKING_FWUP_BUNDLE"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 83
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v7, 0x5

    const-string v8, "READER_KEY_INJECTION_FAILED"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 84
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v8, 0x6

    const-string v9, "READER_TAMPERED"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TAMPERED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 85
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v9, 0x7

    const-string v10, "READER_FWUP_ERROR"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_FWUP_ERROR:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 86
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v10, 0x8

    const-string v11, "READER_SS_SERVER_DENIED"

    invoke-direct {v0, v11, v10, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 87
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v11, 0x9

    const-string v12, "READER_SS_FAILED"

    invoke-direct {v0, v12, v11, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 88
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v12, 0xa

    const-string v13, "READER_BATTERY_DEAD"

    invoke-direct {v0, v13, v12, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_DEAD:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 89
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v13, 0xb

    const-string v14, "READER_TMS_INVALID"

    invoke-direct {v0, v14, v13, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TMS_INVALID:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 92
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v14, 0xc

    const-string v15, "READER_MIC_PERMISSION_MISSING"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 95
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v15, 0xd

    const-string v14, "READER_STILL_BLE_PAIRING"

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 96
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/16 v14, 0xe

    const-string v15, "READER_STILL_BLUETOOTH_PAIRING"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLUETOOTH_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 97
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v15, "READER_SS_ESTABLISHING_SESSION"

    const/16 v14, 0xf

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_ESTABLISHING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 98
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_SS_RENEWING_SESSION"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 99
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_COMMS_CONNECTING"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_COMMS_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 102
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_BATTERY_LOW"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 104
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_SS_OFFLINE_SWIPE_ONLY"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 105
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_READY"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 108
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_BLE_DISCONNECTED"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 109
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const-string v14, "READER_BLUETOOTH_DISCONNECTED"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;-><init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLUETOOTH_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 73
    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_CHECKING_SERVER_FOR_FWUP:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_STARTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TAMPERED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_FWUP_ERROR:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_DEAD:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TMS_INVALID:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLUETOOTH_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_ESTABLISHING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_COMMS_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLUETOOTH_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;",
            ")V"
        }
    .end annotation

    .line 114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 115
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object v0
.end method
