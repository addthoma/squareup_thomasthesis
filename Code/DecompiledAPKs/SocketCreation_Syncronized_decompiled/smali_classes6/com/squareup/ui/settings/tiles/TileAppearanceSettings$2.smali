.class Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;
.super Ljava/lang/Object;
.source "TileAppearanceSettings.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->writeToCogs(Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field final synthetic val$catalogConfiguration:Lcom/squareup/shared/catalog/models/CatalogConfiguration;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;->val$catalogConfiguration:Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 166
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;->val$catalogConfiguration:Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v1
.end method
