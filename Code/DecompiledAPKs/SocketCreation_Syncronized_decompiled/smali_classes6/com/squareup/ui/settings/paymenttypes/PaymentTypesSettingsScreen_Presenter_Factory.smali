.class public final Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "PaymentTypesSettingsScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final computationSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final customerCheckoutSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTenderSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final refresherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderSettingsManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p5, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p6, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p7, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p8, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->refresherProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p9, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p10, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p11, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p12, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->customerCheckoutSettingsProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p13, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;"
        }
    .end annotation

    .line 96
    new-instance v14, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletScopeRunner;Lcom/squareup/ui/settings/SidebarRefresher;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;
    .locals 15

    .line 107
    new-instance v14, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletScopeRunner;Lcom/squareup/ui/settings/SidebarRefresher;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/settings/server/Features;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;
    .locals 14

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tenderpayment/TenderSettingsManager;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->refresherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/settings/SidebarRefresher;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->customerCheckoutSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletScopeRunner;Lcom/squareup/ui/settings/SidebarRefresher;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
