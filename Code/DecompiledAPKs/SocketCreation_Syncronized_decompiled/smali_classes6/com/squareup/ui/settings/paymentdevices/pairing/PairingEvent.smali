.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PairingEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    }
.end annotation


# instance fields
.field public final connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public final error:Ljava/lang/String;

.field public final firmwareVersion:Ljava/lang/String;

.field public final hardwareSerialNumberLast4:Ljava/lang/String;

.field public final lookingToPair:Z

.field public final macAddress:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V
    .locals 2

    .line 36
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 37
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->name:Ljava/lang/String;

    .line 38
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 39
    iget-boolean v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->lookingToPair:Z

    iput-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->lookingToPair:Z

    .line 40
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->hardwareSerialNumberLast4:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->hardwareSerialNumberLast4:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->firmwareVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->firmwareVersion:Ljava/lang/String;

    .line 42
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->macAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->macAddress:Ljava/lang/String;

    .line 43
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 44
    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->error:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->error:Ljava/lang/String;

    return-void
.end method

.method public static eventBuilder(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    .locals 1

    .line 60
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->readerEventName(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static logAnalytics(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string v1, "To EventStream: %s"

    .line 83
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    invoke-interface {p1, p0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public static logAnalyticsAndOhSnap(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;)V
    .locals 0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;

    move-result-object p0

    .line 90
    invoke-static {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->logAnalytics(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;Lcom/squareup/analytics/Analytics;)V

    .line 91
    invoke-static {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->logOhSnap(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;Lcom/squareup/log/OhSnapLogger;)V

    return-void
.end method

.method public static logOhSnap(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;Lcom/squareup/log/OhSnapLogger;)V
    .locals 5

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    .line 70
    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->name:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->error:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->macAddress:Ljava/lang/String;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->hardwareSerialNumberLast4:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object p0, v1, v2

    array-length p0, v1

    :goto_0
    if-ge v3, p0, :cond_1

    aget-object v2, v1, v3

    if-eqz v2, :cond_0

    .line 75
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 79
    :cond_1
    sget-object p0, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "; "

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->error:Ljava/lang/String;

    const-string v1, "; "

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->error:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const-string v3, "UNKNOWN"

    if-nez v2, :cond_1

    move-object v2, v3

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->toString()Ljava/lang/String;

    move-result-object v2

    .line 50
    :goto_1
    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    if-nez v4, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->toString()Ljava/lang/String;

    move-result-object v3

    .line 51
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ": "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->name:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->hardwareSerialNumberLast4:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->macAddress:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
