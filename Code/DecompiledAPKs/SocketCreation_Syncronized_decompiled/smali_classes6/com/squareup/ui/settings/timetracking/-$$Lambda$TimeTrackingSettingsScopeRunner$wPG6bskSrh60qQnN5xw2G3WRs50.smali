.class public final synthetic Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;

    invoke-direct {v0}, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;->INSTANCE:Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/permissions/TimeTrackingSettings$State;

    check-cast p2, Lcom/squareup/util/DeviceScreenSizeInfo;

    invoke-static {p1, p2}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->lambda$timeTrackingSettingsScreenData$0(Lcom/squareup/permissions/TimeTrackingSettings$State;Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;

    move-result-object p1

    return-object p1
.end method
