.class public Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;
.super Lcom/squareup/ui/settings/paymentdevices/pairing/InPairingScope;
.source "R12PairingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    .line 37
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/InPairingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 25
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 21
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->pairing_view:I

    return v0
.end method
