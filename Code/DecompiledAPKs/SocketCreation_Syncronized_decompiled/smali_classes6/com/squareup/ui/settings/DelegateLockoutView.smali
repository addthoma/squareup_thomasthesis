.class public Lcom/squareup/ui/settings/DelegateLockoutView;
.super Landroid/widget/LinearLayout;
.source "DelegateLockoutView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const-class p2, Lcom/squareup/ui/settings/DelegateLockoutScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/DelegateLockoutScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/DelegateLockoutScreen$Component;->inject(Lcom/squareup/ui/settings/DelegateLockoutView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 43
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/DelegateLockoutView;->presenter:Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 27
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 29
    sget v0, Lcom/squareup/settingsapplet/R$id;->empty_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    .line 30
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 31
    sget v1, Lcom/squareup/ui/permissions/R$string;->access_denied:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    .line 32
    sget v1, Lcom/squareup/ui/permissions/R$string;->access_denied_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/DelegateLockoutView;->presenter:Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
