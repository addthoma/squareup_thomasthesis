.class Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TaxApplicableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->buildAndBindButtonRow(Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;->this$1:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;->this$1:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES_EXEMPT_ALL:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 206
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;->this$1:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;->this$1:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    invoke-static {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->access$100(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->exemptTaxAll(Ljava/util/List;)V

    return-void
.end method
