.class public abstract Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "SettingsAppletConnectedDevicesListEntry.java"


# instance fields
.field private final manyConnectedId:I

.field private final oneConnectedId:I


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;Lcom/squareup/util/Device;IILcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 21
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 22
    iput p5, p0, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->oneConnectedId:I

    .line 23
    iput p6, p0, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->manyConnectedId:I

    return-void
.end method

.method private static getNumericPattern(IIII)I
    .locals 0

    if-nez p0, :cond_0

    return p1

    :cond_0
    const/4 p1, 0x1

    if-ne p0, p1, :cond_1

    return p2

    :cond_1
    return p3
.end method


# virtual methods
.method protected abstract connectedCount()I
.end method

.method public getShortValueText()Ljava/lang/String;
    .locals 1

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->connectedCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 55
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValueTextForList()Ljava/lang/String;
    .locals 4

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->connectedCount()I

    move-result v0

    .line 30
    iget v1, p0, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->oneConnectedId:I

    iget v2, p0, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->manyConnectedId:I

    const/4 v3, -0x1

    invoke-static {v0, v3, v1, v2}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->getNumericPattern(IIII)I

    move-result v1

    if-ne v1, v3, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 37
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 38
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValueTextForSidebar()Ljava/lang/String;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;->getShortValueText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
