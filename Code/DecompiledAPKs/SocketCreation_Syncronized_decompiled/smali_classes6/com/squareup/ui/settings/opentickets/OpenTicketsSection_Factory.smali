.class public final Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;
.super Ljava/lang/Object;
.source "OpenTicketsSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->ticketsSettingsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->ticketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection_Factory;->get()Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    move-result-object v0

    return-object v0
.end method
