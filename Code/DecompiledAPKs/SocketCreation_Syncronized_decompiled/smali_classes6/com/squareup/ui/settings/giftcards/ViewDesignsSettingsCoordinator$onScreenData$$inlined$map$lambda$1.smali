.class final Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewDesignsSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/giftcards/ScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$items$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/protos/client/giftcards/EGiftTheme;

.field final synthetic $state$inlined:Lcom/squareup/ui/settings/giftcards/ScreenData;

.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/giftcards/EGiftTheme;Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->$it:Lcom/squareup/protos/client/giftcards/EGiftTheme;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->this$0:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->$state$inlined:Lcom/squareup/ui/settings/giftcards/ScreenData;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->this$0:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->access$getRunner$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->$state$inlined:Lcom/squareup/ui/settings/giftcards/ScreenData;

    check-cast v1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;->$it:Lcom/squareup/protos/client/giftcards/EGiftTheme;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;->disableTheme(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/client/giftcards/EGiftTheme;)V

    return-void
.end method
