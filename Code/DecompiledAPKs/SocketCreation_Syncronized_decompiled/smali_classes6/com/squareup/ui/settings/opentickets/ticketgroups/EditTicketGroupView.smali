.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;
.super Landroid/widget/LinearLayout;
.source "EditTicketGroupView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private automaticNameHint:Landroid/widget/TextView;

.field private automaticTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private customTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private deleteButton:Lcom/squareup/ui/ConfirmButton;

.field private final hint:Ljava/lang/String;

.field private nameField:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

.field private ticketNameMethod:Lcom/squareup/widgets/CheckableGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const-class p2, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen$Component;->inject(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/registerlib/R$string;->predefined_tickets_group:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->hint:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->getHintText(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Landroid/widget/TextView;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->automaticNameHint:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    return-object p0
.end method

.method private getHintText(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4

    .line 213
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->hint:Ljava/lang/String;

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_ticket_group_name_hint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " 1\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "name_one"

    .line 216
    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " 2\""

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "name_two"

    .line 217
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 218
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private getNamingMethodByViewId(I)Lcom/squareup/api/items/TicketGroup$NamingMethod;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->automaticTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/api/items/TicketGroup$NamingMethod;->AUTOMATIC_NUMBERING:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/api/items/TicketGroup$NamingMethod;->MANUAL:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    :goto_0
    return-object p1
.end method

.method private hideKeyboardOrAdvanceCursor()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->nameField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/TicketGroup$NamingMethod;->MANUAL:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    .line 197
    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getDraggableItemCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 199
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$2;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->post(Ljava/lang/Runnable;)Z

    .line 205
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 207
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 208
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private isHeaderViewInflated()Z
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->nameField:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isTicketCountViewInflated()Z
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private selectTicketNameMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V
    .locals 1

    .line 176
    sget-object v0, Lcom/squareup/api/items/TicketGroup$NamingMethod;->AUTOMATIC_NUMBERING:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    if-ne p1, v0, :cond_0

    .line 177
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketNameMethod:Lcom/squareup/widgets/CheckableGroup;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->automaticTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    goto :goto_0

    .line 179
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketNameMethod:Lcom/squareup/widgets/CheckableGroup;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->customTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 75
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onDeleteButtonInflated$2$EditTicketGroupView()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onDeleteTicketGroup()V

    return-void
.end method

.method public synthetic lambda$onHeaderViewInflated$0$EditTicketGroupView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    if-eq p3, p2, :cond_0

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->getNamingMethodByViewId(I)Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onNamingMethodChanged(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V

    .line 105
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->hideKeyboardOrAdvanceCursor()V

    return-void
.end method

.method public synthetic lambda$onTicketCountViewInflated$1$EditTicketGroupView(I)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getAutomaticTemplateCount()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onAutomaticTicketCountChanged(I)V

    :cond_0
    return-void
.end method

.method public notifyItemInserted(I)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->notifyItemInserted(I)V

    return-void
.end method

.method public notifyItemRemoved(I)V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->notifyItemRemoved(I)V

    return-void
.end method

.method protected onDeleteButtonInflated(Lcom/squareup/ui/ConfirmButton;)V
    .locals 1

    .line 122
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupView$k5f0mrlpN-fqI85SgE-qt48g7c4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupView$k5f0mrlpN-fqI85SgE-qt48g7c4;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->dropView(Ljava/lang/Object;)V

    .line 71
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 60
    sget v0, Lcom/squareup/settingsapplet/R$id;->predefined_tickets_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 62
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onHeaderViewInflated(Landroid/view/View;)V
    .locals 1

    .line 79
    sget v0, Lcom/squareup/settingsapplet/R$id;->name_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->nameField:Landroid/widget/EditText;

    .line 80
    sget v0, Lcom/squareup/settingsapplet/R$id;->ticket_name_method:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketNameMethod:Lcom/squareup/widgets/CheckableGroup;

    .line 81
    sget v0, Lcom/squareup/settingsapplet/R$id;->automatic_ticket_names:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->automaticTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 82
    sget v0, Lcom/squareup/settingsapplet/R$id;->custom_ticket_names:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->customTicketNames:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 83
    sget v0, Lcom/squareup/settingsapplet/R$id;->automatic_ticket_name_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->automaticNameHint:Landroid/widget/TextView;

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getCurrentGroupName()Ljava/lang/String;

    move-result-object p1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->nameField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->automaticNameHint:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->getHintText(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->nameField:Landroid/widget/EditText;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object p1

    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->selectTicketNameMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketNameMethod:Lcom/squareup/widgets/CheckableGroup;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupView$8CzoJ2BJuziEUiyOQ_rMHuT8cpw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupView$8CzoJ2BJuziEUiyOQ_rMHuT8cpw;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method protected onTicketCountViewInflated(Landroid/view/View;)V
    .locals 2

    .line 110
    sget v0, Lcom/squareup/settingsapplet/R$id;->ticket_count_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/list/EditQuantityRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getMaxTicketGroupSize()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setMaxQuantity(J)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setAllowZero(Z)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getAutomaticTemplateCount()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupView$5KDGyJnpKtYFRHwWctwj9I6cSmQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupView$5KDGyJnpKtYFRHwWctwj9I6cSmQ;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V

    return-void
.end method

.method public setAutomaticTemplateCount(I)V
    .locals 1

    .line 154
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->isTicketCountViewInflated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->ticketCountRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    :cond_0
    return-void
.end method

.method public setContentVisible(Z)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    return-void
.end method

.method public setDeleteButtonVisible(Z)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->setDeleteButtonVisible(Z)V

    return-void
.end method

.method public setDraggableItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate$Builder;",
            ">;)V"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->setDraggableItems(Ljava/util/List;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->isHeaderViewInflated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->nameField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setNamingMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V
    .locals 2

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->isHeaderViewInflated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->selectTicketNameMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->notifyItemChanged(I)V

    .line 140
    :cond_0
    sget-object v0, Lcom/squareup/api/items/TicketGroup$NamingMethod;->AUTOMATIC_NUMBERING:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    if-ne p1, v0, :cond_1

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showAutomaticNameTickets()V

    goto :goto_0

    .line 143
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showCustomNameTickets()V

    :goto_0
    return-void
.end method

.method public setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
