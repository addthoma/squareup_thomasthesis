.class public Lcom/squareup/ui/settings/taxes/tax/TaxState;
.super Ljava/lang/Object;
.source "TaxState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;,
        Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;,
        Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;,
        Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;
    }
.end annotation


# static fields
.field private static final APPLY:Ljava/lang/String;

.field private static final COUNTS:Ljava/lang/String;

.field private static final CURRENT_TAX:Ljava/lang/String;

.field private static final EXEMPT:Ljava/lang/String;

.field private static final GLOBAL_ACTION:Ljava/lang/String;

.field private static final GLOBAL_ACTION_ORIGINAL:Ljava/lang/String;

.field private static final IS_NEW:Ljava/lang/String;

.field private static final ORIGINAL_TAX:Ljava/lang/String;


# instance fields
.field applyItems:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field exemptItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation
.end field

.field private globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

.field private globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

.field private final gson:Lcom/google/gson/Gson;

.field private itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

.field private newTax:Z

.field final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

.field private taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 44
    const-class v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".current"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->CURRENT_TAX:Ljava/lang/String;

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".original"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->ORIGINAL_TAX:Ljava/lang/String;

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".new"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->IS_NEW:Ljava/lang/String;

    .line 47
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".global"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->GLOBAL_ACTION:Ljava/lang/String;

    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".global_original"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->GLOBAL_ACTION_ORIGINAL:Ljava/lang/String;

    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".apply"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->APPLY:Ljava/lang/String;

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".exempt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->EXEMPT:Ljava/lang/String;

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".counts"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->COUNTS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/gson/Gson;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    .line 78
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->gson:Lcom/google/gson/Gson;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/taxes/tax/TaxState;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/taxes/tax/TaxState;)Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/taxes/tax/TaxState;)Lcom/squareup/catalogapi/CatalogIntegrationController;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-object p0
.end method

.method static synthetic access$400(Ljava/util/LinkedHashSet;Ljava/util/Map;Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;
    .locals 0

    .line 42
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getAppliedState(Ljava/util/LinkedHashSet;Ljava/util/Map;Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;

    move-result-object p0

    return-object p0
.end method

.method private buildTax()Lcom/squareup/shared/catalog/models/CatalogTax;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v0

    return-object v0
.end method

.method private static fromSerializable(Ljava/io/Serializable;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Serializable;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 306
    check-cast p0, Ljava/util/Map;

    .line 307
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 309
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 310
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 311
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    .line 312
    :cond_0
    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    move-result-object v2

    .line 311
    :goto_1
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static getAppliedState(Ljava/util/LinkedHashSet;Ljava/util/Map;Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/util/MaybeBoolean;"
        }
    .end annotation

    .line 267
    invoke-virtual {p0, p3}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    if-nez p4, :cond_0

    sget-object p0, Lcom/squareup/util/MaybeBoolean;->TRUE:Lcom/squareup/util/MaybeBoolean;

    return-object p0

    .line 268
    :cond_0
    invoke-interface {p1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    if-eqz p4, :cond_1

    sget-object p0, Lcom/squareup/util/MaybeBoolean;->FALSE:Lcom/squareup/util/MaybeBoolean;

    return-object p0

    .line 271
    :cond_1
    sget-object p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    if-ne p2, p0, :cond_2

    invoke-interface {p1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    if-nez p4, :cond_2

    .line 272
    sget-object p0, Lcom/squareup/util/MaybeBoolean;->TRUE:Lcom/squareup/util/MaybeBoolean;

    return-object p0

    .line 275
    :cond_2
    sget-object p0, Lcom/squareup/util/MaybeBoolean;->UNKNOWN:Lcom/squareup/util/MaybeBoolean;

    return-object p0
.end method

.method static synthetic lambda$setTax$0(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogTax;
    .locals 1

    .line 165
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    invoke-interface {p1, v0, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogTax;

    return-object p0
.end method

.method private setGlobalAction(Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;)V
    .locals 1

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    if-nez v0, :cond_0

    goto :goto_0

    .line 224
    :cond_0
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    if-ne p1, v0, :cond_1

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->allItems()V

    .line 226
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->allServices()V

    :cond_1
    :goto_0
    return-void
.end method

.method private static toSerializable(Ljava/util/Map;)Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;)",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .line 297
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 298
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 299
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    .line 300
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;->toByteArray()[B

    move-result-object v2

    :goto_1
    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/String;Z)V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_0

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->incrementService()V

    goto :goto_0

    .line 236
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->incrementItem()V

    :goto_0
    return-void
.end method

.method applyToAllItems()V
    .locals 1

    .line 212
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->setGlobalAction(Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;)V

    return-void
.end method

.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 203
    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    .line 204
    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 205
    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    const/4 v1, 0x0

    .line 206
    iput-boolean v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax:Z

    .line 207
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->setGlobalAction(Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;)V

    .line 208
    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    return-void
.end method

.method public exempt(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;Z)V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->decrementService()V

    goto :goto_0

    .line 247
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->decrementItem()V

    :goto_0
    return-void
.end method

.method getAppliedState(Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-static {v0, v1, v2, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getAppliedState(Ljava/util/LinkedHashSet;Ljava/util/Map;Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;

    move-result-object p1

    return-object p1
.end method

.method public getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    return-object v0
.end method

.method getItemCounts()Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getTaxItemUpdater()Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    .line 284
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    .line 286
    :cond_1
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;)V

    :goto_1
    return-object v0
.end method

.method hasTaxChanged()Z
    .locals 4

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    if-nez v0, :cond_0

    goto :goto_1

    .line 137
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/models/CatalogTax;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    return v2

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    iget-object v3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    if-eq v0, v3, :cond_3

    return v2

    :cond_3
    return v1

    :cond_4
    :goto_0
    return v2

    :cond_5
    :goto_1
    return v1
.end method

.method hasTaxDataLoaded()Z
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getItemCounts()Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isNewTax()Z
    .locals 1

    .line 195
    iget-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax:Z

    return v0
.end method

.method public synthetic lambda$newTax$3$TaxState(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 189
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    .line 190
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public synthetic lambda$setTax$1$TaxState(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 166
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogTax;

    if-eqz p1, :cond_0

    .line 170
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->builder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    return-void

    .line 168
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempted to load tax that does not exist!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$setTax$2$TaxState(Ljava/lang/Runnable;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 175
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    .line 176
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method newTax(Lcom/squareup/cogs/Cogs;Ljava/lang/Runnable;)V
    .locals 3

    .line 182
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setAppliesToCustomAmounts(Z)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 183
    iput-boolean v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax:Z

    .line 184
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->setGlobalAction(Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;)V

    .line 185
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;->APPLY:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 188
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/ui/settings/taxes/tax/TaxState$1;)V

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$kk7IA46htal1OzqVLf3D9AhR44g;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$kk7IA46htal1OzqVLf3D9AhR44g;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 102
    :cond_0
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->CURRENT_TAX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->CURRENT_TAX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->builder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 104
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->ORIGINAL_TAX:Ljava/lang/String;

    .line 105
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->builder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 106
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->IS_NEW:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax:Z

    .line 107
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->GLOBAL_ACTION:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 108
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->GLOBAL_ACTION_ORIGINAL:Ljava/lang/String;

    .line 109
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 110
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->APPLY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    .line 111
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->EXEMPT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->fromSerializable(Ljava/io/Serializable;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->gson:Lcom/google/gson/Gson;

    sget-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->COUNTS:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-class v1, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->CURRENT_TAX:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->buildTax()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 119
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->ORIGINAL_TAX:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 120
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->IS_NEW:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 121
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->GLOBAL_ACTION:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 122
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->GLOBAL_ACTION_ORIGINAL:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 123
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->APPLY:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 124
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->EXEMPT:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->toSerializable(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 125
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->COUNTS:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->gson:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->itemCounts:Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method saveCopyOfTaxState()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilder:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogTax;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->taxBuilderOriginal:Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    :cond_0
    return-void
.end method

.method setTax(Ljava/lang/String;Lcom/squareup/cogs/Cogs;Ljava/lang/Runnable;)V
    .locals 3

    const/4 v0, 0x0

    .line 161
    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax:Z

    const/4 v0, 0x0

    .line 162
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->setGlobalAction(Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;)V

    .line 163
    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->globalActionOriginal:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 165
    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$3D9kAO7mL51eDIJ4F8frxlfkJME;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$3D9kAO7mL51eDIJ4F8frxlfkJME;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$-Ww6KA4mFW2HNgUiXalCmqEGZ-c;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$-Ww6KA4mFW2HNgUiXalCmqEGZ-c;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;)V

    invoke-interface {p2, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 174
    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {v1, v2, p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/ui/settings/taxes/tax/TaxState$1;)V

    new-instance p1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$SWh40BL1WCSxeyc08tFi2SaAJqg;

    invoke-direct {p1, p0, p3}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxState$SWh40BL1WCSxeyc08tFi2SaAJqg;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;Ljava/lang/Runnable;)V

    invoke-interface {p2, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method
