.class Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CardReadersScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 42
    sget v1, Lcom/squareup/settingsapplet/R$id;->learn_more_r12:I

    if-ne v0, v1, :cond_0

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->onLearnMoreClicked(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    goto :goto_0

    .line 44
    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$id;->learn_more_magstripe:I

    if-ne v0, v1, :cond_1

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->onLearnMoreClicked(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    :goto_0
    return-void

    .line 47
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view with id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
