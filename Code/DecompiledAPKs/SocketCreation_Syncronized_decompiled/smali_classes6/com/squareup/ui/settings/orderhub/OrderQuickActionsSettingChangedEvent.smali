.class public final Lcom/squareup/ui/settings/orderhub/OrderQuickActionsSettingChangedEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "OrderHubSettingsEvents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderQuickActionsSettingChangedEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "enabled",
        "",
        "(Z)V",
        "getEnabled",
        "()Z",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->ORDER_HUB_CHANGED_QUICK_ACTIONS_SETTINGS:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    iput-boolean p1, p0, Lcom/squareup/ui/settings/orderhub/OrderQuickActionsSettingChangedEvent;->enabled:Z

    return-void
.end method


# virtual methods
.method public final getEnabled()Z
    .locals 1

    .line 38
    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderQuickActionsSettingChangedEvent;->enabled:Z

    return v0
.end method
