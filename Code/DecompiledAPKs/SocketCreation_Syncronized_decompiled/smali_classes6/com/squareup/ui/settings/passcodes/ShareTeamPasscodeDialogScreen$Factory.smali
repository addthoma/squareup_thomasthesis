.class public Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ShareTeamPasscodeDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lflow/Flow;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 51
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-virtual {p0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 39
    const-class v0, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;->res()Lcom/squareup/util/Res;

    move-result-object v0

    .line 40
    const-class v1, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;

    invoke-interface {v1}, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;->flow()Lflow/Flow;

    move-result-object v1

    .line 41
    const-class v2, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;

    invoke-static {p1, v2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;

    .line 42
    invoke-interface {v2}, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen$Component;->settingsAppletEntryPoint()Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    move-result-object v2

    .line 44
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_share_team_passcode_dialog_title:I

    .line 45
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_share_team_passcode_dialog_message:I

    .line 46
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const/16 v4, 0x1e

    .line 47
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "num_days"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 48
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 46
    invoke-virtual {p1, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_share_team_passcode_dialog_button_text:I

    .line 50
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$ShareTeamPasscodeDialogScreen$Factory$B3bU9LgDCPMbDRSSC5U5JsmDrXc;

    invoke-direct {v3, v1, v2}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$ShareTeamPasscodeDialogScreen$Factory$B3bU9LgDCPMbDRSSC5U5JsmDrXc;-><init>(Lflow/Flow;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;)V

    .line 49
    invoke-virtual {p1, v0, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 44
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
