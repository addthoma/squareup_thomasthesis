.class public Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "EnableSignatureOptionEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;
    }
.end annotation


# instance fields
.field public final signature_skip:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;)V
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SKIP_SIG_SETTING:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 11
    invoke-virtual {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;->signature_skip:Ljava/lang/String;

    return-void
.end method
