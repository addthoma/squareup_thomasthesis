.class public final Lcom/squareup/ui/settings/tiles/TileAppearanceSection$Access;
.super Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;
.source "TileAppearanceSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private final tileSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 45
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;-><init>([Lcom/squareup/permissions/Permission;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$Access;->tileSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$Access;->tileSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileAllowed()Z

    move-result v0

    return v0
.end method
