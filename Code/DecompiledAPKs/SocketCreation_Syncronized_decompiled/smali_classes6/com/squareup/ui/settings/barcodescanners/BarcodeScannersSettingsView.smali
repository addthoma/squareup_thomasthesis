.class public Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;
.super Landroid/widget/LinearLayout;
.source "BarcodeScannersSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private adapter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;

.field presenter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 48
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;->presenter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    new-instance v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;

    invoke-direct {v0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;->adapter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;

    .line 33
    sget v0, Lcom/squareup/settingsapplet/R$id;->barcode_scanners_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 34
    iget-object v1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;->adapter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;->presenter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method updateView(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScanner;",
            ">;)V"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;->adapter:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->setBarcodeScannersList(Ljava/util/List;)V

    return-void
.end method
