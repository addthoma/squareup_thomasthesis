.class public final Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;
.super Ljava/lang/Object;
.source "DeviceSection_ListEntry_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->deviceNameProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;-><init>(Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/devicename/DeviceSection;

    iget-object v1, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->deviceNameProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->newInstance(Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceSection_ListEntry_Factory;->get()Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

    move-result-object v0

    return-object v0
.end method
