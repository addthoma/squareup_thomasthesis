.class public final enum Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;
.super Ljava/lang/Enum;
.source "TutorialPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonTap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

.field public static final enum PRIMARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

.field public static final enum SECONDARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 50
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    const/4 v1, 0x0

    const-string v2, "PRIMARY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->PRIMARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    const/4 v2, 0x1

    const-string v3, "SECONDARY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->SECONDARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    .line 49
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->PRIMARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->SECONDARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    return-object v0
.end method
