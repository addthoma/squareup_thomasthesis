.class public Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CreateTeamPasscodeDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Factory;,
        Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$Component;,
        Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateTeamPasscodeDialogScreen$erPPecI7aiNHYjXiGVFqwvROj1Q;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateTeamPasscodeDialogScreen$erPPecI7aiNHYjXiGVFqwvROj1Q;

    .line 56
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;
    .locals 0

    .line 56
    new-instance p0, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;-><init>()V

    return-object p0
.end method
