.class public final Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositSettingsCoordinator.kt\ncom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,495:1\n1429#2,3:496\n1429#2,3:499\n*E\n*S KotlinDebug\n*F\n+ 1 DepositSettingsCoordinator.kt\ncom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator\n*L\n259#1,3:496\n260#1,3:499\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020\u0015H\u0016J\u0010\u0010<\u001a\u00020:2\u0006\u0010;\u001a\u00020\u0015H\u0002J\u0008\u0010=\u001a\u00020:H\u0002J\u0018\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020CH\u0002J\u0010\u0010D\u001a\u00020?2\u0006\u0010@\u001a\u00020AH\u0002J\u0010\u0010E\u001a\u00020:2\u0006\u0010F\u001a\u00020GH\u0002J\u0010\u0010H\u001a\u00020:2\u0006\u0010I\u001a\u00020JH\u0002J\u0010\u0010K\u001a\u00020:2\u0006\u0010I\u001a\u00020JH\u0002J\u0010\u0010L\u001a\u00020:2\u0006\u0010I\u001a\u00020JH\u0002J\u0010\u0010M\u001a\u00020:2\u0006\u0010I\u001a\u00020JH\u0002J\u0008\u0010N\u001a\u00020:H\u0002J\u0008\u0010O\u001a\u00020:H\u0002J\u0010\u0010P\u001a\u00020:2\u0006\u0010I\u001a\u00020JH\u0002J\u0010\u0010Q\u001a\u00020:2\u0006\u0010I\u001a\u00020JH\u0002JN\u0010R\u001a\u00020:2\u0010\u0008\u0002\u0010S\u001a\n\u0012\u0004\u0012\u00020U\u0018\u00010T2\u0010\u0008\u0002\u0010V\u001a\n\u0012\u0004\u0012\u00020U\u0018\u00010T2\u0008\u0008\u0001\u0010W\u001a\u00020X2\u0006\u0010Y\u001a\u00020Z2\u0006\u0010[\u001a\u00020G2\u0006\u0010\\\u001a\u00020GH\u0002J\u0010\u0010]\u001a\u00020:2\u0006\u0010^\u001a\u00020GH\u0002J\u0010\u0010_\u001a\u00020:2\u0006\u0010`\u001a\u00020aH\u0002J\u0010\u0010b\u001a\u00020:2\u0006\u0010c\u001a\u00020dH\u0002J$\u0010e\u001a\u00020:2\u000c\u0010f\u001a\u0008\u0012\u0004\u0012\u00020?0g2\u000c\u0010h\u001a\u0008\u0012\u0004\u0012\u00020?0gH\u0002J(\u0010i\u001a\u00020:2\u000c\u0010j\u001a\u0008\u0012\u0004\u0012\u00020?0T2\u0008\u0008\u0001\u0010k\u001a\u00020X2\u0006\u0010l\u001a\u00020mH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u000202X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u000202X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u000205X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000208X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006n"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
        "device",
        "Lcom/squareup/util/Device;",
        "starter",
        "Lcom/squareup/onboarding/OnboardingStarter;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Device;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/util/Clock;)V",
        "accountErrorMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addMoneyChangeAccountButton",
        "Landroid/widget/Button;",
        "addMoneyInstrument",
        "Lcom/squareup/noho/NohoRow;",
        "addMoneyResendEmailButton",
        "addMoneySection",
        "Landroid/view/View;",
        "depositSchedule",
        "Landroid/view/ViewGroup;",
        "depositScheduleAutomatic",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "depositScheduleCheckableGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "depositScheduleHint",
        "Lcom/squareup/widgets/MessageView;",
        "depositScheduleManual",
        "depositScheduleRows",
        "depositScheduleTitle",
        "Landroid/widget/TextView;",
        "depositSpeedCheckableGroup",
        "depositSpeedCustom",
        "depositSpeedHint",
        "depositSpeedOneToTwoBusDays",
        "depositSpeedSameDay",
        "depositSpeedTitle",
        "instantTransfersChangeAccountButton",
        "instantTransfersDivider",
        "instantTransfersHint",
        "Lcom/squareup/noho/NohoLabel;",
        "instantTransfersInstrument",
        "instantTransfersResendEmailButton",
        "instantTransfersSection",
        "instantTransfersSetUpButton",
        "instantTransfersToggle",
        "orangeSpan",
        "Landroid/text/style/ForegroundColorSpan;",
        "redSpan",
        "res",
        "Landroid/content/res/Resources;",
        "rootView",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "attach",
        "",
        "view",
        "bindViews",
        "configureActionBar",
        "getCutoffTime",
        "",
        "dayOfWeekDepositSettings",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
        "timeZone",
        "Ljava/util/TimeZone;",
        "getDepositType",
        "instantDepositsRowCheckChanged",
        "checked",
        "",
        "onBankSettingsState",
        "screenData",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;",
        "onDepositScheduleState",
        "onFailed",
        "onHasBankAccount",
        "onLoading",
        "onLoadingBankAccount",
        "onScreenData",
        "onSucceeded",
        "setAccountErrorMessage",
        "title",
        "Lcom/squareup/resources/TextModel;",
        "",
        "message",
        "buttonResId",
        "",
        "buttonOnClickListener",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "showButton",
        "showInstantTransfersSection",
        "setAutomaticDeposits",
        "enabled",
        "setChevronVisibility",
        "accessoryType",
        "Lcom/squareup/noho/AccessoryType;",
        "setDepositSpeed",
        "depositSpeed",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;",
        "updateDepositScheduleRows",
        "depositTypes",
        "",
        "cutoffTimes",
        "updateLinkedCardText",
        "cardText",
        "cardDrawable",
        "cardStatus",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addMoneyChangeAccountButton:Landroid/widget/Button;

.field private addMoneyInstrument:Lcom/squareup/noho/NohoRow;

.field private addMoneyResendEmailButton:Landroid/widget/Button;

.field private addMoneySection:Landroid/view/View;

.field private final clock:Lcom/squareup/util/Clock;

.field private depositSchedule:Landroid/view/ViewGroup;

.field private depositScheduleAutomatic:Lcom/squareup/noho/NohoCheckableRow;

.field private depositScheduleCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private depositScheduleHint:Lcom/squareup/widgets/MessageView;

.field private depositScheduleManual:Lcom/squareup/noho/NohoCheckableRow;

.field private depositScheduleRows:Landroid/view/ViewGroup;

.field private depositScheduleTitle:Landroid/widget/TextView;

.field private depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private depositSpeedCustom:Lcom/squareup/noho/NohoCheckableRow;

.field private depositSpeedHint:Lcom/squareup/widgets/MessageView;

.field private depositSpeedOneToTwoBusDays:Lcom/squareup/noho/NohoCheckableRow;

.field private depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

.field private depositSpeedTitle:Landroid/widget/TextView;

.field private final device:Lcom/squareup/util/Device;

.field private instantTransfersChangeAccountButton:Landroid/widget/Button;

.field private instantTransfersDivider:Landroid/view/View;

.field private instantTransfersHint:Lcom/squareup/noho/NohoLabel;

.field private instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

.field private instantTransfersResendEmailButton:Landroid/widget/Button;

.field private instantTransfersSection:Landroid/view/View;

.field private instantTransfersSetUpButton:Landroid/widget/Button;

.field private instantTransfersToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private orangeSpan:Landroid/text/style/ForegroundColorSpan;

.field private redSpan:Landroid/text/style/ForegroundColorSpan;

.field private res:Landroid/content/res/Resources;

.field private rootView:Landroid/view/View;

.field private final scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

.field private spinner:Landroid/widget/ProgressBar;

.field private final starter:Lcom/squareup/onboarding/OnboardingStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Device;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "starter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    iput-object p4, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static final synthetic access$getDepositSpeedCustom$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)Lcom/squareup/noho/NohoCheckableRow;
    .locals 1

    .line 79
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCustom:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p0, :cond_0

    const-string v0, "depositSpeedCustom"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getStarter$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)Lcom/squareup/onboarding/OnboardingStarter;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    return-object p0
.end method

.method public static final synthetic access$instantDepositsRowCheckChanged(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Z)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantDepositsRowCheckChanged(Z)V

    return-void
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V

    return-void
.end method

.method public static final synthetic access$setDepositSpeedCustom$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCustom:Lcom/squareup/noho/NohoCheckableRow;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 3

    .line 464
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 465
    sget v0, Lcom/squareup/settingsapplet/R$id;->spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->spinner:Landroid/widget/ProgressBar;

    .line 466
    sget v0, Lcom/squareup/settingsapplet/R$id;->add_money_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->addMoneySection:Landroid/view/View;

    .line 467
    sget v0, Lcom/squareup/settingsapplet/R$id;->add_money_instrument:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->addMoneyInstrument:Lcom/squareup/noho/NohoRow;

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->addMoneySection:Landroid/view/View;

    const-string v1, "addMoneySection"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/settingsapplet/R$id;->resend_email_button:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->addMoneyResendEmailButton:Landroid/widget/Button;

    .line 469
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->addMoneySection:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/settingsapplet/R$id;->change_account_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->addMoneyChangeAccountButton:Landroid/widget/Button;

    .line 470
    sget v0, Lcom/squareup/settingsapplet/R$id;->instant_transfers_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSection:Landroid/view/View;

    .line 471
    sget v0, Lcom/squareup/settingsapplet/R$id;->instant_transfers_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersHint:Lcom/squareup/noho/NohoLabel;

    .line 472
    sget v0, Lcom/squareup/settingsapplet/R$id;->instant_transfers_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 473
    sget v0, Lcom/squareup/settingsapplet/R$id;->instant_transfers_instrument:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    .line 474
    sget v0, Lcom/squareup/settingsapplet/R$id;->instant_transfers_divider:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersDivider:Landroid/view/View;

    .line 475
    sget v0, Lcom/squareup/settingsapplet/R$id;->instant_transfers_set_up_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSetUpButton:Landroid/widget/Button;

    .line 476
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSection:Landroid/view/View;

    const-string v1, "instantTransfersSection"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v2, Lcom/squareup/settingsapplet/R$id;->resend_email_button:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersResendEmailButton:Landroid/widget/Button;

    .line 478
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSection:Landroid/view/View;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v1, Lcom/squareup/settingsapplet/R$id;->change_account_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersChangeAccountButton:Landroid/widget/Button;

    .line 479
    sget v0, Lcom/squareup/settingsapplet/R$id;->account_error_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    .line 480
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    .line 481
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_rows:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleRows:Landroid/view/ViewGroup;

    .line 482
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSchedule:Landroid/view/ViewGroup;

    .line 483
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleTitle:Landroid/widget/TextView;

    .line 484
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_checkable_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 485
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_automatic:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleAutomatic:Lcom/squareup/noho/NohoCheckableRow;

    .line 486
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_manual:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleManual:Lcom/squareup/noho/NohoCheckableRow;

    .line 487
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedTitle:Landroid/widget/TextView;

    .line 488
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_checkable_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 489
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_one_to_two_business_days:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedOneToTwoBusDays:Lcom/squareup/noho/NohoCheckableRow;

    .line 490
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_same_day:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

    .line 491
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_custom:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCustom:Lcom/squareup/noho/NohoCheckableRow;

    .line 492
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedHint:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final configureActionBar()V
    .locals 4

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 453
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 454
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/common/strings/R$string;->instant_deposits_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 456
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 457
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 460
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final getCutoffTime(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;Ljava/util/TimeZone;)Ljava/lang/CharSequence;
    .locals 7

    .line 293
    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    iget-object p1, p1, Lcom/squareup/protos/common/time/DayTime;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 297
    invoke-static {}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt;->getNO_DAY_OF_WEEK()Ljava/lang/Integer;

    move-result-object v3

    .line 298
    iget-object v0, p1, Lcom/squareup/protos/common/time/LocalTime;->hour_of_day:Ljava/lang/Integer;

    const-string v2, "localTime.hour_of_day"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 299
    iget-object p1, p1, Lcom/squareup/protos/common/time/LocalTime;->minute_of_hour:Ljava/lang/Integer;

    const-string v0, "localTime.minute_of_hour"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const-string v6, "h:mm a"

    move-object v2, p2

    .line 294
    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Times;->currentTimeZoneRepresentation(Ljava/util/TimeZone;Ljava/util/TimeZone;Ljava/lang/Integer;IILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 303
    iget-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez p2, :cond_0

    const-string v0, "res"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$string;->instant_deposits_deposit_schedule_close_of_day:I

    invoke-static {p2, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 304
    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "cutoff_time"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 305
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(res, R.strin\u2026ffTime)\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getDepositType(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)Ljava/lang/CharSequence;
    .locals 1

    .line 282
    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;->same_day_settlement_enabled:Ljava/lang/Boolean;

    const-string v0, "dayOfWeekDepositSettings\u2026me_day_settlement_enabled"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string v0, "res"

    if-eqz p1, :cond_1

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v0, Lcom/squareup/registerlib/R$string;->deposit_speed_same_day:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "res.getString(com.square\u2026g.deposit_speed_same_day)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 285
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v0, Lcom/squareup/settingsapplet/R$string;->deposit_speed_one_to_two_bus_days:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "res.getString(R.string.d\u2026peed_one_to_two_bus_days)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    :goto_0
    return-object p1
.end method

.method private final instantDepositsRowCheckChanged(Z)V
    .locals 2

    .line 448
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->logInstantDepositToggled(Z)V

    .line 449
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_0

    const-string v1, "instantTransfersToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->saveSettings(Z)V

    return-void
.end method

.method private final onBankSettingsState(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 9

    .line 202
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getBankSettingsState()Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 213
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getBankSettingsState()Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 215
    :cond_0
    new-instance v0, Lcom/squareup/resources/ResourceString;

    sget v1, Lcom/squareup/settingsapplet/R$string;->load_deposits_error_title:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 216
    new-instance v0, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/resources/TextModel;

    .line 217
    sget v5, Lcom/squareup/common/strings/R$string;->retry:I

    .line 218
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onBankSettingsState$2;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onBankSettingsState$2;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v6

    const-string v0, "debounceRunnable(scopeRunner::onRefresh)"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result v7

    const/4 v8, 0x0

    move-object v2, p0

    .line 214
    invoke-direct/range {v2 .. v8}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setAccountErrorMessage(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZ)V

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    const/4 v1, 0x0

    .line 206
    new-instance p1, Lcom/squareup/resources/ResourceString;

    sget v0, Lcom/squareup/settingsapplet/R$string;->no_linked_bank_account_error_message:I

    invoke-direct {p1, v0}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 207
    sget v3, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    .line 208
    new-instance p1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onBankSettingsState$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onBankSettingsState$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v4

    const-string p1, "debounce { starter.start\u2026oardingParams(RESTART)) }"

    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    .line 205
    invoke-static/range {v0 .. v8}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setAccountErrorMessage$default(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZILjava/lang/Object;)V

    goto :goto_0

    .line 204
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onHasBankAccount(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V

    goto :goto_0

    .line 203
    :cond_4
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onLoadingBankAccount()V

    :goto_0
    return-void
.end method

.method private final onDepositScheduleState(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 2

    .line 226
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getDepositScheduleState()Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    sget-object v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onSucceeded(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 228
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onFailed(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V

    goto :goto_0

    .line 227
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onLoading()V

    :goto_0
    return-void
.end method

.method private final onFailed(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 12

    .line 245
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getDepositScheduleState()Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 247
    :cond_0
    new-instance v1, Lcom/squareup/resources/FixedText;

    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    move-object v4, v1

    check-cast v4, Lcom/squareup/resources/TextModel;

    const/4 v5, 0x0

    .line 248
    sget v6, Lcom/squareup/common/strings/R$string;->retry:I

    .line 249
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onFailed$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onFailed$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v7

    const-string v1, "debounceRunnable(scopeRu\u2026::refreshDepositSchedule)"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result v8

    .line 251
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersSection()Z

    move-result v9

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v3, p0

    .line 246
    invoke-static/range {v3 .. v11}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setAccountErrorMessage$default(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZILjava/lang/Object;)V

    return-void
.end method

.method private final onHasBankAccount(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 6

    .line 416
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowDepositSchedule()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersToggle:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "instantTransfersToggle"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onHasBankAccount$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onHasBankAccount$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getInstantDepositAllowed()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersHint:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_3

    const-string v2, "instantTransfersHint"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getInstantTransfersHint()Lcom/squareup/resources/TextModel;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    const-string v4, "rootView"

    if-nez v3, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "rootView.context"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_5

    const-string v2, "depositSpeedSameDay"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getSameDayDepositFee()Lcom/squareup/resources/TextModel;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    if-nez v3, :cond_6

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    .line 426
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersInstrument()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 428
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getLinkedCardText()Lcom/squareup/resources/TextModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getCardDrawable()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getCardStatus()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    move-result-object v3

    if-nez v3, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 427
    :cond_7
    invoke-direct {p0, v0, v2, v3}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->updateLinkedCardText(Lcom/squareup/resources/TextModel;ILcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)V

    .line 432
    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSetUpButton:Landroid/widget/Button;

    if-nez v0, :cond_9

    const-string v2, "instantTransfersSetUpButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersSetUpButton()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersToggle()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 434
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_b

    const-string v1, "instantTransfersInstrument"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersInstrument()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 435
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersDivider:Landroid/view/View;

    if-nez v0, :cond_c

    const-string v1, "instantTransfersDivider"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 436
    :cond_c
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersToggle()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersInstrument()Z

    move-result v1

    if-eqz v1, :cond_d

    goto :goto_0

    :cond_d
    const/4 v1, 0x0

    goto :goto_1

    :cond_e
    :goto_0
    const/4 v1, 0x1

    .line 435
    :goto_1
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersChangeAccountButton:Landroid/widget/Button;

    if-nez v0, :cond_f

    const-string v1, "instantTransfersChangeAccountButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersInstrument()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSection:Landroid/view/View;

    if-nez v0, :cond_10

    const-string v1, "instantTransfersSection"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowInstantTransfersSection()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 441
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowDepositSchedule()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 442
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onDepositScheduleState(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V

    .line 444
    :cond_11
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSchedule:Landroid/view/ViewGroup;

    if-nez v0, :cond_12

    const-string v1, "depositSchedule"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowDepositSchedule()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final onLoading()V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_0

    const-string v1, "accountErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleTitle:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "depositScheduleTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_2

    const-string v1, "depositScheduleCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedTitle:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "depositSpeedTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_4

    const-string v1, "depositSpeedCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    const-string v1, "depositSpeedHint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleRows:Landroid/view/ViewGroup;

    if-nez v0, :cond_6

    const-string v1, "depositScheduleRows"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_7

    const-string v1, "depositScheduleHint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_8

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final onLoadingBankAccount()V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_0

    const-string v1, "accountErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSection:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v1, "instantTransfersSection"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSchedule:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    const-string v1, "depositSchedule"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_3

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 10

    .line 187
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getHasActivatedAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onBankSettingsState(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    .line 193
    new-instance p1, Lcom/squareup/resources/ResourceString;

    sget v0, Lcom/squareup/settingsapplet/R$string;->not_activated_account_error_message:I

    invoke-direct {p1, v0}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v3, p1

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 194
    sget v4, Lcom/squareup/registerlib/R$string;->activate_account:I

    .line 195
    new-instance p1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onScreenData$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onScreenData$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v5

    const-string p1, "debounce { starter.start\u2026oardingParams(RESTART)) }"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v1, p0

    .line 192
    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setAccountErrorMessage$default(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZILjava/lang/Object;)V

    return-void
.end method

.method private final onSucceeded(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
    .locals 7

    .line 256
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getDepositScheduleState()Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v0

    .line 257
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 258
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    .line 259
    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeekDepositSettings()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 496
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 497
    move-object v5, v1

    check-cast v5, Ljava/util/Collection;

    check-cast v4, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-object v6, p0

    check-cast v6, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    .line 259
    invoke-direct {v6, v4}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->getDepositType(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 498
    :cond_0
    move-object v3, v1

    check-cast v3, Ljava/util/Collection;

    .line 260
    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeekDepositSettings()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 499
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 500
    move-object v5, v2

    check-cast v5, Ljava/util/Collection;

    check-cast v4, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    .line 260
    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->timeZone()Ljava/util/TimeZone;

    move-result-object v6

    invoke-direct {p0, v4, v6}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->getCutoffTime(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;Ljava/util/TimeZone;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 501
    :cond_1
    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    .line 262
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowAutomaticDeposits()Z

    move-result v3

    .line 263
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getShowDepositSpeed()Z

    move-result v4

    .line 264
    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->pausedSettlement()Z

    move-result v5

    xor-int/lit8 v6, v5, 0x1

    .line 265
    invoke-direct {p0, v6}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setAutomaticDeposits(Z)V

    .line 266
    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setDepositSpeed(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)V

    .line 267
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->updateDepositScheduleRows(Ljava/util/List;Ljava/util/List;)V

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    const-string v1, "depositScheduleHint"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->getDepositScheduleHint()Lcom/squareup/resources/TextModel;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    if-nez v2, :cond_3

    const-string v6, "rootView"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v6, "rootView.context"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez p1, :cond_4

    const-string v0, "spinner"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 271
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_5

    const-string v0, "accountErrorMessage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 272
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleTitle:Landroid/widget/TextView;

    if-nez p1, :cond_6

    const-string v0, "depositScheduleTitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 273
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_7

    const-string v0, "depositScheduleCheckableGroup"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 274
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedTitle:Landroid/widget/TextView;

    if-nez p1, :cond_8

    const-string v0, "depositSpeedTitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast p1, Landroid/view/View;

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-eqz v4, :cond_9

    if-eqz v3, :cond_9

    if-nez v5, :cond_9

    const/4 v3, 0x1

    goto :goto_2

    :cond_9
    const/4 v3, 0x0

    :goto_2
    invoke-static {p1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 275
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_a

    const-string v3, "depositSpeedCheckableGroup"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast p1, Landroid/view/View;

    if-eqz v4, :cond_b

    if-nez v5, :cond_b

    const/4 v3, 0x1

    goto :goto_3

    :cond_b
    const/4 v3, 0x0

    :goto_3
    invoke-static {p1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 276
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedHint:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_c

    const-string v3, "depositSpeedHint"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast p1, Landroid/view/View;

    if-eqz v4, :cond_d

    if-nez v5, :cond_d

    const/4 v0, 0x1

    :cond_d
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleRows:Landroid/view/ViewGroup;

    if-nez p1, :cond_e

    const-string v0, "depositScheduleRows"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast p1, Landroid/view/View;

    xor-int/lit8 v0, v5, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 278
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast p1, Landroid/view/View;

    xor-int/lit8 v0, v5, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final setAccountErrorMessage(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/squareup/debounce/DebouncedOnClickListener;",
            "ZZ)V"
        }
    .end annotation

    .line 366
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "accountErrorMessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const-string v2, "rootView"

    const-string v3, "rootView.context"

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    iget-object v5, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    if-nez v5, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v5}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object p1, v4

    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 367
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    if-eqz p2, :cond_5

    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    if-nez v0, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p2

    move-object v4, p2

    check-cast v4, Ljava/lang/String;

    :cond_5
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 368
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 369
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    if-eqz p5, :cond_9

    .line 372
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->showSecondaryButton()V

    goto :goto_1

    .line 374
    :cond_9
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->hideSecondaryButton()V

    .line 377
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez p1, :cond_b

    const-string p2, "spinner"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 378
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSection:Landroid/view/View;

    if-nez p1, :cond_c

    const-string p2, "instantTransfersSection"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-static {p1, p6}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 379
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSchedule:Landroid/view/ViewGroup;

    if-nez p1, :cond_d

    const-string p2, "depositSchedule"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 380
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method static synthetic setAccountErrorMessage$default(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZILjava/lang/Object;)V
    .locals 9

    and-int/lit8 v0, p7, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 359
    move-object v0, v1

    check-cast v0, Lcom/squareup/resources/TextModel;

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p1

    :goto_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_1

    .line 360
    move-object v0, v1

    check-cast v0, Lcom/squareup/resources/TextModel;

    move-object v4, v0

    goto :goto_1

    :cond_1
    move-object v4, p2

    :goto_1
    move-object v2, p0

    move v5, p3

    move-object v6, p4

    move v7, p5

    move v8, p6

    invoke-direct/range {v2 .. v8}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setAccountErrorMessage(Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;ILcom/squareup/debounce/DebouncedOnClickListener;ZZ)V

    return-void
.end method

.method private final setAutomaticDeposits(Z)V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "depositScheduleCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    .line 310
    sget p1, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_automatic:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_manual:I

    .line 309
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    return-void
.end method

.method private final setChevronVisibility(Lcom/squareup/noho/AccessoryType;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_3

    .line 334
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleRows:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    const-string v3, "depositScheduleRows"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Lcom/squareup/noho/NohoRow;

    .line 335
    invoke-virtual {v2, p1}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 336
    sget-object v3, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    if-ne p1, v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoRow;->setClickable(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 334
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.noho.NohoRow"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    return-void
.end method

.method private final setDepositSpeed(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)V
    .locals 2

    .line 314
    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_6

    const/4 v0, 0x2

    const-string v1, "depositSpeedCheckableGroup"

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 327
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_custom:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 328
    sget-object p1, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setChevronVisibility(Lcom/squareup/noho/AccessoryType;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 323
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_same_day:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 324
    sget-object p1, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setChevronVisibility(Lcom/squareup/noho/AccessoryType;)V

    goto :goto_0

    .line 319
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_one_to_two_business_days:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 320
    sget-object p1, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->setChevronVisibility(Lcom/squareup/noho/AccessoryType;)V

    :cond_6
    :goto_0
    return-void
.end method

.method private final updateDepositScheduleRows(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_2

    .line 345
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleRows:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const-string v2, "depositScheduleRows"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Lcom/squareup/noho/NohoRow;

    .line 346
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 347
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 345
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.noho.NohoRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-void
.end method

.method private final updateLinkedCardText(Lcom/squareup/resources/TextModel;ILcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;I",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
            ")V"
        }
    .end annotation

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    const-string v1, "instantTransfersInstrument"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    if-nez v2, :cond_1

    const-string v3, "rootView"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "rootView.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 389
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    invoke-direct {v0, p2}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 391
    sget-object p1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p3}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    const-string p3, "instantTransfersResendEmailButton"

    if-eq p1, p2, :cond_11

    const/4 p2, 0x2

    const-string v0, "redSpan"

    const-string v2, "res"

    if-eq p1, p2, :cond_c

    const/4 p2, 0x3

    if-eq p1, p2, :cond_7

    .line 409
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 408
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez p2, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v1, Lcom/squareup/settingsapplet/R$string;->instant_deposits_card_failed:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "res.getText(R.string.instant_deposits_card_failed)"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->redSpan:Landroid/text/style/ForegroundColorSpan;

    if-nez v1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v1, Landroid/text/style/CharacterStyle;

    invoke-static {p2, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 410
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersResendEmailButton:Landroid/widget/Button;

    if-nez p1, :cond_6

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto/16 :goto_0

    .line 404
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 403
    :cond_8
    iget-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez p2, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    sget v0, Lcom/squareup/settingsapplet/R$string;->instant_deposits_pending_verification:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "res.getText(R.string.ins\u2026its_pending_verification)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->orangeSpan:Landroid/text/style/ForegroundColorSpan;

    if-nez v0, :cond_a

    const-string v1, "orangeSpan"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v0, Landroid/text/style/CharacterStyle;

    invoke-static {p2, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 405
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersResendEmailButton:Landroid/widget/Button;

    if-nez p1, :cond_b

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 398
    :cond_c
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 397
    :cond_d
    iget-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez p2, :cond_e

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    sget v1, Lcom/squareup/settingsapplet/R$string;->instant_deposits_link_expired:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "res.getText(R.string.ins\u2026nt_deposits_link_expired)"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 398
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->redSpan:Landroid/text/style/ForegroundColorSpan;

    if-nez v1, :cond_f

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast v1, Landroid/text/style/CharacterStyle;

    invoke-static {p2, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 399
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersResendEmailButton:Landroid/widget/Button;

    if-nez p1, :cond_10

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 393
    :cond_11
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersInstrument:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    const-string p2, ""

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 394
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersResendEmailButton:Landroid/widget/Button;

    if-nez p1, :cond_13

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 10

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->bindViews(Landroid/view/View;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->rootView:Landroid/view/View;

    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    .line 127
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->configureActionBar()V

    .line 129
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$color;->card_expired_or_failed:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->redSpan:Landroid/text/style/ForegroundColorSpan;

    .line 130
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$color;->card_pending:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->orangeSpan:Landroid/text/style/ForegroundColorSpan;

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersSetUpButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "instantTransfersSetUpButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 133
    :cond_0
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 132
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersChangeAccountButton:Landroid/widget/Button;

    if-nez v0, :cond_1

    const-string v1, "instantTransfersChangeAccountButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 137
    :cond_1
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$2;

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 136
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->instantTransfersResendEmailButton:Landroid/widget/Button;

    if-nez v0, :cond_2

    const-string v1, "instantTransfersResendEmailButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 141
    :cond_2
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$3;

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$3;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 140
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->accountErrorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_3

    const-string v1, "accountErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v1, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonType(Lcom/squareup/noho/NohoButtonType;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_4

    const-string v1, "depositScheduleCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$4;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_5

    const-string v1, "depositSpeedCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$5;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->res:Landroid/content/res/Resources;

    if-nez v0, :cond_6

    const-string v1, "res"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v1, Lcom/squareup/settingsapplet/R$array;->day_of_week_responsive:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "res.getStringArray(R.array.day_of_week_responsive)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 170
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_8

    .line 171
    new-instance v9, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v3, "view.context"

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 172
    aget-object v3, v0, v1

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 173
    new-instance v3, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;

    invoke-direct {v3, p0, v1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;I)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-static {v3}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v3

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->depositScheduleRows:Landroid/view/ViewGroup;

    if-nez v3, :cond_7

    const-string v4, "depositScheduleRows"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v9, Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositSettingsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 182
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$7;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$7;-><init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "scopeRunner.depositSetti\u2026subscribe(::onScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
