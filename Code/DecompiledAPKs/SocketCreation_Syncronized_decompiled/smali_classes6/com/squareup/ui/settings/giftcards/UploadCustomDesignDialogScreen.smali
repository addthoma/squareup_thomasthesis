.class public final Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;
.super Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;
.source "UploadCustomDesignDialogScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;,
        Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Runner;,
        Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;,
        Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \u00052\u00020\u0001:\u0004\u0005\u0006\u0007\u0008B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;",
        "()V",
        "getParentKey",
        "",
        "Companion",
        "Factory",
        "Runner",
        "UploadCustomDesignDialogScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;

.field private static final INSTANCE:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->Companion:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;

    .line 52
    new-instance v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;

    .line 55
    sget-object v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;-><init>()V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;

    return-object v0
.end method


# virtual methods
.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    move-result-object v0

    return-object v0
.end method
