.class Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$3;
.super Ljava/lang/Object;
.source "CardReadersScreenView.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->showBluetoothRequiredDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$3;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    return-void
.end method

.method public enableBle()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$3;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->enableBleAndGo()V

    return-void
.end method
