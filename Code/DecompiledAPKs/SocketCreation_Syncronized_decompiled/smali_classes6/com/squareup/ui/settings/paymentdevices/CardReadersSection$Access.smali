.class public final Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;
.super Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;
.source "CardReadersSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 61
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;->features:Lcom/squareup/settings/server/Features;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 3

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    .line 72
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    .line 73
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method
