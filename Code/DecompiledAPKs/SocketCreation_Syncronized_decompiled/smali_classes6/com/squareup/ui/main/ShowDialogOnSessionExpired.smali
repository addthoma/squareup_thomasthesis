.class public Lcom/squareup/ui/main/ShowDialogOnSessionExpired;
.super Ljava/lang/Object;
.source "ShowDialogOnSessionExpired.java"

# interfaces
.implements Lcom/squareup/account/SessionExpiredListener;


# instance fields
.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldagger/Lazy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/main/ShowDialogOnSessionExpired;->flow:Ldagger/Lazy;

    return-void
.end method


# virtual methods
.method public onSessionExpired()V
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/main/ShowDialogOnSessionExpired;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/main/SessionExpiredDialog;->INSTANCE:Lcom/squareup/ui/main/SessionExpiredDialog;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
