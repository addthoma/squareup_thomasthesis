.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;
.super Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeadReaderBattery"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 292
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;

    .line 298
    sget-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 295
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 301
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    .line 302
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_empty_battery_120:I

    .line 303
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->vector(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_message:I

    .line 305
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_dead_title:I

    .line 306
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 307
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
