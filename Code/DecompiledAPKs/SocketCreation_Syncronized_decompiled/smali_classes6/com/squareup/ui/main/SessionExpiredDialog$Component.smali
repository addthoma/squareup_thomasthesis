.class public interface abstract Lcom/squareup/ui/main/SessionExpiredDialog$Component;
.super Ljava/lang/Object;
.source "SessionExpiredDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SessionExpiredDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract getAppNameFormatter()Lcom/squareup/util/AppNameFormatter;
.end method

.method public abstract getSessionExpiredDialogRunner()Lcom/squareup/ui/main/SessionExpiredDialog$Runner;
.end method
