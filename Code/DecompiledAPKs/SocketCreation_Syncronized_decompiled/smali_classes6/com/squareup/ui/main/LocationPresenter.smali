.class public Lcom/squareup/ui/main/LocationPresenter;
.super Lmortar/Presenter;
.source "LocationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Landroid/app/Activity;",
        ">;"
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/main/LocationPresenter;->clock:Lcom/squareup/util/Clock;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/main/LocationPresenter;->locationProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/main/LocationPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public check(Z)V
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/main/LocationPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/LocationPresenter;->locationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    if-eqz p1, :cond_2

    if-eqz v0, :cond_1

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/main/LocationPresenter;->clock:Lcom/squareup/util/Clock;

    const-wide v1, 0x202fbf000L

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/util/Clock;->withinPast(Landroid/location/Location;J)Z

    move-result p1

    if-nez p1, :cond_2

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/main/LocationPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/squareup/ui/main/LocationPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, Lcom/squareup/ui/LocationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_2
    return-void
.end method

.method protected extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 39
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 19
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/LocationPresenter;->extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method
