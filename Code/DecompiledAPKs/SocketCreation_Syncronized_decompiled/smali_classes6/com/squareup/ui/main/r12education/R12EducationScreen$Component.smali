.class public interface abstract Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;
.super Ljava/lang/Object;
.source "R12EducationScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/main/r12education/R12EducationPanelVideoView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/main/r12education/R12EducationView;)V
.end method
