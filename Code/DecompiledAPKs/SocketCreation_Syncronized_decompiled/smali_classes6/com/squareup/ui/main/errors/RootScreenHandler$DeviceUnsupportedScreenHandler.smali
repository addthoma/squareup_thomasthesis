.class public Lcom/squareup/ui/main/errors/RootScreenHandler$DeviceUnsupportedScreenHandler;
.super Lcom/squareup/ui/main/errors/RootScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceUnsupportedScreenHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 581
    invoke-direct/range {p0 .. p12}, Lcom/squareup/ui/main/errors/RootScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 1

    .line 587
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_slash_120:I

    .line 588
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->vector(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_device_unsupported_title:I

    .line 589
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_device_unsupported_msg:I

    .line 590
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    .line 591
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/RootScreenHandler$DeviceUnsupportedScreenHandler;->goHomeButton(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 592
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method
