.class public abstract Lcom/squareup/ui/main/PosIntentParserModule;
.super Ljava/lang/Object;
.source "PosIntentParserModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/PosIntentParserModule$DeferredDeepLinkPreference;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideDeferredDeepLink(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "deferredDeepLink_appsFlyer"

    const-string v1, ""

    .line 28
    invoke-virtual {p0, v0, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method
