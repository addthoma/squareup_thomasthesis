.class public final Lcom/squareup/ui/main/BadKeyboardHider;
.super Ljava/lang/Object;
.source "BadKeyboardHider.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Just don\'t, go find a view and do it from there."
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0007\u001a\u00020\u0006J\u000e\u0010\u0008\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/main/BadKeyboardHider;",
        "",
        "()V",
        "view",
        "Landroid/view/View;",
        "dropView",
        "",
        "hideSoftKeyboard",
        "takeView",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dropView(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/main/BadKeyboardHider;->view:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/squareup/ui/main/BadKeyboardHider;->view:Landroid/view/View;

    :cond_0
    return-void
.end method

.method public final hideSoftKeyboard()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/main/BadKeyboardHider;->view:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final takeView(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/squareup/ui/main/BadKeyboardHider;->view:Landroid/view/View;

    return-void
.end method
