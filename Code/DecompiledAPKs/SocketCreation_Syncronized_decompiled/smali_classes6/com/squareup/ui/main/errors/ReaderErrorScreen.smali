.class public final Lcom/squareup/ui/main/errors/ReaderErrorScreen;
.super Lcom/squareup/ui/main/errors/RootReaderWarningScreen;
.source "ReaderErrorScreen.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/main/errors/RootReaderWarningScreen$Component;
.end annotation

.annotation runtime Lcom/squareup/ui/main/errors/RequiresReaderDisconnection;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ReaderErrorScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/ui/main/errors/-$$Lambda$ReaderErrorScreen$smevkcQhQBP-nTyva7kuyCyr98Q;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$ReaderErrorScreen$smevkcQhQBP-nTyva7kuyCyr98Q;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ReaderErrorScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V
    .locals 1

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    .line 48
    iget-object p1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 25
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 26
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/ReaderErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 35
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 36
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 37
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 38
    invoke-virtual {p1, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1, p6}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1, p5}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->userInteractionMessage(Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1, p7}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/ReaderErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/ReaderErrorScreen;
    .locals 1

    .line 52
    const-class v0, Lcom/squareup/ui/main/errors/ReaderErrorScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    .line 54
    new-instance v0, Lcom/squareup/ui/main/errors/ReaderErrorScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/ReaderErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-object v0
.end method
