.class public final Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyerFlowPinRequested"
.end annotation


# instance fields
.field public final pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
    .locals 0

    .line 49
    invoke-interface {p1, p0}, Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;->onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;)V

    return-void
.end method
