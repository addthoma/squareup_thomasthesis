.class public Lcom/squareup/ui/main/errors/ButtonFlowStarter;
.super Ljava/lang/Object;
.source "ButtonFlowStarter.java"


# instance fields
.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final application:Landroid/app/Application;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final flow:Lflow/Flow;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final readerMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/ui/NfcProcessorInterface;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lflow/Flow;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/util/BrowserLauncher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->flow:Lflow/Flow;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->readerMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->application:Landroid/app/Application;

    .line 68
    iput-object p12, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    .line 69
    iput-object p13, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    .line 70
    iput-object p14, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)Lcom/squareup/ui/NfcProcessorInterface;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

    return-object p0
.end method

.method private killTenderAndTurnOffReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter$2;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method


# virtual methods
.method contactSupport()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/SupportUrlSettings;->getHelpCenterUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method dismiss()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    goto :goto_0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->readerMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    invoke-virtual {v0}, Lcom/squareup/ui/main/ReaderStatusMonitor;->showCardReaderCard()V

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    move-result v0

    if-nez v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    :cond_2
    :goto_0
    return-void
.end method

.method goHome(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 106
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->readerMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    invoke-virtual {p1}, Lcom/squareup/ui/main/ReaderStatusMonitor;->showCardReaderCard()V

    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    return-void

    .line 107
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {p1}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    return-void
.end method

.method killTenderHumanInitiatedAndTurnOffReader()V
    .locals 1

    .line 84
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderAndTurnOffReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method killTenderReaderInitiated()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/main/errors/ButtonFlowStarter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter$1;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method killTenderReaderInitiatedAndTurnOffReader()V
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderAndTurnOffReader(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method resumePaymentFlowAtPaymentPrompt()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startOrResumeTenderFlowAtPaymentPrompt()V

    return-void
.end method

.method startActivation()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v1, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    :cond_0
    return-void
.end method
