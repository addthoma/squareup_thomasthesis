.class final Lcom/squareup/ui/main/errors/WarningScreenData$1;
.super Ljava/lang/Object;
.source "WarningScreenData.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/WarningScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/main/errors/WarningScreenData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 10

    .line 83
    const-class v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 84
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->valueOf(Ljava/lang/String;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 88
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 90
    const-class v7, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 91
    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    .line 93
    :goto_0
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 94
    invoke-static {}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->values()[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    move-result-object v8

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    aget-object p1, v8, p1

    .line 96
    new-instance v8, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {v8}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    .line 97
    invoke-virtual {v8, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v7}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->topAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 101
    invoke-virtual {v0, v3}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v9}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->cancellable(Z)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0, v4}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeout(Ljava/lang/Long;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeoutBehavior(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    if-eqz v5, :cond_1

    .line 107
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    .line 110
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 81
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningScreenData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 0

    .line 114
    new-array p1, p1, [Lcom/squareup/ui/main/errors/WarningScreenData;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 81
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningScreenData$1;->newArray(I)[Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
