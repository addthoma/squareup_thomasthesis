.class public final Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "R12ReaderTutorialBootstrapScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen$Scope;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "useUpdatingMessaging",
        "",
        "(Z)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen$Scope;",
        "Scope",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final useUpdatingMessaging:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;->useUpdatingMessaging:Z

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;->Companion:Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;

    move-result-object p1

    .line 32
    iget-boolean v0, p0, Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;->useUpdatingMessaging:Z

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;->start(Z)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen$Scope;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen$Scope;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen$Scope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;->getParentKey()Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen$Scope;

    move-result-object v0

    return-object v0
.end method
