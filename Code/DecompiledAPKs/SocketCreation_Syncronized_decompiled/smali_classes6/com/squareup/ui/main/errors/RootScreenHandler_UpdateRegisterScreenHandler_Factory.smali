.class public final Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;
.super Ljava/lang/Object;
.source "RootScreenHandler_UpdateRegisterScreenHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final accessibilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final goBackAfterWarningProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;"
        }
    .end annotation
.end field

.field private final readerStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->accessibilityManagerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->goBackAfterWarningProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->readerStatusMonitorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->nfcStateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;)",
            "Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;"
        }
    .end annotation

    .line 92
    new-instance v13, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;
    .locals 14

    .line 101
    new-instance v13, Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;
    .locals 13

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->accessibilityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->goBackAfterWarningProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/main/errors/GoBackAfterWarning;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/api/ApiTransactionState;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/api/ApiReaderSettingsController;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->readerStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/main/ReaderStatusMonitor;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->nfcStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/AndroidNfcState;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->newInstance(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler_UpdateRegisterScreenHandler_Factory;->get()Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;

    move-result-object v0

    return-object v0
.end method
