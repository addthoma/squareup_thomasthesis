.class Lcom/squareup/ui/main/errors/RootScreenHandler$5;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;->goHomeAndRestartSecureSessionButton(ILcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

.field final synthetic val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$5;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$5;->val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$5;->val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz p1, :cond_0

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$5;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    iget-object p1, p1, Lcom/squareup/ui/main/errors/RootScreenHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$5;->val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 155
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->reinitializeSecureSession()V

    .line 158
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$5;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/RootScreenHandler;->goHome()V

    return-void
.end method
