.class synthetic Lcom/squareup/ui/main/r12education/R12EducationView$5;
.super Ljava/lang/Object;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$CountryCode:[I

.field static final synthetic $SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$DuringFwup:[I

.field static final synthetic $SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$PreferContactless:[I

.field static final synthetic $SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$VideoAvailable:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 347
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->values()[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$VideoAvailable:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$VideoAvailable:[I

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->R12_EDUCATION_VIDEO_AVAILABLE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;

    invoke-virtual {v2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$VideoAvailable:[I

    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->NO_VIDEO_URL:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;

    invoke-virtual {v3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 329
    :catch_1
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->values()[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$PreferContactless:[I

    :try_start_2
    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$PreferContactless:[I

    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    invoke-virtual {v3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$PreferContactless:[I

    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    invoke-virtual {v3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    const/4 v2, 0x3

    :try_start_4
    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$PreferContactless:[I

    sget-object v4, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->NO_CL_PREFERENCE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    invoke-virtual {v4}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    .line 316
    :catch_4
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->values()[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$DuringFwup:[I

    :try_start_5
    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$DuringFwup:[I

    sget-object v4, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    invoke-virtual {v4}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$DuringFwup:[I

    sget-object v4, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->NO_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    invoke-virtual {v4}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    .line 203
    :catch_6
    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    :try_start_7
    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    sget-object v4, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    invoke-virtual {v4}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    sget-object v3, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {v3}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    sget-object v1, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    return-void
.end method
