.class public Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "SwipeDipTapWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 52
    const-class v0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ParentComponent;

    .line 53
    check-cast p2, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;

    .line 54
    new-instance v0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;-><init>(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;)V

    .line 55
    invoke-interface {p1, v0}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ParentComponent;->swipeDipTapWarningScreen(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;)Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;

    move-result-object p1

    return-object p1
.end method
