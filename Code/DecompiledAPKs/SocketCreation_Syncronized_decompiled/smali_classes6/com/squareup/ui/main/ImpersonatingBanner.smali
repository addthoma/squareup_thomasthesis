.class Lcom/squareup/ui/main/ImpersonatingBanner;
.super Ljava/lang/Object;
.source "ImpersonatingBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/ImpersonatingBanner$Impersonating;
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final isImpersonating:Z


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Z)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/main/ImpersonatingBanner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 39
    iput-boolean p2, p0, Lcom/squareup/ui/main/ImpersonatingBanner;->isImpersonating:Z

    return-void
.end method


# virtual methods
.method onAttached(Landroid/content/Context;)V
    .locals 6

    .line 43
    iget-boolean v0, p0, Lcom/squareup/ui/main/ImpersonatingBanner;->isImpersonating:Z

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v1, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {v1, p1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 46
    iget-object v2, p0, Lcom/squareup/ui/main/ImpersonatingBanner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v2

    .line 47
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v2, 0x11

    .line 48
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 49
    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    const/4 v2, -0x1

    .line 50
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x0

    .line 53
    sget v5, Lcom/squareup/widgets/R$dimen;->text_android_small:I

    .line 54
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    .line 53
    invoke-virtual {v1, v4, v3}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    const v3, -0x1ad7e9

    .line 55
    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundColor(I)V

    const v3, 0x3f4ccccd    # 0.8f

    .line 56
    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setAlpha(F)V

    .line 58
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    new-instance v1, Landroid/app/Dialog;

    sget v3, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {v1, p1, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 62
    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 63
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x18

    .line 64
    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    const/16 v3, 0x50

    .line 65
    invoke-virtual {v0, v3}, Landroid/view/Window;->setGravity(I)V

    .line 66
    invoke-virtual {v0, v2, v4}, Landroid/view/Window;->setLayout(II)V

    .line 67
    invoke-static {p1}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object p1

    .line 68
    invoke-virtual {p1, v1}, Lcom/squareup/ui/DialogDestroyer;->show(Landroid/app/Dialog;)V

    :cond_0
    return-void
.end method
