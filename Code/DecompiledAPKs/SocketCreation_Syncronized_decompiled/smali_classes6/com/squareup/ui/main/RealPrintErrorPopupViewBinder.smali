.class public Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;
.super Ljava/lang/Object;
.source "RealPrintErrorPopupViewBinder.java"

# interfaces
.implements Lcom/squareup/ui/main/PrintErrorPopupViewBinder;


# instance fields
.field private printErrorPopup:Lcom/squareup/ui/print/PrintErrorPopup;

.field private final printErrorPopupPresenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopupPresenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    return-void
.end method


# virtual methods
.method public attachPrintErrorPopup(Landroid/content/Context;)V
    .locals 2

    .line 19
    new-instance v0, Lcom/squareup/ui/print/PrintErrorPopup;

    iget-object v1, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopupPresenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/print/PrintErrorPopup;-><init>(Landroid/content/Context;Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopup:Lcom/squareup/ui/print/PrintErrorPopup;

    .line 20
    iget-object p1, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopupPresenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    iget-object v0, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopup:Lcom/squareup/ui/print/PrintErrorPopup;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public detachPrintErrorPopup()V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopupPresenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;->printErrorPopup:Lcom/squareup/ui/print/PrintErrorPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method
