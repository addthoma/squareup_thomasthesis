.class public Lcom/squareup/ui/main/r12education/TransitionView;
.super Landroid/widget/FrameLayout;
.source "TransitionView.java"


# instance fields
.field private progress:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 28
    iput p1, p0, Lcom/squareup/ui/main/r12education/TransitionView;->progress:F

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 35
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const/4 v0, 0x0

    .line 36
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/r12education/TransitionView;->setProgress(F)V

    return-void
.end method

.method public setAlpha(F)V
    .locals 1

    .line 65
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    const/4 v0, 0x0

    cmpl-float p1, p1, v0

    if-nez p1, :cond_0

    .line 68
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/r12education/TransitionView;->setProgress(F)V

    :cond_0
    return-void
.end method

.method public setProgress(F)V
    .locals 7

    .line 45
    iput p1, p0, Lcom/squareup/ui/main/r12education/TransitionView;->progress:F

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/TransitionView;->getChildCount()I

    move-result v0

    int-to-float v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    div-float v1, v2, v1

    div-float v3, p1, v1

    float-to-int v3, v3

    add-int/lit8 v4, v3, 0x1

    int-to-float v5, v3

    mul-float v5, v5, v1

    sub-float/2addr p1, v5

    div-float/2addr p1, v1

    sub-float v1, v2, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    if-ne v2, v6, :cond_0

    move v5, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v6, v4, -0x1

    if-ne v2, v6, :cond_1

    move v5, p1

    .line 60
    :cond_1
    :goto_1
    invoke-virtual {p0, v2}, Lcom/squareup/ui/main/r12education/TransitionView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
