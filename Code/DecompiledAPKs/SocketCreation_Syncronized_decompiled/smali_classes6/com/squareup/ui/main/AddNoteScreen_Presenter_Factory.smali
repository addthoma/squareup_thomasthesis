.class public final Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "AddNoteScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/AddNoteScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final addNoteScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final bundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final diagProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->diagProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->addNoteScreenRunnerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->bundleKeyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/util/ToastFactory;Lcom/squareup/BundleKey;)Lcom/squareup/ui/main/AddNoteScreen$Presenter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            "Lcom/squareup/util/ToastFactory;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/main/AddNoteScreen$Presenter;"
        }
    .end annotation

    .line 60
    new-instance v7, Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/util/ToastFactory;Lcom/squareup/BundleKey;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/AddNoteScreen$Presenter;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->diagProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/DiagnosticCrasher;

    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->addNoteScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/main/AddNoteScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->bundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/BundleKey;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/util/ToastFactory;Lcom/squareup/BundleKey;)Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen_Presenter_Factory;->get()Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
