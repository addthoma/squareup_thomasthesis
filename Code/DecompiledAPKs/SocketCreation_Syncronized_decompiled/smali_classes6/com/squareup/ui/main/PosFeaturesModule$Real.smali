.class public abstract Lcom/squareup/ui/main/PosFeaturesModule$Real;
.super Ljava/lang/Object;
.source "PosFeaturesModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/PosFeaturesModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Real"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideRenderSignatureProvider(Lio/reactivex/Scheduler;)Lcom/squareup/balance/core/RenderSignatureScheduler;
    .locals 2
    .param p0    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/balance/core/RenderSignatureScheduler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/balance/core/RenderSignatureScheduler;-><init>(Lio/reactivex/Scheduler;Z)V

    return-object v0
.end method


# virtual methods
.method abstract provideIntentAvailabilityManager(Lcom/squareup/util/DefaultIntentAvailabilityManager;)Lcom/squareup/util/IntentAvailabilityManager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
