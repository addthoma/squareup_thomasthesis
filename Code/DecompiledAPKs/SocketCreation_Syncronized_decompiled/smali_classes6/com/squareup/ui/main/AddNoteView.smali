.class public Lcom/squareup/ui/main/AddNoteView;
.super Landroid/widget/LinearLayout;
.source "AddNoteView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private editNotes:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/main/AddNoteScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const-class p2, Lcom/squareup/ui/main/AddNoteScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/AddNoteScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/main/AddNoteScreen$Component;->inject(Lcom/squareup/ui/main/AddNoteView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 86
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 87
    sget v0, Lcom/squareup/orderentry/R$id;->edit_notes:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getNote()Ljava/lang/String;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 65
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$requestInitialFocus$0$AddNoteView()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/main/AddNoteView;->bindViews()V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/main/AddNoteView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/AddNoteView$1;-><init>(Lcom/squareup/ui/main/AddNoteView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->presenter:Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->presenter:Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->presenter:Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$AddNoteView$yr8qwtQ3v0YQpHZVPhKE29jHDuw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/-$$Lambda$AddNoteView$yr8qwtQ3v0YQpHZVPhKE29jHDuw;-><init>(Lcom/squareup/ui/main/AddNoteView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setNote(Ljava/lang/String;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteView;->editNotes:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
