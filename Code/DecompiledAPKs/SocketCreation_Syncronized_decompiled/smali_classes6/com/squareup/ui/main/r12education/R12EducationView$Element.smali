.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$Element;
.super Ljava/lang/Enum;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Element"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/main/r12education/R12EducationView$Element;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum CABLE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum CHIP_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum DOTS_GREEN:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum DOTS_ORANGE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum MAG_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum PHONE_APPLE_PAY:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum PHONE_R4:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum R12:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum STICKER:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

.field public static final enum TAP_CARD_WITH_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$Element;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 64
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v1, 0x0

    const-string v2, "R12"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->R12:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 65
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v2, 0x1

    const-string v3, "CABLE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->CABLE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 66
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v3, 0x2

    const-string v4, "CHIP_CARD"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->CHIP_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 67
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v4, 0x3

    const-string v5, "DOTS_GREEN"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_GREEN:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 68
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v5, 0x4

    const-string v6, "DOTS_ORANGE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_ORANGE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 69
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v6, 0x5

    const-string v7, "PHONE_APPLE_PAY"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->PHONE_APPLE_PAY:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 70
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v7, 0x6

    const-string v8, "PHONE_R4"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->PHONE_R4:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 71
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/4 v8, 0x7

    const-string v9, "MAG_CARD"

    invoke-direct {v0, v9, v8}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->MAG_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 72
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/16 v9, 0x8

    const-string v10, "STICKER"

    invoke-direct {v0, v10, v9}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->STICKER:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 73
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/16 v10, 0x9

    const-string v11, "TAP_CARD_WITH_HAND"

    invoke-direct {v0, v11, v10}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->TAP_CARD_WITH_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    .line 63
    sget-object v11, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->R12:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->CABLE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->CHIP_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_GREEN:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_ORANGE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->PHONE_APPLE_PAY:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->PHONE_R4:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->MAG_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->STICKER:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->TAP_CARD_WITH_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/r12education/R12EducationView$Element;
    .locals 1

    .line 63
    const-class v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/main/r12education/R12EducationView$Element;
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    invoke-virtual {v0}, [Lcom/squareup/ui/main/r12education/R12EducationView$Element;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    return-object v0
.end method
