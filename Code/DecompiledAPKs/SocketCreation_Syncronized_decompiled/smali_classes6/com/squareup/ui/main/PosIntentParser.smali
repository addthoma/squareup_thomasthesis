.class public Lcom/squareup/ui/main/PosIntentParser;
.super Ljava/lang/Object;
.source "PosIntentParser.java"

# interfaces
.implements Lcom/squareup/ui/main/IntentParser;


# static fields
.field private static final DEFAULT_HISTORY_FACTORY:Lcom/squareup/ui/main/HistoryFactory; = null

.field public static final GO_BACK_INTENT_EXTRA:Ljava/lang/String; = "GO_BACK"

.field public static final INTENT_ACTION_PREFIX:Ljava/lang/String; = "com.squareup.action.VIEW_"

.field private static final INTENT_PIP_ACTION_PREFIX:Ljava/lang/String; = "com.squareup.pos.action.PIP_"

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "screen"


# instance fields
.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final apiTransactionFactoryParser:Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

.field private final appletsByIntentExtraName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/applet/HistoryFactoryApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinks:Lcom/squareup/ui/main/DeepLinks;

.field private final deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnap:Lcom/squareup/log/OhSnapLogger;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/main/ApiTransactionFactory$Parser;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/applet/Applets;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/ui/main/ApiTransactionFactory$Parser;",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Lcom/squareup/applet/Applets;",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/main/PosIntentParser;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 98
    iput-object p2, p0, Lcom/squareup/ui/main/PosIntentParser;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    .line 99
    iput-object p3, p0, Lcom/squareup/ui/main/PosIntentParser;->analytics:Lcom/squareup/analytics/Analytics;

    .line 100
    iput-object p4, p0, Lcom/squareup/ui/main/PosIntentParser;->ohSnap:Lcom/squareup/log/OhSnapLogger;

    .line 101
    iput-object p5, p0, Lcom/squareup/ui/main/PosIntentParser;->apiTransactionFactoryParser:Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    .line 102
    iput-object p6, p0, Lcom/squareup/ui/main/PosIntentParser;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    .line 103
    iput-object p8, p0, Lcom/squareup/ui/main/PosIntentParser;->timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    .line 104
    iput-object p9, p0, Lcom/squareup/ui/main/PosIntentParser;->deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;

    .line 106
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/PosIntentParser;->appletsByIntentExtraName:Ljava/util/Map;

    .line 108
    invoke-interface {p7}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/applet/Applet;

    .line 109
    instance-of p3, p2, Lcom/squareup/applet/HistoryFactoryApplet;

    if-eqz p3, :cond_0

    .line 110
    check-cast p2, Lcom/squareup/applet/HistoryFactoryApplet;

    .line 111
    invoke-virtual {p2}, Lcom/squareup/applet/HistoryFactoryApplet;->getIntentScreenExtra()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 113
    iget-object p4, p0, Lcom/squareup/ui/main/PosIntentParser;->appletsByIntentExtraName:Ljava/util/Map;

    invoke-interface {p4, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 52
    invoke-static {p0}, Lcom/squareup/ui/main/MainActivity;->createMainActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p0

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.squareup.action.VIEW_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "screen"

    .line 54
    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public static createPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 1

    .line 67
    invoke-static {p0, p1}, Lcom/squareup/ui/main/PosIntentParser;->doCreatePendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 68
    invoke-static {p0, p1}, Lcom/squareup/ui/main/PosIntentParser;->doCreatePendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method private static doCreatePendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    .line 72
    invoke-static {p0, p1}, Lcom/squareup/ui/main/PosIntentParser;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x1

    const/high16 v1, 0x8000000

    .line 78
    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method private getDeferredDeepLinkIntent()Landroid/content/Intent;
    .locals 3

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser;->deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 217
    :cond_0
    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 218
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/main/PosIntentParser;->deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->delete()V

    .line 225
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.VIEW"

    .line 226
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object v1
.end method

.method private static isWebActivationCallback(Landroid/content/Intent;)Z
    .locals 2

    .line 120
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "complete"

    .line 121
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method


# virtual methods
.method public isGoBackIntent(Landroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 129
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    const-string v2, "com.squareup.action.VIEW_"

    .line 133
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    return v0

    :cond_2
    const-string v0, "screen"

    .line 136
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "GO_BACK"

    .line 137
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public parseIntent(Landroid/content/Intent;)Lcom/squareup/ui/main/HistoryFactory;
    .locals 5

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/main/PosIntentParser;->getDeferredDeepLinkIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 153
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    if-nez v0, :cond_1

    .line 155
    sget-object p1, Lcom/squareup/ui/main/PosIntentParser;->DEFAULT_HISTORY_FACTORY:Lcom/squareup/ui/main/HistoryFactory;

    return-object p1

    :cond_1
    move-object p1, v0

    .line 164
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {p1}, Lcom/squareup/ui/main/PosIntentParser;->isWebActivationCallback(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/main/PosIntentParser;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_WEB_ACTIVATION_COMPLETE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/main/PosIntentParser;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/squareup/ui/main/PosIntentParser;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v4

    .line 171
    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-interface {v2, v3, v4, v1}, Lcom/squareup/adanalytics/AdAnalytics;->recordActivation(Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/lang/String;)V

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/main/PosIntentParser;->ohSnap:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->HANDLE_INTENT:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v1, v2, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/main/PosIntentParser;->apiTransactionFactoryParser:Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->applies(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser;->apiTransactionFactoryParser:Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->parseIntent(Landroid/content/Intent;)Lcom/squareup/ui/main/ApiTransactionFactory;

    move-result-object p1

    return-object p1

    :cond_4
    const-string v1, "android.intent.action.VIEW"

    .line 181
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/DeepLinks;->handleExternalDeepLink(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/deeplinks/DeepLinkResult;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 184
    invoke-virtual {p1}, Lcom/squareup/deeplinks/DeepLinkResult;->getFactory()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object p1

    return-object p1

    .line 186
    :cond_5
    sget-object p1, Lcom/squareup/ui/main/PosIntentParser;->DEFAULT_HISTORY_FACTORY:Lcom/squareup/ui/main/HistoryFactory;

    return-object p1

    :cond_6
    const-string v1, "com.squareup.action.VIEW_"

    .line 190
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 191
    sget-object p1, Lcom/squareup/ui/main/PosIntentParser;->DEFAULT_HISTORY_FACTORY:Lcom/squareup/ui/main/HistoryFactory;

    return-object p1

    .line 194
    :cond_7
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_8

    .line 197
    sget-object p1, Lcom/squareup/ui/main/PosIntentParser;->DEFAULT_HISTORY_FACTORY:Lcom/squareup/ui/main/HistoryFactory;

    return-object p1

    :cond_8
    const-string v0, "CLOCK_IN_OUT"

    .line 200
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/main/PosIntentParser;->timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-interface {p1}, Lcom/squareup/ui/timecards/api/TimecardsLauncher;->clockInOutHistoryFactory()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object p1

    return-object p1

    .line 204
    :cond_9
    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser;->appletsByIntentExtraName:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/HistoryFactoryApplet;

    if-eqz p1, :cond_a

    return-object p1

    .line 209
    :cond_a
    sget-object p1, Lcom/squareup/ui/main/PosIntentParser;->DEFAULT_HISTORY_FACTORY:Lcom/squareup/ui/main/HistoryFactory;

    return-object p1
.end method
