.class public Lcom/squareup/ui/main/TopScreenChecker;
.super Ljava/lang/Object;
.source "TopScreenChecker.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private latestScreen:Lcom/squareup/container/ContainerTreeKey;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private nextScreenSub:Lio/reactivex/disposables/Disposable;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/PosContainer;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/TopScreenChecker;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method static synthetic lambda$unobscured$1(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 58
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$unobscured$2(Ljava/util/List;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Boolean;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 67
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 68
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    .line 70
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public isUnobscured(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/main/TopScreenChecker;->latestScreen:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$0$TopScreenChecker(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/main/TopScreenChecker;->latestScreen:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 29
    iget-object p1, p0, Lcom/squareup/ui/main/TopScreenChecker;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {p1}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$TopScreenChecker$Aqxy2ol-rEpy0beNUzLb0Ska2n0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/-$$Lambda$TopScreenChecker$Aqxy2ol-rEpy0beNUzLb0Ska2n0;-><init>(Lcom/squareup/ui/main/TopScreenChecker;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/main/TopScreenChecker;->nextScreenSub:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/main/TopScreenChecker;->nextScreenSub:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method

.method public unobscured(Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerTreeKey;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/main/TopScreenChecker;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$TopScreenChecker$7pJ-oE_TyD5WreISwZxrbPkDuTc;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/-$$Lambda$TopScreenChecker$7pJ-oE_TyD5WreISwZxrbPkDuTc;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public unobscured(Ljava/util/List;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Class;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/main/TopScreenChecker;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$TopScreenChecker$r2gxfhvVX1ns36cZeoUjL70GCMo;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/-$$Lambda$TopScreenChecker$r2gxfhvVX1ns36cZeoUjL70GCMo;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
