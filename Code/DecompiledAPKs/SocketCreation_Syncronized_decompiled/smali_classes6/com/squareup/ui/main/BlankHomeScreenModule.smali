.class public abstract Lcom/squareup/ui/main/BlankHomeScreenModule;
.super Ljava/lang/Object;
.source "BlankHomeScreenModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideHome$0(Lflow/History;)Ljava/util/List;
    .locals 0

    .line 14
    sget-object p0, Lcom/squareup/ui/main/BlankScreen;->INSTANCE:Lcom/squareup/ui/main/BlankScreen;

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$providePaymentFlowHistoryFactory$1(Lflow/History;)Lflow/History;
    .locals 0

    .line 18
    sget-object p0, Lcom/squareup/ui/main/BlankScreen;->INSTANCE:Lcom/squareup/ui/main/BlankScreen;

    invoke-static {p0}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p0

    return-object p0
.end method

.method public static provideHome()Lcom/squareup/ui/main/Home;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/ui/main/-$$Lambda$BlankHomeScreenModule$SV_Ns5Ix9vJZ-DsBTXyvQmkG8aw;->INSTANCE:Lcom/squareup/ui/main/-$$Lambda$BlankHomeScreenModule$SV_Ns5Ix9vJZ-DsBTXyvQmkG8aw;

    return-object v0
.end method

.method public static providePaymentFlowHistoryFactory()Lcom/squareup/ui/main/PaymentFlowHistoryFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    sget-object v0, Lcom/squareup/ui/main/-$$Lambda$BlankHomeScreenModule$N6BpbHjDCELAxq9keee4Sqybpjo;->INSTANCE:Lcom/squareup/ui/main/-$$Lambda$BlankHomeScreenModule$N6BpbHjDCELAxq9keee4Sqybpjo;

    return-object v0
.end method
