.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$8;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateStandardPanelTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$8;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .line 384
    sget-object p4, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result p1

    aget p1, p4, p1

    const/16 p4, 0x8

    const/high16 p6, 0x3e800000    # 0.25f

    if-eq p1, p4, :cond_1

    const/16 p4, 0x9

    if-eq p1, p4, :cond_0

    .line 408
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 398
    invoke-static {p3, p1, p6}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 400
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 401
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToBottomY(Landroid/view/View;Landroid/view/View;)V

    const/4 p3, 0x0

    .line 402
    invoke-static {p2, p5, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutLeftFromAlignCenter(Landroid/view/View;Landroid/view/View;IF)V

    goto :goto_0

    :cond_1
    const/high16 p1, 0x3f400000    # 0.75f

    .line 388
    invoke-static {p3, p6, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 390
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 391
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->centerX(Landroid/view/View;Landroid/view/View;)V

    .line 392
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$8;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneR4BottomMargin:I

    invoke-static {p2, p5, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutFromAlignBottom(Landroid/view/View;Landroid/view/View;IF)V

    :goto_0
    return-void
.end method
