.class public Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;
.super Ljava/lang/Object;
.source "ReaderTutorialDeepLinkHandler.java"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# instance fields
.field private final helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletHistoryBuilder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 5

    .line 20
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/startTutorial"

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    .line 23
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "R6"

    .line 24
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 25
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/squareup/container/ContainerTreeKey;

    const/4 v3, 0x0

    sget-object v4, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;->INSTANCE:Lcom/squareup/ui/main/R6FirstTimeVideoScreen;

    aput-object v4, v2, v3

    .line 26
    invoke-interface {v1, v2}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistory([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 30
    :cond_0
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method
