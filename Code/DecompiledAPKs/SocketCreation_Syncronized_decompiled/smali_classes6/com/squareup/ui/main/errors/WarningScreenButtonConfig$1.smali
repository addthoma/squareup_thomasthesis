.class final Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$1;
.super Ljava/lang/Object;
.source "WarningScreenButtonConfig.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
    .locals 7

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    move-result-object v2

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 91
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v3, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 92
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 95
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    move-object v1, p1

    move v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;ZZLjava/lang/String;I)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 87
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
    .locals 0

    .line 100
    new-array p1, p1, [Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 87
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$1;->newArray(I)[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    move-result-object p1

    return-object p1
.end method
