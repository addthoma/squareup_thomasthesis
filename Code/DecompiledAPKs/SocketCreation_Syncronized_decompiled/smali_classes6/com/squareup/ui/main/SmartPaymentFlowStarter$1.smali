.class Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;
.super Ljava/lang/Object;
.source "SmartPaymentFlowStarter.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;->this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;->this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->access$000(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object p1, p1, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;->secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlowForHardwarePinRequest(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;)V
    .locals 0

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;->this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static {p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->access$000(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlow()V

    return-void
.end method

.method public onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;->this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->access$000(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlow(Lcom/squareup/ui/buyer/ContactlessPinRequest;)V

    return-void
.end method

.method public onResult(Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;)V
    .locals 0

    return-void
.end method

.method public onResult(Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;->this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->access$000(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 168
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 167
    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->goToEmvAuthorizingScreen(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public onResult(Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$1;->this$0:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->access$000(Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 163
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 162
    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->goToPreparingPaymentScreen(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method
