.class public Lcom/squareup/ui/main/ApiMainActivity;
.super Lcom/squareup/ui/main/MainActivity;
.source "ApiMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/ApiMainActivity$Component;
    }
.end annotation


# instance fields
.field apiAddCardOnFileController:Lcom/squareup/api/ApiAddCardOnFileController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field apiTransactionController:Lcom/squareup/api/ApiTransactionController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 31
    const-class v0, Lcom/squareup/ui/main/ApiMainActivity$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/ApiMainActivity$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/main/ApiMainActivity$Component;->inject(Lcom/squareup/ui/main/ApiMainActivity;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    if-nez p2, :cond_2

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiAddCardOnFileController:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->isApiAddCustomerCardRequest()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiAddCardOnFileController:Lcom/squareup/api/ApiAddCardOnFileController;

    sget-object p2, Lcom/squareup/api/ApiErrorResult;->GET_LOCATION_DENIED:Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {p1, p2}, Lcom/squareup/api/ApiAddCardOnFileController;->handleAddCardOnFileError(Lcom/squareup/api/ApiErrorResult;)V

    goto :goto_0

    .line 44
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiReaderSettingsController;->handleReaderSettingsCanceled()Z

    goto :goto_0

    .line 47
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    const/4 p2, 0x0

    sget-object p3, Lcom/squareup/api/ApiErrorResult;->GET_LOCATION_DENIED:Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    :cond_2
    :goto_0
    return-void

    .line 52
    :cond_3
    new-instance p2, Ljava/lang/IllegalStateException;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 54
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p3, v0, p1

    const-string p1, "unexpected activity result (requestCode=%d, response data=%s"

    .line 52
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
