.class final Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;
.super Ljava/lang/Object;
.source "SpeedTestCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012<\u0010\u0002\u001a8\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 11

    const-string v0, "it"

    .line 41
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->getState()Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    move-result-object p1

    .line 42
    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$StartingSpeedTest;

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getUpButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Landroid/widget/ImageView;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v3, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$1;

    invoke-direct {v3, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v3}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSpinnerGlyph$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->reset()V

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSecondaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/readertutorial/R$string;->speed_test_in_progress:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "view.context.getString(R\u2026g.speed_test_in_progress)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/readertutorial/R$string;->speed_test_checking_connection:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "view.context.getString(R\u2026test_checking_connection)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {p1, v0, v1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$updateTexts(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 52
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Success;

    const-string v2, "view.context"

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getUpButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Landroid/widget/ImageView;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    new-instance v5, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$2;

    invoke-direct {v5, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$2;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v4, v5}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    new-instance v5, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$3;

    invoke-direct {v5, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$3;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v4, v5}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSpinnerGlyph$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->setToSuccess()V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSecondaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 58
    iget-object v3, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/squareup/readertutorial/R$string;->speed_test_complete_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string p1, "view.context.getString(R\u2026peed_test_complete_title)"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget v7, Lcom/squareup/readertutorial/R$string;->speed_test_complete_message:I

    .line 63
    sget v8, Lcom/squareup/readertutorial/R$string;->speed_test_success_url:I

    .line 64
    sget v9, Lcom/squareup/readertutorial/R$string;->speed_test_support_link_text:I

    const-string v6, "support_link"

    .line 58
    invoke-static/range {v3 .. v9}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$updateTextsWithHyperlink(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)V

    goto/16 :goto_0

    .line 67
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Failure;

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getUpButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v4, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$4;

    invoke-direct {v4, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$4;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v4}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSecondaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v4, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$5;

    invoke-direct {v4, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$5;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v4}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v4, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$6;

    invoke-direct {v4, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$6;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v4}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSpinnerGlyph$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->setToFailure()V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$getSecondaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 74
    iget-object v4, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/squareup/readertutorial/R$string;->speed_test_failure_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string p1, "view.context.getString(R\u2026speed_test_failure_title)"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    sget v8, Lcom/squareup/readertutorial/R$string;->speed_test_failure_message:I

    .line 79
    sget v9, Lcom/squareup/readertutorial/R$string;->speed_test_failure_url:I

    .line 80
    sget v10, Lcom/squareup/readertutorial/R$string;->speed_test_support_link_text:I

    const-string v7, "support_link"

    .line 74
    invoke-static/range {v4 .. v10}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->access$updateTextsWithHyperlink(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
