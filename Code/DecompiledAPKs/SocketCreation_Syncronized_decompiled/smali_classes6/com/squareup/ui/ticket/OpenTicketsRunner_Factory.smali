.class public final Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;
.super Ljava/lang/Object;
.source "OpenTicketsRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;)",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/badbus/BadEventSink;)Lcom/squareup/ui/ticket/OpenTicketsRunner;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/ticket/OpenTicketsRunner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/badbus/BadEventSink;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/OpenTicketsRunner;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/badbus/BadEventSink;

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;->newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/badbus/BadEventSink;)Lcom/squareup/ui/ticket/OpenTicketsRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/OpenTicketsRunner_Factory;->get()Lcom/squareup/ui/ticket/OpenTicketsRunner;

    move-result-object v0

    return-object v0
.end method
