.class Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;
.super Ljava/lang/Object;
.source "TicketActionScopeRunner.java"

# interfaces
.implements Lcom/squareup/tickets/Tickets$VoidHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidSelectedTickets(Ljava/lang/String;Lcom/squareup/protos/client/Employee;Lcom/squareup/tickets/TicketsCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field final synthetic val$employee:Lcom/squareup/protos/client/Employee;

.field final synthetic val$orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

.field final synthetic val$voidReason:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketActionScopeRunner;Ljava/lang/String;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->val$voidReason:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->val$orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->val$employee:Lcom/squareup/protos/client/Employee;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public voidTicket(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/protos/client/IdPair;
    .locals 4

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->access$000(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/squareup/compvoidcontroller/VoidController;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->val$voidReason:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->val$orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;->val$employee:Lcom/squareup/protos/client/Employee;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/squareup/compvoidcontroller/VoidController;->voidTicket(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    return-object p1
.end method
