.class public Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;
.super Ljava/lang/Object;
.source "RealOpenTicketsHomeScreenSelector.java"

# interfaces
.implements Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;


# instance fields
.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method private openTicketsHomeScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 77
    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v0

    return-object v0
.end method

.method private safeToInsertOpenTicketsHomeScreen()Z
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 71
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->transaction:Lcom/squareup/payment/Transaction;

    .line 72
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->transaction:Lcom/squareup/payment/Transaction;

    .line 73
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->openTicketsHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public leavingSplitTicketScreen(Lflow/Traversal;)Z
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/ticket/SplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/SplitTicketScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Traversals;->leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    return p1
.end method

.method public updateHomeForOpenTickets(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lflow/History;
    .locals 1

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->safeToInsertOpenTicketsHomeScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->openTicketsHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    .line 57
    invoke-static {p1, v0, p2}, Lcom/squareup/container/Histories;->insertAfter(Lflow/History;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/Class;)Lflow/History;

    move-result-object p1

    return-object p1

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    invoke-direct {p0}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;->openTicketsHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    .line 62
    invoke-static {p1, v0, p2}, Lcom/squareup/container/Histories;->removeAfter(Lflow/History;Ljava/lang/Class;Ljava/lang/Class;)Lflow/History;

    move-result-object p1

    :cond_1
    return-object p1
.end method
