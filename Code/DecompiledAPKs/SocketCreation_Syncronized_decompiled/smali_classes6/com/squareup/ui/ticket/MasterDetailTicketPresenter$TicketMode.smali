.class final enum Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
.super Ljava/lang/Enum;
.source "MasterDetailTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TicketMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field public static final enum BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field public static final enum LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field public static final enum MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field public static final enum MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field public static final enum SAVE_TO_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 131
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v1, 0x0

    const-string v2, "LOAD_TICKET"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 138
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v2, 0x1

    const-string v3, "SAVE_TO_TICKET"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->SAVE_TO_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 151
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v3, 0x2

    const-string v4, "MERGE_TRANSACTION_TICKET"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 157
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v4, 0x3

    const-string v5, "MOVE_TICKET"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 162
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v5, 0x4

    const-string v6, "BULK_EDIT_TICKETS"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 124
    sget-object v6, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->SAVE_TO_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->$VALUES:[Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 1

    .line 124
    const-class v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->$VALUES:[Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-object v0
.end method
