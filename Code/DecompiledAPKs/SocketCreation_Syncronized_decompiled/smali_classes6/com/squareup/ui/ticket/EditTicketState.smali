.class public final Lcom/squareup/ui/ticket/EditTicketState;
.super Ljava/lang/Object;
.source "EditTicketState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/EditTicketState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private didShowInitialFocus:Z

.field private editableName:Ljava/lang/String;

.field private isConvertingToCustomTicket:Z

.field private isPredefinedTicketsEnabled:Z

.field private name:Ljava/lang/String;

.field private nameHint:Ljava/lang/String;

.field private note:Ljava/lang/String;

.field private selectedGroup:Lcom/squareup/api/items/TicketGroup;

.field private shouldSelectName:Z

.field private showCardNotStoredHint:Z

.field private showCompButton:Z

.field private showDeleteButton:Z

.field private showUncompButton:Z

.field private showVoidButton:Z

.field private ticketTemplate:Lcom/squareup/api/items/TicketTemplate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 222
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketState$1;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/EditTicketState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/EditTicketState;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Z)V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicketsEnabled:Z

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3

    .line 254
    const-class v0, Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    .line 255
    const-class v0, Lcom/squareup/api/items/TicketTemplate;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    .line 256
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->nameHint:Ljava/lang/String;

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->note:Ljava/lang/String;

    .line 260
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicketsEnabled:Z

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->isConvertingToCustomTicket:Z

    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCardNotStoredHint:Z

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showDeleteButton:Z

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    :goto_4
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showVoidButton:Z

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    :goto_5
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCompButton:Z

    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    :goto_6
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showUncompButton:Z

    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    if-eqz p1, :cond_7

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/EditTicketState;->didShowInitialFocus:Z

    return-void
.end method


# virtual methods
.method areTicketGroupsVisible()Z
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->isCustomTicket()Z

    move-result v0

    return v0
.end method

.method canSave()Z
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method convertToCustomTicket()V
    .locals 1

    const/4 v0, 0x1

    .line 126
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->isConvertingToCustomTicket:Z

    const/4 v0, 0x0

    .line 127
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setTicketTemplate(Lcom/squareup/api/items/TicketTemplate;)V

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method didShowInitialFocus()Z
    .locals 1

    .line 183
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->didShowInitialFocus:Z

    return v0
.end method

.method getEditableName()Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    return-object v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    return-object v0
.end method

.method getNameHint()Ljava/lang/String;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->nameHint:Ljava/lang/String;

    return-object v0
.end method

.method getNote()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->note:Ljava/lang/String;

    return-object v0
.end method

.method getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    .line 100
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    .line 101
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method getSelectedGroup()Lcom/squareup/api/items/TicketGroup;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    return-object v0
.end method

.method getSelectedGroupId()Ljava/lang/String;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method getTemplateName()Ljava/lang/String;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method getTicketTemplate()Lcom/squareup/api/items/TicketTemplate;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    return-object v0
.end method

.method public initialFocusShown()V
    .locals 1

    const/4 v0, 0x1

    .line 179
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->didShowInitialFocus:Z

    return-void
.end method

.method isCardNotStoredHintVisible()Z
    .locals 1

    .line 167
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCardNotStoredHint:Z

    return v0
.end method

.method isCompButtonVisible()Z
    .locals 1

    .line 147
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCompButton:Z

    return v0
.end method

.method isConvertToCustomTicketButtonVisible()Z
    .locals 1

    .line 135
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicket()Z

    move-result v0

    return v0
.end method

.method isConvertingToCustomTicket()Z
    .locals 1

    .line 131
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->isConvertingToCustomTicket:Z

    return v0
.end method

.method isCustomTicket()Z
    .locals 1

    .line 211
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicketsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isDeleteButtonVisible()Z
    .locals 1

    .line 118
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showDeleteButton:Z

    return v0
.end method

.method isDeleteVoidOrCompButtonVisible()Z
    .locals 1

    .line 163
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showDeleteButton:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCompButton:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showUncompButton:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showVoidButton:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method isPredefinedTicket()Z
    .locals 1

    .line 215
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicketsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTicketTemplateNameVisible()Z
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicket()Z

    move-result v0

    return v0
.end method

.method isUncompButtonVisible()Z
    .locals 1

    .line 155
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showUncompButton:Z

    return v0
.end method

.method isVoidButtonVisible()Z
    .locals 1

    .line 139
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->showVoidButton:Z

    return v0
.end method

.method setCardNotStoredMessageVisible(Z)V
    .locals 0

    .line 171
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCardNotStoredHint:Z

    return-void
.end method

.method setCompButtonVisible(Z)V
    .locals 0

    .line 151
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCompButton:Z

    return-void
.end method

.method setDeleteButtonVisible(Z)V
    .locals 0

    .line 122
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->showDeleteButton:Z

    return-void
.end method

.method setEditableName(Ljava/lang/String;)V
    .locals 1

    .line 202
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    .line 203
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicket()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 204
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    goto :goto_0

    .line 206
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->updateEditableName()V

    return-void
.end method

.method setNameHint(Ljava/lang/String;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->nameHint:Ljava/lang/String;

    return-void
.end method

.method setNote(Ljava/lang/String;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->note:Ljava/lang/String;

    return-void
.end method

.method setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    return-void
.end method

.method setShouldSelectName(Z)V
    .locals 0

    .line 56
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->shouldSelectName:Z

    return-void
.end method

.method setTicketTemplate(Lcom/squareup/api/items/TicketTemplate;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->updateEditableName()V

    return-void
.end method

.method setUncompButtonVisible(Z)V
    .locals 0

    .line 159
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->showUncompButton:Z

    return-void
.end method

.method setVoidButtonVisible(Z)V
    .locals 0

    .line 143
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketState;->showVoidButton:Z

    return-void
.end method

.method shouldSelectName()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->shouldSelectName:Z

    return v0
.end method

.method updateEditableName()V
    .locals 2

    .line 192
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    iget-object v1, v1, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    .line 194
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    iget-object v1, v1, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    goto :goto_0

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 237
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->selectedGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 238
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->ticketTemplate:Lcom/squareup/api/items/TicketTemplate;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 239
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 240
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->editableName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 241
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->nameHint:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 242
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->note:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 243
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->isPredefinedTicketsEnabled:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 244
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->isConvertingToCustomTicket:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 245
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCardNotStoredHint:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 246
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->showDeleteButton:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 247
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->showVoidButton:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 248
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->showCompButton:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 249
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->showUncompButton:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 250
    iget-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketState;->didShowInitialFocus:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
