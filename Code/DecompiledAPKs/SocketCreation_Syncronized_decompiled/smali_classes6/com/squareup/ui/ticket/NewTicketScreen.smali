.class public Lcom/squareup/ui/ticket/NewTicketScreen;
.super Lcom/squareup/ui/ticket/EditTicketScreen;
.source "NewTicketScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/ticket/TicketScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/NewTicketScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/NewTicketScreen$Component;,
        Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/NewTicketScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

.field protected final selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketScreen$5CstSMipMNem9PezZNHsdYuzpeY;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$NewTicketScreen$5CstSMipMNem9PezZNHsdYuzpeY;

    .line 141
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen;-><init>(Landroid/os/Parcel;)V

    .line 114
    invoke-static {}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->values()[Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/TicketGroup;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V
    .locals 1

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V

    .line 95
    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    iput-object v0, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    .line 96
    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    if-nez v0, :cond_0

    .line 97
    sget-object p1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    goto :goto_0

    .line 98
    :cond_0
    iget-boolean p1, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->forSoleGroup:Z

    if-eqz p1, :cond_1

    .line 99
    sget-object p1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    goto :goto_0

    .line 101
    :cond_1
    sget-object p1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    :goto_0
    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)V
    .locals 0

    .line 107
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/ticket/EditTicketScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    .line 109
    iput-object p4, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    return-void
.end method

.method public static forSelectingTicketTemplate(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/ui/ticket/NewTicketScreen;
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketScreen;-><init>(Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)V

    return-object v0
.end method

.method public static forSelectingTicketTemplateFromSoleGroup(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/ui/ticket/NewTicketScreen;
    .locals 2

    .line 58
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketScreen;-><init>(Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)V

    return-object v0
.end method

.method public static synthetic lambda$5CstSMipMNem9PezZNHsdYuzpeY(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/NewTicketScreen;
    .locals 1

    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/NewTicketScreen;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 135
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketScreen;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 136
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    if-nez v0, :cond_0

    .line 120
    invoke-super {p0}, Lcom/squareup/ui/ticket/EditTicketScreen;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    invoke-super {p0}, Lcom/squareup/ui/ticket/EditTicketScreen;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v1, v1, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 131
    sget v0, Lcom/squareup/orderentry/R$layout;->new_ticket_view:I

    return v0
.end method
