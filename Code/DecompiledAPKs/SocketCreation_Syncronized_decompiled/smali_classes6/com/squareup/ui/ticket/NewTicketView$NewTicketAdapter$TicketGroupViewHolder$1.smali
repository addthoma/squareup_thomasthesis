.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;

.field final synthetic val$this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)V
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;->this$2:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;->val$this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;->this$2:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;->this$2:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->access$000(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->onSelectTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    return-void
.end method
