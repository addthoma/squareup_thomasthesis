.class public final Lcom/squareup/ui/ticket/TicketSelection_Factory;
.super Ljava/lang/Object;
.source "TicketSelection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketSelection;",
        ">;"
    }
.end annotation


# instance fields
.field private final selectedTicketsInfoKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketSelection_Factory;->selectedTicketsInfoKeyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketSelection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;>;)",
            "Lcom/squareup/ui/ticket/TicketSelection_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/ticket/TicketSelection_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketSelection_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/BundleKey;)Lcom/squareup/ui/ticket/TicketSelection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;)",
            "Lcom/squareup/ui/ticket/TicketSelection;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/ticket/TicketSelection;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketSelection;-><init>(Lcom/squareup/BundleKey;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketSelection;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSelection_Factory;->selectedTicketsInfoKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/BundleKey;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketSelection_Factory;->newInstance(Lcom/squareup/BundleKey;)Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketSelection_Factory;->get()Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object v0

    return-object v0
.end method
