.class public final synthetic Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

.field private final synthetic f$1:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field private final synthetic f$2:Lcom/squareup/hudtoaster/HudToaster;

.field private final synthetic f$3:Landroid/content/Context;

.field private final synthetic f$4:I

.field private final synthetic f$5:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private final synthetic f$6:Lflow/Flow;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Landroid/content/Context;ILcom/squareup/ui/ticket/OpenTicketsRunner;Lflow/Flow;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$0:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    iput-object p2, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$1:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iput-object p3, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$2:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p4, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$3:Landroid/content/Context;

    iput p5, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$4:I

    iput-object p6, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$5:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iput-object p7, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$6:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    iget-object v0, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$0:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    iget-object v1, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$1:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v2, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$2:Lcom/squareup/hudtoaster/HudToaster;

    iget-object v3, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$3:Landroid/content/Context;

    iget v4, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$4:I

    iget-object v5, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$5:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v6, p0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;->f$6:Lflow/Flow;

    move-object v7, p1

    move v8, p2

    invoke-static/range {v0 .. v8}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Factory;->lambda$create$0(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Landroid/content/Context;ILcom/squareup/ui/ticket/OpenTicketsRunner;Lflow/Flow;Landroid/content/DialogInterface;I)V

    return-void
.end method
