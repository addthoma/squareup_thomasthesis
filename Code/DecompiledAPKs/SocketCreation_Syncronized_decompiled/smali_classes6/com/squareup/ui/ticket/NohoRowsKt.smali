.class public final Lcom/squareup/ui/ticket/NohoRowsKt;
.super Ljava/lang/Object;
.source "NohoRows.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004\u001a\u000c\u0010\u0006\u001a\u00020\u0001*\u00020\u0007H\u0002\u00a8\u0006\u0008"
    }
    d2 = {
        "setLabelAndValueForTicketRow",
        "",
        "Lcom/squareup/noho/NohoRow;",
        "labelText",
        "",
        "valueText",
        "setMaxLinesAndEllipse",
        "Landroid/widget/TextView;",
        "order-entry_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setLabelAndValueForTicketRow(Lcom/squareup/noho/NohoRow;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "$this$setLabelAndValueForTicketRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "labelText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "valueText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 14
    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 16
    check-cast p0, Landroid/view/View;

    sget p1, Lcom/squareup/noho/R$id;->label:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 17
    invoke-static {p1}, Lcom/squareup/ui/ticket/NohoRowsKt;->setMaxLinesAndEllipse(Landroid/widget/TextView;)V

    .line 18
    sget p1, Lcom/squareup/noho/R$id;->value:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 19
    invoke-static {p0}, Lcom/squareup/ui/ticket/NohoRowsKt;->setMaxLinesAndEllipse(Landroid/widget/TextView;)V

    return-void
.end method

.method private static final setMaxLinesAndEllipse(Landroid/widget/TextView;)V
    .locals 1

    const/4 v0, 0x1

    .line 23
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 24
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method
