.class public Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
.super Ljava/lang/Object;
.source "EditTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

.field cardOwnerName:Ljava/lang/String;

.field forSoleGroup:Z

.field preselectedGroup:Lcom/squareup/api/items/TicketGroup;

.field startedFromSwipe:Z

.field ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterAction(Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-object p0
.end method

.method public buildNewTicketScreen()Lcom/squareup/ui/ticket/NewTicketScreen;
    .locals 1

    .line 130
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->startedFromSwipe:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 131
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/NewTicketScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V

    return-object v0
.end method

.method public buildTicketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen;
    .locals 1

    .line 120
    new-instance v0, Lcom/squareup/ui/ticket/TicketDetailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketDetailScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V

    return-object v0
.end method

.method preselectedGroup(Lcom/squareup/api/items/TicketGroup;Z)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    .line 111
    iput-boolean p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->forSoleGroup:Z

    return-object p0
.end method

.method startFromSwipe(Ljava/lang/String;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->cardOwnerName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 100
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->startedFromSwipe:Z

    return-object p0
.end method

.method ticketAction(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->CREATE_NEW_EMPTY_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 80
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    return-object p0
.end method
