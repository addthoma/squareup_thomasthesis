.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.super Lcom/squareup/ui/RangerRecyclerAdapter;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketRecyclerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoResultsHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TextRowHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private showAllEmployeeTickets:Z

.field final synthetic this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

.field private ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

.field private ticketTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private ticketTotals:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView;)V
    .locals 0

    .line 484
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    .line 485
    const-class p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-direct {p0, p1}, Lcom/squareup/ui/RangerRecyclerAdapter;-><init>(Ljava/lang/Class;)V

    .line 486
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketTemplates:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Z
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->hasTicketCursor()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/tickets/TicketRowCursorList;
    .locals 0

    .line 111
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/Map;
    .locals 0

    .line 111
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketTotals:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/ui/Ranger;
    .locals 0

    .line 111
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/List;
    .locals 0

    .line 111
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketTemplates:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Z
    .locals 0

    .line 111
    iget-boolean p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->showAllEmployeeTickets:Z

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;ILcom/squareup/ui/ticket/BaseTicketListView$TicketRow;)Z
    .locals 0

    .line 111
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->rowFollows(ILcom/squareup/ui/ticket/BaseTicketListView$TicketRow;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/ui/Ranger;
    .locals 0

    .line 111
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    return-object p0
.end method

.method private hasTicketCursor()Z
    .locals 1

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private rowFollows(ILcom/squareup/ui/ticket/BaseTicketListView$TicketRow;)Z
    .locals 2

    const/4 v0, 0x1

    if-lez p1, :cond_0

    .line 541
    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    sub-int/2addr p1, v0

    invoke-virtual {v1, p1}, Lcom/squareup/ui/Ranger;->getRowType(I)Ljava/lang/Enum;

    move-result-object p1

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
            ")",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
            ">.RangerHolder;"
        }
    .end annotation

    .line 491
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 518
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized holder type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 515
    :pswitch_0
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 512
    :pswitch_1
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoResultsHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoResultsHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 509
    :pswitch_2
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 506
    :pswitch_3
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 503
    :pswitch_4
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 500
    :pswitch_5
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 497
    :pswitch_6
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TextRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TextRowHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 494
    :pswitch_7
    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 0

    .line 111
    check-cast p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method setList(Lcom/squareup/ui/Ranger;Lcom/squareup/tickets/TicketRowCursorList;Ljava/util/Map;Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Ranger<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
            ">;",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            "Ljava/util/Map<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;Z)V"
        }
    .end annotation

    .line 525
    iput-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    .line 526
    iput-object p3, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketTotals:Ljava/util/Map;

    .line 527
    iput-object p4, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->ticketTemplates:Ljava/util/List;

    .line 528
    iput-boolean p5, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->showAllEmployeeTickets:Z

    .line 529
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method
