.class public final Lcom/squareup/ui/ticket/MasterDetailTicketScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "MasterDetailTicketScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/ticket/TicketScreen;
.implements Lcom/squareup/ui/ticket/TicketModeScreen;
.implements Lcom/squareup/ui/main/EmvSwipePassthroughEnabler$SwipePassthrough;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Component;,
        Lcom/squareup/ui/ticket/MasterDetailTicketScreen$ComponentFactory;,
        Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketScreen$SvxlhFyqkIgMhM8yONE5vI3gm8w;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketScreen$SvxlhFyqkIgMhM8yONE5vI3gm8w;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MasterDetailTicketScreen;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-object p0
.end method

.method public static forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;
    .locals 2

    .line 41
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-direct {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V

    return-object v0
.end method

.method public static forSaveToTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->SAVE_TO_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-direct {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/MasterDetailTicketScreen;
    .locals 1

    .line 92
    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->values()[Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v0, p0

    .line 93
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 87
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketActionScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 88
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 53
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 49
    sget v0, Lcom/squareup/orderentry/R$layout;->master_detail_ticket_view:I

    return v0
.end method

.method public setTicketMode(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-void
.end method
