.class public Lcom/squareup/ui/ticket/TicketView;
.super Landroid/widget/FrameLayout;
.source "TicketView.java"


# static fields
.field private static final DISMISS_SELECTOR_ID:I

.field private static final DISMISS_TEXT_ID:I

.field private static final MOVE_ITEMS_SELECTOR_ID:I

.field private static final SAVE_SELECTOR_ID:I

.field private static final SAVE_TEXT_ID:I

.field private static final SPLIT_NEW_TICKET_SELECTOR_ID:I

.field private static final SPLIT_NEW_TICKET_TEXT_ID:I

.field private static final TEXT_COLOR_BLUE_ID:I

.field private static final TEXT_COLOR_DARK_ID:I

.field private static final TEXT_COLOR_WHITE_ID:I


# instance fields
.field private adapter:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

.field private addCustomer:Landroid/view/View;

.field private caretView:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

.field private dropDownContainer:Lcom/squareup/ui/DropDownContainer;

.field private editTicket:Landroid/view/View;

.field private headerRow:Landroid/view/View;

.field private id:Ljava/lang/String;

.field presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printBill:Landroid/view/View;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private ticketButton:Lcom/squareup/widgets/ScalingTextView;

.field private ticketName:Landroid/widget/TextView;

.field private viewCustomer:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_clear_border_top_1px:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->MOVE_ITEMS_SELECTOR_ID:I

    .line 29
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_blue:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_BLUE_ID:I

    .line 31
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->SPLIT_NEW_TICKET_SELECTOR_ID:I

    .line 33
    sget v0, Lcom/squareup/orderentry/R$string;->split_ticket_new_ticket:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->SPLIT_NEW_TICKET_TEXT_ID:I

    .line 34
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_WHITE_ID:I

    .line 36
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_clear_border_top_1px:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->SAVE_SELECTOR_ID:I

    .line 38
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_save:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->SAVE_TEXT_ID:I

    .line 40
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_clear_border_top_1px:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->DISMISS_SELECTOR_ID:I

    .line 42
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_dismiss:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->DISMISS_TEXT_ID:I

    .line 43
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    sput v0, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_DARK_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    const-class p2, Lcom/squareup/ui/ticket/SplitTicketScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/SplitTicketScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/SplitTicketScreen$Component;->inject(Lcom/squareup/ui/ticket/TicketView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketView;)Lcom/squareup/marin/widgets/MarinVerticalCaretView;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketView;->caretView:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/TicketView;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketView;->id:Ljava/lang/String;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 200
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_menu_edit_ticket:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->editTicket:Landroid/view/View;

    .line 201
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_menu_add_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->addCustomer:Landroid/view/View;

    .line 202
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_menu_view_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->viewCustomer:Landroid/widget/TextView;

    .line 203
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_menu_print_bill:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->printBill:Landroid/view/View;

    .line 204
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 205
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_ticket_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketName:Landroid/widget/TextView;

    .line 206
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_drop_down_caret:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->caretView:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    .line 207
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->headerRow:Landroid/view/View;

    .line 208
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_drop_down_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DropDownContainer;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    .line 209
    sget v0, Lcom/squareup/orderentry/R$id;->split_ticket_footer_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    return-void
.end method


# virtual methods
.method closeDropDown()V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->isDropDownVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    :cond_0
    return-void
.end method

.method initializeTicket(Ljava/lang/String;Ljava/lang/String;ZZZZZ)V
    .locals 7

    .line 142
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->id:Ljava/lang/String;

    .line 143
    new-instance v0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketView;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    new-instance v2, Lcom/squareup/util/Res$RealRes;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Ljava/lang/String;Lcom/squareup/util/Res;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->adapter:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->adapter:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->editTicket:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/ticket/TicketView$7;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketView$7;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->addCustomer:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/ticket/TicketView$8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketView$8;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->viewCustomer:Landroid/widget/TextView;

    new-instance v0, Lcom/squareup/ui/ticket/TicketView$9;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketView$9;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->printBill:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/ticket/TicketView$10;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketView$10;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    .line 170
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/ticket/TicketView;->updateTicket(Ljava/lang/String;ZZZZZ)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 68
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketView;->bindViews()V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 72
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 73
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 74
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->showSaveButton()V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->headerRow:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/TicketView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketView$1;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    new-instance v1, Lcom/squareup/ui/ticket/TicketView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketView$2;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DropDownContainer;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    return-void
.end method

.method setTicketName(Ljava/lang/String;)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showDismissButton()V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/ui/ticket/TicketView;->DISMISS_TEXT_ID:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setText(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/ui/ticket/TicketView;->DISMISS_SELECTOR_ID:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setBackgroundResource(I)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_DARK_ID:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setTextColor(I)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    new-instance v1, Lcom/squareup/ui/ticket/TicketView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketView$6;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showMoveItemsButton(Ljava/lang/String;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v0, Lcom/squareup/ui/ticket/TicketView;->MOVE_ITEMS_SELECTOR_ID:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ScalingTextView;->setBackgroundResource(I)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_BLUE_ID:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ScalingTextView;->setTextColor(I)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    new-instance v0, Lcom/squareup/ui/ticket/TicketView$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketView$3;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showNewTicketButton()V
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/ui/ticket/TicketView;->SPLIT_NEW_TICKET_TEXT_ID:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setText(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/ui/ticket/TicketView;->SPLIT_NEW_TICKET_SELECTOR_ID:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setBackgroundResource(I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_WHITE_ID:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setTextColor(I)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    new-instance v1, Lcom/squareup/ui/ticket/TicketView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketView$4;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showSaveButton()V
    .locals 3

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/ui/ticket/TicketView;->SAVE_TEXT_ID:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setText(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    sget v1, Lcom/squareup/ui/ticket/TicketView;->SAVE_SELECTOR_ID:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setBackgroundResource(I)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/ui/ticket/TicketView;->TEXT_COLOR_BLUE_ID:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setTextColor(I)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketButton:Lcom/squareup/widgets/ScalingTextView;

    new-instance v1, Lcom/squareup/ui/ticket/TicketView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketView$5;-><init>(Lcom/squareup/ui/ticket/TicketView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method toggleDropDown()V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->toggleDropDown()V

    return-void
.end method

.method updateTicket(Ljava/lang/String;ZZZZZ)V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView;->ticketName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->printBill:Landroid/view/View;

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 178
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->printBill:Landroid/view/View;

    invoke-virtual {p1, p4}, Landroid/view/View;->setEnabled(Z)V

    .line 179
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->editTicket:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->addCustomer:Landroid/view/View;

    invoke-static {p1, p5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 181
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->viewCustomer:Landroid/widget/TextView;

    invoke-static {p1, p6}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView;->adapter:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
