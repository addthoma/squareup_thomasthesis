.class Lcom/squareup/ui/ticket/TicketView$4;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketView;->showNewTicketButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketView;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketView$4;->this$0:Lcom/squareup/ui/ticket/TicketView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView$4;->this$0:Lcom/squareup/ui/ticket/TicketView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketView;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->onCreateNewTicketClicked()V

    return-void
.end method
