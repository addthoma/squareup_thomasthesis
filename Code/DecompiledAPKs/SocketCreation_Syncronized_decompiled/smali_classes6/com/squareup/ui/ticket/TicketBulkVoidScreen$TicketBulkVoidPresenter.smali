.class public Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;
.super Lcom/squareup/configure/item/VoidCompPresenter;
.source "TicketBulkVoidScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketBulkVoidScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TicketBulkVoidPresenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/configure/item/VoidCompPresenter<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final HUD_DELAY_LONG:J = 0x2bcL

.field private static final HUD_DELAY_MEDIUM:J = 0x190L

.field private static final HUD_DELAY_SHORT:J = 0x64L


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final openTicketSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private screen:Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field private final voidInProgress:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private voidingWasSlow:Z


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 93
    invoke-direct {p0, p2, p4}, Lcom/squareup/configure/item/VoidCompPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CogsCache;)V

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->voidInProgress:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 95
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->flow:Lflow/Flow;

    .line 96
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    .line 97
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 98
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 99
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    .line 100
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->openTicketSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 101
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 102
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method private finish(Lcom/squareup/tickets/Tickets$VoidResults;)V
    .locals 4

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->voidInProgress:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$FET6Y4KAM09a8W9hCRUCeF4pffk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$FET6Y4KAM09a8W9hCRUCeF4pffk;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;Lcom/squareup/tickets/Tickets$VoidResults;)V

    .line 167
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->getDelay()J

    move-result-wide v2

    .line 165
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishBulkVoidTicketsFromEditBar(Lflow/Flow;)V

    return-void
.end method

.method private getDelay()J
    .locals 2

    .line 181
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->voidingWasSlow:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2bc

    return-wide v0

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->openTicketSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x64

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x190

    :goto_0
    return-wide v0
.end method

.method private getToastText(I)Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_voided_one:I

    .line 174
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_voided_many:I

    .line 175
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 176
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method static synthetic lambda$null$0(Ljava/lang/Boolean;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1

    .line 116
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_void_in_progress:I

    invoke-static {p0, v0}, Lcom/squareup/register/widgets/GlassSpinnerState;->showDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected getCancelButtonTextResourceId()I
    .locals 1

    .line 146
    sget v0, Lcom/squareup/orderentry/R$string;->void_ticket:I

    return v0
.end method

.method protected getPrimaryButtonTextResourceId()I
    .locals 1

    .line 150
    sget v0, Lcom/squareup/configure/item/R$string;->void_initial:I

    return v0
.end method

.method protected getReasonHeaderTextResourceId()I
    .locals 1

    .line 154
    sget v0, Lcom/squareup/configure/item/R$string;->void_uppercase_reason:I

    return v0
.end method

.method public isVoidingTicket()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$finish$8$TicketBulkVoidScreen$TicketBulkVoidPresenter(Lcom/squareup/tickets/Tickets$VoidResults;)V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget p1, p1, Lcom/squareup/tickets/Tickets$VoidResults;->voidedCount:I

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->getToastText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method public synthetic lambda$null$3$TicketBulkVoidScreen$TicketBulkVoidPresenter(Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;)V
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;->SPINNING:Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->voidingWasSlow:Z

    return-void
.end method

.method public synthetic lambda$null$5$TicketBulkVoidScreen$TicketBulkVoidPresenter(Lcom/squareup/tickets/TicketsResult;)V
    .locals 0

    .line 142
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/Tickets$VoidResults;

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->finish(Lcom/squareup/tickets/Tickets$VoidResults;)V

    return-void
.end method

.method public synthetic lambda$null$6$TicketBulkVoidScreen$TicketBulkVoidPresenter(Lcom/squareup/util/Optional;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->getCheckedReason()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 142
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$p-7LOBgk6e72nUsWd1VoSj3NVf0;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$p-7LOBgk6e72nUsWd1VoSj3NVf0;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)V

    .line 141
    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidSelectedTickets(Ljava/lang/String;Lcom/squareup/protos/client/Employee;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$TicketBulkVoidScreen$TicketBulkVoidPresenter()Lrx/Subscription;
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->voidInProgress:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget-object v2, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$ujUzfpYaO-ZhLaRz8hy7xh_O3X8;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$ujUzfpYaO-ZhLaRz8hy7xh_O3X8;

    .line 115
    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lrx/Observable;->subscribe()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$2$TicketBulkVoidScreen$TicketBulkVoidPresenter(Landroid/view/View;)Lrx/Subscription;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$TicketBulkVoidScreen$TicketBulkVoidPresenter()Lrx/Subscription;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/GlassSpinner;->displayState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$M2HGi5KIlQK-k60uizJgrbbP4cM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$M2HGi5KIlQK-k60uizJgrbbP4cM;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)V

    .line 124
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onPrimaryClicked$7$TicketBulkVoidScreen$TicketBulkVoidPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->screen:Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;->access$000(Lcom/squareup/ui/ticket/TicketBulkVoidScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;

    .line 138
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 139
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$G4iCCD5hbTWP8n7gocL0iPuHEm4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$G4iCCD5hbTWP8n7gocL0iPuHEm4;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)V

    .line 140
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onCancelClicked()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 106
    invoke-super {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 107
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->screen:Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 111
    invoke-super {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 113
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$IzOUsovJz69fIfx3XWBCaugNXa4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$IzOUsovJz69fIfx3XWBCaugNXa4;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 119
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$xaqYucyolhqA5BY6CXJVVwqeZ1w;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$xaqYucyolhqA5BY6CXJVVwqeZ1w;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 122
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$eLDS0x8YaQVPSHI-kOsMMANhmBw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$eLDS0x8YaQVPSHI-kOsMMANhmBw;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onPrimaryClicked()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->voidInProgress:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 136
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$16nI4NIrxw518ME97JG274q9DE8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkVoidScreen$TicketBulkVoidPresenter$16nI4NIrxw518ME97JG274q9DE8;-><init>(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
