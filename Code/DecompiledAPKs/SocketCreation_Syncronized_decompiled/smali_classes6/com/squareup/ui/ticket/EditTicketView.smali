.class public abstract Lcom/squareup/ui/ticket/EditTicketView;
.super Landroid/widget/LinearLayout;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;,
        Lcom/squareup/ui/ticket/EditTicketView$HolderType;,
        Lcom/squareup/ui/ticket/EditTicketView$RowType;
    }
.end annotation


# instance fields
.field private adapterUpdater:Lcom/squareup/ui/SafeRecyclerAdapterUpdater;

.field private editState:Lcom/squareup/ui/ticket/EditTicketState;

.field private recyclerAdapter:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private ticketGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 366
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 363
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView;->ticketGroups:Ljava/util/List;

    .line 367
    sget p2, Lcom/squareup/orderentry/R$layout;->edit_ticket_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/ticket/EditTicketView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 368
    new-instance p1, Lcom/squareup/ui/SafeRecyclerAdapterUpdater;

    invoke-direct {p1}, Lcom/squareup/ui/SafeRecyclerAdapterUpdater;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->adapterUpdater:Lcom/squareup/ui/SafeRecyclerAdapterUpdater;

    .line 369
    sget p1, Lcom/squareup/orderentry/R$id;->edit_ticket_recycler_view:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 370
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 371
    new-instance p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-direct {p1, p0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;-><init>(Lcom/squareup/ui/ticket/EditTicketView;)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    .line 372
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/SafeRecyclerAdapterUpdater;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/ticket/EditTicketView;->adapterUpdater:Lcom/squareup/ui/SafeRecyclerAdapterUpdater;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/ticket/EditTicketView;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/ticket/EditTicketView;->ticketGroups:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method hideKeyboard()V
    .locals 0

    .line 443
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onCompClicked()V
    .locals 0

    return-void
.end method

.method protected onConvertToCustomTicketClicked()V
    .locals 0

    return-void
.end method

.method protected onDeleteClicked()V
    .locals 0

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView;->adapterUpdater:Lcom/squareup/ui/SafeRecyclerAdapterUpdater;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/SafeRecyclerAdapterUpdater;->detachedFromWindow(Landroid/view/View;)V

    .line 377
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onTicketNameChanged()V
    .locals 0

    return-void
.end method

.method protected onTicketNoteChanged()V
    .locals 0

    return-void
.end method

.method protected onUncompClicked()V
    .locals 0

    return-void
.end method

.method protected onVoidClicked()V
    .locals 0

    return-void
.end method

.method refresh()V
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->resetRanger()V

    return-void
.end method

.method setButtonsEnabled(Z)V
    .locals 1

    .line 435
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView;->recyclerAdapter:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->setButtonsEnabled(Z)V

    return-void
.end method

.method setEditState(Lcom/squareup/ui/ticket/EditTicketState;)V
    .locals 0

    .line 409
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 410
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketState;->canSave()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/EditTicketView;->setButtonsEnabled(Z)V

    .line 411
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketView;->refresh()V

    return-void
.end method

.method setTicketGroups(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)V"
        }
    .end annotation

    .line 415
    invoke-static {p1}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getSelectedGroupId()Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView;->ticketGroups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 423
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 424
    invoke-static {v2}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object v2

    .line 425
    iget-object v3, p0, Lcom/squareup/ui/ticket/EditTicketView;->ticketGroups:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    iget-object v3, v2, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v2

    goto :goto_0

    .line 430
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V

    .line 431
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketView;->refresh()V

    return-void
.end method
