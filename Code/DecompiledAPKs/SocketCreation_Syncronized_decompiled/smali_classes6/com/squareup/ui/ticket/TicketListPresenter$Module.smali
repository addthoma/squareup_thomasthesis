.class public Lcom/squareup/ui/ticket/TicketListPresenter$Module;
.super Ljava/lang/Object;
.source "TicketListPresenter.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideConfiguration(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 887
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 888
    new-instance v7, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)V

    return-object v7
.end method

.method static provideTicketListPresenter(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;
    .locals 16
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/ui/ticket/TicketListPresenter;"
        }
    .end annotation

    .line 879
    new-instance v15, Lcom/squareup/ui/ticket/TicketListPresenter;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/ticket/TicketListPresenter;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V

    return-object v15
.end method
