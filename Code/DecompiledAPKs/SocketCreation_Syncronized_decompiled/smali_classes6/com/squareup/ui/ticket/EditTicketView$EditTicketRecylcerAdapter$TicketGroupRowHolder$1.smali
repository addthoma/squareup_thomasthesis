.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

.field final synthetic val$adapterIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;I)V
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    iput p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->val$adapterIndex:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 250
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    iget-object v0, v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->ticketGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V

    .line 251
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    iget-object v0, v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$200(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->notifyItemChanged(I)V

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->this$2:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;->val$adapterIndex:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->notifyItemChanged(I)V

    return-void
.end method
