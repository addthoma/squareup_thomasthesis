.class public Lcom/squareup/ui/ticket/SplitTicketDropDownContainer;
.super Lcom/squareup/ui/DropDownContainer;
.source "SplitTicketDropDownContainer.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DropDownContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketDropDownContainer;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 21
    new-instance p1, Landroid/animation/LayoutTransition;

    invoke-direct {p1}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 p2, 0x0

    const/4 v0, 0x2

    .line 23
    invoke-virtual {p1, v0, p2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const/4 v1, 0x3

    .line 24
    invoke-virtual {p1, v1, p2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const-wide/16 v2, 0x0

    .line 25
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 26
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 27
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 28
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const-wide/16 v4, 0x7d

    .line 29
    invoke-virtual {p1, v0, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 30
    invoke-virtual {p1, v1, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 32
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v1, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 33
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v0, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 34
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 35
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 37
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 38
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 39
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 40
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 41
    invoke-virtual {p1, v1, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 42
    invoke-virtual {p1, v0, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketDropDownContainer;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    :cond_0
    return-void
.end method
