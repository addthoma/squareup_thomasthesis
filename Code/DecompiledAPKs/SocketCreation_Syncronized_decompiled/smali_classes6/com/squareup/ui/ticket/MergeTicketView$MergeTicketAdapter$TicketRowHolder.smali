.class Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "MergeTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/MergeTicketView$RowType;",
        "Lcom/squareup/ui/ticket/MergeTicketView$HolderType;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field checkBox:Lcom/squareup/marin/widgets/MarinRadioBox;

.field final synthetic this$0:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

.field ticketName:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->this$0:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    .line 64
    sget v0, Lcom/squareup/orderentry/R$layout;->merge_ticket_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->bindViews()V

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->merge_ticket_row_name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->ticketName:Lcom/squareup/marketfont/MarketTextView;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->merge_ticket_row_checkbox:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinRadioBox;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinRadioBox;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/MergeTicketView$RowType;II)V
    .locals 0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->this$0:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->access$000(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketInfo;

    .line 71
    iget-object p2, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->ticketName:Lcom/squareup/marketfont/MarketTextView;

    iget-object p3, p1, Lcom/squareup/ui/ticket/TicketInfo;->name:Ljava/lang/String;

    invoke-virtual {p2, p3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object p2, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinRadioBox;

    iget-object p3, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->this$0:Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;

    invoke-static {p3}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;->access$100(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter;)Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;

    move-result-object p3

    invoke-virtual {p3, p1}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p3

    invoke-virtual {p2, p3}, Lcom/squareup/marin/widgets/MarinRadioBox;->setChecked(Z)V

    .line 74
    iget-object p2, p0, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->itemView:Landroid/view/View;

    new-instance p3, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder$1;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder$1;-><init>(Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;Lcom/squareup/ui/ticket/TicketInfo;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/MergeTicketView$MergeTicketAdapter$TicketRowHolder;->bindRow(Lcom/squareup/ui/ticket/MergeTicketView$RowType;II)V

    return-void
.end method
