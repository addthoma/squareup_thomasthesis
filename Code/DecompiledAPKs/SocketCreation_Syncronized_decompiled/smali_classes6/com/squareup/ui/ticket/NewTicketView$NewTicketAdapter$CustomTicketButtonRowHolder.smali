.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;
.super Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomTicketButtonRowHolder"
.end annotation


# instance fields
.field private final customTicketButton:Lcom/squareup/marketfont/MarketButton;

.field final synthetic this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    .line 105
    sget v0, Lcom/squareup/orderentry/R$layout;->new_ticket_custom_ticket_button:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;I)V

    .line 106
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->custom_ticket_button:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketButton;

    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;->customTicketButton:Lcom/squareup/marketfont/MarketButton;

    .line 107
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;->customTicketButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder$1;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$CustomTicketButtonRowHolder;Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
