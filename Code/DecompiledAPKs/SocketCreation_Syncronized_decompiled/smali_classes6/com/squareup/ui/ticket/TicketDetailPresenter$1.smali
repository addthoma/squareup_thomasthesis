.class Lcom/squareup/ui/ticket/TicketDetailPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "TicketDetailPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketDetailPresenter;->onDeleteClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$1;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$1;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->deleteCurrentTicketAndReset()V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$1;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->access$000(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/ui/ticket/OpenTicketsRunner;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$1;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    iget-object v1, v1, Lcom/squareup/ui/ticket/TicketDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishDeleteTicketsFromCartMenu(Lflow/Flow;)V

    return-void
.end method
