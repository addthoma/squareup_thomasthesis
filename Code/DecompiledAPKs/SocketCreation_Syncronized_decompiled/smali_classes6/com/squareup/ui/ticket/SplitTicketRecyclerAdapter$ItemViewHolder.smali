.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;
.super Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemViewHolder"
.end annotation


# instance fields
.field private final checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private final itemRowView:Landroid/widget/LinearLayout;

.field private final orderItemView:Lcom/squareup/ui/cart/CartEntryView;

.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 190
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 191
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_item_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->itemRowView:Landroid/widget/LinearLayout;

    .line 192
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_item:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->orderItemView:Lcom/squareup/ui/cart/CartEntryView;

    .line 193
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_check_box:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;)Lcom/squareup/marin/widgets/MarinCheckBox;
    .locals 0

    .line 184
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-object p0
.end method


# virtual methods
.method public bind()V
    .locals 10

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->getPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$000(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;I)I

    move-result v0

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getItem(Ljava/lang/String;I)Lcom/squareup/checkout/CartItem;

    move-result-object v4

    .line 199
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->ticketMutable(Ljava/lang/String;)Z

    move-result v1

    xor-int/lit8 v8, v1, 0x1

    .line 200
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->itemRowView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    if-eqz v8, :cond_0

    .line 203
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v3}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v5}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->isEntrySelected(Ljava/lang/String;IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinCheckBox;->setChecked(Z)V

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->itemRowView:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$300()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->itemRowView:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;I)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinCheckBox;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->itemRowView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const/4 v9, 0x0

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 221
    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v3

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getCartDiningOption(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface/range {v3 .. v9}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->orderItem(Lcom/squareup/checkout/CartItem;ZLjava/lang/String;ZZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->orderItemView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-void
.end method
