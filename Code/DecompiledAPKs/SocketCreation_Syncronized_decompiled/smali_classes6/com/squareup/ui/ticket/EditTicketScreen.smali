.class public abstract Lcom/squareup/ui/ticket/EditTicketScreen;
.super Lcom/squareup/ui/ticket/InTicketScope;
.source "EditTicketScreen.java"

# interfaces
.implements Lcom/squareup/ui/ticket/TicketScreen;
.implements Lcom/squareup/ui/main/EmvSwipePassthroughEnabler$SwipePassthrough;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;,
        Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;,
        Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;,
        Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;,
        Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;,
        Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    }
.end annotation


# instance fields
.field private final afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

.field private final ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 205
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketScope;-><init>()V

    .line 206
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->values()[Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 207
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->values()[Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    aget-object p1, v0, p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V
    .locals 1

    .line 195
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketScope;-><init>()V

    .line 196
    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 197
    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)V
    .locals 0

    .line 200
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketScope;-><init>()V

    .line 201
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 202
    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/EditTicketScreen;)Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/EditTicketScreen;)Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-object p0
.end method

.method static forCreatingEmptyNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    .locals 2

    .line 140
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;-><init>()V

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->CREATE_NEW_EMPTY_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 141
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->ticketAction(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 142
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction(Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;
    .locals 2

    .line 150
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;-><init>()V

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 151
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->ticketAction(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 152
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction(Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 453
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 454
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 455
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
