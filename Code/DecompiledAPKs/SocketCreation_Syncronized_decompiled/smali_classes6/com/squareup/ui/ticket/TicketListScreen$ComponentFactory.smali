.class public Lcom/squareup/ui/ticket/TicketListScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "TicketListScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 72
    const-class v0, Lcom/squareup/ui/ticket/TicketActionScope$Component;

    .line 73
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketActionScope$Component;

    .line 74
    check-cast p2, Lcom/squareup/ui/ticket/TicketListScreen;

    .line 75
    new-instance v0, Lcom/squareup/ui/ticket/TicketListScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/ticket/TicketListScreen$Module;-><init>(Lcom/squareup/ui/ticket/TicketListScreen;)V

    .line 76
    invoke-interface {p1, v0}, Lcom/squareup/ui/ticket/TicketActionScope$Component;->ticketList(Lcom/squareup/ui/ticket/TicketListScreen$Module;)Lcom/squareup/ui/ticket/TicketListScreen$Component;

    move-result-object p1

    return-object p1
.end method
