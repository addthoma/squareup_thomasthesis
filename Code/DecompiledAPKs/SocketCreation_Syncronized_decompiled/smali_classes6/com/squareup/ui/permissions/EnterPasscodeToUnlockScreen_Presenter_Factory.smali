.class public final Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "EnterPasscodeToUnlockScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodesSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->timecardsLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;)",
            "Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;
    .locals 11

    .line 79
    new-instance v10, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;
    .locals 10

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/PasscodesSettings;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/merchantimages/CuratedImage;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->timecardsLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->newInstance(Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen_Presenter_Factory;->get()Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
