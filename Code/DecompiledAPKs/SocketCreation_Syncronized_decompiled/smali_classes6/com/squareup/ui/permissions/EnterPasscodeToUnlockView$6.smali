.class Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;
.super Ljava/lang/Object;
.source "EnterPasscodeToUnlockView.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->lambda$setBackgroundImage$0(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Failed to load curated image"

    .line 199
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSuccess()V
    .locals 3

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$100(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$100(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 177
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$200(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 179
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$300(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/ui/StarGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/ui/StarGroup;->setCorrectColor(I)V

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$400(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/padlock/Padlock;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/padlock/Padlock;->setDigitColor(I)V

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$400(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/padlock/Padlock;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/padlock/Padlock;->setLineColor(I)V

    .line 182
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$400(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/padlock/Padlock;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/padlock/Padlock;->setDeleteColor(I)V

    .line 184
    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_black_transparent_fifty_border_white_transparent_twenty:I

    .line 186
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$500(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 187
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$500(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 188
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$600(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 189
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$600(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 192
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$700(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 193
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$700(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 194
    iget-object v2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v2}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$800(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;->this$0:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-static {v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->access$800(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    return-void
.end method
