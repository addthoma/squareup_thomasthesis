.class public Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;
.super Landroid/widget/FrameLayout;
.source "EnterPasscodeToUnlockView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private clockInOutButton:Lcom/squareup/marketfont/MarketButton;

.field curatedImage:Lcom/squareup/merchantimages/CuratedImage;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private guestUnlockButton:Lcom/squareup/glyph/SquareGlyphView;

.field private header:Landroid/widget/FrameLayout;

.field private legacyClockInOutButton:Lcom/squareup/marketfont/MarketButton;

.field private legacyGuestUnlockButton:Lcom/squareup/marketfont/MarketButton;

.field private legacyHeader:Landroid/widget/FrameLayout;

.field private merchantImage:Landroid/widget/ImageView;

.field private padlock:Lcom/squareup/padlock/Padlock;

.field private passcodeEnabled:Z

.field presenter:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private starGroup:Lcom/squareup/ui/StarGroup;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 49
    iput-boolean p2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->passcodeEnabled:Z

    .line 53
    const-class p2, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Component;->inject(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Z
    .locals 0

    .line 30
    iget-boolean p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->passcodeEnabled:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Landroid/widget/ImageView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->merchantImage:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Landroid/widget/TextView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->title:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/ui/StarGroup;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->starGroup:Lcom/squareup/ui/StarGroup;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/padlock/Padlock;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyGuestUnlockButton:Lcom/squareup/marketfont/MarketButton;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyClockInOutButton:Lcom/squareup/marketfont/MarketButton;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/glyph/SquareGlyphView;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->guestUnlockButton:Lcom/squareup/glyph/SquareGlyphView;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)Lcom/squareup/marketfont/MarketButton;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->clockInOutButton:Lcom/squareup/marketfont/MarketButton;

    return-object p0
.end method


# virtual methods
.method incorrectPasscode()V
    .locals 1

    const/4 v0, 0x1

    .line 129
    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setPasscodePadEnabled(Z)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0}, Lcom/squareup/ui/StarGroup;->wiggleClear()V

    return-void
.end method

.method public synthetic lambda$setBackgroundImage$0$EnterPasscodeToUnlockView(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
    .locals 0

    .line 172
    iget-object p2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->merchantImage:Landroid/widget/ImageView;

    new-instance p3, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;

    invoke-direct {p3, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$6;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    invoke-virtual {p1, p2, p3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 118
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->presenter:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->presenter:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->merchantImage:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    .line 125
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 57
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 59
    sget v0, Lcom/squareup/ui/permissions/R$id;->passcode_unlock_merchant_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->merchantImage:Landroid/widget/ImageView;

    .line 62
    sget v0, Lcom/squareup/ui/permissions/R$id;->legacy_enter_passcode_to_unlock_header:I

    .line 63
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyHeader:Landroid/widget/FrameLayout;

    .line 65
    sget v0, Lcom/squareup/ui/permissions/R$id;->legacy_clock_in_out_button:I

    .line 66
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyClockInOutButton:Lcom/squareup/marketfont/MarketButton;

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyClockInOutButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$1;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    sget v0, Lcom/squareup/ui/permissions/R$id;->legacy_guest_unlock:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyGuestUnlockButton:Lcom/squareup/marketfont/MarketButton;

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyGuestUnlockButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$2;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    sget v0, Lcom/squareup/ui/permissions/R$id;->enter_passcode_to_unlock_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->header:Landroid/widget/FrameLayout;

    .line 82
    sget v0, Lcom/squareup/ui/permissions/R$id;->clock_in_out_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->clockInOutButton:Lcom/squareup/marketfont/MarketButton;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->clockInOutButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$3;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    sget v0, Lcom/squareup/ui/permissions/R$id;->guest_unlock:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->guestUnlockButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->guestUnlockButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$4;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    sget v0, Lcom/squareup/ui/permissions/R$id;->passcode_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->title:Landroid/widget/TextView;

    .line 97
    sget v0, Lcom/squareup/ui/permissions/R$id;->passcode_unlock_pad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, v3}, Lcom/squareup/padlock/Padlock;->setClearEnabled(Z)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    new-instance v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView$5;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 114
    sget v0, Lcom/squareup/ui/permissions/R$id;->star_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StarGroup;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->starGroup:Lcom/squareup/ui/StarGroup;

    return-void
.end method

.method setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->merchantImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockView$c9l9mlBIEf_6JUXxK_57jztW2Vk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockView$c9l9mlBIEf_6JUXxK_57jztW2Vk;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method setClearEnabled(Z)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setClearEnabled(Z)V

    return-void
.end method

.method setClockInOutButtonVisible(Z)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyClockInOutButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->clockInOutButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setExpectedStars(I)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    return-void
.end method

.method setGuestButtonVisible(Z)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyGuestUnlockButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->guestUnlockButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setLegacyHeaderVisible(Z)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->legacyHeader:Landroid/widget/FrameLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->header:Landroid/widget/FrameLayout;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPasscodePadEnabled(Z)V
    .locals 1

    .line 134
    iput-boolean p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->passcodeEnabled:Z

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->padlock:Lcom/squareup/padlock/Padlock;

    iget-boolean v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->passcodeEnabled:Z

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setDigitsEnabled(Z)V

    return-void
.end method

.method setStarCount(I)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    return-void
.end method

.method setTitleText(Ljava/lang/String;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
