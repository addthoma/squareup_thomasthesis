.class public Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PermissionDeniedScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/PermissionDeniedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/permissions/PermissionDeniedView;",
        ">;"
    }
.end annotation


# static fields
.field private static final PASSCODE_LENGTH:I = 0x4


# instance fields
.field private final digits:Ljava/lang/StringBuilder;

.field private enabled:Z

.field private final flow:Lflow/Flow;

.field private final gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private scope:Lmortar/MortarScope;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 100
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    .line 96
    iput-boolean v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 102
    iput-object p2, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;Z)V
    .locals 0

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->dismiss(Z)V

    return-void
.end method

.method private dismiss(Z)V
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PermissionGatekeeper;->dismiss(Z)V

    return-void
.end method

.method private passcodeAttempt(Ljava/lang/String;)V
    .locals 3

    .line 177
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    .line 178
    iput-boolean v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    .line 179
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/permissions/PermissionDeniedView;

    .line 180
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setPasscodePadEnabled(Z)V

    .line 181
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setClearEnabled(Z)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->scope:Lmortar/MortarScope;

    iget-object v1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v2, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedScreen$Presenter$fMoU_IZkQJb3ovC0SXAoCj7zIdY;

    invoke-direct {v2, p0}, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedScreen$Presenter$fMoU_IZkQJb3ovC0SXAoCj7zIdY;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;)V

    invoke-virtual {v1, p1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->attemptPasscode(Ljava/lang/String;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private updateStars()V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 196
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/permissions/PermissionDeniedView;

    .line 197
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setStarCount(I)V

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 198
    :goto_0
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setClearEnabled(Z)V

    return-void
.end method


# virtual methods
.method allowAccountOwnerOrAdminPasscodeOnly()Z
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->allowAccountOwnerOrAdminPasscodeOnly()Z

    move-result v0

    return v0
.end method

.method allowAccountOwnerPasscodeOnly()Z
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->allowAccountOwnerPasscodeOnly()Z

    move-result v0

    return v0
.end method

.method cancel()V
    .locals 1

    const/4 v0, 0x0

    .line 167
    invoke-direct {p0, v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->dismiss(Z)V

    return-void
.end method

.method initPasscodePad()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->shouldAskForPasscode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/permissions/PermissionDeniedView;

    const/4 v1, 0x4

    .line 128
    invoke-virtual {v0, v1}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setExpectedStars(I)V

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setStarCount(I)V

    .line 131
    iget-boolean v2, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    invoke-virtual {v0, v2}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setPasscodePadEnabled(Z)V

    .line 132
    iget-boolean v2, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    if-eqz v2, :cond_0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setClearEnabled(Z)V

    return-void

    .line 123
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Should not ask for passcode for this employee."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic lambda$onEnterScope$0$PermissionDeniedScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->flow:Lflow/Flow;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$passcodeAttempt$1$PermissionDeniedScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 184
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x1

    .line 185
    iput-boolean v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    .line 186
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 187
    invoke-direct {p0, v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->dismiss(Z)V

    goto :goto_0

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/permissions/PermissionDeniedView;

    invoke-virtual {p1}, Lcom/squareup/ui/permissions/PermissionDeniedView;->incorrectPasscode()V

    :goto_0
    return-void
.end method

.method onClearPressed()V
    .locals 3

    .line 160
    iget-boolean v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    if-nez v0, :cond_0

    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 163
    invoke-direct {p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->updateStars()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->scope:Lmortar/MortarScope;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 108
    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->shouldBeShowingPermissionDeniedScreen()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedScreen$Presenter$tKwvgHr3v4A_IBjFLavxUaI2_LA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedScreen$Presenter$tKwvgHr3v4A_IBjFLavxUaI2_LA;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;)V

    .line 109
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 107
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->dismissPermissionDeniedScreen()V

    return-void
.end method

.method onKeyPressed(C)V
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->enabled:Z

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->updateStars()V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->passcodeAttempt(Ljava/lang/String;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_1
    return-void
.end method

.method shouldAskForPasscode()Z
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->gatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->shouldAskForPasscode()Z

    move-result v0

    return v0
.end method
