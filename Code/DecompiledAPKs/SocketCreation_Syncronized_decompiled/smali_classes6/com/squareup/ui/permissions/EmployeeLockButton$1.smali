.class Lcom/squareup/ui/permissions/EmployeeLockButton$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EmployeeLockButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/permissions/EmployeeLockButton;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/permissions/EmployeeLockButton;


# direct methods
.method constructor <init>(Lcom/squareup/ui/permissions/EmployeeLockButton;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/permissions/EmployeeLockButton$1;->this$0:Lcom/squareup/ui/permissions/EmployeeLockButton;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/permissions/EmployeeLockButton$1;->this$0:Lcom/squareup/ui/permissions/EmployeeLockButton;

    iget-object p1, p1, Lcom/squareup/ui/permissions/EmployeeLockButton;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->lockButtonClicked()V

    return-void
.end method
