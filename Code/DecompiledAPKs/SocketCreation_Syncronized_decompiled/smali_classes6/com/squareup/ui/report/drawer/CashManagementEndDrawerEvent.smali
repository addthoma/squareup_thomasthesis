.class public Lcom/squareup/ui/report/drawer/CashManagementEndDrawerEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "CashManagementEndDrawerEvent.java"


# instance fields
.field public final contains_description:Z

.field public final entered_actual_amount:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_ENDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 14
    iput-boolean p1, p0, Lcom/squareup/ui/report/drawer/CashManagementEndDrawerEvent;->contains_description:Z

    .line 15
    iput-boolean p2, p0, Lcom/squareup/ui/report/drawer/CashManagementEndDrawerEvent;->entered_actual_amount:Z

    return-void
.end method
