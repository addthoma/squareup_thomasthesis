.class public interface abstract Lcom/squareup/ui/seller/SellerScope$BaseComponent;
.super Ljava/lang/Object;
.source "SellerScope.java"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;
.implements Lcom/squareup/ui/crm/ChooseCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/main/LibraryCreateNewItemDialog$ParentComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/seller/SellerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseComponent"
.end annotation


# virtual methods
.method public abstract addNoteScreen()Lcom/squareup/ui/main/AddNoteScreen$Component;
.end method

.method public abstract barcodeNotFoundScreen()Lcom/squareup/ui/BarcodeNotFoundScreen$Component;
.end method

.method public abstract cartDiscountsScreen()Lcom/squareup/ui/cart/CartDiscountsScreen$Component;
.end method

.method public abstract cartTaxesScreen()Lcom/squareup/ui/cart/CartTaxesScreen$Component;
.end method

.method public abstract changeHudToaster()Lcom/squareup/tenderpayment/ChangeHudToaster;
.end method

.method public abstract clockSkewScreen()Lcom/squareup/orderentry/ClockSkewScreen$Component;
.end method

.method public abstract discountEntryMoneyScreen()Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Component;
.end method

.method public abstract discountEntryPercentScreen()Lcom/squareup/ui/library/DiscountEntryPercentScreen$Component;
.end method

.method public abstract favoritePageScreen()Lcom/squareup/orderentry/FavoritePageScreen$Component;
.end method

.method public abstract favoritesTileItemSelectionEvents()Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;
.end method

.method public abstract giftCardActivationScreen()Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen$Component;
.end method

.method public abstract itemListScreen()Lcom/squareup/orderentry/category/ItemListScreen$Component;
.end method

.method public abstract pageLabelEditScreen()Lcom/squareup/orderentry/PageLabelEditScreen$Component;
.end method

.method public abstract priceEntryScreen()Lcom/squareup/ui/library/PriceEntryScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/seller/SellerScopeRunner;
.end method

.method public abstract ticketScope()Lcom/squareup/ui/ticket/TicketScope$Component;
.end method
