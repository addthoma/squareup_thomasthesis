.class Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "StartCashDrawerShiftLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;


# direct methods
.method constructor <init>(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;->this$0:Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;->this$0:Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    invoke-static {p1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->access$000(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;->this$0:Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    invoke-static {v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->access$100(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;->this$0:Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    iget-object v0, v0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->presenter:Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;

    iget-object v1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;->this$0:Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    iget-object v1, v1, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v1, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->startCashDrawerShift(Lcom/squareup/protos/common/Money;)V

    return-void
.end method
