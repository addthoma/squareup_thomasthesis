.class public final Lcom/squareup/ui/seller/SellerScopeWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SellerScopeWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowProps;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowState;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0002\u0010\rJ\u001a\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00022\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016JN\u0010\u0012\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00032\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0003H\u0016R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/seller/SellerScopeWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowProps;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowState;",
        "Lcom/squareup/ui/seller/SellerScopeWorkflowOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "separatedPrintoutsWorkflow",
        "Ldagger/Lazy;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
        "(Ldagger/Lazy;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final separatedPrintoutsWorkflow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "separatedPrintoutsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/seller/SellerScopeWorkflow;->separatedPrintoutsWorkflow:Ldagger/Lazy;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/seller/SellerScopeWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/seller/SellerScopeWorkflowState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    instance-of p2, p1, Lcom/squareup/ui/seller/SellerScopeWorkflowProps$StartSeparatedPrintouts;

    if-eqz p2, :cond_0

    new-instance p2, Lcom/squareup/ui/seller/SellerScopeWorkflowState$RunningSeparatedPrintouts;

    check-cast p1, Lcom/squareup/ui/seller/SellerScopeWorkflowProps$StartSeparatedPrintouts;

    invoke-virtual {p1}, Lcom/squareup/ui/seller/SellerScopeWorkflowProps$StartSeparatedPrintouts;->getInput()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/ui/seller/SellerScopeWorkflowState$RunningSeparatedPrintouts;-><init>(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    check-cast p2, Lcom/squareup/ui/seller/SellerScopeWorkflowState;

    return-object p2

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/seller/SellerScopeWorkflowProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/seller/SellerScopeWorkflow;->initialState(Lcom/squareup/ui/seller/SellerScopeWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/seller/SellerScopeWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/seller/SellerScopeWorkflowProps;

    check-cast p2, Lcom/squareup/ui/seller/SellerScopeWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/seller/SellerScopeWorkflow;->render(Lcom/squareup/ui/seller/SellerScopeWorkflowProps;Lcom/squareup/ui/seller/SellerScopeWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/seller/SellerScopeWorkflowProps;Lcom/squareup/ui/seller/SellerScopeWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowProps;",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowState;",
            "-",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    instance-of p1, p2, Lcom/squareup/ui/seller/SellerScopeWorkflowState$RunningSeparatedPrintouts;

    if-eqz p1, :cond_0

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/seller/SellerScopeWorkflow;->separatedPrintoutsWorkflow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "separatedPrintoutsWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    check-cast p2, Lcom/squareup/ui/seller/SellerScopeWorkflowState$RunningSeparatedPrintouts;

    invoke-virtual {p2}, Lcom/squareup/ui/seller/SellerScopeWorkflowState$RunningSeparatedPrintouts;->getInput()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object v3

    const/4 v4, 0x0

    .line 58
    sget-object p1, Lcom/squareup/ui/seller/SellerScopeWorkflow$render$1;->INSTANCE:Lcom/squareup/ui/seller/SellerScopeWorkflow$render$1;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 56
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/seller/SellerScopeWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/seller/SellerScopeWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerScopeWorkflow;->snapshotState(Lcom/squareup/ui/seller/SellerScopeWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
