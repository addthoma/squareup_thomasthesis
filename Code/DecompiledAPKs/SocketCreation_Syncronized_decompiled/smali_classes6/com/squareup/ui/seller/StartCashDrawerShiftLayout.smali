.class public Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;
.super Landroid/view/ViewGroup;
.source "StartCashDrawerShiftLayout.java"


# static fields
.field private static final DIALOG_INDEX:I = 0x1

.field private static final MAX_CHILD_COUNT:I = 0x2

.field private static final STATIC_CONTENT_INDEX:I


# instance fields
.field private final appletsDrawerButtonRect:Landroid/graphics/Rect;

.field private final dialogRect:Landroid/graphics/Rect;

.field private dialogView:Landroid/view/View;

.field private drawerButton:Lcom/squareup/orderentry/OrderEntryDrawerButton;

.field private employeeLockButton:Landroid/view/View;

.field private final employeeLockButtonRect:Landroid/graphics/Rect;

.field private interceptSideTouchEvents:Z

.field private final panelHeight:I

.field presenter:Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final scrimColor:I

.field private scrimOpacity:F

.field private startButton:Lcom/squareup/marketfont/MarketButton;

.field private startingCashView:Lcom/squareup/widgets/SelectableEditText;

.field private staticContent:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    .line 57
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->appletsDrawerButtonRect:Landroid/graphics/Rect;

    .line 58
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButtonRect:Landroid/graphics/Rect;

    const/4 p2, 0x0

    .line 61
    iput p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->scrimOpacity:F

    .line 76
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    .line 79
    iget p1, p1, Landroid/graphics/Point;->y:I

    iput p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->panelHeight:I

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 82
    sget p2, Lcom/squareup/marin/R$color;->marin_screen_scrim:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->scrimColor:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startingCashView:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)Landroid/view/View;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public dismissDialog()V
    .locals 2

    const/4 v0, 0x0

    .line 228
    iput-boolean v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->interceptSideTouchEvents:Z

    .line 229
    iget-object v1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->removeView(Landroid/view/View;)V

    const/4 v1, 0x0

    .line 230
    iput-object v1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    const/4 v1, 0x0

    .line 231
    iput v1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->scrimOpacity:F

    .line 233
    iget-object v1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->drawerButton:Lcom/squareup/orderentry/OrderEntryDrawerButton;

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->setShowingThroughScrim(Z)V

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 4

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->staticContent:Landroid/view/View;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 162
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    if-eqz v0, :cond_1

    .line 166
    iget-object v2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 167
    iget-object v2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    sget-object v3, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 171
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p2

    if-eqz v0, :cond_2

    .line 174
    iget p3, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->scrimOpacity:F

    const/4 p4, 0x0

    cmpl-float p4, p3, p4

    if-lez p4, :cond_2

    iget p4, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->scrimColor:I

    const/4 v0, -0x1

    if-eq p4, v0, :cond_2

    const/high16 v0, -0x1000000

    and-int/2addr v0, p4

    ushr-int/lit8 v0, v0, 0x18

    int-to-float v0, v0

    mul-float v0, v0, p3

    float-to-int p3, v0

    shl-int/lit8 p3, p3, 0x18

    const v0, 0xffffff

    and-int/2addr p4, v0

    or-int/2addr p3, p4

    .line 177
    iget-object p4, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->appletsDrawerButtonRect:Landroid/graphics/Rect;

    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, p4, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 178
    iget-object p4, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButtonRect:Landroid/graphics/Rect;

    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, p4, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 179
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 183
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return p2
.end method

.method public getStartingCashAmount()Ljava/lang/String;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startingCashView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isShowingDialog()Z
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$StartCashDrawerShiftLayout(Landroid/view/View;II)V
    .locals 0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->drawerButton:Lcom/squareup/orderentry/OrderEntryDrawerButton;

    iget-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->appletsDrawerButtonRect:Landroid/graphics/Rect;

    invoke-static {p1, p0, p2}, Lcom/squareup/util/Views;->getBoundsRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$StartCashDrawerShiftLayout(Landroid/view/View;II)V
    .locals 0

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButton:Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButtonRect:Landroid/graphics/Rect;

    invoke-static {p1, p0, p2}, Lcom/squareup/util/Views;->getBoundsRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 108
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->presenter:Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 86
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const/4 v0, 0x0

    .line 87
    invoke-virtual {p0, v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->staticContent:Landroid/view/View;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 94
    sget v0, Lcom/squareup/orderentry/R$id;->drawer_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryDrawerButton;

    iput-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->drawerButton:Lcom/squareup/orderentry/OrderEntryDrawerButton;

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->drawerButton:Lcom/squareup/orderentry/OrderEntryDrawerButton;

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$StartCashDrawerShiftLayout$NREaTUtpyJyVMwdOy49NqPucZqE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$StartCashDrawerShiftLayout$NREaTUtpyJyVMwdOy49NqPucZqE;-><init>(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 99
    sget v0, Lcom/squareup/orderentry/R$id;->employee_lock_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButton:Landroid/view/View;

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$StartCashDrawerShiftLayout$QXDyf3vgvy8HR6CVVBWADNK7uTU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$StartCashDrawerShiftLayout$QXDyf3vgvy8HR6CVVBWADNK7uTU;-><init>(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->presenter:Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 188
    iget-boolean v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->interceptSideTouchEvents:Z

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 190
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 191
    iget-object v2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    .line 192
    iget-object v3, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->appletsDrawerButtonRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 193
    iget-object v4, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->employeeLockButtonRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 198
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .line 137
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getMeasuredWidth()I

    move-result p1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getMeasuredHeight()I

    move-result p2

    .line 139
    div-int/lit8 p4, p1, 0x2

    .line 140
    iget p5, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->panelHeight:I

    div-int/lit8 v0, p5, 0x2

    sub-int/2addr p5, p2

    sub-int/2addr v0, p5

    .line 143
    iget-object p5, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->staticContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p5, v1, v1, p1, p2}, Landroid/view/View;->layout(IIII)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 147
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    .line 148
    iget-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    .line 149
    div-int/lit8 p5, p1, 0x2

    sub-int/2addr p4, p5

    .line 150
    div-int/lit8 p5, p2, 0x2

    sub-int/2addr v0, p5

    add-int/2addr v0, p3

    add-int/2addr p1, p4

    add-int/2addr p2, v0

    .line 153
    iget-object p3, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->left:I

    iget-object p3, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    iget p3, p3, Landroid/graphics/Rect;->top:I

    iget-object p4, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    iget p4, p4, Landroid/graphics/Rect;->right:I

    iget-object p5, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogRect:Landroid/graphics/Rect;

    iget p5, p5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 113
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 114
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 117
    iget-object v2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->staticContent:Landroid/view/View;

    .line 118
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, 0x0

    invoke-static {p1, v4, v3}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getChildMeasureSpec(III)I

    move-result p1

    iget-object v3, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->staticContent:Landroid/view/View;

    .line 119
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v4, v3}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getChildMeasureSpec(III)I

    move-result p2

    .line 117
    invoke-virtual {v2, p1, p2}, Landroid/view/View;->measure(II)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iget p1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 p2, 0x40000000    # 2.0f

    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    const/high16 p2, -0x80000000

    .line 126
    invoke-static {v1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 127
    iget-object v2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    invoke-virtual {v2, p1, p2}, Landroid/view/View;->measure(II)V

    .line 133
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public setDefaultStartingCash(Ljava/lang/String;)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startingCashView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showDialog()V
    .locals 4

    .line 203
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 204
    invoke-virtual {p0, v1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->removeViewAt(I)V

    .line 207
    :cond_0
    iput-boolean v1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->interceptSideTouchEvents:Z

    .line 208
    sget v0, Lcom/squareup/orderentry/R$layout;->start_drawer_modal:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    sget v2, Lcom/squareup/orderentry/R$id;->start_drawer_starting_cash:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startingCashView:Lcom/squareup/widgets/SelectableEditText;

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startingCashView:Lcom/squareup/widgets/SelectableEditText;

    sget-object v3, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    sget v2, Lcom/squareup/orderentry/R$id;->start_drawer_button:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startButton:Lcom/squareup/marketfont/MarketButton;

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->startButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v2, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout$1;-><init>(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dialogView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->addView(Landroid/view/View;)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 222
    iput v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->scrimOpacity:F

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->drawerButton:Lcom/squareup/orderentry/OrderEntryDrawerButton;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->setShowingThroughScrim(Z)V

    return-void
.end method
