.class Lcom/squareup/ui/print/PrintErrorPopupPresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "PrintErrorPopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/print/PrintErrorPopupPresenter;-><init>(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSpooler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/print/PrintErrorPopupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$1;->this$0:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 0

    .line 156
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$1;->this$0:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-static {p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->access$000(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 154
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter$1;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
