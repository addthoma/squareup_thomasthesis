.class public final Lcom/squareup/ui/orderhub/OrderHubApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "OrderHubApplet.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/OrderHubApplet$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubApplet.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubApplet.kt\ncom/squareup/ui/orderhub/OrderHubApplet\n*L\n1#1,117:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \'2\u00020\u0001:\u0001\'B\'\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0016J\u0018\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0008\u0010\u001a\u001a\u00020\u000bH\u0016J\u0010\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001cH\u0016J\u0010\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0006\u0010!\u001a\u00020\"J\u000e\u0010#\u001a\u00020\"2\u0006\u0010$\u001a\u00020\u0010J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u0013H\u0016R\u0014\u0010\n\u001a\u00020\u000bX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u001c\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubApplet;",
        "Lcom/squareup/applet/HistoryFactoryApplet;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "mainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "badgeCount",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "badge",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/applet/Applet$Badge;",
        "getHomeScreens",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "currentHistory",
        "Lflow/History;",
        "getIntentScreenExtra",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "getText",
        "resources",
        "Landroid/content/res/Resources;",
        "resetBadgeCount",
        "",
        "setBadgeCount",
        "newCount",
        "visibility",
        "",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/OrderHubApplet$Companion;

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "ORDER_HUB"

.field private static final workflowScreens:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/legacy/Screen$Key;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final analyticsName:Ljava/lang/String;

.field private final badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubApplet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/OrderHubApplet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubApplet;->Companion:Lcom/squareup/ui/orderhub/OrderHubApplet$Companion;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/workflow/legacy/Screen$Key;

    .line 104
    sget-object v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;->Companion:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 105
    sget-object v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->Companion:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 106
    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->Companion:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 107
    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen;->Companion:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 108
    sget-object v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;->Companion:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 109
    sget-object v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 110
    sget-object v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 111
    sget-object v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;->Companion:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 112
    sget-object v1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;->Companion:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 113
    sget-object v1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;->Companion:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 103
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubApplet;->workflowScreens:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThreadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    iput-object p2, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const/4 p1, 0x0

    .line 42
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(0)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const-string p1, "order-hub"

    .line 78
    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->analyticsName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "badgeCount.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/orderhub/OrderHubApplet;->toBadge(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 65
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object p1

    :goto_0
    const-string v0, "builder"

    .line 67
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 68
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    .line 69
    instance-of v1, v0, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/ui/orderhub/OrderHubApplet;->workflowScreens:Ljava/util/Set;

    check-cast v1, Ljava/lang/Iterable;

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    iget-object v0, v0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 72
    :cond_1
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 75
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_2

    :cond_3
    invoke-static {p1}, Lcom/squareup/container/Histories;->toList(Lflow/History$Builder;)Ljava/util/List;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "ORDER_HUB"

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CHECK_EMPLOYEE_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_ORDERS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_applet_name:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.string.orderhub_applet_name)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final resetBadgeCount()V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final setBadgeCount(I)V
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 49
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(ORDERHUB_APPLET_ROLLOUT)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
