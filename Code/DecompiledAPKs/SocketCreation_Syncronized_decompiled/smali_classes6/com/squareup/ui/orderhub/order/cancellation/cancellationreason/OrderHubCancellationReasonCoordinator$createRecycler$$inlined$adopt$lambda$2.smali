.class final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubCancellationReasonCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubCancellationReasonCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubCancellationReasonCoordinator.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$1$2\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,165:1\n1103#2,7:166\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubCancellationReasonCoordinator.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$1$2\n*L\n125#1,7:166\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "index",
        "",
        "item",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;",
        "row",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "invoke",
        "com/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

    check-cast p3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->invoke(ILcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;Lcom/squareup/noho/NohoCheckableRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 3

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "row"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$getRes$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/proto/CancellationReasonsKt;->getReasonName(Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$getRes$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$inventorySubtext(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$getChosenReason$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 125
    check-cast p3, Landroid/view/View;

    .line 166
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;I)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
