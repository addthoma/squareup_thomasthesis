.class final Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->handleResults(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubWorkflowRunner.kt\ncom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,86:1\n152#2:87\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubWorkflowRunner.kt\ncom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1\n*L\n56#1:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;->this$0:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;->invoke(Lcom/squareup/ui/orderhub/OrderHubResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/orderhub/OrderHubResult;)V
    .locals 4

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    instance-of v0, p1, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;->this$0:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->access$getOrderHubRefundFlowState$p(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    move-result-object v0

    .line 49
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v2

    .line 51
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v3

    .line 52
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubResult$CanceledOrder;->getSelectedLineItems()Ljava/util/Map;

    move-result-object p1

    .line 48
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->initiateRefundFlowForCancellation(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    goto :goto_0

    .line 55
    :cond_0
    instance-of p1, p1, Lcom/squareup/ui/orderhub/OrderHubResult$ExitedOrder;

    if-eqz p1, :cond_1

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$handleResults$1;->this$0:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->access$getContainer$p(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 87
    const-class v2, Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    :cond_1
    :goto_0
    return-void
.end method
