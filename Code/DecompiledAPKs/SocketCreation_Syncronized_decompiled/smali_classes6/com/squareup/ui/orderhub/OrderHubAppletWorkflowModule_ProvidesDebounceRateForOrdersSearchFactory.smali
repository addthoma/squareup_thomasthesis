.class public final Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory;
.super Ljava/lang/Object;
.source "OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory$InstanceHolder;->access$000()Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory;

    move-result-object v0

    return-object v0
.end method

.method public static providesDebounceRateForOrdersSearch()J
    .locals 2

    .line 25
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;->providesDebounceRateForOrdersSearch()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public get()Ljava/lang/Long;
    .locals 2

    .line 17
    invoke-static {}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory;->providesDebounceRateForOrdersSearch()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesDebounceRateForOrdersSearchFactory;->get()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
