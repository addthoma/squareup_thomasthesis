.class public final Lcom/squareup/ui/orderhub/OrderHubViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "OrderHubViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "masterFactory",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;",
        "detailFactory",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;",
        "orderDetailsFactory",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;",
        "itemSelectionFactory",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;",
        "cancellationReasonFactory",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;",
        "orderHubTrackingRunnerFactory",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;",
        "adjustPickupTimeFactory",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;",
        "(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;)V",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;)V
    .locals 23
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    const-string v7, "masterFactory"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "detailFactory"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "orderDetailsFactory"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "itemSelectionFactory"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "cancellationReasonFactory"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "orderHubTrackingRunnerFactory"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "adjustPickupTimeFactory"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v7, 0xa

    new-array v7, v7, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 38
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 39
    sget-object v9, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;->Companion:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Companion;

    invoke-virtual {v9}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 40
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_master_view:I

    .line 41
    new-instance v11, Lcom/squareup/ui/orderhub/OrderHubViewFactory$1;

    invoke-direct {v11, v0}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$1;-><init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;)V

    move-object v13, v11

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v14, 0xc

    const/4 v15, 0x0

    .line 38
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v8, 0x0

    aput-object v0, v7, v8

    .line 43
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 44
    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->Companion:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 45
    sget v11, Lcom/squareup/orderhub/applet/R$layout;->orderhub_detail_view:I

    .line 46
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubViewFactory$2;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$2;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;)V

    move-object v14, v0

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/4 v13, 0x0

    const/16 v15, 0xc

    const/16 v16, 0x0

    .line 43
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v7, v1

    .line 48
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 49
    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;->Companion:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 50
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_view:I

    .line 51
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1df

    const/16 v22, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 52
    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$3;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$3;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 48
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v7, v1

    .line 54
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 55
    const-class v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v9

    .line 56
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_add_tracking_view:I

    .line 57
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 58
    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$4;

    invoke-direct {v1, v5}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$4;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 54
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x3

    aput-object v0, v7, v1

    .line 60
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 61
    sget-object v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;->Companion:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 62
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_item_selection_view:I

    .line 63
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 64
    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$5;

    invoke-direct {v1, v3}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$5;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 60
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v7, v1

    .line 66
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 67
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen;->Companion:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 68
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_mark_shipped_view:I

    .line 69
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 70
    sget-object v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$6;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubViewFactory$6;

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 66
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x5

    aput-object v0, v7, v1

    .line 72
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 73
    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 74
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_cancelation_processing_view:I

    .line 75
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 76
    sget-object v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$7;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubViewFactory$7;

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 72
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x6

    aput-object v0, v7, v1

    .line 78
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 79
    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 80
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_cancelation_reason_view:I

    .line 81
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 82
    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$8;

    invoke-direct {v1, v4}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$8;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 78
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x7

    aput-object v0, v7, v1

    .line 84
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 85
    sget-object v0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;->Companion:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 86
    sget v10, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_adjust_pickup_time:I

    .line 87
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 88
    new-instance v1, Lcom/squareup/ui/orderhub/OrderHubViewFactory$9;

    invoke-direct {v1, v6}, Lcom/squareup/ui/orderhub/OrderHubViewFactory$9;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 84
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x8

    aput-object v0, v7, v1

    .line 90
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 91
    sget-object v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;->Companion:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 92
    sget-object v2, Lcom/squareup/ui/orderhub/OrderHubViewFactory$10;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubViewFactory$10;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 90
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x9

    aput-object v0, v7, v1

    move-object/from16 v0, p0

    .line 37
    invoke-direct {v0, v7}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
