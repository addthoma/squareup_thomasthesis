.class final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1;
.super Ljava/lang/Object;
.source "OrderHubBillLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadFromToken$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubBillLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubBillLoader.kt\ncom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1\n*L\n1#1,129:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    const-string v1, "it.response.bill_family"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 54
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->access$getOrderHubBillHistoryCreator$p(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v2, "it.response.bill_family[0]"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->toBill$orderhub_applet_release(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    check-cast v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    goto :goto_0

    .line 56
    :cond_0
    sget-object p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    goto :goto_0

    .line 58
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    check-cast v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    move-result-object p1

    return-object p1
.end method
