.class final Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$notifications$1;
.super Ljava/lang/Object;
.source "UnknownOrdersNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->notifications()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "it",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;

    invoke-static {v0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->access$toResult(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$notifications$1;->apply(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
