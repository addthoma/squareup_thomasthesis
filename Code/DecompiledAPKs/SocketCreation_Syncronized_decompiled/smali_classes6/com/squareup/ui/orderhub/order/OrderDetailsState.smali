.class public abstract Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.super Ljava/lang/Object;
.source "OrderDetailsState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;,
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;,
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;,
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;,
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;,
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;,
        Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u000e\u000f\u0010\u0011\u0012\u0013\u0014B\'\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\tR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\tR\u0014\u0010\u0007\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\t\u0082\u0001\u0007\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "",
        "isReadOnly",
        "",
        "showOrderIdInActionBar",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "showSpinner",
        "(ZZLcom/squareup/orders/model/Order;Z)V",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getShowOrderIdInActionBar",
        "getShowSpinner",
        "AdjustingPickupTimeState",
        "DisplayingOrderDetailsState",
        "EditingTrackingState",
        "MarkCanceledState",
        "MarkShippedState",
        "RefreshingOrderState",
        "RetrievingBillState",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isReadOnly:Z

.field private final order:Lcom/squareup/orders/model/Order;

.field private final showOrderIdInActionBar:Z

.field private final showSpinner:Z


# direct methods
.method private constructor <init>(ZZLcom/squareup/orders/model/Order;Z)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly:Z

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->showOrderIdInActionBar:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->showSpinner:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZLcom/squareup/orders/model/Order;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;Z)V

    return-void
.end method


# virtual methods
.method public getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public getShowOrderIdInActionBar()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->showOrderIdInActionBar:Z

    return v0
.end method

.method public getShowSpinner()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->showSpinner:Z

    return v0
.end method

.method public isReadOnly()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly:Z

    return v0
.end method
