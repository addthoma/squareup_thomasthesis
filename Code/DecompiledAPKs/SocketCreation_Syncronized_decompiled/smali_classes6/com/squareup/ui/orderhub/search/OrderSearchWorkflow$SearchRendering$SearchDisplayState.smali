.class public final enum Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;
.super Ljava/lang/Enum;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SearchDisplayState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;",
        "",
        "(Ljava/lang/String;I)V",
        "NOT_SEARCHING",
        "SHOW_INFO_TEXT",
        "TYPING",
        "LOADING",
        "SHOW_RESULTS",
        "NO_SEARCH_RESULTS",
        "ERROR",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum ERROR:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum LOADING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum NO_SEARCH_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum SHOW_INFO_TEXT:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum SHOW_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field public static final enum TYPING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x0

    const-string v3, "NOT_SEARCHING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x1

    const-string v3, "SHOW_INFO_TEXT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->SHOW_INFO_TEXT:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x2

    const-string v3, "TYPING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->TYPING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x3

    const-string v3, "LOADING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->LOADING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x4

    const-string v3, "SHOW_RESULTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->SHOW_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x5

    const-string v3, "NO_SEARCH_RESULTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NO_SEARCH_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x6

    const-string v3, "ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ERROR:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->$VALUES:[Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;
    .locals 1

    const-class v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->$VALUES:[Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v0}, [Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    return-object v0
.end method
