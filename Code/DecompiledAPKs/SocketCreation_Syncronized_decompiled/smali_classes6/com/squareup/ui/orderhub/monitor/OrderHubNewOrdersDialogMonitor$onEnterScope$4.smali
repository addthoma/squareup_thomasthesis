.class final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/Boolean;",
        "+",
        "Lcom/squareup/ordermanagerdata/ResultState$Success<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;>;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012f\u0010\u0002\u001ab\u0012\u0004\u0012\u00020\u0004\u0012$\u0012\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u0006 \u0008*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0018\u00010\u00050\u0005 \u0008*0\u0012\u0004\u0012\u00020\u0004\u0012$\u0012\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u0006 \u0008*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0018\u00010\u00050\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "Lcom/squareup/ordermanagerdata/ResultState$Success;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState$Success<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    .line 134
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$setHasAttemptedNotification$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Z)V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-static {v0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$showNewOrdersDialogScreen(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Ljava/util/List;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getRequestIntervalRefresh$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
