.class public final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "OrderHubOrderDetailsCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final dateTimeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final emailAppAvailableProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final lineItemSubtitleFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderHubAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->dateTimeFormatterProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->lineItemSubtitleFormatterProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->emailAppAvailableProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 91
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Z",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;"
        }
    .end annotation

    .line 101
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;
    .locals 13

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->dateTimeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->lineItemSubtitleFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->emailAppAvailableProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator_Factory_Factory;->get()Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
