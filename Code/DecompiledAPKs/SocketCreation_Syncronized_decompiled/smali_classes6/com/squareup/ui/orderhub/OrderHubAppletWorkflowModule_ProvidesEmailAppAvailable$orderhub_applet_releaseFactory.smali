.class public final Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;
.super Ljava/lang/Object;
.source "OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providesEmailAppAvailable$orderhub_applet_release(Landroid/app/Application;)Z
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;->providesEmailAppAvailable$orderhub_applet_release(Landroid/app/Application;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;->providesEmailAppAvailable$orderhub_applet_release(Landroid/app/Application;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule_ProvidesEmailAppAvailable$orderhub_applet_releaseFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
