.class public final Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;
.super Ljava/lang/Object;
.source "OrderHubViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/OrderHubViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final adjustPickupTimeFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final cancellationReasonFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final detailFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSelectionFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final masterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final orderDetailsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubTrackingRunnerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->masterFactoryProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->detailFactoryProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->orderDetailsFactoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->itemSelectionFactoryProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->cancellationReasonFactoryProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->orderHubTrackingRunnerFactoryProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->adjustPickupTimeFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;"
        }
    .end annotation

    .line 67
    new-instance v8, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;)Lcom/squareup/ui/orderhub/OrderHubViewFactory;
    .locals 9

    .line 77
    new-instance v8, Lcom/squareup/ui/orderhub/OrderHubViewFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/orderhub/OrderHubViewFactory;-><init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/OrderHubViewFactory;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->masterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->detailFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->orderDetailsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->itemSelectionFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->cancellationReasonFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->orderHubTrackingRunnerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->adjustPickupTimeFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->newInstance(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;)Lcom/squareup/ui/orderhub/OrderHubViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubViewFactory_Factory;->get()Lcom/squareup/ui/orderhub/OrderHubViewFactory;

    move-result-object v0

    return-object v0
.end method
