.class public final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;
.super Ljava/lang/Object;
.source "OrderHubBillLoader.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\r\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008\u000fJ\u000f\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0001\u00a2\u0006\u0002\u0008\u0012J\u0015\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\u0008\u0015J\u001b\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00172\u0006\u0010\u0018\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\u0008\u0019J\u001b\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u001b2\u0006\u0010\u001c\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\u0008\u001dR\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
        "",
        "billService",
        "Lcom/squareup/server/bills/BillListServiceHelper;",
        "orderHubBillHistoryCreator",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
        "userToken",
        "",
        "(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Ljava/lang/String;)V",
        "billHistoryLoadedState",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "kotlin.jvm.PlatformType",
        "clear",
        "",
        "clear$orderhub_applet_release",
        "getBillHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "getBillHistory$orderhub_applet_release",
        "loadFromBill",
        "bill",
        "loadFromBill$orderhub_applet_release",
        "loadFromToken",
        "Lio/reactivex/Single;",
        "billToken",
        "loadFromToken$orderhub_applet_release",
        "loadedState",
        "Lio/reactivex/Observable;",
        "defaultBillToken",
        "loadedState$orderhub_applet_release",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BILL_FAMILY_LIMIT:I = 0x1

.field public static final Companion:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXCLUDE_RELATED_BILLS:Z

.field private static final NO_PAGINATION_TOKEN:Ljava/lang/String;


# instance fields
.field private final billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ">;"
        }
    .end annotation
.end field

.field private final billService:Lcom/squareup/server/bills/BillListServiceHelper;

.field private final orderHubBillHistoryCreator:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

.field private final userToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->Companion:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "billService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBillHistoryCreator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->orderHubBillHistoryCreator:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->userToken:Ljava/lang/String;

    .line 35
    sget-object p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Empty;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Empty;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026istoryLoadedState>(Empty)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getBillHistoryLoadedState$p(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getNO_PAGINATION_TOKEN$cp()Ljava/lang/String;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->NO_PAGINATION_TOKEN:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getOrderHubBillHistoryCreator$p(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->orderHubBillHistoryCreator:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    return-object p0
.end method


# virtual methods
.method public final clear$orderhub_applet_release()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Empty;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Empty;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final getBillHistory$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        message = "Use the loadedState method to subscribe to updates."
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    .line 105
    sget-object v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Empty;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Empty;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    .line 106
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final loadFromBill$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final loadFromToken$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    const-string v0, "billToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;-><init>()V

    .line 44
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->userToken:Ljava/lang/String;

    sget-object v4, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->NO_PAGINATION_TOKEN:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 47
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/server/bills/BillListServiceHelper;->getBillFamilies(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;ILjava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    .line 50
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$1;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 61
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadFromToken$2;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "billService\n        .get\u2026tate.accept(it)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final loadedState$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    const-string v0, "defaultBillToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadedState$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader$loadedState$1;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "billHistoryLoadedState.s\u2026Observable.just(it)\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
