.class final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Triple<",
        "+",
        "Lcom/squareup/ordermanagerdata/ResultState$Success<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;>;+",
        "Ljava/lang/Boolean;",
        "+",
        "Ljava/lang/Boolean;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012H\u0010\u0002\u001aD\u0012$\u0012\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0005 \u0007*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u0008\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u00080\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Triple;",
        "Lcom/squareup/ordermanagerdata/ResultState$Success;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lkotlin/Triple;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;->invoke(Lkotlin/Triple;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Triple;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Triple<",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState$Success<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    const-string v2, "knownBefore"

    .line 112
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {p1, v2}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$setHasAttemptedNotification$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Z)V

    goto :goto_0

    .line 118
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {p1, v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$showNewOrdersDialogScreen(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Ljava/util/List;)V

    goto :goto_0

    .line 121
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getOrdersKnownBeforePreference$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
