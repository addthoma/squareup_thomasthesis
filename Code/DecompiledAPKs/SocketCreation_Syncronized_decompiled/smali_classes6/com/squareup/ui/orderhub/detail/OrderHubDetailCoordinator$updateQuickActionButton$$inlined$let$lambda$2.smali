.class final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;
.super Ljava/lang/Object;
.source "OrderHubDetailCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->updateQuickActionButton(Lcom/squareup/orders/model/Order;Lcom/squareup/noho/NohoButton;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick",
        "com/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$1$3$1",
        "com/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$apply$lambda$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $action$inlined:Lcom/squareup/protos/client/orders/Action;

.field final synthetic $order$inlined:Lcom/squareup/orders/model/Order;

.field final synthetic $quickActionButton$inlined:Lcom/squareup/noho/NohoButton;

.field final synthetic $quickActionFailureLabel$inlined:Lcom/squareup/noho/NohoLabel;

.field final synthetic $quickActionSpinner$inlined:Landroid/widget/ProgressBar;

.field final synthetic $quickActionStatus$inlined:Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/noho/NohoButton;Lcom/squareup/orders/model/Order;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$action$inlined:Lcom/squareup/protos/client/orders/Action;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$quickActionStatus$inlined:Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$quickActionSpinner$inlined:Landroid/widget/ProgressBar;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$quickActionFailureLabel$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$quickActionButton$inlined:Lcom/squareup/noho/NohoButton;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$order$inlined:Lcom/squareup/orders/model/Order;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .line 473
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->access$getScreen$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;->$order$inlined:Lcom/squareup/orders/model/Order;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;-><init>(Lcom/squareup/orders/model/Order;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
