.class public final Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderHubAdjustPickupTimeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ*\u0010\u000b\u001a\u00020\u000c2\"\u0010\r\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0008\u0012\u0004\u0012\u00020\u0010`\u00120\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;",
        "",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "relativeDateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/time/CurrentTime;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "relativeDateAndTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    .line 50
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 51
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    .line 52
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    .line 53
    iget-object v5, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    move-object v1, v0

    move-object v6, p1

    .line 49
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;-><init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V

    return-object v0
.end method
