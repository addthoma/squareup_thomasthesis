.class public interface abstract Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\u0008H&J\u0008\u0010\n\u001a\u00020\u0008H&J\u0008\u0010\u000b\u001a\u00020\u0008H&J\u0008\u0010\u000c\u001a\u00020\u0008H&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;",
        "",
        "billFromBillToken",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "billToken",
        "",
        "closeBillHistory",
        "",
        "reprintTicket",
        "showIssueReceiptScreen",
        "showIssueRefundScreen",
        "startPrintGiftReceiptFlow",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract billFromBillToken(Ljava/lang/String;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract closeBillHistory()V
.end method

.method public abstract reprintTicket()V
.end method

.method public abstract showIssueReceiptScreen()V
.end method

.method public abstract showIssueRefundScreen()V
.end method

.method public abstract startPrintGiftReceiptFlow()V
.end method
