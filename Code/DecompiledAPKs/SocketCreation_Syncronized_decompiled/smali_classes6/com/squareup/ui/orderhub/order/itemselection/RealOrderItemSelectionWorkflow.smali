.class public final Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderItemSelectionWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderItemSelectionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderItemSelectionWorkflow.kt\ncom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,107:1\n41#2:108\n56#2,2:109\n276#3:111\n149#4,5:112\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderItemSelectionWorkflow.kt\ncom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow\n*L\n64#1:108\n64#1,2:109\n64#1:111\n103#1,5:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u001a\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00032\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J \u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0004H\u0016JN\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00042\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInputKt;->getInitialState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->initialState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {p2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInputKt;->getInitialState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p3

    :goto_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    check-cast p3, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->render(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "connectivityMonitor.internetState()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 110
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 111
    const-class v0, Lcom/squareup/connectivity/InternetState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 65
    new-instance p1, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 63
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 74
    new-instance p1, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow$render$screenEventHandler$1;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 102
    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 103
    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 113
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 114
    const-class p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 115
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 113
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 102
    invoke-virtual {p3, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 110
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/RealOrderItemSelectionWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
