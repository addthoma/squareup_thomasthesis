.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6\n*L\n1#1,885:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "event",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    sget-object v0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryInventoryAdjustment;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryInventoryAdjustment;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 218
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    .line 219
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v4

    .line 220
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 221
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x1

    .line 223
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v8

    const/4 v9, 0x0

    .line 225
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getRetryInventoryAdjustment()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x80

    const/4 v13, 0x0

    move-object v3, v0

    .line 218
    invoke-direct/range {v3 .. v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 217
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 228
    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelInventoryAdjustment;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelInventoryAdjustment;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 229
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 230
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v4

    .line 231
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 232
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x78

    const/4 v12, 0x0

    move-object v3, v0

    .line 229
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 228
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 235
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected error dialog event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;->invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
