.class final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SearchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->render(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/SearchState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;>;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "result",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$2;->$props:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;

    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$2;->$props:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;-><init>(Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 144
    :cond_0
    sget-object p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;->INSTANCE:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$2;->invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
