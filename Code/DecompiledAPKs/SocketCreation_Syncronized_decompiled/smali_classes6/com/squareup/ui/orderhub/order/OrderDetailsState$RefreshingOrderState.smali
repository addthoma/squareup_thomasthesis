.class public final Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;
.super Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.source "OrderDetailsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RefreshingOrderState"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00032\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0008R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0008\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "isReadOnly",
        "",
        "showOrderIdInActionBar",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "(ZZLcom/squareup/orders/model/Order;)V",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getShowOrderIdInActionBar",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isReadOnly:Z

.field private final order:Lcom/squareup/orders/model/Order;

.field private final showOrderIdInActionBar:Z


# direct methods
.method public constructor <init>(ZZLcom/squareup/orders/model/Order;)V
    .locals 7

    const-string v0, "order"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    .line 80
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly:Z

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->showOrderIdInActionBar:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->order:Lcom/squareup/orders/model/Order;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;ZZLcom/squareup/orders/model/Order;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly()Z

    move-result p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getShowOrderIdInActionBar()Z

    move-result p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->copy(ZZLcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly()Z

    move-result v0

    return v0
.end method

.method public final component2()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getShowOrderIdInActionBar()Z

    move-result v0

    return v0
.end method

.method public final component3()Lcom/squareup/orders/model/Order;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    return-object v0
.end method

.method public final copy(ZZLcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;
    .locals 1

    const-string v0, "order"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;-><init>(ZZLcom/squareup/orders/model/Order;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getShowOrderIdInActionBar()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getShowOrderIdInActionBar()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public getShowOrderIdInActionBar()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->showOrderIdInActionBar:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getShowOrderIdInActionBar()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public isReadOnly()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefreshingOrderState(isReadOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->isReadOnly()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showOrderIdInActionBar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getShowOrderIdInActionBar()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
