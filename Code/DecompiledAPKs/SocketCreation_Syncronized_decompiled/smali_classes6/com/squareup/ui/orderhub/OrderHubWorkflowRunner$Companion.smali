.class public final Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "OrderHubWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "getNAME",
        "()Ljava/lang/String;",
        "getRunner",
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;",
        "scope",
        "Lmortar/MortarScope;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 79
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNAME()Ljava/lang/String;
    .locals 1

    .line 80
    invoke-static {}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRunner(Lmortar/MortarScope;)Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    return-object p1
.end method
