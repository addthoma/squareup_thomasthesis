.class final Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.kt"

# interfaces
.implements Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillHistoryDetailScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillHistoryDetailScreen.kt\ncom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1\n*L\n1#1,198:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;",
        "parcel",
        "Landroid/os/Parcel;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/os/Parcel;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
    .locals 1

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 194
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 193
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 188
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;->invoke(Landroid/os/Parcel;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    move-result-object p1

    return-object p1
.end method
