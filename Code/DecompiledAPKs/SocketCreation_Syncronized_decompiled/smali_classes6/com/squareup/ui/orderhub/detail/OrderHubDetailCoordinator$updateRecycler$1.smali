.class final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubDetailCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->updateRecycler(Ljava/util/List;ZLjava/util/Map;ZLcom/squareup/ui/orderhub/master/Filter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $headerRow:Lcom/squareup/ui/orderhub/master/Filter;

.field final synthetic $isLoadingMoreOrders:Z

.field final synthetic $orderQuickActionStatusMap:Ljava/util/Map;

.field final synthetic $orders:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$orders:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$orderQuickActionStatusMap:Ljava/util/Map;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$headerRow:Lcom/squareup/ui/orderhub/master/Filter;

    iput-boolean p5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$isLoadingMoreOrders:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 100
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$orders:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$orderQuickActionStatusMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$headerRow:Lcom/squareup/ui/orderhub/master/Filter;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt;->access$asOrderDetailRows(Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 652
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;->$isLoadingMoreOrders:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$LoadMore;

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$LoadMore;-><init>(Lcom/squareup/ui/LoadMoreState;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setExtraItem(Ljava/lang/Object;)V

    .line 653
    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->onReady(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
