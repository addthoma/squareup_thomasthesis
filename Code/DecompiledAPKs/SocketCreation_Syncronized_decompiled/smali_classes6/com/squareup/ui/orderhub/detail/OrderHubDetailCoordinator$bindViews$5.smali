.class public final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5;
.super Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;
.source "OrderHubDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->bindViews(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5",
        "Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;",
        "onDropDownHiding",
        "",
        "dropDownContentContainer",
        "Landroid/view/View;",
        "onDropDownOpening",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinVerticalCaretView;)V
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5;->$dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropDownHiding(Landroid/view/View;)V
    .locals 1

    const-string v0, "dropDownContentContainer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5;->$dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animateArrowDown()V

    return-void
.end method

.method public onDropDownOpening(Landroid/view/View;)V
    .locals 1

    const-string v0, "dropDownContentContainer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5;->$dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animateArrowUp()V

    return-void
.end method
