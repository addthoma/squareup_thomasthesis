.class public final Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubAdjustPickupTimeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$Factory;,
        Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubAdjustPickupTimeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubAdjustPickupTimeCoordinator.kt\ncom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 7 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,151:1\n49#2:152\n50#2,3:158\n53#2:180\n599#3,4:153\n601#3:157\n114#4,5:161\n120#4:177\n328#5:166\n342#5,5:167\n344#5,4:172\n329#5:176\n43#6,2:178\n1360#7:181\n1429#7,3:182\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubAdjustPickupTimeCoordinator.kt\ncom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator\n*L\n80#1:152\n80#1,3:158\n80#1:180\n80#1,4:153\n80#1:157\n80#1,5:161\n80#1:177\n80#1:166\n80#1,5:167\n80#1,4:172\n80#1:176\n80#1,2:178\n146#1:181\n146#1,3:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002&\'BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\"\u0010\n\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0008\u0012\u0004\u0012\u00020\r`\u000f0\u000b\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0014H\u0016J\u0010\u0010\u001f\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0014H\u0002J\u0008\u0010 \u001a\u00020\u001dH\u0002J\u0010\u0010!\u001a\u00020\u001d2\u0006\u0010\"\u001a\u00020\u0014H\u0002J\u0010\u0010#\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\rH\u0002J\u0012\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u001b0%*\u00020\u0016H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\n\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0008\u0012\u0004\u0012\u00020\r`\u000f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "relativeDateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/time/CurrentTime;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "mainView",
        "Landroid/view/View;",
        "now",
        "Lorg/threeten/bp/ZonedDateTime;",
        "screen",
        "selectedTime",
        "timesRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;",
        "attach",
        "",
        "view",
        "bindViews",
        "configureActionBar",
        "createRecycler",
        "coordinatorView",
        "updateViews",
        "generateDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "Factory",
        "TimeRow",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private mainView:Landroid/view/View;

.field private now:Lorg/threeten/bp/ZonedDateTime;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

.field private screen:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private selectedTime:Lorg/threeten/bp/ZonedDateTime;

.field private timesRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "relativeDateAndTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$generateDataSource(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;Lorg/threeten/bp/ZonedDateTime;)Lcom/squareup/cycler/DataSource;
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->generateDataSource(Lorg/threeten/bp/ZonedDateTime;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    return-object p0
.end method

.method public static final synthetic access$getNow$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->now:Lorg/threeten/bp/ZonedDateTime;

    if-nez p0, :cond_0

    const-string v0, "now"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRelativeDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->screen:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSelectedTime$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lorg/threeten/bp/ZonedDateTime;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->selectedTime:Lorg/threeten/bp/ZonedDateTime;

    return-object p0
.end method

.method public static final synthetic access$setNow$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;Lorg/threeten/bp/ZonedDateTime;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->now:Lorg/threeten/bp/ZonedDateTime;

    return-void
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->screen:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

    return-void
.end method

.method public static final synthetic access$setSelectedTime$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;Lorg/threeten/bp/ZonedDateTime;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->selectedTime:Lorg/threeten/bp/ZonedDateTime;

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->updateViews(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->mainView:Landroid/view/View;

    .line 74
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->mainView:Landroid/view/View;

    if-nez p1, :cond_0

    const-string v0, "mainView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->createRecycler(Landroid/view/View;)V

    return-void
.end method

.method private final configureActionBar()V
    .locals 4

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 134
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 135
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_adjust_pickup_time:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 136
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$configureActionBar$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$configureActionBar$1;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 139
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideAction()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 5

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 81
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_adjust_pickup_time_recycler_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "coordinatorView.findView\u2026ickup_time_recycler_view)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 152
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 153
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 154
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 158
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 159
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 84
    sget-object v0, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast v0, Lcom/squareup/noho/CheckType;

    new-instance v2, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 162
    new-instance v3, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$$special$$inlined$nohoRow$1;

    invoke-direct {v3, v0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$$special$$inlined$nohoRow$1;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 168
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 172
    sget-object v4, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$$special$$inlined$nohoRow$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 168
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 166
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 171
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 167
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 178
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0xa

    .line 99
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 100
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 178
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 156
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->timesRecycler:Lcom/squareup/cycler/Recycler;

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->timesRecycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_0

    const-string v0, "timesRecycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    return-void

    .line 153
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final generateDataSource(Lorg/threeten/bp/ZonedDateTime;)Lcom/squareup/cycler/DataSource;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/threeten/bp/ZonedDateTime;",
            ")",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Lkotlin/ranges/LongRange;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/ranges/LongRange;-><init>(JJ)V

    check-cast v0, Lkotlin/ranges/LongProgression;

    const-wide/16 v1, 0x5

    invoke-static {v0, v1, v2}, Lkotlin/ranges/RangesKt;->step(Lkotlin/ranges/LongProgression;J)Lkotlin/ranges/LongProgression;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 181
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 182
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lkotlin/collections/LongIterator;

    invoke-virtual {v2}, Lkotlin/collections/LongIterator;->nextLong()J

    move-result-wide v2

    .line 146
    new-instance v4, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;

    invoke-virtual {p1, v2, v3}, Lorg/threeten/bp/ZonedDateTime;->plusMinutes(J)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v2

    const-string v3, "plusMinutes(it)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;-><init>(Lorg/threeten/bp/ZonedDateTime;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 147
    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    return-object p1
.end method

.method private final updateViews(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;)V
    .locals 4

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->screen:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

    .line 110
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;->getData()Lcom/squareup/ui/orderhub/order/adjusttime/AdjustPickupTimeScreenData;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/adjusttime/AdjustPickupTimeScreenData;->getSelectedPickupTime()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->selectedTime:Lorg/threeten/bp/ZonedDateTime;

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->now:Lorg/threeten/bp/ZonedDateTime;

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/adjusttime/AdjustPickupTimeScreenData;->getInitialPickupTime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->now:Lorg/threeten/bp/ZonedDateTime;

    const-string v2, "now"

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    move-object v3, v0

    check-cast v3, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    invoke-virtual {v1, v3}, Lorg/threeten/bp/ZonedDateTime;->isAfter(Lorg/threeten/bp/chrono/ChronoZonedDateTime;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->now:Lorg/threeten/bp/ZonedDateTime;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 125
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->timesRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v1, :cond_3

    const-string v2, "timesRecycler"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$updateViews$2;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$updateViews$2;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;Lorg/threeten/bp/ZonedDateTime;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->mainView:Landroid/view/View;

    if-nez v0, :cond_4

    const-string v1, "mainView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$updateViews$3;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$updateViews$3;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->configureActionBar()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->bindViews(Landroid/view/View;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$attach$1;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
