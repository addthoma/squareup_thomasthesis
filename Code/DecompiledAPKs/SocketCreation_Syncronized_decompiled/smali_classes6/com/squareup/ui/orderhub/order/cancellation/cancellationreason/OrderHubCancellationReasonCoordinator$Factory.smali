.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderHubCancellationReasonCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J*\u0010\u0007\u001a\u00020\u00082\"\u0010\t\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\u000e0\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V

    return-object v0
.end method
