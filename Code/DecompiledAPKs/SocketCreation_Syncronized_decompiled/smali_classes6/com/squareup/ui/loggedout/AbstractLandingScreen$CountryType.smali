.class public final enum Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;
.super Ljava/lang/Enum;
.source "AbstractLandingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/AbstractLandingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CountryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

.field public static final enum PAYMENT:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

.field public static final enum WORLD:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 17
    new-instance v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    const/4 v1, 0x0

    const-string v2, "PAYMENT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->PAYMENT:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    new-instance v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    const/4 v2, 0x1

    const-string v3, "WORLD"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->WORLD:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    sget-object v3, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->PAYMENT:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->WORLD:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->$VALUES:[Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;
    .locals 1

    .line 17
    const-class v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->$VALUES:[Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    invoke-virtual {v0}, [Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    return-object v0
.end method
