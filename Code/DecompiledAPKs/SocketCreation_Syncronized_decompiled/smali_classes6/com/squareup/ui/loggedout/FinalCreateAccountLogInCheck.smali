.class public interface abstract Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;
.super Ljava/lang/Object;
.source "FinalLogInCheck.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;",
        "",
        "createAccountLoginCheckFailedMessage",
        "",
        "getCreateAccountLoginCheckFailedMessage",
        "()Ljava/lang/String;",
        "createAccountLoginCheckFailedTitle",
        "getCreateAccountLoginCheckFailedTitle",
        "isLoginSuccessful",
        "",
        "response",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCreateAccountLoginCheckFailedMessage()Ljava/lang/String;
.end method

.method public abstract getCreateAccountLoginCheckFailedTitle()Ljava/lang/String;
.end method

.method public abstract isLoginSuccessful(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
.end method
