.class abstract Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;
.super Lmortar/Presenter;
.source "AbstractLandingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/AbstractLandingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "AbstractLandingScreenPresenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;",
        ">",
        "Lmortar/Presenter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final countryType:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

.field private final encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;Lcom/squareup/analytics/Analytics;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->res:Lcom/squareup/util/Res;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->countryType:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 55
    invoke-interface {p1}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->extractBundleService(Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method onCallToActionTapped()V
    .locals 4

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TOUR_START:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->countryType:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    iget-object v2, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cardreader/ui/R$string;->get_started:I

    .line 61
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->showLearnMore(Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;Ljava/lang/String;)V

    return-void
.end method

.method onCreateAccountClicked()V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startCreateAccount()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 43
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-virtual {v0}, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;->encryptedEmails()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/loggedout/-$$Lambda$0DY5_11zVaMSOkgrC80YjzvBZak;

    invoke-direct {v2, v1}, Lcom/squareup/ui/loggedout/-$$Lambda$0DY5_11zVaMSOkgrC80YjzvBZak;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    .line 45
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 44
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->canShowLearnMore()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;

    invoke-interface {p1}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$LandingView;->showLearnMore()V

    :cond_0
    return-void
.end method

.method onLoginClicked()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startLoginFlow(Z)V

    return-void
.end method
