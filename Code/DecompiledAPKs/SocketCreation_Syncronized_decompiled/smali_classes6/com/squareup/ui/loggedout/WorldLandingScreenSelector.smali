.class public final Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;
.super Ljava/lang/Object;
.source "WorldLandingScreenSelector.kt"

# interfaces
.implements Lcom/squareup/ui/loggedout/LandingScreenSelector;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0002J\u0008\u0010\u000e\u001a\u00020\rH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;",
        "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "postInstallEncryptedEmailSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
        "countryGuesser",
        "Lcom/squareup/location/CountryCodeGuesser;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/location/CountryCodeGuesser;)V",
        "getLandingScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "shouldShowPaymentsLandingScreen",
        "",
        "splashScreenEnabled",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/location/CountryCodeGuesser;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postInstallEncryptedEmailSetting"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryGuesser"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p3, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    return-void
.end method

.method private final shouldShowPaymentsLandingScreen()Z
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LANDING_WORLD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 36
    iget-object v1, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->LANDING_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    invoke-interface {v0}, Lcom/squareup/location/CountryCodeGuesser;->tryTelephony()Lcom/squareup/CountryCode;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/squareup/CountryCode;->hasPayments:Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private final splashScreenEnabled()Z
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    invoke-interface {v0}, Lcom/squareup/location/CountryCodeGuesser;->tryTelephony()Lcom/squareup/CountryCode;

    move-result-object v0

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public getLandingScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;-><init>(Z)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 28
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->splashScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/ui/loggedout/SplashScreen;->Companion:Lcom/squareup/ui/loggedout/SplashScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/SplashScreen$Companion;->getINSTANCE()Lcom/squareup/ui/loggedout/SplashScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 29
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;->shouldShowPaymentsLandingScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/ui/loggedout/LandingScreen;->INSTANCE:Lcom/squareup/ui/loggedout/LandingScreen;

    const-string v1, "LandingScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 30
    :cond_2
    sget-object v0, Lcom/squareup/ui/loggedout/WorldLandingScreen;->INSTANCE:Lcom/squareup/ui/loggedout/WorldLandingScreen;

    const-string v1, "WorldLandingScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    return-object v0
.end method
