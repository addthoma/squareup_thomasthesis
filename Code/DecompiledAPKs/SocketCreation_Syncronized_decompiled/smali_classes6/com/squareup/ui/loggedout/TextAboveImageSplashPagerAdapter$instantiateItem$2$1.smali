.class public final Lcom/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter$instantiateItem$2$1;
.super Ljava/lang/Object;
.source "TextAboveImageSplashPagerAdapter.kt"

# interfaces
.implements Lcom/squareup/util/OnMeasuredCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter$instantiateItem$2$1",
        "Lcom/squareup/util/OnMeasuredCallback;",
        "onMeasured",
        "",
        "view",
        "Landroid/view/View;",
        "width",
        "",
        "height",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_apply:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/widget/ImageView;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter$instantiateItem$2$1;->$this_apply:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMeasured(Landroid/view/View;II)V
    .locals 0

    const-string p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter$instantiateItem$2$1;->$this_apply:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/squareup/ui/loggedout/ImageViewKt;->setCropBottom(Landroid/widget/ImageView;)Landroid/graphics/Matrix;

    return-void
.end method
