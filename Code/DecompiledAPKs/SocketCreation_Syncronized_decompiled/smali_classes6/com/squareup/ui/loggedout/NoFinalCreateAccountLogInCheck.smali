.class public final Lcom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck;
.super Ljava/lang/Object;
.source "FinalLogInCheck.kt"

# interfaces
.implements Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFinalLogInCheck.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FinalLogInCheck.kt\ncom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,74:1\n152#2:75\n*E\n*S KotlinDebug\n*F\n+ 1 FinalLogInCheck.kt\ncom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck\n*L\n69#1:75\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u0012\u0010\u0003\u001a\u00020\u0004X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck;",
        "Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;",
        "()V",
        "createAccountLoginCheckFailedMessage",
        "",
        "getCreateAccountLoginCheckFailedMessage",
        "()Ljava/lang/String;",
        "createAccountLoginCheckFailedTitle",
        "getCreateAccountLoginCheckFailedTitle",
        "isLoginSuccessful",
        "",
        "response",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-class v0, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    const-string v1, "FinalCreateAccountLogInCheck is successful, should not be called"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    iput-object v0, p0, Lcom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck;->$$delegate_0:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    return-void
.end method


# virtual methods
.method public getCreateAccountLoginCheckFailedMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck;->$$delegate_0:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    invoke-interface {v0}, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;->getCreateAccountLoginCheckFailedMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCreateAccountLoginCheckFailedTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/loggedout/NoFinalCreateAccountLogInCheck;->$$delegate_0:Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    invoke-interface {v0}, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;->getCreateAccountLoginCheckFailedTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isLoginSuccessful(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method
