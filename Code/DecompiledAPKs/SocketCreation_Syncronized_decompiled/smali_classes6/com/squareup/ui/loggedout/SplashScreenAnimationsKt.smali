.class public final Lcom/squareup/ui/loggedout/SplashScreenAnimationsKt;
.super Ljava/lang/Object;
.source "SplashScreenAnimations.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSplashScreenAnimations.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SplashScreenAnimations.kt\ncom/squareup/ui/loggedout/SplashScreenAnimationsKt\n*L\n1#1,83:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u001a\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0000\u001a\u001c\u0010\u0007\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0008\u001a\u00020\tH\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "MIN_ALPHA",
        "",
        "MIN_SCALE",
        "fadeAndParallax",
        "",
        "Landroid/view/View;",
        "position",
        "fadeInPlace",
        "parentWidth",
        "",
        "loggedout_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MIN_ALPHA:F = 0.5f

.field private static final MIN_SCALE:F = 0.85f


# direct methods
.method public static final fadeAndParallax(Landroid/view/View;F)V
    .locals 3

    const-string v0, "$this$fadeAndParallax"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    int-to-float v0, v0

    const/4 v1, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 56
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    int-to-float v0, v0

    cmpg-float v2, p1, v0

    if-gtz v2, :cond_1

    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, p1

    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 70
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    sub-float/2addr v0, p1

    const p1, 0x3f59999a    # 0.85f

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float/2addr v0, p1

    const p1, 0x40555558

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v0, v0, p1

    add-float/2addr v0, v1

    .line 75
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    :goto_0
    return-void
.end method

.method public static final fadeInPlace(Landroid/view/View;FI)V
    .locals 2

    const-string v0, "$this$fadeInPlace"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v0, p2

    neg-float v1, p1

    mul-float v1, v1, v0

    .line 29
    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 31
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    mul-float v0, v0, p1

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    add-int/2addr p1, v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    add-int/2addr p1, v1

    sub-int/2addr p2, p1

    int-to-float p1, p2

    div-float/2addr v0, p1

    const/4 p1, 0x2

    int-to-float p1, p1

    .line 35
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p2

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    mul-float p1, p1, v0

    const/4 p2, 0x1

    int-to-float p2, p2

    sub-float/2addr p1, p2

    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method
