.class public final Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;
.super Ljava/lang/Object;
.source "LandingCanceledListener.kt"

# interfaces
.implements Lcom/squareup/ui/loggedout/LandingCanceledListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/LandingCanceledListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoLandingCanceledListener"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;",
        "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
        "()V",
        "onLandingCanceled",
        "",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;->INSTANCE:Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListener;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLandingCanceled()V
    .locals 0

    return-void
.end method
