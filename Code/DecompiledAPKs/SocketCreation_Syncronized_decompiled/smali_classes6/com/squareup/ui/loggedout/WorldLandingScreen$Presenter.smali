.class Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;
.super Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;
.source "WorldLandingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/WorldLandingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter<",
        "Lcom/squareup/ui/loggedout/WorldLandingView;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    sget-object v3, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->WORLD:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;Lcom/squareup/analytics/Analytics;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    return-void
.end method
