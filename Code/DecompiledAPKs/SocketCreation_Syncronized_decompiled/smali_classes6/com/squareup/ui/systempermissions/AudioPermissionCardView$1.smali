.class Lcom/squareup/ui/systempermissions/AudioPermissionCardView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AudioPermissionCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->setupLayout(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardView;

    iget-object p1, p1, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->presenter:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->askForMicPermission()V

    return-void
.end method
