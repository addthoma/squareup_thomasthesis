.class public Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "EmployeeJobsListCoordinator.java"


# instance fields
.field private jobsListLayout:Landroid/widget/LinearLayout;

.field private jobsListScrollView:Landroid/widget/ScrollView;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private createButton(Landroid/view/LayoutInflater;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Z)V
    .locals 3

    .line 78
    sget v0, Lcom/squareup/ui/timecards/R$layout;->employee_jobs_button:I

    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->jobsListLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    .line 79
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 80
    iget-object v0, p2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    if-nez p3, :cond_0

    .line 82
    new-instance p3, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListCoordinator$aiuVyUM94Q6CWUgivsz7Pr7DAIU;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListCoordinator$aiuVyUM94Q6CWUgivsz7Pr7DAIU;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)V

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 85
    :cond_0
    new-instance p3, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListCoordinator$kNFuolu4EO3M9ODBVbEn5Iglk24;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListCoordinator$kNFuolu4EO3M9ODBVbEn5Iglk24;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)V

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->jobsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 34
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListCoordinator$r6F3br9bkc0myhgRJZJBu-zKH0A;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListCoordinator$r6F3br9bkc0myhgRJZJBu-zKH0A;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 1

    .line 92
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 93
    sget v0, Lcom/squareup/ui/timecards/R$id;->employee_jobs_list_scroll_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->jobsListScrollView:Landroid/widget/ScrollView;

    .line 94
    sget v0, Lcom/squareup/ui/timecards/R$id;->employee_jobs_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->jobsListLayout:Landroid/widget/LinearLayout;

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->jobsListScrollView:Landroid/widget/ScrollView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ScrollView;->setVisibility(I)V

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_JOBS_LIST:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$EmployeeJobsListCoordinator()Lrx/Subscription;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->employeeJobsListScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 35
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Enj2NjgYIGcBN4QYGJW-XRaAET8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Enj2NjgYIGcBN4QYGJW-XRaAET8;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;)V

    .line 36
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$4Ax6k1juvVDxuldAsXcKjgU9b8o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$4Ax6k1juvVDxuldAsXcKjgU9b8o;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$f21FqDn6bbaJK55VuEAXg-YaPIk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$f21FqDn6bbaJK55VuEAXg-YaPIk;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;)V

    .line 37
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$createButton$1$EmployeeJobsListCoordinator(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Landroid/view/View;)V
    .locals 1

    .line 83
    iget-object p2, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object p1, p1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_token:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onJobSelected(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$createButton$2$EmployeeJobsListCoordinator(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Landroid/view/View;)V
    .locals 0

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object p1, p1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_token:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onJobSelectedToSwitchJobs(Ljava/lang/String;)V

    return-void
.end method

.method protected showBackArrowOnSuccess()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 57
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;

    .line 59
    iget-object v1, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    .line 60
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 65
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->employee_management_jobs_list_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->view:Landroid/view/View;

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    .line 70
    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->jobsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 71
    iget-object v1, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 72
    iget-boolean v3, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->isEmployeeCurrentlyClockedIn:Z

    invoke-direct {p0, p1, v2, v3}, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->createButton(Landroid/view/LayoutInflater;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Z)V

    goto :goto_0

    :cond_1
    return-void

    .line 61
    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;->showError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method
