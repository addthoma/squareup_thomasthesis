.class public Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TimecardsLoadingCoordinator.java"


# instance fields
.field private mainScheduler:Lrx/Scheduler;

.field private final subs:Lrx/subscriptions/CompositeSubscription;

.field private timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lrx/Scheduler;)V
    .locals 1
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 16
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method public static synthetic lambda$L6YmB8hePxRGTAFD--_v-h3wB1g(Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->showNextScreen(Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;)V

    return-void
.end method

.method private showNextScreen(Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;)V
    .locals 2

    .line 37
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->requestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v0, v1, :cond_0

    return-void

    .line 43
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->requestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-ne v0, v1, :cond_1

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToHomeScreen()V

    return-void

    .line 48
    :cond_1
    iget-boolean v0, p1, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->employeeHasOpenTimecard:Z

    if-nez v0, :cond_2

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToClockInOrContinue()V

    goto :goto_0

    .line 50
    :cond_2
    iget-boolean p1, p1, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->employeeHasOpenBreak:Z

    if-eqz p1, :cond_3

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToEndBreakAfterLogin()V

    goto :goto_0

    .line 53
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToHomeScreen()V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 25
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 27
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->timecardsLoadingScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 28
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsLoadingCoordinator$L6YmB8hePxRGTAFD--_v-h3wB1g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsLoadingCoordinator$L6YmB8hePxRGTAFD--_v-h3wB1g;-><init>(Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;)V

    .line 29
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 27
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    .line 33
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method
