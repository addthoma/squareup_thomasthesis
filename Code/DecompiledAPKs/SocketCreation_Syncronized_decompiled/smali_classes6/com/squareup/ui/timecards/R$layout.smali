.class public final Lcom/squareup/ui/timecards/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final clock_in_or_continue_view:I = 0x7f0d00e8

.field public static final clock_in_out_view:I = 0x7f0d00e9

.field public static final employee_jobs_button:I = 0x7f0d0249

.field public static final employee_jobs_list:I = 0x7f0d024a

.field public static final employee_jobs_list_modal:I = 0x7f0d024b

.field public static final timecards:I = 0x7f0d0540

.field public static final timecards_action_bar:I = 0x7f0d0541

.field public static final timecards_add_or_edit_notes:I = 0x7f0d0542

.field public static final timecards_clockin_confirmation:I = 0x7f0d0543

.field public static final timecards_clockout_confirmation:I = 0x7f0d0544

.field public static final timecards_clockout_summary:I = 0x7f0d0545

.field public static final timecards_clockout_summary_data_cell:I = 0x7f0d0546

.field public static final timecards_clockout_summary_header_cell:I = 0x7f0d0547

.field public static final timecards_clockout_summary_line_separator:I = 0x7f0d0548

.field public static final timecards_end_break_confirmation:I = 0x7f0d0549

.field public static final timecards_list_breaks:I = 0x7f0d054a

.field public static final timecards_list_breaks_button:I = 0x7f0d054b

.field public static final timecards_loading_modal:I = 0x7f0d054c

.field public static final timecards_modal:I = 0x7f0d054d

.field public static final timecards_start_break_or_clockout:I = 0x7f0d054e

.field public static final timecards_success:I = 0x7f0d054f

.field public static final timecards_success_buttons_view:I = 0x7f0d0550

.field public static final timecards_success_modal:I = 0x7f0d0551

.field public static final timecards_view_notes:I = 0x7f0d0552

.field public static final view_notes_entry:I = 0x7f0d0585


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
