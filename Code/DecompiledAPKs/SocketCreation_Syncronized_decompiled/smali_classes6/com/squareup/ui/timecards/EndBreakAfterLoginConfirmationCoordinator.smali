.class public Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EndBreakAfterLoginConfirmationCoordinator.java"


# instance fields
.field private analytics:Lcom/squareup/analytics/Analytics;

.field private appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private mainScheduler:Lrx/Scheduler;

.field private message:Lcom/squareup/marketfont/MarketTextView;

.field private passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private primaryButton:Lcom/squareup/marketfont/MarketButton;

.field private progressBar:Landroid/widget/ProgressBar;

.field private res:Lcom/squareup/util/Res;

.field private secondaryButton:Lcom/squareup/marketfont/MarketButton;

.field private successGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

.field private timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0
    .param p3    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 153
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->progressBar:Landroid/widget/ProgressBar;

    .line 154
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_success:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 155
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_clock_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 156
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_failure:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 157
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 158
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    .line 159
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_primary_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 160
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_secondary_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 161
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    return-void
.end method

.method public static synthetic lambda$35vLcCfOxvClCvikevEWTvCbYjQ(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->showProgressBar()V

    return-void
.end method

.method public static synthetic lambda$GRtl2uFqDfBU_Aa4JZPUXX6CGLw(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->showData(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;)V

    return-void
.end method

.method private setErrorVisibility()V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method private showData(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;)V
    .locals 2

    .line 77
    iget-object v0, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;->internetState:Lcom/squareup/connectivity/InternetState;

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq v0, v1, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->showNoInternet()V

    return-void

    .line 81
    :cond_0
    sget-object v0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I

    iget-object v1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v1}, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 89
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->showError()V

    goto :goto_0

    .line 86
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->showSuccess(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;)V

    goto :goto_0

    .line 83
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->showProgressBar()V

    :goto_0
    return-void
.end method

.method private showError()V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_DEFAULT:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 147
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->setErrorVisibility()V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method private showNoInternet()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->setErrorVisibility()V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method private showProgressBar()V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method private showSuccess(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;)V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 103
    iget-object p1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    goto :goto_0

    .line 106
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_success_continue_button_text:I

    .line 108
    invoke-interface {v0, v1}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 112
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_success_message:I

    .line 113
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 58
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 61
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$8oIiU-gOjl7_3RgFXX-IK2a0Rck;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$8oIiU-gOjl7_3RgFXX-IK2a0Rck;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$Nvalu5TlH7JvkKvX0BAzDjck8FY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$Nvalu5TlH7JvkKvX0BAzDjck8FY;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;)V

    .line 67
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 66
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 70
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p1

    .line 69
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    .line 70
    invoke-virtual {p1, v0}, Lcom/squareup/permissions/EmployeeInfo;->getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$cePlQqc8brG5yNhhmgl4PpRKLXE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/timecards/-$$Lambda$cePlQqc8brG5yNhhmgl4PpRKLXE;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v0, v2, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfigForReminderLandingScreen(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attach$0$EndBreakAfterLoginConfirmationCoordinator()Lrx/Subscription;
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAfterLoginConfirmationScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 62
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$35vLcCfOxvClCvikevEWTvCbYjQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$35vLcCfOxvClCvikevEWTvCbYjQ;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;)V

    .line 63
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$GRtl2uFqDfBU_Aa4JZPUXX6CGLw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginConfirmationCoordinator$GRtl2uFqDfBU_Aa4JZPUXX6CGLw;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;)V

    .line 64
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$EndBreakAfterLoginConfirmationCoordinator(Landroid/view/View;)V
    .locals 0

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToHomeScreen()V

    return-void
.end method
