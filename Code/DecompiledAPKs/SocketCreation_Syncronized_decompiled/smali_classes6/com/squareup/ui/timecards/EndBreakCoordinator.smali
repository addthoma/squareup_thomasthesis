.class public Lcom/squareup/ui/timecards/EndBreakCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "EndBreakCoordinator.java"


# instance fields
.field private successContainer:Landroid/view/View;

.field private successTitle:Lcom/squareup/marketfont/MarketTextView;

.field private successView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 28
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 29
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakCoordinator$rDR5FPP8oLJ1yzlerNrvfm0D7jQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakCoordinator$rDR5FPP8oLJ1yzlerNrvfm0D7jQ;-><init>(Lcom/squareup/ui/timecards/EndBreakCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 66
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->successView:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->successView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->successContainer:Landroid/view/View;

    .line 69
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProgressBarTitle()Ljava/lang/String;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_progress_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$EndBreakCoordinator()Lrx/Subscription;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 30
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$UMwxHsBqCMSvV-CYbTDYgV94f3c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$UMwxHsBqCMSvV-CYbTDYgV94f3c;-><init>(Lcom/squareup/ui/timecards/EndBreakCoordinator;)V

    .line 31
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$BrwnrQ4lit8Rn7IWKa6I2j0AVHM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$BrwnrQ4lit8Rn7IWKa6I2j0AVHM;-><init>(Lcom/squareup/ui/timecards/EndBreakCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$NruoTWry9BwQFdHtMiiVdk9BRXQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$NruoTWry9BwQFdHtMiiVdk9BRXQ;-><init>(Lcom/squareup/ui/timecards/EndBreakCoordinator;)V

    .line 32
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 3

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;

    invoke-direct {v2, v1}, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateConfigForSuccessScreen(Ljava/lang/Runnable;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_success_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 57
    sget v0, Lcom/squareup/ui/timecards/R$id;->success_notes_button:I

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/timecards/EndBreakCoordinator;->showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V

    .line 59
    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->successContainer:Landroid/view/View;

    sget-object v0, Lcom/squareup/ui/timecards/EndBreakCoordinator;->MOBILE_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method
