.class public final Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;
.super Ljava/lang/Object;
.source "RealTimecardsHomeScreenRedirector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;",
        ">;"
    }
.end annotation


# instance fields
.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;)",
            "Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/permissions/PasscodeEmployeeManagement;)Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {v0}, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;->newInstance(Lcom/squareup/permissions/PasscodeEmployeeManagement;)Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector_Factory;->get()Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;

    move-result-object v0

    return-object v0
.end method
