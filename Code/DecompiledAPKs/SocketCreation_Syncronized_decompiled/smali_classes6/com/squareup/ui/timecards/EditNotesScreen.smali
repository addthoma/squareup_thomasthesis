.class public final Lcom/squareup/ui/timecards/EditNotesScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "EditNotesScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/EditNotesScreen$Data;,
        Lcom/squareup/ui/timecards/EditNotesScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 \u000b2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002\u000b\u000cB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/EditNotesScreen;",
        "Lcom/squareup/ui/timecards/InTimecardsScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "()V",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "Data",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/EditNotesScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/timecards/EditNotesScreen$Companion;

.field public static final INSTANCE:Lcom/squareup/ui/timecards/EditNotesScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/timecards/EditNotesScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/EditNotesScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/timecards/EditNotesScreen;->Companion:Lcom/squareup/ui/timecards/EditNotesScreen$Companion;

    .line 26
    new-instance v0, Lcom/squareup/ui/timecards/EditNotesScreen;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/EditNotesScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/timecards/EditNotesScreen;->INSTANCE:Lcom/squareup/ui/timecards/EditNotesScreen;

    .line 28
    sget-object v0, Lcom/squareup/ui/timecards/EditNotesScreen;->INSTANCE:Lcom/squareup/ui/timecards/EditNotesScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/timecards/EditNotesScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    const-class v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    .line 20
    invoke-interface {p1}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->editNotesCoordinator()Lcom/squareup/ui/timecards/EditNotesCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 15
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards_add_or_edit_notes:I

    return v0
.end method
