.class public Lcom/squareup/ui/timecards/ClockInOutPresenter;
.super Lmortar/ViewPresenter;
.source "ClockInOutPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;,
        Lcom/squareup/ui/timecards/ClockInOutPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/timecards/ClockInOutView;",
        ">;"
    }
.end annotation


# static fields
.field private static final DELAY_MS:J = 0x7d0L

.field private static final PASSCODE_LENGTH:I = 0x4


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final autoFinisher:Ljava/lang/Runnable;

.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final breakTrackingHelper:Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;

.field private clockInSubscription:Lrx/Subscription;

.field private clockOutSubscription:Lrx/Subscription;

.field private final digits:Ljava/lang/StringBuilder;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeesService:Lcom/squareup/permissions/EmployeesServiceHelper;

.field private enabled:Z

.field private final featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private isFirstScreenInLayer:Z

.field private final mainScheduler:Lrx/Scheduler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final res:Lcom/squareup/util/Res;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private success:Z

.field private final timecards:Lcom/squareup/ui/timecards/Timecards;

.field private final timecardsService:Lcom/squareup/server/employees/TimecardsService;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/EmployeesServiceHelper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/employees/TimecardsService;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/analytics/Analytics;)V
    .locals 3
    .param p14    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 96
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    .line 77
    new-instance v1, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/ui/timecards/ClockInOutPresenter$1;)V

    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->breakTrackingHelper:Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;

    .line 81
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockInSubscription:Lrx/Subscription;

    .line 82
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockOutSubscription:Lrx/Subscription;

    const/4 v1, 0x1

    .line 84
    iput-boolean v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->enabled:Z

    const/4 v1, 0x0

    .line 85
    iput-boolean v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->success:Z

    .line 87
    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$v9eWvAnLgxWjCHcY58-xlAlppBw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$v9eWvAnLgxWjCHcY58-xlAlppBw;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->autoFinisher:Ljava/lang/Runnable;

    move-object v1, p1

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->employeesService:Lcom/squareup/permissions/EmployeesServiceHelper;

    move-object v1, p2

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object v1, p3

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object v1, p4

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object v1, p5

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    .line 102
    invoke-interface {p6}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->internetState:Lio/reactivex/Observable;

    move-object v1, p7

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    move-object v1, p8

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->flow:Lflow/Flow;

    move-object v1, p9

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p10

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    move-object v1, p11

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->timecards:Lcom/squareup/ui/timecards/Timecards;

    move-object v1, p12

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p13

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    move-object/from16 v1, p14

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainScheduler:Lrx/Scheduler;

    move-object/from16 v1, p15

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    move-object/from16 v1, p16

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lflow/Flow;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/ui/timecards/ClockInOutPresenter;Ljava/lang/String;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->showProgress(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/receiving/StandardReceiver;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lrx/Scheduler;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainScheduler:Lrx/Scheduler;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/server/employees/TimecardsService;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/FeatureReleaseHelper;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/ui/timecards/Timecards;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->timecards:Lcom/squareup/ui/timecards/Timecards;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Ljava/lang/Object;
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/timecards/ClockInOutPresenter;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method private attemptClockIn(Lcom/squareup/permissions/Employee;Ljava/lang/String;)V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockInSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->employeesService:Lcom/squareup/permissions/EmployeesServiceHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/permissions/EmployeesServiceHelper;->clockIn(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$GGHD31cFUoxooAXPIYx-7xhjq0c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$GGHD31cFUoxooAXPIYx-7xhjq0c;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    .line 228
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$Yy9saSvz7ncL89EMnAa5FTq0B98;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$Yy9saSvz7ncL89EMnAa5FTq0B98;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/permissions/Employee;Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$b2sj-PN4zUjClHJBXWdr3DwnNBc;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$b2sj-PN4zUjClHJBXWdr3DwnNBc;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    .line 229
    invoke-virtual {v0, v1, p1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockInSubscription:Lrx/Subscription;

    return-void
.end method

.method private attemptClockOut(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockOutSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->employeesService:Lcom/squareup/permissions/EmployeesServiceHelper;

    invoke-virtual {v0, p3, p2}, Lcom/squareup/permissions/EmployeesServiceHelper;->clockOut(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$VuYfKq3e7h4aZ6qNOHhifpQXiGQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$VuYfKq3e7h4aZ6qNOHhifpQXiGQ;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    .line 244
    invoke-virtual {p2, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$HRZJC5JQYlc911G2Omt2aeiyZVM;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$HRZJC5JQYlc911G2Omt2aeiyZVM;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/permissions/Employee;Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$tOp_Dzh5kfiwosQmohVXOk6F_h8;

    invoke-direct {p1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$tOp_Dzh5kfiwosQmohVXOk6F_h8;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    .line 245
    invoke-virtual {p2, v0, p1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockOutSubscription:Lrx/Subscription;

    return-void
.end method

.method private clockInOutAttemptStarted()V
    .locals 5

    const/4 v0, 0x0

    .line 193
    iput-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->enabled:Z

    .line 194
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/ClockInOutView;

    .line 195
    invoke-virtual {v1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setPasscodePadEnabled(Z)V

    .line 196
    invoke-virtual {v1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setClearEnabled(Z)V

    .line 197
    iput-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->enabled:Z

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getEmployeeForTimeTracking(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$ix7z9XQWTbbqOO3cXBUXBKBxk2g;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$ix7z9XQWTbbqOO3cXBUXBKBxk2g;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;)V

    .line 204
    invoke-static {}, Lio/reactivex/internal/functions/Functions;->emptyConsumer()Lio/reactivex/functions/Consumer;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$1D9l32EVcWwgRWT8GpSHcBsJPgM;

    invoke-direct {v4, p0, v1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$1D9l32EVcWwgRWT8GpSHcBsJPgM;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/ui/timecards/ClockInOutView;)V

    .line 200
    invoke-virtual {v0, v2, v3, v4}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 147
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/ui/timecards/ClockInOutView;Lcom/squareup/connectivity/InternetState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x1

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    return-void
.end method

.method private onClockInResponse(Lcom/squareup/server/employees/ClockInOutResponse;Lcom/squareup/permissions/Employee;Ljava/lang/String;)V
    .locals 2

    .line 258
    iget-object v0, p1, Lcom/squareup/server/employees/ClockInOutResponse;->status:Ljava/lang/String;

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_0

    .line 261
    iget-object p3, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p1, Lcom/squareup/server/employees/ClockInOutResponse;->timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;

    iget-object v0, v0, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/employees/ClockInOutResponse;->timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;

    iget-object p1, p1, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->clockin_time:Ljava/lang/String;

    invoke-virtual {p3, p2, v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->updateEmployeeTimecardId(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p2, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 265
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->hasView()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 266
    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/ui/timecards/R$string;->timecard_clock_in_title:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string p3, "employee_name"

    .line 267
    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 269
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 270
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/timecards/ClockInOutView;->showClockInSuccess(Ljava/lang/String;)V

    :cond_1
    const/4 p1, 0x1

    .line 272
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->success:Z

    .line 273
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->autoFinisher:Ljava/lang/Runnable;

    const-wide/16 v0, 0x7d0

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 275
    :cond_2
    iget-object p1, p1, Lcom/squareup/server/employees/ClockInOutResponse;->timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;

    iget-object p1, p1, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->id:Ljava/lang/String;

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->attemptClockOut(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private onClockOutResponse(Lcom/squareup/server/employees/ClockInOutResponse;Lcom/squareup/permissions/Employee;Ljava/lang/String;)V
    .locals 4

    .line 281
    iget-object v0, p1, Lcom/squareup/server/employees/ClockInOutResponse;->status:Ljava/lang/String;

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string p3, ""

    if-eqz p2, :cond_0

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->updateEmployeeTimecardId(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, p3

    .line 289
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->hasView()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_5

    .line 291
    invoke-static {p1}, Lcom/squareup/permissions/EmployeesServiceHelper;->getTotalTime(Lcom/squareup/server/employees/ClockInOutResponse;)Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;

    move-result-object p1

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "employee_name"

    .line 294
    invoke-virtual {v0, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 295
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 296
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 298
    iget v0, p1, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;->minutes:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_total_minutes_singular:I

    .line 299
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 300
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget v0, p1, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;->minutes:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_total_minutes_zero:I

    .line 301
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_total_minutes_plural:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget v2, p1, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;->minutes:I

    const-string v3, "num_minutes"

    .line 303
    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 307
    :goto_1
    iget v2, p1, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;->hours:I

    if-nez v2, :cond_3

    goto :goto_2

    .line 309
    :cond_3
    iget p3, p1, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;->hours:I

    if-ne p3, v1, :cond_4

    .line 310
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/ui/timecards/R$string;->timecard_total_hours_singular:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    goto :goto_2

    .line 312
    :cond_4
    iget-object p3, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_total_hours_plural:I

    invoke-interface {p3, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    iget p1, p1, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;->hours:I

    const-string v2, "num_hours"

    .line 313
    invoke-virtual {p3, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 314
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    .line 317
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, p3, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->showClockOutSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_5
    iput-boolean v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->success:Z

    .line 320
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object p2, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->autoFinisher:Ljava/lang/Runnable;

    const-wide/16 v0, 0x7d0

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    .line 322
    :cond_6
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->attemptClockIn(Lcom/squareup/permissions/Employee;Ljava/lang/String;)V

    :goto_3
    return-void
.end method

.method private onServerError()V
    .locals 2

    .line 333
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/ClockInOutView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    :cond_0
    return-void
.end method

.method private showProgress(Ljava/lang/String;)V
    .locals 1

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/ClockInOutView;->showProgressBar(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private startTimecardsFlow(Lcom/squareup/permissions/Employee;)V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_BREAK_TRACKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 214
    iget-object v0, p1, Lcom/squareup/permissions/Employee;->timecardId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->attemptClockIn(Lcom/squareup/permissions/Employee;Ljava/lang/String;)V

    goto :goto_0

    .line 217
    :cond_0
    iget-object v0, p1, Lcom/squareup/permissions/Employee;->timecardId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->attemptClockOut(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->timecards:Lcom/squareup/ui/timecards/Timecards;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/Timecards;->setEmployee(Lcom/squareup/permissions/Employee;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->breakTrackingHelper:Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;

    invoke-static {v0, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;->access$100(Lcom/squareup/ui/timecards/ClockInOutPresenter$BreakTrackingHelper;Lcom/squareup/permissions/Employee;)V

    :goto_0
    return-void
.end method


# virtual methods
.method finish()V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_EXIT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->autoFinisher:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public synthetic lambda$attemptClockIn$7$ClockInOutPresenter()V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_punching_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->showProgress(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attemptClockIn$8$ClockInOutPresenter(Lcom/squareup/permissions/Employee;Ljava/lang/String;Lcom/squareup/server/employees/ClockInOutResponse;)V
    .locals 0

    .line 230
    invoke-direct {p0, p3, p1, p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onClockInResponse(Lcom/squareup/server/employees/ClockInOutResponse;Lcom/squareup/permissions/Employee;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attemptClockIn$9$ClockInOutPresenter(Ljava/lang/Throwable;)V
    .locals 1

    .line 232
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onServerError()V

    return-void

    .line 235
    :cond_0
    new-instance v0, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p1}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public synthetic lambda$attemptClockOut$10$ClockInOutPresenter()V
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_punching_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->showProgress(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attemptClockOut$11$ClockInOutPresenter(Lcom/squareup/permissions/Employee;Ljava/lang/String;Lcom/squareup/server/employees/ClockInOutResponse;)V
    .locals 0

    .line 246
    invoke-direct {p0, p3, p1, p2}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onClockOutResponse(Lcom/squareup/server/employees/ClockInOutResponse;Lcom/squareup/permissions/Employee;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attemptClockOut$12$ClockInOutPresenter(Ljava/lang/Throwable;)V
    .locals 1

    .line 248
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 249
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onServerError()V

    return-void

    .line 251
    :cond_0
    new-instance v0, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p1}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public synthetic lambda$clockInOutAttemptStarted$5$ClockInOutPresenter(Lcom/squareup/permissions/Employee;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_SUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 203
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->startTimecardsFlow(Lcom/squareup/permissions/Employee;)V

    return-void
.end method

.method public synthetic lambda$clockInOutAttemptStarted$6$ClockInOutPresenter(Lcom/squareup/ui/timecards/ClockInOutView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_UNSUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    const/4 v0, 0x1

    .line 207
    iput-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->enabled:Z

    .line 208
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOutView;->incorrectPasscode()V

    return-void
.end method

.method public synthetic lambda$null$3$ClockInOutPresenter(Lcom/squareup/ui/timecards/ClockInOutView;Lcom/squareup/connectivity/InternetState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    iget-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->success:Z

    if-nez v0, :cond_1

    .line 152
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq p2, v0, :cond_0

    const/4 p2, 0x1

    .line 153
    invoke-virtual {p1, p2}, Lcom/squareup/ui/timecards/ClockInOutView;->showError(Z)V

    goto :goto_0

    .line 155
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOutView;->resetView()V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onLoad$2$ClockInOutPresenter(Lcom/squareup/ui/timecards/ClockInOutView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$kDmoTb68F26ztaWZITlwviYrns0;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$kDmoTb68F26ztaWZITlwviYrns0;

    .line 147
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$tc4mTFJhmlqApXJ33aFV1CLeAaw;

    invoke-direct {v1, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$tc4mTFJhmlqApXJ33aFV1CLeAaw;-><init>(Lcom/squareup/ui/timecards/ClockInOutView;)V

    .line 148
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$ClockInOutPresenter(Lcom/squareup/ui/timecards/ClockInOutView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$cPpqCVo-Q1HnIG2O5hKTHQqRwyU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$cPpqCVo-Q1HnIG2O5hKTHQqRwyU;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/ui/timecards/ClockInOutView;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method onClearPressed()V
    .locals 3

    .line 173
    iget-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->enabled:Z

    if-nez v0, :cond_0

    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 175
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/ClockInOutView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/ClockInOutView;->updateStars(I)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 116
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutScreen;

    .line 117
    iget-boolean p1, p1, Lcom/squareup/ui/timecards/ClockInOutScreen;->firstScreenInLayer:Z

    iput-boolean p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->isFirstScreenInLayer:Z

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockInSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockOutSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method onKeyPressed(C)V
    .locals 1

    .line 162
    iget-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->enabled:Z

    if-nez v0, :cond_0

    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/ClockInOutView;->updateStars(I)V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 167
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->clockInOutAttemptStarted()V

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_1
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 126
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView;

    const/4 v0, 0x4

    .line 129
    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setExpectedStars(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 131
    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setStarCount(I)V

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 132
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setClearEnabled(Z)V

    .line 134
    iget-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->isFirstScreenInLayer:Z

    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOutView;->setGlyphX()V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 142
    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->startTimecardsFlow(Lcom/squareup/permissions/Employee;)V

    .line 146
    :cond_1
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BvemxfQd11pXV-rNw4koTist6qg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$BvemxfQd11pXV-rNw4koTist6qg;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/ui/timecards/ClockInOutView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 150
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$zcKUPoHUCC1R2uw7RJFS2l2I1gw;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutPresenter$zcKUPoHUCC1R2uw7RJFS2l2I1gw;-><init>(Lcom/squareup/ui/timecards/ClockInOutPresenter;Lcom/squareup/ui/timecards/ClockInOutView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method showBackButton()Z
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 181
    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutPresenter;->isFirstScreenInLayer:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
