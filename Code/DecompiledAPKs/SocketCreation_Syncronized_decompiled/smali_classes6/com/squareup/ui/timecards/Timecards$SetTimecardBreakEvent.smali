.class Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;
.super Lcom/squareup/ui/timecards/Timecards$Event;
.source "Timecards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetTimecardBreakEvent"
.end annotation


# instance fields
.field public final expectedBreakDurationSeconds:Ljava/lang/Integer;

.field public final minBreakDurationSeconds:Ljava/lang/Integer;

.field final synthetic this$0:Lcom/squareup/ui/timecards/Timecards;

.field public final timecardBreakStartTimestamp:Ljava/util/Date;

.field public final timecardBreakToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .line 583
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    .line 584
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->timecardBreakToken:Ljava/lang/String;

    .line 585
    iput-object p3, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->timecardBreakStartTimestamp:Ljava/util/Date;

    .line 586
    iput-object p4, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    .line 587
    iput-object p5, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->minBreakDurationSeconds:Ljava/lang/Integer;

    return-void
.end method
