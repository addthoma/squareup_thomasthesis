.class public Lcom/squareup/ui/timecards/Timecards;
.super Ljava/lang/Object;
.source "Timecards.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;,
        Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;,
        Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;,
        Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;,
        Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;,
        Lcom/squareup/ui/timecards/Timecards$StopTimecardEvent;,
        Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;,
        Lcom/squareup/ui/timecards/Timecards$SetJobs;,
        Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;,
        Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;,
        Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;,
        Lcom/squareup/ui/timecards/Timecards$SetEmployee;,
        Lcom/squareup/ui/timecards/Timecards$SetFailedRequestStateEvent;,
        Lcom/squareup/ui/timecards/Timecards$SetLoadingRequestStateEvent;,
        Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;,
        Lcom/squareup/ui/timecards/Timecards$Event;,
        Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;,
        Lcom/squareup/ui/timecards/Timecards$State;
    }
.end annotation


# instance fields
.field private final events:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/timecards/Timecards$Event;",
            ">;"
        }
    .end annotation
.end field

.field private featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

.field private final state:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/timecards/Timecards$State;",
            ">;"
        }
    .end annotation
.end field

.field private final subs:Lrx/subscriptions/CompositeSubscription;

.field private final timecardsService:Lcom/squareup/server/employees/TimecardsService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/employees/TimecardsService;Lcom/squareup/ui/timecards/FeatureReleaseHelper;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/squareup/ui/timecards/Timecards$State;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;)V

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 51
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    .line 66
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    return-void
.end method

.method public static synthetic lambda$3DhiOeRoisZXrEjrW-61qD_OEYg(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$Event;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onEvent(Lcom/squareup/ui/timecards/Timecards$Event;Lcom/squareup/ui/timecards/Timecards$State;)V

    return-void
.end method

.method static synthetic lambda$onSaveNotesEvent$14(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesResponse;)V
    .locals 0

    return-void
.end method

.method private onEndTimecardBreakEvent(Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 331
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;-><init>()V

    iget-object p2, p2, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    .line 333
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->timecard_break_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    .line 334
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->authorization(Lcom/squareup/protos/client/timecards/Authorization;)Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;

    move-result-object p1

    .line 336
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v0, Lcom/squareup/ui/timecards/Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    .line 337
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->should_block_early_clockin(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;

    .line 340
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/server/employees/TimecardsService;->stopBreak(Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 341
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$cVIPoQZgBNoI6TJX4Ekw8DXcoJo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$cVIPoQZgBNoI6TJX4Ekw8DXcoJo;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    .line 342
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$UPAK8ahvVNPbWw5SDx8eUxgNZNs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$UPAK8ahvVNPbWw5SDx8eUxgNZNs;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 340
    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private onEvent(Lcom/squareup/ui/timecards/Timecards$Event;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 1

    .line 154
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 155
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;

    if-eqz v0, :cond_0

    .line 156
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onInitRequestStateEvent(Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto/16 :goto_0

    .line 157
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetLoadingRequestStateEvent;

    if-eqz v0, :cond_1

    .line 158
    invoke-direct {p0, p2}, Lcom/squareup/ui/timecards/Timecards;->onSetLoadingRequestStateEvent(Lcom/squareup/ui/timecards/Timecards$State;)V

    goto/16 :goto_0

    .line 159
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetFailedRequestStateEvent;

    if-eqz v0, :cond_2

    .line 160
    invoke-direct {p0, p2}, Lcom/squareup/ui/timecards/Timecards;->onSetFailedRequestStateEvent(Lcom/squareup/ui/timecards/Timecards$State;)V

    goto/16 :goto_0

    .line 161
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetEmployee;

    if-eqz v0, :cond_3

    .line 162
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SetEmployee;

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/Timecards;->onSetEmployee(Lcom/squareup/ui/timecards/Timecards$SetEmployee;)V

    goto/16 :goto_0

    .line 163
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;

    if-eqz v0, :cond_4

    .line 164
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onSetTimecardEvent(Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto/16 :goto_0

    .line 165
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;

    if-eqz v0, :cond_5

    .line 166
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onSetTimecardBreakEvent(Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 167
    :cond_5
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;

    if-eqz v0, :cond_6

    .line 168
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onSetTimecardBreakDefinitionsEvent(Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 169
    :cond_6
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SetJobs;

    if-eqz v0, :cond_7

    .line 170
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SetJobs;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onSetJobs(Lcom/squareup/ui/timecards/Timecards$SetJobs;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 171
    :cond_7
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;

    if-eqz v0, :cond_8

    .line 172
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onStartTimecardEvent(Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 173
    :cond_8
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$StopTimecardEvent;

    if-eqz v0, :cond_9

    .line 174
    invoke-direct {p0, p2}, Lcom/squareup/ui/timecards/Timecards;->onStopTimecardEvent(Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 175
    :cond_9
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;

    if-eqz v0, :cond_a

    .line 176
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onSwitchJobsEvent(Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 177
    :cond_a
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;

    if-eqz v0, :cond_b

    .line 178
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onStartTimecardBreakEvent(Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 179
    :cond_b
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;

    if-eqz v0, :cond_c

    .line 180
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onEndTimecardBreakEvent(Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    goto :goto_0

    .line 181
    :cond_c
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;

    if-eqz v0, :cond_d

    .line 182
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/Timecards;->onLoadCurrentEmployeeDataEvent(Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;)V

    goto :goto_0

    .line 183
    :cond_d
    instance-of v0, p1, Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;

    if-eqz v0, :cond_e

    .line 184
    check-cast p1, Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards;->onSaveNotesEvent(Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    :cond_e
    :goto_0
    return-void
.end method

.method private onInitRequestStateEvent(Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    .line 191
    invoke-virtual {p2, v1, p1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestStateAndTotalSecondsWorked(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/Long;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    .line 190
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onLoadCurrentEmployeeDataEvent(Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;)V
    .locals 4

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v2, p1, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;->currentEmployee:Lcom/squareup/permissions/Employee;

    invoke-direct {v1, v2}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    .line 354
    new-instance v1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;->currentEmployee:Lcom/squareup/permissions/Employee;

    iget-object v2, v2, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 356
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    move-result-object v1

    .line 357
    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    move-result-object v1

    .line 359
    iget-object v2, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v3, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {v3, v1}, Lcom/squareup/server/employees/TimecardsService;->getOpenTimecardAndBreakResponse(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)Lrx/Observable;

    move-result-object v1

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$LzSC9WcOUuk-w1s5LXS4cjtvi3w;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$LzSC9WcOUuk-w1s5LXS4cjtvi3w;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;Lcom/squareup/ui/timecards/Timecards$State;)V

    new-instance p1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$VKIcC56ad0dL9Lhyn-EonPlbVms;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$VKIcC56ad0dL9Lhyn-EonPlbVms;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$State;)V

    .line 360
    invoke-virtual {v1, v3, p1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 359
    invoke-virtual {v2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private onSaveNotesEvent(Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;->notes:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withNotes(Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 377
    new-instance v0, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;-><init>()V

    iget-object p2, p2, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    .line 378
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;->notes:Ljava/lang/String;

    .line 379
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->note_content(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    move-result-object p1

    .line 380
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->build()Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    move-result-object p1

    .line 381
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {v0, p1}, Lcom/squareup/server/employees/TimecardsService;->updateNotes(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$nGcHyJW4eRFSeYt_F5NlYHZgKxA;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$Timecards$nGcHyJW4eRFSeYt_F5NlYHZgKxA;

    .line 382
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 381
    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private onSetEmployee(Lcom/squareup/ui/timecards/Timecards$SetEmployee;)V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$SetEmployee;->employee:Lcom/squareup/permissions/Employee;

    invoke-direct {v1, p1}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onSetFailedRequestStateEvent(Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onSetJobs(Lcom/squareup/ui/timecards/Timecards$SetJobs;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$SetJobs;->jobs:Ljava/util/List;

    invoke-virtual {p2, v1, p1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestStateAndJobs(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onSetLoadingRequestStateEvent(Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onSetTimecardBreakDefinitionsEvent(Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;->timecardBreakDefinitions:Ljava/util/List;

    .line 225
    invoke-virtual {p2, v1, p1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestStateAndBreakDefinitions(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    .line 224
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onSetTimecardBreakEvent(Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 4

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->timecardBreakToken:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->timecardBreakStartTimestamp:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;->minBreakDurationSeconds:Ljava/lang/Integer;

    invoke-virtual {p2, v1, v2, v3, p1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onSetTimecardEvent(Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 9

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v2, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->timecardToken:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->clockInUnitToken:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->timecardStartTimestamp:Ljava/util/Date;

    iget-object v5, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v6, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->employeeJobInfo:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v7, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    iget-object v8, p1, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->notes:Ljava/lang/String;

    move-object v1, p2

    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private onStartTimecardBreakEvent(Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 310
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;

    iget-object p2, p2, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;->timecardBreakDefinitionToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;->timecardBreakVersionNumber:Ljava/lang/Long;

    invoke-direct {v0, p2, v1, p1}, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 313
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {p2, v0}, Lcom/squareup/server/employees/TimecardsService;->startBreak(Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;)Lrx/Observable;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 314
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$E0vU1boEBr5ObwkcrTejZYfEgCw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$E0vU1boEBr5ObwkcrTejZYfEgCw;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    .line 315
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$WhI7u19YwDRX__NG1TUF3ES5WPA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$WhI7u19YwDRX__NG1TUF3ES5WPA;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    invoke-virtual {p2, v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p2

    .line 313
    invoke-virtual {p1, p2}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private onStartTimecardEvent(Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 236
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;-><init>()V

    iget-object p2, p2, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object p2, p2, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 237
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->unitToken:Ljava/lang/String;

    .line 238
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->clockin_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    move-result-object p2

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 241
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->use_job_token(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    .line 243
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->jobToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 244
    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->jobToken:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->job_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    .line 246
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    move-result-object p1

    .line 247
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {v0, p1}, Lcom/squareup/server/employees/TimecardsService;->startTimecard(Lcom/squareup/protos/client/timecards/StartTimecardRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 248
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$RX21MwYhrWQ30zHlNRnPmigaj7o;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$RX21MwYhrWQ30zHlNRnPmigaj7o;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    .line 249
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$YeklsWSTLGTevXbefIRLmpRwxEs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$YeklsWSTLGTevXbefIRLmpRwxEs;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 247
    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private onStopTimecardEvent(Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 3

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 265
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;-><init>()V

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    .line 266
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;

    move-result-object p1

    .line 267
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardRequest;

    move-result-object p1

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {v1, p1}, Lcom/squareup/server/employees/TimecardsService;->stopTimecard(Lcom/squareup/protos/client/timecards/StopTimecardRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 269
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$CiaDPw-yjC2smLA5Angdg65sMQA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$CiaDPw-yjC2smLA5Angdg65sMQA;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    .line 270
    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$34e5AsfIa7HL8VZeFvSwZTyFihU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$34e5AsfIa7HL8VZeFvSwZTyFihU;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    invoke-virtual {p1, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 268
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private onSwitchJobsEvent(Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 283
    new-instance v0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;-><init>()V

    iget-object v1, p2, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object v1, v1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 284
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    .line 285
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;

    move-result-object v0

    iget-object p2, p2, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    .line 286
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;

    move-result-object p2

    .line 287
    iget-object v0, p1, Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;->jobToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 288
    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;->jobToken:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->job_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;

    .line 291
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->build()Lcom/squareup/protos/client/timecards/SwitchJobsRequest;

    move-result-object p1

    .line 292
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {v0, p1}, Lcom/squareup/server/employees/TimecardsService;->switchJobs(Lcom/squareup/protos/client/timecards/SwitchJobsRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 293
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$NSFsOnU3MR_Dd1GRY-aiR6SSKBQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$NSFsOnU3MR_Dd1GRY-aiR6SSKBQ;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    .line 294
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$w7tL6sPs2Gm6xRVU9EtXmJLq6dU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$w7tL6sPs2Gm6xRVU9EtXmJLq6dU;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 292
    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private processTimecardAndJobResponses(Lcom/squareup/ui/timecards/Timecards$State;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V
    .locals 9

    .line 389
    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, v0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    new-instance v4, Ljava/util/Date;

    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 392
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    iget-object v5, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v6, v0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v7, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v8, v0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    move-object v1, p1

    .line 390
    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    .line 398
    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 401
    invoke-static {v1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iget-object v2, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    .line 399
    invoke-virtual {p1, v0, v1, v2, p2}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    .line 407
    :cond_0
    sget-object p2, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    if-eqz p3, :cond_1

    iget-object p3, p3, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;->ordered_employee_job_info:Ljava/util/List;

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    :goto_0
    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestStateAndJobs(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    .line 410
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getEmployee()Lcom/squareup/permissions/Employee;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v0, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    return-object v0
.end method

.method public getLatestState()Lcom/squareup/ui/timecards/Timecards$State;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    return-object v0
.end method

.method public initRequestState(Ljava/lang/Long;)V
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/ui/timecards/Timecards$InitRequestStateEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/Long;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$10$Timecards(Lcom/squareup/ui/timecards/Timecards$State;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V
    .locals 0

    .line 365
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/timecards/Timecards;->processTimecardAndJobResponses(Lcom/squareup/ui/timecards/Timecards$State;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    return-void
.end method

.method public synthetic lambda$null$11$Timecards(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Throwable;)V
    .locals 1

    .line 367
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEndTimecardBreakEvent$8$Timecards(Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 1

    .line 343
    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 344
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/ui/timecards/Timecards$State;->clearTimecardBreak()Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 346
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEndTimecardBreakEvent$9$Timecards(Ljava/lang/Throwable;)V
    .locals 2

    .line 348
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoadCurrentEmployeeDataEvent$12$Timecards(Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;Lcom/squareup/ui/timecards/Timecards$State;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V
    .locals 3

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/timecards/Timecards;->timecardsService:Lcom/squareup/server/employees/TimecardsService;

    new-instance v2, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;

    iget-object p1, p1, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;->currentEmployee:Lcom/squareup/permissions/Employee;

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-direct {v2, p1}, Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/server/employees/TimecardsService;->getEmployeeJobInfos(Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosRequest;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$zGS0DWmoNHZdRqQPuhcbowxdncY;

    invoke-direct {v1, p0, p2, p3}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$zGS0DWmoNHZdRqQPuhcbowxdncY;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$State;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V

    new-instance p3, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$2-DIWnnpVCCJaZCS885rlW7nzx0;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$2-DIWnnpVCCJaZCS885rlW7nzx0;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$State;)V

    .line 364
    invoke-virtual {p1, v1, p3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 362
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 370
    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/ui/timecards/Timecards;->processTimecardAndJobResponses(Lcom/squareup/ui/timecards/Timecards$State;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;Lcom/squareup/protos/client/employeejobs/GetEmployeeJobInfosResponse;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onLoadCurrentEmployeeDataEvent$13$Timecards(Lcom/squareup/ui/timecards/Timecards$State;Ljava/lang/Throwable;)V
    .locals 1

    .line 372
    iget-object p2, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onStartTimecardBreakEvent$6$Timecards(Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 4

    .line 316
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, v2, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 318
    invoke-static {v2}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, v3, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    .line 317
    invoke-virtual {p2, v1, v2, v3, p1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 323
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onStartTimecardBreakEvent$7$Timecards(Ljava/lang/Throwable;)V
    .locals 2

    .line 325
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onStartTimecardEvent$0$Timecards(Lcom/squareup/protos/client/timecards/StartTimecardResponse;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 10

    .line 250
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, v1, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v4, v1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    new-instance v5, Ljava/util/Date;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 253
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v1, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    const/4 v8, 0x0

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, p1, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    move-object v2, p2

    .line 251
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 258
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onStartTimecardEvent$1$Timecards(Ljava/lang/Throwable;)V
    .locals 2

    .line 260
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onStopTimecardEvent$2$Timecards(Lcom/squareup/protos/client/timecards/StopTimecardResponse;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 3

    .line 271
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    .line 273
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    invoke-virtual {p2, v1, v2, p1}, Lcom/squareup/ui/timecards/Timecards$State;->clearTimecard(JLcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    .line 272
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 276
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onStopTimecardEvent$3$Timecards(Ljava/lang/Throwable;)V
    .locals 2

    .line 278
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSwitchJobsEvent$4$Timecards(Lcom/squareup/protos/client/timecards/SwitchJobsResponse;Lcom/squareup/ui/timecards/Timecards$State;)V
    .locals 10

    .line 295
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, v1, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v4, v1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    new-instance v5, Ljava/util/Date;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 298
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v5, v1, v2}, Ljava/util/Date;-><init>(J)V

    const/4 v1, 0x1

    .line 299
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v1, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    const/4 v8, 0x0

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, p1, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    move-object v2, p2

    .line 296
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 303
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onSwitchJobsEvent$5$Timecards(Ljava/lang/Throwable;)V
    .locals 2

    .line 305
    iget-object p1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/Timecards$State;

    sget-object v1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->FAILED:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public loadCurrentEmployeeData(Lcom/squareup/permissions/Employee;)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/Timecards$LoadCurrentEmployeeDataEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/permissions/Employee;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 56
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$3DhiOeRoisZXrEjrW-61qD_OEYg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$Timecards$3DhiOeRoisZXrEjrW-61qD_OEYg;-><init>(Lcom/squareup/ui/timecards/Timecards;)V

    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 55
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method

.method public saveNotes(Ljava/lang/String;)V
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/ui/timecards/Timecards$SaveNotesEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setEmployee(Lcom/squareup/permissions/Employee;)V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SetEmployee;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/ui/timecards/Timecards$SetEmployee;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setFailedRequestState()V
    .locals 3

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SetFailedRequestStateEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/timecards/Timecards$SetFailedRequestStateEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setJobs(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;)V"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SetJobs;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/ui/timecards/Timecards$SetJobs;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/util/List;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setLoadingRequestState()V
    .locals 3

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SetLoadingRequestStateEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/timecards/Timecards$SetLoadingRequestStateEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 13

    move-object v10, p0

    .line 101
    iget-object v11, v10, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v12, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;

    const/4 v9, 0x0

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v11, v12}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 8

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v7, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual {v0, v7}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setTimecardBreakDefinitions(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;)V"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/util/List;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public startBreak(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards$StartTimecardBreakEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public startTimecard(Ljava/lang/String;)V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public startTimecard(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public state()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/timecards/Timecards$State;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->state:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public stopBreak(Lcom/squareup/protos/client/timecards/Authorization;)V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/protos/client/timecards/Authorization;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public stopTimecard()V
    .locals 3

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$StopTimecardEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/timecards/Timecards$StopTimecardEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public switchJobs(Ljava/lang/String;)V
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards;->events:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/ui/timecards/Timecards$SwitchJobsEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Lcom/squareup/ui/timecards/Timecards$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
