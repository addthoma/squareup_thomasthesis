.class public abstract Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;
.super Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
.source "DepositOptionsReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LinkingResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000b\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000b\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "()V",
        "BankFailure",
        "BankPending",
        "BankPendingCardFailure",
        "BankPendingCardSkip",
        "BankPendingCardSuccessSameDayDepositSetupFailure",
        "BankPendingCardSuccessSameDayDepositSetupSuccess",
        "BankSuccess",
        "BankSuccessCardFailure",
        "BankSuccessCardSkip",
        "BankSuccessCardSuccessSameDayDepositSetupFailure",
        "BankSuccessCardSuccessSameDayDepositSetupSuccess",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 166
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 166
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;-><init>()V

    return-void
.end method
