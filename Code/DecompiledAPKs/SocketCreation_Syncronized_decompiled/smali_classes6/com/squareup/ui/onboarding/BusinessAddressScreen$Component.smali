.class public interface abstract Lcom/squareup/ui/onboarding/BusinessAddressScreen$Component;
.super Ljava/lang/Object;
.source "BusinessAddressScreen.java"

# interfaces
.implements Lcom/squareup/address/AddressLayout$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/BusinessAddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/onboarding/BusinessAddressView;)V
.end method
