.class Lcom/squareup/ui/onboarding/LoadingStatusPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "LoadingStatusPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationService:Lcom/squareup/server/activation/ActivationService;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final statusRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->statusRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private call()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->statusRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private showFailure(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->network_error_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->network_error_message:I

    .line 117
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 118
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    .line 119
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_status_failure_title:I

    .line 122
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->server_error_message:I

    .line 123
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 124
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-static {p1, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->show(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/mortar/Popup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 98
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 99
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    .line 103
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    .line 64
    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$LoadingStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 73
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 74
    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/activation/ActivationStatus;

    invoke-virtual {v0}, Lcom/squareup/server/activation/ActivationStatus;->isRetryable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    const-string v0, ""

    invoke-interface {p1, v0}, Lcom/squareup/server/activation/ActivationService;->retry(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 77
    :cond_0
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$LoadingStatusPresenter(Lcom/squareup/server/SimpleResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onLoadingFinished()V

    return-void
.end method

.method public synthetic lambda$null$3$LoadingStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 85
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->showFailure(Z)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$LoadingStatusPresenter(Lkotlin/Unit;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    .line 70
    invoke-interface {p1}, Lcom/squareup/server/activation/ActivationService;->status()Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$m7yKfuYtB4_6nO6-BtonGj2jBdY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$m7yKfuYtB4_6nO6-BtonGj2jBdY;-><init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V

    .line 72
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_progress_title:I

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(I)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$4$LoadingStatusPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$TU5gMjERO3jc23QT-QQKMfZ_mfA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$TU5gMjERO3jc23QT-QQKMfZ_mfA;-><init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$w0R1itLgqUglRlZSVOwY53IHy04;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$w0R1itLgqUglRlZSVOwY53IHy04;-><init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->statusRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$-VNLaLxdN9O6NI9w2p1E4x_YCcE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$-VNLaLxdN9O6NI9w2p1E4x_YCcE;-><init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V

    .line 69
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$qag7Zbc8Kx9hw8_z2CMYQ0nFbXc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$LoadingStatusPresenter$qag7Zbc8Kx9hw8_z2CMYQ0nFbXc;-><init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V

    .line 82
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 68
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 90
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->progressSpinnerDisposable:Lio/reactivex/disposables/Disposable;

    .line 94
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->call()V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 0

    .line 55
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->call()V

    goto :goto_0

    .line 58
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivationCallFailed()V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
