.class public final Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "DepositBankLinkingCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->setUpRoutingNumberField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 159
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->access$getRoutingNumberField$p(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)Landroid/widget/EditText;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->access$getRoutingNumberField$p(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)Landroid/widget/EditText;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/banklinking/RoutingNumberUtil;->validateRoutingNumber(Ljava/lang/String;Landroid/widget/EditText;)V

    return-void
.end method
