.class public Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "ConfirmIdentityScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;

    .line 18
    sget-object v0, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;

    .line 19
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_QUIZ:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 33
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->confirm_identity_view:I

    return v0
.end method
