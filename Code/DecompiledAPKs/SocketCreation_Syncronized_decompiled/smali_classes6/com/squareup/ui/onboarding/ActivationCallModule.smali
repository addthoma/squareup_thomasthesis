.class public Lcom/squareup/ui/onboarding/ActivationCallModule;
.super Ljava/lang/Object;
.source "ActivationCallModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/ActivationCallModule$SharedScope;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideApplyMoreServerCallPresenter(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .locals 11
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/server/activation/ActivationService;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ">;"
        }
    .end annotation

    .line 61
    new-instance v10, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    sget-object v5, Lcom/squareup/server/activation/ActivationStatus$State;->MORE_INFORMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

    sget v7, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_progress_title:I

    sget v8, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_failure_title:I

    move-object v0, v10

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/server/activation/ActivationStatus$State;Lcom/squareup/util/Res;IILcom/squareup/register/widgets/GlassSpinner;)V

    return-object v10
.end method

.method static provideApplyServerCallPresenter(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .locals 11
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/server/activation/ActivationService;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v10, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    sget-object v5, Lcom/squareup/server/activation/ActivationStatus$State;->NO_APPLICATION:Lcom/squareup/server/activation/ActivationStatus$State;

    sget v7, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_progress_title:I

    sget v8, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_failure_title:I

    move-object v0, v10

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/server/activation/ActivationStatus$State;Lcom/squareup/util/Res;IILcom/squareup/register/widgets/GlassSpinner;)V

    return-object v10
.end method

.method static provideConfirmIdentityServerCallPresenter(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .locals 11
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/server/activation/ActivationService;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/AnswersBody;",
            ">;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    sget-object v5, Lcom/squareup/server/activation/ActivationStatus$State;->CONFIRMATION_REQUESTED:Lcom/squareup/server/activation/ActivationStatus$State;

    sget v7, Lcom/squareup/onboarding/flow/R$string;->onboarding_answers_progress:I

    sget v8, Lcom/squareup/onboarding/flow/R$string;->onboarding_answers_fail:I

    move-object v0, v10

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/server/activation/ActivationStatus$State;Lcom/squareup/util/Res;IILcom/squareup/register/widgets/GlassSpinner;)V

    return-object v10
.end method


# virtual methods
.method public provideApplyMoreReceivedResponse(Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ShareableReceivedResponse;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/activation/ActivationService;",
            ")",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$LpgBG9lCTJ5biKu9c-T2QcWmQdk;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$LpgBG9lCTJ5biKu9c-T2QcWmQdk;-><init>(Lcom/squareup/server/activation/ActivationService;)V

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/ShareableReceivedResponse;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public provideApplyReceivedResponse(Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ShareableReceivedResponse;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/activation/ActivationService;",
            ")",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$7kxSnerNqr6Rn025ITo2N126e5A;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$7kxSnerNqr6Rn025ITo2N126e5A;-><init>(Lcom/squareup/server/activation/ActivationService;)V

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/ShareableReceivedResponse;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public provideConfirmIdentityReceivedResponse(Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ShareableReceivedResponse;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/activation/ActivationService;",
            ")",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$IZ4DR_cCXLzA5C4rFRwwht-OKOE;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/-$$Lambda$IZ4DR_cCXLzA5C4rFRwwht-OKOE;-><init>(Lcom/squareup/server/activation/ActivationService;)V

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/ShareableReceivedResponse;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method
