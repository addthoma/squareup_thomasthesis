.class Lcom/squareup/ui/onboarding/OnboardingContainer$1;
.super Lcom/squareup/flowlegacy/NoResultPopupPresenter;
.source "OnboardingContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/OnboardingContainer;-><init>(Lcom/squareup/navigation/NavigationListener;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/badbus/BadBus;Lcom/squareup/onboarding/OnboardingScreenSelector;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/ModelHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
        "Lcom/squareup/widgets/warning/Warning;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/OnboardingContainer;

.field final synthetic val$authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field final synthetic val$loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/OnboardingContainer;Ljava/lang/String;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->this$0:Lcom/squareup/ui/onboarding/OnboardingContainer;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->val$authenticator:Lcom/squareup/account/LegacyAuthenticator;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->val$loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    invoke-direct {p0, p2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 138
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 3

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->hasView()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->val$authenticator:Lcom/squareup/account/LegacyAuthenticator;

    new-instance v1, Lcom/squareup/account/LogOutReason$SessionExpired;

    const-string v2, "LoggedInOnboarding"

    invoke-direct {v1, v2}, Lcom/squareup/account/LogOutReason$SessionExpired;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/account/LegacyAuthenticator;->logOut(Lcom/squareup/account/LogOutReason;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer$1;->val$loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    invoke-interface {v0, p1}, Lcom/squareup/loggedout/LoggedOutStarter;->showLogin(Landroid/content/Context;)V

    return-void
.end method
