.class public Lcom/squareup/ui/onboarding/RealOnboardingDiverter;
.super Ljava/lang/Object;
.source "RealOnboardingDiverter.java"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingDiverter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;
    }
.end annotation


# instance fields
.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final onDivert:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final starter:Lcom/squareup/onboarding/OnboardingStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/settings/server/AccountStatusSettings;Ldagger/Lazy;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->onDivert:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->flow:Ldagger/Lazy;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    .line 46
    iput-object p5, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 47
    iput-object p6, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 48
    iput-object p7, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    return-void
.end method

.method private checkEligibility()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;",
            "Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;",
            ">;"
        }
    .end annotation

    .line 102
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$jrtZapsl6wEnvZdHaFBQUQO9I5Q;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$jrtZapsl6wEnvZdHaFBQUQO9I5Q;-><init>(Lcom/squareup/ui/onboarding/RealOnboardingDiverter;)V

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 104
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$3(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;ZZ)Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 105
    new-instance v0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;ZZLcom/squareup/ui/onboarding/RealOnboardingDiverter$1;)V

    return-object v0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 56
    iget-boolean p0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;->eligibleForActivation:Z

    return p0
.end method


# virtual methods
.method public divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->divertedToOnboarding()V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionController;->handleDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->showInAppActivationPostSignup()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->showActivateAccountConfirmation()Z

    goto :goto_0

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    new-instance v1, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    sget-object v2, Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;->HOME:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    invoke-direct {v1, p1, v2}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingStarter;->startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    goto :goto_0

    .line 89
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$checkEligibility$4$RealOnboardingDiverter(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 103
    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/onboarding/-$$Lambda$Mk1iO-9o5wmHdgcMbV57dXx4_2c;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$Mk1iO-9o5wmHdgcMbV57dXx4_2c;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 104
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailableRx2()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$U3K__eOcPhF7E8fHgW4OAY9w5us;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$U3K__eOcPhF7E8fHgW4OAY9w5us;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$aX0xcEWr64919piTnUuLmv4zn8Q;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$aX0xcEWr64919piTnUuLmv4zn8Q;

    .line 102
    invoke-virtual {p1, v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$RealOnboardingDiverter(Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    new-instance v1, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;->launchMode:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    sget-object v2, Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;->HOME:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    invoke-direct {v1, p1, v2}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingStarter;->startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    return-void
.end method

.method public maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->onDivert:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->onDivert:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->checkEligibility()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$_7oXOLqh64efnnHHt8Mxq8oN0X0;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$_7oXOLqh64efnnHHt8Mxq8oN0X0;

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$eQh93qWXozXjVjAebPoLKJH_MJA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$RealOnboardingDiverter$eQh93qWXozXjVjAebPoLKJH_MJA;-><init>(Lcom/squareup/ui/onboarding/RealOnboardingDiverter;)V

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 54
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public shouldDivertToOnboarding()Z
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
