.class public final Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CardProcessingNotActivatedScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Component;,
        Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$ParentComponent;,
        Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;

    .line 78
    sget-object v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;

    .line 79
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 82
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->card_processing_not_activated_view:I

    return v0
.end method
