.class public Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ActivateYourAccountScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/ActivateYourAccountView;",
        ">;"
    }
.end annotation


# instance fields
.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-void
.end method


# virtual methods
.method onContinued()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivateAccountContinued()V

    return-void
.end method

.method onLater()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivateAccountLater()V

    return-void
.end method
