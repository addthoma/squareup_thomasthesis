.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "it",
        "Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event$AccountTypeSelected;

    if-eqz v0, :cond_0

    .line 219
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnAccountTypeSelected;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event$AccountTypeSelected;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event$AccountTypeSelected;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnAccountTypeSelected;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountType;)V

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 220
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event$Canceled;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event$Canceled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 221
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositAccountTypeDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnCancelClickedFromDepositAccountTypeDialog;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;->invoke(Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    move-result-object p1

    return-object p1
.end method
