.class Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "OnboardingErrorScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->access$100(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/SupportUrlSettings;->getHelpCenterUrl()Ljava/lang/String;

    move-result-object p1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->access$200(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingErrorView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingErrorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
