.class public Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "OnboardingFinishedScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/OnboardingFinishedView;",
        ">;"
    }
.end annotation


# instance fields
.field public final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-void
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 40
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingFinishedView;

    .line 43
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_finished_heading:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->setTitleText(I)V

    .line 44
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_finished_subheading:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->setMessageText(I)V

    .line 45
    sget v0, Lcom/squareup/cardreader/ui/R$string;->get_started:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->setTopButton(I)V

    const/4 v0, -0x1

    .line 46
    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->setBottomButton(I)V

    .line 47
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V

    .line 48
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter$1;-><init>(Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V

    return-void
.end method
