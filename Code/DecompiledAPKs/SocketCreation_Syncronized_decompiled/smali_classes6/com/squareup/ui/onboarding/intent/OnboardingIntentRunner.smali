.class public Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;
.super Ljava/lang/Object;
.source "OnboardingIntentRunner.java"


# instance fields
.field private final activationServiceHelper:Lcom/squareup/onboarding/ActivationServiceHelper;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final flow:Lflow/Flow;

.field private how:Lcom/squareup/onboarding/intent/HowOptions;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private selectedWhereOptions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/onboarding/intent/WhereOptions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->flow:Lflow/Flow;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->activationServiceHelper:Lcom/squareup/onboarding/ActivationServiceHelper;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-void
.end method

.method private send()V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->sendToEventStream()V

    .line 73
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->sendToActivationService()V

    return-void
.end method

.method private sendToActivationService()V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->activationServiceHelper:Lcom/squareup/onboarding/ActivationServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->selectedWhereOptions:Ljava/util/Set;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->how:Lcom/squareup/onboarding/intent/HowOptions;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/onboarding/ActivationServiceHelper;->logMerchantAnalytics(Ljava/util/Set;Lcom/squareup/onboarding/intent/HowOptions;)Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private sendToEventStream()V
    .locals 4

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->selectedWhereOptions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/onboarding/intent/WhereSkippedEvent;

    invoke-direct {v1}, Lcom/squareup/onboarding/intent/WhereSkippedEvent;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->selectedWhereOptions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/intent/WhereOptions;

    .line 82
    iget-object v2, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/onboarding/intent/WhereSelectedEvent;

    invoke-direct {v3, v1}, Lcom/squareup/onboarding/intent/WhereSelectedEvent;-><init>(Lcom/squareup/onboarding/intent/WhereOptions;)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 86
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->how:Lcom/squareup/onboarding/intent/HowOptions;

    if-nez v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/onboarding/intent/HowSkippedEvent;

    invoke-direct {v1}, Lcom/squareup/onboarding/intent/HowSkippedEvent;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_2

    .line 89
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/onboarding/intent/HowSelectedEvent;

    invoke-direct {v2, v0}, Lcom/squareup/onboarding/intent/HowSelectedEvent;-><init>(Lcom/squareup/onboarding/intent/HowOptions;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_2
    return-void
.end method


# virtual methods
.method onEmptyHowContinue()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->EMPTY_CONTINUE_FROM_HOW:Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onEmptyWhereContinue()V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->EMPTY_CONTINUE_FROM_WHERE:Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onHowSelected(Lcom/squareup/onboarding/intent/HowOptions;)V
    .locals 2

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->how:Lcom/squareup/onboarding/intent/HowOptions;

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->send()V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->afterIntentQuestions()Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/onboarding/intent/-$$Lambda$U4DmPPmMam8ZHKU81YRTY3jVSM8;

    invoke-direct {v1, v0}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$U4DmPPmMam8ZHKU81YRTY3jVSM8;-><init>(Lflow/Flow;)V

    .line 60
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method onHowSkipped()V
    .locals 1

    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onHowSelected(Lcom/squareup/onboarding/intent/HowOptions;)V

    return-void
.end method

.method onHowUp()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method onWhereSelected(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/onboarding/intent/WhereOptions;",
            ">;)V"
        }
    .end annotation

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->selectedWhereOptions:Ljava/util/Set;

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;->INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentHowScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onWhereSkipped()V
    .locals 1

    .line 49
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onWhereSelected(Ljava/util/Set;)V

    return-void
.end method
