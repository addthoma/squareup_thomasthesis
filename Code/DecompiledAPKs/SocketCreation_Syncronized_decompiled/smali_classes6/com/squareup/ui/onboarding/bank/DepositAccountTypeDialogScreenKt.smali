.class public final Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialogScreenKt;
.super Ljava/lang/Object;
.source "DepositAccountTypeDialogScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a$\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0006*\"\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "DepositAccountTypeDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialogScreen;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final DepositAccountTypeDialogScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method
