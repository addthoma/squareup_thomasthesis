.class public final Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "AdditionalInfoScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final receivedResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final serverCallPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->serverCallPresenterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->receivedResponseProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;)",
            "Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;Ljava/lang/Object;Lcom/squareup/onboarding/ShareableReceivedResponse;)Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Ljava/text/DateFormat;",
            "Ljava/lang/Object;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)",
            "Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;"
        }
    .end annotation

    .line 57
    new-instance v6, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    move-object v4, p3

    check-cast v4, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;Lcom/squareup/onboarding/ShareableReceivedResponse;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->serverCallPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->receivedResponseProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;Ljava/lang/Object;Lcom/squareup/onboarding/ShareableReceivedResponse;)Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
