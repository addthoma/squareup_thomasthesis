.class public final Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "DepositBankLinkingCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator_Factory_Factory;->get()Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
