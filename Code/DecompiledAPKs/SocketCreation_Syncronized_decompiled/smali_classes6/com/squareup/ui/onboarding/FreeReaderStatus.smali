.class public final Lcom/squareup/ui/onboarding/FreeReaderStatus;
.super Ljava/lang/Object;
.source "FreeReaderStatus.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/onboarding/OnboardingActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000eH\u0016J\u0006\u0010\u0012\u001a\u00020\u000eR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/FreeReaderStatus;",
        "Lmortar/Scoped;",
        "rewardsBalanceService",
        "Lcom/squareup/server/activation/RewardsBalanceService;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/server/activation/RewardsBalanceService;Lcom/squareup/settings/server/Features;)V",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "<set-?>",
        "",
        "isAvailable",
        "()Z",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "refresh",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final features:Lcom/squareup/settings/server/Features;

.field private isAvailable:Z

.field private final rewardsBalanceService:Lcom/squareup/server/activation/RewardsBalanceService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/activation/RewardsBalanceService;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "rewardsBalanceService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->rewardsBalanceService:Lcom/squareup/server/activation/RewardsBalanceService;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->features:Lcom/squareup/settings/server/Features;

    .line 25
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$isAvailable$p(Lcom/squareup/ui/onboarding/FreeReaderStatus;)Z
    .locals 0

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/FreeReaderStatus;->isAvailable()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setAvailable$p(Lcom/squareup/ui/onboarding/FreeReaderStatus;Z)V
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->isAvailable:Z

    return-void
.end method


# virtual methods
.method public final isAvailable()Z
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_QUOTA:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->isAvailable:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public final refresh()V
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_QUOTA:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/FreeReaderStatus;->rewardsBalanceService:Lcom/squareup/server/activation/RewardsBalanceService;

    new-instance v2, Lcom/squareup/protos/client/rewards/GetMerchantBalanceRequest;

    invoke-direct {v2}, Lcom/squareup/protos/client/rewards/GetMerchantBalanceRequest;-><init>()V

    invoke-interface {v1, v2}, Lcom/squareup/server/activation/RewardsBalanceService;->getBalance(Lcom/squareup/protos/client/rewards/GetMerchantBalanceRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 48
    sget-object v2, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;->INSTANCE:Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    .line 55
    sget-object v2, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$2;->INSTANCE:Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$2;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    .line 56
    new-instance v2, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/FreeReaderStatus$refresh$3;-><init>(Lcom/squareup/ui/onboarding/FreeReaderStatus;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "rewardsBalanceService.ge\u2026ailable = hasFreeReader }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
