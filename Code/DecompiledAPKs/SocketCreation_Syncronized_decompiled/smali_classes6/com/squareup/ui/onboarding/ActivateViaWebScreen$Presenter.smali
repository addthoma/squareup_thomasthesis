.class public Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ActivateViaWebScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/ActivateViaWebScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/ActivateViaWebView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field final urlHelper:Lcom/squareup/onboarding/ActivationUrlHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/ActivationUrlHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->urlHelper:Lcom/squareup/onboarding/ActivationUrlHelper;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method onActivateViaWebContinued()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_CONTINUE_TO_WEB:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->urlHelper:Lcom/squareup/onboarding/ActivationUrlHelper;

    invoke-virtual {v0}, Lcom/squareup/onboarding/ActivationUrlHelper;->loadAndOpen()V

    return-void
.end method

.method onActivateViaWebLater()V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_WEB:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivateViaWebLater()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 56
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/SupportUrlSettings;->getPrivacyPolicyUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Urls;->extractDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ActivateViaWebView;

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/onboarding/flow/R$string;->accept_credit_subtitle_non_payment:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "domain"

    .line 64
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 63
    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->configureRegisterWorld(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 67
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->accept_credit_subtitle_no_free_reader:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->configurePayment(Ljava/lang/CharSequence;)V

    .line 69
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->urlHelper:Lcom/squareup/onboarding/ActivationUrlHelper;

    invoke-virtual {p1}, Lcom/squareup/onboarding/ActivationUrlHelper;->loadAndOpen()V

    return-void
.end method
