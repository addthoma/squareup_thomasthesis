.class public final Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "IntentWhereScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;->INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;

    .line 21
    sget-object v0, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;->INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INTENT_WHERE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/intent/IntentWhereScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-interface {p1}, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;->intentWhereCoordinator()Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 32
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->intent_where_view:I

    return v0
.end method
