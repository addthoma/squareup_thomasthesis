.class public abstract Lcom/squareup/ui/onboarding/SimpleOnboardingScreenSelectorModule;
.super Ljava/lang/Object;
.source "SimpleOnboardingScreenSelectorModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideOnboardingScreenSelector(Lcom/squareup/ui/onboarding/SimpleOnboardingScreenSelector;)Lcom/squareup/onboarding/OnboardingScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
