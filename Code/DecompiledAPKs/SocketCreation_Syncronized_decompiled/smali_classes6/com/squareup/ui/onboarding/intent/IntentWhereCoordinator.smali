.class public Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "IntentWhereCoordinator.java"


# instance fields
.field private checks:Lcom/squareup/widgets/CheckableGroup;

.field private final onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->res:Lcom/squareup/util/Res;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    return-void
.end method

.method private fromViewIds(Ljava/util/Set;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/onboarding/intent/WhereOptions;",
            ">;"
        }
    .end annotation

    .line 75
    const-class v0, Lcom/squareup/onboarding/intent/WhereOptions;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 76
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 77
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/squareup/onboarding/flow/R$id;->location_phone:I

    if-ne v2, v3, :cond_0

    .line 78
    sget-object v1, Lcom/squareup/onboarding/intent/WhereOptions;->PHONE:Lcom/squareup/onboarding/intent/WhereOptions;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/squareup/onboarding/flow/R$id;->location_tablet:I

    if-ne v2, v3, :cond_1

    .line 80
    sget-object v1, Lcom/squareup/onboarding/intent/WhereOptions;->TABLET:Lcom/squareup/onboarding/intent/WhereOptions;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/squareup/onboarding/flow/R$id;->location_computer:I

    if-ne v2, v3, :cond_2

    .line 82
    sget-object v1, Lcom/squareup/onboarding/intent/WhereOptions;->COMPUTER:Lcom/squareup/onboarding/intent/WhereOptions;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/squareup/onboarding/flow/R$id;->location_invoice:I

    if-ne v2, v3, :cond_3

    .line 84
    sget-object v1, Lcom/squareup/onboarding/intent/WhereOptions;->INVOICE:Lcom/squareup/onboarding/intent/WhereOptions;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/squareup/onboarding/flow/R$id;->location_website:I

    if-ne v2, v3, :cond_4

    .line 86
    sget-object v1, Lcom/squareup/onboarding/intent/WhereOptions;->WEBSITE:Lcom/squareup/onboarding/intent/WhereOptions;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown location id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    return-object v0
.end method

.method public static synthetic lambda$B6FQuOZGhhuh-LC_VnuG6cTychY(Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->onSkip()V

    return-void
.end method

.method static synthetic lambda$attach$0()V
    .locals 0

    return-void
.end method

.method public static synthetic lambda$qDT1vXQr2T7lebJiD-t76VMAWRc(Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->onContinue()V

    return-void
.end method

.method private onContinue()V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->checks:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedIds()Ljava/util/Set;

    move-result-object v0

    .line 62
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onEmptyWhereContinue()V

    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->fromViewIds(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onWhereSelected(Ljava/util/Set;)V

    return-void
.end method

.method private onSkip()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onWhereSkipped()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 36
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 37
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 38
    sget v1, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 39
    sget v2, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/widgets/MessageView;

    .line 40
    sget v3, Lcom/squareup/onboarding/flow/R$id;->location_radios:I

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/widgets/CheckableGroup;

    iput-object v3, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->checks:Lcom/squareup/widgets/CheckableGroup;

    .line 43
    sget-object v3, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentWhereCoordinator$3atM152op5tO1t9hAHdj8dzfssI;->INSTANCE:Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentWhereCoordinator$3atM152op5tO1t9hAHdj8dzfssI;

    invoke-static {p1, v3}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 45
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 49
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentWhereCoordinator$qDT1vXQr2T7lebJiD-t76VMAWRc;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentWhereCoordinator$qDT1vXQr2T7lebJiD-t76VMAWRc;-><init>(Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;)V

    .line 50
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/widgets/R$string;->skip:I

    .line 51
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentWhereCoordinator$B6FQuOZGhhuh-LC_VnuG6cTychY;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentWhereCoordinator$B6FQuOZGhhuh-LC_VnuG6cTychY;-><init>(Lcom/squareup/ui/onboarding/intent/IntentWhereCoordinator;)V

    .line 52
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 56
    sget p1, Lcom/squareup/onboarding/flow/R$string;->intent_where_title:I

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 57
    sget p1, Lcom/squareup/onboarding/flow/R$string;->intent_where_subtitle:I

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method
