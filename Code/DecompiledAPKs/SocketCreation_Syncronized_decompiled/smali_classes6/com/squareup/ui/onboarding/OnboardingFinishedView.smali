.class public Lcom/squareup/ui/onboarding/OnboardingFinishedView;
.super Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;
.source "OnboardingFinishedView.java"


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const-class p2, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Component;->inject(Lcom/squareup/ui/onboarding/OnboardingFinishedView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->presenter:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 28
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 20
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onFinishInflate()V

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingFinishedView;->presenter:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
