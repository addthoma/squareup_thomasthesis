.class Lcom/squareup/ui/onboarding/BusinessInfoView$4;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "BusinessInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/BusinessInfoView;->bindAdvanceOnGo(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/BusinessInfoView;

.field final synthetic val$view:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/BusinessInfoView;Landroid/widget/EditText;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView$4;->this$0:Lcom/squareup/ui/onboarding/BusinessInfoView;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/BusinessInfoView$4;->val$view:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x2

    if-ne p2, p1, :cond_0

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView$4;->val$view:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView$4;->this$0:Lcom/squareup/ui/onboarding/BusinessInfoView;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->onAdvanced()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
