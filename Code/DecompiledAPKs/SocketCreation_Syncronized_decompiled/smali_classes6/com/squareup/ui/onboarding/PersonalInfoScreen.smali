.class public Lcom/squareup/ui/onboarding/PersonalInfoScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "PersonalInfoScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;,
        Lcom/squareup/ui/onboarding/PersonalInfoScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/PersonalInfoScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final isRetry:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoScreen$wLxVkK-6vzmxwQazDVS8nlf5dEo;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoScreen$wLxVkK-6vzmxwQazDVS8nlf5dEo;

    .line 37
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    .line 29
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->isRetry:Z

    return-void
.end method

.method public static firstTry()Lcom/squareup/ui/onboarding/PersonalInfoScreen;
    .locals 2

    .line 19
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen;-><init>(Z)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/onboarding/PersonalInfoScreen;
    .locals 2

    .line 37
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen;-><init>(Z)V

    return-object v0
.end method

.method public static retry()Lcom/squareup/ui/onboarding/PersonalInfoScreen;
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 40
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 41
    iget-boolean p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->isRetry:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_PERSONAL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{isRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen;->isRetry:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 63
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->personal_info_view:I

    return v0
.end method
