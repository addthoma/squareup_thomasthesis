.class public final Lcom/squareup/ui/onboarding/bank/OnboardingDepositOptionsWorkflowRunner;
.super Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;
.source "DepositOptionsWorkflowRunners.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/OnboardingDepositOptionsWorkflowRunner;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;",
        "container",
        "Lcom/squareup/ui/onboarding/OnboardingContainer;",
        "viewFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;",
        "depositOptionsStarterFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;",
        "resultHandler",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;",
        "(Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;)V",
        "pushStack",
        "",
        "stack",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/onboarding/OnboardingContainer;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositOptionsStarterFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultHandler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "container.nextHistory()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p2, p3, v0, p4}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;Lio/reactivex/Observable;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;)V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/OnboardingDepositOptionsWorkflowRunner;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    return-void
.end method


# virtual methods
.method public pushStack(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;)V"
        }
    .end annotation

    const-string v0, "stack"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/OnboardingDepositOptionsWorkflowRunner;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/OnboardingContainer;->pushStack(Ljava/util/List;)V

    return-void
.end method
