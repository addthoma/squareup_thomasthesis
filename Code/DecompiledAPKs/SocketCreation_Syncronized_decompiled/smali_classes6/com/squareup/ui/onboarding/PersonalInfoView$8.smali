.class Lcom/squareup/ui/onboarding/PersonalInfoView$8;
.super Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;
.source "PersonalInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/PersonalInfoView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$8;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnFirstTextChange(Landroid/text/Editable;)V
    .locals 0

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$8;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->onPostalChanged()V

    return-void
.end method
