.class Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;
.super Landroid/view/animation/Animation;
.source "ConfirmIdentityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/ConfirmIdentityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NextQuestionAnimation"
.end annotation


# instance fields
.field private final scrollDistance:I

.field private final scrollFrom:I

.field final synthetic this$0:Lcom/squareup/ui/onboarding/ConfirmIdentityView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/onboarding/ConfirmIdentityView;I)V
    .locals 1

    .line 192
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->this$0:Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 193
    invoke-static {p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->access$100(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->scrollFrom:I

    .line 194
    iget v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->scrollFrom:I

    sub-int/2addr p2, v0

    iput p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->scrollDistance:I

    .line 195
    invoke-static {p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->access$200(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)Landroid/view/animation/Interpolator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 p1, 0x1f4

    .line 196
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->setDuration(J)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/onboarding/ConfirmIdentityView;ILcom/squareup/ui/onboarding/ConfirmIdentityView$1;)V
    .locals 0

    .line 188
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;-><init>(Lcom/squareup/ui/onboarding/ConfirmIdentityView;I)V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 1

    .line 200
    iget p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->scrollDistance:I

    int-to-float p2, p2

    mul-float p1, p1, p2

    float-to-int p1, p1

    .line 201
    iget-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->this$0:Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    invoke-static {p2}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->access$100(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)Landroid/widget/ScrollView;

    move-result-object p2

    iget v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;->scrollFrom:I

    add-int/2addr v0, p1

    const/4 p1, 0x0

    invoke-virtual {p2, p1, v0}, Landroid/widget/ScrollView;->scrollTo(II)V

    return-void
.end method
