.class public Lcom/squareup/ui/onboarding/AdditionalInfoView;
.super Landroid/widget/LinearLayout;
.source "AdditionalInfoView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private birthDateHeader:Landroid/view/View;

.field private birthDateView:Landroid/widget/TextView;

.field private final dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ssn:Lcom/squareup/widgets/SelectableEditText;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class p2, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Component;->inject(Lcom/squareup/ui/onboarding/AdditionalInfoView;)V

    .line 49
    new-instance p2, Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/DateOfBirthPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    .line 50
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    .line 51
    new-instance p2, Lcom/squareup/caller/FailurePopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/AdditionalInfoView;)Landroid/widget/TextView;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic lambda$onFinishInflate$0(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result p1

    if-nez p1, :cond_0

    .line 76
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 77
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 172
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 173
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getSsn()Ljava/lang/String;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 58
    sget v1, Lcom/squareup/onboarding/flow/R$string;->personal_info_heading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 60
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 61
    sget v0, Lcom/squareup/onboarding/flow/R$id;->full_ssn:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-static {}, Lcom/squareup/text/TinFormatter;->createSsnFormatter()Lcom/squareup/text/TinFormatter;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v1, v2, v3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 64
    sget v0, Lcom/squareup/onboarding/flow/R$id;->birth_date:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    .line 65
    sget v0, Lcom/squareup/onboarding/flow/R$id;->birth_date_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateHeader:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/onboarding/AdditionalInfoView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/AdditionalInfoView$1;-><init>(Lcom/squareup/ui/onboarding/AdditionalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/ui/onboarding/-$$Lambda$AdditionalInfoView$ScLDRiOPEBzAs0PsqNS9JfovMH8;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$AdditionalInfoView$ScLDRiOPEBzAs0PsqNS9JfovMH8;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->presenter:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 111
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_1

    .line 112
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    iget-object v1, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 116
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 119
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 103
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v1}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    new-instance v1, Lcom/squareup/mortar/BundleSavedState;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v2}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    return-object v0
.end method

.method public requestBirthDateViewFocus()V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    return-void
.end method

.method public requestSsnFocus()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    return-void
.end method

.method public setBirthDateText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setBirthDateViewGone()V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->subtitle:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->additional_info_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setImeOptions(I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/onboarding/AdditionalInfoView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/AdditionalInfoView$3;-><init>(Lcom/squareup/ui/onboarding/AdditionalInfoView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public setBirthDateViewVisible()V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->birthDateHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->subtitle:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->additional_info_subtitle_vague:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setImeOptions(I)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView;->ssn:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/onboarding/AdditionalInfoView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/AdditionalInfoView$2;-><init>(Lcom/squareup/ui/onboarding/AdditionalInfoView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method
