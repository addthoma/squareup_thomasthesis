.class public abstract Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;
.super Lcom/squareup/container/SimpleWorkflowRunner;
.source "DepositOptionsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ParentComponent;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008&\u0018\u0000 \u00162\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0016\u0017\u0018\u0019B+\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0016\u0010\u0012\u001a\u00020\u000f2\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H&R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;",
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "viewFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;",
        "depositOptionsStarterFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "resultHandler",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;",
        "(Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;Lio/reactivex/Observable;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;)V",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "pushStack",
        "stack",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "Companion",
        "ParentComponent",
        "ResultHandler",
        "Scope",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final resultHandler:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->Companion:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;

    .line 91
    const-class v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DepositOptionsWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;Lio/reactivex/Observable;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;",
            ")V"
        }
    .end annotation

    const-string v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositOptionsStarterFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultHandler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->NAME:Ljava/lang/String;

    .line 25
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    .line 26
    invoke-interface {p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;->create()Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/squareup/container/PosWorkflowStarter;

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    move-object v3, p3

    .line 22
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->resultHandler:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;)V
    .locals 0

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getResultHandler$p(Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;)Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->resultHandler:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;

    return-object p0
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-super {p0, p1}, Lcom/squareup/container/SimpleWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;Lmortar/MortarScope;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n        .swit\u2026   }\n        .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public abstract pushStack(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;)V"
        }
    .end annotation
.end method
