.class public final Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;
.super Ljava/lang/Object;
.source "OnboardingContainer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/OnboardingContainer;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedOutStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final modelHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final screenNavigationLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final selectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingScreenSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final verticalSelectionRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->navigationListenerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->screenNavigationLoggerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p4, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p5, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->loggedOutStarterProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p6, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->busProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p7, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->selectorProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p8, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->verticalSelectionRunnerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p9, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p10, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p11, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->resProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p12, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->modelHolderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;"
        }
    .end annotation

    .line 91
    new-instance v13, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/navigation/NavigationListener;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/badbus/BadBus;Lcom/squareup/onboarding/OnboardingScreenSelector;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/ModelHolder;)Lcom/squareup/ui/onboarding/OnboardingContainer;
    .locals 14

    .line 100
    new-instance v13, Lcom/squareup/ui/onboarding/OnboardingContainer;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/onboarding/OnboardingContainer;-><init>(Lcom/squareup/navigation/NavigationListener;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/badbus/BadBus;Lcom/squareup/onboarding/OnboardingScreenSelector;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/ModelHolder;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/OnboardingContainer;
    .locals 13

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->navigationListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/navigation/NavigationListener;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->screenNavigationLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/navigation/ScreenNavigationLogger;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/SoftInputPresenter;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->loggedOutStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/loggedout/LoggedOutStarter;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->selectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/onboarding/OnboardingScreenSelector;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->verticalSelectionRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/AppNameFormatter;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->modelHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/onboarding/ModelHolder;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->newInstance(Lcom/squareup/navigation/NavigationListener;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/badbus/BadBus;Lcom/squareup/onboarding/OnboardingScreenSelector;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/ModelHolder;)Lcom/squareup/ui/onboarding/OnboardingContainer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Factory;->get()Lcom/squareup/ui/onboarding/OnboardingContainer;

    move-result-object v0

    return-object v0
.end method
