.class public final Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositSpeedTabletCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositSpeedTabletCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositSpeedTabletCoordinator.kt\ncom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator\n*L\n1#1,85:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000cH\u0016J\u0010\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000cH\u0002J(\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000c2\u0016\u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J&\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001dH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "nextBusinessDay",
        "Landroid/view/View;",
        "sameDay",
        "sameDayDepositDescription",
        "Lcom/squareup/marketfont/MarketTextView;",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "attach",
        "",
        "view",
        "bindViews",
        "onScreen",
        "screen",
        "setUpActionBar",
        "canGoBack",
        "",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private nextBusinessDay:Landroid/view/View;

.field private sameDay:Landroid/view/View;

.field private sameDayDepositDescription:Lcom/squareup/marketfont/MarketTextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 77
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 78
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 79
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 80
    sget v0, Lcom/squareup/onboarding/flow/R$id;->deposit_speed_next_business_day:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->nextBusinessDay:Landroid/view/View;

    .line 81
    sget v0, Lcom/squareup/onboarding/flow/R$id;->deposit_speed_same_day:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->sameDay:Landroid/view/View;

    .line 82
    sget v0, Lcom/squareup/onboarding/flow/R$id;->same_day_deposit_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->sameDayDepositDescription:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private final onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 41
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->getCanGoBack()Z

    move-result v0

    iget-object v1, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->setUpActionBar(Landroid/view/View;ZLcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->sameDayDepositDescription:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_0

    const-string v0, "sameDayDepositDescription"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->getSameDayDepositDescription()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->nextBusinessDay:Landroid/view/View;

    if-nez p1, :cond_1

    const-string v0, "nextBusinessDay"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 44
    :cond_1
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 43
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->sameDay:Landroid/view/View;

    if-nez p1, :cond_2

    const-string v0, "sameDay"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 47
    :cond_2
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$2;

    invoke-direct {v0, p2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 46
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setUpActionBar(Landroid/view/View;ZLcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Z",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_0

    const-string v2, "title"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/onboarding/flow/R$string;->deposit_speed_title:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_1

    const-string v2, "subtitle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/onboarding/flow/R$string;->deposit_speed_subtitle:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 59
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_2

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 58
    :cond_2
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    if-eqz p2, :cond_3

    .line 61
    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$1;

    invoke-direct {v3, p2, p1, p3, v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$1;-><init>(ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v3}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 62
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/onboarding/flow/R$string;->back:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    const/4 v3, 0x1

    .line 63
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 64
    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$2;

    invoke-direct {v3, p2, p1, p3, v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$2;-><init>(ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 66
    :cond_3
    sget-object v3, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$1$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$1$3;

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v3}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 67
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 69
    :goto_0
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 70
    sget v3, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_later:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 71
    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;

    invoke-direct {v3, p2, p1, p3, v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;-><init>(ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 72
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 59
    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$attach$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->bindViews(Landroid/view/View;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$attach$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { onScreen(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
