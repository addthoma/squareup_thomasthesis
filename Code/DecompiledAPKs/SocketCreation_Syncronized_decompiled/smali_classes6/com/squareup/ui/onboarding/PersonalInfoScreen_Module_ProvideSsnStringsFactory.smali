.class public final Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;
.super Ljava/lang/Object;
.source "PersonalInfoScreen_Module_ProvideSsnStringsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/SSNStrings;",
        ">;"
    }
.end annotation


# instance fields
.field private final countryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->countryProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSsnStrings(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 0

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen$Module;->provideSsnStrings(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/onboarding/SSNStrings;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->countryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/CountryCode;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->provideSsnStrings(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->get()Lcom/squareup/ui/onboarding/SSNStrings;

    move-result-object v0

    return-object v0
.end method
