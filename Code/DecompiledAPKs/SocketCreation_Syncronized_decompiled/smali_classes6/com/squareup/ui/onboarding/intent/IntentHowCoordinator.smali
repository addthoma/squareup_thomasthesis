.class public Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "IntentHowCoordinator.java"


# instance fields
.field private checks:Lcom/squareup/widgets/CheckableGroup;

.field private final onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->res:Lcom/squareup/util/Res;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    return-void
.end method

.method private fromViewId(I)Lcom/squareup/onboarding/intent/HowOptions;
    .locals 3

    .line 65
    sget v0, Lcom/squareup/onboarding/flow/R$id;->intent_how_custom:I

    if-ne p1, v0, :cond_0

    .line 66
    sget-object p1, Lcom/squareup/onboarding/intent/HowOptions;->CUSTOM:Lcom/squareup/onboarding/intent/HowOptions;

    return-object p1

    .line 67
    :cond_0
    sget v0, Lcom/squareup/onboarding/flow/R$id;->intent_how_preset:I

    if-ne p1, v0, :cond_1

    .line 68
    sget-object p1, Lcom/squareup/onboarding/intent/HowOptions;->ITEMIZED:Lcom/squareup/onboarding/intent/HowOptions;

    return-object p1

    .line 70
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown location id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic lambda$BeIm48olI5oyZ18FeUU5fTKth8Y(Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onSkip()V

    return-void
.end method

.method public static synthetic lambda$YCeYncNZKMGInyRb8_1ONBsJCEc(Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onContinue()V

    return-void
.end method

.method private onContinue()V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->checks:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onEmptyHowContinue()V

    return-void

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->fromViewId(I)Lcom/squareup/onboarding/intent/HowOptions;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onHowSelected(Lcom/squareup/onboarding/intent/HowOptions;)V

    return-void
.end method

.method private onSkip()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onHowSkipped()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 30
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 31
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 32
    sget v1, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 33
    sget v2, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 34
    sget v3, Lcom/squareup/onboarding/flow/R$id;->itemization_radios:I

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->checks:Lcom/squareup/widgets/CheckableGroup;

    .line 36
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->onboardingIntentRunner:Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/onboarding/intent/-$$Lambda$AxUAakxon1gF0oHhK-Bnj6-11NY;

    invoke-direct {v4, v3}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$AxUAakxon1gF0oHhK-Bnj6-11NY;-><init>(Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;)V

    .line 39
    invoke-virtual {v0, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 40
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentHowCoordinator$YCeYncNZKMGInyRb8_1ONBsJCEc;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentHowCoordinator$YCeYncNZKMGInyRb8_1ONBsJCEc;-><init>(Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;)V

    .line 41
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/widgets/R$string;->skip:I

    .line 42
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentHowCoordinator$BeIm48olI5oyZ18FeUU5fTKth8Y;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$IntentHowCoordinator$BeIm48olI5oyZ18FeUU5fTKth8Y;-><init>(Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;)V

    .line 43
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 45
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 47
    sget p1, Lcom/squareup/onboarding/flow/R$string;->intent_how_title:I

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    const/16 p1, 0x8

    .line 48
    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
