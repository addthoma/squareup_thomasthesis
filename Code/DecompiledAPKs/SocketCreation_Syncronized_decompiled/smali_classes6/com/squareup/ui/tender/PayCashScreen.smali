.class public final Lcom/squareup/ui/tender/PayCashScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "PayCashScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/PayCashScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/PayCashScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/PayCashScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/PayCashScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/tender/PayCashScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/PayCashScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/PayCashScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCashScreen;

    .line 41
    sget-object v0, Lcom/squareup/ui/tender/PayCashScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCashScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/PayCashScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CASH:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->ALWAYS_SHOW_RESIZE:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 44
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->pay_cash_view:I

    return v0
.end method
