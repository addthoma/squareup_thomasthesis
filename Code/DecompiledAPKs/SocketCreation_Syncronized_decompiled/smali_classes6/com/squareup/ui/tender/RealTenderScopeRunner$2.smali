.class synthetic Lcom/squareup/ui/tender/RealTenderScopeRunner$2;
.super Ljava/lang/Object;
.source "RealTenderScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/RealTenderScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cancelsplit$CancelSplitTenderTransactionDialog$Event:[I

.field static final synthetic $SwitchMap$com$squareup$tenderpayment$TenderCompleter$CancelPaymentResult:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 177
    invoke-static {}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->values()[Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$cancelsplit$CancelSplitTenderTransactionDialog$Event:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$cancelsplit$CancelSplitTenderTransactionDialog$Event:[I

    sget-object v2, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->CONFIRM_CANCEL_SPLIT_TENDER_TRANSACTION:Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    invoke-virtual {v2}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$cancelsplit$CancelSplitTenderTransactionDialog$Event:[I

    sget-object v3, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->DISMISS_CANCEL_SPLIT_TENDER_TRANSACTION:Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;

    invoke-virtual {v3}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 200
    :catch_1
    invoke-static {}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->values()[Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CancelPaymentResult:[I

    :try_start_2
    sget-object v2, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CancelPaymentResult:[I

    sget-object v3, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_INVOICE_PAYMENT:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CancelPaymentResult:[I

    sget-object v2, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/ui/tender/RealTenderScopeRunner$2;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CancelPaymentResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    return-void
.end method
