.class Lcom/squareup/ui/tender/PayCashPresenter$StartEditingCashEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PayCashPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/PayCashPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartEditingCashEvent"
.end annotation


# instance fields
.field private final event_id:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/tender/PayCashPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/PayCashPresenter;Ljava/lang/String;)V
    .locals 1

    .line 225
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashPresenter$StartEditingCashEvent;->this$0:Lcom/squareup/ui/tender/PayCashPresenter;

    .line 226
    sget-object p1, Lcom/squareup/eventstream/v1/EventStream$Name;->SELECT:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v0, Lcom/squareup/analytics/RegisterSelectName;->PAYMENT_FLOW_METHODS_CASH_FIELD:Lcom/squareup/analytics/RegisterSelectName;

    iget-object v0, v0, Lcom/squareup/analytics/RegisterSelectName;->value:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 227
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCashPresenter$StartEditingCashEvent;->event_id:Ljava/lang/String;

    return-void
.end method
