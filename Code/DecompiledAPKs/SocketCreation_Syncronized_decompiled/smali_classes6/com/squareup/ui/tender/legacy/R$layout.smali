.class public final Lcom/squareup/ui/tender/legacy/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/legacy/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final card_on_file_offline_not_available:I = 0x7f0d00b8

.field public static final crm_choose_card_on_file_view:I = 0x7f0d0118

.field public static final customer_card_on_file_row:I = 0x7f0d01ab

.field public static final customer_no_cards_on_file_row:I = 0x7f0d01ad

.field public static final gift_card_select_screen_view:I = 0x7f0d027e

.field public static final pay_card_cnp_disabled_view:I = 0x7f0d0427

.field public static final pay_card_on_file_card_row:I = 0x7f0d0428

.field public static final pay_card_swipe_only_view:I = 0x7f0d0429

.field public static final pay_cash_view:I = 0x7f0d042a

.field public static final pay_third_party_card_view:I = 0x7f0d0432

.field public static final payment_choose_card_on_file_customer_view_crm_v2:I = 0x7f0d0434

.field public static final tender_order_ticket_name_view:I = 0x7f0d0518


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
