.class public final Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;
.super Ljava/lang/Object;
.source "PayCashPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/PayCashPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation


# instance fields
.field public final amountDue:Lcom/squareup/protos/common/Money;

.field public final enableTenderButton:Z

.field public final maxTender:Lcom/squareup/protos/common/Money;

.field final quickCashOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/common/Money;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    .line 54
    iput-boolean p2, p0, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->enableTenderButton:Z

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->maxTender:Lcom/squareup/protos/common/Money;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->quickCashOptions:Ljava/util/List;

    return-void
.end method
