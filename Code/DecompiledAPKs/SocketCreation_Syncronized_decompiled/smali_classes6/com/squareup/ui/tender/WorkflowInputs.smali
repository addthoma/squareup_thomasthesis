.class public final Lcom/squareup/ui/tender/WorkflowInputs;
.super Ljava/lang/Object;
.source "WorkflowInputs.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0004H\u0001\u00a8\u0006\u0005"
    }
    d2 = {
        "forJava",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "E",
        "handler",
        "Lio/reactivex/functions/Consumer;",
        "tender-payment-legacy_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final forJava(Lio/reactivex/functions/Consumer;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Consumer<",
            "TE;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "TE;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Wouldn\'t you rather be using Kotlin?"
    .end annotation

    const-string v0, "handler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    new-instance v0, Lcom/squareup/ui/tender/WorkflowInputs$forJava$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/tender/WorkflowInputs$forJava$1;-><init>(Lio/reactivex/functions/Consumer;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p0

    return-object p0
.end method
