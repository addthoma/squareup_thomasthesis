.class interface abstract Lcom/squareup/ui/tender/PayCashPresenter$View;
.super Ljava/lang/Object;
.source "PayCashPresenter.java"

# interfaces
.implements Lcom/squareup/mortar/HasContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/PayCashPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "View"
.end annotation


# virtual methods
.method public abstract getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
.end method

.method public abstract getTenderedAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract hideSoftKeyboard()V
.end method

.method public abstract requestInitialFocus()V
.end method

.method public abstract update(Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;)V
.end method
