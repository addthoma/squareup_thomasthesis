.class public interface abstract Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;
.super Ljava/lang/Object;
.source "ChooseCardOnFileScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/tender/ChooseCardOnFileRowView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/tender/ChooseCardOnFileView;)V
.end method
