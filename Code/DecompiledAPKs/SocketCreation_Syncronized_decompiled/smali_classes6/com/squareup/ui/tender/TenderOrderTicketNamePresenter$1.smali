.class synthetic Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;
.super Ljava/lang/Object;
.source "TenderOrderTicketNamePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 416
    invoke-static {}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->values()[Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->SUCCESS:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->FAILURE:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->SWIPE_STRAIGHT:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->CHIP_CARD_SWIPED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->LOADING:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->CHIP_CARD_REMOVED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method
