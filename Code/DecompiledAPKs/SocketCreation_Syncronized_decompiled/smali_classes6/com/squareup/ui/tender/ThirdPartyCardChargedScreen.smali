.class public final Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "ThirdPartyCardChargedScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Component;
.end annotation

.annotation runtime Lcom/squareup/ui/tender/RequiresPayment;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Component;,
        Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;->INSTANCE:Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;

    .line 34
    sget-object v0, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;->INSTANCE:Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;

    .line 35
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 38
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->pay_third_party_card_view:I

    return v0
.end method
