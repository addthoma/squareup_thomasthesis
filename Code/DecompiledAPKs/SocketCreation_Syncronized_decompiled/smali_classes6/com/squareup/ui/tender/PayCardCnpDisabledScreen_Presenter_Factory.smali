.class public final Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "PayCardCnpDisabledScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;"
        }
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;
    .locals 7

    .line 62
    new-instance v6, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;-><init>(Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderScopeRunner;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/NfcProcessor;

    iget-object v2, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v3, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v4, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->newInstance(Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen_Presenter_Factory;->get()Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
