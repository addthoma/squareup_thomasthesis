.class Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$NfcTenderDisplay;
.super Ljava/lang/Object;
.source "TenderOrderTicketNamePresenter.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NfcTenderDisplay"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;)V
    .locals 0

    .line 459
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$NfcTenderDisplay;-><init>()V

    return-void
.end method


# virtual methods
.method public contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method
