.class final Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountHelper;->finalizeLogIn(Ljava/lang/String;Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $existingMerchant:Z

.field final synthetic $sessionToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->$sessionToken:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->$existingMerchant:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)",
            "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_9

    .line 194
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getFinalCreateAccountLogInCheck$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;->isLoginSuccessful(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    new-instance p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getFinalCreateAccountLogInCheck$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;->getCreateAccountLoginCheckFailedTitle()Ljava/lang/String;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getFinalCreateAccountLogInCheck$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;->getCreateAccountLoginCheckFailedMessage()Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-direct {p1, v0, v1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;

    goto/16 :goto_1

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getAuthenticator$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/account/LegacyAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->$sessionToken:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/squareup/account/LegacyAuthenticator;->loggedIn(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 203
    iget-boolean v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->$existingMerchant:Z

    if-nez v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getAnalytics$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/login/CreateAccountHelper$AccountCreationEvent;

    invoke-direct {v1}, Lcom/squareup/ui/login/CreateAccountHelper$AccountCreationEvent;-><init>()V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getAnalytics$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Merchant Signed in"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 214
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    const-string v1, "statusResponse.features!!.can_onboard!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 215
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    .line 216
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getAdAnalytics$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/adanalytics/AdAnalytics;

    move-result-object v1

    if-nez p1, :cond_4

    .line 217
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    iget-object v2, p1, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    if-nez v2, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    const-string v3, "user!!.token!!"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    iget-object v3, p1, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-nez v3, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    iget-object v3, v3, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v3

    if-nez v3, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    const-string v4, "CountryCode.parseCountry\u2026.locale!!.country_code)!!"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    iget-object p1, p1, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    .line 216
    invoke-interface {v1, v2, v3, p1}, Lcom/squareup/adanalytics/AdAnalytics;->recordSignUp(Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/lang/String;)V

    .line 221
    new-instance p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$Success;

    if-eqz v0, :cond_8

    .line 222
    sget-object v0, Lcom/squareup/ui/login/CreateAccountHelper$NextStep;->ACTIVATE:Lcom/squareup/ui/login/CreateAccountHelper$NextStep;

    goto :goto_0

    :cond_8
    sget-object v0, Lcom/squareup/ui/login/CreateAccountHelper$NextStep;->HOME:Lcom/squareup/ui/login/CreateAccountHelper$NextStep;

    .line 221
    :goto_0
    invoke-direct {p1, v0}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$Success;-><init>(Lcom/squareup/ui/login/CreateAccountHelper$NextStep;)V

    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;

    goto :goto_1

    .line 227
    :cond_9
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;

    :goto_1
    return-object p1

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$finalizeLogIn$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;

    move-result-object p1

    return-object p1
.end method
