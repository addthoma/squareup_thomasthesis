.class public final Lcom/squareup/ui/login/CreateAccountHelper$AccountCreationEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "CreateAccountHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/CreateAccountHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AccountCreationEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/login/CreateAccountHelper$AccountCreationEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "()V",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 233
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRODUCT_SIGNUP:Lcom/squareup/analytics/RegisterActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    .line 232
    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    return-void
.end method
