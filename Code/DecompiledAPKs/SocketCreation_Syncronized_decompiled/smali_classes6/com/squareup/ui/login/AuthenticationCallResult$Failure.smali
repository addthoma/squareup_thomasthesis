.class public abstract Lcom/squareup/ui/login/AuthenticationCallResult$Failure;
.super Lcom/squareup/ui/login/AuthenticationCallResult;
.source "AuthenticationServiceEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticationCallResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Failure"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithAlert;,
        Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "",
        "()V",
        "FailureWithAlert",
        "FailureWithWarning",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithAlert;",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticationCallResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;-><init>()V

    return-void
.end method
