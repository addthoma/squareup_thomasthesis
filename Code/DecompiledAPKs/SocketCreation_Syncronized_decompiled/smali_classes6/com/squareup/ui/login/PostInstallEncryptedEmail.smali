.class public interface abstract Lcom/squareup/ui/login/PostInstallEncryptedEmail;
.super Ljava/lang/Object;
.source "PostInstallEncryptedEmail.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001\nJ\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\u0007\u001a\u00020\u0008H&J\n\u0010\t\u001a\u0004\u0018\u00010\u0008H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/login/PostInstallEncryptedEmail;",
        "",
        "clearEncryptedEmail",
        "",
        "decryptEmail",
        "Lio/reactivex/Maybe;",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "encryptedEmail",
        "",
        "getEncryptedEmail",
        "NoPostInstallLoginEncryptedEmail",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clearEncryptedEmail()V
.end method

.method public abstract decryptEmail(Ljava/lang/String;)Lio/reactivex/Maybe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEncryptedEmail()Ljava/lang/String;
.end method
