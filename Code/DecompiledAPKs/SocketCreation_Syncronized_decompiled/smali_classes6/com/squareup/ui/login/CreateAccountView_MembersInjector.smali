.class public final Lcom/squareup/ui/login/CreateAccountView_MembersInjector;
.super Ljava/lang/Object;
.source "CreateAccountView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/login/CreateAccountView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final supportedCountriesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/SupportedCountriesProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/SupportedCountriesProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->supportedCountriesProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/SupportedCountriesProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/login/CreateAccountView;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/login/CreateAccountView;Lcom/squareup/ui/login/CreateAccountPresenter;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/login/CreateAccountView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectSupportedCountriesProvider(Lcom/squareup/ui/login/CreateAccountView;Lcom/squareup/loggedout/SupportedCountriesProvider;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->supportedCountriesProvider:Lcom/squareup/loggedout/SupportedCountriesProvider;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/login/CreateAccountView;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->injectPresenter(Lcom/squareup/ui/login/CreateAccountView;Lcom/squareup/ui/login/CreateAccountPresenter;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->supportedCountriesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/loggedout/SupportedCountriesProvider;

    invoke-static {p1, v0}, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->injectSupportedCountriesProvider(Lcom/squareup/ui/login/CreateAccountView;Lcom/squareup/loggedout/SupportedCountriesProvider;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->injectRes(Lcom/squareup/ui/login/CreateAccountView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountView_MembersInjector;->injectMembers(Lcom/squareup/ui/login/CreateAccountView;)V

    return-void
.end method
