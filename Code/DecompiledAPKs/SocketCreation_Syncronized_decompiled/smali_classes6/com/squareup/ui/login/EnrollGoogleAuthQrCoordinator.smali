.class public final Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EnrollGoogleAuthQrCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnrollGoogleAuthQrCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnrollGoogleAuthQrCoordinator.kt\ncom/squareup/ui/login/EnrollGoogleAuthQrCoordinator\n*L\n1#1,133:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0018\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "cantScanBarcode",
        "Landroid/widget/TextView;",
        "googleAuthTwoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "learnMore",
        "Lcom/squareup/widgets/MessageView;",
        "qrCode",
        "Landroid/widget/ImageView;",
        "subtitle",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "encodeAsBitmap",
        "Landroid/graphics/Bitmap;",
        "context",
        "Landroid/content/Context;",
        "authenticatorUrl",
        "",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private cantScanBarcode:Landroid/widget/TextView;

.field private googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

.field private learnMore:Lcom/squareup/widgets/MessageView;

.field private qrCode:Landroid/widget/ImageView;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$encodeAsBitmap(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->encodeAsBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCantScanBarcode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Landroid/widget/TextView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->cantScanBarcode:Landroid/widget/TextView;

    if-nez p0, :cond_0

    const-string v0, "cantScanBarcode"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getGoogleAuthTwoFactorDetails$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-nez p0, :cond_0

    const-string v0, "googleAuthTwoFactorDetails"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getLearnMore$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->learnMore:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "learnMore"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getQrCode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->qrCode:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "qrCode"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSubtitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "subtitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getTitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "title"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setCantScanBarcode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Landroid/widget/TextView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->cantScanBarcode:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$setGoogleAuthTwoFactorDetails$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-void
.end method

.method public static final synthetic access$setLearnMore$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->learnMore:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setQrCode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->qrCode:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setSubtitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setTitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->title:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 125
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 126
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 127
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 128
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->qr_code:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->qrCode:Landroid/widget/ImageView;

    .line 129
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->cant_scan_barcode:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->cantScanBarcode:Landroid/widget/TextView;

    .line 130
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->learn_more:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->learnMore:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final encodeAsBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10

    const-string v0, "Error while trying to encode bitmap"

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 99
    sget v1, Lcom/squareup/common/authenticatorviews/R$dimen;->two_factor_google_auth_qr_code_size:I

    .line 98
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 103
    :try_start_0
    new-instance p1, Lcom/google/zxing/MultiFormatWriter;

    invoke-direct {p1}, Lcom/google/zxing/MultiFormatWriter;-><init>()V

    sget-object v1, Lcom/google/zxing/BarcodeFormat;->QR_CODE:Lcom/google/zxing/BarcodeFormat;

    invoke-virtual {p1, p2, v1, v5, v5}, Lcom/google/zxing/MultiFormatWriter;->encode(Ljava/lang/String;Lcom/google/zxing/BarcodeFormat;II)Lcom/google/zxing/common/BitMatrix;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/zxing/WriterException; {:try_start_0 .. :try_end_0} :catch_0

    const-string p2, "result"

    .line 110
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getWidth()I

    move-result v8

    .line 111
    invoke-virtual {p1}, Lcom/google/zxing/common/BitMatrix;->getHeight()I

    move-result v9

    mul-int p2, v8, v9

    .line 112
    new-array v3, p2, [I

    const/4 p2, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v9, :cond_2

    mul-int v1, v0, v8

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v8, :cond_1

    add-int v4, v1, v2

    .line 116
    invoke-virtual {p1, v2, v0}, Lcom/google/zxing/common/BitMatrix;->get(II)Z

    move-result v6

    if-eqz v6, :cond_0

    const/high16 v6, -0x1000000

    goto :goto_2

    :cond_0
    const/4 v6, 0x0

    :goto_2
    aput v6, v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_2
    sget-object p1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    .line 121
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    const-string p2, "Bitmap.createBitmap(w, h\u2026qrCodeSize, 0, 0, w, h) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :catch_0
    move-exception p1

    .line 107
    new-instance p2, Ljava/lang/IllegalStateException;

    check-cast p1, Ljava/lang/Throwable;

    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    :catch_1
    move-exception p1

    .line 105
    new-instance p2, Ljava/lang/IllegalStateException;

    check-cast p1, Ljava/lang/Throwable;

    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->bindViews(Landroid/view/View;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
