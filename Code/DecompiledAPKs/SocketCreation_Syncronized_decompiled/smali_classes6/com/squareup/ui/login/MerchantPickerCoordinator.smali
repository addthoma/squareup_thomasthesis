.class public final Lcom/squareup/ui/login/MerchantPickerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "MerchantPickerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMerchantPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MerchantPickerCoordinator.kt\ncom/squareup/ui/login/MerchantPickerCoordinator\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,86:1\n33#2,3:87\n*E\n*S KotlinDebug\n*F\n+ 1 MerchantPickerCoordinator.kt\ncom/squareup/ui/login/MerchantPickerCoordinator\n*L\n34#1,3:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001eB\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00060\u000bR\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R=\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r8F@FX\u0087\u008e\u0002\u00a2\u0006\u0018\n\u0004\u0008\u0016\u0010\u0017\u0012\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/login/MerchantPickerCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "adapter",
        "Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;",
        "<set-?>",
        "",
        "Lcom/squareup/protos/register/api/Unit;",
        "merchants",
        "merchants$annotations",
        "()V",
        "getMerchants",
        "()Ljava/util/List;",
        "setMerchants",
        "(Ljava/util/List;)V",
        "merchants$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "rowContainer",
        "Landroid/widget/ListView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "MerchantAdapter",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final adapter:Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;

.field private final merchants$delegate:Lkotlin/properties/ReadWriteProperty;

.field private rowContainer:Landroid/widget/ListView;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/login/MerchantPickerCoordinator;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "merchants"

    const-string v4, "getMerchants()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->screenData:Lio/reactivex/Observable;

    .line 30
    new-instance p1, Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;-><init>(Lcom/squareup/ui/login/MerchantPickerCoordinator;)V

    iput-object p1, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->adapter:Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;

    .line 34
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 87
    new-instance v0, Lcom/squareup/ui/login/MerchantPickerCoordinator$$special$$inlined$observable$1;

    invoke-direct {v0, p1, p1, p0}, Lcom/squareup/ui/login/MerchantPickerCoordinator$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/ui/login/MerchantPickerCoordinator;)V

    check-cast v0, Lkotlin/properties/ReadWriteProperty;

    .line 89
    iput-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->merchants$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/MerchantPickerCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAdapter$p(Lcom/squareup/ui/login/MerchantPickerCoordinator;)Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->adapter:Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;

    return-object p0
.end method

.method public static final synthetic access$getRowContainer$p(Lcom/squareup/ui/login/MerchantPickerCoordinator;)Landroid/widget/ListView;
    .locals 1

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->rowContainer:Landroid/widget/ListView;

    if-nez p0, :cond_0

    const-string v0, "rowContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/MerchantPickerCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setRowContainer$p(Lcom/squareup/ui/login/MerchantPickerCoordinator;Landroid/widget/ListView;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->rowContainer:Landroid/widget/ListView;

    return-void
.end method

.method public static synthetic merchants$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 38
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 39
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->select_a_business:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 40
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 41
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->select_a_business_subtext:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 43
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->row_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->rowContainer:Landroid/widget/ListView;

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->rowContainer:Landroid/widget/ListView;

    if-nez v0, :cond_0

    const-string v1, "rowContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->adapter:Lcom/squareup/ui/login/MerchantPickerCoordinator$MerchantAdapter;

    check-cast v1, Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/MerchantPickerCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final getMerchants()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->merchants$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/ui/login/MerchantPickerCoordinator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final setMerchants(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator;->merchants$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/ui/login/MerchantPickerCoordinator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
