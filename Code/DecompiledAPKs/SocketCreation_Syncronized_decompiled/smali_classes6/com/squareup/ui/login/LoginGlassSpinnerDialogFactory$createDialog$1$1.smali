.class final Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1$1;
.super Ljava/lang/Object;
.source "LoginGlassSpinnerDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/login/Operation;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "operation",
        "Lcom/squareup/ui/login/Operation;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1$1;->this$0:Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/login/Operation;)V
    .locals 1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/login/Operation;->getLoadingTextId()Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    if-ne p1, v0, :cond_1

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1$1;->this$0:Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;

    iget-object p1, p1, Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;->$textView:Landroid/widget/TextView;

    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1$1;->this$0:Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;

    iget-object v0, v0, Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1;->$textView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/ui/login/Operation;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/LoginGlassSpinnerDialogFactory$createDialog$1$1;->accept(Lcom/squareup/ui/login/Operation;)V

    return-void
.end method
