.class public Lcom/squareup/ui/login/CountryPickerPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "CountryPickerPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;,
        Lcom/squareup/ui/login/CountryPickerPopup$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
        "Lcom/squareup/CountryCode;",
        ">;"
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;


# direct methods
.method constructor <init>(Landroid/content/Context;[Lcom/squareup/CountryCode;)V
    .locals 2

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->single_choice_list_item:I

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;-><init>(Landroid/content/Context;I[Lcom/squareup/CountryCode;)V

    iput-object v0, p0, Lcom/squareup/ui/login/CountryPickerPopup;->adapter:Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 43
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 47
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/ui/login/CountryPickerPopup$Params;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/CountryPickerPopup;->createDialog(Lcom/squareup/ui/login/CountryPickerPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/ui/login/CountryPickerPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 32
    new-instance p2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/login/CountryPickerPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-static {p1}, Lcom/squareup/ui/login/CountryPickerPopup$Params;->access$100(Lcom/squareup/ui/login/CountryPickerPopup$Params;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/login/CountryPickerPopup;->adapter:Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;

    .line 35
    invoke-static {p1}, Lcom/squareup/ui/login/CountryPickerPopup$Params;->access$000(Lcom/squareup/ui/login/CountryPickerPopup$Params;)Lcom/squareup/CountryCode;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/login/CountryPickerPopup;->adapter:Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/login/CountryPickerPopup$Params;->access$000(Lcom/squareup/ui/login/CountryPickerPopup$Params;)Lcom/squareup/CountryCode;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;->getPosition(Ljava/lang/Object;)I

    move-result p1

    :goto_0
    new-instance v1, Lcom/squareup/ui/login/-$$Lambda$CountryPickerPopup$jYuMieghqnjvjyeShg82quXzE3w;

    invoke-direct {v1, p0, p3}, Lcom/squareup/ui/login/-$$Lambda$CountryPickerPopup$jYuMieghqnjvjyeShg82quXzE3w;-><init>(Lcom/squareup/ui/login/CountryPickerPopup;Lcom/squareup/mortar/PopupPresenter;)V

    .line 34
    invoke-virtual {p2, v0, p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v0, Lcom/squareup/ui/login/-$$Lambda$CountryPickerPopup$Gw4H4IFBRshmCWFFt-xPcBPsFus;

    invoke-direct {v0, p3}, Lcom/squareup/ui/login/-$$Lambda$CountryPickerPopup$Gw4H4IFBRshmCWFFt-xPcBPsFus;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 40
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/login/-$$Lambda$CountryPickerPopup$4LoCxvBwwPJOyRECmhxERsa_lXU;

    invoke-direct {p2, p3}, Lcom/squareup/ui/login/-$$Lambda$CountryPickerPopup$4LoCxvBwwPJOyRECmhxERsa_lXU;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 45
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$createDialog$0$CountryPickerPopup(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/login/CountryPickerPopup;->adapter:Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;

    invoke-virtual {v0, p3}, Lcom/squareup/ui/login/CountryPickerPopup$CountryPickerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    .line 38
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
