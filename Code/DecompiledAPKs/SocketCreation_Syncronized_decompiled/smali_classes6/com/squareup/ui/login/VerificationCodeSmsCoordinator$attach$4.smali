.class final Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$4;
.super Lkotlin/jvm/internal/Lambda;
.source "VerificationCodeSmsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Ljava/lang/Integer;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$4;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$4;->invoke(Ljava/lang/Integer;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Integer;)V
    .locals 2

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$4;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->access$getToastFactory$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Lcom/squareup/util/ToastFactory;

    move-result-object p1

    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_resend_code_toast:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    return-void
.end method
