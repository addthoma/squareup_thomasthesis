.class final Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EnrollGoogleAuthCodeCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnrollGoogleAuthCodeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnrollGoogleAuthCodeCoordinator.kt\ncom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,100:1\n1103#2,7:101\n1103#2,7:108\n*E\n*S KotlinDebug\n*F\n+ 1 EnrollGoogleAuthCodeCoordinator.kt\ncom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1\n*L\n61#1,7:101\n62#1,7:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 55
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v2

    .line 50
    new-instance v3, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 51
    sget v4, Lcom/squareup/common/strings/R$string;->next:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 52
    new-instance v4, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$1;

    invoke-direct {v4, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 53
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v5, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 54
    new-instance v3, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$2;

    invoke-direct {v3, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$3;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->access$getTitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object p1

    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_google_auth_code_title:I

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->access$getSubtitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object p1

    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_google_auth_code_subtitle:I

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->access$getAuthenticationKey$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Landroid/widget/TextView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    .line 101
    new-instance v2, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {v2, v1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->access$getCopyCode$p(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)Landroid/widget/TextView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    .line 108
    new-instance v2, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$$special$$inlined$onClickDebounced$2;

    invoke-direct {v2, v1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1$$special$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;->access$setTwoFactorDetails(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    return-void
.end method
