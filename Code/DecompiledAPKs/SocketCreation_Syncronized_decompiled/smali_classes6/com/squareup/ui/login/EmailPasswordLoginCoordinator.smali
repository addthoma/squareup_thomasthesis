.class public final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EmailPasswordLoginCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmailPasswordLoginCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmailPasswordLoginCoordinator.kt\ncom/squareup/ui/login/EmailPasswordLoginCoordinator\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,305:1\n45#2:306\n17#2,22:307\n1103#3,7:329\n*E\n*S KotlinDebug\n*F\n+ 1 EmailPasswordLoginCoordinator.kt\ncom/squareup/ui/login/EmailPasswordLoginCoordinator\n*L\n89#1:306\n89#1,22:307\n145#1,7:329\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008f\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u0001/\u0018\u00002\u00020\u0001:\u0001HB?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0018\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c0\u000b\u00a2\u0006\u0002\u0010\u000fJ\u0010\u00103\u001a\u0002042\u0006\u00105\u001a\u000206H\u0016J\u0010\u00107\u001a\u0002042\u0006\u00105\u001a\u000206H\u0002J\u0008\u00108\u001a\u000204H\u0002J\u0008\u00109\u001a\u000204H\u0002J\u0010\u00109\u001a\u0002042\u0006\u0010:\u001a\u00020\u001cH\u0002J\u0008\u0010;\u001a\u000204H\u0002J\u0010\u0010<\u001a\u0002042\u0006\u0010=\u001a\u00020\tH\u0002J\u0008\u0010>\u001a\u00020#H\u0002J\u0008\u0010?\u001a\u00020#H\u0002J\u0010\u0010@\u001a\u0002042\u0006\u0010A\u001a\u00020#H\u0002J\u0016\u0010B\u001a\u0002042\u000c\u0010C\u001a\u0008\u0012\u0004\u0012\u0002040DH\u0002J\u0006\u0010E\u001a\u000204J&\u0010F\u001a\u0002042\u0006\u00105\u001a\u0002062\u0006\u0010G\u001a\u00020\r2\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u000e02H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00158B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u00020#8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010$R\u0014\u0010%\u001a\u00020#8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010$R\u0014\u0010&\u001a\u00020#8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010$R\u000e\u0010\'\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010(\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00158B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008)\u0010\u0018\"\u0004\u0008*\u0010\u001aR\u000e\u0010+\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u00100R\u0014\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u000e02X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006I"
    }
    d2 = {
        "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "postInstallEncryptedEmail",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmail;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/login/PostInstallEncryptedEmail;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "deviceCodeLink",
        "Landroid/widget/TextView;",
        "value",
        "",
        "email",
        "getEmail",
        "()Ljava/lang/String;",
        "setEmail",
        "(Ljava/lang/String;)V",
        "emailField",
        "Lcom/squareup/ui/XableEditText;",
        "emailSuggestion",
        "Lcom/squareup/marketfont/MarketTextView;",
        "emailSuggestionHandler",
        "Lcom/squareup/widgets/EmailSuggestionHandler;",
        "forgotPasswordLink",
        "isEmailValid",
        "",
        "()Z",
        "isPasswordValid",
        "isSignInEnabled",
        "isTablet",
        "password",
        "getPassword",
        "setPassword",
        "passwordField",
        "title",
        "Lcom/squareup/widgets/MessageView;",
        "updateEnabledWatcher",
        "com/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1",
        "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "indicateEmailError",
        "indicateError",
        "textField",
        "indicatePasswordError",
        "maybeDecryptAndDisplayEmail",
        "postInstallLogin",
        "requestFocusOnEmail",
        "requestFocusOnPassword",
        "requestInitialFocus",
        "showKeyboard",
        "showDeviceCodeText",
        "action",
        "Lkotlin/Function0;",
        "updateEnabledState",
        "updateView",
        "screen",
        "Factory",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private deviceCodeLink:Landroid/widget/TextView;

.field private emailField:Lcom/squareup/ui/XableEditText;

.field private emailSuggestion:Lcom/squareup/marketfont/MarketTextView;

.field private emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

.field private forgotPasswordLink:Landroid/widget/TextView;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final isTablet:Z

.field private passwordField:Lcom/squareup/ui/XableEditText;

.field private final postInstallEncryptedEmail:Lcom/squareup/ui/login/PostInstallEncryptedEmail;

.field private final res:Lcom/squareup/util/Res;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private title:Lcom/squareup/widgets/MessageView;

.field private final updateEnabledWatcher:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;

.field private workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/login/PostInstallEncryptedEmail;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmail;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postInstallEncryptedEmail"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->postInstallEncryptedEmail:Lcom/squareup/ui/login/PostInstallEncryptedEmail;

    iput-object p5, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->screenData:Lio/reactivex/Observable;

    .line 79
    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isTablet:Z

    .line 82
    new-instance p1, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->updateEnabledWatcher:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;

    return-void
.end method

.method public static final synthetic access$getEmail$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Ljava/lang/String;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->getEmail()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getEmailField$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "emailField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getPassword$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Ljava/lang/String;
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->getPassword()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getWorkflow$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-nez p0, :cond_0

    const-string v0, "workflow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$indicateError(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicateError()V

    return-void
.end method

.method public static final synthetic access$isSignInEnabled$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Z
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isSignInEnabled()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$requestFocusOnPassword(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Z
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->requestFocusOnPassword()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setEmail$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Ljava/lang/String;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->setEmail(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setEmailField$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method public static final synthetic access$setPassword$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Ljava/lang/String;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->setPassword(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setWorkflow$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Landroid/view/View;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->updateView(Landroid/view/View;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 296
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 297
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 298
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->email_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    .line 299
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->password_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    .line 300
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->email_suggestion:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailSuggestion:Lcom/squareup/marketfont/MarketTextView;

    .line 301
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->forgot_password_link:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->forgotPasswordLink:Landroid/widget/TextView;

    .line 302
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->device_code_link:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->deviceCodeLink:Landroid/widget/TextView;

    return-void
.end method

.method private final getEmail()Ljava/lang/String;
    .locals 8

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "emailField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 306
    check-cast v0, Ljava/lang/CharSequence;

    .line 308
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    move v4, v1

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    if-gt v1, v4, :cond_6

    if-nez v5, :cond_1

    move v6, v1

    goto :goto_1

    :cond_1
    move v6, v4

    .line 313
    :goto_1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-gt v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-nez v5, :cond_4

    if-nez v6, :cond_3

    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    if-nez v6, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_6
    :goto_3
    add-int/2addr v4, v2

    .line 328
    invoke-interface {v0, v1, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_8

    goto :goto_5

    :cond_8
    const-string v0, ""

    :goto_5
    return-object v0
.end method

.method private final getPassword()Ljava/lang/String;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "passwordField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, ""

    :goto_1
    return-object v0
.end method

.method private final indicateEmailError()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "emailField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicateError(Lcom/squareup/ui/XableEditText;)V

    return-void
.end method

.method private final indicateError()V
    .locals 2

    .line 241
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isEmailValid()Z

    move-result v0

    .line 242
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isPasswordValid()Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicateEmailError()V

    .line 246
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicatePasswordError()V

    .line 247
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->requestFocusOnEmail()Z

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 249
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicateEmailError()V

    .line 250
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->requestFocusOnEmail()Z

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    .line 252
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicatePasswordError()V

    .line 253
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->requestFocusOnPassword()Z

    :cond_2
    :goto_0
    return-void
.end method

.method private final indicateError(Lcom/squareup/ui/XableEditText;)V
    .locals 3

    .line 281
    new-instance v0, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {v0}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    .line 282
    new-instance v1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 283
    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private final indicatePasswordError()V
    .locals 2

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "passwordField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->indicateError(Lcom/squareup/ui/XableEditText;)V

    return-void
.end method

.method private final isEmailValid()Z
    .locals 1

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->getEmail()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final isPasswordValid()Z
    .locals 1

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->getPassword()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final isSignInEnabled()Z
    .locals 1

    .line 100
    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isPasswordValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isEmailValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final maybeDecryptAndDisplayEmail(Lcom/squareup/ui/login/PostInstallEncryptedEmail;)V
    .locals 1

    .line 226
    invoke-interface {p1}, Lcom/squareup/ui/login/PostInstallEncryptedEmail;->getEncryptedEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    invoke-interface {p1}, Lcom/squareup/ui/login/PostInstallEncryptedEmail;->clearEncryptedEmail()V

    .line 230
    invoke-interface {p1, v0}, Lcom/squareup/ui/login/PostInstallEncryptedEmail;->decryptEmail(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 231
    invoke-virtual {p1}, Lio/reactivex/Maybe;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "postInstallLogin.decrypt\u2026)\n        .toObservable()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 232
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform()Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    .line 234
    new-instance v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    check-cast v0, Lrx/functions/Action1;

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :cond_0
    return-void
.end method

.method private final requestFocusOnEmail()Z
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "emailField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    move-result v0

    return v0
.end method

.method private final requestFocusOnPassword()Z
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "passwordField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    move-result v0

    return v0
.end method

.method private final requestInitialFocus(Z)V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    const-string v1, "emailField"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    if-eqz p1, :cond_2

    .line 272
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$requestInitialFocus$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$requestInitialFocus$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method private final setEmail(Ljava/lang/String;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "emailField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setPassword(Ljava/lang/String;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "passwordField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final showDeviceCodeText(Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->deviceCodeLink:Landroid/widget/TextView;

    const-string v1, "deviceCodeLink"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$showDeviceCodeText$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$showDeviceCodeText$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->deviceCodeLink:Landroid/widget/TextView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private final updateView(Landroid/view/View;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    .line 172
    iput-object p3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 176
    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->sign_in:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 178
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p3, p2}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 182
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getAsLandingScreen()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 184
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 185
    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->login_new_account:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 186
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p3, p2}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$2;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 189
    :cond_1
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 190
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$3;

    invoke-direct {v2, p0, p3, p2}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$3;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 191
    iget-boolean v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isTablet:Z

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getSupportsDeviceCode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 193
    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->use_a_device_code:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 194
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;

    invoke-direct {v2, p0, p3, p2}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 198
    :cond_2
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 200
    new-instance v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$2;

    invoke-direct {v0, p3}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 202
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getPassword()Lcom/squareup/account/SecretString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_3

    .line 203
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getPassword()Lcom/squareup/account/SecretString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->setPassword(Ljava/lang/String;)V

    .line 206
    :cond_3
    iget-boolean p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isTablet:Z

    if-nez p1, :cond_4

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getSupportsDeviceCode()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 207
    new-instance p1, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$3;

    invoke-direct {p1, p3}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->showDeviceCodeText(Lkotlin/jvm/functions/Function0;)V

    .line 212
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    if-nez p1, :cond_5

    const-string p3, "emailSuggestionHandler"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/EmailSuggestionHandler;->enableWorld(Z)V

    .line 215
    invoke-virtual {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->updateEnabledState()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->bindViews(Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->sign_in_to_square:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    const-string v1, "emailField"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;

    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailSuggestion:Lcom/squareup/marketfont/MarketTextView;

    if-nez v2, :cond_2

    const-string v3, "emailSuggestion"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v0, v2, p1}, Lcom/squareup/widgets/EmailSuggestionHandler;->wireEmailSuggestions(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)Lcom/squareup/widgets/EmailSuggestionHandler;

    move-result-object v0

    const-string v2, "wireEmailSuggestions(ema\u2026d, emailSuggestion, view)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->disableActionsExceptPaste()V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->updateEnabledWatcher:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    const-string v2, "passwordField"

    if-nez v0, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->updateEnabledWatcher:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateEnabledWatcher$1;

    check-cast v3, Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v3, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Landroid/view/View;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v3}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v3, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$2;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    check-cast v3, Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->passwordField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    check-cast v2, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->forgotPasswordLink:Landroid/widget/TextView;

    if-nez v0, :cond_8

    const-string v2, "forgotPasswordLink"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    .line 329
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->screenData:Lio/reactivex/Observable;

    sget-object v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$5;->INSTANCE:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$5;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    const-string v2, "screenData.map { it.data }\n        .firstElement()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    sget-object v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;->INSTANCE:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2}, Lcom/squareup/util/rx2/Rx2Kt;->mapNotNull(Lio/reactivex/Maybe;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 153
    new-instance v2, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$7;

    invoke-direct {v2, p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$7;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Maybe;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v2, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 160
    invoke-virtual {p1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_c

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_b

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_a

    goto :goto_0

    :cond_a
    const/4 p1, 0x0

    goto :goto_1

    :cond_b
    :goto_0
    const/4 p1, 0x1

    :goto_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->requestInitialFocus(Z)V

    .line 164
    :cond_c
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->postInstallEncryptedEmail:Lcom/squareup/ui/login/PostInstallEncryptedEmail;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->maybeDecryptAndDisplayEmail(Lcom/squareup/ui/login/PostInstallEncryptedEmail;)V

    return-void
.end method

.method public final updateEnabledState()V
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->isSignInEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
