.class public abstract Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;
.super Ljava/lang/Object;
.source "AuthenticatorScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "WarningText"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$AccountStatusErrorText;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$EmailPasswordLoginFailedText;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$DeviceCodeLoginFailedText;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$SessionExpired;,
        Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u0003\u0004\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0008\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;",
        "",
        "()V",
        "AccountStatusErrorText",
        "CustomWarningText",
        "DeviceCodeLoginFailedText",
        "EmailPasswordLoginFailedText",
        "NetworkErrorText",
        "ProvidedDeviceCodeAuthFailedText",
        "ServerErrorText",
        "SessionExpired",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$AccountStatusErrorText;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$EmailPasswordLoginFailedText;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$DeviceCodeLoginFailedText;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$SessionExpired;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;-><init>()V

    return-void
.end method
