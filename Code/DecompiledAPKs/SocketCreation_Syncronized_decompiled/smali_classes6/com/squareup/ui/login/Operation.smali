.class public final Lcom/squareup/ui/login/Operation;
.super Ljava/lang/Object;
.source "Operation.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/Operation$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB#\u0012\n\u0008\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0007H\u00c6\u0003J.\u0010\u0013\u001a\u00020\u00002\n\u0008\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0014J\u0013\u0010\u0015\u001a\u00020\u00072\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/login/Operation;",
        "",
        "loadingTextId",
        "",
        "type",
        "Lcom/squareup/ui/login/OperationType;",
        "showSpinner",
        "",
        "(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V",
        "getLoadingTextId",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getShowSpinner",
        "()Z",
        "getType",
        "()Lcom/squareup/ui/login/OperationType;",
        "component1",
        "component2",
        "component3",
        "copy",
        "(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)Lcom/squareup/ui/login/Operation;",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/Operation$Companion;

.field private static final NONE:Lcom/squareup/ui/login/Operation;


# instance fields
.field private final loadingTextId:Ljava/lang/Integer;

.field private final showSpinner:Z

.field private final type:Lcom/squareup/ui/login/OperationType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/squareup/ui/login/Operation$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/Operation$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    .line 24
    new-instance v0, Lcom/squareup/ui/login/Operation;

    sget-object v1, Lcom/squareup/ui/login/OperationType$None;->INSTANCE:Lcom/squareup/ui/login/OperationType$None;

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/login/OperationType;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/Operation;->NONE:Lcom/squareup/ui/login/Operation;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V
    .locals 1

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    iput-boolean p3, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 21
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    return-void
.end method

.method public static final synthetic access$getNONE$cp()Lcom/squareup/ui/login/Operation;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/login/Operation;->NONE:Lcom/squareup/ui/login/Operation;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/Operation;Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILjava/lang/Object;)Lcom/squareup/ui/login/Operation;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/Operation;->copy(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)Lcom/squareup/ui/login/Operation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/login/OperationType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    return v0
.end method

.method public final copy(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)Lcom/squareup/ui/login/Operation;
    .locals 1

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/Operation;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/Operation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/Operation;

    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    iget-object v1, p1, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    iget-boolean p1, p1, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLoadingTextId()Ljava/lang/Integer;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getShowSpinner()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    return v0
.end method

.method public final getType()Lcom/squareup/ui/login/OperationType;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Operation(loadingTextId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/Operation;->loadingTextId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/Operation;->type:Lcom/squareup/ui/login/OperationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSpinner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/Operation;->showSpinner:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
