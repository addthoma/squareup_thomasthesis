.class final Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AuthenticationServiceEndpoint.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/ui/login/AuthenticationCallResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        ">;",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
        "optionalResponse",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/util/Optional;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;"
        }
    .end annotation

    const-string v0, "optionalResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    instance-of v0, p1, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    .line 267
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    .line 268
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;

    iget-object v2, v2, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;->error_title:Ljava/lang/String;

    const-string v3, "optionalResponse.value.error_title"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;

    iget-object p1, p1, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;->error_message:Ljava/lang/String;

    const-string v3, "optionalResponse.value.error_message"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    .line 266
    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    return-object v0

    .line 272
    :cond_0
    instance-of p1, p1, Lcom/squareup/util/Optional$Empty;

    if-eqz p1, :cond_1

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "A response in an error case is expected."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1$1;->invoke(Lcom/squareup/util/Optional;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    move-result-object p1

    return-object p1
.end method
