.class public final Lcom/squareup/ui/login/CreateAccountPresenter;
.super Lmortar/ViewPresenter;
.source "CreateAccountPresenter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/login/CreateAccountScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/CreateAccountPresenter$EmailCountEvent;,
        Lcom/squareup/ui/login/CreateAccountPresenter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/login/CreateAccountView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateAccountPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateAccountPresenter.kt\ncom/squareup/ui/login/CreateAccountPresenter\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,417:1\n17#2,2:418\n*E\n*S KotlinDebug\n*F\n+ 1 CreateAccountPresenter.kt\ncom/squareup/ui/login/CreateAccountPresenter\n*L\n228#1,2:418\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008\u0007\u0018\u0000 W2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002WXBG\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u0002H\u0002J\u0008\u0010,\u001a\u00020-H\u0002J\u001a\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020\u001d2\u0008\u0008\u0001\u00101\u001a\u000202H\u0002J\"\u00103\u001a\u00020/2\u0006\u00100\u001a\u00020\u001d2\u0008\u0008\u0001\u00101\u001a\u0002022\u0006\u00104\u001a\u00020\u0017H\u0002J\u0008\u00105\u001a\u00020-H\u0007J\u0006\u00106\u001a\u00020-J\u0006\u00107\u001a\u00020-J\u000e\u00108\u001a\u00020-2\u0006\u00109\u001a\u00020:J\u0006\u0010;\u001a\u00020-J\u0010\u0010<\u001a\u00020-2\u0006\u0010=\u001a\u00020>H\u0014J\u0008\u0010?\u001a\u00020-H\u0014J\u0006\u0010@\u001a\u00020-J\u0012\u0010A\u001a\u00020-2\u0008\u0010B\u001a\u0004\u0018\u00010CH\u0014J\u0006\u0010D\u001a\u00020-J\u0006\u0010E\u001a\u00020-J\u0010\u0010F\u001a\u00020-2\u0006\u0010G\u001a\u00020CH\u0014J\u000e\u0010H\u001a\u00020-2\u0006\u0010I\u001a\u00020\u0017J\u0010\u0010J\u001a\u00020-2\u0006\u0010+\u001a\u00020\u0002H\u0003J\u001e\u0010K\u001a\u00020-2\u0014\u0010 \u001a\u0010\u0012\u0004\u0012\u00020M\u0012\u0006\u0012\u0004\u0018\u00010\u001d0LH\u0002J\u0012\u0010N\u001a\u00020-2\u0008\u0010#\u001a\u0004\u0018\u00010\u001dH\u0002J\u0008\u0010O\u001a\u00020-H\u0002J\u0012\u0010P\u001a\u00020-2\u0008\u0010Q\u001a\u0004\u0018\u00010\u001dH\u0002J\u0008\u0010R\u001a\u00020\u0017H\u0002J\u0008\u0010S\u001a\u00020\u0017H\u0002J\u0008\u0010T\u001a\u00020\u0017H\u0002J\u0008\u0010U\u001a\u00020\u0017H\u0002J\u0008\u0010V\u001a\u00020\u0017H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u00170\u00198\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u00198\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\u00020\u001d8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010&R\u000e\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006Y"
    }
    d2 = {
        "Lcom/squareup/ui/login/CreateAccountPresenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/ui/login/CreateAccountView;",
        "countryGuesser",
        "Lcom/squareup/location/CountryCodeGuesser;",
        "helper",
        "Lcom/squareup/ui/login/CreateAccountHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "accountManager",
        "Landroid/accounts/AccountManager;",
        "flow",
        "Lflow/Flow;",
        "authenticatorRunner",
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        "createAccountRunner",
        "Lcom/squareup/ui/login/CreateAccountScreen$Runner;",
        "(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "cancelOnBack",
        "",
        "confirmationPopupPresenter",
        "Lcom/squareup/mortar/PopupPresenter;",
        "Lcom/squareup/register/widgets/Confirmation;",
        "countryPickerPopupPresenter",
        "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
        "Lcom/squareup/CountryCode;",
        "createAccountDisposable",
        "Lio/reactivex/disposables/Disposable;",
        "guessedCountry",
        "possibleEmailAddress",
        "",
        "selectedCountry",
        "selectedCountryWithDefault",
        "getSelectedCountryWithDefault",
        "()Lcom/squareup/CountryCode;",
        "subs",
        "Lrx/subscriptions/CompositeSubscription;",
        "createRequest",
        "Lcom/squareup/server/account/CreateBody;",
        "view",
        "doCreate",
        "",
        "getLegalPhraseFor",
        "",
        "country",
        "phraseRes",
        "",
        "getLegalPhraseForHelperAndPromptText",
        "bold",
        "handleBack",
        "onConfirmEmailAddressChanged",
        "onCountryPickerClicked",
        "onEmailAddressChanged",
        "emailAddress",
        "Landroid/text/Editable;",
        "onEmailAddressSelected",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLegalLinksClicked",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onPasswordChanged",
        "onPrimaryButtonClicked",
        "onSave",
        "outState",
        "onTermsOfServiceAcceptedChanged",
        "accepted",
        "prefillEmailAddress",
        "setGuessedCountry",
        "Lkotlin/Pair;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "setSelectedCountry",
        "showConfirmation",
        "updateLegalFor",
        "maybeCountry",
        "validate",
        "validateEmail",
        "validateFieldsAreSet",
        "validatePassword",
        "validateTermsOfService",
        "Companion",
        "EmailCountEvent",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/CreateAccountPresenter$Companion;

.field private static final MIN_PASSWORD_LENGTH:I = 0x8

.field private static final SELECTED_COUNTRY:Ljava/lang/String; = "selectedCountry"


# instance fields
.field private final accountManager:Landroid/accounts/AccountManager;

.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

.field private cancelOnBack:Z

.field public final confirmationPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

.field public final countryPickerPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private createAccountDisposable:Lio/reactivex/disposables/Disposable;

.field private final createAccountRunner:Lcom/squareup/ui/login/CreateAccountScreen$Runner;

.field private final flow:Lflow/Flow;

.field private guessedCountry:Lcom/squareup/CountryCode;

.field public final helper:Lcom/squareup/ui/login/CreateAccountHelper;

.field private possibleEmailAddress:Ljava/lang/String;

.field private final res:Lcom/squareup/util/Res;

.field private selectedCountry:Lcom/squareup/CountryCode;

.field private final subs:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/CreateAccountPresenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/CreateAccountPresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/CreateAccountPresenter;->Companion:Lcom/squareup/ui/login/CreateAccountPresenter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "countryGuesser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticatorRunner"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createAccountRunner"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->helper:Lcom/squareup/ui/login/CreateAccountHelper;

    iput-object p3, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->accountManager:Landroid/accounts/AccountManager;

    iput-object p6, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->flow:Lflow/Flow;

    iput-object p7, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iput-object p8, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->createAccountRunner:Lcom/squareup/ui/login/CreateAccountScreen$Runner;

    .line 73
    new-instance p1, Lcom/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1;-><init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    check-cast p1, Lcom/squareup/mortar/PopupPresenter;

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->confirmationPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 81
    new-instance p1, Lcom/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1;-><init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    check-cast p1, Lcom/squareup/mortar/PopupPresenter;

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryPickerPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 87
    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 93
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string p2, "Disposables.disposed()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->createAccountDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public static final synthetic access$doCreate(Lcom/squareup/ui/login/CreateAccountPresenter;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->doCreate()V

    return-void
.end method

.method public static final synthetic access$getCountryGuesser$p(Lcom/squareup/ui/login/CreateAccountPresenter;)Lcom/squareup/location/CountryCodeGuesser;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    return-object p0
.end method

.method public static final synthetic access$setGuessedCountry(Lcom/squareup/ui/login/CreateAccountPresenter;Lkotlin/Pair;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->setGuessedCountry(Lkotlin/Pair;)V

    return-void
.end method

.method public static final synthetic access$setSelectedCountry(Lcom/squareup/ui/login/CreateAccountPresenter;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->setSelectedCountry(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method private final createRequest(Lcom/squareup/ui/login/CreateAccountView;)Lcom/squareup/server/account/CreateBody;
    .locals 7

    .line 256
    new-instance v6, Lcom/squareup/server/account/CreateBody;

    .line 257
    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountView;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountView;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountView;->getEmail()Ljava/lang/String;

    move-result-object v3

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    .line 256
    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/CreateBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method private final doCreate()V
    .locals 3

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->possibleEmailAddress:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "view"

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->getEmail()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->possibleEmailAddress:Ljava/lang/String;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_NOT_USE_PREFILL_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->helper:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/ui/login/CreateAccountPresenter;->createRequest(Lcom/squareup/ui/login/CreateAccountView;)Lcom/squareup/server/account/CreateBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/CreateAccountHelper;->attemptCreateAccount(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->createAccountDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private final getLegalPhraseFor(Lcom/squareup/CountryCode;I)Ljava/lang/CharSequence;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->legalDocuments(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 217
    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "documents"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 218
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(phraseRes)\n  \u2026, docs)\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getLegalPhraseForHelperAndPromptText(Lcom/squareup/CountryCode;IZ)Ljava/lang/CharSequence;
    .locals 3

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->legalDocumentsForHelperAndPrompt(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 228
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/login/CreateAccountView;

    const-string v0, "view"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/squareup/ui/login/CreateAccountView;->getContext()Landroid/content/Context;

    move-result-object p3

    const-string v0, "view.context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v1, 0x0

    .line 419
    new-instance v2, Lcom/squareup/fonts/FontSpan;

    invoke-static {v0, v1}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v0

    invoke-direct {v2, p3, v0}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v2, Landroid/text/style/CharacterStyle;

    .line 228
    invoke-static {p1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    :cond_0
    const-string p3, "documents"

    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 229
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(phraseRes)\n  \u2026e docs)\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getSelectedCountryWithDefault()Lcom/squareup/CountryCode;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    :goto_0
    return-object v0
.end method

.method private final prefillEmailAddress(Lcom/squareup/ui/login/CreateAccountView;)V
    .locals 7

    .line 263
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 264
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->accountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "accountManager.accounts"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v5, v1, v3

    .line 267
    iget-object v6, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v0, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_1

    if-nez v4, :cond_0

    .line 269
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v5, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->possibleEmailAddress:Ljava/lang/String;

    :cond_0
    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-lez v4, :cond_3

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->possibleEmailAddress:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/login/CreateAccountView;->useLocalEmailAddress(Ljava/lang/CharSequence;)V

    .line 277
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/login/CreateAccountPresenter$EmailCountEvent;

    invoke-direct {v0}, Lcom/squareup/ui/login/CreateAccountPresenter$EmailCountEvent;-><init>()V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private final setGuessedCountry(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "+",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 297
    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/CountryCode;

    .line 299
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->guessedCountry:Lcom/squareup/CountryCode;

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryPickerPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->showing()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 301
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->setSelectedCountry(Lcom/squareup/CountryCode;)V

    :cond_0
    return-void
.end method

.method private final setSelectedCountry(Lcom/squareup/CountryCode;)V
    .locals 2

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-eq v0, p1, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    const-string v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/CreateAccountView;->setTermsOfServiceAccepted(Z)V

    .line 288
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-eqz p1, :cond_1

    .line 290
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/CreateAccountView;->setSelectedCountry(Lcom/squareup/CountryCode;)V

    .line 291
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    iget-boolean v1, p1, Lcom/squareup/CountryCode;->hasPayments:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/CreateAccountView;->enableWorld(Z)V

    .line 292
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->updateLegalFor(Lcom/squareup/CountryCode;)V

    :cond_1
    return-void
.end method

.method private final showConfirmation()V
    .locals 6

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    invoke-static {v0}, Lcom/squareup/account/CountryCapabilitiesKt;->getHasPayments(Lcom/squareup/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    sget v0, Lcom/squareup/loggedout/R$string;->country_selection_prompt_message:I

    goto :goto_0

    .line 310
    :cond_0
    sget v0, Lcom/squareup/loggedout/R$string;->country_selection_prompt_message_no_payments:I

    .line 312
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 313
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    invoke-static {v2}, Lcom/squareup/address/CountryResources;->countryName(Lcom/squareup/CountryCode;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "country"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 315
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    new-instance v1, Lcom/squareup/register/widgets/ConfirmationStrings;

    .line 317
    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/loggedout/R$string;->country_selection_prompt_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 319
    iget-object v3, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 320
    iget-object v4, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 316
    invoke-direct {v1, v2, v0, v3, v4}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->confirmationPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method private final updateLegalFor(Lcom/squareup/CountryCode;)V
    .locals 3

    if-eqz p1, :cond_0

    goto :goto_0

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getSelectedCountryWithDefault()Lcom/squareup/CountryCode;

    move-result-object p1

    .line 201
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    .line 202
    sget v1, Lcom/squareup/loggedout/R$string;->accept_documents_legal_text:I

    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/login/CreateAccountPresenter;->getLegalPhraseFor(Lcom/squareup/CountryCode;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 201
    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/CreateAccountView;->setTermsOfServiceAcceptText(Ljava/lang/CharSequence;)V

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    .line 207
    sget v1, Lcom/squareup/loggedout/R$string;->view_documents_legal_text:I

    const/4 v2, 0x1

    invoke-direct {p0, p1, v1, v2}, Lcom/squareup/ui/login/CreateAccountPresenter;->getLegalPhraseForHelperAndPromptText(Lcom/squareup/CountryCode;IZ)Ljava/lang/CharSequence;

    move-result-object p1

    .line 206
    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/CreateAccountView;->setTermsOfServiceHelperText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final validate()Z
    .locals 1

    .line 328
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->validateFieldsAreSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->validateEmail()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->validatePassword()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->validateTermsOfService()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final validateEmail()Z
    .locals 5

    .line 363
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    const-string v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->getEmail()Ljava/lang/String;

    move-result-object v0

    .line 364
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    .line 366
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    .line 367
    sget v2, Lcom/squareup/common/strings/R$string;->invalid_email:I

    .line 368
    sget v4, Lcom/squareup/common/strings/R$string;->invalid_email_message:I

    .line 366
    invoke-direct {v1, v2, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    .line 365
    invoke-virtual {v0, v1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    .line 371
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->requestEmailFocus()V

    return v3

    .line 376
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/ui/login/CreateAccountView;->isConfirmEmailGone()Z

    move-result v2

    const/4 v4, 0x1

    if-nez v2, :cond_1

    .line 377
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/ui/login/CreateAccountView;->getConfirmEmail()Ljava/lang/String;

    move-result-object v1

    .line 378
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    .line 380
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/loggedout/R$string;->email_mismatch:I

    sget v4, Lcom/squareup/loggedout/R$string;->email_mismatch_message:I

    invoke-direct {v1, v2, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    .line 379
    invoke-virtual {v0, v1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    .line 382
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->requestEmailFocus()V

    return v3

    :cond_1
    return v4
.end method

.method private final validateFieldsAreSet()Z
    .locals 4

    .line 335
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 336
    sget v1, Lcom/squareup/loggedout/R$string;->signup_missing_required_field:I

    .line 337
    sget v2, Lcom/squareup/loggedout/R$string;->missing_account_fields:I

    .line 335
    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 339
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    const-string v2, "view"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->getEmail()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->requestEmailFocus()V

    .line 341
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    return v3

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->isConfirmEmailGone()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->getConfirmEmail()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 345
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->requestConfirmEmailFocus()V

    .line 346
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    return v3

    .line 349
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->getPassword()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 350
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v1}, Lcom/squareup/ui/login/CreateAccountView;->requestPasswordFocus()V

    .line 351
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    return v3

    .line 354
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-nez v1, :cond_3

    .line 355
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    return v3

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method private final validatePassword()Z
    .locals 4

    .line 392
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    const-string v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->getPassword()Ljava/lang/String;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    .line 395
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/loggedout/R$string;->password_too_short:I

    sget v3, Lcom/squareup/loggedout/R$string;->password_too_short_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    .line 394
    invoke-virtual {v0, v1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    .line 397
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->requestPasswordFocus()V

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private final validateTermsOfService()Z
    .locals 5

    .line 404
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    const-string v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->isTermsOfServiceAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/loggedout/R$string;->accept_documents_prompt_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getSelectedCountryWithDefault()Lcom/squareup/CountryCode;

    move-result-object v1

    sget v2, Lcom/squareup/loggedout/R$string;->accept_documents_prompt_message:I

    const/4 v3, 0x0

    .line 406
    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/login/CreateAccountPresenter;->getLegalPhraseForHelperAndPromptText(Lcom/squareup/CountryCode;IZ)Ljava/lang/CharSequence;

    move-result-object v1

    .line 408
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 409
    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->authenticatorRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    new-instance v4, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-direct {v4, v0, v1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {v2, v4}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    .line 410
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->WELCOME_FLOW_TOS_NOT_AGREED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    return v3

    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final handleBack()V
    .locals 1

    .line 132
    iget-boolean v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->cancelOnBack:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->createAccountRunner:Lcom/squareup/ui/login/CreateAccountScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/login/CreateAccountScreen$Runner;->onCreateAccountCanceled()V

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->createAccountRunner:Lcom/squareup/ui/login/CreateAccountScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/login/CreateAccountScreen$Runner;->onBackPressed()V

    :goto_0
    return-void
.end method

.method public final onConfirmEmailAddressChanged()V
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_CONFIRM_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method public final onCountryPickerClicked()V
    .locals 4

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/loggedout/R$string;->postal_country_prompt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryPickerPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 182
    new-instance v2, Lcom/squareup/ui/login/CountryPickerPopup$Params;

    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getSelectedCountryWithDefault()Lcom/squareup/CountryCode;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/squareup/ui/login/CountryPickerPopup$Params;-><init>(Ljava/lang/String;Lcom/squareup/CountryCode;)V

    check-cast v2, Landroid/os/Parcelable;

    .line 181
    invoke-virtual {v1, v2}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_COUNTRY_PICKER:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method public final onEmailAddressChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "emailAddress"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 158
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->possibleEmailAddress:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountView;->showConfirmEmail()V

    :cond_1
    return-void
.end method

.method public final onEmailAddressSelected()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/CreateAccountScreen;

    iget-boolean p1, p1, Lcom/squareup/ui/login/CreateAccountScreen;->cancelOnBack:Z

    iput-boolean p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->cancelOnBack:Z

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->createAccountDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 153
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method public final onLegalLinksClicked()V
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/login/LegalLinksDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getSelectedCountryWithDefault()Lcom/squareup/CountryCode;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/LegalLinksDialogScreen;-><init>(Lcom/squareup/CountryCode;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 99
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountView;

    const-string v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    const-string v2, "selectedCountry"

    .line 104
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-eq p1, v0, :cond_1

    .line 106
    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v0

    aget-object p1, v0, p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->setSelectedCountry(Lcom/squareup/CountryCode;)V

    goto :goto_0

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountView;->requestInitialFocus()V

    .line 112
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-nez p1, :cond_2

    .line 113
    sget-object p1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->updateLegalFor(Lcom/squareup/CountryCode;)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;-><init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 117
    :cond_2
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 119
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 118
    invoke-virtual {p1, v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 121
    new-instance v0, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$configBuilder$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-direct {v0, v2}, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$configBuilder$1;-><init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v3, Lcom/squareup/ui/login/CreateAccountPresenter$sam$java_lang_Runnable$0;

    invoke-direct {v3, v0}, Lcom/squareup/ui/login/CreateAccountPresenter$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 122
    new-instance v0, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$configBuilder$2;

    invoke-direct {v0, v2}, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$configBuilder$2;-><init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/login/CreateAccountPresenter$sam$java_lang_Runnable$0;

    invoke-direct {v2, v0}, Lcom/squareup/ui/login/CreateAccountPresenter$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loggedout/R$string;->signup_actionbar_sign_up:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/CreateAccountView;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->prefillEmailAddress(Lcom/squareup/ui/login/CreateAccountView;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PREFILL_EMAIL_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public final onPasswordChanged()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_PASSWORD:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method public final onPrimaryButtonClicked()V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SIGN_UP:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 234
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->validate()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->guessedCountry:Lcom/squareup/CountryCode;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-ne v1, v0, :cond_0

    goto :goto_0

    .line 240
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->showConfirmation()V

    goto :goto_1

    .line 237
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->doCreate()V

    :cond_2
    :goto_1
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->selectedCountry:Lcom/squareup/CountryCode;

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    .line 142
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    const-string v1, "selectedCountry"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method

.method public final onTermsOfServiceAcceptedChanged(Z)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p1, :cond_0

    .line 190
    sget-object p1, Lcom/squareup/analytics/RegisterTapName;->WELCOME_FLOW_TOS_CHECKBOX_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    goto :goto_0

    .line 192
    :cond_0
    sget-object p1, Lcom/squareup/analytics/RegisterTapName;->WELCOME_FLOW_TOS_CHECKBOX_UNSELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 188
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
