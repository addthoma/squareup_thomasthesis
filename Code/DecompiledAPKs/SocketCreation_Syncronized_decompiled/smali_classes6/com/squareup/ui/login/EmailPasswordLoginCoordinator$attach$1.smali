.class final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EmailPasswordLoginCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-static {v1, v2, v0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$updateView(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Landroid/view/View;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method
