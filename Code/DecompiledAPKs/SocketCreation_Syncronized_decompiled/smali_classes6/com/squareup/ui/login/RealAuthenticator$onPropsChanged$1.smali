.class final Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->onPropsChanged(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "landingScreenChanged",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $new:Lcom/squareup/ui/login/AuthenticatorInput;

.field final synthetic $old:Lcom/squareup/ui/login/AuthenticatorInput;

.field final synthetic $state:Lcom/squareup/ui/login/AuthenticatorState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$old:Lcom/squareup/ui/login/AuthenticatorInput;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$new:Lcom/squareup/ui/login/AuthenticatorInput;

    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$state:Lcom/squareup/ui/login/AuthenticatorState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 157
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->invoke()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Z
    .locals 3

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$old:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$new:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    if-nez v0, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$old:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;->getAsLandingScreen()Z

    move-result v0

    iget-object v2, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$new:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorInput;->getLaunchMode()Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;->getAsLandingScreen()Z

    move-result v2

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$1;->$state:Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method
