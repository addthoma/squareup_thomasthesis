.class public final Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0016\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u0000\u001a5\u0010\u0005\u001a\u00020\u0006\"\u0008\u0008\u0000\u0010\u0007*\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u0001H\u00072\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000bH\u0002\u00a2\u0006\u0002\u0010\u000c\u001a6\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u000e\"\u0008\u0008\u0000\u0010\u0007*\u00020\u0008*\u0008\u0012\u0004\u0012\u0002H\u00070\u000f2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000bH\u0002\u001a6\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u000e\"\u0008\u0008\u0000\u0010\u0007*\u00020\u0008*\u0008\u0012\u0004\u0012\u0002H\u00070\u00102\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000bH\u0002\u001a0\u0010\u0011\u001a\u00020\u0006\"\u0008\u0008\u0000\u0010\u0007*\u00020\u0008*\u0008\u0012\u0004\u0012\u0002H\u00070\u000f2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000bH\u0002\u00a8\u0006\u0012"
    }
    d2 = {
        "allSameMerchant",
        "",
        "units",
        "",
        "Lcom/squareup/protos/register/api/Unit;",
        "getFailureFromResponse",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
        "T",
        "",
        "response",
        "parser",
        "Lkotlin/Function1;",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
        "toAuthenticationCallResult",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "toAuthenticationFailure",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toAuthenticationCallResult(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->toAuthenticationCallResult(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toAuthenticationCallResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->toAuthenticationCallResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;

    move-result-object p0

    return-object p0
.end method

.method public static final allSameMerchant(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "units"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    move-object v1, p0

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    const/4 v0, 0x0

    .line 354
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/register/api/Unit;

    iget-object v1, v1, Lcom/squareup/protos/register/api/Unit;->merchant_token:Ljava/lang/String;

    .line 355
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/Unit;

    .line 356
    iget-object v2, v2, Lcom/squareup/protos/register/api/Unit;->merchant_token:Ljava/lang/String;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v3

    if-eqz v2, :cond_0

    return v0

    :cond_1
    return v3
.end method

.method private static final getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 407
    new-instance p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    sget-object p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$AccountStatusErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$AccountStatusErrorText;

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    goto :goto_0

    .line 409
    :cond_0
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    :goto_0
    return-object p0
.end method

.method private static final toAuthenticationCallResult(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "TT;>;"
        }
    .end annotation

    .line 372
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    goto :goto_0

    .line 373
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->toAuthenticationFailure(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    move-result-object p0

    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    :goto_0
    return-object p1
.end method

.method private static final toAuthenticationCallResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "TT;>;"
        }
    .end annotation

    .line 379
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    goto :goto_0

    .line 380
    :cond_0
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->toAuthenticationCallResult(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final toAuthenticationFailure(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;"
        }
    .end annotation

    .line 393
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    move-result-object p0

    goto :goto_0

    .line 394
    :cond_0
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    move-result-object p0

    goto :goto_0

    .line 395
    :cond_1
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    move-result-object p0

    goto :goto_0

    .line 396
    :cond_2
    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    sget-object p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    goto :goto_0

    .line 397
    :cond_3
    instance-of p1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz p1, :cond_4

    new-instance p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    sget-object p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    :goto_0
    return-object p0

    .line 398
    :cond_4
    instance-of p0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz p0, :cond_5

    new-instance p0, Ljava/lang/AssertionError;

    const-string p1, "Accepted is not a failure."

    invoke-direct {p0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
