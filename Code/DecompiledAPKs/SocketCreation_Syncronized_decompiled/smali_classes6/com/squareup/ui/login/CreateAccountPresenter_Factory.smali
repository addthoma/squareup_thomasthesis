.class public final Lcom/squareup/ui/login/CreateAccountPresenter_Factory;
.super Ljava/lang/Object;
.source "CreateAccountPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/CreateAccountPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/accounts/AccountManager;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final countryGuesserProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ">;"
        }
    .end annotation
.end field

.field private final createAccountRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final helperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/accounts/AccountManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountScreen$Runner;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->countryGuesserProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->helperProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->accountManagerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->authenticatorRunnerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p8, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->createAccountRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/login/CreateAccountPresenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/accounts/AccountManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountScreen$Runner;",
            ">;)",
            "Lcom/squareup/ui/login/CreateAccountPresenter_Factory;"
        }
    .end annotation

    .line 64
    new-instance v9, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)Lcom/squareup/ui/login/CreateAccountPresenter;
    .locals 10

    .line 71
    new-instance v9, Lcom/squareup/ui/login/CreateAccountPresenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/login/CreateAccountPresenter;-><init>(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/CreateAccountPresenter;
    .locals 9

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->countryGuesserProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/location/CountryCodeGuesser;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->helperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/CreateAccountHelper;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->accountManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/accounts/AccountManager;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->authenticatorRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->createAccountRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/login/CreateAccountScreen$Runner;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->newInstance(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)Lcom/squareup/ui/login/CreateAccountPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter_Factory;->get()Lcom/squareup/ui/login/CreateAccountPresenter;

    move-result-object v0

    return-object v0
.end method
