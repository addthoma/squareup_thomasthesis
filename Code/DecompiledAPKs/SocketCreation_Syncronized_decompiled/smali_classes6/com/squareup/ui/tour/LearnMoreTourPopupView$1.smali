.class Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "LearnMoreTourPopupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tour/LearnMoreTourPopupView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    iget-object v0, v0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->hasCallToAction()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$000(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Lcom/squareup/tour/TourAdapter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$100(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$100(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$000(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Lcom/squareup/tour/TourAdapter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 83
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$100(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroid/widget/Button;

    move-result-object v0

    xor-int/lit8 v2, v1, 0x1

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$200(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 86
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$1;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    iget-object v0, v0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->setCurrentPage(I)V

    return-void
.end method
