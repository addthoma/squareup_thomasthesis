.class public final enum Lcom/starmicronics/starmgsio/ScaleData$Status;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/ScaleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starmgsio/ScaleData$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starmgsio/ScaleData$Status;

.field public static final enum ERROR:Lcom/starmicronics/starmgsio/ScaleData$Status;

.field public static final enum INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

.field public static final enum STABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

.field public static final enum UNSTABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Status;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starmgsio/ScaleData$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Status;

    const/4 v2, 0x1

    const-string v3, "STABLE"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starmgsio/ScaleData$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->STABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Status;

    const/4 v3, 0x2

    const-string v4, "UNSTABLE"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starmgsio/ScaleData$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->UNSTABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Status;

    const/4 v4, 0x3

    const-string v5, "ERROR"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starmgsio/ScaleData$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->ERROR:Lcom/starmicronics/starmgsio/ScaleData$Status;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/starmicronics/starmgsio/ScaleData$Status;

    sget-object v5, Lcom/starmicronics/starmgsio/ScaleData$Status;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

    aput-object v5, v0, v1

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Status;->STABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Status;->UNSTABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Status;->ERROR:Lcom/starmicronics/starmgsio/ScaleData$Status;

    aput-object v1, v0, v4

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleData$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ScaleData$Status;
    .locals 1

    const-class v0, Lcom/starmicronics/starmgsio/ScaleData$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starmgsio/ScaleData$Status;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starmgsio/ScaleData$Status;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleData$Status;

    invoke-virtual {v0}, [Lcom/starmicronics/starmgsio/ScaleData$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starmgsio/ScaleData$Status;

    return-object v0
.end method
