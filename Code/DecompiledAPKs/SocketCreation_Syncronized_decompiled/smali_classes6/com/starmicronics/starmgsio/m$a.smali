.class Lcom/starmicronics/starmgsio/m$a;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/starmicronics/starmgsio/N;

.field final synthetic c:Lcom/starmicronics/starmgsio/m;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/m;ILcom/starmicronics/starmgsio/N;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput p2, p0, Lcom/starmicronics/starmgsio/m$a;->a:I

    iput-object p3, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v4}, Lcom/starmicronics/starmgsio/m;->h(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/starmicronics/starmgsio/a;->a(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :cond_0
    iget-object v3, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->i(Lcom/starmicronics/starmgsio/m;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/starmicronics/starmgsio/B;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :cond_1
    iget-object v3, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :cond_2
    iget-object v3, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->h(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v5}, Lcom/starmicronics/starmgsio/m;->i(Lcom/starmicronics/starmgsio/m;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    const/4 v6, 0x0

    new-array v7, v6, [B

    invoke-static {v5, v7}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;[B)[B

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    new-array v7, v6, [B

    invoke-static {v5, v7}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;[B)[B

    :cond_3
    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v5, v6}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    new-instance v7, Ljava/util/concurrent/Semaphore;

    invoke-direct {v7, v6}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    invoke-static {v5, v7}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;Ljava/util/concurrent/Semaphore;)Ljava/util/concurrent/Semaphore;

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v5}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;)Landroid/os/Handler;

    move-result-object v5

    new-instance v7, Lcom/starmicronics/starmgsio/l;

    invoke-direct {v7, p0, v3}, Lcom/starmicronics/starmgsio/l;-><init>(Lcom/starmicronics/starmgsio/m$a;Landroid/bluetooth/BluetoothDevice;)V

    invoke-virtual {v5, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v5, 0x69

    :try_start_1
    iget-object v7, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v7}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object v7

    iget v8, p0, Lcom/starmicronics/starmgsio/m$a;->a:I

    int-to-long v8, v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v0

    sub-long/2addr v8, v10

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v7, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v7}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result v7

    if-ne v7, v4, :cond_4

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    invoke-virtual {v0, v6}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :cond_4
    iget-object v7, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v7}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;)V

    iget-object v7, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v7, v6}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v0

    iget v9, p0, Lcom/starmicronics/starmgsio/m$a;->a:I

    int-to-long v9, v9

    cmp-long v11, v7, v9

    if-lez v11, :cond_3

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    invoke-virtual {v0, v5}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :catch_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;)V

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->c:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0, v6}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$a;->b:Lcom/starmicronics/starmgsio/N;

    invoke-virtual {v0, v5}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
