.class Lcom/starmicronics/starmgsio/b;
.super Landroid/bluetooth/le/ScanCallback;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starmgsio/d;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/d;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/b;->a:Lcom/starmicronics/starmgsio/d;

    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 2

    iget-object p1, p0, Lcom/starmicronics/starmgsio/b;->a:Lcom/starmicronics/starmgsio/d;

    invoke-static {p1, p2}, Lcom/starmicronics/starmgsio/d;->a(Lcom/starmicronics/starmgsio/d;Landroid/bluetooth/le/ScanResult;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/starmicronics/starmgsio/B;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/starmicronics/starmgsio/b;->a:Lcom/starmicronics/starmgsio/d;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/d;->a(Lcom/starmicronics/starmgsio/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_2
    new-instance v0, Lcom/starmicronics/starmgsio/z$a;

    invoke-direct {v0}, Lcom/starmicronics/starmgsio/z$a;-><init>()V

    invoke-virtual {v0, p2, p1}, Lcom/starmicronics/starmgsio/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/starmicronics/starmgsio/z$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/z$a;->build()Lcom/starmicronics/starmgsio/z;

    move-result-object p1

    iget-object p2, p0, Lcom/starmicronics/starmgsio/b;->a:Lcom/starmicronics/starmgsio/d;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/d;->a(Lcom/starmicronics/starmgsio/d;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lcom/starmicronics/starmgsio/b;->a:Lcom/starmicronics/starmgsio/d;

    invoke-static {p2, p1}, Lcom/starmicronics/starmgsio/d;->a(Lcom/starmicronics/starmgsio/d;Lcom/starmicronics/starmgsio/ConnectionInfo;)V

    :cond_3
    return-void
.end method
