.class Lcom/starmicronics/starioextension/bp;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0xff

    if-le p1, v1, :cond_1

    const/16 p1, 0xff

    :cond_1
    if-eqz p1, :cond_2

    const/4 v1, 0x3

    new-array v1, v1, [B

    const/16 v2, 0x1b

    aput-byte v2, v1, v0

    const/4 v0, 0x1

    const/16 v2, 0x49

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method static b(Ljava/util/List;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v0, 0xff

    if-le p1, v0, :cond_1

    const/16 p1, 0xff

    :cond_1
    if-eqz p1, :cond_4

    const/16 v0, 0x8

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    const/4 v2, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x4

    const/16 v5, 0x64

    const/16 v6, 0xa

    if-lt p1, v5, :cond_2

    div-int/lit8 v7, p1, 0x64

    add-int/lit8 v7, v7, 0x30

    int-to-byte v7, v7

    aput-byte v7, v1, v4

    rem-int/2addr p1, v5

    div-int/lit8 v4, p1, 0xa

    add-int/lit8 v4, v4, 0x30

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    rem-int/2addr p1, v6

    add-int/lit8 p1, p1, 0x30

    int-to-byte p1, p1

    aput-byte p1, v1, v2

    goto :goto_0

    :cond_2
    if-lt p1, v6, :cond_3

    div-int/lit8 v0, p1, 0xa

    add-int/lit8 v0, v0, 0x30

    int-to-byte v0, v0

    aput-byte v0, v1, v4

    rem-int/2addr p1, v6

    add-int/lit8 p1, p1, 0x30

    int-to-byte p1, p1

    aput-byte p1, v1, v3

    const/4 v0, 0x7

    goto :goto_0

    :cond_3
    add-int/lit8 p1, p1, 0x30

    int-to-byte p1, p1

    aput-byte p1, v1, v4

    const/4 v0, 0x6

    :goto_0
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x59t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static c(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0xff

    if-le p1, v1, :cond_1

    const/16 p1, 0xff

    :cond_1
    const/4 v1, 0x3

    new-array v1, v1, [B

    const/16 v2, 0x1b

    aput-byte v2, v1, v0

    const/4 v0, 0x1

    const/16 v2, 0x4a

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0xff

    if-le p1, v1, :cond_1

    const/16 p1, 0xff

    :cond_1
    const/4 v1, 0x3

    new-array v1, v1, [B

    const/16 v2, 0x1b

    aput-byte v2, v1, v0

    const/4 v0, 0x1

    const/16 v2, 0x4a

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
