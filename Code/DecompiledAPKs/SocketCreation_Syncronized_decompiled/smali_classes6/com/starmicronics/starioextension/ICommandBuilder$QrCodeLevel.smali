.class public final enum Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ICommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QrCodeLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

.field public static final enum H:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

.field public static final enum L:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

.field public static final enum M:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

.field public static final enum Q:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    const/4 v1, 0x0

    const-string v2, "L"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->L:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    const/4 v2, 0x1

    const-string v3, "M"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->M:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    const/4 v3, 0x2

    const-string v4, "Q"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->Q:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    const/4 v4, 0x3

    const-string v5, "H"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->H:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->L:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    aput-object v5, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->M:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->Q:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->H:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    aput-object v1, v0, v4

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;

    return-object v0
.end method
