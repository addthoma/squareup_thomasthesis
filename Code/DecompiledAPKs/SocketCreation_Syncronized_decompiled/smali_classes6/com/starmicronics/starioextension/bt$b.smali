.class final enum Lcom/starmicronics/starioextension/bt$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/bt$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/starmicronics/starioextension/bt$b;

.field public static final enum b:Lcom/starmicronics/starioextension/bt$b;

.field public static final enum c:Lcom/starmicronics/starioextension/bt$b;

.field public static final enum d:Lcom/starmicronics/starioextension/bt$b;

.field public static final enum e:Lcom/starmicronics/starioextension/bt$b;

.field public static final enum f:Lcom/starmicronics/starioextension/bt$b;

.field private static final synthetic g:[Lcom/starmicronics/starioextension/bt$b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/starmicronics/starioextension/bt$b;

    const/4 v1, 0x0

    const-string v2, "Unknown"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/bt$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->a:Lcom/starmicronics/starioextension/bt$b;

    new-instance v0, Lcom/starmicronics/starioextension/bt$b;

    const/4 v2, 0x1

    const-string v3, "Linear_PCM"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/bt$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->b:Lcom/starmicronics/starioextension/bt$b;

    new-instance v0, Lcom/starmicronics/starioextension/bt$b;

    const/4 v3, 0x2

    const-string v4, "MS_ADPCM"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/bt$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->c:Lcom/starmicronics/starioextension/bt$b;

    new-instance v0, Lcom/starmicronics/starioextension/bt$b;

    const/4 v4, 0x3

    const-string v5, "IBM_CSVD"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/bt$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->d:Lcom/starmicronics/starioextension/bt$b;

    new-instance v0, Lcom/starmicronics/starioextension/bt$b;

    const/4 v5, 0x4

    const-string v6, "A_Law"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/bt$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->e:Lcom/starmicronics/starioextension/bt$b;

    new-instance v0, Lcom/starmicronics/starioextension/bt$b;

    const/4 v6, 0x5

    const-string v7, "Micro_Law"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/bt$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->f:Lcom/starmicronics/starioextension/bt$b;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/starmicronics/starioextension/bt$b;

    sget-object v7, Lcom/starmicronics/starioextension/bt$b;->a:Lcom/starmicronics/starioextension/bt$b;

    aput-object v7, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/bt$b;->b:Lcom/starmicronics/starioextension/bt$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/bt$b;->c:Lcom/starmicronics/starioextension/bt$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/bt$b;->d:Lcom/starmicronics/starioextension/bt$b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/bt$b;->e:Lcom/starmicronics/starioextension/bt$b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/bt$b;->f:Lcom/starmicronics/starioextension/bt$b;

    aput-object v1, v0, v6

    sput-object v0, Lcom/starmicronics/starioextension/bt$b;->g:[Lcom/starmicronics/starioextension/bt$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/bt$b;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/bt$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/bt$b;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/bt$b;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->g:[Lcom/starmicronics/starioextension/bt$b;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/bt$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/bt$b;

    return-object v0
.end method
