.class public final enum Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/IDisplayCommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CodePageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP1252:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP437:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP850:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP852:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP858:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP860:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP863:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP865:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum CP866:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum Hangul:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum Japanese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum Katakana:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum SimplifiedChinese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

.field public static final enum TraditionalChinese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v1, 0x0

    const-string v2, "CP437"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP437:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v2, 0x1

    const-string v3, "Katakana"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->Katakana:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v3, 0x2

    const-string v4, "CP850"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP850:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v4, 0x3

    const-string v5, "CP860"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP860:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v5, 0x4

    const-string v6, "CP863"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP863:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v6, 0x5

    const-string v7, "CP865"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP865:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v7, 0x6

    const-string v8, "CP1252"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP1252:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/4 v8, 0x7

    const-string v9, "CP866"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP866:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v9, 0x8

    const-string v10, "CP852"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP852:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v10, 0x9

    const-string v11, "CP858"

    invoke-direct {v0, v11, v10}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP858:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v11, 0xa

    const-string v12, "Japanese"

    invoke-direct {v0, v12, v11}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->Japanese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v12, 0xb

    const-string v13, "SimplifiedChinese"

    invoke-direct {v0, v13, v12}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->SimplifiedChinese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v13, 0xc

    const-string v14, "TraditionalChinese"

    invoke-direct {v0, v14, v13}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->TraditionalChinese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v14, 0xd

    const-string v15, "Hangul"

    invoke-direct {v0, v15, v14}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->Hangul:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    sget-object v15, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP437:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v15, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->Katakana:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP850:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP860:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP863:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP865:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP1252:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP866:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP852:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->CP858:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->Japanese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->SimplifiedChinese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->TraditionalChinese:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->Hangul:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    aput-object v1, v0, v14

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->$VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->$VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;

    return-object v0
.end method
