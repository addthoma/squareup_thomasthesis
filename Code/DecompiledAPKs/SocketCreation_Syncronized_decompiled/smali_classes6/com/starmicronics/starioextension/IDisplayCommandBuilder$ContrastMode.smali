.class public final enum Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/IDisplayCommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContrastMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Default:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Minus1:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Minus2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Minus3:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Plus1:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Plus2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

.field public static final enum Plus3:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v1, 0x0

    const-string v2, "Minus3"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Minus3:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v2, 0x1

    const-string v3, "Minus2"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Minus2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v3, 0x2

    const-string v4, "Minus1"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Minus1:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v4, 0x3

    const-string v5, "Default"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Default:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v5, 0x4

    const-string v6, "Plus1"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Plus1:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v6, 0x5

    const-string v7, "Plus2"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Plus2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v7, 0x6

    const-string v8, "Plus3"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Plus3:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    sget-object v8, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Minus3:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v8, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Minus2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Minus1:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Default:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Plus1:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Plus2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->Plus3:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    aput-object v1, v0, v7

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->$VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->$VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;

    return-object v0
.end method
