.class abstract Lcom/starmicronics/starioextension/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/starioextension/ICommandBuilder;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starioextension/d;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/starmicronics/starioextension/j;IZ)V
.end method

.method public abstract append(B)V
.end method

.method public abstract append([B)V
.end method

.method public abstract appendAbsolutePosition(I)V
.end method

.method public appendAbsolutePosition([BI)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/d;->appendAbsolutePosition(I)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    return-void
.end method

.method public abstract appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
.end method

.method public appendAlignment([BLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;->Left:Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public abstract appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
.end method

.method public abstract appendBarcodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V
.end method

.method public appendBarcodeWithAlignment([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 0

    invoke-virtual {p0, p6}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    invoke-virtual/range {p0 .. p5}, Lcom/starmicronics/starioextension/d;->appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;->Left:Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBitmap(Landroid/graphics/Bitmap;Z)V
    .locals 6

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/4 v3, -0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/starmicronics/starioextension/d;->appendBitmap(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public appendBitmap(Landroid/graphics/Bitmap;ZIZ)V
    .locals 6

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/starmicronics/starioextension/d;->appendBitmap(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public appendBitmap(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 7

    new-instance v6, Lcom/starmicronics/starioextension/j;

    move-object v0, v6

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/starmicronics/starioextension/j;-><init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-virtual {p0, v6, p1, p2}, Lcom/starmicronics/starioextension/d;->a(Lcom/starmicronics/starioextension/j;IZ)V

    return-void
.end method

.method public appendBitmap(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 6

    const/4 v3, -0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/starmicronics/starioextension/d;->appendBitmap(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZI)V
    .locals 7

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/4 v3, -0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/starmicronics/starioextension/d;->appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V

    return-void
.end method

.method public appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZI)V
    .locals 7

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/starmicronics/starioextension/d;->appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V

    return-void
.end method

.method public appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V
    .locals 7

    new-instance v6, Lcom/starmicronics/starioextension/j;

    move-object v0, v6

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/starmicronics/starioextension/j;-><init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    const/4 p1, 0x1

    invoke-virtual {p0, v6, p6, p1}, Lcom/starmicronics/starioextension/d;->a(Lcom/starmicronics/starioextension/j;IZ)V

    return-void
.end method

.method public appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V
    .locals 7

    const/4 v3, -0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/starmicronics/starioextension/d;->appendBitmapWithAbsolutePosition(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;I)V

    return-void
.end method

.method public appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 7

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/starmicronics/starioextension/d;->appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 7

    new-instance v6, Lcom/starmicronics/starioextension/j;

    move-object v0, v6

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/starmicronics/starioextension/j;-><init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    invoke-virtual {p0, p6}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    const/4 p1, 0x0

    invoke-virtual {p0, v6, p1, p1}, Lcom/starmicronics/starioextension/d;->a(Lcom/starmicronics/starioextension/j;IZ)V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;->Left:Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 7

    sget-object v5, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/4 v3, -0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/starmicronics/starioextension/d;->appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 7

    const/4 v3, -0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/starmicronics/starioextension/d;->appendBitmapWithAlignment(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public abstract appendBlackMark(Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
.end method

.method public abstract appendCharacterSpace(I)V
.end method

.method public abstract appendCodePage(Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V
.end method

.method public abstract appendCutPaper(Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
.end method

.method public abstract appendEmphasis(Z)V
.end method

.method public appendEmphasis([B)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/d;->appendEmphasis(Z)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendEmphasis(Z)V

    return-void
.end method

.method public abstract appendFontStyle(Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V
.end method

.method public abstract appendHorizontalTabPosition([I)V
.end method

.method public abstract appendInitialization(Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V
.end method

.method public abstract appendInternational(Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V
.end method

.method public abstract appendInvert(Z)V
.end method

.method public appendInvert([B)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/d;->appendInvert(Z)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendInvert(Z)V

    return-void
.end method

.method public abstract appendLineFeed()V
.end method

.method public abstract appendLineFeed(I)V
.end method

.method public appendLineFeed([B)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/d;->appendLineFeed()V

    return-void
.end method

.method public appendLineFeed([BI)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/d;->appendLineFeed(I)V

    return-void
.end method

.method public abstract appendLineSpace(I)V
.end method

.method public abstract appendLogo(Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
.end method

.method public abstract appendMultiple(II)V
.end method

.method public appendMultiple([BII)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Lcom/starmicronics/starioextension/d;->appendMultiple(II)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1, p1}, Lcom/starmicronics/starioextension/d;->appendMultiple(II)V

    return-void
.end method

.method public appendMultipleHeight(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/starmicronics/starioextension/d;->appendMultiple(II)V

    return-void
.end method

.method public appendMultipleHeight([BI)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/d;->appendMultipleHeight(I)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendMultipleHeight(I)V

    return-void
.end method

.method public appendMultipleWidth(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/d;->appendMultiple(II)V

    return-void
.end method

.method public appendMultipleWidth([BI)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/d;->appendMultipleWidth(I)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendMultipleWidth(I)V

    return-void
.end method

.method public abstract appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
.end method

.method public abstract appendPdf417WithAbsolutePosition([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V
.end method

.method public appendPdf417WithAlignment([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;IILcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 0

    invoke-virtual {p0, p7}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    invoke-virtual/range {p0 .. p6}, Lcom/starmicronics/starioextension/d;->appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;->Left:Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;)V
    .locals 1

    const/16 v0, 0xc8

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/d;->appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V

    return-void
.end method

.method public abstract appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
.end method

.method public abstract appendPrintableArea(Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
.end method

.method public abstract appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
.end method

.method public abstract appendQrCodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V
.end method

.method public appendQrCodeWithAlignment([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;ILcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 0

    invoke-virtual {p0, p5}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/d;->appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;->Left:Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendRaw(B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/d;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/af;->a(Ljava/util/List;B)V

    return-void
.end method

.method public appendRaw([B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/d;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/af;->a(Ljava/util/List;[B)V

    return-void
.end method

.method public appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/d;->appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;I)V

    return-void
.end method

.method public appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;I)V
    .locals 1

    const/16 v0, 0xc8

    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/starmicronics/starioextension/d;->appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V

    return-void
.end method

.method public abstract appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
.end method

.method public abstract appendTopMargin(I)V
.end method

.method public abstract appendUnderLine(Z)V
.end method

.method public appendUnderLine([B)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/d;->appendUnderLine(Z)V

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->appendUnderLine(Z)V

    return-void
.end method

.method public abstract appendUnitFeed(I)V
.end method

.method public appendUnitFeed([BI)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/d;->append([B)V

    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/d;->appendUnitFeed(I)V

    return-void
.end method

.method public abstract beginDocument()V
.end method

.method public abstract endDocument()V
.end method

.method public getCommands()[B
    .locals 6

    iget-object v0, p0, Lcom/starmicronics/starioextension/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    array-length v3, v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    new-array v0, v2, [B

    iget-object v2, p0, Lcom/starmicronics/starioextension/d;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v5, v4

    invoke-static {v4, v1, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_1
    return-object v0
.end method
