.class public final enum Lcom/starmicronics/starioextension/StarIoExt$BcrModel;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BcrModel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/StarIoExt$BcrModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

.field public static final enum DS9208:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

.field public static final enum None:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

.field public static final enum POP1:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    const/4 v1, 0x0

    const-string v2, "None"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->None:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    const/4 v2, 0x1

    const-string v3, "POP1"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->POP1:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    const/4 v3, 0x2

    const-string v4, "DS9208"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->DS9208:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    sget-object v4, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->None:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    aput-object v4, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->POP1:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->DS9208:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    aput-object v1, v0, v3

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/StarIoExt$BcrModel;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/StarIoExt$BcrModel;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    return-object v0
.end method
