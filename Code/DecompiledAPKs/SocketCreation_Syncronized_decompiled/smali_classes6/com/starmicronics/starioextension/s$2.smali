.class final Lcom/starmicronics/starioextension/s$2;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/s;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP437:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP737:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP772:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP774:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP851:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP852:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP855:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP857:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP858:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP860:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP861:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP862:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP863:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP864:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP865:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP866:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP869:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP874:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP928:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP932:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP998:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP999:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1250:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1251:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1252:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP2001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3002:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3011:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3012:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3021:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3041:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3840:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3841:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3843:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3844:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3845:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3846:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3847:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3848:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->UTF8:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->Blank:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/s$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
