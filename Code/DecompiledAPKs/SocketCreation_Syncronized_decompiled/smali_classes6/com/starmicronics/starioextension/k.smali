.class Lcom/starmicronics/starioextension/k;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/starioextension/k$1;->a:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    new-array p1, v1, [B

    fill-array-data p1, :array_0

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-array p1, v1, [B

    fill-array-data p1, :array_1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-array p1, v1, [B

    fill-array-data p1, :array_2

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x1et
        0x6dt
        0x30t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x1et
        0x6dt
        0x32t
    .end array-data

    :array_2
    .array-data 1
        0x1bt
        0x1et
        0x6dt
        0x31t
    .end array-data
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/starioextension/k$1;->a:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    new-array p1, v1, [B

    fill-array-data p1, :array_0

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-array p1, v1, [B

    fill-array-data p1, :array_1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-array p1, v1, [B

    fill-array-data p1, :array_2

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x1et
        0x6dt
        0x30t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x1et
        0x6dt
        0x32t
    .end array-data

    :array_2
    .array-data 1
        0x1bt
        0x1et
        0x6dt
        0x31t
    .end array-data
.end method

.method static e(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;",
            ")V"
        }
    .end annotation

    return-void
.end method
