.class synthetic Lcom/starmicronics/starioextension/StarIoExt$1;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I

.field static final synthetic c:[I

.field static final synthetic d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->values()[Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->d:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$1;->d:[I

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->MCS10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    invoke-virtual {v2}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->d:[I

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->FVP10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    invoke-static {}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->values()[Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->c:[I

    :try_start_2
    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->c:[I

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->POP1:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->c:[I

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->DS9208:Lcom/starmicronics/starioextension/StarIoExt$BcrModel;

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExt$BcrModel;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    invoke-static {}, Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;->values()[Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->b:[I

    :try_start_4
    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->b:[I

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;->SCD222:Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExt$DisplayModel;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    invoke-static {}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->values()[Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    :try_start_5
    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNT:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNTL:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v2}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarLine:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarGraphic:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->EscPos:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->EscPosMobile:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$1;->a:[I

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarDotImpact:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    return-void
.end method
