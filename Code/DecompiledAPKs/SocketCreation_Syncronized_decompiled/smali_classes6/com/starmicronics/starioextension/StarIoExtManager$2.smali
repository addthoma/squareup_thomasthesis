.class Lcom/starmicronics/starioextension/StarIoExtManager$2;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;->a(ZLcom/starmicronics/starioextension/u;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/starmicronics/starioextension/u;

.field final synthetic c:Lcom/starmicronics/starioextension/StarIoExtManager;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;ZLcom/starmicronics/starioextension/u;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    iput-boolean p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->a:Z

    iput-object p3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->b:Lcom/starmicronics/starioextension/u;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->a:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->d(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$d;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->e(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$e;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    iget-object v4, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->f(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$f;

    move-result-object v4

    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->d(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$d;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a(Z)V

    :cond_3
    if-eqz v3, :cond_4

    iget-object v4, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->e(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$e;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a(Z)V

    :cond_4
    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->f(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$f;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a(Z)V

    :cond_5
    const/4 v4, 0x0

    if-eqz v0, :cond_6

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->d(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/StarIoExtManager$d;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0, v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$d;)Lcom/starmicronics/starioextension/StarIoExtManager$d;

    :cond_6
    if-eqz v3, :cond_7

    :try_start_1
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->e(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0, v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$e;)Lcom/starmicronics/starioextension/StarIoExtManager$e;

    :cond_7
    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->f(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$f;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a(Z)V

    :try_start_2
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->f(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0, v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$f;)Lcom/starmicronics/starioextension/StarIoExtManager$f;

    :cond_8
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    monitor-enter v0

    :try_start_3
    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_9

    :try_start_4
    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v1

    invoke-static {v1}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V
    :try_end_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_3
    :try_start_5
    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1, v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/stario/StarIOPort;)Lcom/starmicronics/stario/StarIOPort;

    :cond_9
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->h(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->Standard:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->h(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->WithBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-ne v0, v1, :cond_b

    :cond_a
    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->a:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->i(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$a;->a:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v0, v1, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->j(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    :cond_b
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->h(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->WithBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->h(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->OnlyBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-ne v0, v1, :cond_d

    :cond_c
    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->a:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->k(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$a;->n:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v0, v1, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_d
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "bt:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "usb:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_e
    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->a:Z

    if-nez v0, :cond_f

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$a;->d:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v0, v1, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_f
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0, v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/bs;)Lcom/starmicronics/starioextension/bs;

    new-instance v0, Lcom/starmicronics/starioextension/t;

    sget-object v1, Lcom/starmicronics/starioextension/t$a;->e:Lcom/starmicronics/starioextension/t$a;

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->b:Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, v1, v2}, Lcom/starmicronics/starioextension/t;-><init>(Lcom/starmicronics/starioextension/t$a;Lcom/starmicronics/starioextension/u;)V

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$2;->c:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catchall_0
    move-exception v1

    :try_start_6
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v1
.end method
