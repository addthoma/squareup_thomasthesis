.class Lcom/starmicronics/starioextension/i;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x80

.field private static final b:I = 0x3a


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0x400

    if-le v1, v3, :cond_0

    const/16 v1, 0x400

    :cond_0
    const/4 v4, 0x0

    if-gez p2, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p2

    :goto_0
    if-le v5, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_2
    const/4 v7, 0x3

    const/4 v8, 0x2

    if-ge v5, v2, :cond_11

    add-int/lit8 v9, v1, 0x7

    const/16 v10, 0x8

    div-int/2addr v9, v10

    mul-int/lit8 v11, v9, 0x8

    const/16 v12, 0x9

    add-int/2addr v11, v12

    new-array v11, v11, [B

    const/16 v13, 0x1b

    aput-byte v13, v11, v4

    const/16 v14, 0x1d

    const/4 v15, 0x1

    aput-byte v14, v11, v15

    const/16 v16, 0x53

    aput-byte v16, v11, v8

    aput-byte v15, v11, v7

    rem-int/lit16 v14, v9, 0x100

    int-to-byte v14, v14

    const/16 v16, 0x4

    aput-byte v14, v11, v16

    div-int/lit16 v14, v9, 0x100

    int-to-byte v14, v14

    const/4 v15, 0x5

    aput-byte v14, v11, v15

    const/4 v14, 0x6

    aput-byte v10, v11, v14

    const/16 v17, 0x7

    aput-byte v4, v11, v17

    aput-byte v4, v11, v10

    sub-int v4, v2, v5

    if-ge v4, v10, :cond_3

    goto :goto_3

    :cond_3
    const/16 v4, 0x8

    :goto_3
    const/4 v8, 0x0

    const/16 v18, -0x1

    const/16 v19, 0x9

    const/16 v20, -0x1

    :goto_4
    if-ge v8, v4, :cond_9

    move/from16 v12, v18

    const/4 v7, 0x0

    const/4 v14, -0x1

    :goto_5
    if-ge v7, v1, :cond_7

    const/4 v15, 0x0

    const/16 v18, 0x0

    :goto_6
    if-ge v15, v10, :cond_5

    shl-int/lit8 v10, v18, 0x1

    int-to-byte v10, v10

    add-int v13, v7, v15

    move/from16 v21, v1

    add-int v1, v5, v8

    move/from16 v22, v2

    move-object/from16 v2, p1

    invoke-virtual {v2, v13, v1}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    or-int/lit8 v1, v10, 0x1

    int-to-byte v1, v1

    move/from16 v18, v1

    goto :goto_7

    :cond_4
    move/from16 v18, v10

    :goto_7
    add-int/lit8 v15, v15, 0x1

    move/from16 v1, v21

    move/from16 v2, v22

    const/16 v10, 0x8

    goto :goto_6

    :cond_5
    move/from16 v21, v1

    move/from16 v22, v2

    move-object/from16 v2, p1

    if-eqz v18, :cond_6

    div-int/lit8 v14, v7, 0x8

    if-ge v12, v14, :cond_6

    move v12, v14

    :cond_6
    add-int/lit8 v1, v19, 0x1

    aput-byte v18, v11, v19

    add-int/lit8 v7, v7, 0x8

    move/from16 v19, v1

    move/from16 v1, v21

    move/from16 v2, v22

    const/16 v10, 0x8

    const/4 v15, 0x5

    goto :goto_5

    :cond_7
    move/from16 v21, v1

    move/from16 v22, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    if-ne v14, v1, :cond_8

    move v4, v8

    goto :goto_8

    :cond_8
    add-int/lit8 v8, v8, 0x1

    move/from16 v18, v12

    move/from16 v20, v14

    move/from16 v1, v21

    move/from16 v2, v22

    const/4 v7, 0x3

    const/16 v10, 0x8

    const/16 v12, 0x9

    const/4 v14, 0x6

    const/4 v15, 0x5

    goto :goto_4

    :cond_9
    move/from16 v21, v1

    move/from16 v22, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    move/from16 v12, v18

    move/from16 v14, v20

    :goto_8
    if-eq v12, v1, :cond_f

    if-eqz p3, :cond_b

    add-int/lit8 v12, v12, 0x1

    if-ge v12, v9, :cond_b

    rem-int/lit16 v1, v12, 0x100

    int-to-byte v1, v1

    aput-byte v1, v11, v16

    div-int/lit16 v1, v12, 0x100

    int-to-byte v1, v1

    const/4 v7, 0x5

    aput-byte v1, v11, v7

    add-int/lit8 v1, v12, 0x9

    add-int/lit8 v7, v9, 0x9

    move v8, v1

    const/4 v1, 0x1

    :goto_9
    if-ge v1, v4, :cond_a

    invoke-static {v11, v7, v11, v8, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v8, v12

    add-int/2addr v7, v9

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_a
    mul-int v12, v12, v4

    const/16 v1, 0x9

    add-int/lit8 v19, v12, 0x9

    :cond_b
    move/from16 v1, v19

    rem-int/lit16 v7, v4, 0x100

    int-to-byte v7, v7

    const/4 v8, 0x6

    aput-byte v7, v11, v8

    div-int/lit16 v7, v4, 0x100

    int-to-byte v7, v7

    aput-byte v7, v11, v17

    :goto_a
    if-eqz v6, :cond_d

    const/4 v7, 0x3

    new-array v8, v7, [B

    fill-array-data v8, :array_0

    const/16 v7, 0xff

    if-ge v7, v6, :cond_c

    add-int/lit16 v6, v6, -0xff

    const/4 v7, 0x2

    goto :goto_b

    :cond_c
    int-to-byte v6, v6

    const/4 v7, 0x2

    aput-byte v6, v8, v7

    const/4 v6, 0x0

    :goto_b
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_d
    const/4 v7, 0x2

    if-eqz v3, :cond_e

    const/4 v8, 0x5

    new-array v8, v8, [B

    const/16 v9, 0x1b

    const/4 v10, 0x0

    aput-byte v9, v8, v10

    const/16 v9, 0x1d

    const/4 v10, 0x1

    aput-byte v9, v8, v10

    const/16 v9, 0x41

    aput-byte v9, v8, v7

    rem-int/lit16 v7, v3, 0x100

    int-to-byte v7, v7

    const/4 v9, 0x3

    aput-byte v7, v8, v9

    div-int/lit16 v7, v3, 0x100

    int-to-byte v7, v7

    aput-byte v7, v8, v16

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    const/4 v7, 0x0

    invoke-static {v11, v7, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v5, v4

    const/4 v1, -0x1

    if-ne v14, v1, :cond_10

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_f
    const/4 v7, 0x0

    add-int/lit8 v6, v6, 0x1

    :cond_10
    :goto_c
    const/4 v1, 0x1

    add-int/2addr v5, v1

    move/from16 v1, v21

    move/from16 v2, v22

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_11
    const/4 v7, 0x0

    :goto_d
    if-eqz v6, :cond_13

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_1

    const/16 v3, 0xff

    if-ge v3, v6, :cond_12

    add-int/lit16 v4, v6, -0xff

    move v6, v4

    const/4 v4, 0x2

    goto :goto_e

    :cond_12
    int-to-byte v3, v6

    const/4 v4, 0x2

    aput-byte v3, v2, v4

    const/4 v6, 0x0

    :goto_e
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_13
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0x400

    if-le v1, v3, :cond_0

    const/16 v1, 0x400

    :cond_0
    const/4 v4, 0x7

    add-int/2addr v1, v4

    const/16 v5, 0x8

    div-int/2addr v1, v5

    mul-int/lit8 v1, v1, 0x8

    const/4 v6, 0x0

    if-gez p2, :cond_1

    const/4 v7, 0x0

    goto :goto_0

    :cond_1
    move/from16 v7, p2

    :goto_0
    if-le v7, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v7

    :goto_1
    const/16 v7, 0xc

    const/4 v8, 0x5

    const/4 v9, 0x4

    const/4 v10, 0x3

    const/16 v11, 0x1b

    const/4 v12, 0x2

    const/4 v13, 0x1

    if-nez p4, :cond_3

    new-array v14, v7, [B

    aput-byte v11, v14, v6

    const/16 v15, 0x4c

    aput-byte v15, v14, v13

    aput-byte v11, v14, v12

    const/16 v15, 0x57

    aput-byte v15, v14, v10

    rem-int/lit16 v15, v3, 0x100

    int-to-byte v15, v15

    aput-byte v15, v14, v9

    div-int/lit16 v15, v3, 0x100

    int-to-byte v15, v15

    aput-byte v15, v14, v8

    const/4 v15, 0x6

    aput-byte v6, v14, v15

    aput-byte v6, v14, v4

    rem-int/lit16 v4, v1, 0x100

    int-to-byte v4, v4

    aput-byte v4, v14, v5

    const/16 v4, 0x9

    div-int/lit16 v15, v1, 0x100

    int-to-byte v15, v15

    aput-byte v15, v14, v4

    const/16 v4, 0xa

    rem-int/lit16 v15, v2, 0x100

    int-to-byte v15, v15

    aput-byte v15, v14, v4

    const/16 v4, 0xb

    div-int/lit16 v15, v2, 0x100

    int-to-byte v15, v15

    aput-byte v15, v14, v4

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v2, :cond_17

    div-int/lit8 v14, v1, 0x8

    mul-int/lit8 v15, v14, 0x18

    new-array v15, v15, [B

    sub-int v7, v2, v4

    const/16 v8, 0x18

    if-ge v7, v8, :cond_4

    goto :goto_3

    :cond_4
    const/16 v7, 0x18

    :goto_3
    const/4 v8, 0x0

    const/4 v10, 0x0

    :goto_4
    if-ge v8, v7, :cond_9

    add-int v6, v4, v8

    if-ge v6, v2, :cond_8

    move/from16 v16, v10

    const/4 v10, 0x0

    :goto_5
    if-ge v10, v1, :cond_7

    const/4 v11, 0x0

    const/16 v17, 0x0

    :goto_6
    if-ge v11, v5, :cond_6

    shl-int/lit8 v5, v17, 0x1

    int-to-byte v5, v5

    add-int v9, v10, v11

    move-object/from16 v13, p1

    invoke-virtual {v13, v9, v6}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v9

    if-eqz v9, :cond_5

    or-int/lit8 v5, v5, 0x1

    int-to-byte v5, v5

    :cond_5
    move/from16 v17, v5

    add-int/lit8 v11, v11, 0x1

    const/16 v5, 0x8

    const/4 v9, 0x4

    const/4 v13, 0x1

    goto :goto_6

    :cond_6
    move-object/from16 v13, p1

    add-int/lit8 v5, v16, 0x1

    aput-byte v17, v15, v16

    add-int/lit8 v10, v10, 0x8

    move/from16 v16, v5

    const/16 v5, 0x8

    const/4 v9, 0x4

    const/16 v11, 0x1b

    const/4 v13, 0x1

    goto :goto_5

    :cond_7
    move-object/from16 v13, p1

    move/from16 v10, v16

    goto :goto_7

    :cond_8
    move-object/from16 v13, p1

    :goto_7
    add-int/lit8 v8, v8, 0x1

    const/16 v5, 0x8

    const/4 v6, 0x0

    const/4 v9, 0x4

    const/16 v11, 0x1b

    const/4 v13, 0x1

    goto :goto_4

    :cond_9
    move-object/from16 v13, p1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v6, v10

    const/4 v8, 0x0

    :goto_8
    if-eqz v6, :cond_12

    if-ge v6, v12, :cond_a

    const/16 v9, -0x7f

    aget-byte v11, v15, v8

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v11}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_8

    :cond_a
    aget-byte v9, v15, v8

    add-int/lit8 v11, v8, 0x1

    aget-byte v12, v15, v11

    if-ne v9, v12, :cond_d

    aget-byte v9, v15, v8

    add-int/lit8 v8, v8, 0x2

    add-int/lit8 v6, v6, -0x2

    move v11, v8

    const/4 v8, 0x2

    :goto_9
    if-lez v6, :cond_c

    const/16 v12, 0x3a

    if-ge v8, v12, :cond_c

    aget-byte v12, v15, v11

    if-eq v9, v12, :cond_b

    goto :goto_a

    :cond_b
    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    add-int/lit8 v11, v11, 0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_9

    :cond_c
    :goto_a
    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v8, v11

    :goto_b
    const/4 v12, 0x2

    goto :goto_8

    :cond_d
    add-int/lit8 v6, v6, -0x1

    const/4 v9, 0x1

    :goto_c
    if-lez v6, :cond_10

    const/16 v12, 0x3a

    if-ge v9, v12, :cond_10

    const/4 v12, 0x1

    if-le v6, v12, :cond_e

    aget-byte v12, v15, v11

    move/from16 v17, v1

    aget-byte v1, v15, v11

    if-ne v12, v1, :cond_f

    goto :goto_d

    :cond_e
    move/from16 v17, v1

    :cond_f
    add-int/lit8 v9, v9, 0x1

    int-to-byte v9, v9

    add-int/lit8 v11, v11, 0x1

    add-int/lit8 v6, v6, -0x1

    move/from16 v1, v17

    goto :goto_c

    :cond_10
    move/from16 v17, v1

    :goto_d
    or-int/lit16 v1, v9, 0x80

    int-to-byte v1, v1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/2addr v9, v8

    invoke-static {v15, v8, v9}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    array-length v8, v1

    const/4 v9, 0x0

    :goto_e
    if-ge v9, v8, :cond_11

    aget-byte v12, v1, v9

    invoke-static {v12}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v12

    invoke-interface {v5, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_e

    :cond_11
    move v8, v11

    move/from16 v1, v17

    goto :goto_b

    :cond_12
    move/from16 v17, v1

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v10, :cond_13

    const/4 v5, 0x0

    :cond_13
    if-eqz v3, :cond_14

    const/4 v1, 0x4

    new-array v6, v1, [B

    const/16 v1, 0x1b

    const/4 v8, 0x0

    aput-byte v1, v6, v8

    const/16 v1, 0x24

    const/4 v8, 0x1

    aput-byte v1, v6, v8

    rem-int/lit16 v1, v3, 0x100

    int-to-byte v1, v1

    const/4 v8, 0x2

    aput-byte v1, v6, v8

    div-int/lit16 v1, v3, 0x100

    int-to-byte v1, v1

    const/4 v8, 0x3

    aput-byte v1, v6, v8

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    const/16 v1, 0x58

    if-eqz v5, :cond_16

    const/4 v6, 0x5

    new-array v8, v6, [B

    const/16 v6, 0x1b

    const/4 v9, 0x0

    aput-byte v6, v8, v9

    const/4 v6, 0x1

    aput-byte v1, v8, v6

    const/16 v6, 0x33

    const/4 v9, 0x2

    aput-byte v6, v8, v9

    int-to-byte v6, v14

    const/4 v9, 0x3

    aput-byte v6, v8, v9

    int-to-byte v6, v7

    const/4 v9, 0x4

    aput-byte v6, v8, v9

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [B

    const/4 v8, 0x0

    :goto_f
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_15

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Byte;

    invoke-virtual {v9}, Ljava/lang/Byte;->byteValue()B

    move-result v9

    aput-byte v9, v6, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_f

    :cond_15
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x5

    const/4 v9, 0x4

    goto :goto_10

    :cond_16
    const/4 v5, 0x5

    new-array v6, v5, [B

    const/16 v8, 0x1b

    const/4 v9, 0x0

    aput-byte v8, v6, v9

    const/4 v8, 0x1

    aput-byte v1, v6, v8

    const/16 v8, 0x34

    const/4 v9, 0x2

    aput-byte v8, v6, v9

    int-to-byte v8, v14

    const/4 v9, 0x3

    aput-byte v8, v6, v9

    int-to-byte v8, v7

    const/4 v9, 0x4

    aput-byte v8, v6, v9

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_10
    new-array v6, v9, [B

    const/16 v8, 0x1b

    const/4 v10, 0x0

    aput-byte v8, v6, v10

    const/4 v10, 0x1

    aput-byte v1, v6, v10

    const/16 v1, 0x32

    const/4 v11, 0x2

    aput-byte v1, v6, v11

    int-to-byte v1, v7

    const/4 v7, 0x3

    aput-byte v1, v6, v7

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x18

    move/from16 v1, v17

    const/16 v5, 0x8

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x5

    const/4 v10, 0x3

    const/16 v11, 0x1b

    const/4 v12, 0x2

    const/4 v13, 0x1

    goto/16 :goto_2

    :cond_17
    const/4 v10, 0x1

    if-nez p4, :cond_18

    new-array v1, v10, [B

    const/16 v2, 0xc

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_18
    return-void
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0x400

    if-le v1, v3, :cond_0

    const/16 v1, 0x400

    :cond_0
    const/4 v4, 0x0

    if-gez p2, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p2

    :goto_0
    if-le v5, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_2
    const/4 v7, 0x3

    const/4 v8, 0x2

    if-ge v5, v2, :cond_19

    add-int/lit8 v9, v1, 0x7

    const/16 v10, 0x8

    div-int/2addr v9, v10

    mul-int/lit8 v11, v9, 0x8

    const/16 v12, 0x9

    add-int/2addr v11, v12

    new-array v11, v11, [B

    const/16 v13, 0x1b

    aput-byte v13, v11, v4

    const/16 v14, 0x1d

    const/4 v15, 0x1

    aput-byte v14, v11, v15

    const/16 v16, 0x53

    aput-byte v16, v11, v8

    aput-byte v15, v11, v7

    rem-int/lit16 v14, v9, 0x100

    int-to-byte v14, v14

    const/4 v13, 0x4

    aput-byte v14, v11, v13

    div-int/lit16 v14, v9, 0x100

    int-to-byte v14, v14

    const/4 v7, 0x5

    aput-byte v14, v11, v7

    const/4 v14, 0x6

    aput-byte v10, v11, v14

    const/16 v17, 0x7

    aput-byte v4, v11, v17

    aput-byte v4, v11, v10

    sub-int v15, v2, v5

    if-ge v15, v10, :cond_3

    goto :goto_3

    :cond_3
    const/16 v15, 0x8

    :goto_3
    const/4 v8, 0x0

    const/16 v18, -0x1

    const/16 v19, 0x9

    const/16 v20, -0x1

    :goto_4
    if-ge v8, v15, :cond_9

    move/from16 v7, v18

    const/4 v12, -0x1

    const/4 v14, 0x0

    :goto_5
    if-ge v14, v1, :cond_7

    const/4 v13, 0x0

    const/16 v18, 0x0

    :goto_6
    if-ge v13, v10, :cond_5

    shl-int/lit8 v10, v18, 0x1

    int-to-byte v10, v10

    add-int v4, v14, v13

    move/from16 v21, v1

    add-int v1, v5, v8

    move/from16 v22, v2

    move-object/from16 v2, p1

    invoke-virtual {v2, v4, v1}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    or-int/lit8 v1, v10, 0x1

    int-to-byte v1, v1

    move/from16 v18, v1

    goto :goto_7

    :cond_4
    move/from16 v18, v10

    :goto_7
    add-int/lit8 v13, v13, 0x1

    move/from16 v1, v21

    move/from16 v2, v22

    const/16 v10, 0x8

    goto :goto_6

    :cond_5
    move/from16 v21, v1

    move/from16 v22, v2

    move-object/from16 v2, p1

    if-eqz v18, :cond_6

    div-int/lit8 v12, v14, 0x8

    if-ge v7, v12, :cond_6

    move v7, v12

    :cond_6
    add-int/lit8 v1, v19, 0x1

    aput-byte v18, v11, v19

    add-int/lit8 v14, v14, 0x8

    move/from16 v19, v1

    move/from16 v1, v21

    move/from16 v2, v22

    const/16 v10, 0x8

    const/4 v13, 0x4

    goto :goto_5

    :cond_7
    move/from16 v21, v1

    move/from16 v22, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    if-ne v12, v1, :cond_8

    move v15, v8

    goto :goto_8

    :cond_8
    add-int/lit8 v8, v8, 0x1

    move/from16 v18, v7

    move/from16 v20, v12

    move/from16 v1, v21

    move/from16 v2, v22

    const/4 v7, 0x5

    const/16 v10, 0x8

    const/16 v12, 0x9

    const/4 v13, 0x4

    const/4 v14, 0x6

    goto :goto_4

    :cond_9
    move/from16 v21, v1

    move/from16 v22, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    move/from16 v7, v18

    move/from16 v12, v20

    :goto_8
    if-eq v7, v1, :cond_17

    if-eqz p3, :cond_b

    add-int/lit8 v7, v7, 0x1

    if-ge v7, v9, :cond_b

    rem-int/lit16 v1, v7, 0x100

    int-to-byte v1, v1

    const/4 v4, 0x4

    aput-byte v1, v11, v4

    div-int/lit16 v1, v7, 0x100

    int-to-byte v1, v1

    const/4 v4, 0x5

    aput-byte v1, v11, v4

    add-int/lit8 v1, v7, 0x9

    add-int/lit8 v4, v9, 0x9

    move v8, v1

    const/4 v1, 0x1

    :goto_9
    if-ge v1, v15, :cond_a

    invoke-static {v11, v4, v11, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v8, v7

    add-int/2addr v4, v9

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_a
    mul-int v7, v7, v15

    const/16 v1, 0x9

    add-int/lit8 v19, v7, 0x9

    goto :goto_a

    :cond_b
    const/16 v1, 0x9

    :goto_a
    move/from16 v4, v19

    rem-int/lit16 v7, v15, 0x100

    int-to-byte v7, v7

    const/4 v8, 0x6

    aput-byte v7, v11, v8

    div-int/lit16 v7, v15, 0x100

    int-to-byte v7, v7

    aput-byte v7, v11, v17

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v8, v4, -0x9

    const/4 v9, 0x0

    :goto_b
    if-eqz v8, :cond_12

    const/4 v10, 0x2

    if-ge v8, v10, :cond_c

    aget-byte v13, v11, v1

    new-array v14, v10, [B

    const/4 v10, 0x0

    aput-byte v10, v14, v10

    const/4 v10, 0x1

    aput-byte v13, v14, v10

    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x2

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v8, v8, -0x1

    goto :goto_b

    :cond_c
    aget-byte v10, v11, v1

    add-int/lit8 v13, v1, 0x1

    aget-byte v14, v11, v13

    if-ne v10, v14, :cond_f

    aget-byte v10, v11, v1

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v8, v8, -0x2

    move v13, v1

    const/4 v1, 0x2

    :goto_c
    if-eqz v8, :cond_e

    const/16 v14, 0x80

    if-ge v1, v14, :cond_e

    aget-byte v14, v11, v13

    if-eq v10, v14, :cond_d

    goto :goto_d

    :cond_d
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v13, v13, 0x1

    add-int/lit8 v8, v8, -0x1

    goto :goto_c

    :cond_e
    :goto_d
    const/4 v14, 0x1

    rsub-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    const/4 v14, 0x2

    new-array v2, v14, [B

    const/4 v14, 0x0

    aput-byte v1, v2, v14

    const/4 v14, 0x1

    aput-byte v10, v2, v14

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x2

    move-object/from16 v2, p1

    move v1, v13

    goto :goto_b

    :cond_f
    const/4 v14, 0x1

    add-int/lit8 v8, v8, -0x1

    const/4 v2, 0x1

    :goto_e
    if-eqz v8, :cond_11

    const/16 v10, 0x80

    if-ge v2, v10, :cond_11

    if-ge v14, v8, :cond_10

    aget-byte v10, v11, v13

    add-int/lit8 v14, v13, 0x1

    aget-byte v14, v11, v14

    if-ne v10, v14, :cond_10

    goto :goto_f

    :cond_10
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v13, v13, 0x1

    add-int/lit8 v8, v8, -0x1

    const/4 v14, 0x1

    goto :goto_e

    :cond_11
    :goto_f
    add-int/lit8 v10, v2, -0x1

    int-to-byte v10, v10

    move/from16 v17, v8

    const/4 v14, 0x1

    new-array v8, v14, [B

    const/4 v14, 0x0

    aput-byte v10, v8, v14

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int v8, v1, v2

    invoke-static {v11, v1, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v9, v2

    move-object/from16 v2, p1

    move v1, v13

    move/from16 v8, v17

    goto/16 :goto_b

    :cond_12
    :goto_10
    if-eqz v6, :cond_14

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    const/16 v1, 0xff

    if-ge v1, v6, :cond_13

    add-int/lit16 v1, v6, -0xff

    move v6, v1

    const/4 v8, 0x2

    goto :goto_11

    :cond_13
    int-to-byte v1, v6

    const/4 v8, 0x2

    aput-byte v1, v2, v8

    const/4 v6, 0x0

    :goto_11
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    :cond_14
    const/4 v8, 0x2

    if-eqz v3, :cond_15

    const/4 v1, 0x5

    new-array v2, v1, [B

    const/16 v1, 0x1b

    const/4 v10, 0x0

    aput-byte v1, v2, v10

    const/16 v1, 0x1d

    const/4 v10, 0x1

    aput-byte v1, v2, v10

    const/16 v1, 0x41

    aput-byte v1, v2, v8

    rem-int/lit16 v1, v3, 0x100

    int-to-byte v1, v1

    const/4 v8, 0x3

    aput-byte v1, v2, v8

    div-int/lit16 v1, v3, 0x100

    int-to-byte v1, v1

    const/4 v8, 0x4

    aput-byte v1, v2, v8

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_12

    :cond_15
    const/4 v8, 0x4

    :goto_12
    add-int/lit8 v1, v9, 0xd

    if-ge v4, v1, :cond_16

    const/4 v1, 0x0

    invoke-static {v11, v1, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    goto :goto_13

    :cond_16
    new-array v1, v8, [B

    fill-array-data v1, :array_1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x8

    invoke-static {v11, v8, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x5

    new-array v1, v1, [B

    rem-int/lit16 v2, v9, 0x100

    int-to-byte v2, v2

    const/4 v4, 0x0

    aput-byte v2, v1, v4

    div-int/lit16 v9, v9, 0x100

    int-to-byte v2, v9

    const/4 v8, 0x1

    aput-byte v2, v1, v8

    const/4 v2, 0x2

    aput-byte v4, v1, v2

    const/4 v2, 0x3

    aput-byte v4, v1, v2

    const/4 v2, 0x4

    aput-byte v4, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_13
    add-int/lit8 v15, v15, -0x1

    add-int/2addr v5, v15

    const/4 v1, -0x1

    if-ne v12, v1, :cond_18

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_14

    :cond_17
    const/4 v4, 0x0

    add-int/lit8 v6, v6, 0x1

    :cond_18
    :goto_14
    const/4 v1, 0x1

    add-int/2addr v5, v1

    move/from16 v1, v21

    move/from16 v2, v22

    goto/16 :goto_2

    :cond_19
    :goto_15
    if-eqz v6, :cond_1b

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_2

    const/16 v3, 0xff

    if-ge v3, v6, :cond_1a

    add-int/lit16 v3, v6, -0xff

    move v6, v3

    const/4 v5, 0x2

    goto :goto_16

    :cond_1a
    int-to-byte v3, v6

    const/4 v5, 0x2

    aput-byte v3, v2, v5

    const/4 v6, 0x0

    :goto_16
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_15

    :cond_1b
    return-void

    nop

    :array_0
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x1dt
        0x58t
        0x1t
    .end array-data

    :array_2
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0x400

    if-le v1, v3, :cond_0

    const/16 v1, 0x400

    :cond_0
    const/4 v4, 0x0

    if-gez p2, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p2

    :goto_0
    if-le v5, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    :goto_2
    const/4 v12, 0x3

    const/4 v13, 0x2

    if-ge v7, v2, :cond_11

    add-int/lit8 v14, v1, 0x7

    const/16 v15, 0x8

    div-int/2addr v14, v15

    mul-int/lit8 v16, v14, 0x18

    const/16 v17, 0x4

    add-int/lit8 v10, v16, 0x4

    new-array v10, v10, [B

    const/16 v11, 0x1b

    aput-byte v11, v10, v4

    const/16 v16, 0x6b

    const/16 v18, 0x1

    aput-byte v16, v10, v18

    rem-int/lit16 v4, v14, 0x100

    int-to-byte v4, v4

    aput-byte v4, v10, v13

    div-int/lit16 v4, v14, 0x100

    int-to-byte v4, v4

    aput-byte v4, v10, v12

    sub-int v4, v2, v7

    const/16 v11, 0x18

    if-ge v4, v11, :cond_3

    goto :goto_3

    :cond_3
    const/16 v4, 0x18

    :goto_3
    const/4 v5, 0x0

    const/4 v6, 0x4

    const/16 v19, -0x1

    :goto_4
    if-ge v5, v4, :cond_9

    move/from16 v12, v19

    move/from16 v19, v6

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v1, :cond_7

    const/4 v13, 0x0

    const/16 v20, 0x0

    :goto_6
    if-ge v13, v15, :cond_5

    shl-int/lit8 v15, v20, 0x1

    int-to-byte v15, v15

    add-int v11, v6, v13

    move/from16 v22, v1

    add-int v1, v7, v5

    move/from16 v23, v2

    move-object/from16 v2, p1

    invoke-virtual {v2, v11, v1}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    or-int/lit8 v1, v15, 0x1

    int-to-byte v1, v1

    move/from16 v20, v1

    goto :goto_7

    :cond_4
    move/from16 v20, v15

    :goto_7
    add-int/lit8 v13, v13, 0x1

    move/from16 v1, v22

    move/from16 v2, v23

    const/16 v15, 0x8

    goto :goto_6

    :cond_5
    move/from16 v22, v1

    move/from16 v23, v2

    move-object/from16 v2, p1

    if-eqz v20, :cond_6

    div-int/lit8 v1, v6, 0x8

    if-ge v12, v1, :cond_6

    move v12, v1

    :cond_6
    add-int/lit8 v1, v19, 0x1

    aput-byte v20, v10, v19

    add-int/lit8 v6, v6, 0x8

    move/from16 v19, v1

    move/from16 v1, v22

    move/from16 v2, v23

    const/4 v13, 0x2

    const/16 v15, 0x8

    goto :goto_5

    :cond_7
    move/from16 v22, v1

    move/from16 v23, v2

    move-object/from16 v2, p1

    const/4 v1, -0x1

    if-nez v5, :cond_8

    if-ne v12, v1, :cond_8

    move/from16 v6, v19

    goto :goto_8

    :cond_8
    add-int/lit8 v5, v5, 0x1

    move/from16 v6, v19

    move/from16 v1, v22

    move/from16 v2, v23

    const/4 v13, 0x2

    const/16 v15, 0x8

    move/from16 v19, v12

    const/4 v12, 0x3

    goto :goto_4

    :cond_9
    move/from16 v22, v1

    move/from16 v23, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    move/from16 v12, v19

    :goto_8
    const-wide/16 v20, 0x1

    if-eq v12, v1, :cond_10

    int-to-long v1, v4

    :goto_9
    const-wide/16 v24, 0x18

    cmp-long v5, v1, v24

    if-gez v5, :cond_a

    add-int/2addr v6, v14

    add-long v1, v1, v20

    goto :goto_9

    :cond_a
    if-eqz p3, :cond_c

    add-int/lit8 v12, v12, 0x1

    if-ge v12, v14, :cond_c

    rem-int/lit16 v1, v12, 0x100

    int-to-byte v1, v1

    const/4 v2, 0x2

    aput-byte v1, v10, v2

    div-int/lit16 v1, v12, 0x100

    int-to-byte v1, v1

    const/4 v2, 0x3

    aput-byte v1, v10, v2

    add-int/lit8 v1, v12, 0x4

    add-int/lit8 v2, v14, 0x4

    move v6, v1

    move v5, v2

    move-wide/from16 v1, v20

    :goto_a
    cmp-long v11, v1, v24

    if-gez v11, :cond_b

    invoke-static {v10, v5, v10, v6, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v6, v12

    add-int/2addr v5, v14

    add-long v1, v1, v20

    goto :goto_a

    :cond_b
    mul-int/lit8 v12, v12, 0x18

    add-int/lit8 v6, v12, 0x4

    :cond_c
    :goto_b
    const-wide/16 v1, 0x0

    cmp-long v5, v8, v1

    if-eqz v5, :cond_e

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    const-wide/16 v11, 0xff

    cmp-long v1, v8, v11

    if-lez v1, :cond_d

    sub-long/2addr v8, v11

    const/4 v5, 0x2

    goto :goto_c

    :cond_d
    long-to-int v1, v8

    int-to-byte v1, v1

    const/4 v5, 0x2

    aput-byte v1, v2, v5

    const-wide/16 v8, 0x0

    :goto_c
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_e
    const/4 v5, 0x2

    if-eqz v3, :cond_f

    const/4 v1, 0x5

    new-array v1, v1, [B

    const/16 v2, 0x1b

    const/4 v11, 0x0

    aput-byte v2, v1, v11

    const/16 v2, 0x1d

    aput-byte v2, v1, v18

    const/16 v2, 0x41

    aput-byte v2, v1, v5

    rem-int/lit16 v2, v3, 0x100

    int-to-byte v2, v2

    const/4 v5, 0x3

    aput-byte v2, v1, v5

    div-int/lit16 v2, v3, 0x100

    int-to-byte v2, v2

    aput-byte v2, v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_f
    const/4 v5, 0x3

    :goto_d
    const/4 v1, 0x0

    invoke-static {v10, v1, v6}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v2, v5, [B

    const/16 v5, 0x1b

    aput-byte v5, v2, v1

    const/16 v5, 0x49

    aput-byte v5, v2, v18

    int-to-byte v5, v4

    const/4 v6, 0x2

    aput-byte v5, v2, v6

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v7, v4

    goto :goto_e

    :cond_10
    const/4 v1, 0x0

    add-long v8, v8, v20

    :goto_e
    add-int/lit8 v7, v7, 0x1

    move/from16 v1, v22

    move/from16 v2, v23

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_11
    const-wide/16 v1, 0x0

    :goto_f
    cmp-long v3, v8, v1

    if-eqz v3, :cond_13

    const/4 v3, 0x3

    new-array v4, v3, [B

    fill-array-data v4, :array_1

    const-wide/16 v5, 0xff

    cmp-long v7, v8, v5

    if-lez v7, :cond_12

    sub-long v7, v8, v5

    move-wide v9, v7

    const/4 v8, 0x2

    goto :goto_10

    :cond_12
    long-to-int v7, v8

    int-to-byte v7, v7

    const/4 v8, 0x2

    aput-byte v7, v4, v8

    move-wide v9, v1

    :goto_10
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v8, v9

    goto :goto_f

    :cond_13
    return-void

    nop

    :array_0
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v3

    const/16 v4, 0x400

    if-le v2, v4, :cond_0

    const/16 v2, 0x400

    :cond_0
    const/4 v5, 0x0

    if-gez p2, :cond_1

    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    move/from16 v6, p2

    :goto_0
    if-le v6, v4, :cond_2

    goto :goto_1

    :cond_2
    move v4, v6

    :goto_1
    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_2
    const/4 v9, 0x2

    const/4 v10, 0x3

    if-ge v6, v3, :cond_12

    mul-int/lit8 v11, v2, 0x3

    const/4 v12, 0x4

    add-int/2addr v11, v12

    new-array v11, v11, [B

    const/16 v13, 0x1b

    aput-byte v13, v11, v5

    const/16 v14, 0x58

    const/4 v15, 0x1

    aput-byte v14, v11, v15

    rem-int/lit16 v14, v2, 0x100

    int-to-byte v14, v14

    aput-byte v14, v11, v9

    div-int/lit16 v14, v2, 0x100

    int-to-byte v14, v14

    aput-byte v14, v11, v10

    sub-int v14, v3, v6

    const/16 v15, 0x18

    if-ge v14, v15, :cond_3

    goto :goto_3

    :cond_3
    const/16 v14, 0x18

    :goto_3
    add-int/lit8 v15, v2, -0x1

    :goto_4
    const/4 v5, -0x1

    if-ltz v15, :cond_5

    invoke-virtual {v1, v15, v6}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v16

    if-eqz v16, :cond_4

    goto :goto_5

    :cond_4
    add-int/lit8 v15, v15, -0x1

    goto :goto_4

    :cond_5
    const/4 v15, -0x1

    :goto_5
    if-ne v15, v5, :cond_6

    add-int/lit8 v7, v7, 0x1

    move/from16 v21, v3

    const/4 v5, 0x0

    const/4 v9, 0x1

    goto/16 :goto_10

    :cond_6
    const/4 v5, 0x0

    const/16 v16, 0x4

    :goto_6
    if-ge v5, v2, :cond_d

    add-int/lit8 v17, v14, 0x7

    const/16 v18, 0x8

    div-int/lit8 v13, v17, 0x8

    move v8, v15

    const/4 v15, 0x0

    :goto_7
    if-ge v15, v13, :cond_c

    add-int/lit8 v12, v13, -0x1

    if-ne v15, v12, :cond_8

    div-int/lit8 v19, v14, 0x8

    if-nez v19, :cond_7

    goto :goto_8

    :cond_7
    mul-int/lit8 v12, v12, 0x8

    sub-int v12, v14, v12

    goto :goto_9

    :cond_8
    :goto_8
    const/16 v12, 0x8

    :goto_9
    const/4 v9, 0x0

    const/16 v20, 0x0

    :goto_a
    if-ge v9, v12, :cond_a

    shl-int/lit8 v10, v20, 0x1

    int-to-byte v10, v10

    mul-int/lit8 v20, v15, 0x8

    add-int v20, v6, v20

    move/from16 v21, v3

    add-int v3, v20, v9

    invoke-virtual {v1, v5, v3}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v3

    if-eqz v3, :cond_9

    or-int/lit8 v3, v10, 0x1

    int-to-byte v3, v3

    move/from16 v20, v3

    goto :goto_b

    :cond_9
    move/from16 v20, v10

    :goto_b
    add-int/lit8 v9, v9, 0x1

    move/from16 v3, v21

    const/4 v10, 0x3

    goto :goto_a

    :cond_a
    move/from16 v21, v3

    rsub-int/lit8 v3, v12, 0x8

    shl-int v3, v20, v3

    int-to-byte v3, v3

    if-eqz v3, :cond_b

    if-ge v8, v5, :cond_b

    move v8, v5

    :cond_b
    add-int/lit8 v9, v16, 0x1

    aput-byte v3, v11, v16

    add-int/lit8 v15, v15, 0x1

    move/from16 v16, v9

    move/from16 v3, v21

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v12, 0x4

    goto :goto_7

    :cond_c
    move/from16 v21, v3

    const/4 v3, 0x3

    rsub-int/lit8 v10, v13, 0x3

    add-int v16, v16, v10

    add-int/lit8 v5, v5, 0x1

    move v15, v8

    move/from16 v3, v21

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v12, 0x4

    const/16 v13, 0x1b

    goto :goto_6

    :cond_d
    move/from16 v21, v3

    if-eqz p3, :cond_e

    add-int/lit8 v15, v15, 0x1

    if-ge v15, v2, :cond_e

    rem-int/lit16 v3, v15, 0x100

    int-to-byte v3, v3

    const/4 v5, 0x2

    aput-byte v3, v11, v5

    div-int/lit16 v3, v15, 0x100

    int-to-byte v3, v3

    const/4 v5, 0x3

    aput-byte v3, v11, v5

    mul-int/lit8 v15, v15, 0x3

    const/4 v3, 0x4

    invoke-static {v11, v3, v11, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v16, v15, 0x4

    goto :goto_c

    :cond_e
    const/4 v5, 0x3

    :goto_c
    move/from16 v3, v16

    :goto_d
    if-eqz v7, :cond_10

    new-array v8, v5, [B

    fill-array-data v8, :array_0

    const/16 v5, 0xff

    if-le v7, v5, :cond_f

    add-int/lit16 v5, v7, -0xff

    move v7, v5

    const/4 v9, 0x2

    goto :goto_e

    :cond_f
    int-to-byte v5, v7

    const/4 v9, 0x2

    aput-byte v5, v8, v9

    const/4 v7, 0x0

    :goto_e
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x3

    goto :goto_d

    :cond_10
    const/4 v9, 0x2

    if-eqz v4, :cond_11

    const/4 v5, 0x5

    new-array v5, v5, [B

    const/16 v8, 0x1b

    const/4 v10, 0x0

    aput-byte v8, v5, v10

    const/16 v8, 0x1d

    const/4 v10, 0x1

    aput-byte v8, v5, v10

    const/16 v8, 0x41

    aput-byte v8, v5, v9

    rem-int/lit16 v8, v4, 0x100

    int-to-byte v8, v8

    const/4 v9, 0x3

    aput-byte v8, v5, v9

    div-int/lit16 v8, v4, 0x100

    int-to-byte v8, v8

    const/4 v10, 0x4

    aput-byte v8, v5, v10

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    :cond_11
    const/4 v9, 0x3

    :goto_f
    const/4 v5, 0x0

    invoke-static {v11, v5, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v3, v9, [B

    const/16 v8, 0x1b

    aput-byte v8, v3, v5

    const/16 v8, 0x49

    const/4 v9, 0x1

    aput-byte v8, v3, v9

    int-to-byte v8, v14

    const/4 v10, 0x2

    aput-byte v8, v3, v10

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, -0x1

    add-int/2addr v6, v14

    :goto_10
    add-int/2addr v6, v9

    move/from16 v3, v21

    goto/16 :goto_2

    :cond_12
    :goto_11
    if-eqz v7, :cond_14

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_1

    const/16 v3, 0xff

    if-le v7, v3, :cond_13

    add-int/lit16 v4, v7, -0xff

    move v7, v4

    const/4 v6, 0x2

    goto :goto_12

    :cond_13
    int-to-byte v4, v7

    const/4 v6, 0x2

    aput-byte v4, v2, v6

    const/4 v7, 0x0

    :goto_12
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_11

    :cond_14
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x49t
        -0x1t
    .end array-data
.end method

.method static e(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v3

    const/16 v4, 0x400

    if-le v2, v4, :cond_0

    const/16 v2, 0x400

    :cond_0
    const/4 v5, 0x0

    if-gez p2, :cond_1

    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    move/from16 v6, p2

    :goto_0
    if-le v6, v4, :cond_2

    const/16 v6, 0x400

    :cond_2
    add-int v7, v6, v2

    if-le v7, v4, :cond_3

    rsub-int v2, v6, 0x400

    :cond_3
    const/4 v4, 0x0

    const/4 v7, 0x0

    :goto_1
    const/16 v12, 0x8

    if-ge v4, v3, :cond_11

    add-int v13, v6, v2

    add-int/lit8 v13, v13, 0x7

    div-int/2addr v13, v12

    add-int/lit8 v14, v13, 0x3

    new-array v14, v14, [B

    const/16 v15, 0x62

    aput-byte v15, v14, v5

    rem-int/lit16 v15, v13, 0x100

    int-to-byte v15, v15

    const/16 v16, 0x1

    aput-byte v15, v14, v16

    div-int/lit16 v13, v13, 0x100

    int-to-byte v13, v13

    const/4 v15, 0x2

    aput-byte v13, v14, v15

    const/4 v13, 0x3

    const/4 v13, 0x0

    const/16 v17, 0x3

    :goto_2
    if-ge v13, v6, :cond_4

    add-int/lit8 v18, v17, 0x1

    aput-byte v5, v14, v17

    add-int/lit8 v13, v13, 0x8

    move/from16 v17, v18

    goto :goto_2

    :cond_4
    rem-int/lit8 v13, v6, 0x8

    const/4 v5, -0x1

    if-eqz v13, :cond_8

    rsub-int/lit8 v13, v13, 0x8

    add-int/lit8 v17, v17, -0x1

    const/4 v8, 0x0

    const/16 v19, 0x0

    :goto_3
    if-ge v8, v13, :cond_6

    shl-int/lit8 v9, v19, 0x1

    int-to-byte v9, v9

    invoke-virtual {v1, v8, v4}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v19

    if-eqz v19, :cond_5

    or-int/lit8 v9, v9, 0x1

    int-to-byte v9, v9

    :cond_5
    move/from16 v19, v9

    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_6
    if-eqz v19, :cond_7

    move/from16 v8, v17

    goto :goto_4

    :cond_7
    const/4 v8, -0x1

    :goto_4
    add-int/lit8 v9, v17, 0x1

    aput-byte v19, v14, v17

    :goto_5
    move/from16 v17, v9

    goto :goto_6

    :cond_8
    const/4 v8, -0x1

    const/4 v13, 0x0

    :goto_6
    if-ge v13, v2, :cond_c

    const/4 v9, 0x0

    const/16 v19, 0x0

    :goto_7
    if-ge v9, v12, :cond_a

    shl-int/lit8 v10, v19, 0x1

    int-to-byte v10, v10

    add-int v11, v13, v9

    invoke-virtual {v1, v11, v4}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v11

    if-eqz v11, :cond_9

    or-int/lit8 v10, v10, 0x1

    int-to-byte v10, v10

    :cond_9
    move/from16 v19, v10

    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    :cond_a
    if-eqz v19, :cond_b

    move/from16 v8, v17

    :cond_b
    add-int/lit8 v9, v17, 0x1

    aput-byte v19, v14, v17

    add-int/lit8 v13, v13, 0x8

    goto :goto_5

    :cond_c
    if-eq v8, v5, :cond_10

    if-eqz p3, :cond_d

    add-int/lit8 v17, v8, 0x1

    add-int/lit8 v5, v17, -0x3

    rem-int/lit16 v8, v5, 0x100

    int-to-byte v8, v8

    aput-byte v8, v14, v16

    div-int/lit16 v5, v5, 0x100

    int-to-byte v5, v5

    aput-byte v5, v14, v15

    :cond_d
    move/from16 v5, v17

    :goto_8
    if-eqz v7, :cond_f

    new-array v8, v12, [B

    fill-array-data v8, :array_0

    const/16 v9, 0x64

    if-lt v7, v9, :cond_e

    add-int/lit8 v7, v7, -0x64

    goto :goto_9

    :cond_e
    div-int/lit8 v9, v7, 0x64

    add-int/lit8 v9, v9, 0x30

    int-to-byte v9, v9

    const/4 v10, 0x4

    aput-byte v9, v8, v10

    rem-int/lit8 v7, v7, 0x64

    div-int/lit8 v9, v7, 0xa

    add-int/lit8 v9, v9, 0x30

    int-to-byte v9, v9

    const/4 v10, 0x5

    aput-byte v9, v8, v10

    rem-int/lit8 v7, v7, 0xa

    add-int/lit8 v7, v7, 0x30

    int-to-byte v7, v7

    const/4 v9, 0x6

    aput-byte v7, v8, v9

    const/4 v7, 0x0

    :goto_9
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_f
    const/4 v8, 0x0

    invoke-static {v14, v8, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_10
    const/4 v8, 0x0

    add-int/lit8 v7, v7, 0x1

    :goto_a
    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_11
    const/4 v8, 0x0

    :goto_b
    if-eqz v7, :cond_13

    new-array v1, v12, [B

    fill-array-data v1, :array_1

    const/16 v2, 0x64

    if-lt v7, v2, :cond_12

    add-int/lit8 v5, v7, -0x64

    move v7, v5

    const/4 v4, 0x4

    const/4 v5, 0x5

    const/4 v6, 0x6

    goto :goto_c

    :cond_12
    div-int/lit8 v3, v7, 0x64

    add-int/lit8 v3, v3, 0x30

    int-to-byte v3, v3

    const/4 v4, 0x4

    aput-byte v3, v1, v4

    rem-int/lit8 v7, v7, 0x64

    div-int/lit8 v3, v7, 0xa

    add-int/lit8 v3, v3, 0x30

    int-to-byte v3, v3

    const/4 v5, 0x5

    aput-byte v3, v1, v5

    rem-int/lit8 v7, v7, 0xa

    add-int/lit8 v7, v7, 0x30

    int-to-byte v3, v7

    const/4 v6, 0x6

    aput-byte v3, v1, v6

    const/4 v7, 0x0

    :goto_c
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_13
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x59t
        0x31t
        0x30t
        0x30t
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x59t
        0x31t
        0x30t
        0x30t
        0x0t
    .end array-data
.end method

.method static f(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0x400

    if-le v1, v3, :cond_0

    const/16 v1, 0x400

    :cond_0
    const/4 v4, 0x0

    if-gez p2, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p2

    :goto_0
    if-le v5, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_2
    const/4 v8, 0x3

    const/4 v9, 0x2

    if-ge v5, v2, :cond_11

    add-int/lit8 v10, v1, 0x7

    const/16 v11, 0x8

    div-int/2addr v10, v11

    mul-int/lit8 v12, v10, 0x8

    add-int/2addr v12, v11

    new-array v12, v12, [B

    const/16 v13, 0x1d

    aput-byte v13, v12, v4

    const/16 v13, 0x76

    const/4 v14, 0x1

    aput-byte v13, v12, v14

    const/16 v13, 0x30

    aput-byte v13, v12, v9

    aput-byte v4, v12, v8

    rem-int/lit16 v13, v10, 0x100

    int-to-byte v13, v13

    const/4 v15, 0x4

    aput-byte v13, v12, v15

    div-int/lit16 v13, v10, 0x100

    int-to-byte v13, v13

    const/16 v16, 0x5

    aput-byte v13, v12, v16

    const/4 v13, 0x6

    aput-byte v11, v12, v13

    const/16 v17, 0x7

    aput-byte v4, v12, v17

    sub-int v14, v2, v5

    if-ge v14, v11, :cond_3

    goto :goto_3

    :cond_3
    const/16 v14, 0x8

    :goto_3
    const/4 v9, 0x0

    const/16 v18, -0x1

    const/16 v19, 0x8

    const/16 v20, -0x1

    :goto_4
    if-ge v9, v14, :cond_9

    move/from16 v13, v18

    const/4 v7, 0x0

    const/4 v8, -0x1

    :goto_5
    if-ge v7, v1, :cond_7

    const/4 v15, 0x0

    const/16 v18, 0x0

    :goto_6
    if-ge v15, v11, :cond_5

    shl-int/lit8 v11, v18, 0x1

    int-to-byte v11, v11

    add-int v4, v7, v15

    move/from16 v21, v1

    add-int v1, v5, v9

    move/from16 v22, v2

    move-object/from16 v2, p1

    invoke-virtual {v2, v4, v1}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    or-int/lit8 v1, v11, 0x1

    int-to-byte v1, v1

    move/from16 v18, v1

    goto :goto_7

    :cond_4
    move/from16 v18, v11

    :goto_7
    add-int/lit8 v15, v15, 0x1

    move/from16 v1, v21

    move/from16 v2, v22

    const/16 v11, 0x8

    goto :goto_6

    :cond_5
    move/from16 v21, v1

    move/from16 v22, v2

    move-object/from16 v2, p1

    if-eqz v18, :cond_6

    div-int/lit8 v8, v7, 0x8

    if-ge v13, v8, :cond_6

    move v13, v8

    :cond_6
    add-int/lit8 v1, v19, 0x1

    aput-byte v18, v12, v19

    add-int/lit8 v7, v7, 0x8

    move/from16 v19, v1

    move/from16 v1, v21

    move/from16 v2, v22

    const/16 v11, 0x8

    const/4 v15, 0x4

    goto :goto_5

    :cond_7
    move/from16 v21, v1

    move/from16 v22, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    if-ne v8, v1, :cond_8

    move v14, v9

    goto :goto_8

    :cond_8
    add-int/lit8 v9, v9, 0x1

    move/from16 v20, v8

    move/from16 v18, v13

    move/from16 v1, v21

    move/from16 v2, v22

    const/4 v8, 0x3

    const/16 v11, 0x8

    const/4 v13, 0x6

    const/4 v15, 0x4

    goto :goto_4

    :cond_9
    move/from16 v21, v1

    move/from16 v22, v2

    const/4 v1, -0x1

    move-object/from16 v2, p1

    move/from16 v13, v18

    move/from16 v8, v20

    :goto_8
    if-eq v13, v1, :cond_f

    if-eqz p3, :cond_b

    add-int/lit8 v13, v13, 0x1

    if-ge v13, v10, :cond_b

    rem-int/lit16 v1, v13, 0x100

    int-to-byte v1, v1

    const/4 v4, 0x4

    aput-byte v1, v12, v4

    div-int/lit16 v1, v13, 0x100

    int-to-byte v1, v1

    aput-byte v1, v12, v16

    add-int/lit8 v1, v13, 0x8

    add-int/lit8 v4, v10, 0x8

    move v7, v1

    const/4 v1, 0x1

    :goto_9
    if-ge v1, v14, :cond_a

    invoke-static {v12, v4, v12, v7, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v7, v13

    add-int/2addr v4, v10

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_a
    mul-int v13, v13, v14

    const/16 v1, 0x8

    add-int/lit8 v19, v13, 0x8

    :cond_b
    move/from16 v1, v19

    rem-int/lit16 v4, v14, 0x100

    int-to-byte v4, v4

    const/4 v7, 0x6

    aput-byte v4, v12, v7

    div-int/lit16 v4, v14, 0x100

    int-to-byte v4, v4

    aput-byte v4, v12, v17

    :goto_a
    if-eqz v6, :cond_d

    const/4 v4, 0x3

    new-array v7, v4, [B

    fill-array-data v7, :array_0

    const/16 v4, 0xff

    if-le v6, v4, :cond_c

    add-int/lit16 v4, v6, -0xff

    move v6, v4

    goto :goto_b

    :cond_c
    int-to-byte v4, v6

    const/4 v6, 0x2

    aput-byte v4, v7, v6

    const/4 v6, 0x0

    :goto_b
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_d
    if-eqz v3, :cond_e

    const/4 v4, 0x4

    new-array v4, v4, [B

    const/16 v7, 0x1b

    const/4 v9, 0x0

    aput-byte v7, v4, v9

    const/16 v7, 0x24

    const/4 v9, 0x1

    aput-byte v7, v4, v9

    rem-int/lit16 v7, v3, 0x100

    int-to-byte v7, v7

    const/4 v9, 0x2

    aput-byte v7, v4, v9

    div-int/lit16 v7, v3, 0x100

    int-to-byte v7, v7

    const/4 v9, 0x3

    aput-byte v7, v4, v9

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    const/4 v4, 0x0

    invoke-static {v12, v4, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, -0x1

    add-int/2addr v5, v14

    const/4 v1, -0x1

    if-ne v8, v1, :cond_10

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_f
    const/4 v4, 0x0

    add-int/lit8 v6, v6, 0x1

    :cond_10
    :goto_c
    const/4 v1, 0x1

    add-int/2addr v5, v1

    move/from16 v1, v21

    move/from16 v2, v22

    goto/16 :goto_2

    :cond_11
    :goto_d
    if-eqz v6, :cond_13

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_1

    const/16 v3, 0xff

    if-le v6, v3, :cond_12

    add-int/lit16 v5, v6, -0xff

    const/4 v6, 0x2

    goto :goto_e

    :cond_12
    int-to-byte v5, v6

    const/4 v6, 0x2

    aput-byte v5, v2, v6

    const/4 v5, 0x0

    :goto_e
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v6, v5

    goto :goto_d

    :cond_13
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x4at
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x4at
        -0x1t
    .end array-data
.end method

.method static g(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0x400

    if-le v1, v3, :cond_0

    const/16 v1, 0x400

    :cond_0
    const/4 v4, 0x0

    if-gez p2, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p2

    :goto_0
    if-le v5, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_2
    const/4 v8, 0x2

    const/4 v9, 0x3

    if-ge v5, v2, :cond_d

    mul-int/lit8 v10, v1, 0x3

    const/4 v11, 0x5

    add-int/2addr v10, v11

    new-array v10, v10, [B

    const/16 v12, 0x1b

    aput-byte v12, v10, v4

    const/16 v13, 0x2a

    const/4 v14, 0x1

    aput-byte v13, v10, v14

    const/16 v13, 0x21

    aput-byte v13, v10, v8

    rem-int/lit16 v13, v1, 0x100

    int-to-byte v13, v13

    aput-byte v13, v10, v9

    div-int/lit16 v13, v1, 0x100

    int-to-byte v13, v13

    const/4 v15, 0x4

    aput-byte v13, v10, v15

    sub-int v13, v2, v5

    const/16 v4, 0x18

    if-ge v13, v4, :cond_3

    int-to-byte v4, v13

    :cond_3
    const/4 v8, -0x1

    const/4 v12, 0x0

    const/16 v16, 0x5

    :goto_3
    if-ge v12, v1, :cond_7

    const-wide/16 v17, 0x0

    move/from16 v19, v4

    move/from16 v20, v8

    move-wide/from16 v7, v17

    :goto_4
    if-lez v19, :cond_5

    add-int/lit8 v19, v19, -0x1

    shr-long/2addr v7, v14

    add-int v14, v5, v19

    move-object/from16 v11, p1

    invoke-virtual {v11, v12, v14}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v14

    if-eqz v14, :cond_4

    const-wide/32 v21, 0x800000

    or-long v7, v7, v21

    :cond_4
    const/4 v11, 0x5

    const/4 v14, 0x1

    goto :goto_4

    :cond_5
    move-object/from16 v11, p1

    cmp-long v14, v7, v17

    if-eqz v14, :cond_6

    move/from16 v20, v12

    :cond_6
    add-int/lit8 v14, v16, 0x1

    const/16 v17, 0x10

    move/from16 v19, v14

    shr-long v13, v7, v17

    long-to-int v14, v13

    int-to-byte v13, v14

    aput-byte v13, v10, v16

    add-int/lit8 v14, v19, 0x1

    const/16 v13, 0x8

    move-object/from16 v21, v10

    shr-long v9, v7, v13

    long-to-int v10, v9

    int-to-byte v9, v10

    aput-byte v9, v21, v19

    add-int/lit8 v16, v14, 0x1

    long-to-int v8, v7

    int-to-byte v7, v8

    aput-byte v7, v21, v14

    add-int/lit8 v12, v12, 0x1

    move/from16 v8, v20

    move-object/from16 v10, v21

    const/4 v9, 0x3

    const/4 v11, 0x5

    const/4 v14, 0x1

    goto :goto_3

    :cond_7
    move-object/from16 v11, p1

    move v13, v8

    move-object/from16 v21, v10

    const/4 v7, -0x1

    if-eq v13, v7, :cond_c

    if-eqz p3, :cond_8

    add-int/lit8 v8, v13, 0x1

    if-ge v8, v1, :cond_8

    rem-int/lit16 v7, v8, 0x100

    int-to-byte v7, v7

    const/4 v9, 0x3

    aput-byte v7, v21, v9

    div-int/lit16 v7, v8, 0x100

    int-to-byte v7, v7

    aput-byte v7, v21, v15

    mul-int/lit8 v8, v8, 0x3

    const/4 v7, 0x5

    add-int/lit8 v16, v8, 0x5

    :cond_8
    move/from16 v7, v16

    :goto_5
    if-eqz v6, :cond_a

    const/4 v8, 0x3

    new-array v9, v8, [B

    fill-array-data v9, :array_0

    const/16 v8, 0xff

    if-le v6, v8, :cond_9

    add-int/lit16 v6, v6, -0xff

    goto :goto_6

    :cond_9
    int-to-byte v6, v6

    const/4 v8, 0x2

    aput-byte v6, v9, v8

    const/4 v6, 0x0

    :goto_6
    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_a
    if-eqz v3, :cond_b

    new-array v8, v15, [B

    const/16 v9, 0x1b

    const/4 v10, 0x0

    aput-byte v9, v8, v10

    const/16 v9, 0x24

    const/4 v10, 0x1

    aput-byte v9, v8, v10

    rem-int/lit16 v9, v3, 0x100

    int-to-byte v9, v9

    const/4 v10, 0x2

    aput-byte v9, v8, v10

    div-int/lit16 v9, v3, 0x100

    int-to-byte v9, v9

    const/4 v10, 0x3

    aput-byte v9, v8, v10

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_b
    const/4 v10, 0x3

    :goto_7
    move-object/from16 v8, v21

    const/4 v9, 0x0

    invoke-static {v8, v9, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v7, v10, [B

    const/16 v8, 0x1b

    aput-byte v8, v7, v9

    const/16 v8, 0x4a

    const/4 v10, 0x1

    aput-byte v8, v7, v10

    const/4 v8, 0x2

    aput-byte v4, v7, v8

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_c
    const/4 v9, 0x0

    add-int/lit8 v6, v6, 0x18

    :goto_8
    add-int/lit8 v5, v5, 0x18

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_d
    const/4 v9, 0x0

    :goto_9
    if-eqz v6, :cond_f

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_1

    const/16 v3, 0xff

    if-le v6, v3, :cond_e

    add-int/lit16 v4, v6, -0xff

    move v6, v4

    const/4 v5, 0x2

    goto :goto_a

    :cond_e
    int-to-byte v4, v6

    const/4 v5, 0x2

    aput-byte v4, v2, v5

    const/4 v6, 0x0

    :goto_a
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_f
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x33t
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x33t
        -0x1t
    .end array-data
.end method

.method static h(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/j;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->b()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/starmicronics/starioextension/j;->a()I

    move-result v2

    const/16 v3, 0xff

    if-le v1, v3, :cond_0

    const/16 v1, 0xff

    :cond_0
    const/4 v4, 0x0

    if-gez p2, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move/from16 v5, p2

    :goto_0
    if-le v5, v3, :cond_2

    const/16 v5, 0xff

    :cond_2
    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_1
    const/4 v8, 0x3

    const/4 v9, 0x2

    if-ge v6, v2, :cond_e

    add-int/lit8 v10, v1, 0x4

    new-array v10, v10, [B

    const/16 v11, 0x1b

    aput-byte v11, v10, v4

    const/16 v12, 0x4b

    const/4 v13, 0x1

    aput-byte v12, v10, v13

    int-to-byte v12, v1

    aput-byte v12, v10, v9

    aput-byte v4, v10, v8

    sub-int v12, v2, v6

    const/16 v14, 0x8

    if-ge v12, v14, :cond_3

    goto :goto_2

    :cond_3
    const/16 v12, 0x8

    :goto_2
    const/4 v15, -0x1

    const/16 v16, 0x4

    const/4 v4, -0x1

    const/4 v13, 0x0

    const/16 v17, 0x4

    :goto_3
    if-ge v13, v1, :cond_8

    const/4 v11, 0x0

    const/16 v18, 0x0

    :goto_4
    if-ge v11, v12, :cond_5

    shl-int/lit8 v3, v18, 0x1

    int-to-byte v3, v3

    add-int v8, v6, v11

    move-object/from16 v9, p1

    invoke-virtual {v9, v13, v8}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v8

    if-eqz v8, :cond_4

    or-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    :cond_4
    move/from16 v18, v3

    add-int/lit8 v11, v11, 0x1

    const/16 v3, 0xff

    const/4 v8, 0x3

    const/4 v9, 0x2

    goto :goto_4

    :cond_5
    move-object/from16 v9, p1

    if-ge v12, v14, :cond_6

    rsub-int/lit8 v3, v12, 0x8

    shl-int v3, v18, v3

    int-to-byte v3, v3

    goto :goto_5

    :cond_6
    move/from16 v3, v18

    :goto_5
    if-eqz v3, :cond_7

    move v4, v13

    :cond_7
    add-int/lit8 v8, v17, 0x1

    aput-byte v3, v10, v17

    add-int/lit8 v13, v13, 0x1

    move/from16 v17, v8

    const/16 v3, 0xff

    const/4 v8, 0x3

    const/4 v9, 0x2

    const/16 v11, 0x1b

    goto :goto_3

    :cond_8
    move-object/from16 v9, p1

    if-eq v4, v15, :cond_d

    if-eqz p3, :cond_9

    add-int/lit8 v4, v4, 0x1

    if-ge v4, v1, :cond_9

    int-to-byte v3, v4

    const/4 v8, 0x2

    aput-byte v3, v10, v8

    add-int/lit8 v17, v4, 0x4

    :cond_9
    move/from16 v3, v17

    :goto_6
    if-eqz v7, :cond_b

    const/4 v4, 0x3

    new-array v8, v4, [B

    fill-array-data v8, :array_0

    const/16 v4, 0xff

    if-le v7, v4, :cond_a

    add-int/lit16 v4, v7, -0xff

    move v7, v4

    const/4 v11, 0x2

    goto :goto_7

    :cond_a
    int-to-byte v4, v7

    const/4 v11, 0x2

    aput-byte v4, v8, v11

    const/4 v7, 0x0

    :goto_7
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_b
    const/4 v11, 0x2

    if-eqz v5, :cond_c

    const/4 v4, 0x5

    new-array v4, v4, [B

    const/16 v8, 0x1b

    const/4 v13, 0x0

    aput-byte v8, v4, v13

    const/16 v8, 0x1d

    const/4 v13, 0x1

    aput-byte v8, v4, v13

    const/16 v8, 0x41

    aput-byte v8, v4, v11

    rem-int/lit16 v8, v5, 0x100

    int-to-byte v8, v8

    const/4 v11, 0x3

    aput-byte v8, v4, v11

    div-int/lit16 v8, v5, 0x100

    int-to-byte v8, v8

    aput-byte v8, v4, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_c
    const/4 v11, 0x3

    :goto_8
    const/4 v4, 0x0

    invoke-static {v10, v4, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v3, v11, [B

    const/16 v8, 0x1b

    aput-byte v8, v3, v4

    const/16 v8, 0x4a

    const/4 v10, 0x1

    aput-byte v8, v3, v10

    int-to-byte v8, v12

    const/4 v10, 0x2

    aput-byte v8, v3, v10

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_d
    const/4 v4, 0x0

    add-int/lit8 v7, v7, 0x8

    :goto_9
    add-int/lit8 v6, v6, 0x8

    const/16 v3, 0xff

    goto/16 :goto_1

    :cond_e
    :goto_a
    if-eqz v7, :cond_10

    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_1

    const/16 v3, 0xff

    if-le v7, v3, :cond_f

    add-int/lit16 v5, v7, -0xff

    move v7, v5

    const/4 v6, 0x2

    goto :goto_b

    :cond_f
    int-to-byte v5, v7

    const/4 v6, 0x2

    aput-byte v5, v2, v6

    const/4 v7, 0x0

    :goto_b
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_10
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x4at
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x4at
        -0x1t
    .end array-data
.end method
