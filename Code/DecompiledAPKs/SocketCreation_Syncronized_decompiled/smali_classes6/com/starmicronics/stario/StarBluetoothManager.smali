.class public Lcom/starmicronics/stario/StarBluetoothManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/stario/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;,
        Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;,
        Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    }
.end annotation


# static fields
.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private a:Lcom/starmicronics/stario/c;

.field private d:I

.field private e:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    const/16 v0, 0x2710

    iput v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->d:I

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    iput-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->e:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypePortablePrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    if-ne p4, v0, :cond_0

    new-instance v0, Lcom/starmicronics/stario/d;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/starmicronics/stario/d;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/b;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/starmicronics/stario/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V

    :goto_0
    iput-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    const-string v0, "BT:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sput-object p1, Lcom/starmicronics/stario/StarBluetoothManager;->b:Ljava/lang/String;

    sput-object p2, Lcom/starmicronics/stario/StarBluetoothManager;->c:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/StarBluetoothManager;->d:I

    iput-object p4, p0, Lcom/starmicronics/stario/StarBluetoothManager;->e:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "This fuction is available form the bluetooth interface."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public apply()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->apply()V

    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->close()V

    return-void
.end method

.method public getAutoConnect()Z
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getAutoConnect()Z

    move-result v0

    return v0
.end method

.method public getAutoConnectCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getAutoConnectCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getBluetoothDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothDeviceNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getBluetoothDeviceNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothDiagnosticMode()Z
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getBluetoothDiagnosticMode()Z

    move-result v0

    return v0
.end method

.method public getBluetoothDiagnosticModeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getBluetoothDiagnosticModeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceType()Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->e:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-object v0
.end method

.method public getDiscoveryPermission()Z
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getDiscoveryPermission()Z

    move-result v0

    return v0
.end method

.method public getDiscoveryPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getDiscoveryPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getPairingPermission()Z
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getPairingPermission()Z

    move-result v0

    return v0
.end method

.method public getPairingPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getPairingPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getPinCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getPinCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPinCodeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getPinCodeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getPortName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getPortSettings()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSecurityType()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getSecurityType()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    move-result-object v0

    return-object v0
.end method

.method public getSecurityTypeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getSecurityTypeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public getTimeoutMillis()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->d:I

    return v0
.end method

.method public getiOSPortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getiOSPortName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getiOSPortNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->getiOSPortNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    move-result-object v0

    return-object v0
.end method

.method public isOpened()Z
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->isOpened()Z

    move-result v0

    return v0
.end method

.method public loadSetting()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->loadSetting()V

    return-void
.end method

.method public open()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0}, Lcom/starmicronics/stario/c;->open()V

    return-void
.end method

.method public setAutoConnect(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setAutoConnect(Z)V

    return-void
.end method

.method public setBluetoothDeviceName(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setBluetoothDeviceName(Ljava/lang/String;)V

    return-void
.end method

.method public setBluetoothDiagnosticMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setBluetoothDiagnosticMode(Z)V

    return-void
.end method

.method public setDiscoveryPermission(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setDiscoveryPermission(Z)V

    return-void
.end method

.method public setPairingPermission(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setPairingPermission(Z)V

    return-void
.end method

.method public setPinCode(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setPinCode(Ljava/lang/String;)V

    return-void
.end method

.method public setSecurityType(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setSecurityType(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V

    return-void
.end method

.method public setiOSPortName(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/StarBluetoothManager;->a:Lcom/starmicronics/stario/c;

    invoke-interface {v0, p1}, Lcom/starmicronics/stario/c;->setiOSPortName(Ljava/lang/String;)V

    return-void
.end method
