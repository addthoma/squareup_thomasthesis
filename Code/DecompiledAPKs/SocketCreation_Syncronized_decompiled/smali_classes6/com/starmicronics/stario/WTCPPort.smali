.class Lcom/starmicronics/stario/WTCPPort;
.super Lcom/starmicronics/stario/StarIOPort;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/WTCPPort$a;
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field private c:Ljava/net/Socket;

.field private d:Ljava/io/DataOutputStream;

.field private e:Ljava/io/DataInputStream;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Z

.field private final m:Ljava/lang/Object;

.field private n:Z

.field private o:J


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "Firmware check firmware"

    invoke-direct {p0}, Lcom/starmicronics/stario/StarIOPort;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->h:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/starmicronics/stario/WTCPPort;->i:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->j:Z

    const-string v3, ""

    iput-object v3, p0, Lcom/starmicronics/stario/WTCPPort;->k:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/starmicronics/stario/WTCPPort;->l:Z

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/starmicronics/stario/WTCPPort;->m:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->n:Z

    iput-object p1, p0, Lcom/starmicronics/stario/WTCPPort;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/starmicronics/stario/WTCPPort;->b:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    iput p3, p0, Lcom/starmicronics/stario/WTCPPort;->g:I

    const-string p1, ";"

    invoke-virtual {p2, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length p2, p1

    const/4 p3, 0x0

    :goto_0
    if-ge p3, p2, :cond_3

    aget-object v3, p1, p3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v2, :cond_2

    const-string v4, "a"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v2, :cond_0

    iput-boolean v2, p0, Lcom/starmicronics/stario/WTCPPort;->h:Z

    goto :goto_1

    :cond_0
    const-string v4, "n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v2, :cond_1

    iput-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->i:Z

    goto :goto_1

    :cond_1
    const-string v4, "z"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v2, :cond_2

    iput-boolean v2, p0, Lcom/starmicronics/stario/WTCPPort;->j:Z

    :cond_2
    :goto_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->b()V

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iget p3, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    int-to-long v3, p3

    add-long/2addr p1, v3

    :goto_2
    iget-object p3, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-static {}, Lcom/starmicronics/stario/f;->c()[B

    move-result-object v3

    invoke-static {}, Lcom/starmicronics/stario/f;->c()[B

    move-result-object v4

    array-length v4, v4

    invoke-virtual {p3, v3, v1, v4}, Ljava/io/DataOutputStream;->write([BII)V

    const/16 p3, 0x64

    new-array v3, p3, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    const-wide/16 v4, 0x64

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v6, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->available()I

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    array-length v7, v3

    sub-int/2addr v7, v1

    invoke-virtual {v6, v3, v1, v7}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    add-int/2addr v3, v1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    :goto_3
    if-lez v3, :cond_8

    iget-object p1, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-static {}, Lcom/starmicronics/stario/f;->a()[B

    move-result-object p2

    const/4 v3, 0x2

    invoke-virtual {p1, p2, v1, v3}, Ljava/io/DataOutputStream;->write([BII)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iget v6, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    int-to-long v6, v6

    add-long/2addr p1, v6

    new-array p3, p3, [B

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v10, p1, v8

    if-lez v10, :cond_7

    iget-object v8, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    array-length v9, p3

    sub-int/2addr v9, v6

    invoke-virtual {v8, p3, v6, v9}, Ljava/io/DataInputStream;->read([BII)I

    move-result v8

    add-int/2addr v6, v8

    :goto_5
    if-gt v7, v6, :cond_5

    const/16 v8, 0x30

    aget-byte v9, p3, v7

    if-gt v8, v9, :cond_5

    aget-byte v8, p3, v7

    const/16 v9, 0x3f

    if-gt v8, v9, :cond_5

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_5
    array-length v8, p3

    new-array v8, v8, [B

    sub-int v9, v6, v7

    invoke-static {p3, v7, v8, v1, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-array v9, v3, [B

    const/16 v10, 0xa

    aput-byte v10, v9, v1

    aput-byte v1, v9, v2

    new-array v10, v1, [B

    invoke-static {v8, v10, v9}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object v8

    if-eqz v8, :cond_6

    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v8}, Ljava/lang/String;-><init>([B)V

    iput-object p1, p0, Lcom/starmicronics/stario/WTCPPort;->k:Ljava/lang/String;

    iget-object p1, p0, Lcom/starmicronics/stario/WTCPPort;->k:Ljava/lang/String;

    invoke-static {}, Lcom/starmicronics/stario/f;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    return-void

    :cond_6
    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_4

    :catch_0
    :try_start_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    invoke-virtual {p0}, Lcom/starmicronics/stario/WTCPPort;->a()V

    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Firmware check failed"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long p3, p1, v3

    if-ltz p3, :cond_9

    goto/16 :goto_2

    :cond_9
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    invoke-direct {p1}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw p1

    :catch_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "call-version"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    invoke-virtual {p0}, Lcom/starmicronics/stario/WTCPPort;->a()V

    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "getPort: call-version timeout"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_3
    invoke-virtual {p0}, Lcom/starmicronics/stario/WTCPPort;->a()V

    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic a(Lcom/starmicronics/stario/WTCPPort;J)J
    .locals 0

    iput-wide p1, p0, Lcom/starmicronics/stario/WTCPPort;->o:J

    return-wide p1
.end method

.method private a(I)Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->b()V

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [B

    const/16 v1, 0x1b

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    const/16 v1, 0x76

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    array-length v4, v0

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/DataOutputStream;->write([BII)V

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :catch_0
    :goto_0
    new-instance v4, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v4}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v5, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->available()I

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    iget-object v6, v4, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    invoke-virtual {v5, v6, v2, v3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-ne v5, v3, :cond_2

    invoke-static {v4}, Lcom/starmicronics/stario/f;->b(Lcom/starmicronics/stario/StarPrinterStatus;)V

    return-object v4

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    sub-long/2addr v4, v0

    int-to-long v6, p1

    cmp-long v8, v4, v6

    if-gtz v8, :cond_3

    const-wide/16 v4, 0xc8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_3
    :try_start_2
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    const-string v0, "There was no response of the printer within the timeout period."

    invoke-direct {p1, v0}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get parsed status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception p1

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/starmicronics/stario/WTCPPort;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/starmicronics/stario/WTCPPort;->n:Z

    return p0
.end method

.method static synthetic b(Lcom/starmicronics/stario/WTCPPort;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/WTCPPort;->m:Ljava/lang/Object;

    return-object p0
.end method

.method private declared-synchronized b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->a:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/net/InetSocketAddress;

    const/16 v2, 0x238c

    invoke-direct {v1, v0, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    iget v2, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    invoke-virtual {v0, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    iget v2, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    invoke-virtual {v0, v1, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    iget v1, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    iget-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->h:Z

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    iget-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->i:Z

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/DataInputStream;

    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Cannot connect to printer"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;,
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x2710

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x1

    new-array v2, v2, [B

    :goto_1
    :try_start_0
    array-length v3, v2

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v3}, Lcom/starmicronics/stario/WTCPPort;->readPort([BII)I

    move-result v3
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_1

    if-lez v3, :cond_2

    aget-byte v3, v2, v4

    const/16 v5, 0x26

    if-eq v3, v5, :cond_1

    aget-byte v3, v2, v4

    const/16 v4, 0x27

    if-ne v3, v4, :cond_2

    :cond_1
    return-void

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    int-to-long v5, p1

    cmp-long v7, v3, v5

    if-gtz v7, :cond_3

    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "called interrupt() during Thread.sleep()"

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    const-string v0, "There was no response of the printer within the timeout period."

    invoke-direct {p1, v0}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic c(Lcom/starmicronics/stario/WTCPPort;)J
    .locals 2

    iget-wide v0, p0, Lcom/starmicronics/stario/WTCPPort;->o:J

    return-wide v0
.end method

.method private c()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "Failed to get parsed status"

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->b()V

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_3

    const/4 v3, 0x3

    new-array v3, v3, [B

    const/16 v4, 0x10

    aput-byte v4, v3, v1

    const/4 v4, 0x4

    const/4 v5, 0x1

    aput-byte v4, v3, v5

    const/4 v6, 0x2

    aput-byte v4, v3, v6

    iget-object v4, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    array-length v6, v3

    invoke-virtual {v4, v3, v1, v6}, Ljava/io/DataOutputStream;->write([BII)V

    iget-object v3, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->available()I

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    const-string v4, "Firmware check firmware"

    const-wide/16 v6, 0x3e8

    if-nez v3, :cond_1

    add-int/lit8 v2, v2, 0x1

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    :try_start_2
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v3, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v3}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v8, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    iget-object v9, v3, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    invoke-virtual {v8, v9, v1, v5}, Ljava/io/DataInputStream;->read([BII)I

    move-result v8
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eq v8, v5, :cond_2

    add-int/lit8 v2, v2, 0x1

    :try_start_3
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_1
    :try_start_4
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v3}, Lcom/starmicronics/stario/f;->b(Lcom/starmicronics/stario/StarPrinterStatus;)V

    return-object v3

    :cond_3
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic d(Lcom/starmicronics/stario/WTCPPort;)Ljava/io/DataOutputStream;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    return-object p0
.end method

.method private declared-synchronized internalWritePort([BIII)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->b()V

    :cond_0
    new-instance v1, Lcom/starmicronics/stario/WTCPPort$a;

    invoke-direct {v1, p0, p4}, Lcom/starmicronics/stario/WTCPPort$a;-><init>(Lcom/starmicronics/stario/WTCPPort;I)V

    const/4 p4, 0x1

    iput-boolean p4, p0, Lcom/starmicronics/stario/WTCPPort;->n:Z

    invoke-virtual {v1}, Lcom/starmicronics/stario/WTCPPort$a;->start()V

    move p4, p2

    const/4 p2, 0x0

    :goto_0
    if-ge p2, p3, :cond_2

    sub-int v1, p3, p2

    const/16 v2, 0x400

    if-ge v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x400

    :goto_1
    iget-object v2, p0, Lcom/starmicronics/stario/WTCPPort;->m:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/starmicronics/stario/WTCPPort;->o:J

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1, p4, v1}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int p4, p2, v1

    move p2, p4

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    :try_start_5
    iput-boolean v0, p0, Lcom/starmicronics/stario/WTCPPort;->n:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_0
    :try_start_6
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_2
    :try_start_7
    iput-boolean v0, p0, Lcom/starmicronics/stario/WTCPPort;->n:Z

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method protected a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    const/16 v0, 0xc8

    new-array v1, v0, [B

    :cond_0
    iget-object v2, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Ljava/io/DataInputStream;->read([BII)I

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v0, :cond_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    :try_start_2
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    nop

    :goto_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    nop

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_3

    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    :catch_3
    nop

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    if-eqz v0, :cond_6

    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    goto :goto_6

    :catchall_0
    move-exception v0

    goto :goto_7

    :catch_4
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    if-eqz v0, :cond_4

    :try_start_7
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_4

    :catch_5
    nop

    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_5

    :try_start_8
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_5

    :catch_6
    nop

    :cond_5
    :goto_5
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    if-eqz v0, :cond_6

    goto :goto_3

    :catch_7
    :cond_6
    :goto_6
    return-void

    :goto_7
    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    if-eqz v1, :cond_7

    :try_start_9
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    goto :goto_8

    :catch_8
    nop

    :cond_7
    :goto_8
    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_8

    :try_start_a
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_9

    :catch_9
    nop

    :cond_8
    :goto_9
    iget-object v1, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    if-eqz v1, :cond_9

    :try_start_b
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    :catch_a
    :cond_9
    throw v0
.end method

.method public beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->c()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/starmicronics/stario/WTCPPort;->getFirmwareInformation()Ljava/util/Map;

    move-result-object v1

    const-string v2, "FirmwareVersion"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    const v2, 0x4019999a    # 2.4f

    const/4 v3, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->l:Z

    iget-boolean v1, p0, Lcom/starmicronics/stario/WTCPPort;->l:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x6

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    new-array v1, v1, [B

    fill-array-data v1, :array_1

    array-length v4, v2

    array-length v5, v1

    add-int/2addr v4, v5

    new-array v4, v4, [B

    array-length v5, v2

    invoke-static {v2, v3, v4, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v5, v2

    array-length v6, v1

    invoke-static {v1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v4

    invoke-virtual {p0, v4, v3, v1}, Lcom/starmicronics/stario/WTCPPort;->writePort([BII)V

    :try_start_0
    iget v1, p0, Lcom/starmicronics/stario/WTCPPort;->f:I

    invoke-direct {p0, v1}, Lcom/starmicronics/stario/WTCPPort;->b(I)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    array-length v1, v2

    invoke-virtual {p0, v2, v3, v1}, Lcom/starmicronics/stario/WTCPPort;->writePort([BII)V

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Printer does not respond."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Printer is offline"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x15t
        0x5t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x1ct
        0x15t
        0x6t
        0x0t
        0x0t
    .end array-data
.end method

.method public endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/starmicronics/stario/WTCPPort;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lcom/starmicronics/stario/WTCPPort;->writePort([BII)V

    :try_start_0
    iget v0, p0, Lcom/starmicronics/stario/WTCPPort;->g:I

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/WTCPPort;->b(I)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->c()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/starmicronics/stario/WTCPPort;->g:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_1

    iget v1, p0, Lcom/starmicronics/stario/WTCPPort;->g:I

    :cond_1
    invoke-direct {p0, v1}, Lcom/starmicronics/stario/WTCPPort;->a(I)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_2

    return-object v0

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x15t
        0x6t
        0x0t
        0x0t
    .end array-data
.end method

.method public getDipSwitchInformation()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This model is not supported this method."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFirmwareInformation()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/starmicronics/stario/f;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getPortName()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPortSettings()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public readPort([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->b()V

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->e:Ljava/io/DataInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    const/4 p1, 0x0

    :cond_1
    return p1

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to read"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->c()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    return-object v0
.end method

.method public setEndCheckedBlockTimeoutMillis(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/stario/WTCPPort;->g:I

    return-void
.end method

.method public writePort([BII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/WTCPPort;->b()V

    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x400

    if-ge v1, p3, :cond_2

    const/16 v2, 0x400

    :goto_0
    if-ge v0, p3, :cond_3

    iget-object v3, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v3, p1, p2, v2}, Ljava/io/DataOutputStream;->write([BII)V

    add-int p2, v0, v2

    sub-int v0, p3, p2

    if-ge v0, v1, :cond_1

    move v2, v0

    :cond_1
    move v0, p2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/stario/WTCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    return-void

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
