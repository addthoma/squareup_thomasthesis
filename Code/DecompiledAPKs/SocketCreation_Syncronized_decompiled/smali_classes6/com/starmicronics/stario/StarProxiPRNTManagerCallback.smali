.class public interface abstract Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/StarProxiPRNTManagerCallback$StarDeviceType;
    }
.end annotation


# virtual methods
.method public abstract onClosestCashDrawerFound(Ljava/lang/String;)V
.end method

.method public abstract onClosestPrinterFound(Ljava/lang/String;)V
.end method

.method public abstract onPortDiscovered(Ljava/lang/String;Lcom/starmicronics/stario/StarProxiPRNTManagerCallback$StarDeviceType;Ljava/lang/String;I)V
.end method

.method public abstract onStateUpdated(Ljava/lang/String;Ljava/lang/String;)V
.end method
