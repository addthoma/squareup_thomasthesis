.class public final Lcom/squareup/Money;
.super Ljava/lang/Object;
.source "Money.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private cents:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private centsLong:J

.field private currency:Lcom/squareup/Currency;


# direct methods
.method constructor <init>(JLcom/squareup/Currency;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p3, p0, Lcom/squareup/Money;->currency:Lcom/squareup/Currency;

    .line 26
    iput-wide p1, p0, Lcom/squareup/Money;->centsLong:J

    return-void
.end method

.method public static launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;
    .locals 2

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    if-nez p1, :cond_1

    const/4 p0, 0x0

    return-object p0

    .line 39
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/Money;->currencyProto()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    if-nez p0, :cond_2

    .line 40
    sget-object p0, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    .line 42
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/Money;->cents()J

    move-result-wide v0

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 59
    const-class v0, Lcom/squareup/Money;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 63
    iget p1, p0, Lcom/squareup/Money;->cents:I

    if-eqz p1, :cond_0

    :try_start_0
    const-string v1, "centsLong"

    .line 66
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p0, v0, v1, p1}, Lcom/squareup/util/Objects;->setField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    const-string p1, "cents"

    const/4 v1, 0x0

    .line 67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v0, p1, v1}, Lcom/squareup/util/Objects;->setField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_1

    .line 73
    :goto_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 71
    :goto_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 79
    :cond_0
    :goto_2
    iget-object p1, p0, Lcom/squareup/Money;->currency:Lcom/squareup/Currency;

    if-nez p1, :cond_1

    :try_start_1
    const-string p1, "currency"

    .line 82
    sget-object v1, Lcom/squareup/Currency;->USD:Lcom/squareup/Currency;

    invoke-static {p0, v0, p1, v1}, Lcom/squareup/util/Objects;->setField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_5

    :catch_2
    move-exception p1

    goto :goto_3

    :catch_3
    move-exception p1

    goto :goto_4

    .line 88
    :goto_3
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 86
    :goto_4
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :goto_5
    return-void
.end method


# virtual methods
.method public cents()J
    .locals 2

    .line 46
    iget-wide v0, p0, Lcom/squareup/Money;->centsLong:J

    return-wide v0
.end method

.method public currency()Lcom/squareup/Currency;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/Money;->currency:Lcom/squareup/Currency;

    return-object v0
.end method

.method public currencyProto()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/Money;->currency:Lcom/squareup/Currency;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/Currency;->toProto()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    :goto_0
    return-object v0
.end method
