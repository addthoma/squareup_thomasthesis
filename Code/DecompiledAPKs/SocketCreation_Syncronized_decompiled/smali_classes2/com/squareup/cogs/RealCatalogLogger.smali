.class public Lcom/squareup/cogs/RealCatalogLogger;
.super Ljava/lang/Object;
.source "RealCatalogLogger.java"

# interfaces
.implements Lcom/squareup/shared/catalog/logging/CatalogLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 10
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 18
    invoke-static {p1, p2, v0}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public remoteLog(Ljava/lang/Throwable;)V
    .locals 0

    .line 22
    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method public remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method public warn(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 14
    invoke-static {p1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
