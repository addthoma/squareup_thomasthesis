.class public Lcom/squareup/cogs/CogsLockScoped;
.super Ljava/lang/Object;
.source "CogsLockScoped.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

.field private final cogs:Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cogs/CogsLockScoped;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 20
    iget-object p1, p0, Lcom/squareup/cogs/CogsLockScoped;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {p1}, Lcom/squareup/cogs/Cogs;->preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/CogsLockScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    return-void
.end method

.method public onExitScope()V
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/squareup/cogs/CogsLockScoped;->cogs:Lcom/squareup/cogs/Cogs;

    iget-object v1, p0, Lcom/squareup/cogs/CogsLockScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lcom/squareup/cogs/CogsLockScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    .line 28
    iget-object v0, p0, Lcom/squareup/cogs/CogsLockScoped;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method
