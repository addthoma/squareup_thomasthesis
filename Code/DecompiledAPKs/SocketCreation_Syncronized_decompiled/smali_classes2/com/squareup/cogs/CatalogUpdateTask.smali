.class public abstract Lcom/squareup/cogs/CatalogUpdateTask;
.super Ljava/lang/Object;
.source "CatalogUpdateTask.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/cogs/CatalogUpdateTask;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/cogs/CatalogUpdateTask;->update(Lcom/squareup/shared/catalog/Catalog$Local;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public abstract update(Lcom/squareup/shared/catalog/Catalog$Local;)V
.end method
