.class public final Lcom/squareup/cogs/CogsBuilder;
.super Ljava/lang/Object;
.source "CogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;,
        Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

.field private final applicationContext:Landroid/app/Application;

.field private final clock:Lcom/squareup/util/Clock;

.field private final connectV2Endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

.field private final endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final httpThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mainScheduler:Lrx/Scheduler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final messageHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

.field private final persistent:Lcom/squareup/persistent/PersistentFactory;

.field private final supportedConnectV2ObjectTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field private final syntheticTableReaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;"
        }
    .end annotation
.end field

.field private final userDir:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/persistent/PersistentFactory;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/util/Clock;Landroid/app/Application;Lcom/squareup/badbus/BadEventSink;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogEndpoint;",
            "Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;",
            "Lrx/Scheduler;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/persistent/PersistentFactory;",
            "Lcom/squareup/FileThreadEnforcer;",
            "Ljava/io/File;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/logging/CatalogAnalytics;",
            "Lcom/squareup/util/Clock;",
            "Landroid/app/Application;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cogs/CogsBuilder;->syntheticTableReaders:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/squareup/cogs/CogsBuilder;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 59
    iput-object p3, p0, Lcom/squareup/cogs/CogsBuilder;->mainScheduler:Lrx/Scheduler;

    .line 60
    iput-object p4, p0, Lcom/squareup/cogs/CogsBuilder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 61
    iput-object p5, p0, Lcom/squareup/cogs/CogsBuilder;->persistent:Lcom/squareup/persistent/PersistentFactory;

    .line 62
    iput-object p6, p0, Lcom/squareup/cogs/CogsBuilder;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    .line 63
    iput-object p7, p0, Lcom/squareup/cogs/CogsBuilder;->userDir:Ljava/io/File;

    .line 64
    new-instance p3, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;

    const/4 p5, 0x0

    invoke-direct {p3, p5}, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;-><init>(Lcom/squareup/cogs/CogsBuilder$1;)V

    invoke-static {p3}, Lcom/squareup/thread/executor/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/cogs/CogsBuilder;->httpThreadExecutor:Ljava/util/concurrent/Executor;

    .line 65
    new-instance p3, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;

    iget-object p5, p0, Lcom/squareup/cogs/CogsBuilder;->httpThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {p3, p1, p4, p8, p5}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;-><init>(Lcom/squareup/shared/catalog/CatalogEndpoint;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object p3, p0, Lcom/squareup/cogs/CogsBuilder;->messageHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

    .line 67
    iput-object p8, p0, Lcom/squareup/cogs/CogsBuilder;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    .line 68
    iput-object p9, p0, Lcom/squareup/cogs/CogsBuilder;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    .line 69
    iput-object p10, p0, Lcom/squareup/cogs/CogsBuilder;->clock:Lcom/squareup/util/Clock;

    .line 70
    iput-object p11, p0, Lcom/squareup/cogs/CogsBuilder;->applicationContext:Landroid/app/Application;

    .line 71
    iput-object p12, p0, Lcom/squareup/cogs/CogsBuilder;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 72
    invoke-static {p13}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/CogsBuilder;->supportedConnectV2ObjectTypes:Ljava/util/List;

    .line 73
    iput-object p2, p0, Lcom/squareup/cogs/CogsBuilder;->connectV2Endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cogs/CogsBuilder;)Lcom/squareup/util/Clock;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/cogs/CogsBuilder;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/cogs/RealCogs;
    .locals 25

    move-object/from16 v0, p0

    const-string v1, "Cannot get a COGS reference from outside the main thread."

    .line 82
    invoke-static {v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    .line 84
    new-instance v1, Lcom/squareup/cogs/CogsBuilder$1;

    move-object/from16 v20, v1

    invoke-direct {v1, v0}, Lcom/squareup/cogs/CogsBuilder$1;-><init>(Lcom/squareup/cogs/CogsBuilder;)V

    .line 94
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/squareup/cogs/CogsBuilder;->syntheticTableReaders:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 95
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    move-object v11, v5

    .line 96
    new-instance v4, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;

    move-object/from16 v21, v4

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->eventSink:Lcom/squareup/badbus/BadEventSink;

    iget-object v3, v0, Lcom/squareup/cogs/CogsBuilder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-direct {v4, v2, v3}, Lcom/squareup/cogs/RealCatalogUpdateDispatcher;-><init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/thread/executor/MainThread;)V

    .line 98
    new-instance v2, Lcom/squareup/cogs/SqliteCatalogStoreFactory;

    move-object v9, v2

    iget-object v3, v0, Lcom/squareup/cogs/CogsBuilder;->applicationContext:Landroid/app/Application;

    iget-object v7, v0, Lcom/squareup/cogs/CogsBuilder;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    move-object v6, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/cogs/SqliteCatalogStoreFactory;-><init>(Landroid/app/Application;Lcom/squareup/cogs/RealCatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/FileThreadEnforcer;)V

    .line 102
    new-instance v1, Lcom/squareup/cogs/RealCogs;

    move-object v8, v1

    new-instance v2, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;

    move-object v10, v2

    iget-object v3, v0, Lcom/squareup/cogs/CogsBuilder;->persistent:Lcom/squareup/persistent/PersistentFactory;

    invoke-direct {v2, v3}, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;-><init>(Lcom/squareup/persistent/PersistentFactory;)V

    iget-object v12, v0, Lcom/squareup/cogs/CogsBuilder;->mainScheduler:Lrx/Scheduler;

    iget-object v13, v0, Lcom/squareup/cogs/CogsBuilder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v14, v0, Lcom/squareup/cogs/CogsBuilder;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    iget-object v15, v0, Lcom/squareup/cogs/CogsBuilder;->userDir:Ljava/io/File;

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    move-object/from16 v16, v2

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->messageHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

    move-object/from16 v17, v2

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v18, v2

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    move-object/from16 v19, v2

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->supportedConnectV2ObjectTypes:Ljava/util/List;

    move-object/from16 v22, v2

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->connectV2Endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    move-object/from16 v23, v2

    iget-object v2, v0, Lcom/squareup/cogs/CogsBuilder;->httpThreadExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v24, v2

    invoke-direct/range {v8 .. v24}, Lcom/squareup/cogs/RealCogs;-><init>(Lcom/squareup/cogs/SqliteCatalogStoreFactory;Lcom/squareup/shared/catalog/CatalogFile;Ljava/util/List;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;)V

    return-object v1
.end method

.method public registerSyntheticTableReader(Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;)Lcom/squareup/cogs/CogsBuilder;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/cogs/CogsBuilder;->syntheticTableReaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
