.class final Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;
.super Ljava/lang/Object;
.source "CogsBuilder.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cogs/CogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "HttpThreadFactory"
.end annotation


# static fields
.field private static final ueh:Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private final count:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 131
    sget-object v0, Lcom/squareup/cogs/-$$Lambda$CogsBuilder$HttpThreadFactory$drhPzLbOB_Auv8eFb_ZhJFZ3WLw;->INSTANCE:Lcom/squareup/cogs/-$$Lambda$CogsBuilder$HttpThreadFactory$drhPzLbOB_Auv8eFb_ZhJFZ3WLw;

    sput-object v0, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;->ueh:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;->count:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cogs/CogsBuilder$1;)V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Uncaught exception on catalog http thread: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 132
    invoke-static {p1, p0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 3

    .line 138
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sq-HttpBackgroundThread-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;->count:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 139
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 140
    sget-object p1, Lcom/squareup/cogs/CogsBuilder$HttpThreadFactory;->ueh:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-virtual {v0, p1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-object v0
.end method
