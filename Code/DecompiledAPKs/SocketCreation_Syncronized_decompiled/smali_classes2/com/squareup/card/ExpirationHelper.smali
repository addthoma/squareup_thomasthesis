.class public Lcom/squareup/card/ExpirationHelper;
.super Ljava/lang/Object;
.source "ExpirationHelper.java"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/card/ExpirationHelper;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public isExpired(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Z
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eq p1, v0, :cond_0

    iget-object p1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->year:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->month:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/card/ExpirationHelper;->clock:Lcom/squareup/util/Clock;

    .line 28
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 27
    invoke-static {p1, p2, v0}, Lcom/squareup/card/Expiration;->isExpired(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
