.class public final Lcom/squareup/checkoutfe/CheckoutResponse;
.super Lcom/squareup/wire/Message;
.source "CheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;,
        Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/checkoutfe/CheckoutResponse;",
        "Lcom/squareup/checkoutfe/CheckoutResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/checkoutfe/CheckoutResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.checkoutfe.CheckoutClientDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CreatePaymentResponse#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.checkoutfe.LocalizedError#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;"
        }
    .end annotation
.end field

.field public final order:Lcom/squareup/orders/model/Order;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;-><init>()V

    sput-object v0, Lcom/squareup/checkoutfe/CheckoutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/CreatePaymentResponse;Lcom/squareup/orders/model/Order;Lcom/squareup/checkoutfe/CheckoutClientDetails;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;",
            "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/checkoutfe/CheckoutClientDetails;",
            ")V"
        }
    .end annotation

    .line 65
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutfe/CheckoutResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/CreatePaymentResponse;Lcom/squareup/orders/model/Order;Lcom/squareup/checkoutfe/CheckoutClientDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/CreatePaymentResponse;Lcom/squareup/orders/model/Order;Lcom/squareup/checkoutfe/CheckoutClientDetails;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/LocalizedError;",
            ">;",
            "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/checkoutfe/CheckoutClientDetails;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p5, "errors"

    .line 72
    invoke-static {p5, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    .line 73
    iput-object p2, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    .line 74
    iput-object p3, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    .line 75
    iput-object p4, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/checkoutfe/CheckoutResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutResponse;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    .line 95
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    iget-object p1, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    .line 98
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 103
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 105
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/checkoutfe/CheckoutResponse$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->errors:Ljava/util/List;

    .line 82
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    .line 83
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 84
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutResponse;->newBuilder()Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    if-eqz v1, :cond_1

    const-string v1, ", create_payment_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_2

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    if-eqz v1, :cond_3

    const-string v1, ", client_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CheckoutResponse{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
