.class public final Lcom/squareup/checkoutfe/CheckoutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CheckoutRequest;",
        "Lcom/squareup/checkoutfe/CheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public create_order_request:Lcom/squareup/orders/CreateOrderRequest;

.field public create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CheckoutRequest;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutRequest;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/checkoutfe/CheckoutRequest;-><init>(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;Lcom/squareup/orders/CreateOrderRequest;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->build()Lcom/squareup/checkoutfe/CheckoutRequest;

    move-result-object v0

    return-object v0
.end method

.method public create_order_request(Lcom/squareup/orders/CreateOrderRequest;)Lcom/squareup/checkoutfe/CheckoutRequest$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    return-object p0
.end method

.method public create_payment_request(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)Lcom/squareup/checkoutfe/CheckoutRequest$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    return-object p0
.end method
