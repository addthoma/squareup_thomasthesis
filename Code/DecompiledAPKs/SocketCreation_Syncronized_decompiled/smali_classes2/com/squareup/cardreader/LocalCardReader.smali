.class public Lcom/squareup/cardreader/LocalCardReader;
.super Ljava/lang/Object;
.source "LocalCardReader.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReader;


# instance fields
.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

.field private final firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

.field private final paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/PaymentProcessor;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    .line 27
    iput-object p3, p0, Lcom/squareup/cardreader/LocalCardReader;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    .line 28
    iput-object p4, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    return-void
.end method


# virtual methods
.method public abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInitializer;->abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    return-void
.end method

.method public ackTmnWriteNotify()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->ackTmnWriteNotify()V

    return-void
.end method

.method public cancelPayment()V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->cancelPayment()V

    return-void
.end method

.method public cancelTmnRequest()V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->cancelTmnRequest()V

    return-void
.end method

.method public enableSwipePassthrough(Z)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInitializer;->enableSwipePassthrough(Z)V

    return-void
.end method

.method public forget()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->forgetReader()V

    return-void
.end method

.method public getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object v0
.end method

.method public getId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    return-object v0
.end method

.method public identify()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->identify()V

    return-void
.end method

.method public initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderInitializer;->initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method

.method public onCoreDumpDataSent()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->onCoreDumpDataSent()V

    return-void
.end method

.method public onPinBypass()V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->onPinBypass()V

    return-void
.end method

.method public onPinDigitEntered(I)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->onPinDigitEntered(I)V

    return-void
.end method

.method public onPinPadReset()V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->onPinPadReset()V

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method public onTamperDataSent()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->onTamperDataSent()V

    return-void
.end method

.method public powerOff()V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->powerOff()V

    return-void
.end method

.method public processARPC([B)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->processARPC([B)V

    return-void
.end method

.method public processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/FirmwareUpdater;->processFirmwareResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V

    return-void
.end method

.method public processSecureSessionMessageFromServer([B)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInitializer;->processSecureSessionMessageFromServer([B)V

    return-void
.end method

.method public reinitializeSecureSession()V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->reinitializeSecureSession()V

    return-void
.end method

.method public requestPowerStatus()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->requestPowerStatus()V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    invoke-virtual {v0}, Lcom/squareup/cardreader/FirmwareUpdater;->reset()V

    .line 46
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->reset()V

    .line 47
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->cardReaderInitializer:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->reset()V

    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public sendPowerupHint(I)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->sendPowerupHint(I)V

    return-void
.end method

.method public sendTmnDataToReader([B)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->sendTmnBytesToReader([B)V

    return-void
.end method

.method public startPayment(JJ)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentProcessor;->startPayment(JJ)V

    return-void
.end method

.method public startRefund(JJ)V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentProcessor;->startRefund(JJ)V

    return-void
.end method

.method public startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentProcessor;->startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public startTmnMiryo([B)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startTmnMiryo([B)V

    return-void
.end method

.method public startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentProcessor;->startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentProcessor;->startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentProcessor;->startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public submitPinBlock()V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/cardreader/LocalCardReader;->paymentProcessor:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentProcessor;->submitPinBlock()V

    return-void
.end method
