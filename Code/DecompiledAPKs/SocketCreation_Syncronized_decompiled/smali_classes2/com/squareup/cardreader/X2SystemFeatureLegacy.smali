.class public Lcom/squareup/cardreader/X2SystemFeatureLegacy;
.super Ljava/lang/Object;
.source "X2SystemFeatureLegacy.java"


# instance fields
.field private final systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

.field private final systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    .line 18
    iput-object p2, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public setDocked(Z)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 27
    iget-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;->CRS_HARDWARE_PLATFORM_FEATURE_BRAN_DOCKED:Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    goto :goto_0

    .line 30
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;->CRS_HARDWARE_PLATFORM_FEATURE_BRAN_UNDOCKED:Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    :goto_0
    return-void

    .line 24
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "SystemFeature.getFeaturePointer cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
