.class public final Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_ProvideCardReaderInfoFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final addressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
            ">;"
        }
    .end annotation
.end field

.field private final idProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final readerNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->idProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->connectionTypeProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->addressProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->readerNameProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderInfo(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderModule;->provideCardReaderInfo(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->idProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderId;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->connectionTypeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->addressProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->readerNameProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->provideCardReaderInfo(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->get()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    return-object v0
.end method
