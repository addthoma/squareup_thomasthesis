.class Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;
.super Ljava/lang/Object;
.source "CardReaderHub.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderInfo$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalInfoListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderHub;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;->this$0:Lcom/squareup/cardreader/CardReaderHub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 158
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 159
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;->this$0:Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderHub;->access$000(Lcom/squareup/cardreader/CardReaderHub;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;

    .line 160
    invoke-interface {v1, p1}, Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;->onCardReaderInfoUpdated(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;->this$0:Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderHub;->access$100(Lcom/squareup/cardreader/CardReaderHub;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;->this$0:Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderHub;->access$200(Lcom/squareup/cardreader/CardReaderHub;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;->this$0:Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderHub;->access$100(Lcom/squareup/cardreader/CardReaderHub;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
