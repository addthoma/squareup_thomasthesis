.class public final Lcom/squareup/cardreader/DippedCardTracker_Factory;
.super Ljava/lang/Object;
.source "DippedCardTracker_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/DippedCardTracker;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/DippedCardTracker_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/cardreader/DippedCardTracker_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/DippedCardTracker_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)",
            "Lcom/squareup/cardreader/DippedCardTracker_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/cardreader/DippedCardTracker_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/DippedCardTracker_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/DippedCardTracker;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/cardreader/DippedCardTracker;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/DippedCardTracker;-><init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderListeners;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/DippedCardTracker;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/cardreader/DippedCardTracker_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/DippedCardTracker_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/DippedCardTracker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/DippedCardTracker_Factory;->get()Lcom/squareup/cardreader/DippedCardTracker;

    move-result-object v0

    return-object v0
.end method
