.class public interface abstract Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NfcPaymentListener"
.end annotation


# virtual methods
.method public abstract onNfcActionRequired()V
.end method

.method public abstract onNfcCardBlocked()V
.end method

.method public abstract onNfcCardDeclined()V
.end method

.method public abstract onNfcCollision()V
.end method

.method public abstract onNfcInterfaceUnavailable()V
.end method

.method public abstract onNfcLimitExceededInsertCard()V
.end method

.method public abstract onNfcLimitExceededTryAnotherCard()V
.end method

.method public abstract onNfcPresentCardAgain()V
.end method

.method public abstract onNfcProcessingError()V
.end method

.method public abstract onNfcSeePaymentDeviceForInstructions()V
.end method

.method public abstract onNfcTryAnotherCard()V
.end method

.method public abstract onNfcUnlockPaymentDevice()V
.end method

.method public abstract onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onRequestTapCard()V
.end method
