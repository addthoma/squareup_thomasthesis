.class public final Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideSecureSessionRevocationFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
        ">;"
    }
.end annotation


# instance fields
.field private final globalRevocationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->minesweeperProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->resProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->globalRevocationStateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSecureSessionRevocationFeature(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/cardreader/SecureSessionRevocationState;)Lcom/squareup/cardreader/SecureSessionRevocationFeature;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ")",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;"
        }
    .end annotation

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/cardreader/LcrModule;->provideSecureSessionRevocationFeature(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/cardreader/SecureSessionRevocationState;)Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/SecureSessionRevocationFeature;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->minesweeperProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->globalRevocationStateProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/SecureSessionRevocationState;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->provideSecureSessionRevocationFeature(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/cardreader/SecureSessionRevocationState;)Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->get()Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    move-result-object v0

    return-object v0
.end method
