.class public Lcom/squareup/cardreader/SquareLogEventLogger;
.super Ljava/lang/Object;
.source "SquareLogEventLogger.java"

# interfaces
.implements Lcom/squareup/cardreader/ReaderEventLogger;
.implements Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;
.implements Lcom/squareup/crashnado/CrashnadoReporter;
.implements Lcom/squareup/ms/Minesweeper$MinesweeperLogger;


# instance fields
.field private cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/SquareLogEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-void
.end method

.method public static synthetic lambda$AIYqdvGyWmEVnCn2lLwFx6RmcNY(Lcom/squareup/cardreader/SquareLogEventLogger;Lcom/squareup/dipper/events/DipperEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->logAlreadyConnected(Lcom/squareup/dipper/events/DipperEvent;)V

    return-void
.end method

.method public static synthetic lambda$dFBB23B7a_dwm3VU6Ef5bAEiYvA(Lcom/squareup/cardreader/SquareLogEventLogger;Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->logBleConnectionFailure(Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;)V

    return-void
.end method

.method public static synthetic lambda$uAb-3WdFs7bZZrCVazOhTuBikLQ(Lcom/squareup/cardreader/SquareLogEventLogger;Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->logBleConnectionSuccess(Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;)V

    return-void
.end method

.method private logAlreadyConnected(Lcom/squareup/dipper/events/DipperEvent;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 234
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "BLE detected possibly already connected: (%s)"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logBleConnectionFailure(Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 229
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getErrorType()Lcom/squareup/dipper/events/BleErrorType;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 230
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "BLE State: Connection Failure: %s (%s)"

    .line 229
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logBleConnectionSuccess(Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 225
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "BLE State: Connection Success (%s)"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public failedToInstall(Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 54
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Failed to install Crashnado: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public initialize()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/SquareLogEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;

    .line 72
    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$SquareLogEventLogger$uAb-3WdFs7bZZrCVazOhTuBikLQ;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$SquareLogEventLogger$uAb-3WdFs7bZZrCVazOhTuBikLQ;-><init>(Lcom/squareup/cardreader/SquareLogEventLogger;)V

    .line 73
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 75
    iget-object v0, p0, Lcom/squareup/cardreader/SquareLogEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    .line 76
    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$SquareLogEventLogger$dFBB23B7a_dwm3VU6Ef5bAEiYvA;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$SquareLogEventLogger$dFBB23B7a_dwm3VU6Ef5bAEiYvA;-><init>(Lcom/squareup/cardreader/SquareLogEventLogger;)V

    .line 77
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/SquareLogEventLogger;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;

    .line 80
    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$SquareLogEventLogger$AIYqdvGyWmEVnCn2lLwFx6RmcNY;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$SquareLogEventLogger$AIYqdvGyWmEVnCn2lLwFx6RmcNY;-><init>(Lcom/squareup/cardreader/SquareLogEventLogger;)V

    .line 81
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 146
    iget-object p1, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->value:Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Received audio event: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logBatteryInfo(ILcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 183
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryInfo()Lcom/squareup/cardreader/CardReaderBatteryInfo;

    move-result-object p2

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->prettyHashCode(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    const-string p1, "%s (%s)"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logBleConnectionAction(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 1

    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/Object;

    .line 106
    invoke-interface {p4}, Lcom/squareup/cardreader/ble/BleAction;->describe()Ljava/lang/String;

    move-result-object p4

    const/4 v0, 0x0

    aput-object p4, p2, v0

    const/4 p4, 0x1

    aput-object p3, p2, p4

    iget p1, p1, Lcom/squareup/cardreader/CardReaderId;->id:I

    .line 107
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p3, 0x2

    aput-object p1, p2, p3

    const-string p1, "BLE State: Received action \"%s\" in state %s on id: %d"

    .line 106
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logBleConnectionEnqueued(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 95
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 96
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    aput-object p2, v0, p1

    const-string p1, "BLE State: Connection Enqueued (%s %s %s)"

    .line 95
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logBleConnectionStateChange(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V
    .locals 0

    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p5, 0x0

    aput-object p3, p2, p5

    const/4 p3, 0x1

    aput-object p4, p2, p3

    .line 119
    iget p1, p1, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p3, 0x2

    aput-object p1, p2, p3

    const-string p1, "BLE State: State transition %s -> %s on id: %d"

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logBleDisconnectedEvent(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;ZLcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;)V
    .locals 0

    return-void
.end method

.method public logBleReaderForceUnpaired(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 100
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 101
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "BLE State: Connection Force Un-Paired (%s %s)"

    .line 100
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logBluetoothStatusChanged(Ljava/lang/String;ZZ)V
    .locals 0

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    const-string p1, "Bluetooth state has changed to: %s"

    .line 86
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logCirqueTamperStatus(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/bills/CardData$ReaderType;I)V
    .locals 0

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 179
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, p1, p3

    const-string p2, "Received cirque tamper: %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logCommsRateUpdated(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsRate()Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logCommsVersionAcquired(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 1

    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const/4 p2, 0x1

    aput-object p4, p1, p2

    const/4 p2, 0x2

    aput-object p3, p1, p2

    const-string p2, "Received protocol version result %s. Reader: %s Register: %s"

    .line 125
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logCoreDumpResult(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 1

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 134
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const-string p2, "Found coredump: %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logCrashnadoState(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 46
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logEvent(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 151
    invoke-interface {p3, p2}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, v0, p3

    .line 152
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->prettyHashCode(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    const-string p1, "Received event: %s (%s)"

    .line 151
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 142
    invoke-interface {p2, p1}, Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;->getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v0, p2

    const-string p1, "Received event: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 1

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 138
    iget-object p2, p2, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const-string p2, "Received firmware eventlog: %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logFirmwareAssetVersionInfo(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 5

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareAssetVersionInfos()[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    move-result-object p1

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p1, v3

    .line 200
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, " | "

    .line 201
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 204
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    const-string v0, "Firmware component versions received: %s"

    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 90
    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->describe()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "BLE GATT: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logMinesweeperEvent(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 63
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logNativeLibraryLoadEvent(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 42
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
    .locals 0

    return-void
.end method

.method public logReaderError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 0

    return-void
.end method

.method public logSecureSessionResult(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 1

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 157
    invoke-virtual {p2}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const-string p2, "Received event: %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logSecureSessionRevoked(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 162
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const/4 p1, 0x2

    aput-object p3, v0, p1

    const/4 p1, 0x3

    aput-object p4, v0, p1

    const-string p1, "Secure session revoked [%s]: [%s] [%s] [%s]"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logSerialNumberReceived(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)V
    .locals 1

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const-string p2, "Received serial number: %s"

    .line 221
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logSystemCapabilities(ZLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    const-string v2, "supported"

    goto :goto_0

    :cond_0
    const-string v2, "not supported"

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Capabilities are %s"

    .line 188
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 190
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cardreader/lcr/CrsCapability;

    new-array v1, v0, [Ljava/lang/Object;

    .line 191
    invoke-static {p2}, Lcom/squareup/cardreader/SystemFeatureLegacy;->getCapabilityShortName(Lcom/squareup/cardreader/lcr/CrsCapability;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, v3

    const-string p2, "Has capability: %s"

    invoke-static {p2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public logTamperData(ILcom/squareup/cardreader/CardReaderInfo;[B)V
    .locals 1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    .line 168
    new-instance v0, Ljava/lang/String;

    invoke-static {p3}, Lcom/squareup/squarewave/util/Base64;->encode([B)[C

    move-result-object p3

    invoke-direct {v0, p3}, Ljava/lang/String;-><init>([C)V

    const/4 p3, 0x0

    aput-object v0, p2, p3

    .line 169
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->prettyHashCode(I)Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x1

    aput-object p1, p2, p3

    const-string p1, "Received tamper data: %s (%s)"

    .line 168
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logTamperResult(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p3, p2, v0

    .line 174
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/SquareLogEventLogger;->prettyHashCode(I)Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x1

    aput-object p1, p2, p3

    const-string p1, "Found tamper: %s (%s)"

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public logTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/CountryCode;Lcom/squareup/CountryCode;)V
    .locals 1

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const/4 p2, 0x1

    aput-object p3, p1, p2

    const-string p2, "TMS CountryCode %s found for user %s"

    .line 209
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onMinesweeperFailureToInitialize(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 67
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    const-string p1, "Failed to initialize MS: %s\n%s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public preparingIllegalStack(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 58
    new-instance p1, Ljava/lang/Throwable;

    invoke-direct {p1}, Ljava/lang/Throwable;-><init>()V

    .line 59
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "Illegal Stack Prepared on %s: %s"

    .line 58
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method prettyHashCode(I)Ljava/lang/String;
    .locals 2

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public reportCrash(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 50
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "Native Crash: %s\nMinidump:\n%s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
