.class public final Lcom/squareup/cardreader/ReaderPayload;
.super Ljava/lang/Object;
.source "CardreaderMessengerInterface.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/squareup/cardreader/ReaderMessage;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0005H\u00c6\u0003J\u000e\u0010\u000e\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\tJ(\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00028\u0000H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0010J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0013\u0010\u0006\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\n\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderPayload;",
        "MessageType",
        "Lcom/squareup/cardreader/ReaderMessage;",
        "",
        "readerId",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "message",
        "(Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;)V",
        "getMessage",
        "()Lcom/squareup/cardreader/ReaderMessage;",
        "Lcom/squareup/cardreader/ReaderMessage;",
        "getReaderId",
        "()Lcom/squareup/cardreader/CardreaderConnectionId;",
        "component1",
        "component2",
        "copy",
        "(Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;)Lcom/squareup/cardreader/ReaderPayload;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:Lcom/squareup/cardreader/ReaderMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field

.field private final readerId:Lcom/squareup/cardreader/CardreaderConnectionId;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardreaderConnectionId;",
            "TMessageType;)V"
        }
    .end annotation

    const-string v0, "readerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    iput-object p2, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderPayload;Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;ILjava/lang/Object;)Lcom/squareup/cardreader/ReaderPayload;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ReaderPayload;->copy(Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;)Lcom/squareup/cardreader/ReaderPayload;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/CardreaderConnectionId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardreader/ReaderMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;)Lcom/squareup/cardreader/ReaderPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardreaderConnectionId;",
            "TMessageType;)",
            "Lcom/squareup/cardreader/ReaderPayload<",
            "TMessageType;>;"
        }
    .end annotation

    const-string v0, "readerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderPayload;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/ReaderPayload;-><init>(Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/cardreader/ReaderMessage;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderPayload;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderPayload;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessage()Lcom/squareup/cardreader/ReaderMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    return-object v0
.end method

.method public final getReaderId()Lcom/squareup/cardreader/CardreaderConnectionId;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReaderPayload(readerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderPayload;->readerId:Lcom/squareup/cardreader/CardreaderConnectionId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderPayload;->message:Lcom/squareup/cardreader/ReaderMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
