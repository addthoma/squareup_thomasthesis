.class public final enum Lcom/squareup/cardreader/CardReaderEventName;
.super Ljava/lang/Enum;
.source "CardReaderEventName.java"

# interfaces
.implements Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/CardReaderEventName;",
        ">;",
        "Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum APP_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum APP_UPDATE_SUGGESTED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum COMMS_RATE_UPDATED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum COMMS_VERSION_ACQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum ERASE_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum FIRMWARE_ASSET_VERSIONS:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum FW_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum INIT_PAYMENTS:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum INIT_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum MAGSWIPE_FAILURE_IGNORED:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum READER_READY:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum REQUEST_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum REQUEST_POWER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

.field public static final enum RETRY_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v1, 0x0

    const-string v2, "COMMS_VERSION_ACQUIRED"

    const-string v3, "Comms Version Acquired"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_VERSION_ACQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    .line 12
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v2, 0x1

    const-string v3, "COMMS_RATE_UPDATED"

    const-string v4, "Comms Rate Updated"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_RATE_UPDATED:Lcom/squareup/cardreader/CardReaderEventName;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v3, 0x2

    const-string v4, "DEVICE_UNSUPPORTED"

    const-string v5, "Device Unsupported"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/CardReaderEventName;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v4, 0x3

    const-string v5, "FW_UPDATE_REQUIRED"

    const-string v6, "Firmware Update Required"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->FW_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v5, 0x4

    const-string v6, "APP_UPDATE_REQUIRED"

    const-string v7, "Register Update Required"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->APP_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v6, 0x5

    const-string v7, "APP_UPDATE_SUGGESTED"

    const-string v8, "Register Update Suggested"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->APP_UPDATE_SUGGESTED:Lcom/squareup/cardreader/CardReaderEventName;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v7, 0x6

    const-string v8, "REQUEST_POWER_STATUS"

    const-string v9, "Request Power Status"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_POWER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/4 v8, 0x7

    const-string v9, "REQUEST_TAMPER_STATUS"

    const-string v10, "Request Tamper Status"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v9, 0x8

    const-string v10, "REQUEST_CORE_DUMP"

    const-string v11, "Request Core Dump"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v10, 0x9

    const-string v11, "ERASE_CORE_DUMP"

    const-string v12, "Erase Core Dump"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->ERASE_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v11, 0xa

    const-string v12, "INIT_PAYMENTS"

    const-string v13, "Initialize Payments"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->INIT_PAYMENTS:Lcom/squareup/cardreader/CardReaderEventName;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v12, 0xb

    const-string v13, "INIT_SECURE_SESSION"

    const-string v14, "Initialize Secure Session"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->INIT_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v13, 0xc

    const-string v14, "RETRY_SECURE_SESSION"

    const-string v15, "Retry Establishing Secure Session"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->RETRY_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v14, 0xd

    const-string v15, "READER_READY"

    const-string v13, "Reader Ready"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->READER_READY:Lcom/squareup/cardreader/CardReaderEventName;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v13, 0xe

    const-string v15, "FIRMWARE_ASSET_VERSIONS"

    const-string v14, "Firmware Asset Versions Received"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->FIRMWARE_ASSET_VERSIONS:Lcom/squareup/cardreader/CardReaderEventName;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/CardReaderEventName;

    const-string v14, "MAGSWIPE_FAILURE_IGNORED"

    const/16 v15, 0xf

    const-string v13, "MagSwipe Failure Ignored"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/CardReaderEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->MAGSWIPE_FAILURE_IGNORED:Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/cardreader/CardReaderEventName;

    .line 10
    sget-object v13, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_VERSION_ACQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->COMMS_RATE_UPDATED:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->FW_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->APP_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->APP_UPDATE_SUGGESTED:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_POWER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->ERASE_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->INIT_PAYMENTS:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->INIT_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->RETRY_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->READER_READY:Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->FIRMWARE_ASSET_VERSIONS:Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->MAGSWIPE_FAILURE_IGNORED:Lcom/squareup/cardreader/CardReaderEventName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/CardReaderEventName;->$VALUES:[Lcom/squareup/cardreader/CardReaderEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderEventName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderEventName;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/CardReaderEventName;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/cardreader/CardReaderEventName;->$VALUES:[Lcom/squareup/cardreader/CardReaderEventName;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/CardReaderEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/CardReaderEventName;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderEventName;->value:Ljava/lang/String;

    return-object v0
.end method

.method public getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
    .locals 1

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderEventName;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
