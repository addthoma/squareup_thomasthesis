.class public final Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;
.super Ljava/lang/Object;
.source "X2SystemFeatureLegacy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/X2SystemFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final systemFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final systemFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->systemFeatureProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->systemFeatureNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)Lcom/squareup/cardreader/X2SystemFeatureLegacy;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/cardreader/X2SystemFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/X2SystemFeatureLegacy;-><init>(Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/X2SystemFeatureLegacy;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->systemFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SystemFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->systemFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->newInstance(Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)Lcom/squareup/cardreader/X2SystemFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->get()Lcom/squareup/cardreader/X2SystemFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
