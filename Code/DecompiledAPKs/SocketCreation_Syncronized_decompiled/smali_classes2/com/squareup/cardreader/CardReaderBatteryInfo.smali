.class public Lcom/squareup/cardreader/CardReaderBatteryInfo;
.super Ljava/lang/Object;
.source "CardReaderBatteryInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    }
.end annotation


# static fields
.field static final EMPTY_INFO:Lcom/squareup/cardreader/CardReaderBatteryInfo;


# instance fields
.field private final batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

.field private final celsius:Ljava/lang/Integer;

.field private final isCritical:Ljava/lang/Boolean;

.field private final milliamps:Ljava/lang/Integer;

.field private final millivolts:Ljava/lang/Integer;

.field private final percentage:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/CardReaderBatteryInfo;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->EMPTY_INFO:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->access$000(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    .line 21
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->access$100(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->milliamps:Ljava/lang/Integer;

    .line 22
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->access$200(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->millivolts:Ljava/lang/Integer;

    .line 23
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->access$300(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->celsius:Ljava/lang/Integer;

    .line 24
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->access$400(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->isCritical:Ljava/lang/Boolean;

    .line 25
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->access$500(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;Lcom/squareup/cardreader/CardReaderBatteryInfo$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;-><init>(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Boolean;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->isCritical:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->milliamps:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->millivolts:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->celsius:Ljava/lang/Integer;

    return-object p0
.end method


# virtual methods
.method public getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-object v0
.end method

.method public getCelsius()Ljava/lang/Integer;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->celsius:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMilliamps()Ljava/lang/Integer;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->milliamps:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMillivolts()Ljava/lang/Integer;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->millivolts:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPercentage()Ljava/lang/Integer;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    return-object v0
.end method

.method public isCritical()Ljava/lang/Boolean;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->isCritical:Ljava/lang/Boolean;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 53
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->milliamps:Ljava/lang/Integer;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->millivolts:Ljava/lang/Integer;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->celsius:Ljava/lang/Integer;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo;->isCritical:Ljava/lang/Boolean;

    .line 54
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ", is critical"

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-string v2, "Battery info: %.3f%%, %dmA, %dmV, %d\u02daC%s"

    .line 53
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
