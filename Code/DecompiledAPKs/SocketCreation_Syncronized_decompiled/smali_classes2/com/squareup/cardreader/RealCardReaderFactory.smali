.class public Lcom/squareup/cardreader/RealCardReaderFactory;
.super Ljava/lang/Object;
.source "RealCardReaderFactory.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderFactory;


# instance fields
.field private final cardReaderContexts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final features:Lcom/squareup/settings/server/Features;

.field private nextIdNo:I

.field protected parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 32
    iput v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->nextIdNo:I

    .line 35
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 36
    iput-object p2, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->features:Lcom/squareup/settings/server/Features;

    .line 37
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    return-void
.end method

.method private buildAndInitializeBleCardReaderContext(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    if-eqz v0, :cond_0

    .line 142
    new-instance v0, Lcom/squareup/cardreader/CardReaderId;

    iget v1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->nextIdNo:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->nextIdNo:I

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/CardReaderId;-><init>(I)V

    .line 145
    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/cardreader/RealCardReaderFactory;->createBleCardReaderComponent(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/cardreader/CardReaderContextComponent;

    move-result-object p1

    .line 148
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderContextComponent;->cardReaderContext()Lcom/squareup/cardreader/CardReaderContext;

    move-result-object p1

    .line 149
    sget-object p2, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/RealCardReaderFactory;->initializeCardReaderContext(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    return-object p1

    .line 138
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "parentComponent is null. Make sure you call #initialize during app start"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private buildAndInitializeCardReaderContext(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lrx/functions/Func1;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            "Lrx/functions/Func1<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Lcom/squareup/cardreader/CardReaderContextComponent;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderContext;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "parentComponent is null. Make sure you call #initialize during app start"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 158
    new-instance v0, Lcom/squareup/cardreader/CardReaderId;

    iget v1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->nextIdNo:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->nextIdNo:I

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/CardReaderId;-><init>(I)V

    .line 159
    invoke-interface {p2, v0}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cardreader/CardReaderContextComponent;

    .line 161
    invoke-interface {p2}, Lcom/squareup/cardreader/CardReaderContextComponent;->cardReaderContext()Lcom/squareup/cardreader/CardReaderContext;

    move-result-object p2

    .line 162
    invoke-direct {p0, p2, p1}, Lcom/squareup/cardreader/RealCardReaderFactory;->initializeCardReaderContext(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderHub;->addCardReader(Lcom/squareup/cardreader/CardReaderContext;)V

    .line 165
    iget-object p1, p2, Lcom/squareup/cardreader/CardReaderContext;->cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch;->initializeCardReader()V

    return-object p2
.end method

.method private destroyContext(Lcom/squareup/cardreader/CardReaderContext;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 183
    :cond_0
    iget-object v0, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    .line 185
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->reset()V

    .line 186
    iget-object v1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReader(Lcom/squareup/cardreader/CardReader;)V

    .line 188
    iget-object v1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderGraphInitializer:Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;->destroy()V

    return-void
.end method

.method private initializeCardReaderContext(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    .line 174
    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderGraphInitializer:Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;->initialize()V

    return-void
.end method


# virtual methods
.method protected createAudioCardReaderComponent(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReaderContextComponent;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/cardreader/CardReaderModule;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/CardReaderModule;-><init>(Lcom/squareup/cardreader/CardReaderId;)V

    .line 118
    const-class p1, Lcom/squareup/cardreader/audio/AudioCardReaderContextComponent;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 119
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 118
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->createComponent(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContextComponent;

    return-object p1
.end method

.method protected createBleCardReaderComponent(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/cardreader/CardReaderContextComponent;
    .locals 3

    .line 128
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule;

    iget-object v1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_V2_BLE_STATE_MACHINE:Lcom/squareup/settings/server/Features$Feature;

    .line 129
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-direct {v0, p1, p3, v1}, Lcom/squareup/cardreader/ble/BleDeviceModule;-><init>(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;Z)V

    .line 130
    new-instance p1, Lcom/squareup/cardreader/CardReaderModule;

    invoke-direct {p1, p2}, Lcom/squareup/cardreader/CardReaderModule;-><init>(Lcom/squareup/cardreader/CardReaderId;)V

    .line 131
    const-class p2, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent;

    const/4 p3, 0x3

    new-array p3, p3, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    const/4 v2, 0x0

    aput-object v1, p3, v2

    const/4 v1, 0x1

    aput-object p1, p3, v1

    const/4 p1, 0x2

    aput-object v0, p3, p1

    .line 132
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 131
    invoke-static {p2, p1}, Lcom/squareup/dagger/Components;->createComponent(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContextComponent;

    return-object p1
.end method

.method protected createT2CardReaderComponent(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReaderContextComponent;
    .locals 4

    .line 105
    new-instance v0, Lcom/squareup/cardreader/CardReaderModule;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/CardReaderModule;-><init>(Lcom/squareup/cardreader/CardReaderId;)V

    .line 106
    const-class p1, Lcom/squareup/cardreader/squid/t2/T2CardReaderContextComponent;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 107
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 106
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->createComponent(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContextComponent;

    return-object p1
.end method

.method protected createX2CardReaderComponent(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReaderContextComponent;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/cardreader/CardReaderModule;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/CardReaderModule;-><init>(Lcom/squareup/cardreader/CardReaderId;)V

    .line 112
    const-class p1, Lcom/squareup/cardreader/squid/x2/X2CardReaderContextComponent;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 113
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 112
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->createComponent(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContextComponent;

    return-object p1
.end method

.method public destroy(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContext;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/RealCardReaderFactory;->destroyContext(Lcom/squareup/cardreader/CardReaderContext;)V

    return-void
.end method

.method public destroyAllBluetoothReaders()V
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderContext;

    .line 85
    iget-object v2, v1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    invoke-direct {p0, v1}, Lcom/squareup/cardreader/RealCardReaderFactory;->destroyContext(Lcom/squareup/cardreader/CardReaderContext;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public destroyAllReaders()V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderContext;

    .line 79
    invoke-direct {p0, v1}, Lcom/squareup/cardreader/RealCardReaderFactory;->destroyContext(Lcom/squareup/cardreader/CardReaderContext;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public destroyWirelessAutoConnect(Ljava/lang/String;)V
    .locals 3

    .line 96
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderContext;

    .line 97
    iget-object v2, v1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 98
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    invoke-direct {p0, v1}, Lcom/squareup/cardreader/RealCardReaderFactory;->destroyContext(Lcom/squareup/cardreader/CardReaderContext;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public forAudio()Lcom/squareup/cardreader/CardReaderContext;
    .locals 2

    .line 66
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$RcdITGfnKauYLQS9373SgYwgUko;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$RcdITGfnKauYLQS9373SgYwgUko;-><init>(Lcom/squareup/cardreader/RealCardReaderFactory;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/RealCardReaderFactory;->buildAndInitializeCardReaderContext(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lrx/functions/Func1;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v0

    return-object v0
.end method

.method public forBle(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_IMMEDIATELY:Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/RealCardReaderFactory;->buildAndInitializeBleCardReaderContext(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object p1

    return-object p1
.end method

.method public forBleAutoConnect(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/RealCardReaderFactory;->buildAndInitializeBleCardReaderContext(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/cardreader/CardReaderContext;

    return-void
.end method

.method public forT2()Lcom/squareup/cardreader/CardReaderContext;
    .locals 2

    .line 58
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$Kes-4NHE6W6Yzcc9njVf_tPFkUE;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$Kes-4NHE6W6Yzcc9njVf_tPFkUE;-><init>(Lcom/squareup/cardreader/RealCardReaderFactory;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/RealCardReaderFactory;->buildAndInitializeCardReaderContext(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lrx/functions/Func1;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v0

    return-object v0
.end method

.method public forX2()Lcom/squareup/cardreader/CardReaderContext;
    .locals 2

    .line 62
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$l9K97jhvmTxlTQ8bhUqksXxehQs;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$l9K97jhvmTxlTQ8bhUqksXxehQs;-><init>(Lcom/squareup/cardreader/RealCardReaderFactory;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/RealCardReaderFactory;->buildAndInitializeCardReaderContext(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lrx/functions/Func1;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v0

    return-object v0
.end method

.method public hasCardReaderWithAddress(Ljava/lang/String;)Z
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->cardReaderContexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderContext;

    .line 49
    iget-object v2, v1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 50
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public initialize(Lcom/squareup/cardreader/CardReaderContextParent;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    if-nez v0, :cond_0

    .line 44
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderFactory;->parentComponent:Lcom/squareup/cardreader/CardReaderContextParent;

    return-void

    .line 42
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "You can only initialize CardReaderFactory once!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
