.class public Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;
.super Ljava/lang/Object;
.source "FirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/FirmwareUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AssetDescriptor"
.end annotation


# instance fields
.field public final blocking:Z

.field public final reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field public retryCount:I

.field public final size:J


# direct methods
.method public constructor <init>(ZLcom/squareup/protos/client/tarkin/Asset$Reboot;J)V
    .locals 0

    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    iput-boolean p1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->blocking:Z

    .line 268
    iput-object p2, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 269
    iput-wide p3, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->size:J

    const/4 p1, 0x0

    .line 270
    iput p1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->retryCount:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AssetDescriptor{blocking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->blocking:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", reboot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->size:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", retryCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->retryCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
