.class public interface abstract Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CardReaderEvent"
.end annotation


# virtual methods
.method public abstract getValue()Ljava/lang/String;
.end method

.method public abstract getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
.end method
