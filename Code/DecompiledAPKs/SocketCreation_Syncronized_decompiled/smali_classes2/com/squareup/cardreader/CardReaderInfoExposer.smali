.class public Lcom/squareup/cardreader/CardReaderInfoExposer;
.super Ljava/lang/Object;
.source "CardReaderInfoExposer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bleConnectionIntervalUpdated(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderInfo;I)V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$CardReaderInfoExposer$0QjJBu7sGGqaMYw_VjOYpX6Bffw;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderInfoExposer$0QjJBu7sGGqaMYw_VjOYpX6Bffw;-><init>(Lcom/squareup/cardreader/CardReaderInfo;I)V

    invoke-interface {p0, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static bleMtuUpdated(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderInfo;I)V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$CardReaderInfoExposer$KX4vvqxpUlW1OyyK5pISl0SEsLE;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderInfoExposer$KX4vvqxpUlW1OyyK5pISl0SEsLE;-><init>(Lcom/squareup/cardreader/CardReaderInfo;I)V

    invoke-interface {p0, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$bleConnectionIntervalUpdated$1(Lcom/squareup/cardreader/CardReaderInfo;I)V
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->bleConnectionIntervalUpdated(I)V

    return-void
.end method

.method static synthetic lambda$bleMtuUpdated$0(Lcom/squareup/cardreader/CardReaderInfo;I)V
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->bleMtuUpdated(I)V

    return-void
.end method
