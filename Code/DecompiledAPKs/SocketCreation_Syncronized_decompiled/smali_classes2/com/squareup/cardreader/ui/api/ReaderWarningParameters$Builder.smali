.class public Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
.super Ljava/lang/Object;
.source "ReaderWarningParameters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private importantMessageId:I

.field private localizedMessage:Ljava/lang/String;

.field private localizedTitle:Ljava/lang/String;

.field private messageId:I

.field private secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

.field private titleId:I

.field private url:Ljava/lang/String;

.field private userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

.field private vectorId:Ljava/lang/Integer;

.field private warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->vectorId:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/String;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/ui/api/ReaderWarningType;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/CardReaderId;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)I
    .locals 0

    .line 123
    iget p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)I
    .locals 0

    .line 123
    iget p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId:I

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/String;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/String;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)I
    .locals 0

    .line 123
    iget p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->importantMessageId:I

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 2

    .line 207
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$1;)V

    return-object v0
.end method

.method public cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object p0
.end method

.method public defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    return-object p0
.end method

.method public glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 147
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public importantMessageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 177
    iput p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->importantMessageId:I

    return-object p0
.end method

.method public localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage:Ljava/lang/String;

    return-object p0
.end method

.method public localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle:Ljava/lang/String;

    return-object p0
.end method

.method public messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 157
    iput p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId:I

    return-object p0
.end method

.method public secondButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    return-object p0
.end method

.method public titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 152
    iput p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId:I

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url:Ljava/lang/String;

    return-object p0
.end method

.method public userInteractionMessage(Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    return-object p0
.end method

.method public vector(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 142
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->vectorId:Ljava/lang/Integer;

    return-object p0
.end method

.method public warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    return-object p0
.end method
