.class public final Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;
.super Ljava/lang/Object;
.source "DipperUiErrorDisplayTypeSelector.kt"

# interfaces
.implements Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UseConcreteWarningScreen"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0004H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;",
        "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
        "()V",
        "getTamperErrorDisplayType",
        "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;",
        "getUseChipCardDisplayType",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;->INSTANCE:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseConcreteWarningScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTamperErrorDisplayType()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;->CONCRETE_WARNING_SCREEN:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;

    return-object v0
.end method

.method public getUseChipCardDisplayType()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;->CONCRETE_WARNING_SCREEN:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;

    return-object v0
.end method
