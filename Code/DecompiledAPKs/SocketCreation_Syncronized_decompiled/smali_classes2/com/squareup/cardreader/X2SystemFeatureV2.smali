.class public final Lcom/squareup/cardreader/X2SystemFeatureV2;
.super Ljava/lang/Object;
.source "X2SystemFeatureV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/cardreader/X2SystemFeatureV2;",
        "",
        "()V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)V",
        "initialize",
        "",
        "setDocked",
        "isDocked",
        "",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    return-void
.end method

.method private final setDocked(Z)V
    .locals 1

    const-string v0, "featurePointer"

    if-eqz p1, :cond_1

    .line 18
    iget-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 19
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;->CRS_HARDWARE_PLATFORM_FEATURE_BRAN_DOCKED:Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;

    .line 17
    invoke-static {p1, v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    goto :goto_0

    .line 23
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 24
    :cond_2
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;->CRS_HARDWARE_PLATFORM_FEATURE_BRAN_UNDOCKED:Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;

    .line 22
    invoke-static {p1, v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    :goto_0
    return-void
.end method


# virtual methods
.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;
    .locals 2

    .line 9
    iget-object v0, p0, Lcom/squareup/cardreader/X2SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iput-object p1, p0, Lcom/squareup/cardreader/X2SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    return-void
.end method
