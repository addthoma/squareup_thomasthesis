.class public final Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideTamperFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/TamperFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final tamperFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->cardReaderPointerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->tamperFeatureNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTamperFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;)Lcom/squareup/cardreader/TamperFeatureLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;"
        }
    .end annotation

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/cardreader/LcrModule;->provideTamperFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;)Lcom/squareup/cardreader/TamperFeatureLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TamperFeatureLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/TamperFeatureLegacy;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->cardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->tamperFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->provideTamperFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;)Lcom/squareup/cardreader/TamperFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->get()Lcom/squareup/cardreader/TamperFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
