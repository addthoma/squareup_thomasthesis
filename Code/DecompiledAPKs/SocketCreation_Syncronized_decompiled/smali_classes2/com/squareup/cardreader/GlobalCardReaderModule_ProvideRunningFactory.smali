.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_ProvideRunningFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    .line 24
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRunning(Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)Ljava/lang/Boolean;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/GlobalCardReaderModule;->provideRunning(Lcom/squareup/cardreader/CardReaderPauseAndResumer;)Ljava/lang/Boolean;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->provideRunning(Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
