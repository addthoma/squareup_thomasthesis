.class public abstract Lcom/squareup/cardreader/ble/BleCardReaderContextComponent$Module;
.super Ljava/lang/Object;
.source "BleCardReaderContextComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cardreader/ble/BleDeviceModule;,
        Lcom/squareup/cardreader/CardReaderModule;,
        Lcom/squareup/cardreader/CardReaderModule$AllExceptDipper;,
        Lcom/squareup/cardreader/CardReaderModule$Prod;,
        Lcom/squareup/cardreader/LocalCardReaderModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleCardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderGraphInitializer(Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method
