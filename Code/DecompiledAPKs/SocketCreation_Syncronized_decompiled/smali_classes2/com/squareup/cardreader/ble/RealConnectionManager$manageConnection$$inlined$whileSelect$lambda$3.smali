.class final Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "ConnectionManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/RealConnectionManager;->manageConnection(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cardreader/ble/SendMessage;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Ljava/lang/Boolean;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u008a@\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "message",
        "Lcom/squareup/cardreader/ble/SendMessage;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/cardreader/ble/RealConnectionManager$manageConnection$2$4"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connection$inlined:Lcom/squareup/blecoroutines/Connection;

.field final synthetic $controlCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

.field final synthetic $dataWriteCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

.field final synthetic $events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

.field final synthetic $executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

.field final synthetic $negotiatedConnection$inlined:Lcom/squareup/cardreader/ble/NegotiatedConnection;

.field final synthetic $sendMessageChannel$inlined:Lkotlinx/coroutines/channels/Channel;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field label:I

.field private p$0:Lcom/squareup/cardreader/ble/SendMessage;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0

    iput-object p2, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$connection$inlined:Lcom/squareup/blecoroutines/Connection;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$negotiatedConnection$inlined:Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iput-object p4, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iput-object p5, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$sendMessageChannel$inlined:Lkotlinx/coroutines/channels/Channel;

    iput-object p6, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    iput-object p7, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$dataWriteCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object p8, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$controlCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 p2, 0x2

    invoke-direct {p0, p2, p1}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$connection$inlined:Lcom/squareup/blecoroutines/Connection;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$negotiatedConnection$inlined:Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v6, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$sendMessageChannel$inlined:Lkotlinx/coroutines/channels/Channel;

    iget-object v7, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v8, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$dataWriteCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v9, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$controlCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v9}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    check-cast p1, Lcom/squareup/cardreader/ble/SendMessage;

    iput-object p1, v0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->p$0:Lcom/squareup/cardreader/ble/SendMessage;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 61
    iget v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->label:I

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eqz v1, :cond_4

    if-eq v1, v6, :cond_3

    if-eq v1, v5, :cond_2

    if-eq v1, v4, :cond_1

    if-eq v1, v3, :cond_3

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cardreader/ble/SendMessage;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 93
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$1:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CompletableDeferred;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/SendMessage;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$1:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CompletableDeferred;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/SendMessage;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cardreader/ble/SendMessage;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_4
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->p$0:Lcom/squareup/cardreader/ble/SendMessage;

    .line 63
    instance-of v1, p1, Lcom/squareup/cardreader/ble/SendMessage$WriteData;

    const/4 v7, 0x0

    if-eqz v1, :cond_5

    .line 64
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v1

    new-instance v3, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$1;

    invoke-direct {v3, p0, p1, v7}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;Lcom/squareup/cardreader/ble/SendMessage;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    iput v6, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->label:I

    invoke-static {v1, v2, v3, p0}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_c

    return-object v0

    .line 67
    :cond_5
    instance-of v1, p1, Lcom/squareup/cardreader/ble/SendMessage$ReadAckVector;

    if-eqz v1, :cond_7

    .line 68
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ble/SendMessage$ReadAckVector;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/SendMessage$ReadAckVector;->getResult()Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v2}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v2

    new-instance v4, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$2;

    invoke-direct {v4, p0, v7}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$2;-><init>(Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$1:Ljava/lang/Object;

    iput v5, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->label:I

    invoke-static {v2, v3, v4, p0}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_6

    return-object v0

    :cond_6
    move-object v0, v1

    :goto_0
    const-string/jumbo v1, "withTimeout(executionEnv\u2026int16()\n                }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-interface {v0, p1}, Lkotlinx/coroutines/CompletableDeferred;->complete(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 74
    :cond_7
    instance-of v1, p1, Lcom/squareup/cardreader/ble/SendMessage$ReadMtu;

    if-eqz v1, :cond_9

    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ble/SendMessage$ReadMtu;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/SendMessage$ReadMtu;->getResult()Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v1

    .line 75
    iget-object v2, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v2}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v2

    new-instance v5, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$3;

    invoke-direct {v5, p0, v7}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$3;-><init>(Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;Lkotlin/coroutines/Continuation;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$1:Ljava/lang/Object;

    iput v4, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->label:I

    invoke-static {v2, v3, v5, p0}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_8

    return-object v0

    :cond_8
    move-object v0, v1

    :goto_1
    const-string/jumbo v1, "withTimeout(executionEnv\u2026asUint8()\n              }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-interface {v0, p1}, Lkotlinx/coroutines/CompletableDeferred;->complete(Ljava/lang/Object;)Z

    goto :goto_3

    .line 80
    :cond_9
    instance-of v1, p1, Lcom/squareup/cardreader/ble/SendMessage$ForgetBond;

    if-eqz v1, :cond_a

    .line 81
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v1

    new-instance v4, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$4;

    invoke-direct {v4, p0, v7}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$4;-><init>(Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    iput v3, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->label:I

    invoke-static {v1, v2, v4, p0}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_c

    return-object v0

    .line 85
    :cond_a
    instance-of v1, p1, Lcom/squareup/cardreader/ble/SendMessage$ReadConnInterval;

    if-eqz v1, :cond_c

    .line 86
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v3

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$5;

    invoke-direct {v1, p0, v7}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3$5;-><init>(Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->label:I

    invoke-static {v3, v4, v1, p0}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_b

    return-object v0

    .line 61
    :cond_b
    :goto_2
    check-cast p1, Ljava/lang/Integer;

    .line 90
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;->$events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    const-string v1, "connInterval"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishConnectionInterval$impl_release(I)V

    .line 93
    :cond_c
    :goto_3
    invoke-static {v6}, Lkotlin/coroutines/jvm/internal/Boxing;->boxBoolean(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
