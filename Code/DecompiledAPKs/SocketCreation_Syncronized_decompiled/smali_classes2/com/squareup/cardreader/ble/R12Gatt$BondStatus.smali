.class final enum Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;
.super Ljava/lang/Enum;
.source "R12Gatt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/R12Gatt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "BondStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

.field public static final enum BONDED_TO_CONNECTED_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

.field public static final enum BONDED_TO_OTHER_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

.field public static final enum BONDING_FAILED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

.field public static final enum NOT_BONDED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 18
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    const/4 v1, 0x0

    const-string v2, "NOT_BONDED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->NOT_BONDED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    const/4 v2, 0x1

    const-string v3, "BONDING_FAILED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDING_FAILED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    const/4 v3, 0x2

    const-string v4, "BONDED_TO_CONNECTED_PEER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDED_TO_CONNECTED_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    const/4 v4, 0x3

    const-string v5, "BONDED_TO_OTHER_PEER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDED_TO_OTHER_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    .line 17
    sget-object v5, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->NOT_BONDED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDING_FAILED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDED_TO_CONNECTED_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDED_TO_OTHER_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->$VALUES:[Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;
    .locals 1

    .line 17
    const-class v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->$VALUES:[Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    return-object v0
.end method
