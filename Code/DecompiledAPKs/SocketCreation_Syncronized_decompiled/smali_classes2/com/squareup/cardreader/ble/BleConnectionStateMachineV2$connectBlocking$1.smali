.class final Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;
.super Lkotlin/coroutines/jvm/internal/ContinuationImpl;
.source "BleConnectionStateMachineV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectBlocking$impl_release(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u0081@"
    }
    d2 = {
        "connectBlocking",
        "",
        "continuation",
        "Lkotlin/coroutines/Continuation;",
        ""
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.cardreader.ble.BleConnectionStateMachineV2"
    f = "BleConnectionStateMachineV2.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5
    }
    l = {
        0x82,
        0x88,
        0x8a,
        0x92,
        0x95,
        0xa8
    }
    m = "connectBlocking$impl_release"
    n = {
        "this",
        "didConnect",
        "bleError",
        "connectionError",
        "this",
        "didConnect",
        "bleError",
        "connectionError",
        "rssiAsync",
        "this",
        "didConnect",
        "bleError",
        "connectionError",
        "rssiAsync",
        "connection",
        "this",
        "didConnect",
        "bleError",
        "connectionError",
        "rssiAsync",
        "connection",
        "negotiatedConnection",
        "this",
        "didConnect",
        "bleError",
        "connectionError",
        "rssiAsync",
        "connection",
        "negotiatedConnection",
        "this",
        "didConnect",
        "bleError",
        "connectionError",
        "rssiAsync",
        "loggingStateAtDisconnection",
        "disconnectStatus"
    }
    s = {
        "L$0",
        "I$0",
        "L$1",
        "L$2",
        "L$0",
        "I$0",
        "L$1",
        "L$2",
        "L$3",
        "L$0",
        "I$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$0",
        "I$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$0",
        "I$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$0",
        "I$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5"
    }
.end annotation


# instance fields
.field I$0:I

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field label:I

.field synthetic result:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-direct {p0, p2}, Lkotlin/coroutines/jvm/internal/ContinuationImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->result:Ljava/lang/Object;

    iget p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    const/high16 v0, -0x80000000

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectBlocking$impl_release(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
