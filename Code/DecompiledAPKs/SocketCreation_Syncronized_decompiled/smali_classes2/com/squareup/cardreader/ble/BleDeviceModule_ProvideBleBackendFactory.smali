.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleBackendFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleBackendNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderConnectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderDispatchProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardreaderNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final listenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final tmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->listenerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardReaderConnectorProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->sessionProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->bleBackendNativeProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardreaderNativeProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->tmnTimingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;"
        }
    .end annotation

    .line 83
    new-instance v11, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static provideBleBackend(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/BleBackendLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            "Lcom/squareup/tmn/TmnTimings;",
            ")",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;"
        }
    .end annotation

    .line 92
    invoke-static/range {p0 .. p9}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleBackend(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleBackendLegacy;
    .locals 10

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->listenerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardReaderConnectorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->sessionProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    iget-object v6, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/thread/executor/MainThread;

    iget-object v7, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->bleBackendNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v8, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->cardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object v9, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->tmnTimingsProvider:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/tmn/TmnTimings;

    invoke-static/range {v0 .. v9}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->provideBleBackend(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->get()Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-result-object v0

    return-object v0
.end method
