.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/logging/BluetoothDevicesCountInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

.field private final realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    .line 38
    iput-object p2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideBluetoothDevicesCountInitializer(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;",
            "Landroid/app/Application;",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ")",
            "Lcom/squareup/logging/BluetoothDevicesCountInitializer;"
        }
    .end annotation

    .line 61
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;->provideBluetoothDevicesCountInitializer(Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/logging/BluetoothDevicesCountInitializer;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/BluetoothUtils;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->provideBluetoothDevicesCountInitializer(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->get()Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    move-result-object v0

    return-object v0
.end method
