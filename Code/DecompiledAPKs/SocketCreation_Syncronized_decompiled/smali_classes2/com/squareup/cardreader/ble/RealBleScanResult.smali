.class public Lcom/squareup/cardreader/ble/RealBleScanResult;
.super Ljava/lang/Object;
.source "RealBleScanResult.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleScanResult;


# instance fields
.field private final bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private final isLookingToPair:Z


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;Z)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 13
    iput-boolean p2, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->isLookingToPair:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 41
    instance-of v0, p1, Lcom/squareup/cardreader/WirelessConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    check-cast p1, Lcom/squareup/cardreader/WirelessConnection;

    .line 42
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    .line 41
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()I
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->hashCode()I

    move-result v0

    return v0
.end method

.method public isLookingToPair()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/RealBleScanResult;->isLookingToPair:Z

    return v0
.end method
