.class public Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;
.super Ljava/lang/Object;
.source "BleAction.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReconnectDueToGattError"
.end annotation


# instance fields
.field public final description:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;->description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describe()Ljava/lang/String;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;->description:Ljava/lang/String;

    return-object v0
.end method
