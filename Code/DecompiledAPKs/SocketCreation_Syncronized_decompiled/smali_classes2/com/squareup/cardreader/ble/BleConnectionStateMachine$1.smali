.class Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;
.super Ljava/lang/Object;
.source "BleConnectionStateMachine.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->newInitializeHelper()Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize()Landroid/bluetooth/BluetoothGatt;
    .locals 5

    .line 542
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->access$600(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->access$300(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Landroid/app/Application;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->access$400(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleConnectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/ble/BleConnectType;->isAutoConnect()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    .line 543
    invoke-static {v3}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->access$500(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleReceiverFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-virtual {v3, v4}, Lcom/squareup/cardreader/ble/BleReceiverFactory;->newInstance(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Landroid/bluetooth/BluetoothGattCallback;

    move-result-object v3

    .line 542
    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    return-object v0
.end method

.method public onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 1

    .line 551
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void
.end method
