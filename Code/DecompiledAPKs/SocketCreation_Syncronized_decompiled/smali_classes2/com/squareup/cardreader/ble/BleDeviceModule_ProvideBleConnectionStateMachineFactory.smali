.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleConnectionStateMachineFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleConnectionStateMachine;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final bleBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final bleHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private final bleReceiverFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleReceiverFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final bleSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;"
        }
    .end annotation
.end field

.field private final bleThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothDeviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleReceiverFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleBackendProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleHandlerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleReceiverFactoryProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleSenderProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bluetoothDeviceProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p13, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleThreadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleReceiverFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;"
        }
    .end annotation

    .line 92
    new-instance v14, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static provideBleConnectionStateMachine(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Landroid/os/Handler;Lcom/squareup/cardreader/ble/BleReceiverFactory;Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/cardreader/ble/BleConnectionStateMachine;
    .locals 0

    .line 101
    invoke-static/range {p0 .. p12}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleConnectionStateMachine(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Landroid/os/Handler;Lcom/squareup/cardreader/ble/BleReceiverFactory;Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleConnectionStateMachine;
    .locals 14

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleBackendProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/os/Handler;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleReceiverFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/ble/BleReceiverFactory;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/ble/BleSender;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bluetoothDeviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/cardreader/CardReaderId;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->bleThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static/range {v1 .. v13}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->provideBleConnectionStateMachine(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Landroid/os/Handler;Lcom/squareup/cardreader/ble/BleReceiverFactory;Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->get()Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    move-result-object v0

    return-object v0
.end method
