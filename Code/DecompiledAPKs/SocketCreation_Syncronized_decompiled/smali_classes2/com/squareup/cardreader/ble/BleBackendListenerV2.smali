.class public final Lcom/squareup/cardreader/ble/BleBackendListenerV2;
.super Ljava/lang/Object;
.source "BleBackendListenerV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;,
        Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002%&BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0006\u0010\u0018\u001a\u00020\u0019J\u0006\u0010\u001a\u001a\u00020\u0019J\u0008\u0010\u001b\u001a\u00020\u0019H\u0016J\u0008\u0010\u001c\u001a\u00020\u0019H\u0016J\u0008\u0010\u001d\u001a\u00020\u0019H\u0016J\u0018\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u0015\u001a\u00020\u00112\u0006\u0010 \u001a\u00020!H\u0002J\u0010\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020$H\u0016R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/BleBackendListenerV2;",
        "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
        "bleConnectionStateMachineV2",
        "Lcom/squareup/cardreader/ble/StateMachineV2;",
        "bleBackend",
        "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/RealCardReaderListeners;",
        "wirelessConnection",
        "Lcom/squareup/cardreader/WirelessConnection;",
        "cardReaderFactory",
        "Lcom/squareup/cardreader/CardReaderFactory;",
        "cardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "bluetoothUtils",
        "Lcom/squareup/cardreader/BluetoothUtils;",
        "autoConnect",
        "",
        "(Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;Z)V",
        "bleDevice",
        "Lcom/squareup/dipper/events/BleDevice;",
        "disconnectRequested",
        "disposable",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "connect",
        "",
        "disconnect",
        "forgetBond",
        "readAckVector",
        "readMtu",
        "reconnectMode",
        "Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;",
        "state",
        "Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;",
        "writeData",
        "data",
        "",
        "NoReconnectReason",
        "ReconnectMode",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoConnect:Z

.field private final bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

.field private final bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

.field private final bleDevice:Lcom/squareup/dipper/events/BleDevice;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

.field private disconnectRequested:Z

.field private final disposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;Z)V
    .locals 1

    const-string v0, "bleConnectionStateMachineV2"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bleBackend"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderListeners"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "wirelessConnection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderId"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothUtils"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    iput-boolean p8, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->autoConnect:Z

    .line 70
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 71
    new-instance p1, Lcom/squareup/dipper/events/BleDevice;

    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {p2}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {p3}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lcom/squareup/dipper/events/BleDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleDevice:Lcom/squareup/dipper/events/BleDevice;

    return-void
.end method

.method public static final synthetic access$getAutoConnect$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->autoConnect:Z

    return p0
.end method

.method public static final synthetic access$getBleBackend$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    return-object p0
.end method

.method public static final synthetic access$getBleDevice$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/dipper/events/BleDevice;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleDevice:Lcom/squareup/dipper/events/BleDevice;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderFactory$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderFactory;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderId$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderId;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    return-object p0
.end method

.method public static final synthetic access$getDisconnectRequested$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Z
    .locals 0

    .line 60
    iget-boolean p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disconnectRequested:Z

    return p0
.end method

.method public static final synthetic access$getDisposable$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lio/reactivex/disposables/CompositeDisposable;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    return-object p0
.end method

.method public static final synthetic access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    return-object p0
.end method

.method public static final synthetic access$reconnectMode(Lcom/squareup/cardreader/ble/BleBackendListenerV2;ZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->reconnectMode(ZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setDisconnectRequested$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;Z)V
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disconnectRequested:Z

    return-void
.end method

.method private final reconnectMode(ZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;
    .locals 4

    .line 212
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez v0, :cond_0

    new-instance p1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    sget-object p2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->BLUETOOTH_DISABLED:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-direct {p1, p2, v2, v1, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_3

    :cond_0
    if-eqz p1, :cond_1

    .line 215
    new-instance p1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    sget-object p2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->REQUESTED_DISCONNECTION:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-direct {p1, p2, v2, v1, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_3

    .line 219
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->autoConnect:Z

    if-nez p1, :cond_3

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionError;->getConnectionEvent()Lcom/squareup/cardreader/ble/Error;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v3

    :goto_0
    sget-object v0, Lcom/squareup/cardreader/ble/Error;->REMOVED_BOND:Lcom/squareup/cardreader/ble/Error;

    if-eq p1, v0, :cond_3

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getErrorBeforeConnection()Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    sget-object p2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->PAIRING_MODE:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-direct {p1, p2, v2, v1, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_3

    .line 222
    :cond_3
    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/squareup/blecoroutines/BleError;->getEvent()Lcom/squareup/blecoroutines/Event;

    move-result-object p1

    goto :goto_1

    :cond_4
    move-object p1, v3

    :goto_1
    sget-object v0, Lcom/squareup/blecoroutines/Event;->CREATE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    if-ne p1, v0, :cond_5

    new-instance p1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    sget-object p2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->ERROR_BONDING:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-direct {p1, p2, v2, v1, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_3

    .line 225
    :cond_5
    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionError;->getConnectionEvent()Lcom/squareup/cardreader/ble/Error;

    move-result-object p1

    goto :goto_2

    :cond_6
    move-object p1, v3

    :goto_2
    sget-object p2, Lcom/squareup/cardreader/ble/Error;->SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/Error;

    if-ne p1, p2, :cond_7

    .line 226
    new-instance p1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    sget-object p2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->SERVICE_VERSION:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-direct {p1, p2, v2, v1, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_3

    .line 228
    :cond_7
    new-instance p1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    const/4 p2, 0x1

    invoke-direct {p1, v3, p2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;Z)V

    :goto_3
    return-object p1
.end method


# virtual methods
.method public final connect()V
    .locals 4

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {v1}, Lcom/squareup/cardreader/WirelessConnection;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    const-string/jumbo v2, "wirelessConnection.bluetoothDevice"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/BluetoothUtils;->isConnectedBle(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    new-instance v1, Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleDevice:Lcom/squareup/dipper/events/BleDevice;

    invoke-direct {v1, v2}, Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;-><init>(Lcom/squareup/dipper/events/BleDevice;)V

    check-cast v1, Lcom/squareup/dipper/events/DipperEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 84
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getConnectionState()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;

    invoke-direct {v2, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 142
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getData()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$2;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-direct {v2, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$2;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 143
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getMtu()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$3;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-direct {v2, v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$3;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 144
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getLoggingState()Lio/reactivex/Observable;

    move-result-object v1

    .line 145
    sget-object v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;->INSTANCE:Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    new-instance v3, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 146
    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;

    invoke-direct {v2, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->scan(Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    .line 152
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 153
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getDeviceInfo()Lio/reactivex/Observable;

    move-result-object v1

    .line 154
    const-class v2, Lcom/squareup/cardreader/ble/DeviceInfo$Serial;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    .line 155
    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$6;

    invoke-direct {v2, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$6;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 153
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 159
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getRssi()Lio/reactivex/Observable;

    move-result-object v1

    .line 160
    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;

    invoke-direct {v2, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 159
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 164
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/StateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/ConnectionEvents;->getConnectionInterval()Lio/reactivex/Observable;

    move-result-object v1

    .line 165
    new-instance v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$8;

    invoke-direct {v2, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$8;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 164
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 169
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/StateMachineV2;->connect()V

    return-void
.end method

.method public final disconnect()V
    .locals 1

    const/4 v0, 0x1

    .line 173
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disconnectRequested:Z

    .line 174
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/StateMachineV2;->disconnect()V

    return-void
.end method

.method public forgetBond()V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/StateMachineV2;->forgetBond()V

    return-void
.end method

.method public readAckVector()V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/StateMachineV2;->readAckVector()Lio/reactivex/Single;

    move-result-object v0

    .line 183
    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$readAckVector$1;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$readAckVector$1;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public readMtu()V
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/StateMachineV2;->readMtu()Lio/reactivex/Single;

    move-result-object v0

    .line 188
    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$readMtu$1;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$readMtu$1;-><init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public writeData([B)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->bleConnectionStateMachineV2:Lcom/squareup/cardreader/ble/StateMachineV2;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ble/StateMachineV2;->writeData([B)V

    return-void
.end method
