.class public final Lcom/squareup/cardreader/ble/Factory;
.super Ljava/lang/Object;
.source "Factory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J@\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007J8\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/Factory;",
        "",
        "()V",
        "create",
        "Lcom/squareup/cardreader/ble/StateMachineV2;",
        "context",
        "Landroid/content/Context;",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "autoConnect",
        "",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "backgroundDispatcher",
        "connectable",
        "Lcom/squareup/blecoroutines/Connectable;",
        "createForTests",
        "timeouts",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardreader/ble/Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/cardreader/ble/Factory;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/Factory;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/ble/Factory;->INSTANCE:Lcom/squareup/cardreader/ble/Factory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final create(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;ZLcom/squareup/thread/executor/MainThread;Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;)Lcom/squareup/cardreader/ble/StateMachineV2;
    .locals 14
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p3

    const-string v1, "context"

    move-object v3, p0

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "device"

    move-object v4, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mainThread"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mainDispatcher"

    move-object/from16 v2, p4

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "backgroundDispatcher"

    move-object/from16 v7, p5

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "connectable"

    move-object/from16 v8, p6

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    .line 25
    new-instance v5, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    invoke-direct {v5, v0}, Lcom/squareup/cardreader/ble/RealConnectionEvents;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c0

    const/4 v13, 0x0

    move-object v2, v1

    move/from16 v6, p2

    .line 24
    invoke-direct/range {v2 .. v13}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;ZLkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/cardreader/ble/ConnectionNegotiator;Lcom/squareup/cardreader/ble/ConnectionManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/cardreader/ble/StateMachineV2;

    return-object v1
.end method

.method public static final createForTests(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;ZLcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/blecoroutines/Connectable;)Lcom/squareup/cardreader/ble/StateMachineV2;
    .locals 14
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p3

    const-string v1, "context"

    move-object v3, p0

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "device"

    move-object v4, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mainThread"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "timeouts"

    move-object/from16 v9, p4

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "connectable"

    move-object/from16 v8, p5

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    .line 40
    new-instance v5, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    invoke-direct {v5, v0}, Lcom/squareup/cardreader/ble/RealConnectionEvents;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getDefault()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v7

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x180

    const/4 v13, 0x0

    move-object v2, v1

    move/from16 v6, p2

    .line 39
    invoke-direct/range {v2 .. v13}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;ZLkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/cardreader/ble/ConnectionNegotiator;Lcom/squareup/cardreader/ble/ConnectionManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/cardreader/ble/StateMachineV2;

    return-object v1
.end method
