.class public final Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_ProvideCardReaderContextFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderContext;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderDispatchProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderGraphInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideCardReaderContext(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 0

    .line 55
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReaderModule;->provideCardReaderContext(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderContext;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderContext;
    .locals 5

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderId;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/CardReaderDispatch;

    iget-object v4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->cardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->provideCardReaderContext(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->get()Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v0

    return-object v0
.end method
