.class public Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
.super Ljava/lang/Object;
.source "CardReaderBatteryInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderBatteryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

.field private current:Ljava/lang/Integer;

.field private isCritical:Ljava/lang/Boolean;

.field private percentage:Ljava/lang/Integer;

.field private temperature:Ljava/lang/Integer;

.field private voltage:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->access$600(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->percentage:Ljava/lang/Integer;

    .line 70
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->access$700(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->current:Ljava/lang/Integer;

    .line 71
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->access$800(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->voltage:Ljava/lang/Integer;

    .line 72
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->access$900(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->temperature:Ljava/lang/Integer;

    .line 73
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->access$1000(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->isCritical:Ljava/lang/Boolean;

    .line 74
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->access$1100(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->percentage:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->current:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->voltage:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->temperature:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->isCritical:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;)Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/CardReaderBatteryInfo;
    .locals 2

    .line 108
    new-instance v0, Lcom/squareup/cardreader/CardReaderBatteryInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;-><init>(Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;Lcom/squareup/cardreader/CardReaderBatteryInfo$1;)V

    return-object v0
.end method

.method public setBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->batteryMode:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-object p0
.end method

.method public setCritical(Ljava/lang/Boolean;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->isCritical:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setCurrent(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->current:Ljava/lang/Integer;

    return-object p0
.end method

.method public setPercentage(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->percentage:Ljava/lang/Integer;

    return-object p0
.end method

.method public setTemperature(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->temperature:Ljava/lang/Integer;

    return-object p0
.end method

.method public setVoltage(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->voltage:Ljava/lang/Integer;

    return-object p0
.end method
