.class public final Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmvApplication"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;,
        Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADF_NAME:Lokio/ByteString;

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adf_name:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1809
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1813
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->DEFAULT_ADF_NAME:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;)V
    .locals 1

    .line 1830
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;-><init>(Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1834
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1835
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    .line 1836
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1851
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1852
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    .line 1853
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    .line 1854
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    .line 1855
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1860
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1862
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1863
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1864
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1865
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;
    .locals 2

    .line 1841
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;-><init>()V

    .line 1842
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->adf_name:Lokio/ByteString;

    .line 1843
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->label:Ljava/lang/String;

    .line 1844
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1808
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1872
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1873
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", adf_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1874
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EmvApplication{"

    .line 1875
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
