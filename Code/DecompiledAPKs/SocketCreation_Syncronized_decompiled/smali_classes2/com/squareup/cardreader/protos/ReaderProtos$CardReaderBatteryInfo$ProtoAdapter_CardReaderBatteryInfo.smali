.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CardReaderBatteryInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6150
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6177
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;-><init>()V

    .line 6178
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 6179
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 6195
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 6188
    :pswitch_0
    :try_start_0
    sget-object v4, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    invoke-virtual {v0, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->battery_mode(Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 6190
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 6185
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->is_critical(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    goto :goto_0

    .line 6184
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->temperature(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    goto :goto_0

    .line 6183
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->voltage(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    goto :goto_0

    .line 6182
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->current(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    goto :goto_0

    .line 6181
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->percentage(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    goto :goto_0

    .line 6199
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 6200
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6148
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6166
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6167
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6168
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6169
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6170
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6171
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6172
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6148
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)I
    .locals 4

    .line 6155
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 6156
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 6157
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 6158
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 6159
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v3, 0x6

    .line 6160
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6161
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 6148
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
    .locals 0

    .line 6205
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 6206
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 6207
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 6148
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    move-result-object p1

    return-object p1
.end method
