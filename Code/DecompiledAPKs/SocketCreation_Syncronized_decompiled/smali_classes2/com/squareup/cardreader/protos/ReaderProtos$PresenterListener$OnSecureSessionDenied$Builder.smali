.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5094
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;
    .locals 3

    .line 5104
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5091
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    move-result-object v0

    return-object v0
.end method

.method public server_deny_type(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;
    .locals 0

    .line 5098
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0
.end method
