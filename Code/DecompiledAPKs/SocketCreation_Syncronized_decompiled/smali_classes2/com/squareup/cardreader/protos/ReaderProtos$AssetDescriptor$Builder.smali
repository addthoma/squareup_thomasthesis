.class public final Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_blocking:Ljava/lang/Boolean;

.field public requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public size:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2088
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
    .locals 5

    .line 2108
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->is_blocking:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    iget-object v3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->size:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;-><init>(Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2081
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public is_blocking(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
    .locals 0

    .line 2092
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->is_blocking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public requires_reboot(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
    .locals 0

    .line 2097
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0
.end method

.method public size(Ljava/lang/Long;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
    .locals 0

    .line 2102
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->size:Ljava/lang/Long;

    return-object p0
.end method
