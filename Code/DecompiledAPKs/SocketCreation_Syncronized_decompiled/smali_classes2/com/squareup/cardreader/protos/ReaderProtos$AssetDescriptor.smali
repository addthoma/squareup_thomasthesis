.class public final Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AssetDescriptor"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;,
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IS_BLOCKING:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRES_REBOOT:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public static final DEFAULT_SIZE:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final is_blocking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$Reboot#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final size:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1998
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 2002
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->DEFAULT_IS_BLOCKING:Ljava/lang/Boolean;

    .line 2004
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->NO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->DEFAULT_REQUIRES_REBOOT:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const-wide/16 v0, 0x0

    .line 2006
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->DEFAULT_SIZE:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Ljava/lang/Long;)V
    .locals 1

    .line 2027
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;-><init>(Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 2032
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2033
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    .line 2034
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 2035
    iput-object p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2051
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2052
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    .line 2053
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    .line 2054
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 2055
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    .line 2056
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2061
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 2063
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2064
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2065
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2066
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 2067
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
    .locals 2

    .line 2040
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;-><init>()V

    .line 2041
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->is_blocking:Ljava/lang/Boolean;

    .line 2042
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 2043
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->size:Ljava/lang/Long;

    .line 2044
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1997
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2074
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2075
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", is_blocking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2076
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    if-eqz v1, :cond_1

    const-string v1, ", requires_reboot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2077
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AssetDescriptor{"

    .line 2078
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
