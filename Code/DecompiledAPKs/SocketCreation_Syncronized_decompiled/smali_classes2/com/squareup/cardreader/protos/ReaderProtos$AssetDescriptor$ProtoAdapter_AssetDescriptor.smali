.class final Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AssetDescriptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2114
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2135
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;-><init>()V

    .line 2136
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2137
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 2150
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2148
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->size(Ljava/lang/Long;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    goto :goto_0

    .line 2142
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    invoke-virtual {v0, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->requires_reboot(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 2144
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 2139
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->is_blocking(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    goto :goto_0

    .line 2154
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2155
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2112
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2127
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2128
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2129
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2130
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2112
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)I
    .locals 4

    .line 2119
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v3, 0x2

    .line 2120
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 2121
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2122
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2112
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
    .locals 0

    .line 2160
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    move-result-object p1

    .line 2161
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2162
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2112
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$ProtoAdapter_AssetDescriptor;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object p1

    return-object p1
.end method
