.class public abstract Lcom/squareup/cardreader/LcrModule;
.super Ljava/lang/Object;
.source "LcrModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/LcrModule$Real;,
        Lcom/squareup/cardreader/LcrModule$NativeLoggingEnabled;,
        Lcom/squareup/cardreader/LcrModule$SquidTouchReporting;,
        Lcom/squareup/cardreader/LcrModule$LCR;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$providePaymentFeatureDelegate$0()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 2

    .line 152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fake cardreader pointer for LegacyCardreaders - this should not be called!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static provideAudioBackendNative()Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 211
    new-instance v0, Lcom/squareup/cardreader/lcr/AudioBackendNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/AudioBackendNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideBleBackendNative()Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 270
    new-instance v0, Lcom/squareup/cardreader/lcr/BleBackendNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/BleBackendNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideCardReaderPointer(Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 201
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->getCardreader()Lcom/squareup/cardreader/CardReaderPointer;

    move-result-object p0

    return-object p0
.end method

.method static provideCardreaderNative()Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 206
    new-instance v0, Lcom/squareup/cardreader/lcr/CardreaderNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/CardreaderNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideCoreDumpFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;)Lcom/squareup/cardreader/CoreDumpFeatureLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/CoreDumpFeatureLegacy;"
        }
    .end annotation

    .line 101
    new-instance v0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;)V

    return-object v0
.end method

.method static provideCoredumpFeatureNative()Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 235
    new-instance v0, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideEventLogFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;)Lcom/squareup/cardreader/EventLogFeatureLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;"
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/cardreader/EventLogFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/EventLogFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;)V

    return-object v0
.end method

.method static provideEventlogFeatureNative()Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 245
    new-instance v0, Lcom/squareup/cardreader/lcr/EventlogFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideFirmwareUpdateFeatureNative()Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 230
    new-instance v0, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideLogNative()Lcom/squareup/cardreader/lcr/LogNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 225
    new-instance v0, Lcom/squareup/cardreader/lcr/LogNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/LogNativePassthrough;-><init>()V

    return-object v0
.end method

.method static providePaymentFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;Lcom/squareup/tmn/TmnTimings;Lcom/squareup/cardreader/PaymentFeatureV2;)Lcom/squareup/cardreader/PaymentFeatureLegacy;
    .locals 9
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            "Lcom/squareup/tmn/TmnTimings;",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ")",
            "Lcom/squareup/cardreader/PaymentFeatureLegacy;"
        }
    .end annotation

    .line 163
    new-instance v8, Lcom/squareup/cardreader/PaymentFeatureLegacy;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/PaymentFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tmn/TmnTimings;Lcom/squareup/cardreader/PaymentFeature;)V

    return-object v8
.end method

.method static providePaymentFeatureDelegate(Lcom/squareup/cardreader/PaymentFeatureDelegateSender;)Lcom/squareup/cardreader/PaymentFeatureV2;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 151
    sget-object v0, Lcom/squareup/cardreader/-$$Lambda$LcrModule$DV_6fHO0lIt-h2hdq4qE9aVCaTg;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$LcrModule$DV_6fHO0lIt-h2hdq4qE9aVCaTg;

    .line 155
    new-instance v1, Lcom/squareup/cardreader/PaymentFeatureV2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/cardreader/PaymentFeatureV2;-><init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V

    return-object v1
.end method

.method static providePaymentFeatureNative()Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 250
    new-instance v0, Lcom/squareup/cardreader/lcr/PaymentFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static providePowerFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;)Lcom/squareup/cardreader/PowerFeatureLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;"
        }
    .end annotation

    .line 170
    new-instance v0, Lcom/squareup/cardreader/PowerFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/PowerFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;)V

    return-object v0
.end method

.method static providePowerFeatureNative()Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 240
    new-instance v0, Lcom/squareup/cardreader/lcr/PowerFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/PowerFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideRes(Landroid/app/Application;)Lcom/squareup/util/Res;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 129
    invoke-virtual {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 130
    new-instance v0, Lcom/squareup/util/Res$RealRes;

    invoke-direct {v0, p0}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    return-object v0
.end method

.method static provideSecureSessionFeature(Landroid/app/Application;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;Lcom/squareup/cardreader/SecureSessionRevocationState;Z)Lcom/squareup/cardreader/SecureSessionFeatureLegacy;
    .locals 7
    .param p6    # Z
        .annotation runtime Lcom/squareup/cardreader/dagger/IsReaderSdkAppForDipper;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/ms/MinesweeperTicket;",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            "Z)",
            "Lcom/squareup/cardreader/SecureSessionFeatureLegacy;"
        }
    .end annotation

    .line 111
    new-instance p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;Lcom/squareup/cardreader/SecureSessionRevocationState;Z)V

    return-object p0
.end method

.method static provideSecureSessionFeatureNative()Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 255
    new-instance v0, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideSecureSessionRevocationFeature(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/cardreader/SecureSessionRevocationState;)Lcom/squareup/cardreader/SecureSessionRevocationFeature;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ")",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;"
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/SecureSessionRevocationState;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method static provideSecureSessionRevocationState()Lcom/squareup/cardreader/SecureSessionRevocationState;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 124
    new-instance v0, Lcom/squareup/cardreader/SecureSessionRevocationState;

    invoke-direct {v0}, Lcom/squareup/cardreader/SecureSessionRevocationState;-><init>()V

    return-object v0
.end method

.method static provideSecureTouchCardReaderFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;Lcom/squareup/securetouch/TouchReporting;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/NativeBinaries;Lio/reactivex/Scheduler;)Lcom/squareup/cardreader/SecureTouchFeatureInterface;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            "Lcom/squareup/securetouch/TouchReporting;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/cardreader/NativeBinaries;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;"
        }
    .end annotation

    .line 139
    sget-object v0, Lcom/squareup/cardreader/NativeBinaries;->SQUID:Lcom/squareup/cardreader/NativeBinaries;

    if-ne p4, v0, :cond_0

    .line 140
    new-instance p4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;

    move-object v1, p4

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;Lcom/squareup/securetouch/TouchReporting;Ljava/util/concurrent/ExecutorService;Lio/reactivex/Scheduler;)V

    return-object p4

    .line 144
    :cond_0
    new-instance p0, Lcom/squareup/cardreader/SecureTouchFeatureInterface$NoOp;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureTouchFeatureInterface$NoOp;-><init>()V

    return-object p0
.end method

.method static provideSecureTouchFeatureNativeInterface()Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 260
    new-instance v0, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideSystemFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)Lcom/squareup/cardreader/SystemFeatureLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;"
        }
    .end annotation

    .line 176
    new-instance v0, Lcom/squareup/cardreader/SystemFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)V

    return-object v0
.end method

.method static provideSystemFeatureNative()Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 216
    new-instance v0, Lcom/squareup/cardreader/lcr/SystemFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideTamperFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;)Lcom/squareup/cardreader/TamperFeatureLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;"
        }
    .end annotation

    .line 182
    new-instance v0, Lcom/squareup/cardreader/TamperFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/TamperFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;)V

    return-object v0
.end method

.method static provideTamperFeatureNative()Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 221
    new-instance v0, Lcom/squareup/cardreader/lcr/TamperFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/TamperFeatureNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideTimerNative()Lcom/squareup/cardreader/lcr/TimerNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 275
    new-instance v0, Lcom/squareup/cardreader/lcr/TimerNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/TimerNativePassthrough;-><init>()V

    return-object v0
.end method

.method static provideUserInteractionFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;)Lcom/squareup/cardreader/UserInteractionFeatureLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;"
        }
    .end annotation

    .line 189
    new-instance v0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;)V

    return-object v0
.end method

.method static provideUserInteractionnFeatureNative()Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 265
    new-instance v0, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativePassthrough;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativePassthrough;-><init>()V

    return-object v0
.end method
