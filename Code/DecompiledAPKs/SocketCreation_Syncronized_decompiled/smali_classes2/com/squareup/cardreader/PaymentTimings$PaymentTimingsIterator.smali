.class public Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;
.super Ljava/lang/Object;
.source "PaymentTimings.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/PaymentTimings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PaymentTimingsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lcom/squareup/cardreader/PaymentTiming;",
        ">;"
    }
.end annotation


# instance fields
.field private current:I

.field private end:I

.field final synthetic this$0:Lcom/squareup/cardreader/PaymentTimings;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 27
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->this$0:Lcom/squareup/cardreader/PaymentTimings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 28
    iput v0, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->current:I

    .line 29
    invoke-static {p1}, Lcom/squareup/cardreader/PaymentTimings;->access$100(Lcom/squareup/cardreader/PaymentTimings;)[Lcom/squareup/cardreader/PaymentTiming;

    move-result-object p1

    array-length p1, p1

    iput p1, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->end:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/cardreader/PaymentTimings$1;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;-><init>(Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .line 33
    iget v0, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->current:I

    iget v1, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->end:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public next()Lcom/squareup/cardreader/PaymentTiming;
    .locals 3

    .line 37
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->this$0:Lcom/squareup/cardreader/PaymentTimings;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentTimings;->access$100(Lcom/squareup/cardreader/PaymentTimings;)[Lcom/squareup/cardreader/PaymentTiming;

    move-result-object v0

    iget v1, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->current:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->current:I

    aget-object v0, v0, v1

    return-object v0

    .line 38
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;->next()Lcom/squareup/cardreader/PaymentTiming;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
