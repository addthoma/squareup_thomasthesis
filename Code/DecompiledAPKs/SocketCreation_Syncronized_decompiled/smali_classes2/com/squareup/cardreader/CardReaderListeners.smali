.class public interface abstract Lcom/squareup/cardreader/CardReaderListeners;
.super Ljava/lang/Object;
.source "CardReaderListeners.java"


# virtual methods
.method public abstract addBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V
.end method

.method public abstract cardReaderDataEvents()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/dipper/events/CardReaderDataEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dipperEvents()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/dipper/events/DipperEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBlePairingListeners()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/BlePairingListener;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCardReaderStatusListener()Lcom/squareup/cardreader/CardReaderStatusListener;
.end method

.method public abstract getEmvListener()Lcom/squareup/cardreader/EmvListener;
.end method

.method public abstract getFirmwareUpdateListener()Lcom/squareup/cardreader/FirmwareUpdater$Listener;
.end method

.method public abstract getLibraryLoadErrorListener()Lcom/squareup/cardreader/LibraryLoadErrorListener;
.end method

.method public abstract getMagSwipeListener()Lcom/squareup/cardreader/MagSwipeListener;
.end method

.method public abstract getNfcListener()Lcom/squareup/cardreader/NfcListener;
.end method

.method public abstract getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;
.end method

.method public abstract getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;
.end method

.method public abstract getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;
.end method

.method public abstract isSecureTouchEnabled()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
.end method

.method public abstract paymentFeatureMessages()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V
.end method

.method public abstract secureTouchEvents()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCardReaderStatusListener(Lcom/squareup/cardreader/CardReaderStatusListener;)V
.end method

.method public abstract setEmvListener(Lcom/squareup/cardreader/EmvListener;)V
.end method

.method public abstract setFirmwareUpdateListener(Lcom/squareup/cardreader/FirmwareUpdater$Listener;)V
.end method

.method public abstract setLibraryLoadErrorListener(Lcom/squareup/cardreader/LibraryLoadErrorListener;)V
.end method

.method public abstract setMagSwipeListener(Lcom/squareup/cardreader/MagSwipeListener;)V
.end method

.method public abstract setNfcListener(Lcom/squareup/cardreader/NfcListener;)V
.end method

.method public abstract setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V
.end method

.method public abstract setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V
.end method

.method public abstract setReaderEventLogger(Lcom/squareup/cardreader/ReaderEventLogger;)V
.end method

.method public abstract tmnEvents()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract unsetCardReaderStatusListener()V
.end method

.method public abstract unsetEmvListener()V
.end method

.method public abstract unsetFirmwareUpdateListener()V
.end method

.method public abstract unsetLibraryLoadErrorListener()V
.end method

.method public abstract unsetMagSwipeListener()V
.end method

.method public abstract unsetNfcListener()V
.end method

.method public abstract unsetPaymentCompletionListener()V
.end method

.method public abstract unsetPinRequestListener()V
.end method
