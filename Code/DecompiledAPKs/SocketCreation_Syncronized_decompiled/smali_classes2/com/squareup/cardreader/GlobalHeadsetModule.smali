.class public Lcom/squareup/cardreader/GlobalHeadsetModule;
.super Ljava/lang/Object;
.source "GlobalHeadsetModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideHeadset(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/Headset;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/wavpool/swipe/Headset;

    invoke-direct {v0, p1, p2}, Lcom/squareup/wavpool/swipe/Headset;-><init>(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method provideHeadsetConnectionListener(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p1
.end method

.method provideHeadsetConnectionState(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 33
    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/Headset;->currentState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object p1

    return-object p1
.end method

.method provideHeadsetListener(Lcom/squareup/cardreader/HeadsetStateDispatcher;)Lcom/squareup/wavpool/swipe/Headset$Listener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p1
.end method
