.class public Lcom/squareup/cardreader/dagger/CardReaderStoreModule;
.super Ljava/lang/Object;
.source "CardReaderStoreModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method providePairedReaderNames(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule$1;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule$1;-><init>(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;)V

    .line 24
    invoke-virtual {v0}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "saved-cardreaders"

    .line 25
    invoke-static {p1, v1, p2, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p1

    return-object p1
.end method
