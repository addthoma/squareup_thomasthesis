.class public final Lcom/squareup/cardreader/CardInfo;
.super Ljava/lang/Object;
.source "CardInfo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardInfo$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u0000 #2\u00020\u0001:\u0001#B7\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000bB7\u0008\u0016\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\rB;\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0010J\u0006\u0010\u001f\u001a\u00020 J\u0006\u0010!\u001a\u00020 J\u0006\u0010\"\u001a\u00020 R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0015\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u001d\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001a\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/cardreader/CardInfo;",
        "",
        "brandId",
        "",
        "last4",
        "",
        "name",
        "application",
        "Lcom/squareup/cardreader/EmvApplication;",
        "magSwipeTrackMask",
        "cvmPerformedCode",
        "(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;II)V",
        "brandShortCode",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;II)V",
        "brand",
        "Lcom/squareup/Card$Brand;",
        "(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;Ljava/lang/Integer;Ljava/lang/Integer;)V",
        "getApplication",
        "()Lcom/squareup/cardreader/EmvApplication;",
        "getBrand",
        "()Lcom/squareup/Card$Brand;",
        "cvmPerformed",
        "Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;",
        "getCvmPerformed",
        "()Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;",
        "getLast4",
        "()Ljava/lang/String;",
        "getMagSwipeTrackMask",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getName",
        "hasTrack1Data",
        "",
        "hasTrack2Data",
        "requiresSignature",
        "Companion",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/CardInfo$Companion;


# instance fields
.field private final application:Lcom/squareup/cardreader/EmvApplication;

.field private final brand:Lcom/squareup/Card$Brand;

.field private final cvmPerformed:Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;

.field private final last4:Ljava/lang/String;

.field private final magSwipeTrackMask:Ljava/lang/Integer;

.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/CardInfo$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/CardInfo$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/CardInfo;->Companion:Lcom/squareup/cardreader/CardInfo$Companion;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;II)V
    .locals 8

    const-string v0, "last4"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/squareup/cardreader/CardInfo;->Companion:Lcom/squareup/cardreader/CardInfo$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardInfo$Companion;->issuerIdToBrand(I)Lcom/squareup/Card$Brand;

    move-result-object v2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 29
    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/CardInfo;-><init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    const-string v0, "brand"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "last4"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/CardInfo;->brand:Lcom/squareup/Card$Brand;

    iput-object p2, p0, Lcom/squareup/cardreader/CardInfo;->last4:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/cardreader/CardInfo;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/cardreader/CardInfo;->application:Lcom/squareup/cardreader/EmvApplication;

    iput-object p5, p0, Lcom/squareup/cardreader/CardInfo;->magSwipeTrackMask:Ljava/lang/Integer;

    if-nez p6, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;

    move-result-object p1

    .line 46
    :goto_0
    iput-object p1, p0, Lcom/squareup/cardreader/CardInfo;->cvmPerformed:Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;II)V
    .locals 8

    const-string v0, "brandShortCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "last4"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/squareup/cardreader/CardInfo;->Companion:Lcom/squareup/cardreader/CardInfo$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardInfo$Companion;->shortCodeToBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object v2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 42
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 40
    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/CardInfo;-><init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/EmvApplication;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public static final issuerIdToBrand(I)Lcom/squareup/Card$Brand;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/cardreader/CardInfo;->Companion:Lcom/squareup/cardreader/CardInfo$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardInfo$Companion;->issuerIdToBrand(I)Lcom/squareup/Card$Brand;

    move-result-object p0

    return-object p0
.end method

.method public static final shortCodeToBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/cardreader/CardInfo;->Companion:Lcom/squareup/cardreader/CardInfo$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardInfo$Companion;->shortCodeToBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getApplication()Lcom/squareup/cardreader/EmvApplication;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->application:Lcom/squareup/cardreader/EmvApplication;

    return-object v0
.end method

.method public final getBrand()Lcom/squareup/Card$Brand;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->brand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public final getCvmPerformed()Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->cvmPerformed:Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;

    return-object v0
.end method

.method public final getLast4()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->last4:Ljava/lang/String;

    return-object v0
.end method

.method public final getMagSwipeTrackMask()Ljava/lang/Integer;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->magSwipeTrackMask:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final hasTrack1Data()Z
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->magSwipeTrackMask:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 54
    sget-object v1, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->CR_MAGSWIPE_TRACK_TYPE_BITMASK_ONE:Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->swigValue()I

    move-result v1

    and-int/2addr v0, v1

    .line 55
    sget-object v1, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->CR_MAGSWIPE_TRACK_TYPE_BITMASK_ONE:Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->swigValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasTrack2Data()Z
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->magSwipeTrackMask:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 59
    sget-object v1, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->CR_MAGSWIPE_TRACK_TYPE_BITMASK_TWO:Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->swigValue()I

    move-result v1

    and-int/2addr v0, v1

    .line 60
    sget-object v1, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->CR_MAGSWIPE_TRACK_TYPE_BITMASK_TWO:Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrMagswipeTrackTypeBitmask;->swigValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final requiresSignature()Z
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/CardInfo;->cvmPerformed:Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/cardreader/CardInfo$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrCardholderVerificationPerformed;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    return v1
.end method
