.class Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;
.super Ljava/lang/Object;
.source "CardReaderSwig.java"

# interfaces
.implements Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalPaymentListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderSwig;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;)V
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onAccountSelectionRequired$2$CardReaderSwig$InternalPaymentListener(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onAudioRequest$7$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
    .locals 1

    .line 431
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V

    return-void
.end method

.method public synthetic lambda$onAudioVisualRequest$8$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V

    return-void
.end method

.method public synthetic lambda$onCardPresenceChange$0$CardReaderSwig$InternalPaymentListener(Z)V
    .locals 1

    .line 372
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;->onCardPresenceChange(Z)V

    return-void
.end method

.method public synthetic lambda$onCardholderNameReceived$17$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 509
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public synthetic lambda$onDisplayRequest$6$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 424
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onICCApproved$12$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 7

    .line 472
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;->onSmartPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$onICCDeclined$14$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 490
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;->onSmartPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$onICCReversed$13$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 481
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;->onSmartPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$onListApplications$1$CardReaderSwig$InternalPaymentListener([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public synthetic lambda$onMagSwipePassthrough$16$CardReaderSwig$InternalPaymentListener(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public synthetic lambda$onMagswipeFailure$19$CardReaderSwig$InternalPaymentListener(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    .line 520
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onMagSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public synthetic lambda$onMagswipeFallbackSuccess$21$CardReaderSwig$InternalPaymentListener(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 1

    .line 532
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method public synthetic lambda$onMagswipeSuccess$18$CardReaderSwig$InternalPaymentListener(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 514
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onMagSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public synthetic lambda$onPaymentNfcTimedOut$22$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 538
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;->onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$onPaymentTerminated$15$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 498
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;->onSmartPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$onSendAuthorization$9$CardReaderSwig$InternalPaymentListener([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public synthetic lambda$onSendContactlessAuthorization$10$CardReaderSwig$InternalPaymentListener([BLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->sendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public synthetic lambda$onSendTmnAuthorization$11$CardReaderSwig$InternalPaymentListener([B)V
    .locals 1

    .line 458
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->sendTmnAuthorization([B)V

    return-void
.end method

.method public synthetic lambda$onSwipeForFallback$20$CardReaderSwig$InternalPaymentListener(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 1

    .line 525
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    return-void
.end method

.method public synthetic lambda$onTmnDataToTmn$3$CardReaderSwig$InternalPaymentListener(Ljava/lang/String;[B)V
    .locals 1

    .line 401
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onTmnDataToTmn(Ljava/lang/String;[B)V

    return-void
.end method

.method public synthetic lambda$onTmnTransactionComplete$4$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 409
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public synthetic lambda$onTmnWriteNotify$5$CardReaderSwig$InternalPaymentListener(II[B)V
    .locals 1

    .line 416
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;->onTmnWriteNotify(II[B)V

    return-void
.end method

.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 393
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$s5hVTPZ9Ier2jokbJ7fLByonBeU;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$s5hVTPZ9Ier2jokbJ7fLByonBeU;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
    .locals 2

    .line 430
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$PCfE4EQWJNLXLNzWmqkma9altCE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$PCfE4EQWJNLXLNzWmqkma9altCE;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 2

    .line 435
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$aNc7KXRiOsz_L10kSkoDrmmojJY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$aNc7KXRiOsz_L10kSkoDrmmojJY;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCardError()V
    .locals 3

    .line 382
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$xToclOoHKxdPTtyOX2zi2qojilk;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$xToclOoHKxdPTtyOX2zi2qojilk;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCardPresenceChange(Z)V
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rbMXiwhGSf49D6sGUP4A016jblU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rbMXiwhGSf49D6sGUP4A016jblU;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Z)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 3

    .line 377
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$FcgGgfbKfq4CRVcppzmIhg2IXys;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$FcgGgfbKfq4CRVcppzmIhg2IXys;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 2

    .line 508
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$bPemKX2uBc4kEs5aHAz7AjAGs0s;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$bPemKX2uBc4kEs5aHAz7AjAGs0s;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/CardInfo;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$tW2h99ryNMLANFwCFyK__EOEmac;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$tW2h99ryNMLANFwCFyK__EOEmac;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onICCApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 9

    .line 471
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v8, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$2QT3BMTCibb5gZdSAlEIUjqF75U;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$2QT3BMTCibb5gZdSAlEIUjqF75U;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v8, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onICCDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 8

    .line 489
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v7, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v7, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onICCReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 8

    .line 480
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v7, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$LFwwU97ELxGzXCTEmhe4GSQqdPs;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$LFwwU97ELxGzXCTEmhe4GSQqdPs;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v7, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$_kee8vsq7ha4Vc164ovJDtRigP4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$_kee8vsq7ha4Vc164ovJDtRigP4;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;[Lcom/squareup/cardreader/EmvApplication;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 503
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$V6gZZayr3ZfFZ3z_L1fr591aabA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$V6gZZayr3ZfFZ3z_L1fr591aabA;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onMagswipeFailure(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 2

    .line 519
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$2FzyCbYpLR1rtw55ygHgx8jrgKo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$2FzyCbYpLR1rtw55ygHgx8jrgKo;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 2

    .line 531
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$EdTs_9o-reL098EB3XjSKsxRxbg;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onMagswipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 514
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$mn6yfSQtX0iEqnSZQuANDS11D-w;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$mn6yfSQtX0iEqnSZQuANDS11D-w;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcCollision()V
    .locals 3

    .line 569
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$rtsLjxOdON8yz66yXi-LZHU_EHU;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$rtsLjxOdON8yz66yXi-LZHU_EHU;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcInterfaceUnavailable()V
    .locals 3

    .line 543
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$uMDchcf0acHJRXoDHg1OMx9GWT4;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$uMDchcf0acHJRXoDHg1OMx9GWT4;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcLimitExceededInsertCard()V
    .locals 3

    .line 579
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$uj4eyBJRwbAg6O2QID3HFgoRiPw;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$uj4eyBJRwbAg6O2QID3HFgoRiPw;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcLimitExceededTryAnotherCard()V
    .locals 3

    .line 574
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$jkNOWb5PRDzHFUd-pdPGqasTl9Y;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$jkNOWb5PRDzHFUd-pdPGqasTl9Y;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcSeePaymentDeviceForInstructions()V
    .locals 3

    .line 558
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    .line 559
    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$v0OU1_t8atMktAA3iirG2k9t3q8;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$v0OU1_t8atMktAA3iirG2k9t3q8;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    .line 558
    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcTryAgain()V
    .locals 3

    .line 553
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$wRnkfyQoaPKrT2xKschPDnJZCSE;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$wRnkfyQoaPKrT2xKschPDnJZCSE;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcTryAnotherCard()V
    .locals 3

    .line 548
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$sL9ggZrN6Q8zjZivNcanitmJivI;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$sL9ggZrN6Q8zjZivNcanitmJivI;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onNfcUnlockPaymentDevice()V
    .locals 3

    .line 564
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$rB_cMwemwHl0Yxm9Ig5d7OnsLhY;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$rB_cMwemwHl0Yxm9Ig5d7OnsLhY;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 537
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$p56giv-LhEIiyY2IcmiKkW5EQZw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$p56giv-LhEIiyY2IcmiKkW5EQZw;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/PaymentTimings;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 497
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$23Sb2i1GvbUIcf2pQSGdubZWF6g;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$23Sb2i1GvbUIcf2pQSGdubZWF6g;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onRequestTapCard()V
    .locals 3

    .line 584
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$fqcsZvqqC0CVFxeB6ZkDfilyPzE;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$fqcsZvqqC0CVFxeB6ZkDfilyPzE;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 2

    .line 443
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$8ctcc3IvFzzMuM4F2flaXR-G6Ak;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$8ctcc3IvFzzMuM4F2flaXR-G6Ak;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;[BZLcom/squareup/cardreader/CardInfo;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V
    .locals 2

    .line 450
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$_NIhrCWA_v2x8foMkTW5QIf8r2A;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$_NIhrCWA_v2x8foMkTW5QIf8r2A;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;[BLcom/squareup/cardreader/CardInfo;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSendTmnAuthorization([B)V
    .locals 2

    .line 457
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$wWEbtLK9CGG0eXvAWZMd6qdu9Ws;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$wWEbtLK9CGG0eXvAWZMd6qdu9Ws;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;[B)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSigRequested()V
    .locals 3

    .line 463
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$BK3b_4NMXHIu5sMP9USnBg_USAo;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$BK3b_4NMXHIu5sMP9USnBg_USAo;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 2

    .line 525
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$NxblW5E66t9KZXGdWTHsgfRLcqg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$NxblW5E66t9KZXGdWTHsgfRLcqg;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onTmnDataToTmn(Ljava/lang/String;[B)V
    .locals 2

    .line 400
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$WI26byTorSOzSzLW0A0vLpy3jeE;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$WI26byTorSOzSzLW0A0vLpy3jeE;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Ljava/lang/String;[B)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 408
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$lhbd0mt2rCr0duMt7Hp5gEReQvE;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$lhbd0mt2rCr0duMt7Hp5gEReQvE;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onTmnWriteNotify(II[B)V
    .locals 2

    .line 415
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$pP8uj0aKOxDUup9AFAYyrkxEV2s;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$pP8uj0aKOxDUup9AFAYyrkxEV2s;-><init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;II[B)V

    sget-object p1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method

.method public onUseChipCard()V
    .locals 3

    .line 367
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderSwig;->access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$pCdeWZTtfOoxMXrmymz4J_rERMc;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$pCdeWZTtfOoxMXrmymz4J_rERMc;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;)V

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V

    return-void
.end method
