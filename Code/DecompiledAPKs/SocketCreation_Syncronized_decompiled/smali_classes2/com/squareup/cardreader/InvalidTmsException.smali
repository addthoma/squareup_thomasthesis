.class public final Lcom/squareup/cardreader/InvalidTmsException;
.super Ljava/lang/RuntimeException;
.source "InvalidTmsException.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/InvalidTmsException$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00062\u00060\u0001j\u0002`\u0002:\u0001\u0006B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/cardreader/InvalidTmsException;",
        "Ljava/lang/RuntimeException;",
        "Lkotlin/RuntimeException;",
        "errorMessage",
        "",
        "(Ljava/lang/String;)V",
        "Companion",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/InvalidTmsException$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/InvalidTmsException$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/InvalidTmsException$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/InvalidTmsException;->Companion:Lcom/squareup/cardreader/InvalidTmsException$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "errorMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static final remoteLog(Lcom/squareup/CountryCode;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/cardreader/InvalidTmsException;->Companion:Lcom/squareup/cardreader/InvalidTmsException$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/InvalidTmsException$Companion;->remoteLog(Lcom/squareup/CountryCode;)V

    return-void
.end method
