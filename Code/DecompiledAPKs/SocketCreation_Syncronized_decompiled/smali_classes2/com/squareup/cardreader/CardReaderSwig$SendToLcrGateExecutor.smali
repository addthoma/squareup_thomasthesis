.class Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;
.super Ljava/lang/Object;
.source "CardReaderSwig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendToLcrGateExecutor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderSwig;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;)V
    .locals 0

    .line 849
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/CardReaderSwig$1;)V
    .locals 0

    .line 849
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    return-void
.end method


# virtual methods
.method execute(Ljava/lang/Runnable;)V
    .locals 1

    .line 851
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1200(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 855
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1300(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
