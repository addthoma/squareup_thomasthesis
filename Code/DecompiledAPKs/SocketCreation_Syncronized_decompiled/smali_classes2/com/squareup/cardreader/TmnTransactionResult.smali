.class public final enum Lcom/squareup/cardreader/TmnTransactionResult;
.super Ljava/lang/Enum;
.source "TmnTransactionResult.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/TmnTransactionResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0016\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnTransactionResult;",
        "",
        "(Ljava/lang/String;I)V",
        "TMN_RESULT_SUCCESS",
        "TMN_RESULT_CARD_READ_ERROR",
        "TMN_RESULT_DISABLED_CARD",
        "TMN_RESULT_INVALID_BRAND_SELECTION",
        "TMN_RESULT_CANCELLATION",
        "TMN_RESULT_INSUFFICIENT_BALANCE",
        "TMN_RESULT_WAITING_FOR_RETOUCH",
        "TMN_RESULT_TMN_CENTER_ERROR",
        "TMN_RESULT_POLLING_TIMEOUT",
        "TMN_RESULT_IMPOSSIBLE_OPERATION",
        "TMN_RESULT_MULTIPLE_CARDS_DETECTED",
        "TMN_RESULT_EXCEED_THRESHOLD",
        "TMN_RESULT_CENTER_OPERATION_FAILED",
        "TMN_RESULT_INVALID_PARAMETER",
        "TMN_RESULT_SUMMARY_ERROR",
        "TMN_RESULT_DISABLED_TERMINAL",
        "TMN_RESULT_ONLINE_PROCESSING_FAILURE",
        "TMN_RESULT_MIRYO_RESOLUTION_FAILURE",
        "TMN_RESULT_MIRYO_RESULT_FAILURE",
        "TMN_RESULT_OTHER",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_CANCELLATION:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_CARD_READ_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_CENTER_OPERATION_FAILED:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_DISABLED_CARD:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_DISABLED_TERMINAL:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_EXCEED_THRESHOLD:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_IMPOSSIBLE_OPERATION:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_INVALID_BRAND_SELECTION:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_MIRYO_RESOLUTION_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_MULTIPLE_CARDS_DETECTED:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_ONLINE_PROCESSING_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_OTHER:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_POLLING_TIMEOUT:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_SUCCESS:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_SUMMARY_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_TMN_CENTER_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

.field public static final enum TMN_RESULT_WAITING_FOR_RETOUCH:Lcom/squareup/cardreader/TmnTransactionResult;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/cardreader/TmnTransactionResult;

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x0

    const-string v3, "TMN_RESULT_SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_SUCCESS:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x1

    const-string v3, "TMN_RESULT_CARD_READ_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_CARD_READ_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x2

    const-string v3, "TMN_RESULT_DISABLED_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_DISABLED_CARD:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x3

    const-string v3, "TMN_RESULT_INVALID_BRAND_SELECTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_INVALID_BRAND_SELECTION:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x4

    const-string v3, "TMN_RESULT_CANCELLATION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_CANCELLATION:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x5

    const-string v3, "TMN_RESULT_INSUFFICIENT_BALANCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x6

    const-string v3, "TMN_RESULT_WAITING_FOR_RETOUCH"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_WAITING_FOR_RETOUCH:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v2, 0x7

    const-string v3, "TMN_RESULT_TMN_CENTER_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_TMN_CENTER_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0x8

    const-string v3, "TMN_RESULT_POLLING_TIMEOUT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_POLLING_TIMEOUT:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0x9

    const-string v3, "TMN_RESULT_IMPOSSIBLE_OPERATION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_IMPOSSIBLE_OPERATION:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0xa

    const-string v3, "TMN_RESULT_MULTIPLE_CARDS_DETECTED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_MULTIPLE_CARDS_DETECTED:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0xb

    const-string v3, "TMN_RESULT_EXCEED_THRESHOLD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_EXCEED_THRESHOLD:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0xc

    const-string v3, "TMN_RESULT_CENTER_OPERATION_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_CENTER_OPERATION_FAILED:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0xd

    const-string v3, "TMN_RESULT_INVALID_PARAMETER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0xe

    const-string v3, "TMN_RESULT_SUMMARY_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_SUMMARY_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const-string v2, "TMN_RESULT_DISABLED_TERMINAL"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_DISABLED_TERMINAL:Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const-string v2, "TMN_RESULT_ONLINE_PROCESSING_FAILURE"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_ONLINE_PROCESSING_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const-string v2, "TMN_RESULT_MIRYO_RESOLUTION_FAILURE"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_MIRYO_RESOLUTION_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const-string v2, "TMN_RESULT_MIRYO_RESULT_FAILURE"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnTransactionResult;

    const-string v2, "TMN_RESULT_OTHER"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnTransactionResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_OTHER:Lcom/squareup/cardreader/TmnTransactionResult;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/TmnTransactionResult;->$VALUES:[Lcom/squareup/cardreader/TmnTransactionResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/TmnTransactionResult;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/TmnTransactionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TmnTransactionResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/TmnTransactionResult;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/TmnTransactionResult;->$VALUES:[Lcom/squareup/cardreader/TmnTransactionResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/TmnTransactionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/TmnTransactionResult;

    return-object v0
.end method
