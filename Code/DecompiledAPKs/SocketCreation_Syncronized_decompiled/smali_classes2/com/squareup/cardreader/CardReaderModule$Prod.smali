.class public abstract Lcom/squareup/cardreader/CardReaderModule$Prod;
.super Ljava/lang/Object;
.source "CardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/android/util/ClockModule;,
        Lcom/squareup/cardreader/LcrModule;,
        Lcom/squareup/cardreader/LcrModule$Real;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderSwig(Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderFeatureLegacy;Lcom/squareup/cardreader/CoreDumpFeatureLegacy;Lcom/squareup/cardreader/EventLogFeatureLegacy;Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;Lcom/squareup/cardreader/PaymentFeatureLegacy;Lcom/squareup/cardreader/PowerFeatureLegacy;Lcom/squareup/cardreader/SecureSessionFeatureLegacy;Lcom/squareup/cardreader/SecureTouchFeatureInterface;Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/TamperFeatureLegacy;Lcom/squareup/cardreader/UserInteractionFeatureLegacy;Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;Lcom/squareup/cardreader/SecureSessionRevocationFeature;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/CardReaderSwig;
    .locals 25
    .annotation runtime Ldagger/Provides;
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v5, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 53
    new-instance v24, Lcom/squareup/cardreader/CardReaderSwig;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/cardreader/CardReaderSwig;-><init>(Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;Lcom/squareup/cardreader/CardReaderFeatureLegacy;Lcom/squareup/cardreader/CoreDumpFeatureLegacy;Lcom/squareup/cardreader/PaymentFeatureLegacy;Lcom/squareup/cardreader/EventLogFeatureLegacy;Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;Lcom/squareup/cardreader/PowerFeatureLegacy;Lcom/squareup/cardreader/SecureSessionFeatureLegacy;Lcom/squareup/cardreader/SecureTouchFeatureInterface;Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/TamperFeatureLegacy;Lcom/squareup/cardreader/UserInteractionFeatureLegacy;Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;Lcom/squareup/cardreader/SecureSessionRevocationFeature;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderListeners;)V

    return-object v24
.end method


# virtual methods
.method abstract provideCardReaderDispatch(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
