.class public final Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideSecureTouchCardReaderFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final nativeBinariesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/NativeBinaries;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final squidInterfaceSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final touchReportingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/TouchReporting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/TouchReporting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/NativeBinaries;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->cardReaderPointerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->secureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->touchReportingProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->nativeBinariesProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->squidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/TouchReporting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/NativeBinaries;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;"
        }
    .end annotation

    .line 60
    new-instance v7, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideSecureTouchCardReaderFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;Lcom/squareup/securetouch/TouchReporting;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/NativeBinaries;Lio/reactivex/Scheduler;)Lcom/squareup/cardreader/SecureTouchFeatureInterface;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            "Lcom/squareup/securetouch/TouchReporting;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/cardreader/NativeBinaries;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;"
        }
    .end annotation

    .line 68
    invoke-static/range {p0 .. p5}, Lcom/squareup/cardreader/LcrModule;->provideSecureTouchCardReaderFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;Lcom/squareup/securetouch/TouchReporting;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/NativeBinaries;Lio/reactivex/Scheduler;)Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/SecureTouchFeatureInterface;
    .locals 6

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->cardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->secureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v2, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->touchReportingProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/securetouch/TouchReporting;

    iget-object v3, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    iget-object v4, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->nativeBinariesProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/NativeBinaries;

    iget-object v5, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->squidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lio/reactivex/Scheduler;

    invoke-static/range {v0 .. v5}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->provideSecureTouchCardReaderFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;Lcom/squareup/securetouch/TouchReporting;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/NativeBinaries;Lio/reactivex/Scheduler;)Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->get()Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    move-result-object v0

    return-object v0
.end method
