.class public final Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;
.super Ljava/lang/Object;
.source "DaggerX2CardReaderContextComponent.java"

# interfaces
.implements Lcom/squareup/cardreader/squid/x2/X2CardReaderContextComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_magSwipeFailureFilter;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideSquidInterfaceScheduler;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_touchReporting;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_application;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideTmnTimings;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_crashnado;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_nativeLoggingEnabled;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_lcrExecutor;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_cardReaderFactory;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_cardReaderListeners;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_mainThread;,
        Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$Builder;
    }
.end annotation


# instance fields
.field private applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

.field private isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private magSwipeFailureFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            ">;"
        }
    .end annotation
.end field

.field private mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private nativeLoggingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderAddressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderConstantsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderGraphInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderLogBridge;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderSwigProvider:Ljavax/inject/Provider;

.field private provideCardreaderNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectionTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
            ">;"
        }
    .end annotation
.end field

.field private provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

.field private provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventLogFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideFakeBluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private provideLcrBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocalCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LocalCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private provideLogNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/LogNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideNativeBinariesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/NativeBinaries;",
            ">;"
        }
    .end annotation
.end field

.field private provideNfcListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentCompletionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureProvider:Ljavax/inject/Provider;

.field private providePaymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private providePowerFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private providePowerFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideResProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

.field private provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTamperFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideTamperFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimerApiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TimerApiLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimerNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TimerNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideTmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserInteractionFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideX2BackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/squid/common/SpeBackend;",
            ">;"
        }
    .end annotation
.end field

.field private realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private touchReportingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/TouchReporting;",
            ">;"
        }
    .end annotation
.end field

.field private x2SystemFeatureLegacyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/X2SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/CardReaderContextParent;)V
    .locals 0

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 277
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->initialize(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/CardReaderContextParent;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/CardReaderContextParent;Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$1;)V
    .locals 0

    .line 130
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;-><init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/CardReaderContextParent;)V

    return-void
.end method

.method public static builder()Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$Builder;
    .locals 2

    .line 281
    new-instance v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$Builder;-><init>(Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$1;)V

    return-object v0
.end method

.method private initialize(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/CardReaderContextParent;)V
    .locals 25

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 287
    invoke-static/range {p1 .. p1}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderIdFactory;->create(Lcom/squareup/cardreader/CardReaderModule;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderIdFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    .line 288
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideConnectionTypeFactory;->create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideConnectionTypeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideConnectionTypeProvider:Ljavax/inject/Provider;

    .line 289
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;->create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderAddressProvider:Ljavax/inject/Provider;

    .line 290
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderNameFactory;->create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderNameFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderNameProvider:Ljavax/inject/Provider;

    .line 291
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideConnectionTypeProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderAddressProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderNameProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    .line 292
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_mainThread;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_mainThread;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    .line 293
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_cardReaderListeners;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_cardReaderListeners;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 294
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_cardReaderFactory;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_cardReaderFactory;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 295
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_lcrExecutor;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_lcrExecutor;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 296
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_nativeLoggingEnabled;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_nativeLoggingEnabled;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->nativeLoggingEnabledProvider:Ljavax/inject/Provider;

    .line 297
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_crashnado;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_crashnado;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    .line 298
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCardreaderNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideCardreaderNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    .line 299
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideX2BackendProvider:Ljavax/inject/Provider;

    .line 300
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideX2BackendProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideLcrBackendFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideLcrBackendFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideLcrBackendProvider:Ljavax/inject/Provider;

    .line 301
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideTimerNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideTimerNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTimerNativeProvider:Ljavax/inject/Provider;

    .line 302
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTimerNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTimerApiProvider:Ljavax/inject/Provider;

    .line 303
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideLogNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideLogNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideLogNativeProvider:Ljavax/inject/Provider;

    .line 304
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideLogNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_Real_ProvideCardReaderLoggerInterfaceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideCardReaderLoggerInterfaceFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;

    .line 305
    invoke-static {}, Lcom/squareup/cardreader/NativeCardReaderConstants_Factory;->create()Lcom/squareup/cardreader/NativeCardReaderConstants_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    .line 306
    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->nativeLoggingEnabledProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideLcrBackendProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTimerApiProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v10}, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 307
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    .line 308
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;

    .line 309
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideCoreDumpFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCoreDumpFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

    .line 310
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideEventlogFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideEventlogFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;

    .line 311
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideEventLogFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideEventLogFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideEventLogFeatureProvider:Ljavax/inject/Provider;

    .line 312
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideFirmwareUpdateFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideFirmwareUpdateFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    .line 313
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 314
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentFeatureNativeProvider:Ljavax/inject/Provider;

    .line 315
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideTmnTimings;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideTmnTimings;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    .line 316
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 317
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;

    .line 318
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;

    .line 319
    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentFeatureNativeProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v9}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentFeatureProvider:Ljavax/inject/Provider;

    .line 320
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePowerFeatureNativeProvider:Ljavax/inject/Provider;

    .line 321
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePowerFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePowerFeatureProvider:Ljavax/inject/Provider;

    .line 322
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_application;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_application;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    .line 323
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    .line 324
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    .line 325
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 326
    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperTicketFactory;->create()Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperTicketFactory;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v9}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

    .line 327
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchFeatureNativeInterfaceFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchFeatureNativeInterfaceFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    .line 328
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_touchReporting;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_touchReporting;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->touchReportingProvider:Ljavax/inject/Provider;

    .line 329
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideNativeBinariesProvider:Ljavax/inject/Provider;

    .line 330
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideSquidInterfaceScheduler;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideSquidInterfaceScheduler;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    .line 331
    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->touchReportingProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideNativeBinariesProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v8}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    .line 332
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    .line 333
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    .line 334
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTamperFeatureNativeProvider:Ljavax/inject/Provider;

    .line 335
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTamperFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTamperFeatureProvider:Ljavax/inject/Provider;

    .line 336
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;

    .line 337
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideUserInteractionFeatureProvider:Ljavax/inject/Provider;

    .line 338
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    .line 339
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderListenerProvider:Ljavax/inject/Provider;

    .line 340
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    .line 341
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderProvider:Ljavax/inject/Provider;

    .line 342
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    .line 343
    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdateListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdateListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;

    .line 344
    new-instance v2, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_magSwipeFailureFilter;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_magSwipeFailureFilter;-><init>(Lcom/squareup/cardreader/CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    .line 345
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    .line 346
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentListenerProvider:Ljavax/inject/Provider;

    .line 347
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideNfcListenerProvider:Ljavax/inject/Provider;

    .line 348
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentCompletionListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentCompletionListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentCompletionListenerProvider:Ljavax/inject/Provider;

    .line 349
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/LcrModule_ProvideResFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideResFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideResProvider:Ljavax/inject/Provider;

    .line 350
    invoke-static {}, Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;->create()Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    .line 351
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideFakeBluetoothUtilsFactory;->create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideFakeBluetoothUtilsFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFakeBluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 352
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideEventLogFeatureProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentFeatureProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePowerFeatureProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideTamperFeatureProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideUserInteractionFeatureProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderListenerProvider:Ljavax/inject/Provider;

    move-object/from16 p1, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideNfcListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentCompletionListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFakeBluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object/from16 v23, v1

    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v24, v1

    invoke-static/range {v2 .. v24}, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 353
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v8}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 354
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideLocalCardReaderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideLocalCardReaderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    .line 355
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/X2SystemFeatureLegacy_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->x2SystemFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 356
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->x2SystemFeatureLegacyProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 357
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;->create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    .line 358
    iget-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public cardReaderContext()Lcom/squareup/cardreader/CardReaderContext;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderContext;

    return-object v0
.end method
