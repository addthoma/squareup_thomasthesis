.class public final Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideSecureSessionFeatureNativeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSecureSessionFeatureNative()Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/cardreader/LcrModule;->provideSecureSessionFeatureNative()Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;->provideSecureSessionFeatureNative()Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;->get()Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    move-result-object v0

    return-object v0
.end method
