.class public Lcom/squareup/cardreader/SCRPFirmwareVersionHelper;
.super Ljava/lang/Object;
.source "SCRPFirmwareVersionHelper.java"


# static fields
.field private static final FW_MAJOR_VERSION_GROUP:I = 0x2

.field private static final FW_MINOR_VERSION_GROUP:I = 0x3

.field private static final MIN_SCRP_FW_MAJOR_VERSION:I = 0x2

.field private static final MIN_SCRP_FW_MINOR_VERSION:I = 0xc

.field private static final PTS_MINOR_FW_VERSION_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PTS\\.([0-9])+\\.([0-9]+)\\.([0-9]+)(\\.[0-9]+)?"

    .line 25
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/SCRPFirmwareVersionHelper;->PTS_MINOR_FW_VERSION_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static convertFwVersionToSCRPIfRequired(Ljava/lang/String;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_4

    if-nez p1, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    .line 37
    :cond_1
    sget-object p1, Lcom/squareup/cardreader/SCRPFirmwareVersionHelper;->PTS_MINOR_FW_VERSION_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2

    return-object p0

    :cond_2
    const/4 v0, 0x2

    .line 43
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    .line 44
    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    .line 46
    invoke-static {v0, p1}, Lcom/squareup/cardreader/SCRPFirmwareVersionHelper;->supportsSCRP(II)Z

    move-result p1

    if-nez p1, :cond_3

    return-object p0

    :cond_3
    const-string p1, "PTS"

    const-string v0, "SCRP"

    .line 50
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    :goto_0
    return-object p0
.end method

.method private static supportsSCRP(II)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-le p0, v1, :cond_0

    return v0

    :cond_0
    if-ne p0, v1, :cond_1

    const/16 p0, 0xc

    if-lt p1, p0, :cond_1

    return v0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method
