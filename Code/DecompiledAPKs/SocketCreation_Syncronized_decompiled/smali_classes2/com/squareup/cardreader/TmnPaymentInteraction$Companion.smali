.class public final Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;
.super Ljava/lang/Object;
.source "PaymentInteraction.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/TmnPaymentInteraction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\u000c\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;",
        "",
        "()V",
        "startCheckBalance",
        "Lcom/squareup/cardreader/TmnPaymentInteraction;",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "translationId",
        "",
        "amountAuthorized",
        "",
        "startOnlineTest",
        "startPayment",
        "startRefund",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;
    .locals 7

    const-string v0, "brandId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "translationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/squareup/cardreader/TmnPaymentInteraction;

    .line 76
    sget-object v2, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-wide v5, p3

    .line 75
    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/TmnPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-object v0
.end method

.method public final startOnlineTest(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;
    .locals 7

    const-string v0, "brandId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "translationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/squareup/cardreader/TmnPaymentInteraction;

    .line 86
    sget-object v2, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-wide v5, p3

    .line 85
    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/TmnPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-object v0
.end method

.method public final startPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;
    .locals 7

    const-string v0, "brandId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "translationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lcom/squareup/cardreader/TmnPaymentInteraction;

    .line 56
    sget-object v2, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-wide v5, p3

    .line 55
    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/TmnPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-object v0
.end method

.method public final startRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;
    .locals 7

    const-string v0, "brandId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "translationId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/squareup/cardreader/TmnPaymentInteraction;

    .line 66
    sget-object v2, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-wide v5, p3

    .line 65
    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/TmnPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-object v0
.end method
