.class public final Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_ProvideCardReaderInitializerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderDispatchProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->firmwareUpdaterProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p7, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;"
        }
    .end annotation

    .line 60
    new-instance v8, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideCardReaderInitializer(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderInitializer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderInitializer;"
        }
    .end annotation

    .line 67
    invoke-static/range {p0 .. p6}, Lcom/squareup/cardreader/CardReaderModule;->provideCardReaderInitializer(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderInitializer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderInitializer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderInitializer;
    .locals 8

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->firmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/FirmwareUpdater;

    iget-object v7, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->cardReaderProvider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v7}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->provideCardReaderInitializer(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->get()Lcom/squareup/cardreader/CardReaderInitializer;

    move-result-object v0

    return-object v0
.end method
