.class public final Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;
.super Ljava/lang/Object;
.source "FirmwareUpdateDispatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateNotificationServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateService;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final uuidGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->firmwareUpdateNotificationServiceStarterProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->firmwareUpdateServiceProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->firmwareUpdateLoggerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;"
        }
    .end annotation

    .line 69
    new-instance v9, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
    .locals 10

    .line 77
    new-instance v9, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;-><init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/cardreader/CardReaderListeners;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
    .locals 9

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->firmwareUpdateNotificationServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->firmwareUpdateServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/dipper/FirmwareUpdateService;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->firmwareUpdateLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/log/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/log/UUIDGenerator;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static/range {v1 .. v8}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->newInstance(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->get()Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    move-result-object v0

    return-object v0
.end method
