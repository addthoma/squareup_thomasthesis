.class public final synthetic Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

.field private final synthetic f$1:Lcom/squareup/cardreader/CardReader;

.field private final synthetic f$2:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;->f$0:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    iput-object p2, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;->f$1:Lcom/squareup/cardreader/CardReader;

    iput-object p3, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;->f$2:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;->f$0:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;->f$1:Lcom/squareup/cardreader/CardReader;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;->f$2:Lcom/squareup/cardreader/CardReaderInfo;

    check-cast p1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lambda$null$0$ReaderUiEventSink(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;)V

    return-void
.end method
