.class public final Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;
.super Ljava/lang/Object;
.source "ReaderIssueNavigator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;",
            "Lcom/squareup/ui/main/PosContainer;",
            ")",
            "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;-><init>(Ldagger/Lazy;Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator_Factory;->get()Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    move-result-object v0

    return-object v0
.end method
