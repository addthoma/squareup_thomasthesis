.class public final Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;
.super Ljava/lang/Object;
.source "ReaderConnectionEventLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;-><init>(Lcom/squareup/log/OhSnapLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;->newInstance(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger_Factory;->get()Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    move-result-object v0

    return-object v0
.end method
