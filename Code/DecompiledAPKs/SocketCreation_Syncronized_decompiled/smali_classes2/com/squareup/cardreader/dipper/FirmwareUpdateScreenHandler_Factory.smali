.class public final Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;
.super Ljava/lang/Object;
.source "FirmwareUpdateScreenHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final goBackAfterWarningProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final r12BlockingUpdateScreenLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r12ContentLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->goBackAfterWarningProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->r12BlockingUpdateScreenLauncherProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->r12ContentLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/ui/main/R12ForceableContentLauncher;)Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ")",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;"
        }
    .end annotation

    .line 61
    new-instance v6, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;-><init>(Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/ui/main/R12ForceableContentLauncher;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->goBackAfterWarningProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/GoBackAfterWarning;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->r12BlockingUpdateScreenLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/main/ContentLauncher;

    iget-object v4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->r12ContentLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/main/R12ForceableContentLauncher;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->newInstance(Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/ui/main/R12ForceableContentLauncher;)Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler_Factory;->get()Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;

    move-result-object v0

    return-object v0
.end method
