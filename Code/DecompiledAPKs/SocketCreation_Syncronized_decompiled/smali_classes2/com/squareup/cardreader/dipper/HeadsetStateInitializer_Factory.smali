.class public final Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;
.super Ljava/lang/Object;
.source "HeadsetStateInitializer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetStateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/HeadsetStateDispatcher;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;-><init>(Lcom/squareup/cardreader/HeadsetStateDispatcher;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/HeadsetStateDispatcher;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->newInstance(Lcom/squareup/cardreader/HeadsetStateDispatcher;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->get()Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;

    move-result-object v0

    return-object v0
.end method
