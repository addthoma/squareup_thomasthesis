.class public final Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;
.super Ljava/lang/Object;
.source "DefaultEmvCardInsertRemoveProcessor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private final badKeyboardHiderIsBadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->badKeyboardHiderIsBadProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;)Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->badKeyboardHiderIsBadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/main/BadKeyboardHider;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/PaymentHudToaster;

    invoke-static/range {v1 .. v6}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;)Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor_Factory;->get()Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    move-result-object v0

    return-object v0
.end method
