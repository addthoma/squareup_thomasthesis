.class public Lcom/squareup/cardreader/lcr/BleBackendNativePassthrough;
.super Ljava/lang/Object;
.source "BleBackendNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ble_received_data_from_characteristic_ack_vector(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 0

    .line 38
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/BleBackendNative;->ble_received_data_from_characteristic_ack_vector(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    move-result-object p1

    return-object p1
.end method

.method public ble_received_data_from_characteristic_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[B)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 0

    .line 32
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/BleBackendNative;->ble_received_data_from_characteristic_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[B)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    move-result-object p1

    return-object p1
.end method

.method public ble_received_data_from_characteristic_mtu(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 0

    .line 44
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/BleBackendNative;->ble_received_data_from_characteristic_mtu(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    move-result-object p1

    return-object p1
.end method

.method public cr_comms_backend_ble_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;
    .locals 1

    .line 9
    invoke-static {}, Lcom/squareup/cardreader/lcr/BleBackendNative;->cr_comms_backend_ble_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    move-result-object v0

    return-object v0
.end method

.method public cr_comms_backend_ble_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 0

    .line 15
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/BleBackendNative;->cr_comms_backend_ble_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    move-result-object p1

    return-object p1
.end method

.method public cr_comms_backend_ble_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V
    .locals 0

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/BleBackendNative;->cr_comms_backend_ble_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V

    return-void
.end method

.method public initialize_backend_ble(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[BLjava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 0

    .line 26
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/BleBackendNative;->initialize_backend_ble(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[BLjava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object p1

    return-object p1
.end method
