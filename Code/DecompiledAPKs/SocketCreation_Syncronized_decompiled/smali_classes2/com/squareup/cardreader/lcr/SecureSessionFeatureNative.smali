.class public Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;
.super Ljava/lang/Object;
.source "SecureSessionFeatureNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_securesession_feature_establish_session(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->cr_securesession_feature_establish_session(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_securesession_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->cr_securesession_feature_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_securesession_feature_notify_server_error(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->cr_securesession_feature_notify_server_error(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_securesession_feature_pin_bypass(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->cr_securesession_feature_pin_bypass(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_securesession_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->cr_securesession_feature_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static pin_add_digit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;I)Z
    .locals 2

    .line 46
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->pin_add_digit(JI)Z

    move-result p0

    return p0
.end method

.method public static pin_reset(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V
    .locals 2

    .line 42
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->pin_reset(J)V

    return-void
.end method

.method public static pin_submit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 50
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->pin_submit(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static securesession_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;
    .locals 2

    .line 33
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->securesession_initialize(JLjava/lang/Object;Ljava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long p2, p0, v0

    if-nez p2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 34
    :cond_0
    new-instance p2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    const/4 v0, 0x0

    invoke-direct {p2, p0, p1, v0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;-><init>(JZ)V

    move-object p0, p2

    :goto_0
    return-object p0
.end method

.method public static securesession_recv_server_message(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;
    .locals 3

    .line 38
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v1

    invoke-static {v1, v2, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->securesession_recv_server_message(J[B)J

    move-result-wide p0

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;-><init>(JZ)V

    return-object v0
.end method

.method public static set_kb(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Z
    .locals 2

    .line 54
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->set_kb(J[B)Z

    move-result p0

    return p0
.end method
