.class public Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeJNI;
.super Ljava/lang/Object;
.source "UserInteractionFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native CR_USER_INTERACTION_RESULT_ALREADY_INITIALIZED_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_ALREADY_TERMINATED_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_CALL_UNEXPECTED_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_FATAL_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_INVALID_PARAMETER_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_NOT_INITIALIZED_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_NOT_TERMINATED_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_SESSION_ERROR_get()I
.end method

.method public static final native CR_USER_INTERACTION_RESULT_SUCCESS_get()I
.end method

.method public static final native cr_user_interaction_free(J)I
.end method

.method public static final native cr_user_interaction_identify_reader(J)I
.end method

.method public static final native cr_user_interaction_term(J)I
.end method

.method public static final native user_interaction_initialize(J)J
.end method
