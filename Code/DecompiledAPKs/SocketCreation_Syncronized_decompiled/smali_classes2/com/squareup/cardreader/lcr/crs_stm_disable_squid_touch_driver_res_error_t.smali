.class public final enum Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;
.super Ljava/lang/Enum;
.source "crs_stm_disable_squid_touch_driver_res_error_t.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

.field public static final enum CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_FAILED:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

.field public static final enum CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_GENERIC:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

.field public static final enum CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_NONE:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    const/4 v1, 0x0

    const-string v2, "CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_NONE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_NONE:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    const/4 v2, 0x1

    const-string v3, "CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_GENERIC"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_GENERIC:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    const/4 v3, 0x2

    const-string v4, "CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_FAILED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_FAILED:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    .line 11
    sget-object v4, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_NONE:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_GENERIC:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->CRS_STM_DISABLE_SQUID_TOUCH_DRIVER_ERROR_FAILED:Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->$VALUES:[Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    invoke-static {}, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 38
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iget p1, p3, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    .line 44
    iget p1, p0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;
    .locals 6

    .line 21
    const-class v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    .line 22
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 23
    aget-object p0, v1, p0

    return-object p0

    .line 24
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 25
    iget v5, v4, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 27
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->$VALUES:[Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/squareup/cardreader/lcr/crs_stm_disable_squid_touch_driver_res_error_t;->swigValue:I

    return v0
.end method
