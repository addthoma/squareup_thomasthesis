.class public Lcom/squareup/cardreader/lcr/TimerNative;
.super Ljava/lang/Object;
.source "TimerNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initialize_timer_api(Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;
    .locals 3

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/TimerNativeJNI;->initialize_timer_api(Ljava/lang/Object;)J

    move-result-wide v1

    const/4 p0, 0x1

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;-><init>(JZ)V

    return-object v0
.end method

.method public static on_timer_expired(JJJ)V
    .locals 0

    .line 17
    invoke-static/range {p0 .. p5}, Lcom/squareup/cardreader/lcr/TimerNativeJNI;->on_timer_expired(JJJ)V

    return-void
.end method
