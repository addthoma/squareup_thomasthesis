.class public Lcom/squareup/cardreader/lcr/SystemFeatureNative;
.super Ljava/lang/Object;
.source "SystemFeatureNative.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/SystemFeatureNativeConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_system_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->cr_system_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 33
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->cr_system_mark_feature_flags_ready_to_send(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_system_read_system_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->cr_system_read_system_info(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_system_send_external_charging_state(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Z)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->cr_system_send_external_charging_state(JZ)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_system_set_hardware_platform_feature(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsHardwarePlatformFeature;->swigValue()I

    move-result p0

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->cr_system_set_hardware_platform_feature(JI)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_system_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->cr_system_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method

.method public static system_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;
    .locals 3

    .line 37
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->system_initialize(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 38
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static system_set_reader_feature_flag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Ljava/lang/String;S)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 2

    .line 42
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;->system_set_reader_feature_flag(JLjava/lang/String;S)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSystemResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p0

    return-object p0
.end method
