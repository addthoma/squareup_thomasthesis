.class public interface abstract Lcom/squareup/cardreader/lcr/PaymentFeatureNativeConstants;
.super Ljava/lang/Object;
.source "PaymentFeatureNativeConstants.java"


# static fields
.field public static final CRS_TMN_CARD_NUMBER_LEN:I = 0x14

.field public static final CRS_TMN_DISPLAY_REQUEST_STRING_LEN:I = 0x10

.field public static final CRS_TMN_ENCRYPTED_DATA_TO_TMN_LEN:I = 0x432

.field public static final CRS_TMN_ENCRYPTED_PACKET_MAX_LEN:I = 0x536

.field public static final CRS_TMN_PACKET_MAX_LEN:I = 0x500

.field public static final CRS_TMN_PACKET_MAX_NUM_PARTS:I = 0x3

.field public static final CRS_TMN_PACKET_PART_MAX_LEN:I = 0x1d8

.field public static final CRS_TMN_TIMESTAMP_LENGTH:I = 0x7

.field public static final CRS_TMN_TRANSACTION_ID_LENGTH:I = 0x20

.field public static final CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_CVM_FALLTHROUGH:I = 0x40

.field public static final CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_RFU_BIT8:I = 0x80

.field public static final CR_EMV_FLOW_LANG_PREF_MAX:I = 0x4

.field public static final CR_PAYMENT_ACCOUNT_TYPE_MAX:I = 0x6

.field public static final CR_PAYMENT_APP_ADF_NAME_MAX:I = 0x10

.field public static final CR_PAYMENT_APP_LABEL_MAX:I = 0x10

.field public static final CR_PAYMENT_APP_PREFNAME_MAX:I = 0x10

.field public static final CR_PAYMENT_LAST4_LENGTH:I = 0x4

.field public static final CR_PAYMENT_MAGSWIPE_DUPLICATE_TIMEOUT_MS:I = 0xfa0

.field public static final CR_PAYMENT_MAGSWIPE_MAX_TIME_BETWEEN_M1_MESSAGES_MS:I = 0x96

.field public static final CR_PAYMENT_MAX_ICC_FAILURES:I = 0x3

.field public static final CR_PAYMENT_MAX_TIMINGS:I = 0x10

.field public static final CR_PAYMENT_NAME_MAX_LENGTH:I = 0x1a

.field public static final CR_PAYMENT_PAN_IIN_PREFIX_LENGTH:I = 0x6

.field public static final CR_PAYMENT_PIN_ENTRY_TIMEOUT_MS:I = 0xea60

.field public static final CR_PAYMENT_TIMING_LABEL_SIZE:I = 0x10
