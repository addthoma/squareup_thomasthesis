.class public interface abstract Lcom/squareup/cardreader/NonX2CardReaderContextParent;
.super Ljava/lang/Object;
.source "NonX2CardReaderContextParent.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderContextParent;


# virtual methods
.method public abstract accessibilityManager()Landroid/view/accessibility/AccessibilityManager;
.end method

.method public abstract audioManager()Landroid/media/AudioManager;
.end method

.method public abstract audioRingBuffer()Lcom/squareup/squarewave/wav/AudioRingBuffer;
.end method

.method public abstract badBus()Lcom/squareup/badbus/BadBus;
.end method

.method public abstract badEventSink()Lcom/squareup/badbus/BadEventSink;
.end method

.method public abstract bleDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
.end method

.method public abstract bleExecutor()Landroid/os/Handler;
.end method

.method public abstract bleThread()Ljava/lang/Thread;
.end method

.method public abstract bleThreadEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;
.end method

.method public abstract bluetoothAdapter()Landroid/bluetooth/BluetoothAdapter;
.end method

.method public abstract bluetoothStatusReceiver()Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;
.end method

.method public abstract bluetoothUtils()Lcom/squareup/cardreader/BluetoothUtils;
.end method

.method public abstract cardReaderHub()Lcom/squareup/cardreader/CardReaderHub;
.end method

.method public abstract cardReaderPauseAndResumer()Lcom/squareup/cardreader/CardReaderPauseAndResumer;
.end method

.method public abstract headset()Lcom/squareup/wavpool/swipe/Headset;
.end method

.method public abstract libraryLoader()Lcom/squareup/cardreader/loader/LibraryLoader;
.end method

.method public abstract mainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end method

.method public abstract minesweeperTicket()Lcom/squareup/ms/MinesweeperTicket;
.end method

.method public abstract provideBleBondingBroadcastReceiver()Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;
.end method

.method public abstract provideBleScanner()Lcom/squareup/cardreader/ble/BleScanner;
.end method

.method public abstract provideBluetoothDiscoveryBroadcastReceiver()Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;
.end method

.method public abstract provideHeadsetConnectionState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
.end method

.method public abstract provideHeadsetStateDispatcher()Lcom/squareup/cardreader/HeadsetStateDispatcher;
.end method

.method public abstract provideMinesweeper()Lcom/squareup/ms/Minesweeper;
.end method

.method public abstract provideMsFactory()Lcom/squareup/ms/MsFactory;
.end method

.method public abstract running()Z
    .annotation runtime Lcom/squareup/cardreader/CardReaderPauseAndResumer$Running;
    .end annotation
.end method

.method public abstract swipeBus()Lcom/squareup/wavpool/swipe/SwipeBus;
.end method

.method public abstract swipeEventLogger()Lcom/squareup/logging/SwipeEventLogger;
.end method

.method public abstract telephoneManager()Landroid/telephony/TelephonyManager;
.end method
