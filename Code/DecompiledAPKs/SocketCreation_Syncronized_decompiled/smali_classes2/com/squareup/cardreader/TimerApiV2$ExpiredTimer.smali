.class public final Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;
.super Ljava/util/TimerTask;
.source "TimerApiV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/TimerApiV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ExpiredTimer"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;",
        "Ljava/util/TimerTask;",
        "timerId",
        "",
        "methodPtr",
        "contextPtr",
        "(Lcom/squareup/cardreader/TimerApiV2;JJJ)V",
        "getContextPtr",
        "()J",
        "getMethodPtr",
        "getTimerId",
        "run",
        "",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contextPtr:J

.field private final methodPtr:J

.field final synthetic this$0:Lcom/squareup/cardreader/TimerApiV2;

.field private final timerId:J


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/TimerApiV2;JJJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ)V"
        }
    .end annotation

    .line 58
    iput-object p1, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->this$0:Lcom/squareup/cardreader/TimerApiV2;

    .line 62
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    iput-wide p2, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->timerId:J

    iput-wide p4, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->methodPtr:J

    iput-wide p6, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->contextPtr:J

    return-void
.end method


# virtual methods
.method public final getContextPtr()J
    .locals 2

    .line 61
    iget-wide v0, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->contextPtr:J

    return-wide v0
.end method

.method public final getMethodPtr()J
    .locals 2

    .line 60
    iget-wide v0, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->methodPtr:J

    return-wide v0
.end method

.method public final getTimerId()J
    .locals 2

    .line 59
    iget-wide v0, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->timerId:J

    return-wide v0
.end method

.method public run()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->this$0:Lcom/squareup/cardreader/TimerApiV2;

    invoke-static {v0, p0}, Lcom/squareup/cardreader/TimerApiV2;->access$execute(Lcom/squareup/cardreader/TimerApiV2;Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;)V

    return-void
.end method
