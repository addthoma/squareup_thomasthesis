.class public Lcom/squareup/cardreader/FirmwareUpdater;
.super Ljava/lang/Object;
.source "FirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/FirmwareUpdater$Listener;,
        Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;,
        Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;,
        Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;
    }
.end annotation


# static fields
.field private static final SQUID_FWUP_REBOOT_WAIT_TIME_MSEC:I = 0x1388

.field private static final SQUID_FWUP_RETRIES:I = 0x3


# instance fields
.field private assetDataBeingSent:Z

.field private assetIndex:I

.field private assets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReader:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderDispatch:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private descriptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private final internalListener:Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private updateType:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 47
    iput-object p2, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderDispatch:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReader:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 50
    iput-object p5, p0, Lcom/squareup/cardreader/FirmwareUpdater;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 51
    new-instance p1, Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;-><init>(Lcom/squareup/cardreader/FirmwareUpdater;)V

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->internalListener:Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;

    const/4 p1, 0x0

    .line 52
    iput-boolean p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    .line 53
    sget-object p1, Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;->NONE:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->updateType:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/FirmwareUpdater;)Ljavax/inject/Provider;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReader:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/FirmwareUpdater$Listener;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->externalListener()Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/FirmwareUpdater;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    return p0
.end method

.method static synthetic access$308(Lcom/squareup/cardreader/FirmwareUpdater;)I
    .locals 2

    .line 21
    iget v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/FirmwareUpdater;)Ljava/util/List;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$502(Lcom/squareup/cardreader/FirmwareUpdater;Z)Z
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    return p1
.end method

.method static synthetic access$602(Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;)Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->updateType:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    return-object p1
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/FirmwareUpdater;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->continueUpdates()V

    return-void
.end method

.method public static buildAssetDescriptors(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;"
        }
    .end annotation

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 142
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/tarkin/Asset;

    .line 143
    new-instance v2, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    iget-object v3, v1, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v4, v1, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    iget-object v1, v1, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    .line 144
    invoke-virtual {v1}, Lokio/ByteString;->size()I

    move-result v1

    int-to-long v5, v1

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;-><init>(ZLcom/squareup/protos/client/tarkin/Asset$Reboot;J)V

    .line 143
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private continueUpdates()V
    .locals 3

    .line 117
    iget-boolean v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "#continueUpdates called while asset update is already in progress."

    .line 118
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->updatesPending()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setFirmwareUpdateInProgress(Z)V

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    invoke-static {v2}, Lcom/squareup/cardreader/FirmwareUpdater;->hasBlockingUpdates(Ljava/util/List;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setServerRequiresFirmwareUpdate(Z)V

    .line 128
    iput-boolean v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    .line 130
    sget-object v0, Lcom/squareup/cardreader/FirmwareUpdater$1;->$SwitchMap$com$squareup$cardreader$FirmwareUpdater$UpdateType:[I

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareUpdater;->updateType:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    invoke-virtual {v2}, Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    if-ne v0, v1, :cond_2

    .line 132
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assets:Ljava/util/List;

    iget v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tarkin/Asset;

    .line 133
    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderDispatch;->updateFirmware(Lcom/squareup/protos/client/tarkin/Asset;)V

    return-void

    .line 136
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to apply asset when update type is `None`."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private externalListener()Lcom/squareup/cardreader/FirmwareUpdater$Listener;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getFirmwareUpdateListener()Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    move-result-object v0

    return-object v0
.end method

.method static hasBlockingUpdates(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 151
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 152
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    .line 153
    iget-boolean v2, v2, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->blocking:Z

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private startApplyingUpdates()V
    .locals 3

    const/4 v0, 0x0

    .line 83
    iput v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    .line 84
    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    invoke-static {v2}, Lcom/squareup/cardreader/FirmwareUpdater;->hasBlockingUpdates(Ljava/util/List;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setServerRequiresFirmwareUpdate(Z)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 86
    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    aput-object v2, v1, v0

    const-string v0, "Firmware updates to perform: %s"

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->externalListener()Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/squareup/cardreader/FirmwareUpdater$Listener;->onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Ljava/util/List;)V

    .line 89
    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->continueUpdates()V

    return-void
.end method


# virtual methods
.method getCurrentAssetDescriptor()Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    if-ltz v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    iget v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method getInternalListener()Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->internalListener:Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;

    return-object v0
.end method

.method getUpdateType()Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->updateType:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    return-object v0
.end method

.method public pauseUpdates()V
    .locals 1

    const/4 v0, 0x0

    .line 94
    iput-boolean v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    .line 95
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->pauseFirmwareUpdate()V

    return-void
.end method

.method public processFirmwareResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderRequiresFirmwareUpdate(Z)V

    .line 64
    iget-boolean v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    if-eqz v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "#startUpdates called while asset update is already in progress."

    .line 65
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    if-eqz p1, :cond_2

    .line 69
    iget-object v0, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 74
    :cond_1
    sget-object v0, Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;->LCR:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    iput-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->updateType:Lcom/squareup/cardreader/FirmwareUpdater$UpdateType;

    .line 76
    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assets:Ljava/util/List;

    .line 77
    iget-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assets:Ljava/util/List;

    invoke-static {p1}, Lcom/squareup/cardreader/FirmwareUpdater;->buildAssetDescriptors(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    .line 79
    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->startApplyingUpdates()V

    return-void

    :cond_2
    :goto_0
    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "No firmware updates to perform"

    .line 70
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetDataBeingSent:Z

    return-void
.end method

.method public resumeUpdates()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->continueUpdates()V

    return-void
.end method

.method updatesPending()I
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdater;->descriptors:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/cardreader/FirmwareUpdater;->assetIndex:I

    sub-int/2addr v0, v1

    :goto_0
    return v0
.end method
