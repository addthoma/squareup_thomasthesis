.class public interface abstract Lcom/squareup/cardreader/LcrBackend;
.super Ljava/lang/Object;
.source "LcrBackend.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/LcrBackend$BackendSession;
    }
.end annotation


# virtual methods
.method public abstract cardReaderInitialized()V
.end method

.method public abstract initialize(Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;
.end method

.method public abstract onResume()V
.end method

.method public abstract powerOnReader()V
.end method

.method public abstract resetBackend()V
.end method
