.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "OrderSquareCardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "businessNameCoordinatorFactory",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;",
        "signatureCoordinatorFactory",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;",
        "stampsDialogFactory",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;",
        "(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)V
    .locals 20
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "businessNameCoordinatorFactory"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "signatureCoordinatorFactory"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "stampsDialogFactory"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 22
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    sget-object v5, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash;

    invoke-virtual {v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 24
    sget v6, Lcom/squareup/balance/squarecard/impl/R$layout;->order_square_card_splash_view:I

    .line 25
    new-instance v19, Lcom/squareup/workflow/ScreenHint;

    const-class v11, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1f7

    const/16 v18, 0x0

    move-object/from16 v7, v19

    invoke-direct/range {v7 .. v18}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 26
    sget-object v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$1;

    move-object v9, v7

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object/from16 v7, v19

    .line 22
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 28
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 29
    sget-object v4, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;

    invoke-virtual {v4}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;->getSQUARE_CARD_PROGRESS_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 30
    sget v8, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_progress_view:I

    .line 31
    sget-object v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$2;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$2;

    move-object v11, v4

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v12, 0xc

    .line 28
    invoke-static/range {v6 .. v13}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 33
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 34
    sget-object v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName;

    invoke-virtual {v4}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 35
    sget v8, Lcom/squareup/balance/squarecard/impl/R$layout;->order_square_card_business_name_view:I

    .line 36
    new-instance v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$3;

    invoke-direct {v4, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$3;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;)V

    move-object v11, v4

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 33
    invoke-static/range {v6 .. v13}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v4, 0x2

    aput-object v0, v3, v4

    .line 38
    sget-object v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 39
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 40
    sget v7, Lcom/squareup/balance/squarecard/impl/R$layout;->order_square_card_signature_view:I

    .line 41
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    .line 42
    sget-object v9, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    sget-object v10, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1fc

    const/16 v19, 0x0

    move-object v8, v0

    .line 41
    invoke-direct/range {v8 .. v19}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 44
    new-instance v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$4;

    invoke-direct {v4, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$4;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;)V

    move-object v10, v4

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x0

    const/16 v11, 0x8

    .line 38
    invoke-static/range {v5 .. v12}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v3, v1

    .line 46
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    new-instance v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$5;

    invoke-direct {v4, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory$5;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v3, v1

    move-object/from16 v0, p0

    .line 21
    invoke-direct {v0, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
