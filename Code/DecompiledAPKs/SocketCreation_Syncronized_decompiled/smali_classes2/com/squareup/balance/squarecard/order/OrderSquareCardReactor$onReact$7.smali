.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->onReact(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->access$fetchStamps(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$7;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuccess(Lio/reactivex/Single;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
