.class public final synthetic Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->values()[Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ADDRESS:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->BIRTHDATE:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->SSN:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    return-void
.end method
