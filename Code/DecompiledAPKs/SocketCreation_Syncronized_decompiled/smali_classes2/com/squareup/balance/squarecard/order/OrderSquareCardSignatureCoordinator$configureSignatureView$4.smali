.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureSignatureView(Lio/reactivex/Observable;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 593
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashStampButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$fadeOut(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    .line 594
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getClearButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$fadeIn(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    .line 595
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getUndoButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$fadeIn(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    .line 596
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$fadeIn(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    .line 597
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getIndicator$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$fadeIn(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    .line 598
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getDrawingModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$fadeIn(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    .line 599
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$isFadingControlsOut$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 600
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashStampButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 601
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$notifyState(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    .line 602
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getUndoStack$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/ArrayDeque;

    move-result-object v0

    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    return-void
.end method
