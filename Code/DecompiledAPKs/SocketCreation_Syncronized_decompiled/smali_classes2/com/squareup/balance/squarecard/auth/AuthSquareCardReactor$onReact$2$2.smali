.class final Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AuthSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "it",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2$2;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2$2;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;->logPersonalInfoContinueClicked()V

    .line 181
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 182
    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2$2;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;

    iget-object v1, v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    .line 183
    iget-object v2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2$2;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;

    iget-object v2, v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;->$state:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    .line 184
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v6

    .line 185
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;->getBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object v7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x13

    const/4 v10, 0x0

    .line 183
    invoke-static/range {v3 .. v10}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->copy$default(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    const/4 v2, 0x0

    .line 182
    invoke-static {v1, p1, v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$tryStartIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    move-result-object p1

    .line 181
    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2$2;->invoke(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
