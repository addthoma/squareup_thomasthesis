.class public final Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CardOrderingAuthPersonalInfoWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOrderingAuthPersonalInfoWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOrderingAuthPersonalInfoWorkflow.kt\ncom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,68:1\n149#2,5:69\n*E\n*S KotlinDebug\n*F\n+ 1 CardOrderingAuthPersonalInfoWorkflow.kt\ncom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow\n*L\n65#1,5:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000*\u0001\u000c\u0018\u00002*\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ:\u0010\u000e\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0011H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\r\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoProps;",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V",
        "logAnalytics",
        "com/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

.field private final logAnalytics:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    .line 36
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;-><init>(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-object p0
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p2, v0, v1, v2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 48
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$render$outputSink$1;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$render$outputSink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 50
    new-instance v6, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    .line 51
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerName()Ljava/lang/String;

    move-result-object v1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerAddress()Lcom/squareup/address/Address;

    move-result-object v2

    .line 53
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 54
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 62
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$render$2;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    move-object v0, v6

    .line 50
    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;-><init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 70
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 71
    const-class p2, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v0, ""

    invoke-static {p2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 72
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 70
    invoke-direct {p1, p2, v6, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;->render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
