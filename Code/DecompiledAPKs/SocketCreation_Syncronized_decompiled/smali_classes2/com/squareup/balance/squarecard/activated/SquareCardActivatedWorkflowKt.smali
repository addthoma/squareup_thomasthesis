.class public final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;
.super Ljava/lang/Object;
.source "SquareCardActivatedWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0082\u0002*,\u0010\u0004\"\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\u00052\u001c\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0007\u00a8\u0006\n"
    }
    d2 = {
        "plus",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "screenState",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;",
        "SquareCardActivatedWorkflowScreen",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/ContainerLayering;",
        "",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p0

    return-object p0
.end method

.method private static final plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;
    .locals 7

    const-string v0, "$this$plus"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    .line 508
    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p0

    return-object p0
.end method
