.class final Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardCustomizationSettings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardIdvSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lokio/BufferedSink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lokio/BufferedSink;",
        "Lorg/threeten/bp/LocalDate;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lokio/BufferedSink;",
        "it",
        "Lorg/threeten/bp/LocalDate;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lokio/BufferedSink;

    check-cast p2, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;->invoke(Lokio/BufferedSink;Lorg/threeten/bp/LocalDate;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {p1, p2}, Lcom/squareup/utilities/threeten/LocalDatesKt;->writeLocalDate(Lokio/BufferedSink;Lorg/threeten/bp/LocalDate;)V

    return-void
.end method
