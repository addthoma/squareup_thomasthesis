.class public final Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardCreatePinCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardCreatePinCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardCreatePinCoordinator.kt\ncom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator\n*L\n1#1,96:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0005H\u0002J\u0018\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0005H\u0002J\u0010\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0005H\u0002J\u0010\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0005H\u0002J\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "finishButton",
        "Lcom/squareup/noho/NohoButton;",
        "pinConfirmationEntry",
        "Landroid/widget/TextView;",
        "pinEntry",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleBack",
        "screen",
        "update",
        "updateActionBar",
        "updateFinishButton",
        "data",
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;",
        "updateLayout",
        "watchInputs",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private finishButton:Lcom/squareup/noho/NohoButton;

.field private pinConfirmationEntry:Landroid/widget/TextView;

.field private pinEntry:Landroid/widget/TextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getPinEntry$p(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;)Landroid/widget/TextView;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinEntry:Landroid/widget/TextView;

    if-nez p0, :cond_0

    const-string v0, "pinEntry"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->handleBack(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    return-void
.end method

.method public static final synthetic access$setPinEntry$p(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Landroid/widget/TextView;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinEntry:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Landroid/view/View;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->update(Landroid/view/View;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    return-void
.end method

.method public static final synthetic access$updateFinishButton(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->updateFinishButton(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 90
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 91
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_pin_entry:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinEntry:Landroid/widget/TextView;

    .line 92
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_pin_confirmation_entry:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinConfirmationEntry:Landroid/widget/TextView;

    .line 93
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_activation_submit_pin:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->finishButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final handleBack(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V
    .locals 1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$GoBack;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V
    .locals 1

    .line 65
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 66
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->updateActionBar(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    .line 67
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->updateLayout(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    .line 68
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->updateFinishButton(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V

    .line 69
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->watchInputs(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V
    .locals 5

    .line 82
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;->getData()Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;->getActionBarConfigs()Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData$ActionBarConfig;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 83
    :cond_0
    new-instance v2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 84
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData$ActionBarConfig;->getTitle()I

    move-result v4

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v2

    .line 85
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData$ActionBarConfig;->getIcon()Lcom/squareup/noho/UpIcon;

    move-result-object v0

    new-instance v3, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v2, v0, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateFinishButton(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->finishButton:Lcom/squareup/noho/NohoButton;

    const-string v1, "finishButton"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;->getPrimaryButtonText()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 58
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->finishButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinEntry:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "pinEntry"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinConfirmationEntry:Landroid/widget/TextView;

    if-nez v1, :cond_3

    const-string v2, "pinConfirmationEntry"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    return-void
.end method

.method private final updateLayout(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->finishButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "finishButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 78
    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$updateLayout$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$updateLayout$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    .line 77
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final watchInputs(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinEntry:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const-string v1, "pinEntry"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 52
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->pinConfirmationEntry:Landroid/widget/TextView;

    if-nez p1, :cond_1

    const-string v1, "pinConfirmationEntry"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->bindViews(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026iew, it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
