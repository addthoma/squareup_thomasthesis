.class public final Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;
.super Ljava/lang/Object;
.source "NotificationPreferencesErrorWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000f\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00062\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;",
        "",
        "title",
        "Lcom/squareup/resources/TextModel;",
        "",
        "allowRetry",
        "",
        "(Lcom/squareup/resources/TextModel;Z)V",
        "getAllowRetry",
        "()Z",
        "getTitle",
        "()Lcom/squareup/resources/TextModel;",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allowRetry:Z

.field private final title:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/resources/TextModel;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    iput-boolean p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;Lcom/squareup/resources/TextModel;ZILjava/lang/Object;)Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->copy(Lcom/squareup/resources/TextModel;Z)Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    return v0
.end method

.method public final copy(Lcom/squareup/resources/TextModel;Z)Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;-><init>(Lcom/squareup/resources/TextModel;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    iget-boolean p1, p1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowRetry()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    return v0
.end method

.method public final getTitle()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NotificationPreferencesErrorProps(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->title:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->allowRetry:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
