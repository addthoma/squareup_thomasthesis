.class public final Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "CardOrderingAuthWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOrderingAuthWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOrderingAuthWorkflow.kt\ncom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,180:1\n32#2,12:181\n*E\n*S KotlinDebug\n*F\n+ 1 CardOrderingAuthWorkflow.kt\ncom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow\n*L\n63#1,12:181\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002@\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0001BG\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000e\u0012\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000e\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000e\u00a2\u0006\u0002\u0010\u0016J\u001e\u0010\u0017\u001a\u00020\u00042\n\u0010\u0018\u001a\u00060\u0002j\u0002`\u00032\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0014\u0010\u001b\u001a\u00020\u00042\n\u0010\u0018\u001a\u00060\u0002j\u0002`\u0003H\u0002JR\u0010\u001c\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\n\u0010\u0018\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u001d\u001a\u00020\u00042\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fH\u0016J\u0010\u0010 \u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u0004H\u0016J\u001c\u0010!\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010#\u001a\u00020$H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthProps;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "cardOrderingAuthPersonalInfoWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;",
        "cardOrderingAuthSsnInfoWorkflow",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;",
        "cardOrderingAuthMissingFieldWorkflow",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;",
        "cardOrderingAuthIdvRejectionWorkflow",
        "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "initialStateFromProps",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "tryStartIdv",
        "idvSettings",
        "ssnProvided",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

.field private final cardOrderingAuthIdvRejectionWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cardOrderingAuthMissingFieldWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cardOrderingAuthPersonalInfoWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final cardOrderingAuthSsnInfoWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardOrderingAuthPersonalInfoWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardOrderingAuthSsnInfoWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardOrderingAuthMissingFieldWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardOrderingAuthIdvRejectionWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthPersonalInfoWorkflow:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthSsnInfoWorkflow:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthMissingFieldWorkflow:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthIdvRejectionWorkflow:Ljavax/inject/Provider;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$tryStartIdv(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->tryStartIdv(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    move-result-object p0

    return-object p0
.end method

.method private final initialStateFromProps(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;
    .locals 2

    .line 134
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object v0

    sget-object v1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/ApprovalState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 138
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingIdvRejection;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingIdvRejection;

    check-cast v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 137
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingPersonalInfo;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingPersonalInfo;

    check-cast v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    .line 141
    :goto_0
    new-instance v1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    invoke-direct {v1, p1, v0}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;)V

    return-object v1

    .line 135
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "APPROVED idv state should never enter into this workflow"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final tryStartIdv(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;
    .locals 3

    .line 152
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->isAddressValid()Z

    move-result v0

    .line 153
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->isBirthDateValid()Z

    move-result v1

    .line 154
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->isSsnValid()Z

    move-result v2

    if-nez v0, :cond_0

    .line 157
    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ADDRESS:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    goto :goto_0

    :cond_0
    if-nez v1, :cond_1

    .line 158
    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->BIRTHDATE:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    if-eqz p3, :cond_2

    .line 159
    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->SSN:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 164
    new-instance p3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;

    invoke-direct {p3, v0}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;-><init>(Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;)V

    check-cast p3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    .line 162
    invoke-virtual {p1, p2, p3}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;->copy(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    move-result-object p1

    return-object p1

    :cond_3
    if-nez p3, :cond_4

    .line 168
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/balance/squarecard/order/ApprovalState;->requiresSsn()Z

    move-result p3

    if-eqz p3, :cond_4

    .line 169
    sget-object p3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingSsnInfo;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingSsnInfo;

    check-cast p3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    goto :goto_1

    .line 171
    :cond_4
    sget-object p3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$PromptingIdvFlow;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$PromptingIdvFlow;

    check-cast p3, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    .line 174
    :goto_1
    invoke-virtual {p1, p2, p3}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;->copy(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 181
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 186
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 188
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 189
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 190
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 192
    :cond_3
    check-cast v2, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 63
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->initialStateFromProps(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->initialState(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    check-cast p2, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
            "-",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;->getScreenState()Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    move-result-object p1

    .line 70
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingPersonalInfo;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingPersonalInfo;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthPersonalInfoWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "cardOrderingAuthPersonalInfoWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 72
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    const/4 v4, 0x0

    .line 73
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 70
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 90
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 91
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingSsnInfo;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingSsnInfo;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthSsnInfoWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "cardOrderingAuthSsnInfoWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 93
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    const/4 v4, 0x0

    .line 94
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$2;-><init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 91
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 108
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 109
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthMissingFieldWorkflow:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "cardOrderingAuthMissingFieldWorkflow.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 111
    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v4

    const/4 v5, 0x0

    .line 112
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;-><init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 109
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 121
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 122
    :cond_2
    sget-object p2, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingIdvRejection;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingIdvRejection;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 123
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->cardOrderingAuthIdvRejectionWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "cardOrderingAuthIdvRejectionWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    .line 124
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$4;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$4;-><init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    .line 122
    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 127
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    .line 128
    :cond_3
    sget-object p2, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$PromptingIdvFlow;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$PromptingIdvFlow;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Lkotlin/NotImplementedError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "An operation is not implemented: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "BB-3096 - Submit IDV Workflow"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
