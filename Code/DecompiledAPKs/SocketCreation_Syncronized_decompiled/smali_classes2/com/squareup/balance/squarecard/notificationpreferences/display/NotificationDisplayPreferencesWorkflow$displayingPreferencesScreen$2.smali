.class final Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "NotificationDisplayPreferencesWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->displayingPreferencesScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "desiredPreferences",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;->$state:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;->invoke(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)V
    .locals 3

    const-string v0, "desiredPreferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$Action$UpdateNotificationPreferences;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;->$state:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$Action$UpdateNotificationPreferences;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
