.class public final Lcom/squareup/balance/squarecard/ui/SvgView;
.super Landroid/view/View;
.source "SvgView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ui/SvgView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSvgView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SvgView.kt\ncom/squareup/balance/squarecard/ui/SvgView\n*L\n1#1,93:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u0000 %2\u00020\u0001:\u0001%B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\u0010\u0010\u001b\u001a\u00020\u000c2\u0006\u0010\u001c\u001a\u00020\u001dH\u0014J\u0018\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u0007H\u0014J\u000e\u0010!\u001a\u00020\u000c2\u0006\u0010\"\u001a\u00020\u000bJ\u0014\u0010#\u001a\u00020\u0014*\u00020\u00142\u0006\u0010$\u001a\u00020\u0016H\u0002R(\u0010\t\u001a\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ui/SvgView;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "clickListener",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "",
        "getClickListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setClickListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "paint",
        "Landroid/graphics/Paint;",
        "scaledMatrix",
        "Landroid/graphics/Matrix;",
        "svgStamp",
        "Lcom/squareup/cardcustomizations/stampview/Stamp;",
        "transformationMatrix",
        "viewBounds",
        "Landroid/graphics/RectF;",
        "createPaint",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "setStamp",
        "inputStamp",
        "updateBounds",
        "stamp",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/ui/SvgView$Companion;

.field private static final SVG_OFFSET:F = 4.0f


# instance fields
.field private clickListener:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final paint:Landroid/graphics/Paint;

.field private final scaledMatrix:Landroid/graphics/Matrix;

.field private svgStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

.field private final transformationMatrix:Landroid/graphics/Matrix;

.field private final viewBounds:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/ui/SvgView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/ui/SvgView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/ui/SvgView;->Companion:Lcom/squareup/balance/squarecard/ui/SvgView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ui/SvgView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ui/SvgView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->viewBounds:Landroid/graphics/RectF;

    .line 28
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->transformationMatrix:Landroid/graphics/Matrix;

    .line 29
    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->scaledMatrix:Landroid/graphics/Matrix;

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ui/SvgView;->createPaint(Landroid/content/Context;)Landroid/graphics/Paint;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->paint:Landroid/graphics/Paint;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 24
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 25
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/ui/SvgView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final createPaint(Landroid/content/Context;)Landroid/graphics/Paint;
    .locals 2

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 81
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/balance/squarecard/impl/R$dimen;->default_stamp_stroke_width:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 83
    sget-object p1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 84
    sget-object p1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    const/high16 p1, -0x1000000

    .line 85
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-object v0
.end method

.method private final updateBounds(Landroid/graphics/Matrix;Lcom/squareup/cardcustomizations/stampview/Stamp;)Landroid/graphics/Matrix;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->transformationMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p2, v0}, Lcom/squareup/cardcustomizations/stampview/Stamp;->bounds(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->viewBounds:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {p1, p2, v0, v1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    return-object p1
.end method


# virtual methods
.method public final getClickListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->clickListener:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->svgStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    if-eqz v0, :cond_0

    .line 70
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->paint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->scaledMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v2, v0}, Lcom/squareup/balance/squarecard/ui/SvgView;->updateBounds(Landroid/graphics/Matrix;Lcom/squareup/cardcustomizations/stampview/Stamp;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/cardcustomizations/stampview/Stamp;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Matrix;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .line 56
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 57
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->viewBounds:Landroid/graphics/RectF;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SvgView;->getMeasuredWidth()I

    move-result p2

    int-to-float p2, p2

    const/high16 v0, 0x40800000    # 4.0f

    sub-float/2addr p2, v0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SvgView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v0

    .line 57
    invoke-virtual {p1, v0, v0, p2, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 61
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->svgStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    if-eqz p1, :cond_0

    .line 62
    iget-object p2, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->scaledMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getCanvasBounds()Landroid/graphics/RectF;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->viewBounds:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {p2, p1, v0, v1}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    :cond_0
    return-void
.end method

.method public final setClickListener(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->clickListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setStamp(Lcom/squareup/protos/client/bizbank/Stamp;)V
    .locals 4

    const-string v0, "inputStamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    :try_start_0
    new-instance v0, Lcom/squareup/cardcustomizations/stampview/Stamp;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    const-string v2, "inputStamp.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    const-string v3, "inputStamp.svg"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardcustomizations/stampview/Stamp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    new-instance v1, Lcom/squareup/balance/squarecard/ui/SvgView$setStamp$$inlined$apply$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ui/SvgView$setStamp$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/squarecard/ui/SvgView;Lcom/squareup/protos/client/bizbank/Stamp;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v1}, Lcom/squareup/balance/squarecard/ui/SvgView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->scaledMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getCanvasBounds()Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->viewBounds:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 38
    iput-object v0, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->svgStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ui/SvgView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    check-cast p1, Lcom/squareup/cardcustomizations/stampview/Stamp;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ui/SvgView;->svgStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SvgView;->invalidate()V

    return-void
.end method
