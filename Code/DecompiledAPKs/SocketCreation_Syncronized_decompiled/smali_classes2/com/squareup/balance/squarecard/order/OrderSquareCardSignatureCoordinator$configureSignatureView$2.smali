.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureSignatureView(Lio/reactivex/Observable;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Landroid/graphics/Rect;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012z\u0010\u0002\u001av\u0012\u0004\u0012\u00020\u0004\u0012.\u0012,\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007 \t*\u0016\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005j\u0004\u0018\u0001`\u00080\u0005j\u0002`\u0008 \t*:\u0012\u0004\u0012\u00020\u0004\u0012.\u0012,\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007 \t*\u0016\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005j\u0004\u0018\u0001`\u00080\u0005j\u0002`\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Landroid/graphics/Rect;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 80
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Landroid/graphics/Rect;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 559
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$setStrokeWidth$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;F)V

    .line 560
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStrokeWidth$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setStrokeWidth(F)V

    .line 561
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStrokeWidth$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setStrokeWidth(F)V

    .line 562
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;

    move-result-object v1

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v2}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setSignatureStartBounds(Landroid/graphics/RectF;)V

    .line 563
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->setBoundingBox(Landroid/graphics/Rect;)V

    .line 566
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    const v2, 0x3f333333    # 0.7f

    mul-float v0, v0, v2

    invoke-static {v1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$setStampSize$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;F)V

    .line 568
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$maybeRestoreSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Ljava/lang/String;)V

    .line 569
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getStampsToRestore()Ljava/util/List;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$restoreStamps(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Ljava/util/List;)V

    .line 575
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;

    move-result-object p1

    const-string v0, "SignatureViewReadyForUiTestingTag"

    invoke-virtual {p1, v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setTag(Ljava/lang/Object;)V

    return-void
.end method
