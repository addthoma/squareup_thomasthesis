.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "AuthSquareCardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 11
    sget-object v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 12
    sget v3, Lcom/squareup/balance/squarecard/impl/R$layout;->auth_square_card_personal_info_view:I

    .line 13
    sget-object v4, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 10
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 15
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    sget-object v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 17
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->auth_square_card_ssn_info_view:I

    .line 18
    sget-object v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$2;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$2;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    .line 15
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 20
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 21
    sget-object v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 22
    sget-object v3, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$3;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 20
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 24
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 25
    sget-object v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 26
    sget-object v3, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$4;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$4;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 24
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 28
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 29
    sget-object v1, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;->getSQUARE_CARD_PROGRESS_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 30
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_progress_view:I

    .line 31
    sget-object v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$5;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory$5;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 28
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
