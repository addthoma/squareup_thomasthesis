.class public interface abstract Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;
.super Ljava/lang/Object;
.source "OrderSquareCardAnalytics.kt"

# interfaces
.implements Lcom/squareup/mailorder/MailOrderAnalytics;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\n\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0008\u0010\n\u001a\u00020\u0003H&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0008\u0010\u000c\u001a\u00020\u0003H&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;",
        "Lcom/squareup/mailorder/MailOrderAnalytics;",
        "logBusinessInfoErrorScreen",
        "",
        "logBusinessInfoScreen",
        "logCancelFromOrderSplash",
        "logCardCustomizationScreen",
        "logNoOwnerNameBusinessInfoErrorScreen",
        "logOrderSquareCardClick",
        "logOrderSquareCardScreen",
        "logOrderSquareCardTermsOfServiceClick",
        "logPersonalizeCardClick",
        "logSignatureErrorScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logBusinessInfoErrorScreen()V
.end method

.method public abstract logBusinessInfoScreen()V
.end method

.method public abstract logCancelFromOrderSplash()V
.end method

.method public abstract logCardCustomizationScreen()V
.end method

.method public abstract logNoOwnerNameBusinessInfoErrorScreen()V
.end method

.method public abstract logOrderSquareCardClick()V
.end method

.method public abstract logOrderSquareCardScreen()V
.end method

.method public abstract logOrderSquareCardTermsOfServiceClick()V
.end method

.method public abstract logPersonalizeCardClick()V
.end method

.method public abstract logSignatureErrorScreen()V
.end method
