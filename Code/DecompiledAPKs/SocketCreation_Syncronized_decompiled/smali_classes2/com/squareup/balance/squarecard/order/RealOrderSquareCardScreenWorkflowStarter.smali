.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "RealOrderSquareCardScreenWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;,
        Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderSquareCardScreenWorkflowStarter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderSquareCardScreenWorkflowStarter.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter\n*L\n1#1,287:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 %2\u00020\u0001:\u0002%&B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002JD\u0010\u000b\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000f\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00110\u000e0\r\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u000cj\u0002`\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016JD\u0010\u000b\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000f\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00110\u000e0\r\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u000cj\u0002`\u00142\u0006\u0010\u0017\u001a\u00020\u0018H\u0016JH\u0010\u0019\u001a2\u0012.\u0008\u0001\u0012*\u0012&\u0012$\u0012\u0004\u0012\u00020\u000f\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00110\u000ej\u0008\u0012\u0004\u0012\u00020\u000f`\u001b0\r0\u001a2\u0006\u0010\t\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u000c\u0010\u001f\u001a\u00020 *\u00020!H\u0002J\u000c\u0010\"\u001a\u00020#*\u00020$H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;",
        "reactor",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/util/Device;)V",
        "orderSquareCardBusinessNameScreenData",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;",
        "state",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflow;",
        "startArg",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toWorkflowState",
        "Lrx/Observable;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "input",
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;",
        "toCardSignatureScreenData",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;",
        "toCardStampsDialogScreenData",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;",
        "Companion",
        "ScreenInput",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;

.field private static final SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final reactor:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;

    .line 85
    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardProgress;

    sget-object v1, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$Companion;

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->createKey(Ljava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static final synthetic access$getSQUARE_CARD_PROGRESS_KEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static final synthetic access$toWorkflowState(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;
    .locals 0

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->toWorkflowState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final orderSquareCardBusinessNameScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;)Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;
    .locals 2

    .line 257
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object p1

    .line 258
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getOwnerName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final toCardSignatureScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;
    .locals 9

    .line 262
    new-instance v8, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    .line 263
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    .line 264
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getMinInkLevel()F

    move-result v2

    .line 265
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getMaxInkLevel()F

    move-result v3

    .line 266
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v4

    .line 267
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v5

    .line 268
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    sget-object v6, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;->ALLOW:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    if-ne v0, v6, :cond_0

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 269
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;->getStampsToRestore()Ljava/util/List;

    move-result-object v7

    move-object v0, v8

    .line 262
    invoke-direct/range {v0 .. v7}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;-><init>(Ljava/lang/String;FFLcom/squareup/protos/client/bizbank/Stamp;Ljava/lang/String;ZLjava/util/List;)V

    return-object v8
.end method

.method private final toCardStampsDialogScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;
    .locals 2

    .line 275
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Loading;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Loading;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;

    goto :goto_0

    .line 276
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    if-eqz v0, :cond_3

    .line 277
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStatus()Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    move-result-object v0

    .line 278
    instance-of v1, v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Display;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStatus()Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;->getStamps()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData$Display;-><init>(Ljava/util/List;)V

    .line 277
    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;

    :goto_0
    return-object p1

    .line 281
    :cond_1
    instance-of p1, v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error;

    if-eqz p1, :cond_2

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "OrderSquareCardStampsDialog does not handle error scenarios."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 277
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toWorkflowState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 150
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 152
    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;

    iget-object v4, v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->device:Lcom/squareup/util/Device;

    invoke-interface {v4}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v4

    invoke-direct {v3, v4}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;-><init>(Z)V

    .line 153
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForSplash()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 151
    invoke-static {v3, v4}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashScreenKt;->OrderSquareCardSplashScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v3

    .line 150
    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v2

    .line 158
    sget-object v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_2

    .line 160
    :cond_0
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettings;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 161
    sget-object v5, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 162
    new-instance v6, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;

    sget v7, Lcom/squareup/balance/squarecard/impl/R$string;->fetching_card_details_spinner_message:I

    invoke-direct {v6, v7}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;-><init>(I)V

    .line 163
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 160
    invoke-direct {v4, v5, v6, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    goto/16 :goto_2

    .line 166
    :cond_1
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v3, :cond_4

    .line 170
    move-object v3, v1

    check-cast v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;->getErrorType()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;

    move-result-object v3

    sget-object v6, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;->ordinal()I

    move-result v3

    aget v3, v6, v3

    if-eq v3, v5, :cond_3

    if-ne v3, v4, :cond_2

    .line 176
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_settings_no_owner_name_error_title:I

    .line 177
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_settings_no_owner_name_error_message:I

    goto :goto_0

    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 172
    :cond_3
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_settings_fetch_error_title:I

    .line 173
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_settings_fetch_error_message:I

    .line 181
    :goto_0
    sget-object v5, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 182
    sget-object v7, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 183
    new-instance v15, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 184
    new-instance v8, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v8, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v9, v8

    check-cast v9, Lcom/squareup/util/ViewString;

    .line 185
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v10, v3

    check-cast v10, Lcom/squareup/util/ViewString;

    .line 186
    sget v11, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v12, 0x0

    const/16 v13, 0x8

    const/4 v14, 0x0

    move-object v8, v15

    .line 183
    invoke-direct/range {v8 .. v14}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 188
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 181
    invoke-direct {v6, v7, v15, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v5, v6}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    goto/16 :goto_2

    .line 192
    :cond_4
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    if-eqz v3, :cond_5

    sget-object v3, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 193
    move-object v4, v1

    check-cast v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    invoke-direct {v0, v4}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->orderSquareCardBusinessNameScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;)Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForBusinessName()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 192
    invoke-static {v4, v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameScreenKt;->OrderSquareCardBusinessNameScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessName$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    goto/16 :goto_2

    .line 196
    :cond_5
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    if-eqz v3, :cond_6

    sget-object v3, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 197
    move-object v4, v1

    check-cast v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;

    invoke-direct {v0, v4}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->toCardSignatureScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    move-result-object v4

    .line 198
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForSignature()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 196
    invoke-static {v4, v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureScreenKt;->OrderSquareCardSignatureScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    goto/16 :goto_2

    .line 201
    :cond_6
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    if-eqz v3, :cond_7

    sget-object v3, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 202
    sget-object v5, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 203
    new-instance v6, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;

    sget v7, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customizing_message:I

    invoke-direct {v6, v7}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;-><init>(I)V

    .line 204
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 201
    invoke-direct {v4, v5, v6, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    goto/16 :goto_2

    .line 207
    :cond_7
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    if-eqz v3, :cond_b

    .line 208
    move-object v3, v1

    check-cast v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getInkLevel()Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v3

    sget-object v6, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v3}, Lcom/squareup/cardcustomizations/signature/InkLevel;->ordinal()I

    move-result v3

    aget v3, v6, v3

    if-eq v3, v5, :cond_a

    if-eq v3, v4, :cond_9

    const/4 v4, 0x3

    if-ne v3, v4, :cond_8

    .line 218
    new-instance v3, Lkotlin/Pair;

    .line 219
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_error_title:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 220
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_error_message:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 218
    invoke-direct {v3, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_8
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 213
    :cond_9
    new-instance v3, Lkotlin/Pair;

    .line 214
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_error_title:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 215
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_too_much_ink_error_message:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 213
    invoke-direct {v3, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 209
    :cond_a
    new-instance v3, Lkotlin/Pair;

    .line 210
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_error_title:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 211
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_too_little_ink_error_message:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 209
    invoke-direct {v3, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 208
    :goto_1
    invoke-virtual {v3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-virtual {v3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 223
    sget-object v5, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 224
    sget-object v7, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 225
    new-instance v15, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 226
    new-instance v8, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v8, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v9, v8

    check-cast v9, Lcom/squareup/util/ViewString;

    .line 227
    new-instance v4, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v4, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v10, v4

    check-cast v10, Lcom/squareup/util/ViewString;

    .line 228
    sget v11, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v12, 0x0

    const/16 v13, 0x8

    const/4 v14, 0x0

    move-object v8, v15

    .line 225
    invoke-direct/range {v8 .. v14}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 230
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 223
    invoke-direct {v6, v7, v15, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v5, v6}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    goto :goto_2

    .line 234
    :cond_b
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;

    if-eqz v3, :cond_c

    .line 236
    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;->getSubWorkflow()Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v1

    .line 238
    sget-object v3, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$toWorkflowState$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$toWorkflowState$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v3}, Lcom/squareup/workflow/rx1/WorkflowKt;->mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v1

    .line 240
    new-instance v3, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$toWorkflowState$2;

    invoke-direct {v3, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$toWorkflowState$2;-><init>(Ljava/util/Map;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v3}, Lcom/squareup/workflow/rx1/ScreensKt;->mapScreen(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/workflow/rx1/Workflow;->getState()Lrx/Observable;

    move-result-object v1

    return-object v1

    .line 243
    :cond_c
    instance-of v3, v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;

    if-eqz v3, :cond_d

    .line 244
    sget-object v3, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 245
    move-object v4, v1

    check-cast v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;

    invoke-direct {v0, v4}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->toCardSignatureScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    move-result-object v4

    .line 246
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForSignature()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 244
    invoke-static {v4, v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureScreenKt;->OrderSquareCardSignatureScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    .line 243
    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    .line 248
    sget-object v3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    .line 249
    move-object v4, v1

    check-cast v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;

    invoke-direct {v0, v4}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->toCardStampsDialogScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;)Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;

    move-result-object v4

    .line 250
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;->getForStamps()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 248
    invoke-static {v4, v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogScreenKt;->OrderSquareCardStampsDialogScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    .line 247
    invoke-static {v2, v3}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v2

    .line 252
    :goto_2
    new-instance v3, Lcom/squareup/workflow/ScreenState;

    invoke-static/range {p1 .. p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->toSnapshot(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object v1

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    invoke-static {v3}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-string v2, "just(ScreenState(this, state.toSnapshot()))"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "when (state) {\n      Dis\u2026s, state.toSnapshot())) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1

    .line 247
    :cond_d
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method


# virtual methods
.method public adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 75
    invoke-static {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter$DefaultImpls;->adapter(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v0

    return-object v0
.end method

.method public start(Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    .line 90
    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->startWorkflow(Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 91
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$1;-><init>(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->switchMapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    .line 96
    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 97
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;-><init>(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->switchMapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
