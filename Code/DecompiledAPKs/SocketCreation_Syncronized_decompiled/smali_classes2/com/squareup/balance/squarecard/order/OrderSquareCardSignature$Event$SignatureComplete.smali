.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;
.super Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;
.source "OrderSquareCardSignatureScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SignatureComplete"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "signaturePayload",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
        "signatureAsJson",
        "",
        "stampsToRestore",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V",
        "getSignatureAsJson",
        "()Ljava/lang/String;",
        "getSignaturePayload",
        "()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
        "getStampsToRestore",
        "()Ljava/util/List;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final signatureAsJson:Ljava/lang/String;

.field private final signaturePayload:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

.field private final stampsToRestore:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    const-string v0, "signaturePayload"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureAsJson"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsToRestore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->signaturePayload:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->signatureAsJson:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->stampsToRestore:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getSignatureAsJson()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->signatureAsJson:Ljava/lang/String;

    return-object v0
.end method

.method public final getSignaturePayload()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->signaturePayload:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    return-object v0
.end method

.method public final getStampsToRestore()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->stampsToRestore:Ljava/util/List;

    return-object v0
.end method
