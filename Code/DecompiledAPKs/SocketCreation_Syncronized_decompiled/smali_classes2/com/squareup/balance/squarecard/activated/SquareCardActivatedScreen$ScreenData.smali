.class public final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;
.super Ljava/lang/Object;
.source "SquareCardActivatedScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008 \n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u00014Be\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0005H\u00c6\u0003J\t\u0010#\u001a\u00020\u0005H\u00c6\u0003J\t\u0010$\u001a\u00020\u0010H\u00c6\u0003J\t\u0010%\u001a\u00020\u0005H\u00c6\u0003J\t\u0010&\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0005H\u00c6\u0003J\t\u0010(\u001a\u00020\u0005H\u00c6\u0003J\t\u0010)\u001a\u00020\u0005H\u00c6\u0003J\t\u0010*\u001a\u00020\u0005H\u00c6\u0003J\t\u0010+\u001a\u00020\u0005H\u00c6\u0003J\t\u0010,\u001a\u00020\u0005H\u00c6\u0003J\u0081\u0001\u0010-\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00052\u0008\u0008\u0002\u0010\r\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010H\u00c6\u0001J\u0013\u0010.\u001a\u00020\u00052\u0008\u0010/\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00100\u001a\u000201H\u00d6\u0001J\t\u00102\u001a\u000203H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0013R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0013R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0013R\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0013R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0013R\u0011\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0013\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;",
        "",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "allowBack",
        "",
        "showGetHelp",
        "showCardToggle",
        "showCardDisabled",
        "showCardIsSuspended",
        "showAddToGooglePay",
        "showResetCardPin",
        "showNotificationPreferences",
        "toggleIsChecked",
        "toggleIsInProgress",
        "privateDataToggleState",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;",
        "(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;)V",
        "getAllowBack",
        "()Z",
        "getCard",
        "()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "getPrivateDataToggleState",
        "()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;",
        "getShowAddToGooglePay",
        "getShowCardDisabled",
        "getShowCardIsSuspended",
        "getShowCardToggle",
        "getShowGetHelp",
        "getShowNotificationPreferences",
        "getShowResetCardPin",
        "getToggleIsChecked",
        "getToggleIsInProgress",
        "component1",
        "component10",
        "component11",
        "component12",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "ToggleState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allowBack:Z

.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field private final privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

.field private final showAddToGooglePay:Z

.field private final showCardDisabled:Z

.field private final showCardIsSuspended:Z

.field private final showCardToggle:Z

.field private final showGetHelp:Z

.field private final showNotificationPreferences:Z

.field private final showResetCardPin:Z

.field private final toggleIsChecked:Z

.field private final toggleIsInProgress:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "privateDataToggleState"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iput-boolean p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    iput-boolean p3, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    iput-boolean p4, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    iput-boolean p5, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    iput-boolean p6, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    iput-boolean p7, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    iput-boolean p8, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    iput-boolean p9, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    iput-boolean p10, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    iput-boolean p11, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    iput-object p12, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;
    .locals 13

    move-object v0, p0

    move/from16 v1, p13

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-boolean v9, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-boolean v10, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-boolean v11, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-boolean v12, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    goto :goto_b

    :cond_b
    move-object/from16 v1, p12

    :goto_b
    move-object p1, v2

    move p2, v3

    move/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move-object/from16 p12, v1

    invoke-virtual/range {p0 .. p12}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    return v0
.end method

.method public final component11()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    return v0
.end method

.method public final component12()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    return v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    return v0
.end method

.method public final copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;
    .locals 14

    const-string v0, "card"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "privateDataToggleState"

    move-object/from16 v13, p12

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    move-object v1, v0

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v1 .. v13}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZZZZZZZZLcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    iget-boolean v1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowBack()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    return v0
.end method

.method public final getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final getPrivateDataToggleState()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    return-object v0
.end method

.method public final getShowAddToGooglePay()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    return v0
.end method

.method public final getShowCardDisabled()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    return v0
.end method

.method public final getShowCardIsSuspended()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    return v0
.end method

.method public final getShowCardToggle()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    return v0
.end method

.method public final getShowGetHelp()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    return v0
.end method

.method public final getShowNotificationPreferences()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    return v0
.end method

.method public final getShowResetCardPin()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    return v0
.end method

.method public final getToggleIsChecked()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    return v0
.end method

.method public final getToggleIsInProgress()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :cond_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_b
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->allowBack:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showGetHelp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showGetHelp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showCardToggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardToggle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showCardDisabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardDisabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showCardIsSuspended="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showCardIsSuspended:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showAddToGooglePay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showAddToGooglePay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showResetCardPin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showResetCardPin:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showNotificationPreferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->showNotificationPreferences:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", toggleIsChecked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsChecked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", toggleIsInProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->toggleIsInProgress:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", privateDataToggleState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData;->privateDataToggleState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$ScreenData$ToggleState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
