.class public final Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;
.super Ljava/lang/Object;
.source "ShowPrivateCardDataTwoFactorDataStore.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nShowPrivateCardDataTwoFactorDataStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ShowPrivateCardDataTwoFactorDataStore.kt\ncom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore\n*L\n1#1,69:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u001e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00060\n2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "convertToVerificationResult",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;",
        "createUserResponse",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;",
        "startTwoFactorAuth",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;",
        "cardToken",
        "",
        "verifyTwoFactorAuth",
        "authenticationToken",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method public static final synthetic access$convertToVerificationResult(Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;->convertToVerificationResult(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    move-result-object p0

    return-object p0
.end method

.method private final convertToVerificationResult(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;
    .locals 2

    .line 60
    sget-object v0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 65
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal result type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 64
    :cond_1
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->UNKNOWN_FAILURE:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    goto :goto_0

    .line 63
    :cond_2
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->TOKEN_EXPIRED:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    goto :goto_0

    .line 62
    :cond_3
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->INCORRECT_TOKEN:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    goto :goto_0

    .line 61
    :cond_4
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->SUCCESS:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public startTwoFactorAuth(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;",
            ">;"
        }
    .end annotation

    const-string v0, "cardToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;-><init>()V

    .line 22
    sget-object v1, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->SHOW_PRIVATE_CARD_DATA:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->request_type(Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;)Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;

    move-result-object v0

    .line 23
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;

    move-result-object p1

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    .line 27
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->startTwoFactorAuthentication(Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 29
    sget-object v0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$startTwoFactorAuth$1;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$startTwoFactorAuth$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026URE\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public verifyTwoFactorAuth(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "cardToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authenticationToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;-><init>()V

    .line 42
    invoke-virtual {v0, p2}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->authentication_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;

    move-result-object p2

    .line 43
    invoke-virtual {p2, p1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;

    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest$Builder;->build()Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;

    move-result-object p1

    .line 46
    iget-object p2, p0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v0, "request"

    .line 47
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->createUserAuthorization(Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$CreateUserAuthorizationStandardResponse;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService$CreateUserAuthorizationStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 49
    new-instance p2, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$verifyTwoFactorAuth$1;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$verifyTwoFactorAuth$1;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService\n        .\u2026URE\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
