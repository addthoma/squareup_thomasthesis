.class final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;
.super Ljava/lang/Object;
.source "SquareCardOrderedReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->finishCardActivationRequest(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic $state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

.field final synthetic $token:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$token:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 542
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getSwipeCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 544
    :goto_0
    instance-of v1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_1

    .line 545
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->access$logCardActivationSuccess(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Z)V

    .line 546
    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->access$isBillingDuringActivationEnabled(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;)Z

    move-result v1

    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Z)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_1

    .line 548
    :cond_1
    instance-of v1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v1, :cond_2

    .line 549
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    invoke-static {v1, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->access$logCardActivationFailure(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Z)V

    .line 550
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    .line 551
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->access$isIncorrectCardInfo(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z

    move-result v4

    .line 552
    iget-object v3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    .line 553
    iget-object v5, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$token:Ljava/lang/String;

    .line 554
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getExpiration()Ljava/lang/String;

    move-result-object v6

    .line 555
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;->getCvv()Ljava/lang/String;

    move-result-object v7

    move-object v2, v0

    .line 550
    invoke-direct/range {v2 .. v7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$finishCardActivationRequest$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object p1

    return-object p1
.end method
