.class public final Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "SubmittingCardFeedbackWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSubmittingCardFeedbackWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SubmittingCardFeedbackWorkflow.kt\ncom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,82:1\n85#2:83\n240#3:84\n276#4:85\n*E\n*S KotlinDebug\n*F\n+ 1 SubmittingCardFeedbackWorkflow.kt\ncom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow\n*L\n79#1:83\n79#1:84\n79#1:85\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002*\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ:\u0010\u000b\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00072\u0006\u0010\u000c\u001a\u00020\u00022\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u000eH\u0016J\u001e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "submitFeedback",
        "Lcom/squareup/workflow/Worker;",
        "feedback",
        "",
        "feedbackSource",
        "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method private final submitFeedback(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackOutput;",
            ">;"
        }
    .end annotation

    .line 65
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;-><init>()V

    .line 66
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;->feedback_text(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;

    move-result-object p1

    .line 67
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;->feedback_source(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;

    move-result-object p1

    .line 71
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v0, "provideFeedbackRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->provideFeedback(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 73
    sget-object p2, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$submitFeedback$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$submitFeedback$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService.provideFe\u2026led\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$submitFeedback$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$submitFeedback$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 84
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 85
    const-class p2, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackOutput;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;->getFeedback()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;->getFeedbackSource()Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;->submitFeedback(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)Lcom/squareup/workflow/Worker;

    move-result-object v3

    new-instance p1, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p2

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 54
    new-instance p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 55
    new-instance p2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->submitting_feedback_spinner_messsage:I

    invoke-direct {p2, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast p2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 56
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$render$2;

    invoke-direct {v1, v0}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 54
    invoke-direct {p1, p2, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 57
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;->render(Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
