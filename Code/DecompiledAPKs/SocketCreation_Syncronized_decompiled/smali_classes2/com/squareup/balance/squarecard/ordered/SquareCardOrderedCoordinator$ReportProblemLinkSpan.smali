.class final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;
.super Lcom/squareup/ui/LinkSpan;
.source "SquareCardOrderedCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ReportProblemLinkSpan"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;",
        "Lcom/squareup/ui/LinkSpan;",
        "context",
        "Landroid/content/Context;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
        "(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "getWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget v0, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public final getWorkflow()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event$GetHelpWithCard;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event$GetHelpWithCard;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
