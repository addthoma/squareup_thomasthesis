.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderSquareCardSplashCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardSplashCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardSplashCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n1103#2,7:88\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSquareCardSplashCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator\n*L\n46#1,7:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000cH\u0016J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000cH\u0002J\u001e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0017H\u0002J \u0010\u0018\u001a\u00020\u00102\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J(\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000c2\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "orderSquareCard",
        "Landroid/view/View;",
        "orderSquareCardCardHolderAgreement",
        "Landroid/widget/TextView;",
        "attach",
        "",
        "view",
        "bindViews",
        "formatTermsOfService",
        "context",
        "Landroid/content/Context;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "handleBack",
        "screen",
        "update",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private orderSquareCard:Landroid/view/View;

.field private orderSquareCardCardHolderAgreement:Landroid/widget/TextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->handleBack(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 82
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->orderSquareCard:Landroid/view/View;

    .line 83
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->order_square_card_cardholder_agreement:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->orderSquareCardCardHolderAgreement:Landroid/widget/TextView;

    .line 84
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method

.method private final formatTermsOfService(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;)V"
        }
    .end annotation

    .line 67
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_terms_of_service_url:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 69
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$formatTermsOfService$termsOfService$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$formatTermsOfService$termsOfService$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->onClick(Ljava/lang/Runnable;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 71
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_terms_of_service_link:I

    invoke-virtual {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 72
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object p2

    .line 77
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->orderSquareCardCardHolderAgreement:Landroid/widget/TextView;

    const-string v1, "orderSquareCardCardHolderAgreement"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_0
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_terms_of_service_hint:I

    invoke-static {p1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 76
    check-cast p2, Ljava/lang/CharSequence;

    const-string v2, "terms_of_service"

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->orderSquareCardCardHolderAgreement:Landroid/widget/TextView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;)V"
        }
    .end annotation

    .line 58
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;->getAllowBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$Event;",
            ">;)V"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->orderSquareCard:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "orderSquareCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 88
    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->formatTermsOfService(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 50
    :cond_1
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 51
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->square_card:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 52
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$update$3;

    invoke-direct {v2, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$update$3;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 53
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;->getAllowBack()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 54
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->bindViews(Landroid/view/View;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSplashCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
