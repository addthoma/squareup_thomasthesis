.class public final Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;
.super Ljava/lang/Object;
.source "NotificationPreferencesLoadingLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationPreferencesLoadingLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationPreferencesLoadingLayoutRunner.kt\ncom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner\n*L\n1#1,58:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00102\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "showBackButton",
        "",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final showBackButton:Z

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->view:Landroid/view/View;

    .line 26
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->notification_loading_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 27
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->showBackButton:Z

    .line 32
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 30
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 31
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->notification_preferences_action_bar_title:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 48
    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 40
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 42
    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->showBackButton:Z

    if-eqz v1, :cond_0

    .line 43
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$showRendering$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$showRendering$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 48
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
