.class public final Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;
.super Ljava/lang/Object;
.source "CancelSquareCardWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p8, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
            ")",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;"
        }
    .end annotation

    .line 75
    new-instance v9, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;
    .locals 8

    .line 55
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    invoke-static/range {v0 .. v7}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow_Factory;->get()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    move-result-object v0

    return-object v0
.end method
