.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;
.super Ljava/lang/Object;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->sendSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->access$getStateFactory$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    move-result-object v1

    .line 558
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSkippedInitialScreens()Z

    move-result v2

    .line 559
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v3

    .line 560
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v4

    .line 561
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationResponse;

    iget-object v5, p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationResponse;->cutomization_token:Ljava/lang/String;

    const-string p1, "successOrFailure.response.cutomization_token"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 562
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getStampsToRestore()Ljava/util/List;

    move-result-object v6

    .line 557
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->startOrderingSquareCard(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p1

    goto :goto_0

    .line 564
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    .line 565
    sget-object v1, Lcom/squareup/cardcustomizations/signature/InkLevel;->JUST_RIGHT:Lcom/squareup/cardcustomizations/signature/InkLevel;

    .line 566
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSkippedInitialScreens()Z

    move-result v2

    .line 567
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v3

    .line 568
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v4

    .line 569
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v5

    .line 570
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v6

    .line 571
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->$sendingSignature:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignature;->getStampsToRestore()Ljava/util/List;

    move-result-object v7

    move-object v0, p1

    .line 564
    invoke-direct/range {v0 .. v7}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;-><init>(Lcom/squareup/cardcustomizations/signature/InkLevel;ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$sendSignature$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p1

    return-object p1
.end method
