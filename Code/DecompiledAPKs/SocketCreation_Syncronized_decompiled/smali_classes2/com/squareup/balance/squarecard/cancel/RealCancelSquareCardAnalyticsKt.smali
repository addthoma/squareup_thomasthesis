.class public final Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalyticsKt;
.super Ljava/lang/Object;
.source "RealCancelSquareCardAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0014\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "CANCEL_CARD_ERROR_SCREEN",
        "",
        "DEACTIVATE_CARD_GENERIC",
        "DEACTIVATE_LOST_CARD",
        "DEACTIVATE_NEVER_RECEIVED_CARD",
        "DEACTIVATE_STOLEN_CARD",
        "GENERIC_CANCEL_CARD_DEACTIVATED_OKAY",
        "GENERIC_CANCEL_CARD_DEACTIVATED_QUIT",
        "GENERIC_CANCEL_CARD_DEACTIVATED_SCREEN",
        "LOST_CARD_DEACTIVATED_SCREEN",
        "NEVER_RECEIVED_CARD_DEACTIVATED_SCREEN",
        "ORDER_REPLACEMENT_FOR_LOST_CARD",
        "ORDER_REPLACEMENT_FOR_NEVER_RECEIVED_CARD",
        "ORDER_REPLACEMENT_FOR_STOLEN_CARD",
        "REPORT_CARD_NEVER_RECEIVED",
        "REPORT_GENERIC_CANCEL_REASON",
        "REPORT_LOST_CARD",
        "REPORT_STOLEN_CARD",
        "STOLEN_CARD_DEACTIVATED_SCREEN",
        "SWITCH_TO_DEPOSITS_ERROR",
        "SWITCH_TO_REGULAR_DEPOSITS",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CANCEL_CARD_ERROR_SCREEN:Ljava/lang/String; = "Cancel Card: Deactivation Error"

.field private static final DEACTIVATE_CARD_GENERIC:Ljava/lang/String; = "Cancel Card: Generic Cancel Deactivate"

.field private static final DEACTIVATE_LOST_CARD:Ljava/lang/String; = "Cancel Card: Lost Card Deactivate"

.field private static final DEACTIVATE_NEVER_RECEIVED_CARD:Ljava/lang/String; = "Cancel Card: Never Received Card Deactivate"

.field private static final DEACTIVATE_STOLEN_CARD:Ljava/lang/String; = "Cancel Card: Card Stolen Deactivate"

.field private static final GENERIC_CANCEL_CARD_DEACTIVATED_OKAY:Ljava/lang/String; = "Cancel Card: Generic Cancel Card Deactivated Okay"

.field private static final GENERIC_CANCEL_CARD_DEACTIVATED_QUIT:Ljava/lang/String; = "Cancel Card: Generic Cancel Card Deactivated Quit"

.field private static final GENERIC_CANCEL_CARD_DEACTIVATED_SCREEN:Ljava/lang/String; = "Cancel Card: Generic Cancel Card Deactivated"

.field private static final LOST_CARD_DEACTIVATED_SCREEN:Ljava/lang/String; = "Cancel Card: Lost Card Deactivated"

.field private static final NEVER_RECEIVED_CARD_DEACTIVATED_SCREEN:Ljava/lang/String; = "Cancel Card: Never Received Card Deactivated"

.field private static final ORDER_REPLACEMENT_FOR_LOST_CARD:Ljava/lang/String; = "Cancel Card: Lost Card Order Replacement"

.field private static final ORDER_REPLACEMENT_FOR_NEVER_RECEIVED_CARD:Ljava/lang/String; = "Cancel Card: Never Received Card Order Replacement"

.field private static final ORDER_REPLACEMENT_FOR_STOLEN_CARD:Ljava/lang/String; = "Cancel Card: Stolen Card Order Replacement"

.field private static final REPORT_CARD_NEVER_RECEIVED:Ljava/lang/String; = "Cancel Card: Never Received Card"

.field private static final REPORT_GENERIC_CANCEL_REASON:Ljava/lang/String; = "Cancel Card: Generic Cancel"

.field private static final REPORT_LOST_CARD:Ljava/lang/String; = "Cancel Card: Lost Card"

.field private static final REPORT_STOLEN_CARD:Ljava/lang/String; = "Cancel Card: Card Stolen"

.field private static final STOLEN_CARD_DEACTIVATED_SCREEN:Ljava/lang/String; = "Cancel Card: Stolen Card Deactivated"

.field private static final SWITCH_TO_DEPOSITS_ERROR:Ljava/lang/String; = "Cancel Card: Generic Cancel Switch To Regular Deposits Error"

.field private static final SWITCH_TO_REGULAR_DEPOSITS:Ljava/lang/String; = "Cancel Card: Generic Cancel Switch To Regular Deposits"
