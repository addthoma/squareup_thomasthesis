.class public final Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;
.super Ljava/lang/Object;
.source "EnterPhoneNumberCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\u0008\u0001\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J*\u0010\u0005\u001a\u00020\u00062\"\u0010\u0007\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0008\u0012\u0004\u0012\u00020\n`\u000c0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;",
        "",
        "phoneNumberScrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "(Lcom/squareup/text/InsertingScrubber;)V",
        "create",
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;


# direct methods
.method public constructor <init>(Lcom/squareup/text/InsertingScrubber;)V
    .locals 1
    .param p1    # Lcom/squareup/text/InsertingScrubber;
        .annotation runtime Lcom/squareup/text/InsertingScrubber$PhoneNumber;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "phoneNumberScrubber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/InsertingScrubber;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
