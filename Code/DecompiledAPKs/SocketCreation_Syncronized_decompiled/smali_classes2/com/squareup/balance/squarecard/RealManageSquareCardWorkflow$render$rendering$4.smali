.class final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealManageSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/ManageSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "",
        "workflowResult",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "workflowResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$OrderCompleted;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$OrderCompleted;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$refetchCardOrAddPhoneNumber(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 179
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$finishOrRefetchCardStatus(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 180
    :cond_1
    instance-of p1, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$MissingOwnerNameError;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$activateSettingsApplet(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;->invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
