.class public abstract Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen;
.super Ljava/lang/Object;
.source "NotificationPreferencesErrorScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$OnlyMessage;,
        Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$WithRetry;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u0082\u0001\u0002\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "()V",
        "onBack",
        "Lkotlin/Function0;",
        "",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "title",
        "Lcom/squareup/resources/TextModel;",
        "",
        "getTitle",
        "()Lcom/squareup/resources/TextModel;",
        "OnlyMessage",
        "WithRetry",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$OnlyMessage;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$WithRetry;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getOnBack()Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTitle()Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
