.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateSubscriptions(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Triple;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
        "",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lkotlin/Triple;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 80
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;->apply(Lkotlin/Unit;)Lkotlin/Triple;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lkotlin/Unit;)Lkotlin/Triple;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lkotlin/Triple<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    new-instance p1, Lkotlin/Triple;

    .line 265
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$createSignatureUploadPayload(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$createSignatureJson(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$createStampsToRestore(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/List;

    move-result-object v2

    .line 264
    invoke-direct {p1, v0, v1, v2}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method
