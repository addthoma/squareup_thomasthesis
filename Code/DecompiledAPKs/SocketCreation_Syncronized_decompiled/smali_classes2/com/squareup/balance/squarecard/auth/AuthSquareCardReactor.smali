.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;
.super Ljava/lang/Object;
.source "AuthSquareCardReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;,
        Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Reactor<",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthSquareCardReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthSquareCardReactor.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardReactor\n*L\n1#1,409:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u000267B)\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\"\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J$\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00132\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J2\u0010\u001a\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00130\u001b2\u0006\u0010\u0014\u001a\u00020\u00022\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001dH\u0016J\"\u0010\u001e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u001fH\u0002J$\u0010 \u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040!j\u0002`\"2\u0006\u0010#\u001a\u00020\u0010J$\u0010 \u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040!j\u0002`\"2\u0006\u0010$\u001a\u00020%J\u0018\u0010&\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\'\u001a\u00020(H\u0002J\u000c\u0010)\u001a\u00020**\u00020\u0015H\u0002J\u000c\u0010)\u001a\u00020+*\u00020\u001fH\u0002J\u000c\u0010,\u001a\u00020(*\u00020-H\u0002JJ\u0010.\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00130\u0012\"\u0008\u0008\u0000\u0010/*\u000200*\u0008\u0012\u0004\u0012\u0002H/012\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u0002H/\u0012\u0004\u0012\u00020\u001803H\u0002J\u000c\u00104\u001a\u00020\u0018*\u000205H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "accountSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lio/reactivex/Scheduler;Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V",
        "cancelPendingIdv",
        "idvSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "checkIdv",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;",
        "checkIdvStatus",
        "received",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
        "onIdvStatusCheckFailed",
        "onReact",
        "Lrx/Single;",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "startIdv",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;",
        "startWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardWorkflow;",
        "settings",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "tryStartIdv",
        "ssnProvided",
        "",
        "createRequest",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;",
        "isValid",
        "Lcom/squareup/address/Address;",
        "parseIdvStatus",
        "T",
        "",
        "Lcom/squareup/server/AcceptedResponse;",
        "responseParser",
        "Lkotlin/Function1;",
        "toCheckIdvStatusResponse",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;",
        "AuthSquareCardEvent",
        "AuthSquareCardWorkflowState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lio/reactivex/Scheduler;Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bizbankService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-void
.end method

.method public static final synthetic access$cancelPendingIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->cancelPendingIdv(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$checkIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lio/reactivex/Single;
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->checkIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$checkIdvStatus(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->checkIdvStatus(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createRequest(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->createRequest(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getBizbankService$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)Lcom/squareup/balance/core/server/bizbank/BizbankService;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-object p0
.end method

.method public static final synthetic access$onIdvStatusCheckFailed(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->onIdvStatusCheckFailed(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$parseIdvStatus(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/server/AcceptedResponse;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->parseIdvStatus(Lcom/squareup/server/AcceptedResponse;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;)Lio/reactivex/Single;
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->startIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toCheckIdvStatusResponse(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->toCheckIdvStatusResponse(Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$tryStartIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->tryStartIdv(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    move-result-object p0

    return-object p0
.end method

.method private final cancelPendingIdv(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;
    .locals 1

    .line 297
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/ApprovalState;->requiresSsn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    goto :goto_0

    .line 300
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    :goto_0
    return-object v0
.end method

.method private final checkIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;>;"
        }
    .end annotation

    .line 289
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v2, 0x2

    invoke-static {v2, v3, v0, v1}, Lio/reactivex/Single;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 290
    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$checkIdv$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "io.reactivex.Single.time\u2026arser = { it })\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final checkIdvStatus(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    .line 331
    iget-object v0, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    if-eqz v0, :cond_3

    .line 332
    iget-object v0, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iget-object v0, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->poll_token:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 333
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    .line 334
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;

    .line 336
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_error_title:I

    .line 337
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_error_message:I

    .line 334
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;II)V

    .line 333
    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    return-object p2

    .line 341
    :cond_2
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

    iget-object p2, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iget-object p2, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->poll_token:Ljava/lang/String;

    const-string v2, "received.pending.poll_token"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    return-object v0

    .line 344
    :cond_3
    iget-object v0, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    if-eqz v0, :cond_4

    .line 345
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    .line 346
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;

    .line 348
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_error_title:I

    .line 349
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_error_message:I

    .line 346
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;II)V

    .line 345
    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    return-object p2

    .line 354
    :cond_4
    iget-object v0, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    if-eqz v0, :cond_7

    .line 355
    iget-object v0, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iget-object v0, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    sget-object v1, Lcom/squareup/protos/client/bizbank/IdvState;->APPROVED:Lcom/squareup/protos/client/bizbank/IdvState;

    if-ne v0, v1, :cond_5

    .line 357
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Approved;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Approved;

    invoke-direct {p1, p2}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    return-object p1

    .line 360
    :cond_5
    iget-object p2, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iget-object p2, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->can_retry_idv:Ljava/lang/Boolean;

    const-string v0, "received.response.can_retry_idv"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 362
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_2

    .line 365
    :cond_6
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    :goto_2
    return-object p2

    .line 371
    :cond_7
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    return-object p2
.end method

.method private final createRequest(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;
    .locals 1

    .line 393
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest$Builder;-><init>()V

    .line 395
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;->getPollToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest$Builder;->poll_token:Ljava/lang/String;

    .line 397
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;

    move-result-object p1

    const-string v0, "CheckIdentityVerificatio\u2026       }\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createRequest(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;)Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;
    .locals 5

    .line 382
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "accountSettings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 383
    new-instance v1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;-><init>()V

    .line 385
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerAddress()Lcom/squareup/address/Address;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const-string v4, "countryCode"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/squareup/address/Address;->toGlobalAddress(Lcom/squareup/CountryCode;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v3

    :goto_0
    iput-object v0, v1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 386
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->localDateToYmd(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    :cond_1
    iput-object v3, v1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 387
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerSsn()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_social_security_number:Ljava/lang/String;

    .line 389
    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;

    move-result-object p1

    const-string v0, "StartIdentityVerificatio\u2026       }\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final isValid(Lcom/squareup/address/Address;)Z
    .locals 2

    .line 377
    iget-object v0, p1, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 378
    iget-object p1, p1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private final onIdvStatusCheckFailed(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;
    .locals 3

    .line 320
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;

    .line 322
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_error_title:I

    .line 323
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_error_message:I

    .line 320
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;II)V

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    return-object v0
.end method

.method private final parseIdvStatus(Lcom/squareup/server/AcceptedResponse;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/server/AcceptedResponse<",
            "TT;>;",
            "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;>;"
        }
    .end annotation

    .line 308
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 309
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "successOrFailure()\n     \u2026s))\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final startIdv(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;>;"
        }
    .end annotation

    .line 282
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->createRequest(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;)Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->startIdv(Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 283
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$startIdv$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$startIdv$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->parseIdvStatus(Lcom/squareup/server/AcceptedResponse;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final toCheckIdvStatusResponse(Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
    .locals 2

    .line 404
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;-><init>()V

    .line 405
    new-instance v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;-><init>()V

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;->poll_token:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;->poll_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    move-result-object p1

    .line 406
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object p1

    const-string v0, "CheckIdentityVerificatio\u2026build())\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final tryStartIdv(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Z)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;
    .locals 7

    .line 259
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerAddress()Lcom/squareup/address/Address;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->isValid(Lcom/squareup/address/Address;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 260
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    .line 261
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerSsn()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    check-cast v4, Ljava/lang/CharSequence;

    new-instance v5, Lkotlin/text/Regex;

    const-string v6, "[0-9]{9}"

    invoke-direct {v5, v6}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v4

    if-ne v4, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    if-nez v0, :cond_3

    .line 264
    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ADDRESS:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    goto :goto_2

    :cond_3
    if-nez v3, :cond_4

    .line 265
    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->BIRTHDATE:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    if-eqz p2, :cond_5

    .line 266
    sget-object v0, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->SSN:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_6

    .line 269
    new-instance p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;)V

    check-cast p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    return-object p2

    :cond_6
    if-nez p2, :cond_7

    .line 272
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/ApprovalState;->requiresSsn()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 273
    new-instance p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    invoke-direct {p2, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    check-cast p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    goto :goto_3

    .line 275
    :cond_7
    new-instance p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;

    invoke-direct {p2, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    check-cast p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    :goto_3
    return-object p2
.end method


# virtual methods
.method public onAbandoned(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/Reactor$DefaultImpls;->onAbandoned(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onAbandoned(Ljava/lang/Object;)V
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->onAbandoned(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    return-void
.end method

.method public onReact(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
            ">;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$CheckingIdvState;

    if-eqz v0, :cond_3

    .line 169
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object p2

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/ApprovalState;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 173
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 172
    :cond_1
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p2, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 170
    :cond_2
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Approved;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Approved;

    invoke-direct {p1, p2}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    move-object p2, p1

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    .line 174
    :goto_0
    invoke-static {p2}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    const-string/jumbo p2, "when (state.idvSettings.\u2026s))\n        }.let(::just)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 176
    :cond_3
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$2;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_1

    .line 192
    :cond_4
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$3;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_1

    .line 205
    :cond_5
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$4;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_1

    .line 215
    :cond_6
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$5;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_1

    .line 219
    :cond_7
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$6;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$6;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_1

    .line 223
    :cond_8
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$7;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$7;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_1

    .line 226
    :cond_9
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$8;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_1

    .line 237
    :cond_a
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$9;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$9;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_1

    .line 248
    :cond_b
    instance-of p1, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;

    if-eqz p1, :cond_c

    new-instance p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$10;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$onReact$10;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_c
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->onReact(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$CheckingIdvState;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$CheckingIdvState;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
