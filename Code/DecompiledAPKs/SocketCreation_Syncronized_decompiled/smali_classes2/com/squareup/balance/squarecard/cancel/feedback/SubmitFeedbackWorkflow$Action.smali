.class abstract Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action;
.super Ljava/lang/Object;
.source "SubmitFeedbackWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$NoFeedbackSubmitted;,
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithSuccess;,
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithError;,
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;,
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingSucceeded;,
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingFailed;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0006\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "FeedbackSubmittingFailed",
        "FeedbackSubmittingSucceeded",
        "FinishWithError",
        "FinishWithSuccess",
        "NoFeedbackSubmitted",
        "SubmitFeedback",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$NoFeedbackSubmitted;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithSuccess;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithError;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingSucceeded;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingFailed;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
            ">;)",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$NoFeedbackSubmitted;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$NoFeedbackSubmitted;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput$ExitedWithoutFeedback;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput$ExitedWithoutFeedback;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;

    goto :goto_0

    .line 130
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithSuccess;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput$ExitedWithSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput$ExitedWithSuccess;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;

    goto :goto_0

    .line 131
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithError;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FinishWithError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput$ExitedWithFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput$ExitedWithFailure;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;

    goto :goto_0

    .line 132
    :cond_2
    instance-of v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;

    if-eqz v0, :cond_3

    .line 133
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$SubmittingFeedback;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;->getFeedback()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$SubmittingFeedback;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    :cond_3
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingSucceeded;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingSucceeded;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 137
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingSuccess;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 140
    :cond_4
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingFailed;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$FeedbackSubmittingFailed;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 141
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingFailure;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    :goto_0
    return-object v1

    .line 142
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
