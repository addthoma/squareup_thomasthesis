.class final Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "MaybeCancelBizbankWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->render(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankProps;Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
        "+",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        "output",
        "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$1;->this$0:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankState;",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput$OnBackFromConfirmStatus;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput$OnBackFromConfirmStatus;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbort;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$OnAbort;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 130
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput$OnActionFromConfirmStatus;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput$OnActionFromConfirmStatus;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$1;->this$0:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$Action$ConfirmedCurrentBizbankStatus;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow$render$1;->invoke(Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
