.class public final Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;
.super Ljava/lang/Object;
.source "ManageSquareCardState_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;",
            ">;)",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;
    .locals 7

    .line 57
    new-instance v6, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    iget-object v4, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->newInstance(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState_Factory_Factory;->get()Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object v0

    return-object v0
.end method
