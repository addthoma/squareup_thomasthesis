.class final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;
.super Ljava/lang/Object;
.source "SquareCardAddToGooglePayHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a(\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003 \u0004*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lkotlin/Pair;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/Single<",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->access$getGooglePay$p(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)Lcom/squareup/googlepay/GooglePay;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/squareup/googlepay/GooglePay;->activeWalletId(Z)Lio/reactivex/Single;

    move-result-object p1

    .line 48
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;->apply(Ljava/lang/Boolean;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
