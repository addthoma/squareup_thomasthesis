.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureStampButton(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 OrderSquareCardSignatureCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator\n*L\n1#1,1322:1\n295#2,9:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Landroid/widget/ImageButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getMode$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->STAMP:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 1328
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->STAMP:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$setMode$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;)V

    goto :goto_1

    .line 1324
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$LaunchStampsGallery;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$LaunchStampsGallery;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    .line 1326
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1330
    :goto_1
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$notifyState(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    return-void
.end method
