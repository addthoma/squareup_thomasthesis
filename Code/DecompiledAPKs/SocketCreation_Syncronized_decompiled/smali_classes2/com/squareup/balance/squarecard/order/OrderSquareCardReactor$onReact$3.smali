.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;
.super Ljava/lang/Object;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->onReact(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "authorizationResult",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    .line 307
    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Approved;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Approved;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 308
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    .line 309
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->getSkippedInitialScreens()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v2

    .line 308
    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)V

    .line 307
    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 312
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Cancelled;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$Cancelled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;->access$backToDepositsInfoOrFinish(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Z)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    goto :goto_0

    .line 313
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$TryIdvLater;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult$TryIdvLater;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$3;->call(Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
