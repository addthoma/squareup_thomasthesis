.class public final Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;
.super Landroid/view/View;
.source "SquareCardMagStripe.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardMagStripe.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardMagStripe.kt\ncom/squareup/balance/core/views/cardpreview/SquareCardMagStripe\n+ 2 Paints.kt\ncom/squareup/util/Paints\n*L\n1#1,53:1\n7#2,4:54\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardMagStripe.kt\ncom/squareup/balance/core/views/cardpreview/SquareCardMagStripe\n*L\n38#1,4:54\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\u000f\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0007H\u0003J\u0008\u0010\u0011\u001a\u00020\nH\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0016\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0018R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "magStripePaint",
        "Landroid/graphics/Paint;",
        "getMagStripePaint",
        "()Landroid/graphics/Paint;",
        "magStripePaint$delegate",
        "Lkotlin/Lazy;",
        "fromResource",
        "color",
        "magStripePaintFactory",
        "onDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "setEnabled",
        "enabled",
        "",
        "animate",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final magStripePaint$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    new-instance p1, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe$magStripePaint$2;

    move-object p2, p0

    check-cast p2, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;

    invoke-direct {p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe$magStripePaint$2;-><init>(Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->magStripePaint$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 15
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 16
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$magStripePaintFactory(Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;)Landroid/graphics/Paint;
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->magStripePaintFactory()Landroid/graphics/Paint;

    move-result-object p0

    return-object p0
.end method

.method private final fromResource(I)I
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method private final getMagStripePaint()Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->magStripePaint$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method

.method private final magStripePaintFactory()Landroid/graphics/Paint;
    .locals 11

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    .line 55
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 40
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->getHeight()I

    move-result v5

    const/4 v2, 0x3

    new-array v6, v2, [I

    .line 42
    sget v3, Lcom/squareup/balance/core/R$color;->magstripe_start:I

    invoke-direct {p0, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->fromResource(I)I

    move-result v3

    const/4 v7, 0x0

    aput v3, v6, v7

    .line 43
    sget v3, Lcom/squareup/balance/core/R$color;->magstripe_center:I

    invoke-direct {p0, v3}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->fromResource(I)I

    move-result v3

    aput v3, v6, v1

    .line 44
    sget v1, Lcom/squareup/balance/core/R$color;->magstripe_end:I

    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->fromResource(I)I

    move-result v1

    const/4 v3, 0x2

    aput v1, v6, v3

    new-array v7, v2, [F

    .line 46
    fill-array-data v7, :array_0

    const-wide v2, 0x4046800000000000L    # 45.0

    const/4 v8, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    .line 39
    invoke-static/range {v2 .. v10}, Lcom/squareup/util/Gradients;->createLinearGradientFromAngle$default(DII[I[FLandroid/graphics/Shader$TileMode;ILjava/lang/Object;)Landroid/graphics/LinearGradient;

    move-result-object v1

    check-cast v1, Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    return-object v0

    :array_0
    .array-data 4
        0x0
        0x3f3e48e9    # 0.7433f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->getWidth()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->getHeight()I

    move-result v0

    int-to-float v5, v0

    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->getMagStripePaint()Landroid/graphics/Paint;

    move-result-object v6

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 21
    invoke-virtual {p0, p1, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->setEnabled(ZZ)V

    return-void
.end method

.method public final setEnabled(ZZ)V
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->isEnabled()Z

    move-result v0

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_1
    const v0, 0x3ecccccd    # 0.4f

    .line 30
    :goto_0
    invoke-static {p0, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 31
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method
