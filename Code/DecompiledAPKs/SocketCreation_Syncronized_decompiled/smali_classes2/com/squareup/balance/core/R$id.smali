.class public final Lcom/squareup/balance/core/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/core/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final back:I = 0x7f0a01ea

.field public static final bottom_margin_guide:I = 0x7f0a0245

.field public static final card_details_container:I = 0x7f0a02b5

.field public static final card_preview_chip:I = 0x7f0a02c2

.field public static final card_preview_constraint_container:I = 0x7f0a02c3

.field public static final card_preview_logo:I = 0x7f0a02c4

.field public static final card_preview_magstripe:I = 0x7f0a02c5

.field public static final card_preview_square_logo:I = 0x7f0a02c6

.field public static final expiration_and_cvc:I = 0x7f0a0737

.field public static final font_bottom_chip:I = 0x7f0a0767

.field public static final font_bottom_signature:I = 0x7f0a0768

.field public static final font_top_chip:I = 0x7f0a0769

.field public static final font_top_signature:I = 0x7f0a076a

.field public static final front:I = 0x7f0a0778

.field public static final front_bottom_logo:I = 0x7f0a0779

.field public static final front_bottom_margin:I = 0x7f0a077a

.field public static final front_bottom_name:I = 0x7f0a077b

.field public static final front_business_name:I = 0x7f0a077c

.field public static final front_left_chip:I = 0x7f0a077d

.field public static final front_left_name:I = 0x7f0a077e

.field public static final front_right_margin:I = 0x7f0a077f

.field public static final front_top_margin:I = 0x7f0a0780

.field public static final large:I = 0x7f0a0910

.field public static final left_margin_guide:I = 0x7f0a0921

.field public static final logo_guideline_top:I = 0x7f0a0961

.field public static final name_on_card:I = 0x7f0a0a0e

.field public static final pan:I = 0x7f0a0b9a

.field public static final right_margin_guide:I = 0x7f0a0d91

.field public static final signature_on_card:I = 0x7f0a0e92

.field public static final small:I = 0x7f0a0ea8

.field public static final square_card_primary_action_button:I = 0x7f0a0f11

.field public static final square_card_progress_result:I = 0x7f0a0f12

.field public static final square_card_progress_spinner_holder:I = 0x7f0a0f13

.field public static final square_card_progress_spinner_message:I = 0x7f0a0f14

.field public static final square_card_upsell_message:I = 0x7f0a0f15

.field public static final square_card_upsell_preview:I = 0x7f0a0f16

.field public static final square_card_upsell_title:I = 0x7f0a0f17

.field public static final top_margin_guide:I = 0x7f0a1052

.field public static final top_text_guide:I = 0x7f0a1054

.field public static final top_text_guide_smaller:I = 0x7f0a1055


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
