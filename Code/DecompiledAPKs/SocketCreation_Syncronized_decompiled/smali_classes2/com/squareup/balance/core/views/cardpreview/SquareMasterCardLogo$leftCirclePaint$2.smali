.class final synthetic Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$leftCirclePaint$2;
.super Lkotlin/jvm/internal/FunctionReference;
.source "SquareMasterCardLogo.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function0<",
        "Landroid/graphics/Paint;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/graphics/Paint;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "leftCirclePaintFactory"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "leftCirclePaintFactory()Landroid/graphics/Paint;"

    return-object v0
.end method

.method public final invoke()Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$leftCirclePaint$2;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    .line 26
    invoke-static {v0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->access$leftCirclePaintFactory(Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo$leftCirclePaint$2;->invoke()Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method
