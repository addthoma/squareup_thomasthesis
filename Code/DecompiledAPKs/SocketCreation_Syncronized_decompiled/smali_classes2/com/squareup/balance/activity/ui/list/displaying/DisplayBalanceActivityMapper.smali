.class public final Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;
.super Ljava/lang/Object;
.source "DisplayBalanceActivityMapper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayBalanceActivityMapper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayBalanceActivityMapper.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper\n*L\n1#1,141:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ \u0010\u000c\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u000c\u0010\u0015\u001a\u00020\u000e*\u00020\u000bH\u0002J\u000c\u0010\u0016\u001a\u00020\u0017*\u00020\tH\u0003R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;",
        "",
        "balanceActivityMapper",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;",
        "(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)V",
        "mapToUiActivityList",
        "",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
        "tabType",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "result",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "mapToUiBalanceActivity",
        "isPending",
        "",
        "showRunningBalance",
        "activity",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "shouldShowRunningBalance",
        "transactionAmount",
        "Lcom/squareup/balance/activity/ui/common/Amount;",
        "hasAllDataLoaded",
        "toEmptyMessageRow",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "balanceActivityMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    return-void
.end method

.method public static final synthetic access$mapToUiBalanceActivity(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;ZZLcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->mapToUiBalanceActivity(ZZLcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$shouldShowRunningBalance(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;Lcom/squareup/balance/activity/data/BalanceActivityType;)Z
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->shouldShowRunningBalance(Lcom/squareup/balance/activity/data/BalanceActivityType;)Z

    move-result p0

    return p0
.end method

.method private final hasAllDataLoaded(Lcom/squareup/balance/activity/data/BalanceActivity;)Z
    .locals 1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;->hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;->hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final mapToUiBalanceActivity(ZZLcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;
    .locals 6

    if-eqz p2, :cond_0

    .line 113
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-virtual {p2, p3}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->runningBalanceAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v4, p2

    .line 115
    new-instance p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    .line 116
    iget-object v0, p3, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    const-string v1, "activity.description"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    .line 117
    invoke-direct {p0, p3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->transactionAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/common/Amount;

    move-result-object v3

    .line 118
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-virtual {v0, p3, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->subLabel(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/resources/TextModel;

    move-result-object v2

    move-object v0, p2

    move-object v5, p3

    .line 115
    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;-><init>(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    check-cast p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;

    return-object p2
.end method

.method private final shouldShowRunningBalance(Lcom/squareup/balance/activity/data/BalanceActivityType;)Z
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/balance/activity/data/BalanceActivityType;->ALL:Lcom/squareup/balance/activity/data/BalanceActivityType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final toEmptyMessageRow(Lcom/squareup/balance/activity/data/BalanceActivityType;)I
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 104
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_empty_message_transfers:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 103
    :cond_1
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_empty_message_card_spend:I

    goto :goto_0

    .line 102
    :cond_2
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_empty_message_all:I

    :goto_0
    return p1
.end method

.method private final transactionAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/common/Amount;
    .locals 2

    .line 127
    new-instance v0, Lcom/squareup/resources/FixedText;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-virtual {v1, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->transactionAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-virtual {v1, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->shouldCrossOutAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p1, Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/common/Amount;

    goto :goto_0

    .line 131
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    const-string v1, "activity.amount"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/money/MoneyExtensionsKt;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/common/Amount;

    goto :goto_0

    .line 132
    :cond_1
    new-instance p1, Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/common/Amount;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final mapToUiActivityList(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
            ">;"
        }
    .end annotation

    const-string v0, "tabType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 43
    new-instance v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;Ljava/util/List;Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    .line 77
    invoke-virtual {p2}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->invoke(ZLcom/squareup/balance/activity/data/UnifiedActivityResult;)V

    .line 81
    invoke-virtual {p2}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;->hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 82
    invoke-virtual {p2}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->invoke(ZLcom/squareup/balance/activity/data/UnifiedActivityResult;)V

    .line 86
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->toEmptyMessageRow(Lcom/squareup/balance/activity/data/BalanceActivityType;)I

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5, v4, v5}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_1
    sget-object v1, Lcom/squareup/balance/activity/data/BalanceActivityType;->TRANSFERS:Lcom/squareup/balance/activity/data/BalanceActivityType;

    if-ne p1, v1, :cond_2

    invoke-direct {p0, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->hasAllDataLoaded(Lcom/squareup/balance/activity/data/BalanceActivity;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 92
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    new-instance p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    .line 93
    sget v1, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_navigate_to_transfer_reports:I

    .line 94
    sget-object v2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction$OpenTransferReports;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction$OpenTransferReports;

    check-cast v2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;

    .line 92
    invoke-direct {p2, v1, v2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;)V

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method
