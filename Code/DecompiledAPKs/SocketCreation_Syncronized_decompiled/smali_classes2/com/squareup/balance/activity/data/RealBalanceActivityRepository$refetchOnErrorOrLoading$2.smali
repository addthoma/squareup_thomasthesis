.class final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->refetchOnErrorOrLoading(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "balanceActivity",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_refetchOnErrorOrLoading:Lcom/jakewharton/rxrelay2/BehaviorRelay;

.field final synthetic $type:Lcom/squareup/balance/activity/data/BalanceActivityType;

.field final synthetic this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/balance/activity/data/BalanceActivityType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    iput-object p2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->$this_refetchOnErrorOrLoading:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iput-object p3, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->$type:Lcom/squareup/balance/activity/data/BalanceActivityType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    const-string v0, "balanceActivity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    invoke-static {v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->access$maxLatestEventAt(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    .line 167
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v1

    .line 168
    instance-of v2, v1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    if-eqz v2, :cond_0

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "Single.just(pendingResult)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    move-object v7, v1

    goto :goto_2

    .line 169
    :cond_0
    sget-object v2, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 170
    :cond_1
    sget-object v2, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    invoke-static {v1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->access$getRemoteStore$p(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;)Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->$type:Lcom/squareup/balance/activity/data/BalanceActivityType;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v3, v0

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore$DefaultImpls;->fetchPendingBalanceActivity$default(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0

    .line 172
    :goto_2
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object p1

    .line 173
    instance-of v1, p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    if-eqz v1, :cond_2

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(completedResult)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    .line 174
    :cond_2
    sget-object v1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_3

    .line 175
    :cond_3
    sget-object v1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    :goto_3
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    invoke-static {p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->access$getRemoteStore$p(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;)Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->$type:Lcom/squareup/balance/activity/data/BalanceActivityType;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v3, v0

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore$DefaultImpls;->fetchCompletedBalanceActivity$default(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 178
    :goto_4
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->$this_refetchOnErrorOrLoading:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0, v1, v7, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->access$zipAndFetch(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lio/reactivex/Single;Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 175
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 170
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;->apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
