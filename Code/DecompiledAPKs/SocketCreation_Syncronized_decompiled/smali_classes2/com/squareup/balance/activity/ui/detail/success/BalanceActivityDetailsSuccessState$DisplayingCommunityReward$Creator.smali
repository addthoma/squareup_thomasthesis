.class public final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward$Creator;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Creator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    const-string v0, "in"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 0

    new-array p1, p1, [Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;

    return-object p1
.end method
