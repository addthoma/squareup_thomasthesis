.class public final Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealBalanceActivityDetailsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;)Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;-><init>(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;

    invoke-static {v0, v1, v2}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->newInstance(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;)Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow_Factory;->get()Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;

    move-result-object v0

    return-object v0
.end method
