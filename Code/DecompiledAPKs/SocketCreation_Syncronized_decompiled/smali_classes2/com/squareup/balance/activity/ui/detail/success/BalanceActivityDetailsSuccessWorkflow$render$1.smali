.class final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BalanceActivityDetailsSuccessWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->render(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;",
        "details",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$1;->$state:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;
    .locals 3

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;

    .line 77
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivityDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivityDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$1;->$state:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    check-cast v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$UpdatingDetailsCategory;->isPersonal()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 76
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$1;->invoke(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;

    move-result-object p1

    return-object p1
.end method
