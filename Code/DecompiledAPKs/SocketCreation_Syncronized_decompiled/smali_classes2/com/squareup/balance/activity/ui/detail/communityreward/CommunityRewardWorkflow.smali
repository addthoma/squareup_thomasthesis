.class public final Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CommunityRewardWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCommunityRewardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CommunityRewardWorkflow.kt\ncom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,44:1\n179#2,3:45\n199#2,4:48\n*E\n*S KotlinDebug\n*F\n+ 1 CommunityRewardWorkflow.kt\ncom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow\n*L\n40#1,3:45\n40#1,4:48\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0008\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ$\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00022\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00030\u0012H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000c\u001a\u0012\u0012\u0004\u0012\u00020\u000e\u0012\u0008\u0012\u00060\u0003j\u0002`\u00040\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;",
        "",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/Exit;",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/text/Formatter;)V",
        "exit",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final exit:Lcom/squareup/workflow/WorkflowAction;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "browserLauncher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 48
    new-instance p1, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow$$special$$inlined$action$1;

    const-string p2, ""

    invoke-direct {p1, p2}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow$$special$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    .line 47
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->exit:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getExit$p(Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->exit:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;
    .locals 8

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;

    .line 31
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v1, "moneyFormatter.format(props.details.net_amount)"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v1, "moneyFormatter.format(props.details.gross_amount)"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;->getModifier()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v1, "moneyFormatter.format(pr\u2026modifier.modifier_amount)"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;->getModifier()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    .line 35
    new-instance p1, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow$render$1;-><init>(Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 36
    new-instance p1, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow$render$2;-><init>(Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v1, v0

    .line 30
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardWorkflow;->render(Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogScreen;

    move-result-object p1

    return-object p1
.end method
