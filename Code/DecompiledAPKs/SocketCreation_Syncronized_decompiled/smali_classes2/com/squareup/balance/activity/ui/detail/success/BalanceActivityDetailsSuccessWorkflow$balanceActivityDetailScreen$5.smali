.class final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;
.super Lkotlin/jvm/internal/Lambda;
.source "BalanceActivityDetailsSuccessWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->balanceActivityDetailScreen(ZZLcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/workflow/Sink;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "modifier",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;->$details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;->invoke(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V
    .locals 3

    const-string v0, "modifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayCommunityReward;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$balanceActivityDetailScreen$5;->$details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-direct {v1, v2, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayCommunityReward;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
