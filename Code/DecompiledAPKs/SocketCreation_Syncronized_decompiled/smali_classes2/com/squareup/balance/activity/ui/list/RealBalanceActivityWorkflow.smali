.class public final Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealBalanceActivityWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBalanceActivityWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBalanceActivityWorkflow.kt\ncom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,146:1\n32#2,12:147\n149#3,5:159\n*E\n*S KotlinDebug\n*F\n+ 1 RealBalanceActivityWorkflow.kt\ncom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow\n*L\n46#1,12:147\n89#1,5:159\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t0\u0002:\u0001\u001dB1\u0008\u0007\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000b\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000b\u00a2\u0006\u0002\u0010\u0011J\u001f\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016\u00a2\u0006\u0002\u0010\u0016JM\u0010\u0017\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00042\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001aH\u0016\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0004H\u0016R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityScreen;",
        "displayBalanceActivityWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
        "balanceActivityDetailsWorkflow",
        "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
        "transferReportsWorkflow",
        "Lcom/squareup/transferreports/TransferReportsWorkflow;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/balance/activity/ui/list/BalanceActivityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceActivityDetailsWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final displayBalanceActivityWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final transferReportsWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "displayBalanceActivityWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceActivityDetailsWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferReportsWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->displayBalanceActivityWorkflow:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->balanceActivityDetailsWorkflow:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->transferReportsWorkflow:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/list/BalanceActivityState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 147
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 152
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 154
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 155
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 156
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 158
    :cond_3
    check-cast v1, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 46
    :cond_4
    new-instance p1, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;

    sget-object p2, Lcom/squareup/balance/activity/data/BalanceActivityType;->ALL:Lcom/squareup/balance/activity/data/BalanceActivityType;

    invoke-direct {p1, p2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/activity/ui/list/BalanceActivityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/balance/activity/ui/list/BalanceActivityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 55
    instance-of v0, p2, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->displayBalanceActivityWorkflow:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "displayBalanceActivityWorkflow.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 57
    new-instance v4, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;

    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    const/4 v5, 0x0

    .line 58
    new-instance v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$1;

    invoke-direct {v0, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$1;-><init>(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 55
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    .line 78
    new-instance v0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;

    .line 79
    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$1;

    invoke-direct {v2, p2, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$1;-><init>(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 87
    new-instance p2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$2;

    invoke-direct {p2, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 78
    invoke-direct {v0, v1, p3, p2, v2}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 160
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 161
    const-class p2, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 162
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 160
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 90
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 64
    :cond_0
    instance-of p1, p2, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;

    if-eqz p1, :cond_1

    .line 66
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->balanceActivityDetailsWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "balanceActivityDetailsWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 67
    new-instance v3, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;

    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;->getActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    const/4 v4, 0x0

    .line 68
    new-instance p1, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$2;

    invoke-direct {p1, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$2;-><init>(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 65
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    .line 70
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingTransferReports;

    if-eqz p1, :cond_2

    .line 72
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->transferReportsWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "transferReportsWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 73
    sget-object v3, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsSummaryProps;->INSTANCE:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsSummaryProps;

    const/4 v4, 0x0

    .line 74
    new-instance p1, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$3;

    invoke-direct {p1, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$render$childScreen$3;-><init>(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 71
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;->snapshotState(Lcom/squareup/balance/activity/ui/list/BalanceActivityState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
