.class public final Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;
.super Ljava/lang/Object;
.source "RealBalanceActivityMapper.kt"

# interfaces
.implements Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBalanceActivityMapper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBalanceActivityMapper.kt\ncom/squareup/balance/activity/ui/list/RealBalanceActivityMapper\n*L\n1#1,123:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B=\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u0018\u0010\u000c\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J \u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u0012H\u0016J\u0010\u0010\u001a\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "positiveNegativeMoneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "defaultMoneyFormatter",
        "locale",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)V",
        "dateToViewString",
        "Lcom/squareup/resources/TextModel;",
        "",
        "date",
        "",
        "isPositiveAmount",
        "",
        "activity",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "label",
        "runningBalanceAmount",
        "shouldCrossOutAmount",
        "subLabel",
        "isPending",
        "transactionAmount",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final defaultMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final locale:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final positiveNegativeMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)V
    .locals 1
    .param p1    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumFormNoYear;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/PositiveNegativeFormat;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dateFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "positiveNegativeMoneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultMoneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->dateFormatter:Ljava/text/DateFormat;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->positiveNegativeMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->defaultMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->locale:Ljavax/inject/Provider;

    return-void
.end method

.method public static final synthetic access$dateToViewString(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;Ljava/lang/String;)Lcom/squareup/resources/TextModel;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->dateToViewString(Ljava/lang/String;)Lcom/squareup/resources/TextModel;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDateFormatter$p(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)Ljava/text/DateFormat;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->dateFormatter:Ljava/text/DateFormat;

    return-object p0
.end method

.method public static final synthetic access$getLocale$p(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)Ljavax/inject/Provider;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->locale:Ljavax/inject/Provider;

    return-object p0
.end method

.method private final dateToViewString(Ljava/lang/String;)Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 116
    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 119
    :cond_1
    new-instance v0, Lcom/squareup/resources/FixedText;

    invoke-direct {v0, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0
.end method


# virtual methods
.method public isPositiveAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    const-string v0, "activity.amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/money/MoneyExtensionsKt;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method public label(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/String;
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    const-string v0, "activity.description"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public runningBalanceAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->defaultMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "defaultMoneyFormatter.format(activity.balance)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public shouldCrossOutAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const-string v0, "activity.activity_state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/balance/activity/ui/common/MapperHelperKt;->shouldCrossOut(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;)Z

    move-result p1

    return p1
.end method

.method public subLabel(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/resources/TextModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            "Z)",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 45
    :cond_0
    new-instance p2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;-><init>(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 53
    new-instance v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDateWhenNotNull$1;

    invoke-direct {v0, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDateWhenNotNull$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 64
    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 94
    :pswitch_0
    new-instance p1, Lcom/squareup/resources/ResourceCharSequence;

    sget p2, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_sub_label_card_processing:I

    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_1

    .line 91
    :pswitch_1
    new-instance p1, Lcom/squareup/resources/ResourceCharSequence;

    sget p2, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_sub_label_instant_transfer_out:I

    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_1

    .line 87
    :pswitch_2
    new-instance p1, Lcom/squareup/resources/ResourceCharSequence;

    .line 88
    sget p2, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_sub_label_transfer_out:I

    .line 87
    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_1

    .line 84
    :pswitch_3
    new-instance p1, Lcom/squareup/resources/ResourceCharSequence;

    .line 85
    sget p2, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_sub_label_transfer_in:I

    .line 84
    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_1

    .line 79
    :pswitch_4
    sget-object p1, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$2;->INSTANCE:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$2;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_1

    .line 66
    :pswitch_5
    new-instance p2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;-><init>(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_1

    .line 95
    :goto_0
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "formattedDate()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->dateToViewString(Ljava/lang/String;)Lcom/squareup/resources/TextModel;

    move-result-object p1

    :goto_1
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public transactionAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->positiveNegativeMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "positiveNegativeMoneyFor\u2026r.format(activity.amount)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
