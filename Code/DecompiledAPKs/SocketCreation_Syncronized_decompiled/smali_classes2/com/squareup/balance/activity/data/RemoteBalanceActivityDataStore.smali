.class public interface abstract Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;
.super Ljava/lang/Object;
.source "RemoteBalanceActivityDataStore.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH&J(\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
        "",
        "fetchCompletedBalanceActivity",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "type",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "maxLatestEventAt",
        "Lcom/squareup/protos/common/time/DateTime;",
        "batchToken",
        "",
        "fetchPendingBalanceActivity",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract fetchCompletedBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fetchPendingBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;"
        }
    .end annotation
.end method
