.class final Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayBalanceActivityWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->render(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;",
        "result",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;
    .locals 3

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;

    invoke-static {v1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->access$getMapper$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    check-cast v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->mapToUiActivityList(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Ljava/util/List;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;

    invoke-virtual {v1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;-><init>(Ljava/util/List;Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$activityResult$2;->invoke(Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;

    move-result-object p1

    return-object p1
.end method
