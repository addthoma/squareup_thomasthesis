.class public final Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideRegisterGsonFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/google/gson/Gson;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory$InstanceHolder;->access$000()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideRegisterGson()Lcom/google/gson/Gson;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonAppModule;->provideRegisterGson()Lcom/google/gson/Gson;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/google/gson/Gson;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->provideRegisterGson()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->get()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method
