.class public abstract Lcom/squareup/PosReleaseAppComponent$Module;
.super Ljava/lang/Object;
.source "PosReleaseAppComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/balance/activity/service/BalanceActivityServiceReleaseModule;,
        Lcom/squareup/balance/core/server/bizbank/BizbankServiceReleaseModule;,
        Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceReleaseModule;,
        Lcom/squareup/CommonAppModuleProd;,
        Lcom/squareup/locale/CommonLocaleModule;,
        Lcom/squareup/checkoutflow/installments/InstallmentsServiceReleaseModule;,
        Lcom/squareup/server/messages/MessagesServiceReleaseModule;,
        Lcom/squareup/payment/NotifierModule;,
        Lcom/squareup/PosAppComponent$Module;,
        Lcom/squareup/ui/main/PosFeaturesModule$Prod;,
        Lcom/squareup/http/RealDeviceInformationModule;,
        Lcom/squareup/pushmessages/register/RegisterPushNotificationServiceReleaseModule;,
        Lcom/squareup/time/TimeReleaseModule;,
        Lcom/squareup/balance/core/server/transfers/TransfersServiceReleaseModule;,
        Lcom/squareup/checkoutflow/receipt/ReceiptServiceReleaseModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/PosReleaseAppComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
