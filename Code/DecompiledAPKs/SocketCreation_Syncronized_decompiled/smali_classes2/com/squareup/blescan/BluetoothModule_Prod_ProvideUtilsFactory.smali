.class public final Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;
.super Ljava/lang/Object;
.source "BluetoothModule_Prod_ProvideUtilsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/BluetoothUtils;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->contextProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->bluetoothManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;)",
            "Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUtils(Landroid/app/Application;Landroid/bluetooth/BluetoothManager;)Lcom/squareup/cardreader/BluetoothUtils;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/blescan/BluetoothModule$Prod;->provideUtils(Landroid/app/Application;Landroid/bluetooth/BluetoothManager;)Lcom/squareup/cardreader/BluetoothUtils;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/BluetoothUtils;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/BluetoothUtils;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->bluetoothManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    invoke-static {v0, v1}, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->provideUtils(Landroid/app/Application;Landroid/bluetooth/BluetoothManager;)Lcom/squareup/cardreader/BluetoothUtils;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->get()Lcom/squareup/cardreader/BluetoothUtils;

    move-result-object v0

    return-object v0
.end method
