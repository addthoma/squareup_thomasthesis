.class final Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/rpc/Error;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 283
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/rpc/Error;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/rpc/Error;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    new-instance v0, Lcom/squareup/api/rpc/Error$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Error$Builder;-><init>()V

    .line 311
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 312
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 342
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 340
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Error$Builder;->invalid_field_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Error$Builder;

    goto :goto_0

    .line 339
    :pswitch_1
    sget-object v3, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/rpc/Error$Builder;

    goto :goto_0

    .line 333
    :pswitch_2
    :try_start_0
    sget-object v4, Lcom/squareup/api/sync/FieldValidationErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/sync/FieldValidationErrorCode;

    invoke-virtual {v0, v4}, Lcom/squareup/api/rpc/Error$Builder;->field_validation_error_code(Lcom/squareup/api/sync/FieldValidationErrorCode;)Lcom/squareup/api/rpc/Error$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 335
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/rpc/Error$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 325
    :pswitch_3
    :try_start_1
    sget-object v4, Lcom/squareup/api/sync/SyncErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/sync/SyncErrorCode;

    invoke-virtual {v0, v4}, Lcom/squareup/api/rpc/Error$Builder;->sync_error_code(Lcom/squareup/api/sync/SyncErrorCode;)Lcom/squareup/api/rpc/Error$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 327
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/rpc/Error$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 317
    :cond_0
    :try_start_2
    sget-object v4, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {v0, v4}, Lcom/squareup/api/rpc/Error$Builder;->server_error_code(Lcom/squareup/api/rpc/Error$ServerErrorCode;)Lcom/squareup/api/rpc/Error$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 319
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/rpc/Error$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 314
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Error$Builder;->description(Ljava/lang/String;)Lcom/squareup/api/rpc/Error$Builder;

    goto/16 :goto_0

    .line 346
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/rpc/Error$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 347
    invoke-virtual {v0}, Lcom/squareup/api/rpc/Error$Builder;->build()Lcom/squareup/api/rpc/Error;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x3fc
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/rpc/Error;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/rpc/Error;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 299
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 300
    sget-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 301
    sget-object v0, Lcom/squareup/api/sync/SyncErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    const/16 v2, 0x3fc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 302
    sget-object v0, Lcom/squareup/api/sync/FieldValidationErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    const/16 v2, 0x3fd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 303
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    const/16 v2, 0x3fe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    const/16 v2, 0x3ff

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    invoke-virtual {p2}, Lcom/squareup/api/rpc/Error;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    check-cast p2, Lcom/squareup/api/rpc/Error;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/rpc/Error;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/rpc/Error;)I
    .locals 4

    .line 288
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const/4 v3, 0x2

    .line 289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    const/16 v3, 0x3fc

    .line 290
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/FieldValidationErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    const/16 v3, 0x3fd

    .line 291
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    const/16 v3, 0x3fe

    .line 292
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    const/16 v3, 0x3ff

    .line 293
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Error;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/api/rpc/Error;

    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;->encodedSize(Lcom/squareup/api/rpc/Error;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/rpc/Error;)Lcom/squareup/api/rpc/Error;
    .locals 2

    .line 352
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Error;->newBuilder()Lcom/squareup/api/rpc/Error$Builder;

    move-result-object p1

    .line 353
    iget-object v0, p1, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    iput-object v0, p1, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    .line 354
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Error$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 355
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Error$Builder;->build()Lcom/squareup/api/rpc/Error;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/api/rpc/Error;

    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;->redact(Lcom/squareup/api/rpc/Error;)Lcom/squareup/api/rpc/Error;

    move-result-object p1

    return-object p1
.end method
