.class public final Lcom/squareup/api/ApiTransactionState_Factory;
.super Ljava/lang/Object;
.source "ApiTransactionState_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ApiTransactionState;",
        ">;"
    }
.end annotation


# instance fields
.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionState_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/api/ApiTransactionState_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/api/ApiTransactionState_Factory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/api/ApiTransactionState_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/api/ApiTransactionState_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Z)Lcom/squareup/api/ApiTransactionState;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/api/ApiTransactionState;

    invoke-direct {v0, p0}, Lcom/squareup/api/ApiTransactionState;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/api/ApiTransactionState;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState_Factory;->newInstance(Z)Lcom/squareup/api/ApiTransactionState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/api/ApiTransactionState_Factory;->get()Lcom/squareup/api/ApiTransactionState;

    move-result-object v0

    return-object v0
.end method
