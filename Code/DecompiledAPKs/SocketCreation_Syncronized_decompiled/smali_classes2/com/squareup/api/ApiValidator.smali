.class public Lcom/squareup/api/ApiValidator;
.super Ljava/lang/Object;
.source "ApiValidator.java"


# static fields
.field private static final MAX_TIMEOUT:J = 0x2710L

.field private static final MIN_AMOUNT:I = 0x0

.field private static final MIN_TIMEOUT:J = 0xc80L


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final appDelegate:Lcom/squareup/AppDelegate;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final clientSettingsValidator:Lcom/squareup/api/ClientSettingsValidator;

.field private downloadCustomer:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Lcom/squareup/api/RequestParams;",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/RequestParams;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final isReaderSdk:Z

.field private final resources:Landroid/content/res/Resources;

.field private validation:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/RequestParams;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation
.end field

.field private final validationFirstStep:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lcom/squareup/api/RequestParams;",
            ">;"
        }
    .end annotation
.end field

.field private final validationLastStep:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lcom/squareup/api/RequestParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/AppDelegate;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Landroid/content/res/Resources;ZLcom/squareup/api/ClientSettingsValidator;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    sget-object v0, Lcom/squareup/api/-$$Lambda$ApiValidator$y1afkaVuOzvA00Jr7zPcUnBGShU;->INSTANCE:Lcom/squareup/api/-$$Lambda$ApiValidator$y1afkaVuOzvA00Jr7zPcUnBGShU;

    iput-object v0, p0, Lcom/squareup/api/ApiValidator;->validationFirstStep:Lrx/functions/Action1;

    .line 123
    new-instance v0, Lcom/squareup/api/ApiValidator$1;

    invoke-direct {v0, p0}, Lcom/squareup/api/ApiValidator$1;-><init>(Lcom/squareup/api/ApiValidator;)V

    iput-object v0, p0, Lcom/squareup/api/ApiValidator;->validationLastStep:Lrx/functions/Action1;

    .line 218
    new-instance v0, Lcom/squareup/api/ApiValidator$2;

    invoke-direct {v0, p0}, Lcom/squareup/api/ApiValidator$2;-><init>(Lcom/squareup/api/ApiValidator;)V

    iput-object v0, p0, Lcom/squareup/api/ApiValidator;->downloadCustomer:Lrx/functions/Func1;

    .line 303
    iput-object p1, p0, Lcom/squareup/api/ApiValidator;->appDelegate:Lcom/squareup/AppDelegate;

    .line 304
    iput-object p2, p0, Lcom/squareup/api/ApiValidator;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    .line 305
    iput-object p3, p0, Lcom/squareup/api/ApiValidator;->features:Lcom/squareup/settings/server/Features;

    .line 306
    iput-object p4, p0, Lcom/squareup/api/ApiValidator;->resources:Landroid/content/res/Resources;

    .line 307
    iput-boolean p5, p0, Lcom/squareup/api/ApiValidator;->isReaderSdk:Z

    .line 308
    iput-object p6, p0, Lcom/squareup/api/ApiValidator;->clientSettingsValidator:Lcom/squareup/api/ClientSettingsValidator;

    .line 309
    iput-object p7, p0, Lcom/squareup/api/ApiValidator;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ApiValidator;)Lcom/squareup/account/LegacyAuthenticator;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/api/ApiValidator;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/api/ApiValidator;)Lcom/squareup/AppDelegate;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/api/ApiValidator;->appDelegate:Lcom/squareup/AppDelegate;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/api/ApiValidator;)Z
    .locals 0

    .line 76
    iget-boolean p0, p0, Lcom/squareup/api/ApiValidator;->isReaderSdk:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/api/ApiValidator;)Landroid/content/res/Resources;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/api/ApiValidator;->resources:Landroid/content/res/Resources;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/api/ApiValidator;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/api/ApiValidator;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/api/ApiValidator;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/api/ApiValidator;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method static synthetic lambda$createValidation$1(Lcom/squareup/api/RequestParams;)Lcom/squareup/api/ClientInfo;
    .locals 0

    .line 337
    iget-object p0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    return-object p0
.end method

.method static synthetic lambda$createValidation$2(Lcom/squareup/api/RequestParams;Lkotlin/Pair;)Lcom/squareup/api/RequestParams;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/api/RequestParams;)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->NO_VERSION:Lcom/squareup/api/ApiVersion;

    if-eq v0, v1, :cond_9

    .line 98
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->UNSUPPORTED_VERSION:Lcom/squareup/api/ApiVersion;

    if-eq v0, v1, :cond_8

    .line 102
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->INVALID_VERSION:Lcom/squareup/api/ApiVersion;

    if-eq v0, v1, :cond_7

    .line 106
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 110
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_3:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 111
    :cond_0
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_WEB_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 114
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v0, v0, Lcom/squareup/api/ClientInfo;->packageName:Ljava/lang/String;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 115
    :cond_2
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_START_METHOD:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 118
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object p0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object p0, p0, Lcom/squareup/api/ClientInfo;->webCallbackUri:Ljava/lang/String;

    if-eqz p0, :cond_4

    goto :goto_2

    .line 119
    :cond_4
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    :cond_5
    :goto_2
    return-void

    .line 107
    :cond_6
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 103
    :cond_7
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 99
    :cond_8
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 95
    :cond_9
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0
.end method


# virtual methods
.method createValidation(Lcom/squareup/api/RequestParams;)V
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/squareup/api/ApiValidator;->validation:Lrx/Observable;

    if-nez v0, :cond_1

    .line 333
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ApiValidator;->validationFirstStep:Lrx/functions/Action1;

    .line 334
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 335
    iget-boolean v1, p0, Lcom/squareup/api/ApiValidator;->isReaderSdk:Z

    if-nez v1, :cond_0

    .line 336
    sget-object v1, Lcom/squareup/api/-$$Lambda$ApiValidator$BIBet15-N5FF0qw_jGWfb3M6Aow;->INSTANCE:Lcom/squareup/api/-$$Lambda$ApiValidator$BIBet15-N5FF0qw_jGWfb3M6Aow;

    .line 337
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ApiValidator;->clientSettingsValidator:Lcom/squareup/api/ClientSettingsValidator;

    .line 338
    invoke-virtual {v1}, Lcom/squareup/api/ClientSettingsValidator;->validateClientSettings()Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/api/-$$Lambda$ApiValidator$VhhwbZO-onWsEhZXd6TYIqerdqE;

    invoke-direct {v1, p1}, Lcom/squareup/api/-$$Lambda$ApiValidator$VhhwbZO-onWsEhZXd6TYIqerdqE;-><init>(Lcom/squareup/api/RequestParams;)V

    .line 339
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 341
    :cond_0
    iget-object p1, p0, Lcom/squareup/api/ApiValidator;->validationLastStep:Lrx/functions/Action1;

    .line 342
    invoke-virtual {v0, p1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/api/ApiValidator;->downloadCustomer:Lrx/functions/Func1;

    .line 343
    invoke-virtual {p1, v0}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 344
    invoke-virtual {p1}, Lrx/Observable;->cache()Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/ApiValidator;->validation:Lrx/Observable;

    return-void

    .line 331
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Validation already created"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method hasValidation()Z
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/api/ApiValidator;->validation:Lrx/Observable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action1<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/RequestParams;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;",
            "Lrx/functions/Action1<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lrx/Subscription;"
        }
    .end annotation

    .line 320
    iget-object v0, p0, Lcom/squareup/api/ApiValidator;->validation:Lrx/Observable;

    invoke-virtual {v0, p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method
