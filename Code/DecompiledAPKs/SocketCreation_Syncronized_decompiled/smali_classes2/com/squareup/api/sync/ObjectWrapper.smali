.class public final Lcom/squareup/api/sync/ObjectWrapper;
.super Lcom/squareup/wire/Message;
.source "ObjectWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;,
        Lcom/squareup/api/sync/ObjectWrapper$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/ObjectWrapper;",
        "Lcom/squareup/api/sync/ObjectWrapper$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DELETED:Ljava/lang/Boolean;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_OBJECT_TYPE:Lcom/squareup/api/items/Type;

.field public static final DEFAULT_TIMESTAMP:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.AdditionalItemImage#ADAPTER"
        tag = 0x3f8
    .end annotation
.end field

.field public final business:Lcom/squareup/protos/agenda/Business;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.Business#ADAPTER"
        tag = 0x771
    .end annotation
.end field

.field public final configuration:Lcom/squareup/api/items/Configuration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Configuration#ADAPTER"
        tag = 0x3fe
    .end annotation
.end field

.field public final deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final dining_option:Lcom/squareup/api/items/DiningOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.DiningOption#ADAPTER"
        tag = 0x3fc
    .end annotation
.end field

.field public final discount:Lcom/squareup/api/items/Discount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount#ADAPTER"
        tag = 0x3f2
    .end annotation
.end field

.field public final favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.FavoritesListPosition#ADAPTER"
        tag = 0x406
    .end annotation
.end field

.field public final fee:Lcom/squareup/api/items/Fee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Fee#ADAPTER"
        tag = 0x3f0
    .end annotation
.end field

.field public final floor_plan:Lcom/squareup/api/items/FloorPlan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.FloorPlan#ADAPTER"
        tag = 0x404
    .end annotation
.end field

.field public final floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.FloorPlanTile#ADAPTER"
        tag = 0x405
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final inventory_info:Lcom/squareup/api/items/InventoryInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.InventoryInfo#ADAPTER"
        tag = 0x3fa
    .end annotation
.end field

.field public final item:Lcom/squareup/api/items/Item;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Item#ADAPTER"
        tag = 0x3e9
    .end annotation
.end field

.field public final item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemFeeMembership#ADAPTER"
        tag = 0x3f3
    .end annotation
.end field

.field public final item_image:Lcom/squareup/api/items/ItemImage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemImage#ADAPTER"
        tag = 0x3ea
    .end annotation
.end field

.field public final item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemItemModifierListMembership#ADAPTER"
        tag = 0x3f5
    .end annotation
.end field

.field public final item_modifier_list:Lcom/squareup/api/items/ItemModifierList;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemModifierList#ADAPTER"
        tag = 0x3f4
    .end annotation
.end field

.field public final item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemModifierOption#ADAPTER"
        tag = 0x3f6
    .end annotation
.end field

.field public final item_page_membership:Lcom/squareup/api/items/ItemPageMembership;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemPageMembership#ADAPTER"
        tag = 0x3ec
    .end annotation
.end field

.field public final item_variation:Lcom/squareup/api/items/ItemVariation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemVariation#ADAPTER"
        tag = 0x3ef
    .end annotation
.end field

.field public final market_item_settings:Lcom/squareup/api/items/MarketItemSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MarketItemSettings#ADAPTER"
        tag = 0x3f7
    .end annotation
.end field

.field public final menu:Lcom/squareup/api/items/Menu;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Menu#ADAPTER"
        tag = 0x402
    .end annotation
.end field

.field public final menu_category:Lcom/squareup/api/items/MenuCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MenuCategory#ADAPTER"
        tag = 0x3ed
    .end annotation
.end field

.field public final menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MenuGroupMembership#ADAPTER"
        tag = 0x407
    .end annotation
.end field

.field public final object_id:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final object_type:Lcom/squareup/api/items/Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Type#ADAPTER"
        tag = 0x65
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.OBSOLETE_TenderFee#ADAPTER"
        tag = 0x3fb
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final page:Lcom/squareup/api/items/Page;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Page#ADAPTER"
        tag = 0x3eb
    .end annotation
.end field

.field public final placeholder:Lcom/squareup/api/items/Placeholder;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Placeholder#ADAPTER"
        tag = 0x3f1
    .end annotation
.end field

.field public final pricing_rule:Lcom/squareup/api/items/PricingRule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingRule#ADAPTER"
        tag = 0x409
    .end annotation
.end field

.field public final product_set:Lcom/squareup/api/items/ProductSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ProductSet#ADAPTER"
        tag = 0x40a
    .end annotation
.end field

.field public final promo:Lcom/squareup/api/items/Promo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Promo#ADAPTER"
        tag = 0x3f9
    .end annotation
.end field

.field public final surcharge:Lcom/squareup/api/items/Surcharge;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Surcharge#ADAPTER"
        tag = 0x408
    .end annotation
.end field

.field public final surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.SurchargeFeeMembership#ADAPTER"
        tag = 0x40c
    .end annotation
.end field

.field public final tag:Lcom/squareup/api/items/Tag;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Tag#ADAPTER"
        tag = 0x403
    .end annotation
.end field

.field public final tax_rule:Lcom/squareup/api/items/TaxRule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TaxRule#ADAPTER"
        tag = 0x3fd
    .end annotation
.end field

.field public final ticket_group:Lcom/squareup/api/items/TicketGroup;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TicketGroup#ADAPTER"
        tag = 0x3ff
    .end annotation
.end field

.field public final ticket_template:Lcom/squareup/api/items/TicketTemplate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TicketTemplate#ADAPTER"
        tag = 0x400
    .end annotation
.end field

.field public final time_period:Lcom/squareup/api/items/TimePeriod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TimePeriod#ADAPTER"
        tag = 0x40b
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final void_reason:Lcom/squareup/api/items/VoidReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.VoidReason#ADAPTER"
        tag = 0x401
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 60
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 66
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/ObjectWrapper;->DEFAULT_TIMESTAMP:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 68
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/ObjectWrapper;->DEFAULT_DELETED:Ljava/lang/Boolean;

    .line 70
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    sput-object v0, Lcom/squareup/api/sync/ObjectWrapper;->DEFAULT_OBJECT_TYPE:Lcom/squareup/api/items/Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lokio/ByteString;)V
    .locals 1

    .line 442
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 443
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    .line 444
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 445
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->timestamp:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    .line 446
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->deleted:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    .line 447
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business:Lcom/squareup/protos/agenda/Business;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    .line 448
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_type:Lcom/squareup/api/items/Type;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    .line 449
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    .line 450
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 451
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page:Lcom/squareup/api/items/Page;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    .line 452
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    .line 453
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    .line 454
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    .line 455
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    .line 456
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder:Lcom/squareup/api/items/Placeholder;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    .line 457
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount:Lcom/squareup/api/items/Discount;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    .line 458
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    .line 459
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 460
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 461
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 462
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    .line 463
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    .line 464
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo:Lcom/squareup/api/items/Promo;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    .line 465
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    .line 466
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    .line 467
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    .line 468
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    .line 469
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration:Lcom/squareup/api/items/Configuration;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    .line 470
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 471
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    .line 472
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason:Lcom/squareup/api/items/VoidReason;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    .line 473
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu:Lcom/squareup/api/items/Menu;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    .line 474
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag:Lcom/squareup/api/items/Tag;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    .line 475
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    .line 476
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    .line 477
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    .line 478
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    .line 479
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 480
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    .line 481
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    .line 482
    iget-object p2, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    iput-object p2, p0, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    .line 483
    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 537
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/ObjectWrapper;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 538
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/ObjectWrapper;

    .line 539
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    .line 540
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 541
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    .line 542
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    .line 543
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    .line 544
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    .line 545
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    .line 546
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 547
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    .line 548
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    .line 549
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    .line 550
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    .line 551
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    .line 552
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    .line 553
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    .line 554
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    .line 555
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 556
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 557
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 558
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    .line 559
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    .line 560
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    .line 561
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    .line 562
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    .line 563
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    .line 564
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    .line 565
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    .line 566
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 567
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    .line 568
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    .line 569
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    .line 570
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    .line 571
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    .line 572
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    .line 573
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    .line 574
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    .line 575
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 576
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    .line 577
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    .line 578
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    .line 579
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    .line 580
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 585
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_29

    .line 587
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 588
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 589
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 590
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 591
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 592
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/Business;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 593
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 594
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/items/Item;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 595
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemImage;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 596
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/api/items/Page;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 597
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemPageMembership;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 598
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuCategory;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 599
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemVariation;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 600
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 601
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/api/items/Placeholder;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 602
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 603
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemFeeMembership;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 604
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierList;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 605
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemItemModifierListMembership;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 606
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierOption;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 607
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/api/items/MarketItemSettings;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 608
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/api/items/AdditionalItemImage;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 609
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/api/items/Promo;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 610
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/api/items/InventoryInfo;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 611
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/api/items/OBSOLETE_TenderFee;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 612
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/api/items/DiningOption;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 613
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/api/items/TaxRule;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 614
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/api/items/Configuration;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 615
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Lcom/squareup/api/items/TicketGroup;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 616
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/api/items/TicketTemplate;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 617
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/api/items/VoidReason;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 618
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/api/items/Menu;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 619
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Lcom/squareup/api/items/Tag;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 620
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Lcom/squareup/api/items/FloorPlan;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 621
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Lcom/squareup/api/items/FloorPlanTile;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 622
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Lcom/squareup/api/items/FavoritesListPosition;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 623
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuGroupMembership;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 624
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/squareup/api/items/Surcharge;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 625
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingRule;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 626
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Lcom/squareup/api/items/ProductSet;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 627
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Lcom/squareup/api/items/TimePeriod;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 628
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Lcom/squareup/api/items/SurchargeFeeMembership;->hashCode()I

    move-result v2

    :cond_28
    add-int/2addr v0, v2

    .line 629
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_29
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 2

    .line 488
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;-><init>()V

    .line 489
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->id:Ljava/lang/String;

    .line 490
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 491
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->timestamp:Ljava/lang/Long;

    .line 492
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->deleted:Ljava/lang/Boolean;

    .line 493
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business:Lcom/squareup/protos/agenda/Business;

    .line 494
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_type:Lcom/squareup/api/items/Type;

    .line 495
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    .line 496
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 497
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page:Lcom/squareup/api/items/Page;

    .line 498
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    .line 499
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    .line 500
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    .line 501
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    .line 502
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder:Lcom/squareup/api/items/Placeholder;

    .line 503
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount:Lcom/squareup/api/items/Discount;

    .line 504
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    .line 505
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 506
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 507
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 508
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    .line 509
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    .line 510
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo:Lcom/squareup/api/items/Promo;

    .line 511
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    .line 512
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    .line 513
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    .line 514
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    .line 515
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration:Lcom/squareup/api/items/Configuration;

    .line 516
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 517
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    .line 518
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason:Lcom/squareup/api/items/VoidReason;

    .line 519
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu:Lcom/squareup/api/items/Menu;

    .line 520
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag:Lcom/squareup/api/items/Tag;

    .line 521
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    .line 522
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    .line 523
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    .line 524
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    .line 525
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 526
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    .line 527
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    .line 528
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    .line 529
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    .line 530
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 636
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 637
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    const-string v1, ", object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 639
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 640
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 641
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    if-eqz v1, :cond_4

    const-string v1, ", business="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 642
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    if-eqz v1, :cond_5

    const-string v1, ", object_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 643
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    if-eqz v1, :cond_6

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 644
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v1, :cond_7

    const-string v1, ", item_image="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 645
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    if-eqz v1, :cond_8

    const-string v1, ", page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 646
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    if-eqz v1, :cond_9

    const-string v1, ", item_page_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 647
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v1, :cond_a

    const-string v1, ", menu_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 648
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v1, :cond_b

    const-string v1, ", item_variation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 649
    :cond_b
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    if-eqz v1, :cond_c

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 650
    :cond_c
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    if-eqz v1, :cond_d

    const-string v1, ", placeholder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 651
    :cond_d
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v1, :cond_e

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 652
    :cond_e
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    if-eqz v1, :cond_f

    const-string v1, ", item_fee_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 653
    :cond_f
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v1, :cond_10

    const-string v1, ", item_modifier_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 654
    :cond_10
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    if-eqz v1, :cond_11

    const-string v1, ", item_item_modifier_list_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 655
    :cond_11
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v1, :cond_12

    const-string v1, ", item_modifier_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 656
    :cond_12
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    if-eqz v1, :cond_13

    const-string v1, ", market_item_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 657
    :cond_13
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    if-eqz v1, :cond_14

    const-string v1, ", additional_item_image="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 658
    :cond_14
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    if-eqz v1, :cond_15

    const-string v1, ", promo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 659
    :cond_15
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    if-eqz v1, :cond_16

    const-string v1, ", inventory_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 660
    :cond_16
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    if-eqz v1, :cond_17

    const-string v1, ", obsolete_TenderFee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 661
    :cond_17
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    if-eqz v1, :cond_18

    const-string v1, ", dining_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 662
    :cond_18
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    if-eqz v1, :cond_19

    const-string v1, ", tax_rule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 663
    :cond_19
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    if-eqz v1, :cond_1a

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 664
    :cond_1a
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v1, :cond_1b

    const-string v1, ", ticket_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 665
    :cond_1b
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v1, :cond_1c

    const-string v1, ", ticket_template="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 666
    :cond_1c
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    if-eqz v1, :cond_1d

    const-string v1, ", void_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 667
    :cond_1d
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    if-eqz v1, :cond_1e

    const-string v1, ", menu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 668
    :cond_1e
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    if-eqz v1, :cond_1f

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 669
    :cond_1f
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    if-eqz v1, :cond_20

    const-string v1, ", floor_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 670
    :cond_20
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    if-eqz v1, :cond_21

    const-string v1, ", floor_plan_tile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 671
    :cond_21
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    if-eqz v1, :cond_22

    const-string v1, ", favorites_list_position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 672
    :cond_22
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    if-eqz v1, :cond_23

    const-string v1, ", menu_group_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 673
    :cond_23
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    if-eqz v1, :cond_24

    const-string v1, ", surcharge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 674
    :cond_24
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    if-eqz v1, :cond_25

    const-string v1, ", pricing_rule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 675
    :cond_25
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    if-eqz v1, :cond_26

    const-string v1, ", product_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 676
    :cond_26
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    if-eqz v1, :cond_27

    const-string v1, ", time_period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 677
    :cond_27
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    if-eqz v1, :cond_28

    const-string v1, ", surcharge_fee_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_28
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ObjectWrapper{"

    .line 678
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
