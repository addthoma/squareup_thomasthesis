.class public final Lcom/squareup/api/sync/CreateSessionResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateSessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/CreateSessionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/CreateSessionResponse;",
        "Lcom/squareup/api/sync/CreateSessionResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public session_id:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/CreateSessionResponse;
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/api/sync/CreateSessionResponse;

    iget-object v1, p0, Lcom/squareup/api/sync/CreateSessionResponse$Builder;->session_id:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/api/sync/CreateSessionResponse;-><init>(Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/api/sync/CreateSessionResponse$Builder;->build()Lcom/squareup/api/sync/CreateSessionResponse;

    move-result-object v0

    return-object v0
.end method

.method public session_id(Ljava/lang/Long;)Lcom/squareup/api/sync/CreateSessionResponse$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/api/sync/CreateSessionResponse$Builder;->session_id:Ljava/lang/Long;

    return-object p0
.end method
