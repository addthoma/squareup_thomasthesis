.class public final Lcom/squareup/api/sync/ObjectWrapper$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ObjectWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ObjectWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/ObjectWrapper;",
        "Lcom/squareup/api/sync/ObjectWrapper$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

.field public business:Lcom/squareup/protos/agenda/Business;

.field public configuration:Lcom/squareup/api/items/Configuration;

.field public deleted:Ljava/lang/Boolean;

.field public dining_option:Lcom/squareup/api/items/DiningOption;

.field public discount:Lcom/squareup/api/items/Discount;

.field public favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

.field public fee:Lcom/squareup/api/items/Fee;

.field public floor_plan:Lcom/squareup/api/items/FloorPlan;

.field public floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

.field public id:Ljava/lang/String;

.field public inventory_info:Lcom/squareup/api/items/InventoryInfo;

.field public item:Lcom/squareup/api/items/Item;

.field public item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

.field public item_image:Lcom/squareup/api/items/ItemImage;

.field public item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

.field public item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

.field public item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

.field public item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

.field public item_variation:Lcom/squareup/api/items/ItemVariation;

.field public market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

.field public menu:Lcom/squareup/api/items/Menu;

.field public menu_category:Lcom/squareup/api/items/MenuCategory;

.field public menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

.field public object_id:Lcom/squareup/api/sync/ObjectId;

.field public object_type:Lcom/squareup/api/items/Type;

.field public obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

.field public page:Lcom/squareup/api/items/Page;

.field public placeholder:Lcom/squareup/api/items/Placeholder;

.field public pricing_rule:Lcom/squareup/api/items/PricingRule;

.field public product_set:Lcom/squareup/api/items/ProductSet;

.field public promo:Lcom/squareup/api/items/Promo;

.field public surcharge:Lcom/squareup/api/items/Surcharge;

.field public surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

.field public tag:Lcom/squareup/api/items/Tag;

.field public tax_rule:Lcom/squareup/api/items/TaxRule;

.field public ticket_group:Lcom/squareup/api/items/TicketGroup;

.field public ticket_template:Lcom/squareup/api/items/TicketTemplate;

.field public time_period:Lcom/squareup/api/items/TimePeriod;

.field public timestamp:Ljava/lang/Long;

.field public void_reason:Lcom/squareup/api/items/VoidReason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 764
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public additional_item_image(Lcom/squareup/api/items/AdditionalItemImage;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 880
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    return-object p0
.end method

.method public build()Lcom/squareup/api/sync/ObjectWrapper;
    .locals 2

    .line 987
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/sync/ObjectWrapper;-><init>(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 681
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public business(Lcom/squareup/protos/agenda/Business;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 798
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business:Lcom/squareup/protos/agenda/Business;

    return-object p0
.end method

.method public configuration(Lcom/squareup/api/items/Configuration;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 911
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration:Lcom/squareup/api/items/Configuration;

    return-object p0
.end method

.method public deleted(Ljava/lang/Boolean;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 793
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->deleted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public dining_option(Lcom/squareup/api/items/DiningOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 901
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    return-object p0
.end method

.method public discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 849
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount:Lcom/squareup/api/items/Discount;

    return-object p0
.end method

.method public favorites_list_position(Lcom/squareup/api/items/FavoritesListPosition;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 951
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    return-object p0
.end method

.method public fee(Lcom/squareup/api/items/Fee;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 839
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    return-object p0
.end method

.method public floor_plan(Lcom/squareup/api/items/FloorPlan;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 941
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    return-object p0
.end method

.method public floor_plan_tile(Lcom/squareup/api/items/FloorPlanTile;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 946
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 772
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public inventory_info(Lcom/squareup/api/items/InventoryInfo;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 890
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    return-object p0
.end method

.method public item(Lcom/squareup/api/items/Item;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 809
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    return-object p0
.end method

.method public item_fee_membership(Lcom/squareup/api/items/ItemFeeMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 854
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    return-object p0
.end method

.method public item_image(Lcom/squareup/api/items/ItemImage;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 814
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    return-object p0
.end method

.method public item_item_modifier_list_membership(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 865
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    return-object p0
.end method

.method public item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 859
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    return-object p0
.end method

.method public item_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 870
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    return-object p0
.end method

.method public item_page_membership(Lcom/squareup/api/items/ItemPageMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 824
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    return-object p0
.end method

.method public item_variation(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 834
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    return-object p0
.end method

.method public market_item_settings(Lcom/squareup/api/items/MarketItemSettings;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 875
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    return-object p0
.end method

.method public menu(Lcom/squareup/api/items/Menu;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 931
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu:Lcom/squareup/api/items/Menu;

    return-object p0
.end method

.method public menu_category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 829
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    return-object p0
.end method

.method public menu_group_membership(Lcom/squareup/api/items/MenuGroupMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 956
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    return-object p0
.end method

.method public object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 777
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public object_type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 804
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_type:Lcom/squareup/api/items/Type;

    return-object p0
.end method

.method public obsolete_TenderFee(Lcom/squareup/api/items/OBSOLETE_TenderFee;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 896
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    return-object p0
.end method

.method public page(Lcom/squareup/api/items/Page;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 819
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page:Lcom/squareup/api/items/Page;

    return-object p0
.end method

.method public placeholder(Lcom/squareup/api/items/Placeholder;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 844
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder:Lcom/squareup/api/items/Placeholder;

    return-object p0
.end method

.method public pricing_rule(Lcom/squareup/api/items/PricingRule;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 966
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    return-object p0
.end method

.method public product_set(Lcom/squareup/api/items/ProductSet;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 971
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    return-object p0
.end method

.method public promo(Lcom/squareup/api/items/Promo;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 885
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo:Lcom/squareup/api/items/Promo;

    return-object p0
.end method

.method public surcharge(Lcom/squareup/api/items/Surcharge;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 961
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    return-object p0
.end method

.method public surcharge_fee_membership(Lcom/squareup/api/items/SurchargeFeeMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 981
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    return-object p0
.end method

.method public tag(Lcom/squareup/api/items/Tag;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 936
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag:Lcom/squareup/api/items/Tag;

    return-object p0
.end method

.method public tax_rule(Lcom/squareup/api/items/TaxRule;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 906
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    return-object p0
.end method

.method public ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 916
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    return-object p0
.end method

.method public ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 921
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    return-object p0
.end method

.method public time_period(Lcom/squareup/api/items/TimePeriod;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 976
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    return-object p0
.end method

.method public timestamp(Ljava/lang/Long;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 785
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->timestamp:Ljava/lang/Long;

    return-object p0
.end method

.method public void_reason(Lcom/squareup/api/items/VoidReason;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 0

    .line 926
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason:Lcom/squareup/api/items/VoidReason;

    return-object p0
.end method
