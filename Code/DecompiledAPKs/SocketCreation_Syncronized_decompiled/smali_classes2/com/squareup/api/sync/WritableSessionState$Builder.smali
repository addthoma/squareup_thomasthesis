.class public final Lcom/squareup/api/sync/WritableSessionState$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "WritableSessionState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/WritableSessionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/WritableSessionState;",
        "Lcom/squareup/api/sync/WritableSessionState$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public seq:Ljava/lang/Long;

.field public session_id:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/WritableSessionState;
    .locals 4

    .line 126
    new-instance v0, Lcom/squareup/api/sync/WritableSessionState;

    iget-object v1, p0, Lcom/squareup/api/sync/WritableSessionState$Builder;->session_id:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/api/sync/WritableSessionState$Builder;->seq:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/sync/WritableSessionState;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/api/sync/WritableSessionState$Builder;->build()Lcom/squareup/api/sync/WritableSessionState;

    move-result-object v0

    return-object v0
.end method

.method public seq(Ljava/lang/Long;)Lcom/squareup/api/sync/WritableSessionState$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/api/sync/WritableSessionState$Builder;->seq:Ljava/lang/Long;

    return-object p0
.end method

.method public session_id(Ljava/lang/Long;)Lcom/squareup/api/sync/WritableSessionState$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/api/sync/WritableSessionState$Builder;->session_id:Ljava/lang/Long;

    return-object p0
.end method
