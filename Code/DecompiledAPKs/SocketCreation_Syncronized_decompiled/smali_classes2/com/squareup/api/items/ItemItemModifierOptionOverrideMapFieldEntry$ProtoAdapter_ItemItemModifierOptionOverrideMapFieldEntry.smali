.class final Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ItemItemModifierOptionOverrideMapFieldEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 117
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;-><init>()V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 140
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 145
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 143
    :cond_0
    sget-object v3, Lcom/squareup/api/items/ItemModifierOptionOverride;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierOptionOverride;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value(Lcom/squareup/api/items/ItemModifierOptionOverride;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;

    goto :goto_0

    .line 142
    :cond_1
    sget-object v3, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;

    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 150
    invoke-virtual {v0}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->build()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 130
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 131
    sget-object v0, Lcom/squareup/api/items/ItemModifierOptionOverride;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 132
    invoke-virtual {p2}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    check-cast p2, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;)I
    .locals 4

    .line 122
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemModifierOptionOverride;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    const/4 v3, 0x2

    .line 123
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;->encodedSize(Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;
    .locals 2

    .line 156
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->newBuilder()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;

    move-result-object p1

    .line 157
    iget-object v0, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    iput-object v0, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->key:Lcom/squareup/api/sync/ObjectId;

    .line 158
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/items/ItemModifierOptionOverride;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOptionOverride;

    iput-object v0, p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    .line 159
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 160
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$Builder;->build()Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry$ProtoAdapter_ItemItemModifierOptionOverrideMapFieldEntry;->redact(Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;)Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    move-result-object p1

    return-object p1
.end method
