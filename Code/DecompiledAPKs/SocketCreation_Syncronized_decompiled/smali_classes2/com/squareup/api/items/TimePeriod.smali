.class public final Lcom/squareup/api/items/TimePeriod;
.super Lcom/squareup/wire/Message;
.source "TimePeriod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/TimePeriod$ProtoAdapter_TimePeriod;,
        Lcom/squareup/api/items/TimePeriod$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/TimePeriod;",
        "Lcom/squareup/api/items/TimePeriod$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/TimePeriod;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_RRULE:Ljava/lang/String; = ""

.field public static final DEFAULT_STARTS_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final rrule:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final starts_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/api/items/TimePeriod$ProtoAdapter_TimePeriod;

    invoke-direct {v0}, Lcom/squareup/api/items/TimePeriod$ProtoAdapter_TimePeriod;-><init>()V

    sput-object v0, Lcom/squareup/api/items/TimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 87
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/TimePeriod;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/api/items/TimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 93
    iput-object p1, p0, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    .line 94
    iput-object p2, p0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    .line 95
    iput-object p3, p0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    .line 96
    iput-object p4, p0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    .line 97
    iput-object p5, p0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 115
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/TimePeriod;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 116
    :cond_1
    check-cast p1, Lcom/squareup/api/items/TimePeriod;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/api/items/TimePeriod;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/TimePeriod;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    .line 122
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 127
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 129
    invoke-virtual {p0}, Lcom/squareup/api/items/TimePeriod;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 135
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/TimePeriod$Builder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TimePeriod$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TimePeriod$Builder;->id:Ljava/lang/String;

    .line 104
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TimePeriod$Builder;->name:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TimePeriod$Builder;->starts_at:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TimePeriod$Builder;->rrule:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TimePeriod$Builder;->duration:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/api/items/TimePeriod;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TimePeriod$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/api/items/TimePeriod;->newBuilder()Lcom/squareup/api/items/TimePeriod$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", starts_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->starts_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", rrule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->rrule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod;->duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TimePeriod{"

    .line 148
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
