.class public final Lcom/squareup/api/items/Placeholder$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Placeholder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Placeholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Placeholder;",
        "Lcom/squareup/api/items/Placeholder$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public tag_membership:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 135
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/Placeholder$Builder;->tag_membership:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/Placeholder;
    .locals 7

    .line 165
    new-instance v6, Lcom/squareup/api/items/Placeholder;

    iget-object v1, p0, Lcom/squareup/api/items/Placeholder$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Placeholder$Builder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    iget-object v3, p0, Lcom/squareup/api/items/Placeholder$Builder;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/items/Placeholder$Builder;->tag_membership:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/Placeholder;-><init>(Ljava/lang/String;Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/api/items/Placeholder$Builder;->build()Lcom/squareup/api/items/Placeholder;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Placeholder$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/api/items/Placeholder$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Placeholder$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/api/items/Placeholder$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public placeholder_type(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Lcom/squareup/api/items/Placeholder$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/api/items/Placeholder$Builder;->placeholder_type:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0
.end method

.method public tag_membership(Ljava/util/List;)Lcom/squareup/api/items/Placeholder$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/Placeholder$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 158
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 159
    iput-object p1, p0, Lcom/squareup/api/items/Placeholder$Builder;->tag_membership:Ljava/util/List;

    return-object p0
.end method
