.class public final Lcom/squareup/api/items/Fee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Fee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Fee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Fee;",
        "Lcom/squareup/api/items/Fee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

.field public amount:Lcom/squareup/protos/common/dinero/Money;

.field public applies_to_custom_amounts:Ljava/lang/Boolean;

.field public applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

.field public calculation_phase:Lcom/squareup/api/items/CalculationPhase;

.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public enabled:Ljava/lang/Boolean;

.field public fee_type_id:Ljava/lang/String;

.field public fee_type_name:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public v2_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 316
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustment_type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object p0
.end method

.method public amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public applies_to_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public applies_to_product_set(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/Fee;
    .locals 18

    move-object/from16 v0, p0

    .line 427
    new-instance v17, Lcom/squareup/api/items/Fee;

    iget-object v2, v0, Lcom/squareup/api/items/Fee$Builder;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/api/items/Fee$Builder;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/api/items/Fee$Builder;->percentage:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/api/items/Fee$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v6, v0, Lcom/squareup/api/items/Fee$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v7, v0, Lcom/squareup/api/items/Fee$Builder;->fee_type_id:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/api/items/Fee$Builder;->fee_type_name:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/api/items/Fee$Builder;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    iget-object v10, v0, Lcom/squareup/api/items/Fee$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    iget-object v11, v0, Lcom/squareup/api/items/Fee$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/api/items/Fee$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v13, v0, Lcom/squareup/api/items/Fee$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v14, v0, Lcom/squareup/api/items/Fee$Builder;->v2_id:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/api/items/Fee$Builder;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/api/items/Fee;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Fee$AdjustmentType;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/api/items/Fee$InclusionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 287
    invoke-virtual {p0}, Lcom/squareup/api/items/Fee$Builder;->build()Lcom/squareup/api/items/Fee;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    return-object p0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public fee_type_id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->fee_type_id:Ljava/lang/String;

    return-object p0
.end method

.method public fee_type_name(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->fee_type_name:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;
    .locals 0

    .line 410
    iput-object p1, p0, Lcom/squareup/api/items/Fee$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method
