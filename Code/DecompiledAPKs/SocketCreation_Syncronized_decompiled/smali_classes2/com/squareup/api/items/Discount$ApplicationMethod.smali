.class public final enum Lcom/squareup/api/items/Discount$ApplicationMethod;
.super Ljava/lang/Enum;
.source "Discount.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ApplicationMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Discount$ApplicationMethod$ProtoAdapter_ApplicationMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Discount$ApplicationMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Discount$ApplicationMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public static final enum MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 487
    new-instance v0, Lcom/squareup/api/items/Discount$ApplicationMethod;

    const/4 v1, 0x0

    const-string v2, "MANUALLY_APPLIED"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/Discount$ApplicationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 492
    new-instance v0, Lcom/squareup/api/items/Discount$ApplicationMethod;

    const/4 v2, 0x1

    const-string v3, "COMP"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/Discount$ApplicationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 483
    sget-object v3, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->$VALUES:[Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 494
    new-instance v0, Lcom/squareup/api/items/Discount$ApplicationMethod$ProtoAdapter_ApplicationMethod;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$ApplicationMethod$ProtoAdapter_ApplicationMethod;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 499
    iput p3, p0, Lcom/squareup/api/items/Discount$ApplicationMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Discount$ApplicationMethod;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 508
    :cond_0
    sget-object p0, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object p0

    .line 507
    :cond_1
    sget-object p0, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Discount$ApplicationMethod;
    .locals 1

    .line 483
    const-class v0, Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Discount$ApplicationMethod;
    .locals 1

    .line 483
    sget-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->$VALUES:[Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Discount$ApplicationMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 515
    iget v0, p0, Lcom/squareup/api/items/Discount$ApplicationMethod;->value:I

    return v0
.end method
