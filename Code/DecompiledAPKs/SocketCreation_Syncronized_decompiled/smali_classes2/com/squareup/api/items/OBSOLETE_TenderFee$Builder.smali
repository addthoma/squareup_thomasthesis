.class public final Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OBSOLETE_TenderFee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/OBSOLETE_TenderFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/OBSOLETE_TenderFee;",
        "Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

.field public calculation_phase:Lcom/squareup/api/items/CalculationPhase;

.field public enabled:Ljava/lang/Boolean;

.field public id:Ljava/lang/String;

.field public percentage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 153
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustment_type(Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;)Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/OBSOLETE_TenderFee;
    .locals 8

    .line 192
    new-instance v7, Lcom/squareup/api/items/OBSOLETE_TenderFee;

    iget-object v1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->percentage:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->adjustment_type:Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;

    iget-object v5, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->enabled:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/OBSOLETE_TenderFee;-><init>(Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Lcom/squareup/api/items/OBSOLETE_TenderFee$AdjustmentType;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->build()Lcom/squareup/api/items/OBSOLETE_TenderFee;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    return-object p0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/api/items/OBSOLETE_TenderFee$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method
