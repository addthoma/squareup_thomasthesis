.class final Lcom/squareup/api/items/Fee$InclusionType$ProtoAdapter_InclusionType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Fee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Fee$InclusionType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InclusionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/Fee$InclusionType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 509
    const-class v0, Lcom/squareup/api/items/Fee$InclusionType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Fee$InclusionType;
    .locals 0

    .line 514
    invoke-static {p1}, Lcom/squareup/api/items/Fee$InclusionType;->fromValue(I)Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 507
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Fee$InclusionType$ProtoAdapter_InclusionType;->fromValue(I)Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object p1

    return-object p1
.end method
