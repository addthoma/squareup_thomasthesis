.class public final enum Lcom/squareup/api/items/StraightFireType;
.super Ljava/lang/Enum;
.source "StraightFireType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/StraightFireType$ProtoAdapter_StraightFireType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/StraightFireType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/StraightFireType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/StraightFireType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALWAYS:Lcom/squareup/api/items/StraightFireType;

.field public static final enum NEVER:Lcom/squareup/api/items/StraightFireType;

.field public static final enum UNKNOWN_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 18
    new-instance v0, Lcom/squareup/api/items/StraightFireType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_STRAIGHT_FIRE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/StraightFireType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/StraightFireType;->UNKNOWN_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

    .line 23
    new-instance v0, Lcom/squareup/api/items/StraightFireType;

    const/4 v2, 0x1

    const-string v3, "NEVER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/StraightFireType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/StraightFireType;->NEVER:Lcom/squareup/api/items/StraightFireType;

    .line 28
    new-instance v0, Lcom/squareup/api/items/StraightFireType;

    const/4 v3, 0x2

    const-string v4, "ALWAYS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/StraightFireType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/StraightFireType;->ALWAYS:Lcom/squareup/api/items/StraightFireType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/api/items/StraightFireType;

    .line 14
    sget-object v4, Lcom/squareup/api/items/StraightFireType;->UNKNOWN_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/api/items/StraightFireType;->NEVER:Lcom/squareup/api/items/StraightFireType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/StraightFireType;->ALWAYS:Lcom/squareup/api/items/StraightFireType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/api/items/StraightFireType;->$VALUES:[Lcom/squareup/api/items/StraightFireType;

    .line 30
    new-instance v0, Lcom/squareup/api/items/StraightFireType$ProtoAdapter_StraightFireType;

    invoke-direct {v0}, Lcom/squareup/api/items/StraightFireType$ProtoAdapter_StraightFireType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/StraightFireType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lcom/squareup/api/items/StraightFireType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/StraightFireType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 45
    :cond_0
    sget-object p0, Lcom/squareup/api/items/StraightFireType;->ALWAYS:Lcom/squareup/api/items/StraightFireType;

    return-object p0

    .line 44
    :cond_1
    sget-object p0, Lcom/squareup/api/items/StraightFireType;->NEVER:Lcom/squareup/api/items/StraightFireType;

    return-object p0

    .line 43
    :cond_2
    sget-object p0, Lcom/squareup/api/items/StraightFireType;->UNKNOWN_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/StraightFireType;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/api/items/StraightFireType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/StraightFireType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/StraightFireType;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/api/items/StraightFireType;->$VALUES:[Lcom/squareup/api/items/StraightFireType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/StraightFireType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/StraightFireType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/api/items/StraightFireType;->value:I

    return v0
.end method
