.class public final Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemItemModifierListMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemItemModifierListMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemItemModifierListMembership;",
        "Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enabled:Ljava/lang/Boolean;

.field public hidden_from_customer:Ljava/lang/Boolean;

.field public item:Lcom/squareup/api/sync/ObjectId;

.field public item_modifier_option_overrides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;"
        }
    .end annotation
.end field

.field public max_selected_modifiers:Ljava/lang/Integer;

.field public min_selected_modifiers:Ljava/lang/Integer;

.field public modifier_list:Lcom/squareup/api/sync/ObjectId;

.field public visibility:Lcom/squareup/api/items/Visibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 214
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 215
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item_modifier_option_overrides:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemItemModifierListMembership;
    .locals 11

    .line 285
    new-instance v10, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->visibility:Lcom/squareup/api/items/Visibility;

    iget-object v4, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item_modifier_option_overrides:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->min_selected_modifiers:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->max_selected_modifiers:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->hidden_from_customer:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/ItemItemModifierListMembership;-><init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/Visibility;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 197
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->build()Lcom/squareup/api/items/ItemItemModifierListMembership;

    move-result-object v0

    return-object v0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public hidden_from_customer(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->hidden_from_customer:Ljava/lang/Boolean;

    return-object p0
.end method

.method public item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public item_modifier_option_overrides(Ljava/util/List;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;)",
            "Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;"
        }
    .end annotation

    .line 242
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 243
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item_modifier_option_overrides:Ljava/util/List;

    return-object p0
.end method

.method public max_selected_modifiers(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->max_selected_modifiers:Ljava/lang/Integer;

    return-object p0
.end method

.method public min_selected_modifiers(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->min_selected_modifiers:Ljava/lang/Integer;

    return-object p0
.end method

.method public modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public visibility(Lcom/squareup/api/items/Visibility;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->visibility:Lcom/squareup/api/items/Visibility;

    return-object p0
.end method
