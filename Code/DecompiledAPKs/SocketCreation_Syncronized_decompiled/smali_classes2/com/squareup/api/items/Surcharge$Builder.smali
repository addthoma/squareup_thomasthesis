.class public final Lcom/squareup/api/items/Surcharge$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Surcharge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Surcharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Surcharge;",
        "Lcom/squareup/api/items/Surcharge$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/dinero/Money;

.field public auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

.field public calculation_phase:Lcom/squareup/api/items/CalculationPhase;

.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public taxable:Ljava/lang/Boolean;

.field public type:Lcom/squareup/api/items/Surcharge$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 219
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public auto_gratuity(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/Surcharge;
    .locals 12

    .line 280
    new-instance v11, Lcom/squareup/api/items/Surcharge;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Surcharge$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/Surcharge$Builder;->percentage:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/items/Surcharge$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v5, p0, Lcom/squareup/api/items/Surcharge$Builder;->type:Lcom/squareup/api/items/Surcharge$Type;

    iget-object v6, p0, Lcom/squareup/api/items/Surcharge$Builder;->taxable:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/api/items/Surcharge$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v8, p0, Lcom/squareup/api/items/Surcharge$Builder;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    iget-object v9, p0, Lcom/squareup/api/items/Surcharge$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/api/items/Surcharge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/Surcharge$Type;Ljava/lang/Boolean;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 200
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge$Builder;->build()Lcom/squareup/api/items/Surcharge;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    return-object p0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public taxable(Ljava/lang/Boolean;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->taxable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public type(Lcom/squareup/api/items/Surcharge$Type;)Lcom/squareup/api/items/Surcharge$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$Builder;->type:Lcom/squareup/api/items/Surcharge$Type;

    return-object p0
.end method
