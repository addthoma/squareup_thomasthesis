.class public final Lcom/squareup/api/items/TimePeriod$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TimePeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TimePeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TimePeriod;",
        "Lcom/squareup/api/items/TimePeriod$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public duration:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public rrule:Ljava/lang/String;

.field public starts_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 162
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TimePeriod;
    .locals 8

    .line 211
    new-instance v7, Lcom/squareup/api/items/TimePeriod;

    iget-object v1, p0, Lcom/squareup/api/items/TimePeriod$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/TimePeriod$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/TimePeriod$Builder;->starts_at:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/items/TimePeriod$Builder;->rrule:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/api/items/TimePeriod$Builder;->duration:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/TimePeriod;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/api/items/TimePeriod$Builder;->build()Lcom/squareup/api/items/TimePeriod;

    move-result-object v0

    return-object v0
.end method

.method public duration(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/api/items/TimePeriod$Builder;->duration:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/api/items/TimePeriod$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/api/items/TimePeriod$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public rrule(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/api/items/TimePeriod$Builder;->rrule:Ljava/lang/String;

    return-object p0
.end method

.method public starts_at(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/api/items/TimePeriod$Builder;->starts_at:Ljava/lang/String;

    return-object p0
.end method
