.class public final enum Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;
.super Ljava/lang/Enum;
.source "Surcharge.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AutoEnableType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType$ProtoAdapter_AutoEnableType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALWAYS:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

.field public static final enum MINIMUM_SEAT_COUNT:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

.field public static final enum UNKNOWN:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 435
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->UNKNOWN:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    .line 441
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    const/4 v2, 0x1

    const-string v3, "MINIMUM_SEAT_COUNT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->MINIMUM_SEAT_COUNT:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    .line 446
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    const/4 v3, 0x2

    const-string v4, "ALWAYS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ALWAYS:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    .line 434
    sget-object v4, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->UNKNOWN:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->MINIMUM_SEAT_COUNT:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ALWAYS:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->$VALUES:[Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    .line 448
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType$ProtoAdapter_AutoEnableType;

    invoke-direct {v0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType$ProtoAdapter_AutoEnableType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 452
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 453
    iput p3, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 463
    :cond_0
    sget-object p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ALWAYS:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-object p0

    .line 462
    :cond_1
    sget-object p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->MINIMUM_SEAT_COUNT:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-object p0

    .line 461
    :cond_2
    sget-object p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->UNKNOWN:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;
    .locals 1

    .line 434
    const-class v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;
    .locals 1

    .line 434
    sget-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->$VALUES:[Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 470
    iget v0, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->value:I

    return v0
.end method
