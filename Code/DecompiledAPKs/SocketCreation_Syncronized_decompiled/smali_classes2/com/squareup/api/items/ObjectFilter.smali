.class public final enum Lcom/squareup/api/items/ObjectFilter;
.super Ljava/lang/Enum;
.source "ObjectFilter.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ObjectFilter$ProtoAdapter_ObjectFilter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/ObjectFilter;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/ObjectFilter;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ObjectFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL:Lcom/squareup/api/items/ObjectFilter;

.field public static final enum INCLUDE:Lcom/squareup/api/items/ObjectFilter;

.field public static final enum UNKNOWN_OBJECT_FILTER:Lcom/squareup/api/items/ObjectFilter;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 11
    new-instance v0, Lcom/squareup/api/items/ObjectFilter;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_OBJECT_FILTER"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/ObjectFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ObjectFilter;->UNKNOWN_OBJECT_FILTER:Lcom/squareup/api/items/ObjectFilter;

    .line 17
    new-instance v0, Lcom/squareup/api/items/ObjectFilter;

    const/4 v2, 0x1

    const-string v3, "ALL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/ObjectFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    .line 22
    new-instance v0, Lcom/squareup/api/items/ObjectFilter;

    const/4 v3, 0x2

    const-string v4, "INCLUDE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/ObjectFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/api/items/ObjectFilter;

    .line 10
    sget-object v4, Lcom/squareup/api/items/ObjectFilter;->UNKNOWN_OBJECT_FILTER:Lcom/squareup/api/items/ObjectFilter;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/api/items/ObjectFilter;->$VALUES:[Lcom/squareup/api/items/ObjectFilter;

    .line 24
    new-instance v0, Lcom/squareup/api/items/ObjectFilter$ProtoAdapter_ObjectFilter;

    invoke-direct {v0}, Lcom/squareup/api/items/ObjectFilter$ProtoAdapter_ObjectFilter;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ObjectFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/squareup/api/items/ObjectFilter;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/ObjectFilter;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 39
    :cond_0
    sget-object p0, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    return-object p0

    .line 38
    :cond_1
    sget-object p0, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    return-object p0

    .line 37
    :cond_2
    sget-object p0, Lcom/squareup/api/items/ObjectFilter;->UNKNOWN_OBJECT_FILTER:Lcom/squareup/api/items/ObjectFilter;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/ObjectFilter;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/items/ObjectFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/ObjectFilter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/ObjectFilter;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/items/ObjectFilter;->$VALUES:[Lcom/squareup/api/items/ObjectFilter;

    invoke-virtual {v0}, [Lcom/squareup/api/items/ObjectFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/ObjectFilter;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/api/items/ObjectFilter;->value:I

    return v0
.end method
