.class Lcom/squareup/api/ClientSettingsValidator$2;
.super Ljava/lang/Object;
.source "ClientSettingsValidator.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ClientSettingsValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Pair<",
        "Lcom/squareup/api/ClientInfo;",
        "Lcom/squareup/server/api/ClientSettings;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ClientSettingsValidator;


# direct methods
.method constructor <init>(Lcom/squareup/api/ClientSettingsValidator;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 97
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/api/ClientSettingsValidator$2;->call(Lkotlin/Pair;)V

    return-void
.end method

.method public call(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/ClientInfo;",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;)V"
        }
    .end annotation

    .line 99
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ClientInfo;

    .line 100
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/api/ClientSettings;

    .line 101
    iget-boolean v1, v0, Lcom/squareup/api/ClientInfo;->isWebRequest:Z

    if-nez v1, :cond_1

    .line 102
    iget-object v1, v0, Lcom/squareup/api/ClientInfo;->packageName:Ljava/lang/String;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/api/ClientSettingsValidator;->access$500(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    iget-object v2, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {v2}, Lcom/squareup/api/ClientSettingsValidator;->access$000(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/ClientSettingsCache;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/api/ClientSettingsCache;->remove(Ljava/lang/String;)V

    .line 106
    iget-object v2, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    sget-object v3, Lcom/squareup/api/ApiErrorResult;->INVALID_PACKAGE:Lcom/squareup/api/ApiErrorResult;

    invoke-static {v2, v0, v3}, Lcom/squareup/api/ClientSettingsValidator;->access$600(Lcom/squareup/api/ClientSettingsValidator;Lcom/squareup/api/ClientInfo;Lcom/squareup/api/ApiErrorResult;)V

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {v2}, Lcom/squareup/api/ClientSettingsValidator;->access$700(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/FingerprintVerifier;

    move-result-object v2

    invoke-virtual {v2, v1, p1}, Lcom/squareup/api/FingerprintVerifier;->validateFingerprint(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 109
    iget-object p1, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {p1}, Lcom/squareup/api/ClientSettingsValidator;->access$000(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/ClientSettingsCache;

    move-result-object p1

    iget-object v1, v0, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/api/ClientSettingsCache;->remove(Ljava/lang/String;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->UNKNOWN_FINGERPRINT:Lcom/squareup/api/ApiErrorResult;

    invoke-static {p1, v0, v1}, Lcom/squareup/api/ClientSettingsValidator;->access$600(Lcom/squareup/api/ClientSettingsValidator;Lcom/squareup/api/ClientInfo;Lcom/squareup/api/ApiErrorResult;)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v1, v0, Lcom/squareup/api/ClientInfo;->webCallbackUri:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/squareup/api/ClientSettingsValidator;->access$800(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    :goto_0
    return-void

    .line 113
    :cond_3
    iget-object p1, p0, Lcom/squareup/api/ClientSettingsValidator$2;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {p1}, Lcom/squareup/api/ClientSettingsValidator;->access$000(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/ClientSettingsCache;

    move-result-object p1

    iget-object v0, v0, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/api/ClientSettingsCache;->remove(Ljava/lang/String;)V

    .line 114
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1
.end method
