.class public Lcom/squareup/api/TransactionParams;
.super Ljava/lang/Object;
.source "TransactionParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/TransactionParams$Builder;
    }
.end annotation


# static fields
.field public static final AUTO_RETURN_TIMEOUT_MILLIS:J = 0x7d0L

.field public static final V3_AUTO_RETURN_TIMEOUT_KEY:Ljava/lang/String; = "V3_AUTO_RETURN"

.field public static final V3_EXTRA_TENDER_CARD:Ljava/lang/String; = "V3_TENDER_CARD"


# instance fields
.field public final allowSplitTender:Z

.field public final amount:Ljava/lang/Long;

.field public final currencyCode:Ljava/lang/String;

.field public final customTipPercentages:[I

.field public final customerId:Ljava/lang/String;

.field public final delayCapture:Z

.field public final note:Ljava/lang/String;

.field public final showCustomTip:Z

.field public final showSeparateTipScreen:Z

.field public final skipReceipt:Z

.field public final skipSignature:Z

.field public final tenderTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public final timeout:J

.field public final tippingEnabled:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/Long;JLjava/util/Set;Ljava/lang/String;Ljava/lang/String;ZZZZZZ[IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "J",
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZZZZ[IZ)V"
        }
    .end annotation

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p1, p0, Lcom/squareup/api/TransactionParams;->currencyCode:Ljava/lang/String;

    .line 150
    iput-object p2, p0, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    .line 151
    iput-wide p3, p0, Lcom/squareup/api/TransactionParams;->timeout:J

    .line 152
    iput-object p5, p0, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    .line 153
    iput-object p6, p0, Lcom/squareup/api/TransactionParams;->customerId:Ljava/lang/String;

    .line 154
    iput-object p7, p0, Lcom/squareup/api/TransactionParams;->note:Ljava/lang/String;

    .line 155
    iput-boolean p9, p0, Lcom/squareup/api/TransactionParams;->skipReceipt:Z

    .line 156
    iput-boolean p8, p0, Lcom/squareup/api/TransactionParams;->skipSignature:Z

    .line 157
    iput-boolean p10, p0, Lcom/squareup/api/TransactionParams;->allowSplitTender:Z

    .line 158
    iput-boolean p11, p0, Lcom/squareup/api/TransactionParams;->tippingEnabled:Z

    .line 159
    iput-boolean p12, p0, Lcom/squareup/api/TransactionParams;->showSeparateTipScreen:Z

    .line 160
    iput-boolean p13, p0, Lcom/squareup/api/TransactionParams;->showCustomTip:Z

    .line 161
    iput-object p14, p0, Lcom/squareup/api/TransactionParams;->customTipPercentages:[I

    .line 162
    iput-boolean p15, p0, Lcom/squareup/api/TransactionParams;->delayCapture:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/Long;JLjava/util/Set;Ljava/lang/String;Ljava/lang/String;ZZZZZZ[IZLcom/squareup/api/TransactionParams$1;)V
    .locals 0

    .line 26
    invoke-direct/range {p0 .. p15}, Lcom/squareup/api/TransactionParams;-><init>(Ljava/lang/String;Ljava/lang/Long;JLjava/util/Set;Ljava/lang/String;Ljava/lang/String;ZZZZZZ[IZ)V

    return-void
.end method

.method public static fromNativeRequest(Landroid/content/Intent;)Lcom/squareup/api/TransactionParams;
    .locals 15

    const-string v0, "com.squareup.pos.CURRENCY_CODE"

    .line 79
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {p0}, Lcom/squareup/api/ApiTenderType;->extractTenderTypes(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v1

    .line 81
    invoke-static {p0}, Lcom/squareup/api/TransactionParams;->parseNativeAmount(Landroid/content/Intent;)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "com.squareup.pos.AUTO_RETURN"

    .line 82
    invoke-static {p0, v3}, Lcom/squareup/api/TransactionParams;->parseTimeout(Landroid/content/Intent;Ljava/lang/String;)J

    move-result-wide v3

    const-string v5, "com.squareup.pos.NOTE"

    .line 83
    invoke-virtual {p0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.squareup.pos.CUSTOMER_ID"

    .line 84
    invoke-virtual {p0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string v8, "com.squareup.pos.SKIP_RECEIPT"

    .line 85
    invoke-virtual {p0, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const-string v9, "com.squareup.pos.ALLOW_SPLIT_TENDER"

    .line 86
    invoke-virtual {p0, v9, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const-string v10, "com.squareup.pos.SKIP_SIGNATURE"

    .line 89
    invoke-virtual {p0, v10, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v11, "com.squareup.pos.DELAY_CAPTURE"

    .line 90
    invoke-virtual {p0, v11, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    const-string v12, "com.squareup.pos.ENABLE_TIPPING"

    const/4 v13, 0x1

    .line 91
    invoke-virtual {p0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    const-string v13, "com.squareup.pos.SHOW_CUSTOM_TIP"

    .line 92
    invoke-virtual {p0, v13, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    const-string v14, "com.squareup.pos.SEPARATE_TIP_SCREEN"

    .line 93
    invoke-virtual {p0, v14, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v14, "com.squareup.pos.CUSTOM_TIP_PERCENTAGES"

    .line 94
    invoke-virtual {p0, v14}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p0

    .line 96
    new-instance v14, Lcom/squareup/api/TransactionParams$Builder;

    invoke-direct {v14}, Lcom/squareup/api/TransactionParams$Builder;-><init>()V

    .line 97
    invoke-virtual {v14, v0}, Lcom/squareup/api/TransactionParams$Builder;->currencyCode(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v2}, Lcom/squareup/api/TransactionParams$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v3, v4}, Lcom/squareup/api/TransactionParams$Builder;->timeout(J)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v1}, Lcom/squareup/api/TransactionParams$Builder;->tenderTypes(Ljava/util/Set;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 101
    invoke-virtual {v0, v6}, Lcom/squareup/api/TransactionParams$Builder;->customerId(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v5}, Lcom/squareup/api/TransactionParams$Builder;->note(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v10}, Lcom/squareup/api/TransactionParams$Builder;->skipSignature(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0, v8}, Lcom/squareup/api/TransactionParams$Builder;->skipReceipt(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0, v9}, Lcom/squareup/api/TransactionParams$Builder;->allowSplitTender(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v12}, Lcom/squareup/api/TransactionParams$Builder;->tippingEnabled(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0, v7}, Lcom/squareup/api/TransactionParams$Builder;->showSeparateTipScreen(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v13}, Lcom/squareup/api/TransactionParams$Builder;->showCustomTip(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0, p0}, Lcom/squareup/api/TransactionParams$Builder;->customTipPercentages([I)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 110
    invoke-virtual {p0, v11}, Lcom/squareup/api/TransactionParams$Builder;->delayCapture(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 111
    invoke-virtual {p0}, Lcom/squareup/api/TransactionParams$Builder;->build()Lcom/squareup/api/TransactionParams;

    move-result-object p0

    return-object p0
.end method

.method public static fromWebRequest(Landroid/content/Intent;)Lcom/squareup/api/TransactionParams;
    .locals 10

    const-string v0, "currency_code"

    .line 50
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-static {p0}, Lcom/squareup/api/ApiTenderType;->extractTenderTypes(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v1

    .line 52
    invoke-static {p0}, Lcom/squareup/api/TransactionParams;->parseWebAmount(Landroid/content/Intent;)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "auto_return"

    .line 53
    invoke-static {p0, v3}, Lcom/squareup/api/TransactionParams;->parseTimeout(Landroid/content/Intent;Ljava/lang/String;)J

    move-result-wide v3

    const-string v5, "note"

    .line 54
    invoke-virtual {p0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "customer_id"

    .line 55
    invoke-virtual {p0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string v8, "skip_receipt"

    .line 56
    invoke-virtual {p0, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const-string v9, "allow_split_tender"

    .line 58
    invoke-virtual {p0, v9, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p0

    .line 60
    new-instance v9, Lcom/squareup/api/TransactionParams$Builder;

    invoke-direct {v9}, Lcom/squareup/api/TransactionParams$Builder;-><init>()V

    .line 61
    invoke-virtual {v9, v0}, Lcom/squareup/api/TransactionParams$Builder;->currencyCode(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v2}, Lcom/squareup/api/TransactionParams$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v3, v4}, Lcom/squareup/api/TransactionParams$Builder;->timeout(J)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0, v1}, Lcom/squareup/api/TransactionParams$Builder;->tenderTypes(Ljava/util/Set;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0, v6}, Lcom/squareup/api/TransactionParams$Builder;->customerId(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0, v5}, Lcom/squareup/api/TransactionParams$Builder;->note(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 67
    invoke-virtual {v0, v7}, Lcom/squareup/api/TransactionParams$Builder;->skipSignature(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v8}, Lcom/squareup/api/TransactionParams$Builder;->skipReceipt(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0, p0}, Lcom/squareup/api/TransactionParams$Builder;->allowSplitTender(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 70
    invoke-virtual {p0, v7}, Lcom/squareup/api/TransactionParams$Builder;->tippingEnabled(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 71
    invoke-virtual {p0, v7}, Lcom/squareup/api/TransactionParams$Builder;->showSeparateTipScreen(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 72
    invoke-virtual {p0, v7}, Lcom/squareup/api/TransactionParams$Builder;->showCustomTip(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    const/4 v0, 0x0

    .line 73
    invoke-virtual {p0, v0}, Lcom/squareup/api/TransactionParams$Builder;->customTipPercentages([I)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 74
    invoke-virtual {p0, v7}, Lcom/squareup/api/TransactionParams$Builder;->delayCapture(Z)Lcom/squareup/api/TransactionParams$Builder;

    move-result-object p0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/api/TransactionParams$Builder;->build()Lcom/squareup/api/TransactionParams;

    move-result-object p0

    return-object p0
.end method

.method private static parseNativeAmount(Landroid/content/Intent;)Ljava/lang/Long;
    .locals 5

    const-string v0, "com.squareup.pos.TOTAL_AMOUNT"

    .line 121
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const/high16 v1, -0x80000000

    .line 122
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    int-to-long v0, p0

    const-wide/32 v3, -0x80000000

    cmp-long p0, v0, v3

    if-eqz p0, :cond_0

    .line 123
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_0
    return-object v2

    :cond_1
    const-string v0, "com.squareup.pos.TOTAL_AMOUNT_LONG"

    .line 124
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-wide/high16 v3, -0x8000000000000000L

    .line 125
    invoke-virtual {p0, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long p0, v0, v3

    if-eqz p0, :cond_2

    .line 126
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_2
    return-object v2
.end method

.method private static parseTimeout(Landroid/content/Intent;Ljava/lang/String;)J
    .locals 2

    const/4 v0, 0x0

    .line 134
    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x7d0

    const-string p1, "V3_AUTO_RETURN"

    .line 137
    invoke-virtual {p0, p1, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide p0

    goto :goto_0

    :cond_0
    const-wide/16 p0, 0x0

    :goto_0
    return-wide p0
.end method

.method private static parseWebAmount(Landroid/content/Intent;)Ljava/lang/Long;
    .locals 4

    const-string v0, "amount"

    const/high16 v1, -0x80000000

    .line 116
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    int-to-long v0, p0

    const-wide/32 v2, -0x80000000

    cmp-long p0, v0, v2

    if-eqz p0, :cond_0

    .line 117
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public copyToIntent(Landroid/content/Intent;)V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    invoke-static {p1, v0}, Lcom/squareup/api/ApiTenderType;->putTenderTypes(Landroid/content/Intent;Ljava/util/Set;)V

    .line 167
    iget-wide v0, p0, Lcom/squareup/api/TransactionParams;->timeout:J

    const-string v2, "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

    invoke-virtual {p1, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 168
    iget-boolean v0, p0, Lcom/squareup/api/TransactionParams;->skipReceipt:Z

    const-string v1, "com.squareup.pos.SKIP_RECEIPT"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    iget-boolean v0, p0, Lcom/squareup/api/TransactionParams;->allowSplitTender:Z

    const-string v1, "com.squareup.pos.ALLOW_SPLIT_TENDER"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 170
    iget-boolean v0, p0, Lcom/squareup/api/TransactionParams;->delayCapture:Z

    const-string v1, "com.squareup.pos.DELAY_CAPTURE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-void
.end method
