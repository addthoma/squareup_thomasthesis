.class public final Lcom/squareup/api/multipassauth/MultipassService$DefaultImpls;
.super Ljava/lang/Object;
.source "MultipassService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/multipassauth/MultipassService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic updateDeviceIdIfNeeded$default(Lcom/squareup/api/multipassauth/MultipassService;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/server/SimpleStandardResponse;
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 53
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    :cond_0
    invoke-interface {p0, p1}, Lcom/squareup/api/multipassauth/MultipassService;->updateDeviceIdIfNeeded(Ljava/lang/Object;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: updateDeviceIdIfNeeded"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
