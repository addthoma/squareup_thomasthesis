.class final Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;
.super Ljava/lang/Object;
.source "RealMultipassOtkHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->authAndRedirectToUrl(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $targetUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;


# direct methods
.method constructor <init>(Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;->this$0:Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    iput-object p2, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;->$targetUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
            ">;)",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_2

    .line 56
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/multipass/service/CreateOtkResponse;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lokio/ByteString;->base64Url()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 59
    new-instance v0, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$OtkUri;

    iget-object v1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;->this$0:Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    iget-object v2, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;->$targetUri:Landroid/net/Uri;

    const-string v3, "targetUri"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2, p1}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->access$buildOtkUri(Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$OtkUri;-><init>(Landroid/net/Uri;)V

    check-cast v0, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;

    goto :goto_1

    .line 61
    :cond_1
    sget-object p1, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$None;->INSTANCE:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$None;

    move-object v0, p1

    check-cast v0, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;

    goto :goto_1

    .line 64
    :cond_2
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_3

    .line 65
    sget-object p1, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$None;->INSTANCE:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$None;

    move-object v0, p1

    check-cast v0, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;

    :goto_1
    return-object v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;

    move-result-object p1

    return-object p1
.end method
