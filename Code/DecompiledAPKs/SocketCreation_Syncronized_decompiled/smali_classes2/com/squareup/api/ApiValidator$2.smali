.class Lcom/squareup/api/ApiValidator$2;
.super Ljava/lang/Object;
.source "ApiValidator.java"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ApiValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/api/RequestParams;",
        "Lrx/Observable<",
        "Lkotlin/Pair<",
        "Lcom/squareup/api/RequestParams;",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiValidator;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiValidator;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/api/ApiValidator$2;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 219
    check-cast p1, Lcom/squareup/api/RequestParams;

    invoke-virtual {p0, p1}, Lcom/squareup/api/ApiValidator$2;->call(Lcom/squareup/api/RequestParams;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(Lcom/squareup/api/RequestParams;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/RequestParams;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/RequestParams;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation

    .line 222
    invoke-virtual {p1}, Lcom/squareup/api/RequestParams;->isStoreCardRequest()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p1, Lcom/squareup/api/RequestParams;->saveCardOnFileParams:Lcom/squareup/api/SaveCardOnFileParams;

    iget-object v0, v0, Lcom/squareup/api/SaveCardOnFileParams;->customerId:Ljava/lang/String;

    goto :goto_0

    .line 224
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/api/RequestParams;->isChargeRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p1, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    iget-object v0, v0, Lcom/squareup/api/TransactionParams;->customerId:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 231
    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, p1, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 234
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/ApiValidator$2;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v1}, Lcom/squareup/api/ApiValidator;->access$500(Lcom/squareup/api/ApiValidator;)Lcom/squareup/settings/server/Features;

    move-result-object v1

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/squareup/api/ApiValidator$2;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v1}, Lcom/squareup/api/ApiValidator;->access$500(Lcom/squareup/api/ApiValidator;)Lcom/squareup/settings/server/Features;

    move-result-object v1

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    .line 236
    :cond_3
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NOT_SUPPORTED:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 240
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/squareup/api/ApiValidator$2;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v1}, Lcom/squareup/api/ApiValidator;->access$100(Lcom/squareup/api/ApiValidator;)Lcom/squareup/AppDelegate;

    move-result-object v1

    const-class v2, Lcom/squareup/api/ApiValidationLoggedInComponent;

    invoke-interface {v1, v2}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/ApiValidationLoggedInComponent;

    .line 241
    invoke-interface {v1}, Lcom/squareup/api/ApiValidationLoggedInComponent;->rolodexServiceHelper()Lcom/squareup/crm/RolodexServiceHelper;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/api/-$$Lambda$ApiValidator$2$fc3zKc9vmTIsusnWtn4nH_7khDY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/api/-$$Lambda$ApiValidator$2$fc3zKc9vmTIsusnWtn4nH_7khDY;-><init>(Lcom/squareup/api/ApiValidator$2;Lcom/squareup/api/RequestParams;)V

    .line 243
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$call$0$ApiValidator$2(Lcom/squareup/api/RequestParams;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lkotlin/Pair;
    .locals 1

    .line 244
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 245
    new-instance v0, Lkotlin/Pair;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {v0, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 247
    :cond_0
    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 248
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 249
    new-instance p2, Lcom/squareup/api/ApiValidator$2$1;

    invoke-direct {p2, p0}, Lcom/squareup/api/ApiValidator$2$1;-><init>(Lcom/squareup/api/ApiValidator$2;)V

    invoke-virtual {p1, p2}, Lcom/squareup/receiving/ReceivedResponse;->map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/ApiErrorResult;

    .line 292
    new-instance p2, Lcom/squareup/api/ApiValidationException;

    invoke-direct {p2, p1}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p2
.end method
