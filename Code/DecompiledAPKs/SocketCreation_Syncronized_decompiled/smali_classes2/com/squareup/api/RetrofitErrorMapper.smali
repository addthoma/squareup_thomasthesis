.class public abstract Lcom/squareup/api/RetrofitErrorMapper;
.super Ljava/lang/Object;
.source "RetrofitErrorMapper.java"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Ljava/lang/Throwable;",
        "Lrx/Observable<",
        "+TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private mapToError(Ljava/lang/Throwable;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")TE;"
        }
    .end annotation

    .line 27
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-nez v0, :cond_0

    const-string v0, "Unexpected error"

    .line 28
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/api/RetrofitErrorMapper;->notANetworkError()Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 31
    :cond_0
    check-cast p1, Lretrofit/RetrofitError;

    .line 32
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v0

    sget-object v1, Lretrofit/RetrofitError$Kind;->NETWORK:Lretrofit/RetrofitError$Kind;

    if-ne v0, v1, :cond_1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/api/RetrofitErrorMapper;->noNetwork()Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 35
    :cond_1
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object p1

    if-nez p1, :cond_2

    .line 37
    invoke-virtual {p0}, Lcom/squareup/api/RetrofitErrorMapper;->unexpectedHttpError()Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 39
    :cond_2
    invoke-virtual {p1}, Lretrofit/client/Response;->getStatus()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/api/RetrofitErrorMapper;->fromStatus(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/api/RetrofitErrorMapper;->call(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(Ljava/lang/Throwable;)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lrx/Observable<",
            "+TT;>;"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/api/RetrofitErrorMapper;->mapToError(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p1

    .line 23
    invoke-virtual {p0, p1}, Lcom/squareup/api/RetrofitErrorMapper;->createException(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->error(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected abstract createException(Ljava/lang/Object;)Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation
.end method

.method protected abstract fromStatus(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation
.end method

.method protected abstract noNetwork()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation
.end method

.method protected abstract notANetworkError()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation
.end method

.method protected abstract unexpectedHttpError()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation
.end method
