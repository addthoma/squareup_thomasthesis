.class public Lcom/squareup/api/TransactionParams$Builder;
.super Ljava/lang/Object;
.source "TransactionParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/TransactionParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private allowSplitTender:Z

.field private amount:Ljava/lang/Long;

.field private currencyCode:Ljava/lang/String;

.field private customTipPercentages:[I

.field private customerId:Ljava/lang/String;

.field private delayCapture:Z

.field private note:Ljava/lang/String;

.field private showCustomTip:Z

.field private showSeparateTipScreen:Z

.field private skipReceipt:Z

.field private skipSignature:Z

.field private tenderTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;"
        }
    .end annotation
.end field

.field private timeout:J

.field private tippingEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public allowSplitTender(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 239
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->allowSplitTender:Z

    return-object p0
.end method

.method public amount(Ljava/lang/Long;)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/api/TransactionParams$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/api/TransactionParams;
    .locals 20

    move-object/from16 v0, p0

    .line 264
    new-instance v18, Lcom/squareup/api/TransactionParams;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/api/TransactionParams$Builder;->currencyCode:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/api/TransactionParams$Builder;->amount:Ljava/lang/Long;

    iget-wide v4, v0, Lcom/squareup/api/TransactionParams$Builder;->timeout:J

    iget-object v6, v0, Lcom/squareup/api/TransactionParams$Builder;->tenderTypes:Ljava/util/Set;

    iget-object v7, v0, Lcom/squareup/api/TransactionParams$Builder;->customerId:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/api/TransactionParams$Builder;->note:Ljava/lang/String;

    iget-boolean v9, v0, Lcom/squareup/api/TransactionParams$Builder;->skipSignature:Z

    iget-boolean v10, v0, Lcom/squareup/api/TransactionParams$Builder;->skipReceipt:Z

    iget-boolean v11, v0, Lcom/squareup/api/TransactionParams$Builder;->allowSplitTender:Z

    iget-boolean v12, v0, Lcom/squareup/api/TransactionParams$Builder;->tippingEnabled:Z

    iget-boolean v13, v0, Lcom/squareup/api/TransactionParams$Builder;->showSeparateTipScreen:Z

    iget-boolean v14, v0, Lcom/squareup/api/TransactionParams$Builder;->showCustomTip:Z

    iget-object v15, v0, Lcom/squareup/api/TransactionParams$Builder;->customTipPercentages:[I

    move-object/from16 v19, v1

    iget-boolean v1, v0, Lcom/squareup/api/TransactionParams$Builder;->delayCapture:Z

    move/from16 v16, v1

    const/16 v17, 0x0

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/api/TransactionParams;-><init>(Ljava/lang/String;Ljava/lang/Long;JLjava/util/Set;Ljava/lang/String;Ljava/lang/String;ZZZZZZ[IZLcom/squareup/api/TransactionParams$1;)V

    return-object v18
.end method

.method public currencyCode(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/api/TransactionParams$Builder;->currencyCode:Ljava/lang/String;

    return-object p0
.end method

.method public customTipPercentages([I)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/api/TransactionParams$Builder;->customTipPercentages:[I

    return-object p0
.end method

.method public customerId(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/api/TransactionParams$Builder;->customerId:Ljava/lang/String;

    return-object p0
.end method

.method public delayCapture(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 234
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->delayCapture:Z

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/api/TransactionParams$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public showCustomTip(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 254
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->showCustomTip:Z

    return-object p0
.end method

.method public showSeparateTipScreen(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 249
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->showSeparateTipScreen:Z

    return-object p0
.end method

.method public skipReceipt(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 224
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->skipReceipt:Z

    return-object p0
.end method

.method public skipSignature(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 229
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->skipSignature:Z

    return-object p0
.end method

.method public tenderTypes(Ljava/util/Set;)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;)",
            "Lcom/squareup/api/TransactionParams$Builder;"
        }
    .end annotation

    .line 209
    iput-object p1, p0, Lcom/squareup/api/TransactionParams$Builder;->tenderTypes:Ljava/util/Set;

    return-object p0
.end method

.method public timeout(J)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 204
    iput-wide p1, p0, Lcom/squareup/api/TransactionParams$Builder;->timeout:J

    return-object p0
.end method

.method public tippingEnabled(Z)Lcom/squareup/api/TransactionParams$Builder;
    .locals 0

    .line 244
    iput-boolean p1, p0, Lcom/squareup/api/TransactionParams$Builder;->tippingEnabled:Z

    return-object p0
.end method
