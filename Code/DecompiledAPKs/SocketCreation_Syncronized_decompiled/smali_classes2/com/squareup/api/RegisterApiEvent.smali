.class public Lcom/squareup/api/RegisterApiEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "RegisterApiEvent.java"


# static fields
.field public static final LOG_SEQUENCE_KEY:Ljava/lang/String; = "api_sequence_uuid"


# instance fields
.field public final client_id:Ljava/lang/String;

.field public final millis_since_request_start:J


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V
    .locals 0

    .line 29
    invoke-direct {p0, p2}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 30
    iput-object p3, p0, Lcom/squareup/api/RegisterApiEvent;->client_id:Ljava/lang/String;

    .line 31
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide p1

    sub-long/2addr p1, p4

    iput-wide p1, p0, Lcom/squareup/api/RegisterApiEvent;->millis_since_request_start:J

    return-void
.end method
