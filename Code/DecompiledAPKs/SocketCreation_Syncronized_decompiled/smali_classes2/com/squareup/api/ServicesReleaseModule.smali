.class public Lcom/squareup/api/ServicesReleaseModule;
.super Ljava/lang/Object;
.source "ServicesReleaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideActivationService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/activation/ActivationService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    const-class v0, Lcom/squareup/server/activation/ActivationService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/activation/ActivationService;

    return-object p0
.end method

.method static provideBankAccountService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/bankaccount/BankAccountService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 105
    const-class v0, Lcom/squareup/server/bankaccount/BankAccountService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/bankaccount/BankAccountService;

    return-object p0
.end method

.method static provideBillCreationService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/bills/BillCreationService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 63
    const-class v0, Lcom/squareup/server/bills/BillCreationService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/bills/BillCreationService;

    return-object p0
.end method

.method static provideBillListService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/bills/BillListService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 69
    const-class v0, Lcom/squareup/server/bills/BillListService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/bills/BillListService;

    return-object p0
.end method

.method static provideBillRefundService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/payment/BillRefundService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 150
    const-class v0, Lcom/squareup/server/payment/BillRefundService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/payment/BillRefundService;

    return-object p0
.end method

.method static provideCashManagementService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/cashmanagement/CashManagementService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 74
    const-class v0, Lcom/squareup/server/cashmanagement/CashManagementService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/cashmanagement/CashManagementService;

    return-object p0
.end method

.method static provideCatalogConnectV2Service(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/catalog/CatalogConnectV2Service;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 201
    const-class v0, Lcom/squareup/server/catalog/CatalogConnectV2Service;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/catalog/CatalogConnectV2Service;

    return-object p0
.end method

.method static provideCatalogService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/catalog/CatalogService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 195
    const-class v0, Lcom/squareup/server/catalog/CatalogService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/catalog/CatalogService;

    return-object p0
.end method

.method static provideClientInvoiceService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/invoices/ClientInvoiceService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 89
    const-class v0, Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/invoices/ClientInvoiceService;

    return-object p0
.end method

.method static provideCouponsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/coupons/CouponsService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 121
    const-class v0, Lcom/squareup/server/coupons/CouponsService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/coupons/CouponsService;

    return-object p0
.end method

.method static provideDamagedReaderService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/DamagedReaderService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 156
    const-class v0, Lcom/squareup/server/DamagedReaderService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/DamagedReaderService;

    return-object p0
.end method

.method static provideDisputesService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/disputes/DisputesService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 234
    const-class v0, Lcom/squareup/server/disputes/DisputesService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/disputes/DisputesService;

    return-object p0
.end method

.method static provideEmployeesService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/employees/EmployeesService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 184
    const-class v0, Lcom/squareup/server/employees/EmployeesService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/employees/EmployeesService;

    return-object p0
.end method

.method static provideFelicaService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/felica/FelicaService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 240
    const-class v0, Lcom/squareup/server/felica/FelicaService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/felica/FelicaService;

    return-object p0
.end method

.method static provideGiftCardService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/payment/GiftCardService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 139
    const-class v0, Lcom/squareup/server/payment/GiftCardService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/payment/GiftCardService;

    return-object p0
.end method

.method static provideInventoryService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/inventory/InventoryService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 207
    const-class v0, Lcom/squareup/server/inventory/InventoryService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/inventory/InventoryService;

    return-object p0
.end method

.method static provideInvoiceFileAttachmentService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/invoices/InvoiceFileAttachmentService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 95
    const-class v0, Lcom/squareup/server/invoices/InvoiceFileAttachmentService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/invoices/InvoiceFileAttachmentService;

    return-object p0
.end method

.method static provideJediService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/help/JediService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 217
    const-class v0, Lcom/squareup/server/help/JediService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/help/JediService;

    return-object p0
.end method

.method static provideLogoutService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/account/LogoutService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 58
    const-class v0, Lcom/squareup/server/account/LogoutService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/LogoutService;

    return-object p0
.end method

.method static provideLoyaltyService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/loyalty/LoyaltyService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 127
    const-class v0, Lcom/squareup/server/loyalty/LoyaltyService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/loyalty/LoyaltyService;

    return-object p0
.end method

.method static provideMessagehubService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/messagehub/MessagehubService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 228
    const-class v0, Lcom/squareup/server/messagehub/MessagehubService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/messagehub/MessagehubService;

    return-object p0
.end method

.method static provideOnboardingService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/onboard/OnboardingService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 223
    const-class v0, Lcom/squareup/server/onboard/OnboardingService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/onboard/OnboardingService;

    return-object p0
.end method

.method static provideOrderHubService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/orderhub/OrderHubService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 115
    const-class v0, Lcom/squareup/server/orderhub/OrderHubService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/orderhub/OrderHubService;

    return-object p0
.end method

.method static provideOrdersService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/orders/OrdersService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 110
    const-class v0, Lcom/squareup/server/orders/OrdersService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/orders/OrdersService;

    return-object p0
.end method

.method static providePaperSignatureBatchService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/papersignature/PaperSignatureBatchService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 179
    const-class v0, Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    return-object p0
.end method

.method static providePaymentService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/payment/PaymentService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 144
    const-class v0, Lcom/squareup/server/payment/PaymentService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/payment/PaymentService;

    return-object p0
.end method

.method static providePublicStatusService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/PublicApiService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 173
    const-class v0, Lcom/squareup/server/PublicApiService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/PublicApiService;

    return-object p0
.end method

.method static provideReferralService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/referral/ReferralService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 167
    const-class v0, Lcom/squareup/server/referral/ReferralService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/referral/ReferralService;

    return-object p0
.end method

.method static provideReportCoredumpService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/ReportCoredumpService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 212
    const-class v0, Lcom/squareup/server/ReportCoredumpService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/ReportCoredumpService;

    return-object p0
.end method

.method static provideReportEmailService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/reporting/ReportEmailService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 161
    const-class v0, Lcom/squareup/server/reporting/ReportEmailService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/reporting/ReportEmailService;

    return-object p0
.end method

.method static provideRewardsBalanceService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/activation/RewardsBalanceService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 84
    const-class v0, Lcom/squareup/server/activation/RewardsBalanceService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/activation/RewardsBalanceService;

    return-object p0
.end method

.method static provideShippingAddressService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/shipping/ShippingAddressService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 79
    const-class v0, Lcom/squareup/server/shipping/ShippingAddressService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/shipping/ShippingAddressService;

    return-object p0
.end method

.method static provideSpeedTestService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/felica/FelicaSpeedTestService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 246
    const-class v0, Lcom/squareup/server/felica/FelicaSpeedTestService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/felica/FelicaSpeedTestService;

    return-object p0
.end method

.method static provideStoreAndForwardBillService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/bills/StoreAndForwardBillService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 133
    const-class v0, Lcom/squareup/server/bills/StoreAndForwardBillService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/bills/StoreAndForwardBillService;

    return-object p0
.end method

.method static provideTimecardsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/employees/TimecardsService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 189
    const-class v0, Lcom/squareup/server/employees/TimecardsService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/employees/TimecardsService;

    return-object p0
.end method

.method static provideTransactionsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/transactions/ProcessedTransactionsService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 252
    const-class v0, Lcom/squareup/server/transactions/ProcessedTransactionsService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/transactions/ProcessedTransactionsService;

    return-object p0
.end method

.method static provideTransferReportsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/reporting/TransferReportsService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 100
    const-class v0, Lcom/squareup/server/reporting/TransferReportsService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/reporting/TransferReportsService;

    return-object p0
.end method
