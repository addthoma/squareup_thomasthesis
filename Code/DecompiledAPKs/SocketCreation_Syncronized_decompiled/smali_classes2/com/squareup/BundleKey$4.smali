.class final Lcom/squareup/BundleKey$4;
.super Lcom/squareup/BundleKey;
.source "BundleKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/BundleKey;->serializable(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/BundleKey<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$type:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0

    .line 149
    iput-object p2, p0, Lcom/squareup/BundleKey$4;->val$type:Ljava/lang/Class;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/squareup/BundleKey;-><init>(Ljava/lang/String;Lcom/squareup/BundleKey$1;)V

    return-void
.end method


# virtual methods
.method public get(Landroid/content/Intent;)Ljava/io/Serializable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")TT;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/BundleKey$4;->val$type:Ljava/lang/Class;

    iget-object v1, p0, Lcom/squareup/BundleKey$4;->key:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/Serializable;

    return-object p1
.end method

.method public get(Landroid/os/Bundle;)Ljava/io/Serializable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/BundleKey$4;->val$type:Ljava/lang/Class;

    iget-object v1, p0, Lcom/squareup/BundleKey$4;->key:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/Serializable;

    return-object p1
.end method

.method public bridge synthetic get(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 0

    .line 149
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey$4;->get(Landroid/content/Intent;)Ljava/io/Serializable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 0

    .line 149
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey$4;->get(Landroid/os/Bundle;)Ljava/io/Serializable;

    move-result-object p1

    return-object p1
.end method

.method public put(Landroid/content/Intent;Ljava/io/Serializable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 166
    iget-object v0, p0, Lcom/squareup/BundleKey$4;->key:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public bridge synthetic put(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .line 149
    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/BundleKey$4;->put(Landroid/content/Intent;Ljava/io/Serializable;)V

    return-void
.end method

.method public put(Landroid/os/Bundle;Ljava/io/Serializable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 160
    iget-object v0, p0, Lcom/squareup/BundleKey$4;->key:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic put(Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 0

    .line 149
    check-cast p2, Ljava/io/Serializable;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/BundleKey$4;->put(Landroid/os/Bundle;Ljava/io/Serializable;)V

    return-void
.end method
