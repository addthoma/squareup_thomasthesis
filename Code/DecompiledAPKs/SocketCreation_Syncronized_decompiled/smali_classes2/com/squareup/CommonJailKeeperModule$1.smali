.class final Lcom/squareup/CommonJailKeeperModule$1;
.super Ljava/lang/Object;
.source "CommonJailKeeperModule.java"

# interfaces
.implements Lcom/squareup/jailkeeper/JailKeeperService;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/CommonJailKeeperModule;->provideTileAppearanceSettingsAsJailKeeperService(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/jailkeeper/JailKeeperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/CommonJailKeeperModule$1;->val$tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public preload()Lio/reactivex/Completable;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/CommonJailKeeperModule$1;->val$tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->loadTask()Lrx/Completable;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public reload()Lio/reactivex/Completable;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/CommonJailKeeperModule$1;->val$tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->loadTask()Lrx/Completable;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method
