.class public Lcom/squareup/caller/FailurePopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "FailurePopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;)V
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->showing()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 21
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;)V
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->showing()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/caller/FailurePopup;->createDialog(Lcom/squareup/register/widgets/FailureAlertDialogFactory;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/register/widgets/FailureAlertDialogFactory;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 18
    invoke-virtual {p0}, Lcom/squareup/caller/FailurePopup;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 19
    new-instance v0, Lcom/squareup/caller/-$$Lambda$FailurePopup$s4Nqouq2Z8lk_0Z5YCwX_E-RiwQ;

    invoke-direct {v0, p3}, Lcom/squareup/caller/-$$Lambda$FailurePopup$s4Nqouq2Z8lk_0Z5YCwX_E-RiwQ;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 24
    new-instance v1, Lcom/squareup/caller/-$$Lambda$FailurePopup$f4UQRcP1HwJbuAySauBIldlDmKE;

    invoke-direct {v1, p3}, Lcom/squareup/caller/-$$Lambda$FailurePopup$f4UQRcP1HwJbuAySauBIldlDmKE;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 29
    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
