.class public Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;
.super Ljava/lang/Object;
.source "ProgressAndFailureRxGlue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressAndFailureRxGlue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadBus;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;->badBus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method static synthetic lambda$forSimpleResponse$0(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    invoke-virtual {p0}, Lcom/squareup/server/SimpleResponse;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/server/SimpleResponse;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public forSimpleResponse(Lcom/squareup/caller/ProgressAndFailurePresenter;)Lcom/squareup/caller/ProgressAndFailureRxGlue;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/server/SimpleResponse;",
            ">(",
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "TT;>;)",
            "Lcom/squareup/caller/ProgressAndFailureRxGlue<",
            "TT;>;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/caller/ProgressAndFailureRxGlue;

    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;->badBus:Lcom/squareup/badbus/BadBus;

    sget-object v2, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$Factory$pF28mpejDGtvnFsTHoYREJx1-Lw;->INSTANCE:Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$Factory$pF28mpejDGtvnFsTHoYREJx1-Lw;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/caller/ProgressAndFailureRxGlue;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/caller/ProgressAndFailurePresenter;Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;Lcom/squareup/caller/ProgressAndFailureRxGlue$1;)V

    return-object v0
.end method
