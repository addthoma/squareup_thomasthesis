.class public Lcom/squareup/caller/ProgressAndFailurePresenter;
.super Lmortar/Presenter;
.source "ProgressAndFailurePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/caller/ProgressAndFailurePresenter$View;,
        Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lmortar/Presenter<",
        "Lcom/squareup/caller/ProgressAndFailurePresenter$View;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final messages:Lcom/squareup/request/RequestMessages;

.field private final showsFailure:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final showsProgress:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final stateKey:Ljava/lang/String;

.field private successResponse:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/request/RequestMessages;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/request/RequestMessages;",
            "Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener<",
            "TT;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->stateKey:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    .line 37
    new-instance p2, Lcom/squareup/caller/ProgressAndFailurePresenter$1;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/caller/ProgressAndFailurePresenter$1;-><init>(Lcom/squareup/caller/ProgressAndFailurePresenter;Ljava/lang/String;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V

    iput-object p2, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    .line 48
    new-instance p2, Lcom/squareup/caller/ProgressAndFailurePresenter$2;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/caller/ProgressAndFailurePresenter$2;-><init>(Lcom/squareup/caller/ProgressAndFailurePresenter;Ljava/lang/String;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V

    iput-object p2, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsFailure:Lcom/squareup/mortar/PopupPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/caller/ProgressAndFailurePresenter;)Ljava/lang/Object;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->successResponse:Ljava/lang/Object;

    return-object p0
.end method

.method private showFailure(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V
    .locals 1

    const/4 v0, 0x0

    .line 119
    iput-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->successResponse:Ljava/lang/Object;

    .line 120
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 121
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsFailure:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public beginProgress()V
    .locals 4

    .line 68
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->successResponse:Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsFailure:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 71
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v2}, Lcom/squareup/request/RequestMessages;->getLoadingMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    invoke-interface {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter$View;->getProgressPopup()Lcom/squareup/mortar/Popup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsFailure:Lcom/squareup/mortar/PopupPresenter;

    invoke-interface {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter$View;->getFailurePopup()Lcom/squareup/mortar/Popup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 64
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)Lmortar/bundler/BundleService;
    .locals 0

    .line 125
    invoke-interface {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter$View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->extractBundleService(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected getMortarBundleKey()Ljava/lang/String;
    .locals 2

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->stateKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClientError(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 87
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 88
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getDefaultFailureTitle()Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v1}, Lcom/squareup/request/RequestMessages;->getServerErrorMessage()Ljava/lang/String;

    move-result-object v1

    .line 91
    instance-of v2, p1, Lcom/squareup/server/SimpleResponse;

    if-eqz v2, :cond_0

    .line 92
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    .line 93
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    :cond_0
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {p1}, Lcom/squareup/request/RequestMessages;->getDismissButton()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->showFailure(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-void
.end method

.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 107
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 108
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getDefaultFailureTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 109
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getDismissButton()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->showFailure(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 56
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-interface {v0}, Lcom/squareup/caller/ProgressAndFailurePresenter$View;->getProgressPopup()Lcom/squareup/mortar/Popup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsFailure:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-interface {v0}, Lcom/squareup/caller/ProgressAndFailurePresenter$View;->getFailurePopup()Lcom/squareup/mortar/Popup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onNetworkError()V
    .locals 4

    .line 113
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 114
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getNetworkErrorTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    .line 115
    invoke-virtual {v1}, Lcom/squareup/request/RequestMessages;->getNetworkErrorMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v2}, Lcom/squareup/request/RequestMessages;->getDismissButton()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v3}, Lcom/squareup/request/RequestMessages;->getRetryButton()Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->showFailure(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-void
.end method

.method public onServerError()V
    .locals 4

    .line 101
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 102
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getServerErrorTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    .line 103
    invoke-virtual {v1}, Lcom/squareup/request/RequestMessages;->getServerErrorMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v2}, Lcom/squareup/request/RequestMessages;->getDismissButton()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {v3}, Lcom/squareup/request/RequestMessages;->getRetryButton()Ljava/lang/String;

    move-result-object v3

    .line 102
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->showFailure(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 75
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 76
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->successResponse:Ljava/lang/Object;

    .line 78
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->messages:Lcom/squareup/request/RequestMessages;

    invoke-virtual {p1}, Lcom/squareup/request/RequestMessages;->getLoadingCompleteMessage()Ljava/lang/String;

    move-result-object p1

    .line 79
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter;->showsProgress:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method
