.class public Lcom/squareup/caller/ProgressAndFailureRxGlue;
.super Ljava/lang/Object;
.source "ProgressAndFailureRxGlue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;,
        Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final presenter:Lcom/squareup/caller/ProgressAndFailurePresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final rejectedMessageParser:Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/caller/ProgressAndFailurePresenter;Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "TT;>;",
            "Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser<",
            "TT;>;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->badBus:Lcom/squareup/badbus/BadBus;

    .line 53
    iput-object p3, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->rejectedMessageParser:Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;

    .line 54
    iput-object p2, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->presenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/caller/ProgressAndFailurePresenter;Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;Lcom/squareup/caller/ProgressAndFailureRxGlue$1;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/caller/ProgressAndFailureRxGlue;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/caller/ProgressAndFailurePresenter;Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailurePresenter;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->presenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->rejectedMessageParser:Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/badbus/BadBus;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->badBus:Lcom/squareup/badbus/BadBus;

    return-object p0
.end method

.method private static ignoreFailure()Lrx/functions/Action3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Action3<",
            "TR;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 118
    sget-object v0, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$ux87GopnA2UIGR-np2k6TJfcCi0;->INSTANCE:Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$ux87GopnA2UIGR-np2k6TJfcCi0;

    return-object v0
.end method

.method static synthetic lambda$ignoreFailure$3(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public handler(Lio/reactivex/functions/Consumer;)Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/functions/Consumer<",
            "TT;>;)",
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    .line 69
    invoke-static {}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->ignoreFailure()Lrx/functions/Action3;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->handler(Lio/reactivex/functions/Consumer;Lrx/functions/Action3;)Lio/reactivex/functions/Consumer;

    move-result-object p1

    return-object p1
.end method

.method public handler(Lio/reactivex/functions/Consumer;Lrx/functions/Action3;)Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/functions/Consumer<",
            "TT;>;",
            "Lrx/functions/Action3<",
            "TT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    .line 77
    new-instance v0, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$v0SDIYSpDfGyoDx1n7DLlgrdB0A;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$v0SDIYSpDfGyoDx1n7DLlgrdB0A;-><init>(Lcom/squareup/caller/ProgressAndFailureRxGlue;Lio/reactivex/functions/Consumer;Lrx/functions/Action3;)V

    return-object v0
.end method

.method public synthetic lambda$handler$2$ProgressAndFailureRxGlue(Lio/reactivex/functions/Consumer;Lrx/functions/Action3;Lcom/squareup/receiving/ReceivedResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 77
    new-instance v0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;-><init>(Lcom/squareup/caller/ProgressAndFailureRxGlue;Lio/reactivex/functions/Consumer;Lrx/functions/Action3;)V

    invoke-virtual {p3, v0}, Lcom/squareup/receiving/ReceivedResponse;->map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Void;

    return-void
.end method

.method public synthetic lambda$null$0$ProgressAndFailureRxGlue(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 61
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue;->presenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-virtual {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->beginProgress()V

    return-void
.end method

.method public synthetic lambda$showRetrofitProgress$1$ProgressAndFailureRxGlue(Lio/reactivex/Single;)Lio/reactivex/SingleSource;
    .locals 1

    .line 61
    new-instance v0, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$k5O9YheNmdKYubYlrOyjDluUtU4;

    invoke-direct {v0, p0}, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$k5O9YheNmdKYubYlrOyjDluUtU4;-><init>(Lcom/squareup/caller/ProgressAndFailureRxGlue;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public showRetrofitProgress()Lio/reactivex/SingleTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/SingleTransformer<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$8SErI1W71OSNm9Z9rtwnU_y-hBs;

    invoke-direct {v0, p0}, Lcom/squareup/caller/-$$Lambda$ProgressAndFailureRxGlue$8SErI1W71OSNm9Z9rtwnU_y-hBs;-><init>(Lcom/squareup/caller/ProgressAndFailureRxGlue;)V

    return-object v0
.end method
