.class public Lcom/squareup/caller/BlockingPopupPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "BlockingPopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/Showing;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final DELAY_MS:I = 0x1f4


# instance fields
.field private delayedRunnnable:Ljava/lang/Runnable;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/caller/BlockingPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/caller/BlockingPopupPresenter;->delayedRunnnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/caller/BlockingPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 35
    :cond_0
    invoke-super {p0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/caller/BlockingPopupPresenter;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 0

    return-void
.end method

.method public show(Ljava/lang/Runnable;)V
    .locals 3

    .line 25
    new-instance v0, Lcom/squareup/ui/Showing;

    invoke-direct {v0}, Lcom/squareup/ui/Showing;-><init>()V

    invoke-super {p0, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    if-eqz p1, :cond_0

    .line 28
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/caller/-$$Lambda$V0-9sqUSIFOLX1gMMVX_zFcg3Ws;

    invoke-direct {v0, p1}, Lcom/squareup/caller/-$$Lambda$V0-9sqUSIFOLX1gMMVX_zFcg3Ws;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/squareup/caller/BlockingPopupPresenter;->delayedRunnnable:Ljava/lang/Runnable;

    .line 29
    iget-object p1, p0, Lcom/squareup/caller/BlockingPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/caller/BlockingPopupPresenter;->delayedRunnnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x1f4

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
