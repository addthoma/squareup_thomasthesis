.class Lcom/squareup/caller/ProgressAndFailurePresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "ProgressAndFailurePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/caller/ProgressAndFailurePresenter;-><init>(Ljava/lang/String;Lcom/squareup/request/RequestMessages;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/caller/ProgressPopup$Progress;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/caller/ProgressAndFailurePresenter;

.field final synthetic val$viewListener:Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;


# direct methods
.method constructor <init>(Lcom/squareup/caller/ProgressAndFailurePresenter;Ljava/lang/String;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter$1;->this$0:Lcom/squareup/caller/ProgressAndFailurePresenter;

    iput-object p3, p0, Lcom/squareup/caller/ProgressAndFailurePresenter$1;->val$viewListener:Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/PopupPresenter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 37
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter$1;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 1

    .line 39
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailurePresenter$1;->val$viewListener:Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;

    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailurePresenter$1;->this$0:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-static {v0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->access$000(Lcom/squareup/caller/ProgressAndFailurePresenter;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;->onProgressViewDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method
