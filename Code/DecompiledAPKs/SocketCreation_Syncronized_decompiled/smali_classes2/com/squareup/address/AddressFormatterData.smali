.class final Lcom/squareup/address/AddressFormatterData;
.super Ljava/lang/Object;
.source "AddressFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/AddressFormatterData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015BK\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/address/AddressFormatterData;",
        "",
        "addressLine1",
        "",
        "addressLine2",
        "addressLine3",
        "city",
        "state",
        "postalCode",
        "country",
        "Lcom/squareup/CountryCode;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V",
        "getAddressLine1",
        "()Ljava/lang/String;",
        "getAddressLine2",
        "getAddressLine3",
        "getCity",
        "getCountry",
        "()Lcom/squareup/CountryCode;",
        "getPostalCode",
        "getState",
        "Companion",
        "address_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/address/AddressFormatterData$Companion;


# instance fields
.field private final addressLine1:Ljava/lang/String;

.field private final addressLine2:Ljava/lang/String;

.field private final addressLine3:Ljava/lang/String;

.field private final city:Ljava/lang/String;

.field private final country:Lcom/squareup/CountryCode;

.field private final postalCode:Ljava/lang/String;

.field private final state:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/address/AddressFormatterData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/address/AddressFormatterData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/address/AddressFormatterData;->Companion:Lcom/squareup/address/AddressFormatterData$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/address/AddressFormatterData;->addressLine1:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/address/AddressFormatterData;->addressLine2:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/address/AddressFormatterData;->addressLine3:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/address/AddressFormatterData;->city:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/address/AddressFormatterData;->state:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/address/AddressFormatterData;->postalCode:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/address/AddressFormatterData;->country:Lcom/squareup/CountryCode;

    return-void
.end method


# virtual methods
.method public final getAddressLine1()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->addressLine1:Ljava/lang/String;

    return-object v0
.end method

.method public final getAddressLine2()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->addressLine2:Ljava/lang/String;

    return-object v0
.end method

.method public final getAddressLine3()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->addressLine3:Ljava/lang/String;

    return-object v0
.end method

.method public final getCity()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->city:Ljava/lang/String;

    return-object v0
.end method

.method public final getCountry()Lcom/squareup/CountryCode;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->country:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getPostalCode()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public final getState()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/address/AddressFormatterData;->state:Ljava/lang/String;

    return-object v0
.end method
