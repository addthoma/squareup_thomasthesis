.class Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;
.super Ljava/lang/Object;
.source "CountryFlagsAndNames.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/CountryFlagsAndNames;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CountryByNameComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/address/CountryFlagsAndNames;",
        ">;"
    }
.end annotation


# instance fields
.field collator:Ljava/text/Collator;

.field nameCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/address/CountryFlagsAndNames;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final resources:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->collator:Ljava/text/Collator;

    .line 221
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->nameCache:Ljava/util/Map;

    .line 225
    iput-object p1, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method private getCountryName(Lcom/squareup/address/CountryFlagsAndNames;)Ljava/lang/String;
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->nameCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->resources:Landroid/content/res/Resources;

    iget v1, p1, Lcom/squareup/address/CountryFlagsAndNames;->countryName:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 254
    iget-object v1, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->nameCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public compare(Lcom/squareup/address/CountryFlagsAndNames;Lcom/squareup/address/CountryFlagsAndNames;)I
    .locals 3

    .line 230
    sget-object v0, Lcom/squareup/address/CountryFlagsAndNames;->US:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v1, -0x1

    if-ne p1, v0, :cond_1

    .line 231
    sget-object p1, Lcom/squareup/address/CountryFlagsAndNames;->US:Lcom/squareup/address/CountryFlagsAndNames;

    if-ne p2, p1, :cond_0

    const/4 v1, 0x0

    :cond_0
    return v1

    .line 233
    :cond_1
    sget-object v0, Lcom/squareup/address/CountryFlagsAndNames;->US:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x1

    if-ne p2, v0, :cond_2

    return v2

    .line 237
    :cond_2
    iget-object v0, p1, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    iget-boolean v0, v0, Lcom/squareup/CountryCode;->hasPayments:Z

    if-eqz v0, :cond_3

    .line 238
    iget-object v0, p2, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    iget-boolean v0, v0, Lcom/squareup/CountryCode;->hasPayments:Z

    if-nez v0, :cond_3

    return v1

    .line 243
    :cond_3
    iget-object v0, p2, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    iget-boolean v0, v0, Lcom/squareup/CountryCode;->hasPayments:Z

    if-eqz v0, :cond_4

    return v2

    .line 247
    :cond_4
    iget-object v0, p0, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->collator:Ljava/text/Collator;

    invoke-direct {p0, p1}, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->getCountryName(Lcom/squareup/address/CountryFlagsAndNames;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->getCountryName(Lcom/squareup/address/CountryFlagsAndNames;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 219
    check-cast p1, Lcom/squareup/address/CountryFlagsAndNames;

    check-cast p2, Lcom/squareup/address/CountryFlagsAndNames;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;->compare(Lcom/squareup/address/CountryFlagsAndNames;Lcom/squareup/address/CountryFlagsAndNames;)I

    move-result p1

    return p1
.end method
