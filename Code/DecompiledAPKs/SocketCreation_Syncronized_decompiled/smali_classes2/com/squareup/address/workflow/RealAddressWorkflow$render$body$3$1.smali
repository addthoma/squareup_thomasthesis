.class final Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddressWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3;->invoke(Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/address/workflow/AddressState;",
        "Lcom/squareup/address/workflow/AddressState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/address/workflow/AddressState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $pickResult:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;


# direct methods
.method constructor <init>(Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;->$pickResult:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/address/workflow/AddressState;
    .locals 14

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;->$pickResult:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;

    .line 98
    sget-object v1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$NoCityPicked;->INSTANCE:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$NoCityPicked;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 101
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1b3

    const/4 v13, 0x0

    const-string v5, ""

    const-string v6, ""

    move-object v2, p1

    .line 98
    invoke-static/range {v2 .. v13}, Lcom/squareup/address/workflow/AddressState;->copy$default(Lcom/squareup/address/workflow/AddressState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    goto :goto_0

    .line 103
    :cond_0
    sget-object v1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$OtherCityPicked;->INSTANCE:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$OtherCityPicked;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 104
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1bf

    const/4 v13, 0x0

    move-object v2, p1

    .line 103
    invoke-static/range {v2 .. v13}, Lcom/squareup/address/workflow/AddressState;->copy$default(Lcom/squareup/address/workflow/AddressState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    goto :goto_0

    .line 106
    :cond_1
    instance-of v0, v0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;->$pickResult:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;

    check-cast v0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;

    invoke-virtual {v0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;->getCity()Ljava/lang/String;

    move-result-object v4

    .line 108
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;->$pickResult:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;

    check-cast v0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;

    invoke-virtual {v0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;->getState()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 109
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;->$pickResult:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;

    check-cast v0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;

    invoke-virtual {v0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;->getPosition()I

    move-result v9

    const/4 v10, 0x0

    const/16 v11, 0x173

    const/4 v12, 0x0

    move-object v1, p1

    .line 106
    invoke-static/range {v1 .. v12}, Lcom/squareup/address/workflow/AddressState;->copy$default(Lcom/squareup/address/workflow/AddressState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/AddressState;

    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3$1;->invoke(Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    return-object p1
.end method
