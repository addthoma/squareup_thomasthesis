.class public final Lcom/squareup/address/AddressLayout_MembersInjector;
.super Ljava/lang/Object;
.source "AddressLayout_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/address/AddressLayout;",
        ">;"
    }
.end annotation


# instance fields
.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/address/AddressLayoutRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/address/AddressLayoutRunner;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/address/AddressLayout_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/address/AddressLayoutRunner;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/address/AddressLayout;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/address/AddressLayout_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/address/AddressLayout_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectRunner(Lcom/squareup/address/AddressLayout;Lcom/squareup/address/AddressLayoutRunner;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/address/AddressLayout;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/address/AddressLayout_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayoutRunner;

    invoke-static {p1, v0}, Lcom/squareup/address/AddressLayout_MembersInjector;->injectRunner(Lcom/squareup/address/AddressLayout;Lcom/squareup/address/AddressLayoutRunner;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/address/AddressLayout;

    invoke-virtual {p0, p1}, Lcom/squareup/address/AddressLayout_MembersInjector;->injectMembers(Lcom/squareup/address/AddressLayout;)V

    return-void
.end method
