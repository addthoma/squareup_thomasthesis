.class public final Lcom/squareup/address/AddressLayoutRunner_Factory;
.super Ljava/lang/Object;
.source "AddressLayoutRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/address/AddressLayoutRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final addressServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/address/AddressService;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/address/AddressService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->addressServiceProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/address/AddressLayoutRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/address/AddressService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/address/AddressLayoutRunner_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/address/AddressLayoutRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/address/AddressLayoutRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/address/AddressService;Lio/reactivex/Scheduler;Lcom/squareup/CountryCode;Lflow/Flow;)Lcom/squareup/address/AddressLayoutRunner;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/address/AddressLayoutRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/address/AddressLayoutRunner;-><init>(Lcom/squareup/server/address/AddressService;Lio/reactivex/Scheduler;Lcom/squareup/CountryCode;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/address/AddressLayoutRunner;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->addressServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/address/AddressService;

    iget-object v1, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/CountryCode;

    iget-object v3, p0, Lcom/squareup/address/AddressLayoutRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/Flow;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/address/AddressLayoutRunner_Factory;->newInstance(Lcom/squareup/server/address/AddressService;Lio/reactivex/Scheduler;Lcom/squareup/CountryCode;Lflow/Flow;)Lcom/squareup/address/AddressLayoutRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayoutRunner_Factory;->get()Lcom/squareup/address/AddressLayoutRunner;

    move-result-object v0

    return-object v0
.end method
