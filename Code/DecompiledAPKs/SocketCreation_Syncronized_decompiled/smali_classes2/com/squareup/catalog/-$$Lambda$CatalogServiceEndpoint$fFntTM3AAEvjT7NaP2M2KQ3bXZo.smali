.class public final synthetic Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# instance fields
.field private final synthetic f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

.field private final synthetic f$1:Z

.field private final synthetic f$2:Z

.field private final synthetic f$3:Lcom/squareup/catalog/EditItemVariationsState;

.field private final synthetic f$4:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private final synthetic f$5:Lcom/squareup/cogs/Cogs;

.field private final synthetic f$6:Ljava/lang/String;

.field private final synthetic f$7:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/catalog/CatalogServiceEndpoint;ZZLcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iput-boolean p2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$1:Z

    iput-boolean p3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$2:Z

    iput-object p4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$3:Lcom/squareup/catalog/EditItemVariationsState;

    iput-object p5, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$4:Lcom/squareup/shared/catalog/models/CatalogItem;

    iput-object p6, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$5:Lcom/squareup/cogs/Cogs;

    iput-object p7, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$6:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$7:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 9

    iget-object v0, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-boolean v1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$1:Z

    iget-boolean v2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$2:Z

    iget-object v3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$3:Lcom/squareup/catalog/EditItemVariationsState;

    iget-object v4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$4:Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v5, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$5:Lcom/squareup/cogs/Cogs;

    iget-object v6, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$6:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$fFntTM3AAEvjT7NaP2M2KQ3bXZo;->f$7:Ljava/lang/String;

    move-object v8, p1

    invoke-virtual/range {v0 .. v8}, Lcom/squareup/catalog/CatalogServiceEndpoint;->lambda$updateItemOptionsOnItemAndSaveVariations$3$CatalogServiceEndpoint(ZZLcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
