.class public interface abstract Lcom/squareup/catalog/AttributeEditor;
.super Ljava/lang/Object;
.source "AttributeEditor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final BOOLEAN_ATTRIBUTE_EDITOR_DEFAULT_FALSE:Lcom/squareup/catalog/AttributeEditor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/AttributeEditor<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final INTEGER_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/AttributeEditor<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final LONG_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/AttributeEditor<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/AttributeEditor<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/catalog/AttributeEditor$1;

    invoke-direct {v0}, Lcom/squareup/catalog/AttributeEditor$1;-><init>()V

    sput-object v0, Lcom/squareup/catalog/AttributeEditor;->STRING_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    .line 25
    new-instance v0, Lcom/squareup/catalog/AttributeEditor$2;

    invoke-direct {v0}, Lcom/squareup/catalog/AttributeEditor$2;-><init>()V

    sput-object v0, Lcom/squareup/catalog/AttributeEditor;->LONG_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    .line 35
    new-instance v0, Lcom/squareup/catalog/AttributeEditor$3;

    invoke-direct {v0}, Lcom/squareup/catalog/AttributeEditor$3;-><init>()V

    sput-object v0, Lcom/squareup/catalog/AttributeEditor;->BOOLEAN_ATTRIBUTE_EDITOR_DEFAULT_FALSE:Lcom/squareup/catalog/AttributeEditor;

    .line 49
    new-instance v0, Lcom/squareup/catalog/AttributeEditor$4;

    invoke-direct {v0}, Lcom/squareup/catalog/AttributeEditor$4;-><init>()V

    sput-object v0, Lcom/squareup/catalog/AttributeEditor;->INTEGER_ATTRIBUTE_EDITOR:Lcom/squareup/catalog/AttributeEditor;

    return-void
.end method


# virtual methods
.method public abstract getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/Attribute$Builder;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/Attribute$Builder;",
            "TT;)V"
        }
    .end annotation
.end method
