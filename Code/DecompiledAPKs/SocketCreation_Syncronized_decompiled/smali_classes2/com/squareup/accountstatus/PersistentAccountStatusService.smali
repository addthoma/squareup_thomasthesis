.class public Lcom/squareup/accountstatus/PersistentAccountStatusService;
.super Ljava/lang/Object;
.source "PersistentAccountStatusService.java"

# interfaces
.implements Lcom/squareup/accountstatus/QuietServerPreferences;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountstatus/PersistentAccountStatusService$ASREvent;
    }
.end annotation


# instance fields
.field private final accountStatusResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

.field private final accountStatusService:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/util/Clock;

.field private final crashReporter:Lcom/squareup/log/CrashReporter;

.field private final deviceId:Lcom/squareup/settings/DeviceIdProvider;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private initialized:Z

.field private final serverTimeMinusElapsedRealtime:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/DeviceIdProvider;Ldagger/Lazy;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/Clock;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Ldagger/Lazy<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;",
            "Lcom/squareup/log/CrashReporter;",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->serverTimeMinusElapsedRealtime:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->initialized:Z

    .line 62
    iput-object p1, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->deviceId:Lcom/squareup/settings/DeviceIdProvider;

    .line 63
    iput-object p2, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusService:Ldagger/Lazy;

    .line 64
    iput-object p3, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 65
    iput-object p4, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    .line 66
    iput-object p5, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->crashReporter:Lcom/squareup/log/CrashReporter;

    .line 67
    iput-object p6, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->clock:Lcom/squareup/util/Clock;

    .line 68
    iput-object p7, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->sessionTokenProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private configureCrashReporter(Lcom/squareup/server/account/protos/User;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->crashReporter:Lcom/squareup/log/CrashReporter;

    iget-object p1, p1, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/log/CrashReporter;->setUserIdentifier(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_0
    iget-object p1, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->crashReporter:Lcom/squareup/log/CrashReporter;

    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->deviceId:Lcom/squareup/settings/DeviceIdProvider;

    invoke-interface {v0}, Lcom/squareup/settings/DeviceIdProvider;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/log/CrashReporter;->setUserIdentifier(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$setPreferencesQuietly$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 163
    sget-object v0, Lcom/squareup/accountstatus/-$$Lambda$Cr7--fit3zZC6H5ZRdmRiTpdkN0;->INSTANCE:Lcom/squareup/accountstatus/-$$Lambda$Cr7--fit3zZC6H5ZRdmRiTpdkN0;

    invoke-virtual {p0, v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p0

    return-object p0
.end method

.method private ohsnap(Ljava/lang/String;)V
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p1, "; new status "

    .line 264
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/squareup/server/accountstatus/AccountStatusService;->EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-ne v0, p1, :cond_0

    const-string p1, "=="

    goto :goto_0

    :cond_0
    const-string p1, "!="

    .line 265
    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " UNSET; "

    .line 266
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    iget-object p1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz p1, :cond_1

    .line 268
    iget-object p1, p1, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    const-string v0, "; user token is "

    .line 269
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_2

    const-string p1, ""

    goto :goto_2

    :cond_2
    const-string p1, "not "

    :goto_2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "set"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object p1, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/accountstatus/PersistentAccountStatusService$ASREvent;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/accountstatus/PersistentAccountStatusService$ASREvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method private onStatusResponseReceived(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/server/SquareCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            "Lcom/squareup/server/SquareCallback<",
            "-",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .line 245
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->clearCache()V

    return-void

    .line 251
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->updateCache(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    if-eqz p2, :cond_1

    .line 253
    invoke-virtual {p2, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    .line 257
    :cond_1
    invoke-direct {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postStatusUpdateEvent()V

    .line 258
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postServerTimeUpdateEvent(Ljava/lang/String;)V

    return-void
.end method

.method private postServerTimeUpdateEvent(Ljava/lang/String;)V
    .locals 5

    .line 131
    :try_start_0
    invoke-static {p1}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 132
    iget-object v2, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->serverTimeMinusElapsedRealtime:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v3, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v3}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to parse server time \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\""

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private postStatusUpdateEvent()V
    .locals 2

    .line 125
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 126
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v1}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private privateObserveStatus(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;>;"
        }
    .end annotation

    .line 207
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusService:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/accountstatus/AccountStatusService;

    .line 208
    invoke-static {p1}, Lcom/squareup/server/account/AuthorizationHeaders;->authorizationHeaderValueFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 207
    invoke-static {v0, p1}, Lcom/squareup/server/accountstatus/AccountStatusServiceKt;->getAccountStatusWithDefaults(Lcom/squareup/server/accountstatus/AccountStatusService;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public accountStatusResponse()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public clearCache()V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->clear()V

    const-string v0, "cache cleared"

    .line 226
    invoke-direct {p0, v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->ohsnap(Ljava/lang/String;)V

    return-void
.end method

.method public getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public init()V
    .locals 2

    .line 76
    iget-boolean v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->initialized:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 77
    iput-boolean v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->initialized:Z

    .line 79
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0, p0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->init(Lcom/squareup/accountstatus/QuietServerPreferences;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/accountstatus/AccountStatusService;->EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eq v0, v1, :cond_0

    .line 82
    invoke-direct {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postStatusUpdateEvent()V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    invoke-direct {p0, v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->configureCrashReporter(Lcom/squareup/server/account/protos/User;)V

    return-void

    .line 76
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic lambda$observeStatus$3$PersistentAccountStatusService(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 199
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 201
    invoke-direct {p0, p1, v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->onStatusResponseReceived(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/server/SquareCallback;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$setPreferencesQuietly$1$PersistentAccountStatusService(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 165
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/Preferences;

    if-eqz p1, :cond_0

    .line 169
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->get()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    .line 170
    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    .line 172
    invoke-virtual {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->updateCache(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 175
    invoke-direct {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postStatusUpdateEvent()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$status$2$PersistentAccountStatusService(Lcom/squareup/server/SquareCallback;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 187
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0, v0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->onStatusResponseReceived(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/server/SquareCallback;)V

    goto :goto_0

    .line 191
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/server/SquareCallback;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    :goto_0
    return-void
.end method

.method public observeStatus(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;>;"
        }
    .end annotation

    .line 197
    invoke-direct {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->privateObserveStatus(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$E-YAIsasZ-8Ig3u2QTtgmYxK7v4;

    invoke-direct {v0, p0}, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$E-YAIsasZ-8Ig3u2QTtgmYxK7v4;-><init>(Lcom/squareup/accountstatus/PersistentAccountStatusService;)V

    .line 198
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public onLoggedIn()V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->onLoggedIn()V

    return-void
.end method

.method public onLoginResponseReceived(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    .line 232
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->clearCache()V

    return-void

    .line 237
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->updateCache(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 238
    invoke-direct {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postStatusUpdateEvent()V

    .line 239
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postServerTimeUpdateEvent(Ljava/lang/String;)V

    return-void
.end method

.method public serverTimeMinusElapsedRealtime()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->serverTimeMinusElapsedRealtime:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 146
    iget-boolean v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->initialized:Z

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    .line 149
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;

    move-result-object p1

    .line 151
    invoke-direct {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->postStatusUpdateEvent()V

    return-object p1

    .line 146
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "You forgot to call init()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setPreferencesQuietly(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusService:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/accountstatus/AccountStatusService;

    iget-object v1, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->sessionTokenProvider:Ljavax/inject/Provider;

    .line 161
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/server/account/AuthorizationHeaders;->authorizationHeaderValueFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/server/accountstatus/AccountStatusService;->setPreferences(Ljava/lang/String;Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/accountstatus/AccountStatusStandardResponse;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/server/accountstatus/AccountStatusStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$FB_LzBRptBHyylGcnp4G2ob4EoU;->INSTANCE:Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$FB_LzBRptBHyylGcnp4G2ob4EoU;

    .line 163
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$XsbZra6_IQ3KSRWNsXv326TGrHk;

    invoke-direct {v0, p0}, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$XsbZra6_IQ3KSRWNsXv326TGrHk;-><init>(Lcom/squareup/accountstatus/PersistentAccountStatusService;)V

    .line 164
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public status(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->sessionTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->privateObserveStatus(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$bDVtRs4KY-wjVNhbPpfJ8U9EMtg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/accountstatus/-$$Lambda$PersistentAccountStatusService$bDVtRs4KY-wjVNhbPpfJ8U9EMtg;-><init>(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/server/SquareCallback;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method updateCache(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponseCache:Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;->put(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 219
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    invoke-direct {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->configureCrashReporter(Lcom/squareup/server/account/protos/User;)V

    const-string p1, "cache replaced"

    .line 220
    invoke-direct {p0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->ohsnap(Ljava/lang/String;)V

    return-void
.end method
