.class public final Lcom/squareup/blecoroutines/GattCallbackBroker;
.super Ljava/lang/Object;
.source "GattCallbackBroker.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGattCallbackBroker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GattCallbackBroker.kt\ncom/squareup/blecoroutines/GattCallbackBroker\n+ 2 Select.kt\nkotlinx/coroutines/selects/SelectKt\n*L\n1#1,176:1\n191#2,9:177\n*E\n*S KotlinDebug\n*F\n+ 1 GattCallbackBroker.kt\ncom/squareup/blecoroutines/GattCallbackBroker\n*L\n89#1,9:177\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0001\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u001c\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001a2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bJ\u000e\u0010 \u001a\u00020\t2\u0006\u0010!\u001a\u00020\u000eJ-\u0010\"\u001a\u0002H#\"\n\u0008\u0000\u0010#\u0018\u0001*\u00020\u000e2\u000e\u0008\u0008\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\t0%H\u0086H\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010&J7\u0010\"\u001a\u0002H#\"\u0008\u0008\u0000\u0010#*\u00020\u000e2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u0002H#0(2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\t0%H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010)J\u001c\u0010*\u001a\u00020\t2\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\t0\rH\u0002J\u0008\u0010,\u001a\u00020\tH\u0002R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\t\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00108F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R \u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u001b0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/GattCallbackBroker;",
        "",
        "deviceMacAddress",
        "",
        "onDisconnection",
        "Lkotlinx/coroutines/CompletableDeferred;",
        "",
        "(Ljava/lang/String;Lkotlinx/coroutines/CompletableDeferred;)V",
        "_onConnection",
        "",
        "_rssiChannel",
        "Lkotlinx/coroutines/channels/Channel;",
        "delegate",
        "Lkotlin/Function1;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
        "onConnection",
        "Lkotlinx/coroutines/Deferred;",
        "getOnConnection",
        "()Lkotlinx/coroutines/Deferred;",
        "getOnDisconnection",
        "()Lkotlinx/coroutines/CompletableDeferred;",
        "onReadRemoteRssi",
        "getOnReadRemoteRssi",
        "()Lkotlinx/coroutines/channels/Channel;",
        "subscribedCharacteristics",
        "",
        "Ljava/util/UUID;",
        "Lkotlinx/coroutines/channels/SendChannel;",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "addSubscription",
        "uuid",
        "channel",
        "handle",
        "data",
        "runAndWaitForCallback",
        "T",
        "operation",
        "Lkotlin/Function0;",
        "(Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "clazz",
        "Lkotlin/reflect/KClass;",
        "(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "setDelegateIfConnected",
        "callback",
        "unsetDelegate",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _onConnection:Lkotlinx/coroutines/CompletableDeferred;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/CompletableDeferred<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final _rssiChannel:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private delegate:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceMacAddress:Ljava/lang/String;

.field private final onDisconnection:Lkotlinx/coroutines/CompletableDeferred;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/CompletableDeferred<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final subscribedCharacteristics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/util/UUID;",
            "Lkotlinx/coroutines/channels/SendChannel<",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lkotlinx/coroutines/CompletableDeferred;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlinx/coroutines/CompletableDeferred<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "deviceMacAddress"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDisconnection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->deviceMacAddress:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    .line 46
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->subscribedCharacteristics:Ljava/util/Map;

    const/4 p1, 0x0

    const/4 p2, 0x1

    .line 47
    invoke-static {p1, p2, p1}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->_onConnection:Lkotlinx/coroutines/CompletableDeferred;

    const p1, 0x7fffffff

    .line 48
    invoke-static {p1}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->_rssiChannel:Lkotlinx/coroutines/channels/Channel;

    return-void
.end method

.method public static final synthetic access$unsetDelegate(Lcom/squareup/blecoroutines/GattCallbackBroker;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->unsetDelegate()V

    return-void
.end method

.method private final setDelegateIfConnected(Lkotlin/jvm/functions/Function1;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->delegate:Lkotlin/jvm/functions/Function1;

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    invoke-interface {v0}, Lkotlinx/coroutines/CompletableDeferred;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    iput-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->delegate:Lkotlin/jvm/functions/Function1;

    return-void

    .line 172
    :cond_0
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    sget-object v2, Lcom/squareup/blecoroutines/Event;->ALREADY_DISCONNECTED:Lcom/squareup/blecoroutines/Event;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7e

    const/4 v10, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v10}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 171
    :cond_1
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    sget-object v1, Lcom/squareup/blecoroutines/Event;->BLE_BUSY:Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7e

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final unsetDelegate()V
    .locals 11

    .line 164
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->delegate:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 167
    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->delegate:Lkotlin/jvm/functions/Function1;

    return-void

    .line 165
    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    sget-object v2, Lcom/squareup/blecoroutines/Event;->INTERNAL_BLE_ERROR:Lcom/squareup/blecoroutines/Event;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    const-string v8, "Can\'t unset delegate; already unset!"

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final addSubscription(Ljava/util/UUID;Lkotlinx/coroutines/channels/SendChannel;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Lkotlinx/coroutines/channels/SendChannel<",
            "-",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "uuid"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->subscribedCharacteristics:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->subscribedCharacteristics:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 106
    :cond_0
    new-instance p2, Lcom/squareup/blecoroutines/BleError;

    sget-object v2, Lcom/squareup/blecoroutines/Event;->ALREADY_SUBSCRIBED:Lcom/squareup/blecoroutines/Event;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7a

    const/4 v10, 0x0

    move-object v1, p2

    move-object v4, p1

    invoke-direct/range {v1 .. v10}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final getOnConnection()Lkotlinx/coroutines/Deferred;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/Deferred<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->_onConnection:Lkotlinx/coroutines/CompletableDeferred;

    check-cast v0, Lkotlinx/coroutines/Deferred;

    return-object v0
.end method

.method public final getOnDisconnection()Lkotlinx/coroutines/CompletableDeferred;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/CompletableDeferred<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    return-object v0
.end method

.method public final getOnReadRemoteRssi()Lkotlinx/coroutines/channels/Channel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/Channel<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->_rssiChannel:Lkotlinx/coroutines/channels/Channel;

    return-object v0
.end method

.method public final handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V
    .locals 5

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p1}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 122
    :goto_0
    iget-object v1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->deviceMacAddress:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    .line 123
    iget-object v1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->deviceMacAddress:Ljava/lang/String;

    aput-object v1, p1, v3

    aput-object v0, p1, v2

    const-string v0, "Callback from wrong device! Expected %s but was for %s"

    invoke-static {v0, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 127
    :cond_1
    instance-of v0, p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;

    if-nez v0, :cond_2

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    const-string v4, "handle: %s"

    invoke-static {v4, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    :cond_2
    instance-of v1, p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    if-eqz v1, :cond_4

    .line 130
    check-cast p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    invoke-virtual {p1}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 131
    iget-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->_onConnection:Lkotlinx/coroutines/CompletableDeferred;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {p1, v0}, Lkotlinx/coroutines/CompletableDeferred;->complete(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    new-array p1, v3, [Ljava/lang/Object;

    const-string v0, "Received connection callback but already connected!"

    .line 132
    invoke-static {v0, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->onDisconnection:Lkotlinx/coroutines/CompletableDeferred;

    invoke-virtual {p1}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getStatus()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlinx/coroutines/CompletableDeferred;->complete(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    new-array p1, v3, [Ljava/lang/Object;

    const-string v0, "Received disconnected callback but already disconnected!"

    .line 136
    invoke-static {v0, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 140
    :cond_4
    instance-of v1, p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;

    if-eqz v1, :cond_6

    .line 141
    move-object v0, p1

    check-cast v0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;

    invoke-virtual {v0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    .line 142
    iget-object v4, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->subscribedCharacteristics:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlinx/coroutines/channels/SendChannel;

    if-nez v1, :cond_5

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string p1, "no subscription for %s!"

    .line 144
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 147
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p1

    invoke-interface {v1, p1}, Lkotlinx/coroutines/channels/SendChannel;->offer(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    new-array p1, v3, [Ljava/lang/Object;

    const-string v0, "Failed to offer data to send channel!"

    .line 149
    invoke-static {v0, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    if-eqz v0, :cond_7

    .line 153
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->_rssiChannel:Lkotlinx/coroutines/channels/Channel;

    check-cast p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;

    invoke-virtual {p1}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;->getRssi()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    new-array p1, v3, [Ljava/lang/Object;

    const-string v0, "Failed to offer data to rssi channel!"

    .line 154
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 158
    :cond_7
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker;->delegate:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_8

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    if-eqz v0, :cond_8

    goto :goto_1

    :cond_8
    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string p1, "no delegate to handle %s!"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :cond_9
    :goto_1
    return-void
.end method

.method public final synthetic runAndWaitForCallback(Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const/4 v0, 0x4

    const-string v1, "T"

    .line 61
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->mark(I)V

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/blecoroutines/GattCallbackBroker;->runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x1

    invoke-static {p2}, Lkotlin/jvm/internal/InlineMarker;->mark(I)V

    return-object p1
.end method

.method final synthetic runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 68
    invoke-static {v0, v1, v0}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v1

    .line 69
    new-instance v2, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;

    invoke-direct {v2, p0, p1, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;-><init>(Lcom/squareup/blecoroutines/GattCallbackBroker;Lkotlin/reflect/KClass;Lkotlinx/coroutines/CompletableDeferred;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v2}, Lcom/squareup/blecoroutines/GattCallbackBroker;->setDelegateIfConnected(Lkotlin/jvm/functions/Function1;)V

    .line 81
    :try_start_0
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;
    :try_end_0
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    new-instance p1, Lkotlinx/coroutines/selects/SelectBuilderImpl;

    invoke-direct {p1, p3}, Lkotlinx/coroutines/selects/SelectBuilderImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 180
    :try_start_1
    move-object p2, p1

    check-cast p2, Lkotlinx/coroutines/selects/SelectBuilder;

    .line 90
    invoke-interface {v1}, Lkotlinx/coroutines/CompletableDeferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v1

    new-instance v2, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$4$1;

    invoke-direct {v2, v0}, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$4$1;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-interface {p2, v1, v2}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->getOnDisconnection()Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v1

    invoke-interface {v1}, Lkotlinx/coroutines/CompletableDeferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v1

    new-instance v2, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$4$2;

    invoke-direct {v2, v0}, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$4$2;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-interface {p2, v1, v2}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p2

    .line 182
    invoke-virtual {p1, p2}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->handleBuilderException(Ljava/lang/Throwable;)V

    .line 184
    :goto_0
    invoke-virtual {p1}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->getResult()Ljava/lang/Object;

    move-result-object p1

    .line 177
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object p2

    if-ne p1, p2, :cond_0

    invoke-static {p3}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V

    :cond_0
    return-object p1

    :catch_0
    move-exception p1

    .line 85
    invoke-direct {p0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->unsetDelegate()V

    .line 86
    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
