.class final Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "RealConnectable.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blecoroutines/RealConnectable;->connectGatt(Landroid/content/Context;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/blecoroutines/RealConnection;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealConnectable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealConnectable.kt\ncom/squareup/blecoroutines/RealConnectable$connectGatt$2\n+ 2 Select.kt\nkotlinx/coroutines/selects/SelectKt\n*L\n1#1,79:1\n191#2,9:80\n*E\n*S KotlinDebug\n*F\n+ 1 RealConnectable.kt\ncom/squareup/blecoroutines/RealConnectable$connectGatt$2\n*L\n38#1,9:80\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/blecoroutines/RealConnection;",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.blecoroutines.RealConnectable$connectGatt$2"
    f = "RealConnectable.kt"
    i = {
        0x0,
        0x0,
        0x0
    }
    l = {
        0x50
    }
    m = "invokeSuspend"
    n = {
        "$this$withContext",
        "callback",
        "gatt"
    }
    s = {
        "L$0",
        "L$1",
        "L$2"
    }
.end annotation


# instance fields
.field final synthetic $autoConnect:Z

.field final synthetic $context:Landroid/content/Context;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/blecoroutines/RealConnectable;


# direct methods
.method constructor <init>(Lcom/squareup/blecoroutines/RealConnectable;Landroid/content/Context;ZLkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    iput-object p2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->$context:Landroid/content/Context;

    iput-boolean p3, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->$autoConnect:Z

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    iget-object v1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    iget-object v2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->$context:Landroid/content/Context;

    iget-boolean v3, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->$autoConnect:Z

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;-><init>(Lcom/squareup/blecoroutines/RealConnectable;Landroid/content/Context;ZLkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 31
    iget v1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$3:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;

    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$2:Ljava/lang/Object;

    check-cast v0, Landroid/bluetooth/BluetoothGatt;

    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$1:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/DelegatingGattCallback;

    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 88
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 31
    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 32
    iget-object v1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v1}, Lcom/squareup/blecoroutines/RealConnectable;->access$getBluetoothGatt$p(Lcom/squareup/blecoroutines/RealConnectable;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    if-nez v1, :cond_5

    .line 33
    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback;

    iget-object v3, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v3}, Lcom/squareup/blecoroutines/RealConnectable;->access$getCallbackBroker$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/blecoroutines/GattCallbackBroker;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v4}, Lcom/squareup/blecoroutines/RealConnectable;->access$getTmnTimings$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/squareup/blecoroutines/DelegatingGattCallback;-><init>(Lcom/squareup/blecoroutines/GattCallbackBroker;Lcom/squareup/tmn/TmnTimings;)V

    .line 35
    iget-object v3, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v3}, Lcom/squareup/blecoroutines/RealConnectable;->access$getDevice$p(Lcom/squareup/blecoroutines/RealConnectable;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->$context:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->$autoConnect:Z

    move-object v6, v1

    check-cast v6, Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v3, v4, v5, v6}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 37
    iget-object v4, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v4, v3}, Lcom/squareup/blecoroutines/RealConnectable;->access$setBluetoothGatt$p(Lcom/squareup/blecoroutines/RealConnectable;Landroid/bluetooth/BluetoothGatt;)V

    .line 80
    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$1:Ljava/lang/Object;

    iput-object v3, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$2:Ljava/lang/Object;

    iput-object p0, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->L$3:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->label:I

    .line 81
    new-instance p1, Lkotlinx/coroutines/selects/SelectBuilderImpl;

    invoke-direct {p1, p0}, Lkotlinx/coroutines/selects/SelectBuilderImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 83
    :try_start_0
    move-object v1, p1

    check-cast v1, Lkotlinx/coroutines/selects/SelectBuilder;

    .line 39
    iget-object v2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v2}, Lcom/squareup/blecoroutines/RealConnectable;->access$getCallbackBroker$p(Lcom/squareup/blecoroutines/RealConnectable;)Lcom/squareup/blecoroutines/GattCallbackBroker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/blecoroutines/GattCallbackBroker;->getOnConnection()Lkotlinx/coroutines/Deferred;

    move-result-object v2

    invoke-interface {v2}, Lkotlinx/coroutines/Deferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v2

    new-instance v4, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;

    const/4 v5, 0x0

    invoke-direct {v4, v5, p0, v3}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$invokeSuspend$$inlined$select$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;Landroid/bluetooth/BluetoothGatt;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-interface {v1, v2, v4}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 42
    iget-object v2, p0, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2;->this$0:Lcom/squareup/blecoroutines/RealConnectable;

    invoke-static {v2}, Lcom/squareup/blecoroutines/RealConnectable;->access$getOnDisconnection$p(Lcom/squareup/blecoroutines/RealConnectable;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v2

    invoke-interface {v2}, Lkotlinx/coroutines/CompletableDeferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v2

    new-instance v3, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$1$2;

    invoke-direct {v3, v5}, Lcom/squareup/blecoroutines/RealConnectable$connectGatt$2$1$2;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-interface {v1, v2, v3}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 85
    invoke-virtual {p1, v1}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->handleBuilderException(Ljava/lang/Throwable;)V

    .line 87
    :goto_0
    invoke-virtual {p1}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->getResult()Ljava/lang/Object;

    move-result-object p1

    .line 80
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_2

    invoke-static {p0}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V

    :cond_2
    if-ne p1, v0, :cond_3

    return-object v0

    :cond_3
    :goto_1
    return-object p1

    .line 36
    :cond_4
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    sget-object v1, Lcom/squareup/blecoroutines/Event;->CONNECTION_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7e

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 32
    :cond_5
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    sget-object v1, Lcom/squareup/blecoroutines/Event;->ALREADY_CONNECTING:Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7e

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
