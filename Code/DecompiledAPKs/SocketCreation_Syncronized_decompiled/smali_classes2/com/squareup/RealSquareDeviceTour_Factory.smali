.class public final Lcom/squareup/RealSquareDeviceTour_Factory;
.super Ljava/lang/Object;
.source "RealSquareDeviceTour_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/RealSquareDeviceTour;",
        ">;"
    }
.end annotation


# instance fields
.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final squareDeviceTourRedirectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourRedirector;",
            ">;"
        }
    .end annotation
.end field

.field private final squareDeviceTourSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourRedirector;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/RealSquareDeviceTour_Factory;->homeProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/RealSquareDeviceTour_Factory;->squareDeviceTourSettingsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/RealSquareDeviceTour_Factory;->squareDeviceTourRedirectorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/RealSquareDeviceTour_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourRedirector;",
            ">;)",
            "Lcom/squareup/RealSquareDeviceTour_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/RealSquareDeviceTour_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/RealSquareDeviceTour_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/Home;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/SquareDeviceTourRedirector;)Lcom/squareup/RealSquareDeviceTour;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/RealSquareDeviceTour;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/RealSquareDeviceTour;-><init>(Lcom/squareup/ui/main/Home;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/SquareDeviceTourRedirector;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/RealSquareDeviceTour;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/RealSquareDeviceTour_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/Home;

    iget-object v1, p0, Lcom/squareup/RealSquareDeviceTour_Factory;->squareDeviceTourSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/SquareDeviceTourSettings;

    iget-object v2, p0, Lcom/squareup/RealSquareDeviceTour_Factory;->squareDeviceTourRedirectorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/SquareDeviceTourRedirector;

    invoke-static {v0, v1, v2}, Lcom/squareup/RealSquareDeviceTour_Factory;->newInstance(Lcom/squareup/ui/main/Home;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/SquareDeviceTourRedirector;)Lcom/squareup/RealSquareDeviceTour;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/RealSquareDeviceTour_Factory;->get()Lcom/squareup/RealSquareDeviceTour;

    move-result-object v0

    return-object v0
.end method
