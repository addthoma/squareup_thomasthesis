.class public abstract Lcom/squareup/CommonAppModuleProd;
.super Ljava/lang/Object;
.source "CommonAppModuleProd.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/CommonAppModuleProdWithoutServer;,
        Lcom/squareup/connectivity/ConnectivityReleaseModule;,
        Lcom/squareup/ProductionServerModule;,
        Lcom/squareup/server/precog/PrecogServiceReleaseModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideFeatures(Lcom/squareup/settings/server/FeatureFlagFeatures;)Lcom/squareup/settings/server/Features;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
