.class public final Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideX2SystemPropertiesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/SystemProperties;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory$InstanceHolder;->access$000()Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideX2SystemProperties()Lcom/squareup/util/SystemProperties;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonAppModule;->provideX2SystemProperties()Lcom/squareup/util/SystemProperties;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/SystemProperties;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/util/SystemProperties;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;->provideX2SystemProperties()Lcom/squareup/util/SystemProperties;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;->get()Lcom/squareup/util/SystemProperties;

    move-result-object v0

    return-object v0
.end method
