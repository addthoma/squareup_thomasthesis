.class public final Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanOfferFormatterNotSupported;
.super Ljava/lang/Object;
.source "CapitalFlexLoanOfferFormatterNotSupported.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanOfferFormatterNotSupported;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
        "()V",
        "offerLimit",
        "",
        "offer",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;",
        "impl-unsupported_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public offerLimit(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)Ljava/lang/String;
    .locals 1

    const-string v0, "offer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance p1, Ljava/lang/Exception;

    const-string v0, "Not Supported"

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
