.class final Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1;
.super Ljava/lang/Object;
.source "RealCapitalOfferWorkflow.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->onForegroundedActivity()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1;->this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1$listener$1;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1$listener$1;-><init>(Lio/reactivex/ObservableEmitter;)V

    .line 78
    iget-object v1, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1;->this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    invoke-static {v1}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->access$getActivityListener$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/ActivityListener;

    move-result-object v1

    move-object v2, v0

    check-cast v2, Lcom/squareup/ActivityListener$ResumedPausedListener;

    invoke-virtual {v1, v2}, Lcom/squareup/ActivityListener;->registerResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    .line 79
    new-instance v1, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1$1;-><init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1;Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$onForegroundedActivity$1$listener$1;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    return-void
.end method
