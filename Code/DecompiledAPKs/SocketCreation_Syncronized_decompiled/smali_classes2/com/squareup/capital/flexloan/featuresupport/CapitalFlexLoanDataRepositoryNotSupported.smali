.class public final Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanDataRepositoryNotSupported;
.super Ljava/lang/Object;
.source "CapitalFlexLoanDataRepositoryNotSupported.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0016J\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanDataRepositoryNotSupported;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "()V",
        "fetchFlexLoanStatus",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "fetchFlexPlan",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
        "planId",
        "",
        "impl-unsupported_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public fetchFlexLoanStatus()Lio/reactivex/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation

    .line 17
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    .line 18
    new-instance v1, Lcom/squareup/capital/flexloan/FailureData;

    const-string v2, "Error title"

    const-string v3, "Error description"

    const/4 v4, 0x0

    const-string v5, ""

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/capital/flexloan/FailureData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 17
    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;-><init>(Lcom/squareup/capital/flexloan/FailureData;)V

    .line 16
    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(\n      C\u2026\"\n          )\n      )\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
            ">;"
        }
    .end annotation

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance p1, Ljava/lang/Error;

    const-string v0, "Should not call fetchFlexPlan"

    invoke-direct {p1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
