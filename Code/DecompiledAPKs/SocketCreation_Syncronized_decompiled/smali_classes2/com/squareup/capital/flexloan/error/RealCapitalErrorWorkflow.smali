.class public final Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealCapitalErrorWorkflow.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/capital/flexloan/FailureData;",
        "Lkotlin/Unit;",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;",
        ">;",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalErrorWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalErrorWorkflow.kt\ncom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,27:1\n179#2,3:28\n199#2,4:31\n*E\n*S KotlinDebug\n*F\n+ 1 RealCapitalErrorWorkflow.kt\ncom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow\n*L\n15#1,3:28\n15#1,4:31\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J$\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u00032\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\rH\u0016R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/capital/flexloan/FailureData;",
        "",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;",
        "()V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final finish:Lcom/squareup/workflow/WorkflowAction;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    .line 31
    new-instance v0, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow$$special$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow$$special$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    .line 30
    iput-object v0, p0, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getFinish$p(Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lcom/squareup/capital/flexloan/FailureData;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;

    .line 22
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/FailureData;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/FailureData;->getDescription()Ljava/lang/String;

    move-result-object p1

    .line 24
    new-instance v2, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow$render$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow$render$1;-><init>(Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 21
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/capital/flexloan/FailureData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;->render(Lcom/squareup/capital/flexloan/FailureData;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;

    move-result-object p1

    return-object p1
.end method
