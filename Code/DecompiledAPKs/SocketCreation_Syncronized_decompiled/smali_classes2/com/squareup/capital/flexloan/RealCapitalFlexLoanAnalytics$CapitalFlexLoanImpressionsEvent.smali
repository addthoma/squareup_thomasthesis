.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "RealCapitalFlexLoanAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CapitalFlexLoanImpressionsEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0008\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\t\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u000b\u001a\u00020\u0003H\u00c2\u0003J1\u0010\u000c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "capital_pos_description",
        "",
        "capital_pos_offer_id",
        "capital_pos_plan_id",
        "capital_pos_error_code",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final capital_pos_description:Ljava/lang/String;

.field private final capital_pos_error_code:Ljava/lang/String;

.field private final capital_pos_offer_id:Ljava/lang/String;

.field private final capital_pos_plan_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "capital_pos_description"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos_offer_id"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos_plan_id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos_error_code"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos"

    .line 44
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    return-void
.end method

.method private final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    return-object v0
.end method

.method private final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    return-object v0
.end method

.method private final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    return-object v0
.end method

.method private final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;
    .locals 1

    const-string v0, "capital_pos_description"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos_offer_id"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos_plan_id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capital_pos_error_code"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CapitalFlexLoanImpressionsEvent(capital_pos_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", capital_pos_offer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_offer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", capital_pos_plan_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_plan_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", capital_pos_error_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;->capital_pos_error_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
