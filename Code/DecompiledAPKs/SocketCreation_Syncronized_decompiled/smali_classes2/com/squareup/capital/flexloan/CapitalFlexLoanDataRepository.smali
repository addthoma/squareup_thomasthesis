.class public interface abstract Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;
.super Ljava/lang/Object;
.source "CapitalFlexLoanDataRepository.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "",
        "fetchFlexLoanStatus",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "fetchFlexPlan",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
        "planId",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract fetchFlexLoanStatus()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
            ">;"
        }
    .end annotation
.end method
