.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;

    iget-object v2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0, v1, v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->newInstance(Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner_Factory;->get()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
