.class public final Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;
.super Ljava/lang/Object;
.source "CapitalFlexLoanAnalyticsNotSupported.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCapitalFlexLoanAnalyticsNotSupported.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CapitalFlexLoanAnalyticsNotSupported.kt\ncom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,9:1\n151#2,2:10\n*E\n*S KotlinDebug\n*F\n+ 1 CapitalFlexLoanAnalyticsNotSupported.kt\ncom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported\n*L\n8#1,2:10\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u000f\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u0005\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u0006\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u0007\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u0008\u001a\u00020\u0004H\u0096\u0001J\t\u0010\t\u001a\u00020\u0004H\u0096\u0001J\t\u0010\n\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u000b\u001a\u00020\u0004H\u0096\u0001J\u0011\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH\u0096\u0001J\t\u0010\u000f\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u0096\u0001J\u0011\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u000eH\u0096\u0001J\u0011\u0010\u0013\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH\u0096\u0001J\t\u0010\u0014\u001a\u00020\u0004H\u0096\u0001J\u0011\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u000eH\u0096\u0001J\t\u0010\u0017\u001a\u00020\u0004H\u0096\u0001J\u0011\u0010\u0018\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH\u0096\u0001J\u0011\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u000eH\u0096\u0001J\t\u0010\u001a\u001a\u00020\u0004H\u0096\u0001J\u0019\u0010\u001b\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u000eH\u0096\u0001J\'\u0010\u001c\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u000eH\u0096\u0001\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "()V",
        "logClickCapitalActiveOfferRow",
        "",
        "logClickCapitalActivePlanRow",
        "logClickCapitalChooseOffer",
        "logClickCapitalClosedPlanRow",
        "logClickCapitalManagePlan",
        "logClickCapitalPastDuePlanRow",
        "logClickCapitalPendingApplicationRow",
        "logViewCapitalActiveOfferRow",
        "logViewCapitalActivePlan",
        "planId",
        "",
        "logViewCapitalActivePlanRow",
        "logViewCapitalBalanceAppletRow",
        "logViewCapitalBalanceAppletRowError",
        "errorCode",
        "logViewCapitalClosedPlan",
        "logViewCapitalClosedPlanRow",
        "logViewCapitalFlexOffer",
        "offerId",
        "logViewCapitalIneligibleRow",
        "logViewCapitalPastDuePlan",
        "logViewCapitalPendingApplication",
        "logViewCapitalPendingApplicationRow",
        "logViewCapitalPlanError",
        "logViewCapitalSessionBridgeError",
        "impl-unsupported_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 10
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 11
    const-class v2, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iput-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    return-void
.end method


# virtual methods
.method public logClickCapitalActiveOfferRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalActiveOfferRow()V

    return-void
.end method

.method public logClickCapitalActivePlanRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalActivePlanRow()V

    return-void
.end method

.method public logClickCapitalChooseOffer()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalChooseOffer()V

    return-void
.end method

.method public logClickCapitalClosedPlanRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalClosedPlanRow()V

    return-void
.end method

.method public logClickCapitalManagePlan()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalManagePlan()V

    return-void
.end method

.method public logClickCapitalPastDuePlanRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalPastDuePlanRow()V

    return-void
.end method

.method public logClickCapitalPendingApplicationRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalPendingApplicationRow()V

    return-void
.end method

.method public logViewCapitalActiveOfferRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalActiveOfferRow()V

    return-void
.end method

.method public logViewCapitalActivePlan(Ljava/lang/String;)V
    .locals 1

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalActivePlan(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalActivePlanRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalActivePlanRow()V

    return-void
.end method

.method public logViewCapitalBalanceAppletRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalBalanceAppletRow()V

    return-void
.end method

.method public logViewCapitalBalanceAppletRowError(Ljava/lang/String;)V
    .locals 1

    const-string v0, "errorCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalBalanceAppletRowError(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalClosedPlan(Ljava/lang/String;)V
    .locals 1

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalClosedPlan(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalClosedPlanRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalClosedPlanRow()V

    return-void
.end method

.method public logViewCapitalFlexOffer(Ljava/lang/String;)V
    .locals 1

    const-string v0, "offerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalFlexOffer(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalIneligibleRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalIneligibleRow()V

    return-void
.end method

.method public logViewCapitalPastDuePlan(Ljava/lang/String;)V
    .locals 1

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalPastDuePlan(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalPendingApplication(Ljava/lang/String;)V
    .locals 1

    const-string v0, "offerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalPendingApplication(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalPendingApplicationRow()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalPendingApplicationRow()V

    return-void
.end method

.method public logViewCapitalPlanError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1, p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalPlanError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalSessionBridgeError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "offerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "planId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;->$$delegate_0:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logViewCapitalSessionBridgeError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
