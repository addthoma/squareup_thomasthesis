.class public Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;
.super Ljava/lang/Object;
.source "PaidInOutSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashmanagement/PaidInOutSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaidInOutEvent"
.end annotation


# instance fields
.field public final amount:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final label:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;->label:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;->amount:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;->description:Ljava/lang/String;

    return-void
.end method
