.class public Lcom/squareup/cashmanagement/AutoPrintReportSetting;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "AutoPrintReportSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final printReportEnabled:Lcom/squareup/settings/BooleanLocalSetting;


# direct methods
.method protected constructor <init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/BooleanLocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/settings/BooleanLocalSetting;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x0

    .line 21
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    .line 22
    iput-object p2, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->accountStatusProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p3, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->printReportEnabled:Lcom/squareup/settings/BooleanLocalSetting;

    return-void
.end method


# virtual methods
.method protected getValueFromDeviceProfile()Ljava/lang/Boolean;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->getValueFromDeviceProfile()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/Boolean;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->printReportEnabled:Lcom/squareup/settings/BooleanLocalSetting;

    invoke-virtual {v0}, Lcom/squareup/settings/BooleanLocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method protected bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->getValueFromLocalSettings()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected setValueLocallyInternal(Ljava/lang/Boolean;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->printReportEnabled:Lcom/squareup/settings/BooleanLocalSetting;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/BooleanLocalSetting;->set(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/AutoPrintReportSetting;->setValueLocallyInternal(Ljava/lang/Boolean;)V

    return-void
.end method
