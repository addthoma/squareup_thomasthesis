.class public interface abstract Lcom/squareup/cashmanagement/CashDrawerShiftManager;
.super Ljava/lang/Object;
.source "CashDrawerShiftManager.java"

# interfaces
.implements Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;


# virtual methods
.method public abstract addPaidInOutEvent(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V
.end method

.method public abstract cashManagementEnabledAndNeedsLoading()Z
.end method

.method public abstract closeCashDrawerShift(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract createNewOpenCashDrawerShift(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end method

.method public abstract currentCashDrawerShiftOrNull()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;"
        }
    .end annotation
.end method

.method public abstract emailCashDrawerShiftReport(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract enableCashManagement(Z)V
.end method

.method public abstract endAndCloseCurrentCashDrawerShift(Lcom/squareup/protos/common/Money;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
.end method

.method public abstract endCurrentCashDrawerShift()V
.end method

.method public abstract endCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
.end method

.method public abstract getCashDrawerShift(Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftCursor;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end method

.method public abstract loadCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateCurrentCashDrawerShiftDescription(Ljava/lang/String;)V
.end method
