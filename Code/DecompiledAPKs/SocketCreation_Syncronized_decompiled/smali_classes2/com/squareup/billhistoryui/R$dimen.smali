.class public final Lcom/squareup/billhistoryui/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final activity_applet_list_top_message_panel_horizontal_padding:I = 0x7f070056

.field public static final activity_applet_list_top_message_panel_message_text_size:I = 0x7f070057

.field public static final activity_applet_list_top_message_panel_tap_state_message_top_padding:I = 0x7f070058

.field public static final activity_applet_list_top_message_panel_title_bottom_padding:I = 0x7f070059

.field public static final activity_applet_list_top_message_panel_title_text_size:I = 0x7f07005a

.field public static final activity_applet_list_top_message_panel_vertical_padding:I = 0x7f07005b

.field public static final activity_applet_sticky_header_height:I = 0x7f07005c

.field public static final activity_applet_sticky_header_padding:I = 0x7f07005d

.field public static final bill_history_detail_empty_view_spacing_between_icon_and_text:I = 0x7f070081

.field public static final bill_history_detail_tender_cashier_row_top_padding:I = 0x7f070082

.field public static final bill_history_detail_tender_customer_circle_diameter:I = 0x7f070083

.field public static final bill_history_detail_tender_customer_circle_margin_end:I = 0x7f070084

.field public static final bill_history_detail_tender_customer_circle_vertical_padding:I = 0x7f070085

.field public static final bill_history_detail_tender_customer_initials:I = 0x7f070086

.field public static final bill_history_detail_tender_customer_name:I = 0x7f070087

.field public static final bulk_settle_button_badge_center_offset:I = 0x7f07008b

.field public static final bulk_settle_tender_row_amount_width:I = 0x7f07008c

.field public static final bulk_settle_tender_row_field_spacing:I = 0x7f07008d

.field public static final bulk_settle_tender_row_receipt_number_width:I = 0x7f07008e

.field public static final bulk_settle_tender_row_time_width:I = 0x7f07008f

.field public static final instant_deposits_progress_bar:I = 0x7f0701ca

.field public static final transactions_history_after_search_bar_icon_padding:I = 0x7f07054a

.field public static final transactions_history_before_search_bar_icon_margin:I = 0x7f07054c

.field public static final transactions_history_divider:I = 0x7f07054d

.field public static final transactions_history_divider_offset:I = 0x7f07054e

.field public static final transactions_history_header_bottom_padding:I = 0x7f07054f

.field public static final transactions_history_header_height:I = 0x7f070550

.field public static final transactions_history_header_horizontal_padding:I = 0x7f070551

.field public static final transactions_history_header_text_size:I = 0x7f070552

.field public static final transactions_history_header_top_padding:I = 0x7f070553

.field public static final transactions_history_list_row_height:I = 0x7f070554

.field public static final transactions_history_list_row_horizontal_padding:I = 0x7f070555

.field public static final transactions_history_list_row_subtitle_text_size:I = 0x7f070556

.field public static final transactions_history_list_row_title_bottom_padding:I = 0x7f070557

.field public static final transactions_history_list_row_title_text_size:I = 0x7f070558

.field public static final transactions_history_list_row_value_text_size:I = 0x7f070559

.field public static final transactions_history_search_bar_height:I = 0x7f07055a

.field public static final transactions_history_search_bar_magnifying_glass_icon_large_height:I = 0x7f07055b

.field public static final transactions_history_search_bar_magnifying_glass_icon_large_width:I = 0x7f07055c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
