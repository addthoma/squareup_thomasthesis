.class final Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;
.super Ljava/lang/Object;
.source "AppsFlyerAdAnalytics.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;


# direct methods
.method constructor <init>(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;->this$0:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Boolean;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;->this$0:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;

    invoke-static {v0}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->access$getAppsFlyerLib$p(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)Lcom/appsflyer/AppsFlyerLib;

    move-result-object v0

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iget-object v1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;->this$0:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;

    invoke-static {v1}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->access$getContext$p(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)Landroid/app/Application;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/appsflyer/AppsFlyerLib;->stopTracking(ZLandroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;->accept(Ljava/lang/Boolean;)V

    return-void
.end method
