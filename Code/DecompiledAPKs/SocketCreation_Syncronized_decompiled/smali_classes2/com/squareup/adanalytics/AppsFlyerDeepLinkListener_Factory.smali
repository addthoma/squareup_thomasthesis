.class public final Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;
.super Ljava/lang/Object;
.source "AppsFlyerDeepLinkListener_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final deferredDeepLinkPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;->deferredDeepLinkPreferenceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;

    invoke-direct {v0, p0}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;-><init>(Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;->deferredDeepLinkPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {v0}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;->newInstance(Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;->get()Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;

    move-result-object v0

    return-object v0
.end method
