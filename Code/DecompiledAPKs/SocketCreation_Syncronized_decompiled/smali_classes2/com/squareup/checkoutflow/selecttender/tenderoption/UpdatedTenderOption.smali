.class public final Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
.super Ljava/lang/Object;
.source "UpdatedTenderOption.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\'B\u008f\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012!\u0010\u0004\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\n0\u0005\u0012!\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\u000c0\u0005\u0012:\u0010\r\u001a6\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0012&\u0012$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00150\u000e\u00a2\u0006\u0002\u0010\u0016J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J$\u0010\u001e\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\n0\u0005H\u00c6\u0003J$\u0010\u001f\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\u000c0\u0005H\u00c6\u0003J=\u0010 \u001a6\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0012&\u0012$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00150\u000eH\u00c6\u0003J\u009b\u0001\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032#\u0008\u0002\u0010\u0004\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\n0\u00052#\u0008\u0002\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\u000c0\u00052<\u0008\u0002\u0010\r\u001a6\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0012&\u0012$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00150\u000eH\u00c6\u0001J\u0013\u0010\"\u001a\u00020\u000c2\u0008\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\t\u0010&\u001a\u00020\nH\u00d6\u0001R,\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\u000c0\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R,\u0010\u0004\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\n0\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0017RE\u0010\r\u001a6\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0012&\u0012$\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0013j\u0002`\u00140\u0011j\u0008\u0012\u0004\u0012\u00020\u0012`\u00150\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "",
        "tapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "titleStrategy",
        "Lkotlin/Function1;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "Lkotlin/ParameterName;",
        "name",
        "displayData",
        "",
        "isEnabledStrategy",
        "",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V",
        "()Lkotlin/jvm/functions/Function1;",
        "getTapName",
        "()Lcom/squareup/analytics/RegisterTapName;",
        "getTitleStrategy",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "DisplayData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isEnabledStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final tapName:Lcom/squareup/analytics/RegisterTapName;

.field private final titleStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final workflow:Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Workflow<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/RegisterTapName;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/workflow/Workflow<",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "+",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)V"
        }
    .end annotation

    const-string v0, "tapName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "titleStrategy"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isEnabledStrategy"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    iput-object p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;ILjava/lang/Object;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->copy(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component4()Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public final copy(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/RegisterTapName;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/workflow/Workflow<",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "+",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;"
        }
    .end annotation

    const-string v0, "tapName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "titleStrategy"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isEnabledStrategy"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    iget-object v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    iget-object p1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getTapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method

.method public final getTitleStrategy()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isEnabledStrategy()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdatedTenderOption(tapName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->tapName:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", titleStrategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->titleStrategy:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isEnabledStrategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->isEnabledStrategy:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", workflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;->workflow:Lcom/squareup/workflow/Workflow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
