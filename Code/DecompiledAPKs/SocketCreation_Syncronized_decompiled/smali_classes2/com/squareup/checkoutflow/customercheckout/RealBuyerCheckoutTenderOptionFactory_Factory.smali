.class public final Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutTenderOptionFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Ldagger/Lazy;Lcom/squareup/CountryCode;)Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/CountryCode;",
            ")",
            "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;-><init>(Landroid/app/Application;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Ldagger/Lazy;Lcom/squareup/CountryCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    iget-object v2, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/CountryCode;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Ldagger/Lazy;Lcom/squareup/CountryCode;)Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory_Factory;->get()Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;

    move-result-object v0

    return-object v0
.end method
