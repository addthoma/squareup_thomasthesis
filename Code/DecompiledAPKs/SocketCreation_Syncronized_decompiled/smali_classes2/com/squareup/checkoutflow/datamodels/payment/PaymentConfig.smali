.class public final Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
.super Ljava/lang/Object;
.source "PaymentConfig.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B)\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u0011\u001a\u00020\u0012J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\u00122\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\u0006\u0010\u001d\u001a\u00020\u0012J\t\u0010\u001e\u001a\u00020\u0019H\u00d6\u0001J\u0006\u0010\u001f\u001a\u00020\u0012J\t\u0010 \u001a\u00020!H\u00d6\u0001J\u0019\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\n\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "Landroid/os/Parcelable;",
        "billIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "amountToCollect",
        "Lcom/squareup/protos/common/Money;",
        "amountPaidOnBill",
        "totalAmountOfBill",
        "(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V",
        "getAmountPaidOnBill",
        "()Lcom/squareup/protos/common/Money;",
        "amountRemainingOnBill",
        "getAmountRemainingOnBill",
        "getAmountToCollect",
        "getBillIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "getTotalAmountOfBill",
        "collectingTotalAmountOfBill",
        "",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hasCapturedTenders",
        "hashCode",
        "isLastPaymentOnBill",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final amountPaidOnBill:Lcom/squareup/protos/common/Money;

.field private final amountToCollect:Lcom/squareup/protos/common/Money;

.field private final billIdPair:Lcom/squareup/protos/client/IdPair;

.field private final totalAmountOfBill:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig$Creator;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig$Creator;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "amountToCollect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountPaidOnBill"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalAmountOfBill"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    .line 37
    iget-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isNegative(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 39
    iget-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 43
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountRemainingOnBill()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isNegative(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 45
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountRemainingOnBill()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 49
    iget-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountRemainingOnBill()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 50
    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 51
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Amount to collect and amount remaining on bill together cannot be greater than the total amount of the bill"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 46
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Amount Remaining cannot be greater than total amount of bill"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 44
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Amount Remaining cannot be less than zero"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 40
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Cannot collect more than bill amount"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 38
    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Cannot collect a negative amount"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    .line 25
    check-cast p1, Lcom/squareup/protos/client/IdPair;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final collectingTotalAmountOfBill()Z
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final component1()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 1

    const-string v0, "amountToCollect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountPaidOnBill"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalAmountOfBill"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, p1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountPaidOnBill()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAmountRemainingOnBill()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMathOperatorsKt;->minus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMathOperatorsKt;->minus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public final getAmountToCollect()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getBillIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getTotalAmountOfBill()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final hasCapturedTenders()Z
    .locals 5

    .line 81
    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isLastPaymentOnBill()Z
    .locals 5

    .line 71
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountRemainingOnBill()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentConfig(billIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amountToCollect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amountPaidOnBill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalAmountOfBill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->amountPaidOnBill:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->totalAmountOfBill:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
