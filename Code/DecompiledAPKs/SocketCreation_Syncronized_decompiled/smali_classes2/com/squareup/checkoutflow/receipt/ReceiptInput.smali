.class public final Lcom/squareup/checkoutflow/receipt/ReceiptInput;
.super Ljava/lang/Object;
.source "ReceiptInput.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008/\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001KB\u0095\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u0012\u0008\u0010\r\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0003\u0012\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u0012\u0006\u0010\u0014\u001a\u00020\u0007\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0003\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\u000b\u00104\u001a\u0004\u0018\u00010\u000fH\u00c6\u0003J\t\u00105\u001a\u00020\u0003H\u00c6\u0003J\u0010\u00106\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001cJ\t\u00107\u001a\u00020\u0003H\u00c6\u0003J\t\u00108\u001a\u00020\u0007H\u00c6\u0003J\t\u00109\u001a\u00020\u0016H\u00c6\u0003J\t\u0010:\u001a\u00020\u0003H\u00c6\u0003J\t\u0010;\u001a\u00020\u0019H\u00c6\u0003J\t\u0010<\u001a\u00020\u0003H\u00c6\u0003J\t\u0010=\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\t\u0010?\u001a\u00020\u0003H\u00c6\u0003J\t\u0010@\u001a\u00020\u0003H\u00c6\u0003J\t\u0010A\u001a\u00020\u000bH\u00c6\u0003J\t\u0010B\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010C\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u00c0\u0001\u0010D\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00032\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00162\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0019H\u00c6\u0001\u00a2\u0006\u0002\u0010EJ\u0013\u0010F\u001a\u00020\u00032\u0008\u0010G\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010H\u001a\u00020IH\u00d6\u0001J\t\u0010J\u001a\u00020\u0007H\u00d6\u0001R\u0015\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\n\n\u0002\u0010\u001d\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0011\u0010\u0014\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001fR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\"R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\"R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010(R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010*R\u0011\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010,R\u0011\u0010\u0017\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010\"R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010\"R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010\"R\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u0010\"R\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u0010\"R\u0011\u0010\u0010\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u0010\"\u00a8\u0006L"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
        "",
        "isPaymentComplete",
        "",
        "canClickToContinue",
        "shouldHideCustomerButton",
        "billServerId",
        "",
        "hasCustomer",
        "showAddCardButton",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "showLanguageSelection",
        "displayName",
        "mostRecentActiveCardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "supportsSms",
        "autoReceiptCompleteTimeout",
        "",
        "showSmsMarketing",
        "businessName",
        "locale",
        "Ljava/util/Locale;",
        "shouldAutoSendReceipt",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V",
        "getAutoReceiptCompleteTimeout",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getBillServerId",
        "()Ljava/lang/String;",
        "getBusinessName",
        "getCanClickToContinue",
        "()Z",
        "getCountryCode",
        "()Lcom/squareup/CountryCode;",
        "getDisplayName",
        "getHasCustomer",
        "getLocale",
        "()Ljava/util/Locale;",
        "getMostRecentActiveCardReaderId",
        "()Lcom/squareup/cardreader/CardReaderId;",
        "getPaymentTypeInfo",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "getShouldAutoSendReceipt",
        "getShouldHideCustomerButton",
        "getShowAddCardButton",
        "getShowLanguageSelection",
        "getShowSmsMarketing",
        "getSupportsSms",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "PaymentTypeInfo",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoReceiptCompleteTimeout:Ljava/lang/Long;

.field private final billServerId:Ljava/lang/String;

.field private final businessName:Ljava/lang/String;

.field private final canClickToContinue:Z

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final displayName:Ljava/lang/String;

.field private final hasCustomer:Z

.field private final isPaymentComplete:Z

.field private final locale:Ljava/util/Locale;

.field private final mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

.field private final shouldAutoSendReceipt:Z

.field private final shouldHideCustomerButton:Z

.field private final showAddCardButton:Z

.field private final showLanguageSelection:Z

.field private final showSmsMarketing:Z

.field private final supportsSms:Z


# direct methods
.method public constructor <init>(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V
    .locals 6

    move-object v0, p0

    move-object v1, p7

    move-object/from16 v2, p14

    move-object/from16 v3, p15

    move-object/from16 v4, p17

    const-string v5, "countryCode"

    invoke-static {p7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "businessName"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "locale"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "paymentTypeInfo"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v5, p1

    iput-boolean v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    move v5, p2

    iput-boolean v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    move v5, p3

    iput-boolean v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    move-object v5, p4

    iput-object v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    move v5, p5

    iput-boolean v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    move v5, p6

    iput-boolean v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    iput-object v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    move v1, p8

    iput-boolean v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    move-object v1, p9

    iput-object v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    move-object/from16 v1, p10

    iput-object v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    move/from16 v1, p11

    iput-boolean v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    iput-object v2, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    iput-object v3, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    iput-object v4, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/ReceiptInput;ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/ReceiptInput;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p18

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    goto :goto_0

    :cond_0
    move/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-boolean v9, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-boolean v12, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-boolean v14, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    move-object/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move-object/from16 p15, v15

    if-eqz v16, :cond_f

    iget-boolean v15, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    goto :goto_f

    :cond_f
    move/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v1, v1, v16

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    goto :goto_10

    :cond_10
    move-object/from16 v1, p17

    :goto_10
    move/from16 p1, v2

    move/from16 p2, v3

    move/from16 p3, v4

    move-object/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move/from16 p11, v12

    move-object/from16 p12, v13

    move/from16 p13, v14

    move/from16 p16, v15

    move-object/from16 p17, v1

    invoke-virtual/range {p0 .. p17}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->copy(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    return v0
.end method

.method public final component10()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final component11()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    return v0
.end method

.method public final component12()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    return-object v0
.end method

.method public final component13()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    return v0
.end method

.method public final component14()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final component15()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public final component16()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    return v0
.end method

.method public final component17()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    return v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    return v0
.end method

.method public final component7()Lcom/squareup/CountryCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    return v0
.end method

.method public final component9()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/checkoutflow/receipt/ReceiptInput;
    .locals 19

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v12, p12

    move/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move/from16 v16, p16

    move-object/from16 v17, p17

    const-string v0, "countryCode"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    move-object/from16 v1, p14

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    move-object/from16 v1, p15

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTypeInfo"

    move-object/from16 v1, p17

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v18, Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-direct/range {v0 .. v17}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;-><init>(ZZZLjava/lang/String;ZZLcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;Ljava/util/Locale;ZLcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V

    return-object v18
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoReceiptCompleteTimeout()Ljava/lang/Long;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    return-object v0
.end method

.method public final getBillServerId()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    return-object v0
.end method

.method public final getBusinessName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCanClickToContinue()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    return v0
.end method

.method public final getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getHasCustomer()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    return v0
.end method

.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public final getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    return-object v0
.end method

.method public final getShouldAutoSendReceipt()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    return v0
.end method

.method public final getShouldHideCustomerButton()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    return v0
.end method

.method public final getShowAddCardButton()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    return v0
.end method

.method public final getShowLanguageSelection()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    return v0
.end method

.method public final getShowSmsMarketing()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    return v0
.end method

.method public final getSupportsSms()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_b
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :cond_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_d
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_e
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    if-eqz v2, :cond_f

    goto :goto_7

    :cond_f
    move v1, v2

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_10
    add-int/2addr v0, v3

    return v0
.end method

.method public final isPaymentComplete()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptInput(isPaymentComplete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", canClickToContinue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->canClickToContinue:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldHideCustomerButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldHideCustomerButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", billServerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->billServerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", hasCustomer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->hasCustomer:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showAddCardButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showAddCardButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showLanguageSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showLanguageSelection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mostRecentActiveCardReaderId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", supportsSms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->supportsSms:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", autoReceiptCompleteTimeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSmsMarketing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->showSmsMarketing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", businessName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldAutoSendReceipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->shouldAutoSendReceipt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", paymentTypeInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->paymentTypeInfo:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
