.class public final Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;
.super Ljava/lang/Object;
.source "ReceiptTextHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000bJ$\u0010\u000c\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
        "",
        "()V",
        "resolveSubtitle",
        "Lcom/squareup/util/ViewString$TextString;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "resolveTitle",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final resolveSubtitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
            ")",
            "Lcom/squareup/util/ViewString$TextString;"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTypeInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    instance-of v0, p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;

    const/4 v1, 0x0

    const-string v2, "null cannot be cast to non-null type com.squareup.checkoutflow.receipt.ReceiptInput.PaymentTypeInfo.RemainingAmountConfig.HasRemainingAmount"

    const-string v3, "res.phrase(com.squareup.\u2026                .format()"

    const-string v4, "amount"

    if-eqz v0, :cond_3

    .line 55
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;

    const-string v5, "total"

    if-eqz v0, :cond_1

    .line 56
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 59
    sget v0, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change_sub_with_remaining_balance:I

    .line 57
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 64
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;->getRemainingAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 63
    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 61
    invoke-virtual {p1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 68
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v5, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(\n            \u2026                .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 64
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 72
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    .line 73
    :goto_0
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 74
    sget v0, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change_sub:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 75
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v5, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 82
    :cond_3
    instance-of v0, p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    if-eqz v0, :cond_9

    .line 84
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;

    if-eqz v0, :cond_5

    .line 86
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 87
    sget v0, Lcom/squareup/checkout/R$string;->buyer_remaining_payment_due:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 91
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;

    move-result-object p3

    if-eqz p3, :cond_4

    check-cast p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig$HasRemainingAmount;->getRemainingAmount()Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 90
    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 88
    invoke-virtual {p1, v4, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 91
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 98
    :cond_5
    move-object v0, p3

    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingBalanceConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$HasRemainingBalance;

    if-eqz v2, :cond_7

    .line 100
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 101
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getRemainingBalanceConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig;

    move-result-object p2

    if-eqz p2, :cond_6

    check-cast p2, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$HasRemainingBalance;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$RemainingBalanceConfig$HasRemainingBalance;->getGetRemainingBalanceText()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 102
    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 100
    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 101
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.checkoutflow.receipt.ReceiptInput.PaymentTypeInfo.PaymentInfo.RemainingBalanceConfig.HasRemainingBalance"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 106
    :cond_7
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTipConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig;

    move-result-object v0

    .line 107
    instance-of v2, v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$HasTip;

    if-eqz v2, :cond_8

    .line 109
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 110
    sget v2, Lcom/squareup/checkout/R$string;->buyer_payment_note_tip:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 111
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p1, v4, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 112
    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$HasTip;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo$TipConfig$HasTip;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "tip"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    :cond_8
    :goto_1
    return-object v1

    .line 83
    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final resolveTitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
            ")",
            "Lcom/squareup/util/ViewString$TextString;"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTypeInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    instance-of v0, p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;

    if-eqz v0, :cond_2

    check-cast p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;->getChangeConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig;

    move-result-object p3

    .line 25
    instance-of v0, p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$HasChange;

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    .line 27
    sget v1, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_cash_change_only:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 30
    check-cast p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$HasChange;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$HasChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "amount"

    .line 28
    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(com.squareup.\u2026                .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 35
    :cond_0
    sget-object p2, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$NoChange;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo$ChangeConfig$NoChange;

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 36
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    .line 37
    sget p2, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 36
    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 41
    :cond_2
    instance-of p1, p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    if-eqz p1, :cond_3

    .line 42
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    check-cast p3, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;->getTotalAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "moneyFormatter.format(paymentTypeInfo.totalAmount)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
