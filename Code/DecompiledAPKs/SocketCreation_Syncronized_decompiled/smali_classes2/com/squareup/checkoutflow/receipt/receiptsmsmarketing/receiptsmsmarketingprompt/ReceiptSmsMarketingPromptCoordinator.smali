.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReceiptSmsMarketingPromptCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001!B3\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "acceptButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "couponBusinessName",
        "Landroid/widget/TextView;",
        "couponReason",
        "couponValue",
        "declineButton",
        "disclaimer",
        "Lcom/squareup/widgets/MessageView;",
        "languageSelectionButton",
        "Lcom/squareup/marketfont/MarketTextView;",
        "receiptSentTitle",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private acceptButton:Lcom/squareup/marketfont/MarketButton;

.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private couponBusinessName:Landroid/widget/TextView;

.field private couponReason:Landroid/widget/TextView;

.field private couponValue:Landroid/widget/TextView;

.field private declineButton:Lcom/squareup/marketfont/MarketButton;

.field private disclaimer:Lcom/squareup/widgets/MessageView;

.field private languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

.field private receiptSentTitle:Landroid/widget/TextView;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;Landroid/view/View;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 148
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 149
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->sms_marketing_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.sms_marketing_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->receiptSentTitle:Landroid/widget/TextView;

    .line 150
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->coupon_business_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.coupon_business_name)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->couponBusinessName:Landroid/widget/TextView;

    .line 151
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->coupon_value:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.coupon_value)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->couponValue:Landroid/widget/TextView;

    .line 152
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->coupon_reason:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.coupon_reason)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->couponReason:Landroid/widget/TextView;

    .line 153
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->sms_marketing_disclaimer:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.sms_marketing_disclaimer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->disclaimer:Lcom/squareup/widgets/MessageView;

    .line 154
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->sms_marketing_decline_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026marketing_decline_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->declineButton:Lcom/squareup/marketfont/MarketButton;

    .line 155
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->sms_marketing_accept_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026_marketing_accept_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->acceptButton:Lcom/squareup/marketfont/MarketButton;

    .line 156
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->switch_language_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.switch_language_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private final update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;Landroid/view/View;)V
    .locals 7

    .line 70
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 72
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 74
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v2, "buyerActionBar"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_0
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/squareup/checkout/R$string;->new_sale:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "view.context.getString(c\u2026eckout.R.string.new_sale)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 76
    new-instance v4, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$2;

    invoke-direct {v4, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 74
    invoke-virtual {v1, v3, v4}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 78
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->getAddCardState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object v1

    .line 79
    sget-object v3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideLeftGlyphButton()V

    goto :goto_0

    .line 80
    :cond_2
    instance-of v3, v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;

    if-eqz v3, :cond_4

    .line 81
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v3, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v5, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$3;

    invoke-direct {v5, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setLeftGlyphButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 87
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideTitle()V

    .line 88
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideSubtitle()V

    .line 90
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->getUpdateCustomerState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object v1

    .line 91
    instance-of v3, v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v3, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 92
    :cond_7
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_add_customer:I

    invoke-direct {v2, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 93
    new-instance v4, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$4;

    invoke-direct {v4, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$4;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 91
    invoke-virtual {v3, v2, v4}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 95
    :cond_8
    instance-of v3, v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v3, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 96
    :cond_9
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_view_customer:I

    invoke-direct {v2, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 97
    new-instance v4, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$5;

    invoke-direct {v4, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$5;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 95
    invoke-virtual {v3, v2, v4}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 99
    :cond_a
    instance-of v1, v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideRightButton()V

    .line 102
    :cond_c
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->getCouponInfo()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    move-result-object v1

    .line 103
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v3, "X"

    .line 105
    check-cast v3, Ljava/lang/CharSequence;

    .line 106
    new-instance v4, Landroid/text/style/ImageSpan;

    .line 107
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 108
    sget v5, Lcom/squareup/vectoricons/R$drawable;->circle_filled_check_24:I

    const/4 v6, 0x1

    .line 106
    invoke-direct {v4, p3, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/4 p3, 0x0

    .line 104
    invoke-virtual {v2, v3, v4, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    .line 114
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    sget v3, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_receipt_sent_to:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 116
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;->getReceiptDestination()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    const-string v5, "destination"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    .line 113
    invoke-virtual {v2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 118
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->receiptSentTitle:Landroid/widget/TextView;

    if-nez p3, :cond_d

    const-string v3, "receiptSentTitle"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->couponBusinessName:Landroid/widget/TextView;

    if-nez p3, :cond_e

    const-string v2, "couponBusinessName"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->getBusinessName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->couponValue:Landroid/widget/TextView;

    if-nez p3, :cond_f

    const-string v2, "couponValue"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;->getDiscount()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->couponReason:Landroid/widget/TextView;

    if-nez p3, :cond_10

    const-string v2, "couponReason"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;->getDiscountReason()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->disclaimer:Lcom/squareup/widgets/MessageView;

    if-nez p3, :cond_11

    const-string v2, "disclaimer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;->getCouponTerms()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->declineButton:Lcom/squareup/marketfont/MarketButton;

    const-string v1, "declineButton"

    if-nez p3, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_no_thanks:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->declineButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p3, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 128
    :cond_13
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$6;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$6;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 127
    invoke-virtual {p3, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->acceptButton:Lcom/squareup/marketfont/MarketButton;

    const-string v1, "acceptButton"

    if-nez p3, :cond_14

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_sign_up:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->acceptButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p3, :cond_15

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 132
    :cond_15
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$7;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$7;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 131
    invoke-virtual {p3, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "languageSelectionButton"

    if-nez p3, :cond_16

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    check-cast p3, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    invoke-static {p3, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 136
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object p2

    .line 137
    instance-of p3, p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    if-eqz p3, :cond_1a

    .line 138
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p3, :cond_17

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_18

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 140
    :cond_18
    sget p3, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    invoke-interface {v0, p3}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    const/4 v0, 0x0

    .line 139
    invoke-virtual {p1, p3, v0, v0, v0}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 142
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_19

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 143
    :cond_19
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$8;

    invoke-direct {p3, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$update$8;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 142
    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1a
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->bindViews(Landroid/view/View;)V

    .line 61
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
