.class public final Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReceiptCompleteCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptCompleteCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptCompleteCoordinator.kt\ncom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator\n*L\n1#1,268:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001,BK\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001dH\u0016J\u0010\u0010!\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001dH\u0002J\u0010\u0010\"\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001dH\u0016J \u0010#\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020%2\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u001dH\u0002J\u0012\u0010&\u001a\u00020\u001f2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0002J\u0018\u0010)\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020%2\u0006\u0010*\u001a\u00020+H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "tutorialPresenter",
        "Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "receiptTextHelper",
        "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "currentScreen",
        "glyphMessage",
        "Lcom/squareup/widgets/MessageView;",
        "glyphTitle",
        "Landroid/widget/TextView;",
        "languageSelectionButton",
        "Lcom/squareup/marketfont/MarketTextView;",
        "merchantImage",
        "Landroid/widget/ImageView;",
        "receiptContent",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "detach",
        "update",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "updateBackground",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "updateTitleAndSubtitle",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private currentScreen:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

.field private glyphMessage:Lcom/squareup/widgets/MessageView;

.field private glyphTitle:Landroid/widget/TextView;

.field private languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

.field private merchantImage:Landroid/widget/ImageView;

.field private receiptContent:Landroid/view/View;

.field private final receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final tutorialPresenter:Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
            ")V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->tutorialPresenter:Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 54
    invoke-direct/range {p0 .. p5}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V

    return-void
.end method

.method public static final synthetic access$getBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p0, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    return-object p0
.end method

.method public static final synthetic access$getLanguageSelectionButton$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p0, :cond_0

    const-string v0, "languageSelectionButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "merchantImage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Landroid/view/View;
    .locals 1

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptContent:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "receiptContent"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getTutorialCore$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method

.method public static final synthetic access$getTutorialPresenter$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->tutorialPresenter:Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;

    return-object p0
.end method

.method public static final synthetic access$setBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    return-void
.end method

.method public static final synthetic access$setCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    return-void
.end method

.method public static final synthetic access$setLanguageSelectionButton$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public static final synthetic access$setMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->merchantImage:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Landroid/view/View;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptContent:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;Landroid/view/View;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateBackground(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/picasso/RequestCreator;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->updateBackground(Lcom/squareup/picasso/RequestCreator;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 3

    .line 254
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 255
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphTitle:Landroid/widget/TextView;

    .line 256
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphMessage:Lcom/squareup/widgets/MessageView;

    .line 257
    sget v0, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026kout.R.id.merchant_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->merchantImage:Landroid/widget/ImageView;

    .line 258
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->receipt_content:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.receipt_content)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptContent:Landroid/view/View;

    .line 259
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->switch_language_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.switch_language_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    .line 262
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$layout;->auth_glyph_view:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    .line 264
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 265
    sget v1, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 262
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.glyph.SquareGlyphView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;Landroid/view/View;)V
    .locals 6

    .line 142
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 144
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 146
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v2, "buyerActionBar"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 147
    :cond_0
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    .line 148
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 149
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isPaymentComplete()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 150
    sget v4, Lcom/squareup/checkout/R$string;->new_sale:I

    goto :goto_0

    .line 152
    :cond_1
    sget v4, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 148
    :goto_0
    invoke-virtual {p3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string/jumbo v4, "view.context.getString(\n\u2026          }\n            )"

    invoke-static {p3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Ljava/lang/CharSequence;

    .line 147
    invoke-direct {v3, p3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 156
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$2;

    invoke-direct {p3, p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 146
    invoke-virtual {v1, v3, p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 158
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object p3

    invoke-direct {p0, p1, p3}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->updateTitleAndSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V

    .line 160
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getAddCardState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object p3

    .line 162
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideLeftGlyphButton()V

    goto :goto_1

    .line 163
    :cond_3
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;

    if-eqz v1, :cond_5

    .line 164
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v4, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$3;

    invoke-direct {v4, p3}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v3, v4}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setLeftGlyphButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 170
    :cond_5
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getDisplayName()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_7

    .line 171
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    check-cast p3, Ljava/lang/CharSequence;

    invoke-direct {v3, p3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    invoke-virtual {v1, v3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTicketName(Lcom/squareup/util/ViewString;)V

    .line 174
    :cond_7
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getUpdateCustomerState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object p3

    .line 175
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;

    if-eqz v1, :cond_9

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 176
    :cond_8
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_add_customer:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 177
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$5;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$5;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 175
    invoke-virtual {p3, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 179
    :cond_9
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;

    if-eqz v1, :cond_b

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 180
    :cond_a
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_view_customer:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 181
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$6;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$6;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 179
    invoke-virtual {p3, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 183
    :cond_b
    instance-of p3, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;

    if-eqz p3, :cond_d

    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p3, :cond_c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideRightButton()V

    .line 187
    :cond_d
    :goto_2
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getReceiptSelection()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    move-result-object p3

    .line 188
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const-string v3, "glyphTitle"

    if-eqz v1, :cond_f

    .line 189
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphTitle:Landroid/widget/TextView;

    if-nez p3, :cond_e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    sget v1, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 191
    :cond_f
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$PaperReceipt;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$PaperReceipt;

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    goto :goto_3

    :cond_10
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$FormalPaperReceipt;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$FormalPaperReceipt;

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 192
    :goto_3
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphTitle:Landroid/widget/TextView;

    if-nez p3, :cond_11

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_printed:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 194
    :cond_12
    instance-of v1, p3, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    if-eqz v1, :cond_1d

    .line 195
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getSmsMarketingResult()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    move-result-object v1

    sget-object v4, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->ordinal()I

    move-result v1

    aget v1, v4, v1

    const/4 v4, 0x0

    const-string v5, "glyphMessage"

    if-eq v1, v2, :cond_19

    const/4 p3, 0x2

    if-eq v1, p3, :cond_17

    const/4 p3, 0x3

    if-eq v1, p3, :cond_13

    goto/16 :goto_4

    .line 206
    :cond_13
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphTitle:Landroid/widget/TextView;

    if-nez p3, :cond_14

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_youre_all_set:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphMessage:Lcom/squareup/widgets/MessageView;

    if-nez p3, :cond_15

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_check_your_texts:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphMessage:Lcom/squareup/widgets/MessageView;

    if-nez p3, :cond_16

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p3, v4}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_4

    .line 203
    :cond_17
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphTitle:Landroid/widget/TextView;

    if-nez p3, :cond_18

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_transaction_complete:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 197
    :cond_19
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphTitle:Landroid/widget/TextView;

    if-nez v1, :cond_1a

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1a
    sget v3, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_sent:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphMessage:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_1b

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1b
    check-cast p3, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;->getDigitalDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;->getDestinationValue()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v1, p3}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->glyphMessage:Lcom/squareup/widgets/MessageView;

    if-nez p3, :cond_1c

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1c
    invoke-virtual {p3, v4}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 215
    :cond_1d
    :goto_4
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "languageSelectionButton"

    if-nez p3, :cond_1e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1e
    check-cast p3, Landroid/view/View;

    .line 216
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    .line 215
    invoke-static {p3, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 218
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object p3

    .line 219
    instance-of v3, p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    if-eqz v3, :cond_22

    .line 220
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez v3, :cond_1f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1f
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v3, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_20

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 222
    :cond_20
    sget v3, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v3, 0x0

    .line 221
    invoke-virtual {p1, v0, v3, v3, v3}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 224
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->languageSelectionButton:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_21

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 225
    :cond_21
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$7;

    invoke-direct {v0, p3}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$7;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p3

    check-cast p3, Landroid/view/View$OnClickListener;

    .line 224
    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    :cond_22
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getClickAnywhereState()Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    move-result-object p1

    .line 229
    instance-of p2, p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState$Enabled;

    if-eqz p2, :cond_25

    .line 230
    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptContent:Landroid/view/View;

    const-string p3, "receiptContent"

    if-nez p2, :cond_23

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_23
    invoke-virtual {p2, v2}, Landroid/view/View;->setClickable(Z)V

    .line 231
    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptContent:Landroid/view/View;

    if-nez p2, :cond_24

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_24
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$8;

    invoke-direct {p3, p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$update$8;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_25
    return-void
.end method

.method private final updateBackground(Lcom/squareup/picasso/RequestCreator;)V
    .locals 3

    if-eqz p1, :cond_2

    .line 110
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->merchantImage:Landroid/widget/ImageView;

    const-string v1, "merchantImage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;

    invoke-direct {v1, p1, p0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$updateBackground$$inlined$let$lambda$1;-><init>(Lcom/squareup/picasso/RequestCreator;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)V

    check-cast v1, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    :cond_2
    return-void
.end method

.method private final updateTitleAndSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V
    .locals 5

    .line 241
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v1, "buyerActionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    .line 243
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v4

    .line 242
    invoke-virtual {v2, v3, v4, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;->resolveTitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 241
    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    .line 248
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object p1

    .line 247
    invoke-virtual {v1, v2, p1, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;->resolveSubtitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/ViewString;

    .line 246
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->bindViews(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->screen:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/BiPredicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screen.distinctUntilChan\u2026reen.requestCreator\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 96
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 97
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getCuratedImage()Lcom/squareup/merchantimages/CuratedImage;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "merchantImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    :cond_1
    return-void
.end method
