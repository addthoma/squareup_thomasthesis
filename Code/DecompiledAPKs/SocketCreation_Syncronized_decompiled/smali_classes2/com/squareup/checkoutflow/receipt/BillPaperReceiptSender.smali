.class public final Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;
.super Ljava/lang/Object;
.source "BillPaperReceiptSender.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0010H\u0016J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0013H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;",
        "Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;",
        "receiptForLastPayment",
        "Lcom/squareup/payment/ReceiptForLastPayment;",
        "printerStations",
        "Lcom/squareup/print/PrinterStations;",
        "printSettings",
        "Lcom/squareup/print/PrintSettings;",
        "buyerFlowReceiptManager",
        "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
        "(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;)V",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getReceipt",
        "()Lcom/squareup/payment/PaymentReceipt;",
        "maybePrintFormalReceipt",
        "",
        "maybePrintReceipt",
        "supportsFormalPaper",
        "",
        "supportsPaper",
        "impl-bill_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

.field private final printSettings:Lcom/squareup/print/PrintSettings;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "receiptForLastPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowReceiptManager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->printSettings:Lcom/squareup/print/PrintSettings;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    return-void
.end method

.method private final getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

    invoke-interface {v0}, Lcom/squareup/payment/ReceiptForLastPayment;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public maybePrintFormalReceipt()V
    .locals 3

    .line 39
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->maybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public maybePrintReceipt()V
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->maybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public supportsFormalPaper()Z
    .locals 2

    .line 35
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->supportsPaper()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->printSettings:Lcom/squareup/print/PrintSettings;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/print/PrintSettings;->isFormalReceiptPrintingAvailable(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public supportsPaper()Z
    .locals 4

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method
