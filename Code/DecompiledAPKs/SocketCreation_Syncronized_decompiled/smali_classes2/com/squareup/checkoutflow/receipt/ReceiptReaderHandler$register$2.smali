.class public final Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2;
.super Ljava/lang/Object;
.source "ReceiptReaderHandler.kt"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->register(Lcom/squareup/cardreader/CardReaderHub;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptReaderHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptReaderHandler.kt\ncom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2\n*L\n1#1,69:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u0006\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
        "onCardReaderAdded",
        "",
        "cardReader",
        "Lcom/squareup/cardreader/CardReader;",
        "onCardReaderRemoved",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 47
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2;->this$0:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 54
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2;->this$0:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->getCardListenerRelay()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
