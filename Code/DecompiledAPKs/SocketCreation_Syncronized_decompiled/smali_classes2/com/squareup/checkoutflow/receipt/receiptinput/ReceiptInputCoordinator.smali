.class public final Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReceiptInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptInputCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptInputCoordinator.kt\ncom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,213:1\n1103#2,7:214\n*E\n*S KotlinDebug\n*F\n+ 1 ReceiptInputCoordinator.kt\ncom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator\n*L\n172#1,7:214\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001,BK\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001bH\u0016J\u0010\u0010!\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001bH\u0002J\u0010\u0010\"\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001bH\u0016J \u0010#\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020%2\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u001bH\u0002J\u0012\u0010&\u001a\u00020\u001f2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u0002J\u0018\u0010)\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020%2\u0006\u0010*\u001a\u00020+H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "scrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "receiptTextHelper",
        "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "currentScreen",
        "helperText",
        "Landroid/widget/TextView;",
        "inputField",
        "Lcom/squareup/noho/NohoEditText;",
        "merchantImage",
        "Landroid/widget/ImageView;",
        "receiptContent",
        "Landroid/view/View;",
        "sendButton",
        "Lcom/squareup/noho/NohoButton;",
        "attach",
        "",
        "view",
        "bindViews",
        "detach",
        "update",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "updateBackground",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "updateTitleAndSubtitle",
        "paymentTypeInfo",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private currentScreen:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

.field private helperText:Landroid/widget/TextView;

.field private inputField:Lcom/squareup/noho/NohoEditText;

.field private merchantImage:Landroid/widget/ImageView;

.field private receiptContent:Landroid/view/View;

.field private final receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private final scrubber:Lcom/squareup/text/InsertingScrubber;

.field private sendButton:Lcom/squareup/noho/NohoButton;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/text/InsertingScrubber;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;",
            ")V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 44
    invoke-direct/range {p0 .. p5}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;)V

    return-void
.end method

.method public static final synthetic access$getBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;
    .locals 1

    .line 44
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p0, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    return-object p0
.end method

.method public static final synthetic access$getInputField$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/noho/NohoEditText;
    .locals 1

    .line 44
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez p0, :cond_0

    const-string v0, "inputField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 44
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "merchantImage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Landroid/view/View;
    .locals 1

    .line 44
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->receiptContent:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "receiptContent"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getScrubber$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/text/InsertingScrubber;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    return-object p0
.end method

.method public static final synthetic access$setBuyerActionBar$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    return-void
.end method

.method public static final synthetic access$setCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    return-void
.end method

.method public static final synthetic access$setInputField$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Lcom/squareup/noho/NohoEditText;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    return-void
.end method

.method public static final synthetic access$setMerchantImage$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->merchantImage:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setReceiptContent$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Landroid/view/View;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->receiptContent:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;Landroid/view/View;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateBackground(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Lcom/squareup/picasso/RequestCreator;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->updateBackground(Lcom/squareup/picasso/RequestCreator;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 205
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 206
    sget v0, Lcom/squareup/checkout/R$id;->input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    .line 207
    sget v0, Lcom/squareup/checkout/R$id;->confirm_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    .line 208
    sget v0, Lcom/squareup/checkout/R$id;->helper_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->helperText:Landroid/widget/TextView;

    .line 209
    sget v0, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026kout.R.id.merchant_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->merchantImage:Landroid/widget/ImageView;

    .line 210
    sget v0, Lcom/squareup/checkoutflow/receipt/impl/R$id;->receipt_content:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.receipt_content)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->receiptContent:Landroid/view/View;

    return-void
.end method

.method private final update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;Landroid/view/View;)V
    .locals 5

    .line 136
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getScreenState()Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$update$1;

    invoke-direct {v1, v0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p3, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 142
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v1, "buyerActionBar"

    if-nez p3, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v3, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$update$2;

    invoke-direct {v3, v0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p3, v2, v3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 146
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object p3

    invoke-direct {p0, p1, p3}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->updateTitleAndSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V

    .line 148
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getDisplayName()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_2

    .line 149
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    check-cast p3, Ljava/lang/CharSequence;

    invoke-direct {v1, p3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTicketName(Lcom/squareup/util/ViewString;)V

    .line 152
    :cond_2
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    const-string v1, "inputField"

    if-nez p3, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;->getCustomerData()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 153
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;->getCustomerData()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    goto :goto_1

    .line 155
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    .line 157
    instance-of v3, v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$EmailInput;

    if-eqz v3, :cond_5

    sget v3, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_email_hint:I

    goto :goto_0

    .line 158
    :cond_5
    instance-of v3, v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;

    if-eqz v3, :cond_10

    sget v3, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_text_hint:I

    .line 155
    :goto_0
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 152
    :goto_1
    invoke-virtual {p3, v2}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 163
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->inputField:Lcom/squareup/noho/NohoEditText;

    if-nez p3, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 165
    :cond_6
    instance-of v1, v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$EmailInput;

    if-eqz v1, :cond_7

    const/16 v2, 0x20

    goto :goto_2

    .line 166
    :cond_7
    instance-of v2, v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;

    if-eqz v2, :cond_f

    const/4 v2, 0x3

    .line 163
    :goto_2
    invoke-virtual {p3, v2}, Lcom/squareup/noho/NohoEditText;->setRawInputType(I)V

    .line 170
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    const-string v2, "sendButton"

    if-nez p3, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    sget v4, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_send:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p3, v3}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    if-nez p3, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p3, Landroid/view/View;

    .line 214
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v1, :cond_a

    .line 177
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    .line 178
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/address/CountryResources;->emailReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result p2

    .line 177
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 180
    :cond_a
    instance-of p3, v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;

    if-eqz p3, :cond_e

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/address/CountryResources;->smsReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result p2

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 182
    :goto_3
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    const-string p3, "helperText"

    if-eqz p2, :cond_c

    .line 183
    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->helperText:Landroid/widget/TextView;

    if-nez p2, :cond_b

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    :cond_c
    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->helperText:Landroid/widget/TextView;

    if-nez p2, :cond_d

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast p2, Landroid/view/View;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void

    .line 180
    :cond_e
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 166
    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 158
    :cond_10
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final updateBackground(Lcom/squareup/picasso/RequestCreator;)V
    .locals 3

    if-eqz p1, :cond_2

    .line 107
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->merchantImage:Landroid/widget/ImageView;

    const-string v1, "merchantImage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;

    invoke-direct {v1, p1, p0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$updateBackground$$inlined$let$lambda$1;-><init>(Lcom/squareup/picasso/RequestCreator;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)V

    check-cast v1, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    :cond_2
    return-void
.end method

.method private final updateTitleAndSubtitle(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)V
    .locals 5

    .line 192
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    const-string v1, "buyerActionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 193
    :cond_0
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v4

    .line 193
    invoke-virtual {v2, v3, v4, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;->resolveTitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 192
    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 198
    :cond_1
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->receiptTextHelper:Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;

    .line 199
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object p1

    .line 198
    invoke-virtual {v1, v2, p1, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptTextHelper;->resolveSubtitle(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;)Lcom/squareup/util/ViewString$TextString;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/ViewString;

    .line 197
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->bindViews(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    const-string v1, "screen.firstElement()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Maybe;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 85
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->screen:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$2;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$2;

    check-cast v1, Lio/reactivex/functions/BiPredicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screen.distinctUntilChan\u2026reen.requestCreator\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 95
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->screen:Lio/reactivex/Observable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$4;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 98
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown ReceiptInputScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->currentScreen:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getCuratedImage()Lcom/squareup/merchantimages/CuratedImage;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->merchantImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "merchantImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    :cond_1
    return-void
.end method
