.class final Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckoutWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->render(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "+",
        "Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;",
        "it",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$5;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
            "Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$5;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;->handlePaymentResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    .line 120
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;->INSTANCE:Lcom/squareup/checkoutflow/CheckoutResult$CheckoutCompleted;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$5;->invoke(Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
