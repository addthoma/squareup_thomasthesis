.class public final Lcom/squareup/checkoutflow/emoney/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final emoney:I = 0x7f1209a6

.field public static final emoney_amount:I = 0x7f1209a7

.field public static final emoney_authorizing:I = 0x7f1209a8

.field public static final emoney_brand_id:I = 0x7f1209a9

.field public static final emoney_brand_quicpay:I = 0x7f1209aa

.field public static final emoney_brand_suica:I = 0x7f1209ab

.field public static final emoney_cancel_dialog_subtitle:I = 0x7f1209ac

.field public static final emoney_canceling:I = 0x7f1209ad

.field public static final emoney_check_balance:I = 0x7f1209ae

.field public static final emoney_current_balance_message:I = 0x7f1209af

.field public static final emoney_minimum_amount_message:I = 0x7f1209b0

.field public static final emoney_miryo_cancel_dialog_discard:I = 0x7f1209b1

.field public static final emoney_miryo_cancel_dialog_keep:I = 0x7f1209b2

.field public static final emoney_miryo_cancel_dialog_subtitle:I = 0x7f1209b3

.field public static final emoney_miryo_cancel_dialog_title:I = 0x7f1209b4

.field public static final emoney_miryo_error:I = 0x7f1209b5

.field public static final emoney_miryo_error_no_reader:I = 0x7f1209b6

.field public static final emoney_miryo_error_subtitle:I = 0x7f1209b7

.field public static final emoney_miryo_failure_subtitle:I = 0x7f1209b8

.field public static final emoney_online_only:I = 0x7f1209b9

.field public static final emoney_online_processing:I = 0x7f1209ba

.field public static final emoney_payment:I = 0x7f1209bb

.field public static final emoney_payment_complete:I = 0x7f1209bc

.field public static final emoney_payment_complete_miryo_subtitle:I = 0x7f1209bd

.field public static final emoney_payment_error_cancel:I = 0x7f1209be

.field public static final emoney_payment_error_miryo_title:I = 0x7f1209bf

.field public static final emoney_payment_error_retry:I = 0x7f1209c0

.field public static final emoney_payment_error_title:I = 0x7f1209c1

.field public static final emoney_payment_title:I = 0x7f1209c2

.field public static final emoney_payment_title_with_balance:I = 0x7f1209c3

.field public static final emoney_payment_title_with_balance_check:I = 0x7f1209c4

.field public static final emoney_processing:I = 0x7f1209c5

.field public static final emoney_processing_message:I = 0x7f1209c6

.field public static final emoney_reader_disconnected:I = 0x7f1209c7

.field public static final emoney_reader_disconnected_subtitle:I = 0x7f1209c8

.field public static final emoney_reader_disconnected_title:I = 0x7f1209c9

.field public static final emoney_reader_incompatible:I = 0x7f1209ca

.field public static final emoney_reader_not_ready_title:I = 0x7f1209cb

.field public static final emoney_reader_timed_out:I = 0x7f1209cc

.field public static final emoney_selection_subtitle:I = 0x7f1209cd

.field public static final emoney_something_went_wrong:I = 0x7f1209ce

.field public static final emoney_split_payment:I = 0x7f1209cf

.field public static final emoney_tap_again:I = 0x7f1209d0

.field public static final emoney_tap_card_prompt:I = 0x7f1209d1

.field public static final emoney_tmn_msg_common_error:I = 0x7f1209d2

.field public static final emoney_tmn_msg_different_card:I = 0x7f1209d3

.field public static final emoney_tmn_msg_exceeded_limit_amount:I = 0x7f1209d4

.field public static final emoney_tmn_msg_expired_card:I = 0x7f1209d5

.field public static final emoney_tmn_msg_insufficient_balance:I = 0x7f1209d6

.field public static final emoney_tmn_msg_invalid_card:I = 0x7f1209d7

.field public static final emoney_tmn_msg_locked_mobile_service:I = 0x7f1209d8

.field public static final emoney_tmn_msg_read_error:I = 0x7f1209d9

.field public static final emoney_tmn_msg_several_suica_card:I = 0x7f1209da

.field public static final emoney_tmn_msg_write_error:I = 0x7f1209db

.field public static final emoney_unknown_error:I = 0x7f1209dc

.field public static final emoney_verify_balance:I = 0x7f1209dd

.field public static final emoney_verify_balance_failed_title:I = 0x7f1209de


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
