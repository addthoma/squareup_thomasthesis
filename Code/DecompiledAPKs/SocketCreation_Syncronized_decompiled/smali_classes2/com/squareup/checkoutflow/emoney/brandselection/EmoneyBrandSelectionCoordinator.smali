.class public final Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EmoneyBrandSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001 BA\u0008\u0007\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0018\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001f\u001a\u00020\u0005H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "container",
        "Landroid/view/ViewGroup;",
        "subtitle",
        "Landroid/widget/TextView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "createEmoneyRow",
        "Lcom/squareup/marin/widgets/BorderedLinearLayout;",
        "brandRow",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;",
        "brandSelection",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;",
        "update",
        "data",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private container:Landroid/view/ViewGroup;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private subtitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;)V

    return-void
.end method

.method private final createEmoneyRow(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;)Lcom/squareup/marin/widgets/BorderedLinearLayout;
    .locals 8

    .line 120
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Enabled;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->getEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    .line 122
    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color:I

    goto :goto_1

    .line 124
    :cond_1
    sget v4, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    .line 128
    :goto_1
    new-instance v5, Lcom/squareup/ui/account/view/LineRow$Builder;

    iget-object v6, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v6, :cond_2

    const-string v7, "container"

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->getStringID()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(I)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v5

    .line 130
    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v5, v6}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v5

    .line 131
    sget-object v6, Lcom/squareup/marin/widgets/ChevronVisibility;->IF_ENABLED:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v5, v6}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v5

    .line 132
    sget v6, Lcom/squareup/noho/R$dimen;->noho_text_size_body:I

    invoke-virtual {v5, v6}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitleTextSize(I)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v5

    .line 133
    invoke-virtual {v5}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object v5

    if-eqz v0, :cond_3

    .line 135
    new-instance v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;

    invoke-direct {v0, p2, p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v0}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_3
    invoke-virtual {v5, v2, v2, v2, v2}, Lcom/squareup/ui/account/view/LineRow;->setPadding(IIII)V

    if-eqz v3, :cond_4

    .line 143
    sget p1, Lcom/squareup/noho/R$drawable;->noho_selector_row_background:I

    invoke-virtual {v5, p1}, Lcom/squareup/ui/account/view/LineRow;->setBackgroundResource(I)V

    :cond_4
    const-string p1, "lineRow"

    .line 145
    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Lcom/squareup/ui/account/view/LineRow;->setEnabled(Z)V

    .line 146
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, v4}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    invoke-virtual {v5, p1}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabelTextColor(I)V

    .line 149
    new-instance p1, Lcom/squareup/marin/widgets/BorderedLinearLayout;

    invoke-virtual {v5}, Lcom/squareup/ui/account/view/LineRow;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 150
    invoke-virtual {p1, v1, v1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setHorizontalBorders(ZZ)V

    .line 151
    check-cast v5, Landroid/view/View;

    invoke-virtual {p1, v5}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addView(Landroid/view/View;)V

    const/16 p2, 0x10

    .line 152
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setGravity(I)V

    return-object p1
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;)V
    .locals 12

    .line 80
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_0

    const-string v3, "actionBar"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 84
    :cond_0
    new-instance v3, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 85
    new-instance v4, Lcom/squareup/util/ViewString$TextString;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v4, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v4, Lcom/squareup/resources/TextModel;

    invoke-virtual {v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 86
    sget-object v3, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v4, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$update$1;

    invoke-direct {v4, p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v5

    .line 87
    sget-object v6, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_check_balance:I

    invoke-direct {v0, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v7, v0

    check-cast v7, Lcom/squareup/resources/TextModel;

    const/4 v8, 0x0

    new-instance v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$update$2;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;)V

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function0;

    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 92
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;->getBrandSelection()Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Enabled;

    const-string v1, "subtitle"

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->subtitle:Landroid/widget/TextView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_selection_subtitle:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->subtitle:Landroid/widget/TextView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 95
    :cond_3
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_minimum_amount_message:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 98
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    const-wide/16 v4, 0x1

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v7, "data.money.currency_code"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v5, v6}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 96
    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :goto_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->container:Landroid/view/ViewGroup;

    const-string v1, "container"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 105
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;->getBrands()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    .line 106
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;->getBrandSelection()Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->createEmoneyRow(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;)Lcom/squareup/marin/widgets/BorderedLinearLayout;

    move-result-object v2

    .line 107
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 110
    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v4, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v2, Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v4, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 113
    :cond_6
    new-instance v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$update$3;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 68
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 69
    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$id;->emoney_payment_selection_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.e\u2026ment_selection_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->container:Landroid/view/ViewGroup;

    .line 70
    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$id;->emoney_brand_selection_subtitle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.e\u2026brand_selection_subtitle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->subtitle:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026iew, it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
