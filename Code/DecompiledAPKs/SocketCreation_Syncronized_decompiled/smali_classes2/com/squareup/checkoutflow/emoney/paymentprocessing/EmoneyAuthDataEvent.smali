.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "EmoneyAuthDataEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAuthDataEvent;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 6
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "EMoney Auth Data Received"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
