.class public final Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;
.super Ljava/lang/Object;
.source "EmoneyIconFactoryLight.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmoneyIconFactoryLight.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmoneyIconFactoryLight.kt\ncom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight\n*L\n1#1,26:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "()V",
        "create",
        "Landroid/graphics/drawable/Drawable;",
        "context",
        "Landroid/content/Context;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 20
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_no_icon_144:I

    .line 21
    invoke-virtual {p1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/vector/icons/R$style;->Cardreader_Dark:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 18
    invoke-static {p1, v0, v1}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 17
    check-cast p1, Landroid/graphics/drawable/Drawable;

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
