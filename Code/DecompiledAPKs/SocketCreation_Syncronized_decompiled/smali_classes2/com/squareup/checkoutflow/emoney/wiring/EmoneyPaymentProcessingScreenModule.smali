.class public final Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;
.super Ljava/lang/Object;
.source "EmoneyPaymentProcessingScreenModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0008\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;",
        "",
        "()V",
        "provideEmoneyIconFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "provideInflater",
        "Lcom/squareup/workflow/InflaterDelegate;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;->INSTANCE:Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideEmoneyIconFactory()Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 19
    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryLight;

    check-cast v0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    return-object v0
.end method

.method public static final provideInflater()Lcom/squareup/workflow/InflaterDelegate;
    .locals 1
    .annotation runtime Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessing;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 16
    sget-object v0, Lcom/squareup/workflow/InflaterDelegate$Real;->INSTANCE:Lcom/squareup/workflow/InflaterDelegate$Real;

    check-cast v0, Lcom/squareup/workflow/InflaterDelegate;

    return-object v0
.end method
