.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;
.super Ljava/lang/Object;
.source "EmoneyAddTenderProcessor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J&\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u000e\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u000e\u0010!\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0017R(\u0010\u0007\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\n \u000b*\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;",
        "",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)V",
        "_serverResponse",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "kotlin.jvm.PlatformType",
        "serverResponse",
        "Lio/reactivex/Observable;",
        "getServerResponse",
        "()Lio/reactivex/Observable;",
        "addTender",
        "",
        "billPayment",
        "Lcom/squareup/payment/BillPayment;",
        "cardData",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "brand",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "authorizeTender",
        "convertCardData",
        "Lokio/ByteString;",
        "dropAndResetTender",
        "dropTender",
        "getServerEmoneyBrand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "resetTender",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _serverResponse:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tenderFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 31
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Succ\u2026re<AddTendersResponse>>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->_serverResponse:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$get_serverResponse$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->_serverResponse:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method private final convertCardData([B)Lokio/ByteString;
    .locals 3

    .line 108
    sget-object v0, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lokio/ByteString$Companion;->of([BII)Lokio/ByteString;

    move-result-object p1

    return-object p1
.end method

.method private final getServerEmoneyBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 100
    instance-of v0, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    goto :goto_0

    .line 101
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    goto :goto_0

    .line 102
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_QP:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public final addTender(Lcom/squareup/payment/BillPayment;[BLcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V
    .locals 2

    const-string v0, "billPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brand"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->convertCardData([B)Lokio/ByteString;

    move-result-object p2

    invoke-direct {p0, p4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->getServerEmoneyBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {v1, p4}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result p4

    .line 45
    invoke-virtual {p1, p2, p3, v0, p4}, Lcom/squareup/payment/BillPayment;->promoteEmoneyTender(Lokio/ByteString;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;I)V

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->authorizeTender(Lcom/squareup/payment/BillPayment;)V

    return-void
.end method

.method public final authorizeTender(Lcom/squareup/payment/BillPayment;)V
    .locals 1

    const-string v0, "billPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->prepareToAuthorize()Lio/reactivex/Observable;

    move-result-object v0

    .line 61
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->authorize()V

    .line 65
    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor$authorizeTender$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor$authorizeTender$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;)V

    check-cast p1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final dropAndResetTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "billPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 88
    invoke-virtual {p1, v0}, Lcom/squareup/payment/BillPayment;->dropLastAddedTender(Z)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 89
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->createEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;

    move-result-object p1

    const-string v0, "emoney"

    .line 90
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 91
    iget-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast p1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {p2, p1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    :cond_0
    return-void
.end method

.method public final dropTender(Lcom/squareup/payment/BillPayment;)V
    .locals 1

    const-string v0, "billPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p1, v0}, Lcom/squareup/payment/BillPayment;->dropLastAddedTender(Z)Lcom/squareup/payment/tender/BaseTender$Builder;

    return-void
.end method

.method public final getServerResponse()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->_serverResponse:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final resetTender(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;

    move-result-object v0

    const-string v1, "emoney"

    .line 76
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 77
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {p1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    return-void
.end method
