.class public final Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;
.super Ljava/lang/Object;
.source "EmoneyBrandUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0005\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;",
        "",
        "()V",
        "getBrandList",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "suicaMax",
        "Lcom/squareup/protos/common/Money;",
        "getDisplayNameForBrand",
        "",
        "emoneyBrand",
        "getTmnBrandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "brand",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getBrandList(Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
            ">;"
        }
    .end annotation

    const-string v0, "suicaMax"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    .line 13
    new-instance v1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 12
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I
    .locals 3

    const-string v0, "emoneyBrand"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    instance-of v0, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;

    if-eqz v0, :cond_0

    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_brand_suica:I

    goto :goto_0

    .line 19
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_brand_id:I

    goto :goto_0

    .line 20
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget p1, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_brand_quicpay:I

    :goto_0
    return p1

    .line 21
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown brand "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getTmnBrandId(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 3

    const-string v0, "brand"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 30
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 31
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    :goto_0
    return-object p1

    .line 32
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown brand "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
