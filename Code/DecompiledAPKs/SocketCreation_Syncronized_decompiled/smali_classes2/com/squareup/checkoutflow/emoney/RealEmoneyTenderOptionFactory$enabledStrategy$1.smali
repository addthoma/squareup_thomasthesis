.class final Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyTenderOptionFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "conditionalData",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;->invoke(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z
    .locals 1

    const-string v0, "conditionalData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    .line 59
    invoke-static {v0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->access$isSplitStateValid(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$enabledStrategy$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->access$getCardReaderHubUtils$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/cardreader/CardReaderHubUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHubUtils;->isPaymentReadyFelicaReaderConnected()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
