.class public final Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEmoneyWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012,\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00080\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u001a\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00032\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J>\u0010\u0012\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00082\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00042\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0004H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "emoneyBrandWorkflow",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;",
        "emoneyCheckBalanceWorkflow",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;",
        "(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;)V",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final emoneyBrandWorkflow:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;

.field private final emoneyCheckBalanceWorkflow:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "emoneyBrandWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyCheckBalanceWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->emoneyBrandWorkflow:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->emoneyCheckBalanceWorkflow:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 37
    sget-object p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;->Companion:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;

    invoke-virtual {p1, p2}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$Companion;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    move-result-object p1

    goto :goto_0

    .line 39
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->initialState(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$BrandSelection;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object p2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->emoneyBrandWorkflow:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 50
    new-instance v2, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;->getEmoneyMin()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;->getSuicaMax()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v2, p2, v0, p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    const/4 v3, 0x0

    .line 51
    sget-object p1, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 48
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto :goto_0

    .line 58
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState$CheckBalance;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 59
    iget-object p2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->emoneyCheckBalanceWorkflow:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 60
    new-instance v2, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;

    .line 61
    new-instance p2, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;->getSuicaMax()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p2, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v0, "input.money.currency_code"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {v2, p2, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/CurrencyCode;)V

    const/4 v3, 0x0

    .line 63
    sget-object p1, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$2;->INSTANCE:Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow$render$2;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 58
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->render(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;->snapshotState(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
