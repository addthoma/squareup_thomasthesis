.class public final Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$smsTextWatcher$1;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "InstallmentsSmsInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$smsTextWatcher$1",
        "Lcom/squareup/text/ScrubbingTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/InsertingScrubber;",
            "Lcom/squareup/text/HasSelectableText;",
            ")V"
        }
    .end annotation

    .line 54
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$smsTextWatcher$1;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "editable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$smsTextWatcher$1;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->access$updateSendEnabled(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;)V

    return-void
.end method
