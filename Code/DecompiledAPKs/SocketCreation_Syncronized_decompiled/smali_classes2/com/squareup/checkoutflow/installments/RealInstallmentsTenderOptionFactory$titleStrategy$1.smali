.class final Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealInstallmentsTenderOptionFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "conditionalData",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;->invoke(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Ljava/lang/String;
    .locals 2

    const-string v0, "conditionalData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 45
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->access$getRes$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_select_tender_title_enabled:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 40
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->access$getRes$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_select_tender_title_disabled:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 41
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->access$getRes$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$string;->online_only_reason:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "disabled_reason"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method
