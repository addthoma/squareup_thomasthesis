.class public final Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "InstallmentsViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "installmentsOptionsFactory",
        "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;",
        "installmentsLinkOptionsFactory",
        "Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;",
        "installmentsQRCodeFactory",
        "Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;",
        "installmentsSmsCodeFactory",
        "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;",
        "installmentsSmsSentFactory",
        "Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;",
        "(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;)V
    .locals 22
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    const-string v5, "installmentsOptionsFactory"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "installmentsLinkOptionsFactory"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "installmentsQRCodeFactory"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "installmentsSmsCodeFactory"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "installmentsSmsSentFactory"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x5

    new-array v5, v5, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 28
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 29
    sget-object v7, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$Companion;

    invoke-virtual {v7}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 30
    sget v8, Lcom/squareup/checkoutflow/installments/impl/R$layout;->installments_options_view:I

    .line 31
    new-instance v21, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1df

    const/16 v20, 0x0

    move-object/from16 v9, v21

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 32
    new-instance v9, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$1;

    invoke-direct {v9, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$1;-><init>(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;)V

    move-object v11, v9

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/16 v12, 0x8

    move-object/from16 v9, v21

    .line 28
    invoke-static/range {v6 .. v13}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v6, 0x0

    aput-object v0, v5, v6

    .line 34
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 35
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 36
    sget v9, Lcom/squareup/checkoutflow/installments/impl/R$layout;->installments_link_options_view:I

    .line 37
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x19f

    const/16 v21, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 38
    new-instance v6, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$2;

    invoke-direct {v6, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$2;-><init>(Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;)V

    move-object v12, v6

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 34
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v5, v1

    .line 40
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 41
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 42
    sget v8, Lcom/squareup/checkoutflow/installments/impl/R$layout;->installments_qr_code_view:I

    .line 43
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x19f

    const/16 v20, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 44
    new-instance v1, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$3;

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$3;-><init>(Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/16 v12, 0x8

    .line 40
    invoke-static/range {v6 .. v13}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v5, v1

    .line 46
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 47
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 48
    sget v8, Lcom/squareup/checkoutflow/installments/impl/R$layout;->installments_sms_input_view:I

    .line 49
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 50
    new-instance v1, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$4;

    invoke-direct {v1, v3}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$4;-><init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/16 v12, 0x8

    .line 46
    invoke-static/range {v6 .. v13}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v5, v1

    .line 52
    sget-object v6, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 53
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 54
    sget v8, Lcom/squareup/checkoutflow/installments/impl/R$layout;->installments_sms_sent_view:I

    .line 55
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 56
    new-instance v1, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$5;

    invoke-direct {v1, v4}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory$5;-><init>(Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/16 v12, 0x8

    .line 52
    invoke-static/range {v6 .. v13}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v5, v1

    move-object/from16 v0, p0

    .line 27
    invoke-direct {v0, v5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
