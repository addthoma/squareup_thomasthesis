.class public final Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InstallmentsQRCodeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001!B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0019H\u0016J\u0010\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0019H\u0002J\u0008\u0010\u001f\u001a\u00020\u001cH\u0002J\u0018\u0010 \u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00192\u0006\u0010\r\u001a\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "data",
        "errorGlyph",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "hint",
        "Lcom/squareup/marketfont/MarketTextView;",
        "link",
        "Lcom/squareup/noho/NohoButton;",
        "qrContainer",
        "Landroid/widget/LinearLayout;",
        "qrImage",
        "Landroid/widget/ImageView;",
        "qrSpinner",
        "Landroid/view/View;",
        "title",
        "attach",
        "",
        "view",
        "bindViews",
        "hideQrViews",
        "update",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private errorGlyph:Landroidx/appcompat/widget/AppCompatImageView;

.field private hint:Lcom/squareup/marketfont/MarketTextView;

.field private link:Lcom/squareup/noho/NohoButton;

.field private qrContainer:Landroid/widget/LinearLayout;

.field private qrImage:Landroid/widget/ImageView;

.field private qrSpinner:Landroid/view/View;

.field private final res:Lcom/squareup/util/Res;

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    .line 42
    sget-object p2, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$data$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$data$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "screens.map { it.data }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->data:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 121
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 122
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_qr_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_qr_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 123
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_qr_hint:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_qr_hint)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->hint:Lcom/squareup/marketfont/MarketTextView;

    .line 124
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_qr_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_qr_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrContainer:Landroid/widget/LinearLayout;

    .line 125
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_qr_spinner:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_qr_spinner)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrSpinner:Landroid/view/View;

    .line 126
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_qr_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_qr_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrImage:Landroid/widget/ImageView;

    .line 127
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_qr_error_glyph:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.i\u2026tallments_qr_error_glyph)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->errorGlyph:Landroidx/appcompat/widget/AppCompatImageView;

    .line 128
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_text_link:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.installments_text_link)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->link:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final hideQrViews()V
    .locals 3

    .line 115
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrSpinner:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "qrSpinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrImage:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const-string v2, "qrImage"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->errorGlyph:Landroidx/appcompat/widget/AppCompatImageView;

    if-nez v0, :cond_2

    const-string v2, "errorGlyph"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setVisibility(I)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;)V
    .locals 6

    .line 64
    sget-object v0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_0

    const-string v0, "title"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_scan_prompt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrContainer:Landroid/widget/LinearLayout;

    const-string v0, "qrContainer"

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 68
    invoke-direct {p0}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->hideQrViews()V

    .line 70
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->getQrState()Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    move-result-object p1

    .line 72
    instance-of p2, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoading;

    const/4 v1, 0x0

    if-eqz p2, :cond_3

    .line 73
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrSpinner:Landroid/view/View;

    if-nez p1, :cond_2

    const-string p2, "qrSpinner"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 75
    :cond_3
    instance-of p2, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoaded;

    const-string v2, "buyerActionBar"

    const-string v3, "hint"

    const-string v4, "link"

    if-eqz p2, :cond_b

    .line 76
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->hint:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_qr_code_loaded_hint:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p2, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p2, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 78
    :cond_5
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/checkout/R$string;->new_sale:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 79
    new-instance v3, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$2;

    invoke-direct {v3, p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 77
    invoke-virtual {p2, v2, v3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 83
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->link:Lcom/squareup/noho/NohoButton;

    if-nez p2, :cond_6

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_link_instead:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p2, v2}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->link:Lcom/squareup/noho/NohoButton;

    if-nez p2, :cond_7

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance v2, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$3;

    invoke-direct {v2, p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoaded;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoaded;->getQrCode()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 91
    array-length p2, p1

    invoke-static {p1, v1, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 92
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrImage:Landroid/widget/ImageView;

    const-string v2, "qrImage"

    if-nez p2, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrImage:Landroid/widget/ImageView;

    if-nez p1, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 94
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->qrContainer:Landroid/widget/LinearLayout;

    if-nez p1, :cond_a

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$drawable;->installments_qr_code_background:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 96
    :cond_b
    instance-of p2, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrError;

    if-eqz p2, :cond_11

    .line 97
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->hint:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_c

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_qr_code_error_hint:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p2, :cond_d

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$4;

    invoke-direct {v2, p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$4;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p2, v0, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 102
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->link:Lcom/squareup/noho/NohoButton;

    if-nez p2, :cond_e

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_link_instead:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->link:Lcom/squareup/noho/NohoButton;

    if-nez p2, :cond_f

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    new-instance v0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$5;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$update$5;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->errorGlyph:Landroidx/appcompat/widget/AppCompatImageView;

    if-nez p1, :cond_10

    const-string p2, "errorGlyph"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setVisibility(I)V

    :cond_11
    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->bindViews(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;->data:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
