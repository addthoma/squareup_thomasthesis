.class public Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;
.super Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;
.source "PayCreditCardPresenter.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter<",
        "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;",
        ">;",
        "Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;"
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

.field private final nfcReaderHasConnectedSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field private final paymentProcessingStarter:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;

.field private final r6HasConnectedSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final restartNfcMonitoringWhenCardRemoved:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

.field private screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

.field private settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cnp/LinkSpanDataHelper;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v12, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p11

    move-object/from16 v8, p16

    move-object/from16 v9, p19

    move-object/from16 v10, p17

    move-object/from16 v11, p23

    .line 121
    invoke-direct/range {v0 .. v11}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;-><init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/payment/TenderInEdit;Ljavax/inject/Provider;)V

    .line 79
    new-instance v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V

    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->restartNfcMonitoringWhenCardRemoved:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    move-object/from16 v0, p5

    .line 124
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v0, p7

    .line 125
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->bus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v0, p8

    .line 126
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    move-object/from16 v0, p9

    .line 127
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->r6HasConnectedSetting:Lcom/squareup/settings/LocalSetting;

    move-object/from16 v0, p10

    .line 128
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->nfcReaderHasConnectedSetting:Lcom/squareup/settings/LocalSetting;

    move-object/from16 v0, p12

    .line 129
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-object/from16 v0, p13

    .line 130
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    move-object/from16 v0, p14

    .line 131
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object/from16 v0, p15

    .line 132
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-object/from16 v0, p16

    .line 133
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v0, p18

    .line 134
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v0, p20

    .line 135
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    move-object/from16 v0, p21

    .line 136
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-object/from16 v0, p22

    .line 137
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

    move-object/from16 v0, p24

    .line 138
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentProcessingStarter:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)Lcom/squareup/ui/main/errors/PaymentInputHandler;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    return-object p0
.end method

.method private enableNfcField()Z
    .locals 3

    .line 259
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 260
    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    xor-int/lit8 v0, v1, 0x1

    return v0

    :cond_0
    return v2
.end method

.method static getCreditCardHintResIdForReader(ZZZ)I
    .locals 0

    if-eqz p2, :cond_1

    if-eqz p0, :cond_0

    .line 305
    sget p0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->ce_card_number_hint_with_dip_and_tap:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->ce_card_number_hint_with_only_dip_and_tap:I

    :goto_0
    return p0

    :cond_1
    if-eqz p1, :cond_3

    if-eqz p0, :cond_2

    .line 309
    sget p0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->ce_card_number_hint_with_dip:I

    goto :goto_1

    :cond_2
    sget p0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->ce_card_number_hint_with_only_swipe_and_dip:I

    :goto_1
    return p0

    :cond_3
    if-eqz p0, :cond_4

    .line 313
    sget p0, Lcom/squareup/widgets/pos/R$string;->ce_card_number_hint:I

    goto :goto_2

    :cond_4
    sget p0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->swipe_only_hint:I

    :goto_2
    return p0
.end method

.method public static synthetic lambda$AIjFKyj9hHS2DG3WIkKf39uV7Rg(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->processTap(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method public static synthetic lambda$Hn669-Try76-4t3sKmpXBz-48Og(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->processEmvDip(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method private processEmvDip(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 278
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 280
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForSplitTenderEmvPayment()V

    goto :goto_0

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForEmvPayment(Lcom/squareup/cardreader/CardReaderInfo;)V

    :goto_0
    return-void
.end method

.method private processTap(Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method


# virtual methods
.method public contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method getStrategy()Lcom/squareup/register/widgets/card/PanValidationStrategy;
    .locals 1

    .line 216
    new-instance v0, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v0}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    return-object v0
.end method

.method public bridge synthetic giftCardOnFileSelected()V
    .locals 0

    .line 59
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->giftCardOnFileSelected()V

    return-void
.end method

.method isGiftCardPayment()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic isThirdPartyGiftCardFeatureEnabled()Z
    .locals 1

    .line 59
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    return v0
.end method

.method onAvailabilityChanged(Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;)V
    .locals 0

    .line 225
    iget-boolean p1, p1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    if-eqz p1, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 227
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->availabilityOffline()V

    :cond_0
    return-void
.end method

.method public onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V
    .locals 1

    .line 232
    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PartialCard;->isBlank()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->promptForPayment()V

    goto :goto_0

    .line 235
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v0, Lcom/squareup/comms/protos/common/TenderType;->CARD_NOT_PRESENT:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    :goto_0
    return-void
.end method

.method public bridge synthetic onCardInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 0

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardInvalid(Lcom/squareup/Card$PanWarning;)V

    return-void
.end method

.method public bridge synthetic onCardValid(Lcom/squareup/Card;)V
    .locals 0

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardValid(Lcom/squareup/Card;)V

    return-void
.end method

.method public onChargeCard(Lcom/squareup/Card;)V
    .locals 3

    .line 220
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentProcessingStarter:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;

    new-instance v1, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)V

    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;->startPayment(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$ja-KoTEUm2NushbAS4CqNO53OJQ;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$ja-KoTEUm2NushbAS4CqNO53OJQ;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V

    .line 183
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 182
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V

    .line 186
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->enableNfcField()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReinsertChipCardToCharge()Z

    .line 191
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->restartNfcMonitoringWhenCardRemoved:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    goto :goto_0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithNfcFieldOn(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    .line 195
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$vyy9aDWI4s2W-JT3iYqHIXzHA8w;->INSTANCE:Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$vyy9aDWI4s2W-JT3iYqHIXzHA8w;

    .line 196
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardPresenter$Hn669-Try76-4t3sKmpXBz-48Og;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardPresenter$Hn669-Try76-4t3sKmpXBz-48Og;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V

    .line 197
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 194
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    .line 199
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$dakt9vkpAaF3xdpytuXY50Yvnso;->INSTANCE:Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$dakt9vkpAaF3xdpytuXY50Yvnso;

    .line 200
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardPresenter$AIjFKyj9hHS2DG3WIkKf39uV7Rg;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardPresenter$AIjFKyj9hHS2DG3WIkKf39uV7Rg;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V

    .line 201
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 198
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    .line 208
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 142
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Shown PayCardCreditScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 145
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;

    .line 146
    iget-object v0, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    .line 147
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;->getIgnoreTransactionLimits()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setIgnoreTransactionLimits(Z)V

    .line 149
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;

    if-nez p1, :cond_0

    .line 151
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->requestInitialFocus()V

    .line 154
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    .line 155
    invoke-virtual {v2}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;->getTitle()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 154
    invoke-interface {v1, v2, v3}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 156
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 157
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    .line 158
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 154
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 161
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->hasCard()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->updateChargeButton(Z)V

    .line 163
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->nfcReaderHasConnectedSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 164
    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->r6HasConnectedSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 165
    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v2}, Lcom/squareup/api/ApiTransactionState;->cardNotPresentSupported()Z

    move-result v2

    .line 166
    iget-object v4, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 167
    invoke-virtual {v5}, Lcom/squareup/api/ApiTransactionState;->cardSupported()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 168
    invoke-static {v2, v1, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getCreditCardHintResIdForReader(ZZZ)I

    move-result p1

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/common/strings/R$string;->ce_card_number_only_hint:I

    .line 166
    :goto_0
    invoke-interface {v4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 170
    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->setHint(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v0, v3}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->hideGlyphBasedOnHint(Z)V

    .line 174
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    .line 175
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;->getLinkSpanData()Lcom/squareup/cnp/LinkSpanData;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/cnp/LinkSpanDataHelper;->formatLinkSpanData(Lcom/squareup/cnp/LinkSpanData;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 176
    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->setHelperText(Ljava/lang/CharSequence;)V

    .line 178
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->x2ClearTender()V

    return-void
.end method

.method public bridge synthetic onPanValid(Lcom/squareup/Card;Z)Z
    .locals 0

    .line 59
    invoke-super {p0, p1, p2}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onPanValid(Lcom/squareup/Card;Z)Z

    move-result p1

    return p1
.end method

.method onTenderButtonClicked()V
    .locals 1

    .line 251
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;

    .line 252
    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 253
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreenView;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->onChargeCard(Lcom/squareup/Card;)V

    return-void
.end method

.method onWindowFocusChanged(Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 241
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 242
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->hasRequestedAuthorization()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 246
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->HIDDEN:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/SoftInputPresenter;->setSoftInputMode(Lcom/squareup/workflow/SoftInputMode;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic promptForPayment()V
    .locals 0

    .line 59
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->promptForPayment()V

    return-void
.end method
