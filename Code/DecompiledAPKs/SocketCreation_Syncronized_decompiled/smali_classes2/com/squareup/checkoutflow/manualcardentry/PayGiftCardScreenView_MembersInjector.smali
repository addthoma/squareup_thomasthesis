.class public final Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;
.super Ljava/lang/Object;
.source "PayGiftCardScreenView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;",
        ">;"
    }
.end annotation


# instance fields
.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->countryCodeProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCountryCode(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    return-void
.end method

.method public static injectThirdPartyGiftCardStrategyProvider(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .line 55
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-static {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->injectPresenter(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/CountryCode;

    invoke-static {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->injectCountryCode(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;Lcom/squareup/CountryCode;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->injectThirdPartyGiftCardStrategyProvider(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;Ljavax/inject/Provider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView_MembersInjector;->injectMembers(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V

    return-void
.end method
