.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "OrderPaymentWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BootstrapScreen"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "startArgs",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final startArgs:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V
    .locals 1

    const-string v0, "startArgs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;->startArgs:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->Companion:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    move-result-object p1

    .line 56
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;->startArgs:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;

    invoke-virtual {p1, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->start(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;->getParentKey()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;

    move-result-object v0

    return-object v0
.end method
