.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrdersPaymentStarter;
.super Ljava/lang/Object;
.source "RealOrdersPaymentStarter.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrdersPaymentStarter;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;",
        "flow",
        "Lflow/Flow;",
        "(Lflow/Flow;)V",
        "start",
        "",
        "orderPaymentProps",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrdersPaymentStarter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public start(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V
    .locals 2

    const-string v0, "orderPaymentProps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrdersPaymentStarter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
