.class public final Lcom/squareup/checkoutflow/core/auth/AuthNetworking$DefaultImpls;
.super Ljava/lang/Object;
.source "AuthNetworking.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/core/auth/AuthNetworking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static doesSameWorkAs(Lcom/squareup/checkoutflow/core/auth/AuthNetworking;Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/auth/AuthNetworking;",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/workflow/Worker;

    invoke-static {p0, p1}, Lcom/squareup/workflow/Worker$DefaultImpls;->doesSameWorkAs(Lcom/squareup/workflow/Worker;Lcom/squareup/workflow/Worker;)Z

    move-result p0

    return p0
.end method
