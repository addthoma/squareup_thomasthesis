.class public final Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;
.super Ljava/lang/Object;
.source "CheckoutFlowServiceModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;",
        "",
        "()V",
        "checkoutFrontEndService",
        "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
        "serviceCreator",
        "Lretrofit2/Retrofit;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;->INSTANCE:Lcom/squareup/checkoutflow/core/services/CheckoutFlowServiceModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final checkoutFrontEndService(Lretrofit2/Retrofit;)Lcom/squareup/checkoutflow/core/services/CheckoutFeService;
    .locals 1
    .param p1    # Lretrofit2/Retrofit;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "serviceCreator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    const-class v0, Lcom/squareup/checkoutflow/core/services/CheckoutFeService;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "serviceCreator.create(Ch\u2026outFeService::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/checkoutflow/core/services/CheckoutFeService;

    return-object p1
.end method
