.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderPaymentWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p5, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;)Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;
    .locals 7

    .line 54
    new-instance v6, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;-><init>(Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;
    .locals 5

    .line 40
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;

    iget-object v4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->newInstance(Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;)Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow_Factory;->get()Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;

    move-result-object v0

    return-object v0
.end method
