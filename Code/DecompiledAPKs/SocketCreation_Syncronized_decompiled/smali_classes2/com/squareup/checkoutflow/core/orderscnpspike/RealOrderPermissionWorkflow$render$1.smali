.class final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderPermissionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/permissionworkflow/PermissionRequestOutput;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/checkoutflow/core/error/OrderPermissionWorkflowOutput;",
        "it",
        "Lcom/squareup/permissionworkflow/PermissionRequestOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/permissionworkflow/PermissionRequestOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    instance-of v0, p1, Lcom/squareup/permissionworkflow/PermissionRequestOutput$Success;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;

    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1$1;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 32
    :cond_0
    instance-of p1, p1, Lcom/squareup/permissionworkflow/PermissionRequestOutput$Cancelled;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow;

    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1$2;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/permissionworkflow/PermissionRequestOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPermissionWorkflow$render$1;->invoke(Lcom/squareup/permissionworkflow/PermissionRequestOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
