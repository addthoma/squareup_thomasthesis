.class public final Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;
.super Ljava/lang/Object;
.source "StatusLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$Factory;,
        Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/checkoutflow/core/auth/StatusScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00132\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0013\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/checkoutflow/core/auth/StatusScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "glyphMessage",
        "Lcom/squareup/widgets/MessageView;",
        "glyphTitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "spinnerGlyph",
        "Lcom/squareup/marin/widgets/MarinSpinnerGlyph;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$Companion;


# instance fields
.field private final buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final glyphMessage:Lcom/squareup/widgets/MessageView;

.field private final glyphTitle:Lcom/squareup/marketfont/MarketTextView;

.field private final spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->Companion:Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 33
    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.glyph_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphMessage:Lcom/squareup/widgets/MessageView;

    .line 35
    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.glyph_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$layout;->auth_spinner_glyph:I

    const/4 v2, 0x0

    .line 36
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object v0, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    .line 41
    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 42
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 43
    sget-object v0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$1;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void

    .line 36
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.marin.widgets.MarinSpinnerGlyph"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/checkoutflow/core/auth/StatusScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 55
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getSubtitle()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_0

    .line 56
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideSubtitle()V

    goto :goto_0

    .line 58
    :cond_0
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    .line 60
    :goto_0
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideUpGlyph()V

    .line 61
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphTitle:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getGlyphTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getGlyphMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getGlyphMessage()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 64
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphMessage:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 65
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getGlyphMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 67
    :cond_1
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->glyphMessage:Lcom/squareup/widgets/MessageView;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 69
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;->getState()Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState;

    move-result-object p1

    .line 70
    instance-of p2, p1, Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processing;

    if-eqz p2, :cond_2

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->reset()V

    goto :goto_2

    .line 71
    :cond_2
    sget-object p2, Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processed;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processed;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    :cond_3
    :goto_2
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/checkoutflow/core/auth/StatusScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/core/auth/StatusLayoutRunner;->showRendering(Lcom/squareup/checkoutflow/core/auth/StatusScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
