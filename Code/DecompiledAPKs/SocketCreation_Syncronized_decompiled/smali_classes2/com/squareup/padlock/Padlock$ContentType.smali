.class public final enum Lcom/squareup/padlock/Padlock$ContentType;
.super Ljava/lang/Enum;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/padlock/Padlock$ContentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/padlock/Padlock$ContentType;

.field public static final enum MARIN_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

.field public static final enum PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

.field public static final enum TEXT:Lcom/squareup/padlock/Padlock$ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1293
    new-instance v0, Lcom/squareup/padlock/Padlock$ContentType;

    const/4 v1, 0x0

    const-string v2, "TEXT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/padlock/Padlock$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    .line 1294
    new-instance v0, Lcom/squareup/padlock/Padlock$ContentType;

    const/4 v2, 0x1

    const-string v3, "PADLOCK_GLYPH"

    invoke-direct {v0, v3, v2}, Lcom/squareup/padlock/Padlock$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    .line 1295
    new-instance v0, Lcom/squareup/padlock/Padlock$ContentType;

    const/4 v3, 0x2

    const-string v4, "MARIN_GLYPH"

    invoke-direct {v0, v4, v3}, Lcom/squareup/padlock/Padlock$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$ContentType;->MARIN_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/padlock/Padlock$ContentType;

    .line 1292
    sget-object v4, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$ContentType;->MARIN_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/padlock/Padlock$ContentType;->$VALUES:[Lcom/squareup/padlock/Padlock$ContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/padlock/Padlock$ContentType;
    .locals 1

    .line 1292
    const-class v0, Lcom/squareup/padlock/Padlock$ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/padlock/Padlock$ContentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/padlock/Padlock$ContentType;
    .locals 1

    .line 1292
    sget-object v0, Lcom/squareup/padlock/Padlock$ContentType;->$VALUES:[Lcom/squareup/padlock/Padlock$ContentType;

    invoke-virtual {v0}, [Lcom/squareup/padlock/Padlock$ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/padlock/Padlock$ContentType;

    return-object v0
.end method
