.class public final enum Lcom/squareup/padlock/PadlockTypeface$Glyph;
.super Ljava/lang/Enum;
.source "PadlockTypeface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/PadlockTypeface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Glyph"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/padlock/PadlockTypeface$Glyph;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/padlock/PadlockTypeface$Glyph;

.field public static final enum C:Lcom/squareup/padlock/PadlockTypeface$Glyph;

.field public static final enum CHECK:Lcom/squareup/padlock/PadlockTypeface$Glyph;

.field public static final enum PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

.field public static final enum ROUNDED_BACKSPACE:Lcom/squareup/padlock/PadlockTypeface$Glyph;

.field public static final enum ROUNDED_PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;


# instance fields
.field private final string:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 16
    new-instance v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    const/4 v1, 0x0

    const-string v2, "C"

    const/16 v3, 0x63

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/padlock/PadlockTypeface$Glyph;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->C:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 17
    new-instance v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    const/4 v2, 0x1

    const-string v3, "CHECK"

    const/16 v4, 0x3d

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/padlock/PadlockTypeface$Glyph;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->CHECK:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 18
    new-instance v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    const/4 v3, 0x2

    const-string v4, "PLUS"

    const/16 v5, 0x2b

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/padlock/PadlockTypeface$Glyph;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 19
    new-instance v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    const/4 v4, 0x3

    const-string v5, "ROUNDED_BACKSPACE"

    const/16 v6, 0x3c

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/padlock/PadlockTypeface$Glyph;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->ROUNDED_BACKSPACE:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 20
    new-instance v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    const/4 v5, 0x4

    const-string v6, "ROUNDED_PLUS"

    const/16 v7, 0x3e

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/padlock/PadlockTypeface$Glyph;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->ROUNDED_PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 15
    sget-object v6, Lcom/squareup/padlock/PadlockTypeface$Glyph;->C:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/padlock/PadlockTypeface$Glyph;->CHECK:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/PadlockTypeface$Glyph;->PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/padlock/PadlockTypeface$Glyph;->ROUNDED_BACKSPACE:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/padlock/PadlockTypeface$Glyph;->ROUNDED_PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->$VALUES:[Lcom/squareup/padlock/PadlockTypeface$Glyph;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IC)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    invoke-static {p3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->string:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/padlock/PadlockTypeface$Glyph;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/padlock/PadlockTypeface$Glyph;

    return-object p0
.end method

.method public static values()[Lcom/squareup/padlock/PadlockTypeface$Glyph;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->$VALUES:[Lcom/squareup/padlock/PadlockTypeface$Glyph;

    invoke-virtual {v0}, [Lcom/squareup/padlock/PadlockTypeface$Glyph;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/padlock/PadlockTypeface$Glyph;

    return-object v0
.end method


# virtual methods
.method public getLetter()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->string:Ljava/lang/String;

    return-object v0
.end method
