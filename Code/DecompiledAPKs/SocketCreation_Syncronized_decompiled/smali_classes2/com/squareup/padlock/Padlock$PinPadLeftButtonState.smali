.class public final enum Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;
.super Ljava/lang/Enum;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PinPadLeftButtonState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

.field public static final enum CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

.field public static final enum CANCEL_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

.field public static final enum CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

.field public static final enum CLEAR_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 218
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    const/4 v1, 0x0

    const-string v2, "CANCEL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 219
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    const/4 v2, 0x1

    const-string v3, "CANCEL_DISABLED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 220
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    const/4 v3, 0x2

    const-string v4, "CLEAR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 221
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    const/4 v4, 0x3

    const-string v5, "CLEAR_DISABLED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 217
    sget-object v5, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->$VALUES:[Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;
    .locals 1

    .line 217
    const-class v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;
    .locals 1

    .line 217
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->$VALUES:[Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    invoke-virtual {v0}, [Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    return-object v0
.end method
