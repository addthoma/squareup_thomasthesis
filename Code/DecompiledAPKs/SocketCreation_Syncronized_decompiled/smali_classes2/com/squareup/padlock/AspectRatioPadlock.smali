.class public Lcom/squareup/padlock/AspectRatioPadlock;
.super Lcom/squareup/padlock/Padlock;
.source "AspectRatioPadlock.java"


# static fields
.field private static final NO_ASPECT_RATIO:F = -1.0f


# instance fields
.field private final aspectRatio:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/padlock/Padlock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    sget-object v0, Lcom/squareup/padlock/R$styleable;->AspectRatioPadlock:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 20
    sget p2, Lcom/squareup/padlock/R$styleable;->AspectRatioPadlock_sq_aspectRatio:I

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/padlock/AspectRatioPadlock;->aspectRatio:F

    .line 21
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .line 25
    invoke-super {p0, p1, p2}, Lcom/squareup/padlock/Padlock;->onMeasure(II)V

    .line 28
    iget p1, p0, Lcom/squareup/padlock/AspectRatioPadlock;->aspectRatio:F

    const/high16 p2, -0x40800000    # -1.0f

    cmpl-float p1, p1, p2

    if-nez p1, :cond_0

    return-void

    .line 32
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/padlock/AspectRatioPadlock;->getMeasuredWidth()I

    move-result p1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/padlock/AspectRatioPadlock;->getMeasuredHeight()I

    move-result p2

    int-to-float v0, p1

    int-to-float v1, p2

    .line 38
    iget v2, p0, Lcom/squareup/padlock/AspectRatioPadlock;->aspectRatio:F

    mul-float v3, v1, v2

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    mul-float v1, v1, v2

    float-to-int p1, v1

    goto :goto_0

    :cond_1
    div-float/2addr v0, v2

    float-to-int p2, v0

    :goto_0
    const/high16 v0, 0x40000000    # 2.0f

    .line 47
    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-super {p0, p1, p2}, Lcom/squareup/padlock/Padlock;->onMeasure(II)V

    return-void
.end method
