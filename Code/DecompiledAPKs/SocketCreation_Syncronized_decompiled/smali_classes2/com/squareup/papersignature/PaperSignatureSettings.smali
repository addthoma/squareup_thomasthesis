.class public Lcom/squareup/papersignature/PaperSignatureSettings;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "PaperSignatureSettings.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_TIP_WARN_PERIOD_MILLIS:J = 0x4b87f00L

.field protected static final USE_PRINT_SIGNATURE_SETTING_LOCAL_PREFERENCE:Ljava/lang/String; = "use-print-signature-setting-local"


# instance fields
.field private final accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final usePrintSignatureLocalPref:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/print/PrinterStations;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x1

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    .line 38
    iput-object p2, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 39
    iput-object p3, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 40
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->features:Lcom/squareup/settings/server/Features;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->getValueFromDeviceProfile()Ljava/lang/Boolean;

    move-result-object p1

    if-nez p1, :cond_0

    move-object p1, v0

    :cond_0
    const-string/jumbo p2, "use-print-signature-setting-local"

    .line 50
    invoke-virtual {p4, p2, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->usePrintSignatureLocalPref:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method


# virtual methods
.method public getNumberOfReceiptPrinters()I
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getNumberOfEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)I

    move-result v0

    return v0
.end method

.method public getTipWarnPeriodMillis()J
    .locals 2

    const-wide/32 v0, 0x4b87f00

    return-wide v0
.end method

.method protected getValueFromDeviceProfile()Ljava/lang/Boolean;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->usePaperSignature()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->getValueFromDeviceProfile()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/Boolean;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->usePrintSignatureLocalPref:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method protected bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->getValueFromLocalSettings()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public hasReceiptPrinter()Z
    .locals 4

    .line 89
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isPrintAdditionalAuthSlipEnabled()Z
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignaturePrintAdditionalAuthSlip()Z

    move-result v0

    return v0
.end method

.method public isQuickTipEnabled()Z
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SignatureSettings;->forPaperSignatureQuickTipReceipt()Z

    move-result v0

    return v0
.end method

.method public isSignOnPrintedReceiptEnabled()Z
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public setSignOnPrintedReceiptEnabled(Z)V
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->useDeviceProfile()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureSettings;->setValueLocally(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected setValueLocallyInternal(Ljava/lang/Boolean;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->usePrintSignatureLocalPref:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureSettings;->setValueLocallyInternal(Ljava/lang/Boolean;)V

    return-void
.end method

.method public shouldPrintReceiptToSign()Z
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->hasReceiptPrinter()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public useDeviceProfile()Z
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_LOCAL_PAPER_SIGNATURE_SETTING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
