.class public final Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;
.super Ljava/lang/Object;
.source "TenderStatusCacheUpdater_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
        ">;"
    }
.end annotation


# instance fields
.field private final cacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->cacheProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->paperSignatureServiceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/papersignature/TenderStatusCacheUpdater;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;-><init>(Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/thread/executor/MainThread;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/papersignature/TenderStatusCacheUpdater;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->cacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/papersignature/TenderStatusCache;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->paperSignatureServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    iget-object v2, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2}, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->newInstance(Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater_Factory;->get()Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    move-result-object v0

    return-object v0
.end method
