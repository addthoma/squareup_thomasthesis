.class public final Lcom/squareup/papersignature/TenderStatusManager_Factory;
.super Ljava/lang/Object;
.source "TenderStatusManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/papersignature/TenderStatusManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final expiryCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStatusCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStatusCacheUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final tendersAwaitingTipCountSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->tendersAwaitingTipCountSchedulerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->tenderStatusCacheProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->tenderStatusCacheUpdaterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/papersignature/TenderStatusManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;)",
            "Lcom/squareup/papersignature/TenderStatusManager_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/papersignature/TenderStatusManager_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/papersignature/TenderStatusManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/activity/ExpiryCalculator;)Lcom/squareup/papersignature/TenderStatusManager;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/papersignature/TenderStatusManager;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/papersignature/TenderStatusManager;-><init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/activity/ExpiryCalculator;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/papersignature/TenderStatusManager;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->tendersAwaitingTipCountSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->tenderStatusCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/TenderStatusCache;

    iget-object v2, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->tenderStatusCacheUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    iget-object v3, p0, Lcom/squareup/papersignature/TenderStatusManager_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/activity/ExpiryCalculator;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/papersignature/TenderStatusManager_Factory;->newInstance(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/activity/ExpiryCalculator;)Lcom/squareup/papersignature/TenderStatusManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/papersignature/TenderStatusManager_Factory;->get()Lcom/squareup/papersignature/TenderStatusManager;

    move-result-object v0

    return-object v0
.end method
