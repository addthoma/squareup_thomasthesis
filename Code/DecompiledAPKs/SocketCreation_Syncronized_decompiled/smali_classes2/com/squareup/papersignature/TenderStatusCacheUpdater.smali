.class public Lcom/squareup/papersignature/TenderStatusCacheUpdater;
.super Ljava/lang/Object;
.source "TenderStatusCacheUpdater.java"


# static fields
.field static final POLL_TENDER_STATUS_INITIAL_INTERVAL_SEC:J = 0x2L

.field static final POLL_TENDER_STATUS_MAX_INTERVAL_SEC:J = 0x12cL


# instance fields
.field private final cache:Lcom/squareup/papersignature/TenderStatusCache;

.field private currentPollingTask:Ljava/lang/Runnable;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

.field private pollTenderStatusIntervalSec:J


# direct methods
.method public constructor <init>(Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/thread/executor/MainThread;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x2

    .line 40
    iput-wide v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->pollTenderStatusIntervalSec:J

    .line 44
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cache:Lcom/squareup/papersignature/TenderStatusCache;

    .line 45
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    .line 46
    iput-object p3, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/papersignature/TenderStatusCacheUpdater;Ljava/util/List;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->onTenderStatusSuccess(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/papersignature/TenderStatusCacheUpdater;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->scheduleNextUpdateIfNecessaryWithBackoff()V

    return-void
.end method

.method static getProcessedTenderIds(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 144
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;

    .line 145
    iget-object v2, v1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    sget-object v3, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSED:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    if-ne v2, v3, :cond_0

    .line 146
    iget-object v1, v1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->tender_id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private onTenderStatusSuccess(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;)V"
        }
    .end annotation

    .line 136
    invoke-static {p1}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->getProcessedTenderIds(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 137
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {v0, p1}, Lcom/squareup/papersignature/TenderStatusCache;->markTendersAsProcessed(Ljava/util/List;)V

    return-void
.end method

.method private scheduleNextUpdateIfNecessaryWithBackoff()V
    .locals 6

    .line 117
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 119
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->currentPollingTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {v0}, Lcom/squareup/papersignature/TenderStatusCache;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 129
    :cond_1
    new-instance v0, Lcom/squareup/papersignature/-$$Lambda$psOImD46M8Jn5fVjexn3eGOH1Us;

    invoke-direct {v0, p0}, Lcom/squareup/papersignature/-$$Lambda$psOImD46M8Jn5fVjexn3eGOH1Us;-><init>(Lcom/squareup/papersignature/TenderStatusCacheUpdater;)V

    iput-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->currentPollingTask:Ljava/lang/Runnable;

    .line 130
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->currentPollingTask:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->pollTenderStatusIntervalSec:J

    const-wide/16 v4, 0x3e8

    mul-long v2, v2, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    iget-wide v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->pollTenderStatusIntervalSec:J

    mul-long v0, v0, v0

    const-wide/16 v2, 0x12c

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->pollTenderStatusIntervalSec:J

    return-void
.end method


# virtual methods
.method buildRequestList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/BillAndTender;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {v0}, Lcom/squareup/papersignature/TenderStatusCache;->getUnprocessedTenderAndBillIds()Ljava/util/Map;

    move-result-object v0

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 96
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 97
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 98
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 100
    new-instance v4, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 101
    invoke-virtual {v4, v3}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v3

    .line 102
    invoke-virtual {v3}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 103
    new-instance v4, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 104
    invoke-virtual {v4, v2}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v2

    .line 105
    invoke-virtual {v2}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 107
    new-instance v4, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;-><init>()V

    .line 108
    invoke-virtual {v4, v3}, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;

    move-result-object v3

    .line 109
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;

    move-result-object v2

    .line 110
    invoke-virtual {v2}, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->build()Lcom/squareup/protos/client/paper_signature/BillAndTender;

    move-result-object v2

    .line 107
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public cancel()V
    .locals 2

    .line 56
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 58
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->currentPollingTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 59
    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->currentPollingTask:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method requestStatusUpdate()V
    .locals 3

    .line 65
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 67
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {v0}, Lcom/squareup/papersignature/TenderStatusCache;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 72
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;-><init>()V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->buildRequestList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->bill_and_tender_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->cache:Lcom/squareup/papersignature/TenderStatusCache;

    .line 74
    invoke-virtual {v1}, Lcom/squareup/papersignature/TenderStatusCache;->getUnprocessedTenderIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->tender_id_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;

    move-result-object v0

    const/4 v1, 0x0

    .line 77
    iput-object v1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->currentPollingTask:Ljava/lang/Runnable;

    .line 78
    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->paperSignatureService:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    new-instance v2, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;

    invoke-direct {v2, p0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;-><init>(Lcom/squareup/papersignature/TenderStatusCacheUpdater;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->tenderStatus(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method

.method public scheduleNextUpdateIfNecessary()V
    .locals 2

    const-wide/16 v0, 0x2

    .line 51
    iput-wide v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->pollTenderStatusIntervalSec:J

    .line 52
    invoke-direct {p0}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->scheduleNextUpdateIfNecessaryWithBackoff()V

    return-void
.end method
