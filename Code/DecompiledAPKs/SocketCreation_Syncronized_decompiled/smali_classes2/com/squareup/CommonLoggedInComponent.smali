.class public interface abstract Lcom/squareup/CommonLoggedInComponent;
.super Ljava/lang/Object;
.source "CommonLoggedInComponent.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$Component;
.implements Lcom/squareup/RegisterAppDelegateLoggedInComponent;
.implements Lcom/squareup/RegisterAppDelegate$ParentLoggedInComponent;
.implements Lcom/squareup/ui/SquareActivity$ParentLoggedInComponent;
.implements Lcom/squareup/api/ApiValidationLoggedInComponent;


# virtual methods
.method public abstract locationActivity()Lcom/squareup/ui/LocationActivity$Component;
.end method

.method public abstract loggedInScopeRunner()Lcom/squareup/LoggedInScopeRunner;
.end method

.method public abstract mainActivity()Lcom/squareup/ui/main/CommonMainActivityComponent;
.end method
