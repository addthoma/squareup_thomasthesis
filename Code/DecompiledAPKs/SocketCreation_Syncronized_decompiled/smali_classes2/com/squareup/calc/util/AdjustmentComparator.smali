.class public Lcom/squareup/calc/util/AdjustmentComparator;
.super Ljava/lang/Object;
.source "AdjustmentComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/calc/order/Adjustment;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMPARATOR:Lcom/squareup/calc/util/AdjustmentComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/calc/util/AdjustmentComparator;

    invoke-direct {v0}, Lcom/squareup/calc/util/AdjustmentComparator;-><init>()V

    sput-object v0, Lcom/squareup/calc/util/AdjustmentComparator;->COMPARATOR:Lcom/squareup/calc/util/AdjustmentComparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)Z"
        }
    .end annotation

    if-eq p0, p1, :cond_1

    if-eqz p0, :cond_0

    .line 94
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static equalByCompare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "TT;>;>(TT;TT;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 103
    :cond_0
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result p0

    if-nez p0, :cond_2

    goto :goto_1

    :cond_1
    :goto_0
    if-ne p0, p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public static sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/calc/order/Adjustment;",
            ">(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;)",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 34
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/calc/order/Adjustment;

    .line 35
    invoke-interface {v1}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static sortToCalculationOrder(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/calc/order/Adjustment;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 23
    sget-object p0, Lcom/squareup/calc/util/AdjustmentComparator;->COMPARATOR:Lcom/squareup/calc/util/AdjustmentComparator;

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method public compare(Lcom/squareup/calc/order/Adjustment;Lcom/squareup/calc/order/Adjustment;)I
    .locals 5

    .line 48
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v0

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/calc/constants/CalculationPhase;->ordinal()I

    move-result p1

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/calc/constants/CalculationPhase;->ordinal()I

    move-result p2

    sub-int/2addr p1, p2

    return p1

    .line 51
    :cond_0
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->priority()Lcom/squareup/calc/constants/CalculationPriority;

    move-result-object v0

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->priority()Lcom/squareup/calc/constants/CalculationPriority;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/calc/util/AdjustmentComparator;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-nez v0, :cond_3

    .line 52
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->priority()Lcom/squareup/calc/constants/CalculationPriority;

    move-result-object v0

    if-nez v0, :cond_1

    return v2

    .line 53
    :cond_1
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->priority()Lcom/squareup/calc/constants/CalculationPriority;

    move-result-object v0

    if-nez v0, :cond_2

    return v1

    .line 54
    :cond_2
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->priority()Lcom/squareup/calc/constants/CalculationPriority;

    move-result-object p1

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->priority()Lcom/squareup/calc/constants/CalculationPriority;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/calc/constants/CalculationPriority;->compareTo(Ljava/lang/Enum;)I

    move-result p1

    return p1

    .line 58
    :cond_3
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;

    move-result-object v0

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/calc/util/AdjustmentComparator;->equalByCompare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 59
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;

    move-result-object v0

    if-nez v0, :cond_4

    return v2

    .line 60
    :cond_4
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;

    move-result-object p2

    if-nez p2, :cond_5

    return v1

    .line 61
    :cond_5
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;

    move-result-object p1

    sget-object p2, Lcom/squareup/calc/constants/ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    if-ne p1, p2, :cond_6

    return v2

    :cond_6
    return v1

    .line 66
    :cond_7
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/calc/util/AdjustmentComparator;->equalByCompare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 67
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v0

    if-nez v0, :cond_8

    return v2

    .line 68
    :cond_8
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v0

    if-nez v0, :cond_9

    return v1

    .line 69
    :cond_9
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    return p1

    .line 73
    :cond_a
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->amountAppliesPerItemQuantity()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amountAppliesPerItemQuantity()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/calc/util/AdjustmentComparator;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 74
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->amountAppliesPerItemQuantity()Z

    move-result p1

    if-eqz p1, :cond_b

    const/4 v1, 0x1

    :cond_b
    return v1

    .line 78
    :cond_c
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/calc/util/AdjustmentComparator;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 79
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_d

    return v2

    .line 80
    :cond_d
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_e

    return v1

    .line 81
    :cond_e
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    cmp-long v0, v3, p1

    if-gez v0, :cond_f

    goto :goto_0

    :cond_f
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 85
    :cond_10
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/calc/order/Adjustment;

    check-cast p2, Lcom/squareup/calc/order/Adjustment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/calc/util/AdjustmentComparator;->compare(Lcom/squareup/calc/order/Adjustment;Lcom/squareup/calc/order/Adjustment;)I

    move-result p1

    return p1
.end method
