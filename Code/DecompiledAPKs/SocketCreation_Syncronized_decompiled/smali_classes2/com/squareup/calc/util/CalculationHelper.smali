.class public Lcom/squareup/calc/util/CalculationHelper;
.super Ljava/lang/Object;
.source "CalculationHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static aggregateModifierPrice(Ljava/util/Map;Ljava/math/BigDecimal;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Modifier;",
            ">;",
            "Ljava/math/BigDecimal;",
            ")J"
        }
    .end annotation

    .line 103
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Modifier;

    .line 104
    invoke-interface {v2, p1}, Lcom/squareup/calc/order/Modifier;->totalPrice(Ljava/math/BigDecimal;)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public static itemBaseAmount(JLjava/math/BigDecimal;Ljava/util/Map;)J
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/math/BigDecimal;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Modifier;",
            ">;)J"
        }
    .end annotation

    .line 96
    invoke-static {p0, p1, p2}, Lcom/squareup/calc/util/CalculationHelper;->itemPriceTimesItemQuantity(JLjava/math/BigDecimal;)J

    move-result-wide p0

    .line 97
    invoke-static {p3, p2}, Lcom/squareup/calc/util/CalculationHelper;->aggregateModifierPrice(Ljava/util/Map;Ljava/math/BigDecimal;)J

    move-result-wide p2

    add-long/2addr p0, p2

    return-wide p0
.end method

.method public static itemBaseAmount(Lcom/squareup/calc/order/Item;)J
    .locals 3

    .line 86
    invoke-interface {p0}, Lcom/squareup/calc/order/Item;->price()J

    move-result-wide v0

    invoke-interface {p0}, Lcom/squareup/calc/order/Item;->quantity()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-interface {p0}, Lcom/squareup/calc/order/Item;->appliedModifiers()Ljava/util/Map;

    move-result-object p0

    invoke-static {v0, v1, v2, p0}, Lcom/squareup/calc/util/CalculationHelper;->itemBaseAmount(JLjava/math/BigDecimal;Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static itemPriceTimesItemQuantity(JLjava/math/BigDecimal;)J
    .locals 1

    .line 76
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-static {p0, p1, p2, v0}, Lcom/squareup/calc/util/BigDecimalHelper;->multiply(JLjava/math/BigDecimal;Ljava/math/RoundingMode;)J

    move-result-wide p0

    return-wide p0
.end method

.method public static itemPriceTimesItemQuantity(Lcom/squareup/calc/order/Item;)J
    .locals 2

    .line 62
    invoke-interface {p0}, Lcom/squareup/calc/order/Item;->price()J

    move-result-wide v0

    invoke-interface {p0}, Lcom/squareup/calc/order/Item;->quantity()Ljava/math/BigDecimal;

    move-result-object p0

    invoke-static {v0, v1, p0}, Lcom/squareup/calc/util/CalculationHelper;->itemPriceTimesItemQuantity(JLjava/math/BigDecimal;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static modifierQuantityTimesItemQuantity(ILjava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 2

    int-to-long v0, p0

    .line 31
    invoke-static {v0, v1}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method public static totalModifierPrice(JILjava/math/BigDecimal;)J
    .locals 0

    .line 48
    invoke-static {p2, p3}, Lcom/squareup/calc/util/CalculationHelper;->modifierQuantityTimesItemQuantity(ILjava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p2

    sget-object p3, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    .line 47
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/calc/util/BigDecimalHelper;->multiply(JLjava/math/BigDecimal;Ljava/math/RoundingMode;)J

    move-result-wide p0

    return-wide p0
.end method
