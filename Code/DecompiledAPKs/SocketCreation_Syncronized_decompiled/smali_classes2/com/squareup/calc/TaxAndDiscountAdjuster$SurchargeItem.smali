.class final Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;
.super Ljava/lang/Object;
.source "TaxAndDiscountAdjuster.java"

# interfaces
.implements Lcom/squareup/calc/order/Item;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/calc/TaxAndDiscountAdjuster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SurchargeItem"
.end annotation


# instance fields
.field private final amount:J

.field private final surcharge:Lcom/squareup/calc/order/Surcharge;


# direct methods
.method private constructor <init>(Lcom/squareup/calc/order/Surcharge;J)V
    .locals 0

    .line 492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;->surcharge:Lcom/squareup/calc/order/Surcharge;

    .line 494
    iput-wide p2, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;->amount:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/calc/order/Surcharge;JLcom/squareup/calc/TaxAndDiscountAdjuster$1;)V
    .locals 0

    .line 487
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;-><init>(Lcom/squareup/calc/order/Surcharge;J)V

    return-void
.end method


# virtual methods
.method public appliedDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation

    .line 519
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public appliedModifiers()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Modifier;",
            ">;"
        }
    .end annotation

    .line 514
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public appliedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation

    .line 524
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;->surcharge:Lcom/squareup/calc/order/Surcharge;

    invoke-interface {v0}, Lcom/squareup/calc/order/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public baseAmount()J
    .locals 2

    .line 509
    iget-wide v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;->amount:J

    return-wide v0
.end method

.method public price()J
    .locals 2

    .line 499
    iget-wide v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;->amount:J

    return-wide v0
.end method

.method public quantity()Ljava/math/BigDecimal;
    .locals 1

    .line 504
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    return-object v0
.end method
