.class public interface abstract Lcom/squareup/calc/order/Adjustment;
.super Ljava/lang/Object;
.source "Adjustment.java"


# virtual methods
.method public abstract amount()Ljava/lang/Long;
.end method

.method public abstract amountAppliesPerItemQuantity()Z
.end method

.method public abstract id()Ljava/lang/String;
.end method

.method public abstract inclusionType()Lcom/squareup/calc/constants/InclusionType;
.end method

.method public abstract maxAmount()Ljava/lang/Long;
.end method

.method public abstract phase()Lcom/squareup/calc/constants/CalculationPhase;
.end method

.method public abstract priority()Lcom/squareup/calc/constants/CalculationPriority;
.end method

.method public abstract rate()Ljava/math/BigDecimal;
.end method

.method public abstract taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;
.end method
