.class public Lcom/squareup/account/LoginResponseCacheErrorEvent;
.super Lcom/squareup/analytics/event/v1/ErrorEvent;
.source "LoginResponseCacheErrorEvent.java"


# instance fields
.field private final encryptedFileExists:Z

.field private final errorMessage:Ljava/lang/String;

.field private final eventId:Ljava/lang/String;

.field private final preferEncryptedCache:Z

.field private final throwableMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/analytics/RegisterErrorName;->LOGIN_RESPONSE_ENCRYPTION_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ErrorEvent;-><init>(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 18
    iput-object p1, p0, Lcom/squareup/account/LoginResponseCacheErrorEvent;->throwableMessage:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/squareup/account/LoginResponseCacheErrorEvent;->errorMessage:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/squareup/account/LoginResponseCacheErrorEvent;->eventId:Ljava/lang/String;

    .line 21
    iput-boolean p4, p0, Lcom/squareup/account/LoginResponseCacheErrorEvent;->preferEncryptedCache:Z

    .line 22
    iput-boolean p5, p0, Lcom/squareup/account/LoginResponseCacheErrorEvent;->encryptedFileExists:Z

    return-void
.end method
