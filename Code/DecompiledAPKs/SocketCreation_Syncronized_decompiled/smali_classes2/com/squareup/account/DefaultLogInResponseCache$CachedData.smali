.class public Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
.super Ljava/lang/Object;
.source "DefaultLogInResponseCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/DefaultLogInResponseCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CachedData"
.end annotation


# instance fields
.field public final session_token:Ljava/lang/String;

.field public final status:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    iput-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    .line 515
    iput-object p2, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 0

    .line 508
    invoke-direct {p0, p1}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->withStatus(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object p0

    return-object p0
.end method

.method private withStatus(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 2

    .line 519
    new-instance v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 524
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 525
    :cond_1
    check-cast p1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    .line 526
    iget-object v2, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object p1, p1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 527
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 531
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
