.class public final Lcom/squareup/account/LegacyAuthenticator$DefaultImpls;
.super Ljava/lang/Object;
.source "LegacyAuthenticator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/LegacyAuthenticator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static lastLoggedInStatus(Lcom/squareup/account/LegacyAuthenticator;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
    .locals 0

    check-cast p0, Lcom/squareup/account/LoggedInStatusProvider;

    invoke-static {p0}, Lcom/squareup/account/LoggedInStatusProvider$DefaultImpls;->lastLoggedInStatus(Lcom/squareup/account/LoggedInStatusProvider;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;

    move-result-object p0

    return-object p0
.end method

.method public static logOut(Lcom/squareup/account/LegacyAuthenticator;)V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/account/LogOutReason$Normal;->INSTANCE:Lcom/squareup/account/LogOutReason$Normal;

    check-cast v0, Lcom/squareup/account/LogOutReason;

    invoke-interface {p0, v0}, Lcom/squareup/account/LegacyAuthenticator;->logOut(Lcom/squareup/account/LogOutReason;)V

    return-void
.end method

.method public static synthetic logOut$default(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/account/LogOutReason;ILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 38
    sget-object p1, Lcom/squareup/account/LogOutReason$Normal;->INSTANCE:Lcom/squareup/account/LogOutReason$Normal;

    check-cast p1, Lcom/squareup/account/LogOutReason;

    :cond_0
    invoke-interface {p0, p1}, Lcom/squareup/account/LegacyAuthenticator;->logOut(Lcom/squareup/account/LogOutReason;)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: logOut"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
