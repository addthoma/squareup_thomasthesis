.class public interface abstract Lcom/squareup/account/LegacyAuthenticator;
.super Ljava/lang/Object;
.source "LegacyAuthenticator.kt"

# interfaces
.implements Lcom/squareup/account/SessionIdPIIProvider;
.implements Lcom/squareup/account/LoggedInStatusProvider;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/LegacyAuthenticator$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0012\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH&J\u0018\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH&J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0011H&R\u0012\u0010\u0004\u001a\u00020\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0006\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/account/LegacyAuthenticator;",
        "Lcom/squareup/account/SessionIdPIIProvider;",
        "Lcom/squareup/account/LoggedInStatusProvider;",
        "Lmortar/Scoped;",
        "isLoggedIn",
        "",
        "()Z",
        "logOut",
        "",
        "reason",
        "Lcom/squareup/account/LogOutReason;",
        "loggedIn",
        "sessionToken",
        "",
        "status",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "onLoggingOut",
        "Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isLoggedIn()Z
.end method

.method public abstract logOut()V
.end method

.method public abstract logOut(Lcom/squareup/account/LogOutReason;)V
.end method

.method public abstract loggedIn(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
.end method

.method public abstract onLoggingOut()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method
