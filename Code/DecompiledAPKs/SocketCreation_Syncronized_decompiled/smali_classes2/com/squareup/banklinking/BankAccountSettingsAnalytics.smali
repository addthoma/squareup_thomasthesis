.class public final Lcom/squareup/banklinking/BankAccountSettingsAnalytics;
.super Ljava/lang/Object;
.source "BankAccountSettingsAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\u0006J\u0018\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J\u0010\u0010\u000c\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0008H\u0002J\u000e\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\u000e\u001a\u00020\u0006J\u0006\u0010\u000f\u001a\u00020\u0006J\u000e\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\u0011\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/banklinking/BankAccountSettingsAnalytics;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logAddBankAccountFailure",
        "",
        "error",
        "",
        "logAddBankAccountSuccess",
        "logBankLinkingFailureEvent",
        "description",
        "logBankLinkingSuccessEvent",
        "logChangeBankAccountFailure",
        "logChangeBankAccountSuccess",
        "logCheckPasswordFailure",
        "logOnboardingAddBankAccountFailure",
        "logOnboardingAddBankAccountSuccess",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private final logBankLinkingFailureEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/deposits/BankLinkingResultEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, p2}, Lcom/squareup/log/deposits/BankLinkingResultEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private final logBankLinkingSuccessEvent(Ljava/lang/String;)V
    .locals 4

    .line 49
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/deposits/BankLinkingResultEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/squareup/log/deposits/BankLinkingResultEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method


# virtual methods
.method public final logAddBankAccountFailure(Ljava/lang/String;)V
    .locals 1

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Settings Bank Accounts: Add Bank Account Result"

    .line 37
    invoke-direct {p0, v0, p1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logBankLinkingFailureEvent(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final logAddBankAccountSuccess()V
    .locals 1

    const-string v0, "Settings Bank Accounts: Add Bank Account Result"

    .line 25
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logBankLinkingSuccessEvent(Ljava/lang/String;)V

    return-void
.end method

.method public final logChangeBankAccountFailure(Ljava/lang/String;)V
    .locals 1

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Settings Bank Accounts: Edit Bank Account Result"

    .line 41
    invoke-direct {p0, v0, p1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logBankLinkingFailureEvent(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final logChangeBankAccountSuccess()V
    .locals 1

    const-string v0, "Settings Bank Accounts: Edit Bank Account Result"

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logBankLinkingSuccessEvent(Ljava/lang/String;)V

    return-void
.end method

.method public final logCheckPasswordFailure()V
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Settings Bank Account: Edit Bank Account Password Error"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public final logOnboardingAddBankAccountFailure(Ljava/lang/String;)V
    .locals 1

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Onboard: Add Bank Account Result"

    .line 33
    invoke-direct {p0, v0, p1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logBankLinkingFailureEvent(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final logOnboardingAddBankAccountSuccess()V
    .locals 1

    const-string v0, "Onboard: Add Bank Account Result"

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logBankLinkingSuccessEvent(Ljava/lang/String;)V

    return-void
.end method
