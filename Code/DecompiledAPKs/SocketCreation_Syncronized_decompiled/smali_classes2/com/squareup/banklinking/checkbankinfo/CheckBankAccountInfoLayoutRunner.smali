.class public final Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;
.super Ljava/lang/Object;
.source "CheckBankAccountInfoLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Factory;,
        Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckBankAccountInfoLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckBankAccountInfoLayoutRunner.kt\ncom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner\n*L\n1#1,309:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002/0B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eH\u0002J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010 \u001a\u00020!H\u0002J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0002H\u0002J\u0010\u0010%\u001a\u00020&2\u0006\u0010$\u001a\u00020\u0002H\u0002J\u0010\u0010\'\u001a\u00020#2\u0006\u0010 \u001a\u00020!H\u0002J\u0018\u0010(\u001a\u00020&2\u0006\u0010)\u001a\u00020\u00022\u0006\u0010*\u001a\u00020+H\u0016J\u0010\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020#H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;",
        "view",
        "Landroid/view/View;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Landroid/view/View;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "accountHolderField",
        "Landroid/widget/EditText;",
        "accountNumberField",
        "accountType",
        "Lcom/squareup/marketfont/MarketTextView;",
        "accountTypeField",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "bankAccountType",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "continueButton",
        "Landroid/widget/Button;",
        "directDebitGuarantee",
        "Lcom/squareup/widgets/MessageView;",
        "directDebitLogo",
        "Landroid/widget/ImageView;",
        "finePrint",
        "primaryInstitutionNumber",
        "primaryInstitutionNumberField",
        "secondaryInstitutionNumber",
        "secondaryInstitutionNumberField",
        "Lcom/squareup/resources/TextModel;",
        "",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "isValidInput",
        "",
        "screen",
        "onPrimaryButtonClicked",
        "",
        "showAccountType",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "showSpinner",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountHolderField:Landroid/widget/EditText;

.field private final accountNumberField:Landroid/widget/EditText;

.field private final accountType:Lcom/squareup/marketfont/MarketTextView;

.field private final accountTypeField:Lcom/squareup/noho/NohoCheckableGroup;

.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field private final continueButton:Landroid/widget/Button;

.field private final directDebitGuarantee:Lcom/squareup/widgets/MessageView;

.field private final directDebitLogo:Landroid/widget/ImageView;

.field private final finePrint:Lcom/squareup/widgets/MessageView;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final primaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

.field private final primaryInstitutionNumberField:Landroid/widget/EditText;

.field private final secondaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

.field private final secondaryInstitutionNumberField:Landroid/widget/EditText;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 68
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 69
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->account_holder_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountHolderField:Landroid/widget/EditText;

    .line 71
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->primary_institution_number:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

    .line 73
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->primary_institution_number_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    .line 75
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->secondary_institution_number:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

    .line 77
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->secondary_institution_number_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    .line 78
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->account_number_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountNumberField:Landroid/widget/EditText;

    .line 79
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->account_type:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountType:Lcom/squareup/marketfont/MarketTextView;

    .line 80
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->account_type_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountTypeField:Lcom/squareup/noho/NohoCheckableGroup;

    .line 81
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->fine_print:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->finePrint:Lcom/squareup/widgets/MessageView;

    .line 82
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->direct_debit_logo:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->directDebitLogo:Landroid/widget/ImageView;

    .line 83
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->direct_debit_guarantee:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->directDebitGuarantee:Lcom/squareup/widgets/MessageView;

    .line 84
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->continue_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->continueButton:Landroid/widget/Button;

    .line 88
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    new-instance p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$1;

    invoke-direct {p2, p0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$1;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountTypeField:Lcom/squareup/noho/NohoCheckableGroup;

    new-instance p2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$2;

    invoke-direct {p2, p0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$2;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)V

    check-cast p2, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public static final synthetic access$getBankAccountType$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0
.end method

.method public static final synthetic access$getGlassSpinner$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)Lcom/squareup/register/widgets/GlassSpinner;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-object p0
.end method

.method public static final synthetic access$getPrimaryInstitutionNumberField$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)Landroid/widget/EditText;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    return-object p0
.end method

.method public static final synthetic access$onPrimaryButtonClicked(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->onPrimaryButtonClicked(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)V

    return-void
.end method

.method public static final synthetic access$setBankAccountType$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/protos/client/bankaccount/BankAccountType;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-void
.end method

.method private final directDebitGuarantee()Lcom/squareup/resources/TextModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 294
    new-instance v0, Lcom/squareup/ui/LinkSpanModel;

    .line 295
    sget v1, Lcom/squareup/banklinking/impl/R$string;->read_the_direct_debit_guarantee:I

    .line 296
    sget v2, Lcom/squareup/banklinking/impl/R$string;->direct_debit_guarantee_url:I

    .line 297
    sget v3, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 294
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method private final finePrint(Lcom/squareup/CountryCode;)Lcom/squareup/resources/TextModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/CountryCode;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 263
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 290
    sget-object p1, Lcom/squareup/resources/TextModel;->Companion:Lcom/squareup/resources/TextModel$Companion;

    invoke-virtual {p1}, Lcom/squareup/resources/TextModel$Companion;->getEmpty()Lcom/squareup/resources/FixedText;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 289
    :cond_0
    new-instance p1, Lcom/squareup/resources/ResourceCharSequence;

    sget v0, Lcom/squareup/banklinking/impl/R$string;->bank_account_fine_print:I

    invoke-direct {p1, v0}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 281
    :cond_1
    new-instance p1, Lcom/squareup/ui/LinkSpanModel;

    .line 282
    sget v0, Lcom/squareup/banklinking/impl/R$string;->learn_more_with_arrow:I

    .line 283
    sget v1, Lcom/squareup/banklinking/impl/R$string;->learn_more_url_gb:I

    .line 284
    sget v2, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 281
    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    .line 286
    new-instance v0, Lcom/squareup/resources/PhraseModel;

    sget v1, Lcom/squareup/banklinking/impl/R$string;->bank_account_fine_print_gb:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 287
    check-cast p1, Lcom/squareup/resources/TextModel;

    const-string v1, "learn_more"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Lcom/squareup/resources/TextModel;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 265
    :cond_2
    new-instance p1, Lcom/squareup/ui/LinkSpanModel;

    .line 266
    sget v0, Lcom/squareup/banklinking/impl/R$string;->square_seller_agreement:I

    .line 267
    sget v1, Lcom/squareup/banklinking/impl/R$string;->square_seller_agreement_url_au:I

    .line 268
    sget v2, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 265
    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    .line 270
    new-instance v0, Lcom/squareup/ui/LinkSpanModel;

    .line 271
    sget v1, Lcom/squareup/banklinking/impl/R$string;->square_seller_agreement:I

    .line 272
    sget v2, Lcom/squareup/banklinking/impl/R$string;->square_seller_agreement_url_au:I

    .line 273
    sget v3, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 270
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    .line 276
    new-instance v1, Lcom/squareup/resources/PhraseModel;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->bank_account_fine_print_au:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 277
    check-cast p1, Lcom/squareup/resources/TextModel;

    const-string v2, "square_seller_agreement_one"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Lcom/squareup/resources/TextModel;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    .line 278
    check-cast v0, Lcom/squareup/resources/TextModel;

    const-string v1, "square_seller_agreement_two"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Lcom/squareup/resources/TextModel;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    :goto_0
    return-object p1
.end method

.method private final isValidInput(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)Z
    .locals 4

    .line 189
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountHolderField:Landroid/widget/EditText;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnInvalidBankInfo()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 191
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 192
    sget v2, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 193
    sget v3, Lcom/squareup/banklinking/R$string;->missing_account_holder:I

    .line 191
    invoke-direct {v0, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountHolderField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v1

    .line 200
    :cond_0
    sget-object v0, Lcom/squareup/banklinking/InstitutionNumberUtil;->INSTANCE:Lcom/squareup/banklinking/InstitutionNumberUtil;

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/banklinking/InstitutionNumberUtil;->isPrimaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnInvalidBankInfo()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 202
    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    .line 203
    sget v3, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 204
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->invalidPrimaryInstitutionNumberMessage(Lcom/squareup/CountryCode;)I

    move-result p1

    .line 202
    invoke-direct {v2, v3, p1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-interface {v0, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v1

    .line 211
    :cond_1
    sget-object v0, Lcom/squareup/banklinking/InstitutionNumberUtil;->INSTANCE:Lcom/squareup/banklinking/InstitutionNumberUtil;

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/banklinking/InstitutionNumberUtil;->isSecondaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 212
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnInvalidBankInfo()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 213
    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    .line 214
    sget v3, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 215
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->invalidSecondaryInstitutionNumberMessage(Lcom/squareup/CountryCode;)I

    move-result p1

    .line 213
    invoke-direct {v2, v3, p1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-interface {v0, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v1

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountNumberField:Landroid/widget/EditText;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 223
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnInvalidBankInfo()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 224
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 225
    sget v2, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 226
    sget v3, Lcom/squareup/banklinking/R$string;->missing_account_number:I

    .line 224
    invoke-direct {v0, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountNumberField:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v1

    .line 233
    :cond_3
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountNumberField:Landroid/widget/EditText;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 234
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnInvalidBankInfo()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 235
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 236
    sget v2, Lcom/squareup/banklinking/R$string;->routing_account_number_match_headline:I

    .line 237
    sget v3, Lcom/squareup/banklinking/R$string;->routing_account_number_match_prompt:I

    .line 235
    invoke-direct {v0, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    .line 243
    :cond_4
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    if-nez v0, :cond_5

    .line 244
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnInvalidBankInfo()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 245
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 246
    sget v2, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 247
    sget v3, Lcom/squareup/banklinking/R$string;->missing_account_type:I

    .line 245
    invoke-direct {v0, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    :cond_5
    const/4 p1, 0x1

    return p1
.end method

.method private final onPrimaryButtonClicked(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)V
    .locals 4

    .line 166
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->isValidInput(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)Z

    move-result v0

    .line 167
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnLinkBankAttempt()Lkotlin/jvm/functions/Function2;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 169
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;-><init>()V

    .line 170
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountHolderField:Landroid/widget/EditText;

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v0

    .line 171
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->primary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->secondary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountNumberField:Landroid/widget/EditText;

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_type(Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v0

    .line 180
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getOnLinkBank()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    const-string v1, "bankAccountDetails"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private final showAccountType(Lcom/squareup/CountryCode;)Z
    .locals 2

    .line 256
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private final spinnerData(Z)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 185
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 6

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getShowSpinner()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->spinnerData(Z)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->setSpinnerState(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V

    .line 106
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$1;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 110
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 107
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 108
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 109
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$2;

    invoke-direct {v2, p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$2;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 112
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 113
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 114
    invoke-static {v0}, Lcom/squareup/address/CountryResources;->primaryInstitutionNumber(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 115
    iget-object v2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    new-instance v2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;

    invoke-direct {v2, p0, v0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/CountryCode;)V

    check-cast v2, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->primaryInstitutionNumberField:Landroid/widget/EditText;

    new-instance v2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$5;

    invoke-direct {v2, p0, v0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$5;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/CountryCode;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 131
    invoke-static {v0}, Lcom/squareup/address/CountryResources;->secondaryInstitutionNumber(Lcom/squareup/CountryCode;)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 132
    invoke-static {v0}, Lcom/squareup/address/CountryResources;->secondaryInstitutionNumber(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 133
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    invoke-virtual {v1, p2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 136
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 137
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    invoke-virtual {p2, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0

    .line 139
    :cond_0
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumber:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 140
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->secondaryInstitutionNumberField:Landroid/widget/EditText;

    invoke-virtual {p2, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 143
    :goto_0
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->showAccountType(Lcom/squareup/CountryCode;)Z

    move-result p2

    .line 144
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountType:Lcom/squareup/marketfont/MarketTextView;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 145
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->accountTypeField:Lcom/squareup/noho/NohoCheckableGroup;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-nez p2, :cond_1

    .line 147
    sget-object p2, Lcom/squareup/protos/client/bankaccount/BankAccountType;->CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 150
    :cond_1
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->finePrint(Lcom/squareup/CountryCode;)Lcom/squareup/resources/TextModel;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v3, "view.context"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 151
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->finePrint:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->finePrint:Lcom/squareup/widgets/MessageView;

    check-cast v1, Landroid/view/View;

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 v4, 0x1

    xor-int/2addr p2, v4

    invoke-static {v1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 155
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->directDebitLogo:Landroid/widget/ImageView;

    check-cast p2, Landroid/view/View;

    sget-object v1, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    invoke-static {p2, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 157
    sget-object p2, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne v0, p2, :cond_3

    .line 158
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->directDebitGuarantee:Lcom/squareup/widgets/MessageView;

    invoke-direct {p0}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->directDebitGuarantee()Lcom/squareup/resources/TextModel;

    move-result-object v1

    iget-object v5, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :cond_3
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->directDebitGuarantee:Lcom/squareup/widgets/MessageView;

    check-cast p2, Landroid/view/View;

    sget-object v1, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_4

    const/4 v2, 0x1

    :cond_4
    invoke-static {p2, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 162
    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->continueButton:Landroid/widget/Button;

    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$8;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->showRendering(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
