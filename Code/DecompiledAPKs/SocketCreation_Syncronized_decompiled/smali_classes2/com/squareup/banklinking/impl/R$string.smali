.class public final Lcom/squareup/banklinking/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final account_password:I = 0x7f120039

.field public static final address:I = 0x7f1200a7

.field public static final bank_account_fine_print:I = 0x7f120142

.field public static final bank_account_fine_print_au:I = 0x7f120143

.field public static final bank_account_fine_print_continued:I = 0x7f120144

.field public static final bank_account_fine_print_gb:I = 0x7f120145

.field public static final bank_account_linking_failed:I = 0x7f120149

.field public static final bank_account_linking_failed_message:I = 0x7f12014a

.field public static final bank_account_verification_in_progress:I = 0x7f120156

.field public static final bank_account_verification_in_progress_message:I = 0x7f120157

.field public static final bank_account_verified:I = 0x7f120158

.field public static final bank_account_verified_message:I = 0x7f120159

.field public static final bank_or_building_society_information:I = 0x7f120162

.field public static final cancel_verification_failed:I = 0x7f12029e

.field public static final check_your_inbox:I = 0x7f120400

.field public static final check_your_inbox_message:I = 0x7f120401

.field public static final confirm_bank_account_failed:I = 0x7f12047d

.field public static final direct_debit_guarantee_url:I = 0x7f120875

.field public static final direct_debit_logo:I = 0x7f120876

.field public static final email:I = 0x7f120990

.field public static final get_direct_debit_info_failure_message:I = 0x7f120aee

.field public static final get_direct_debit_info_failure_title:I = 0x7f120aef

.field public static final holder_name:I = 0x7f120b53

.field public static final instruction_to_your_bank_or_building_society_body:I = 0x7f120c41

.field public static final instruction_to_your_bank_or_building_society_title:I = 0x7f120c42

.field public static final invalid_password:I = 0x7f120c5e

.field public static final invalid_password_subtext:I = 0x7f120c5f

.field public static final learn_more:I = 0x7f120eb5

.field public static final learn_more_url_gb:I = 0x7f120eb8

.field public static final learn_more_with_arrow:I = 0x7f120eb9

.field public static final link_bank_account_continue:I = 0x7f120ece

.field public static final load_bank_account_error_title:I = 0x7f120ee3

.field public static final managing_bank_building_society_body:I = 0x7f120f8b

.field public static final managing_bank_building_society_title:I = 0x7f120f8c

.field public static final name_on_bank_account:I = 0x7f12104e

.field public static final organization:I = 0x7f1212f4

.field public static final password_helper_hint:I = 0x7f12138e

.field public static final password_helper_text:I = 0x7f12138f

.field public static final read_the_direct_debit_guarantee:I = 0x7f121567

.field public static final reference:I = 0x7f121616

.field public static final service_user_number_body:I = 0x7f1217cb

.field public static final service_user_number_title:I = 0x7f1217cc

.field public static final square_seller_agreement:I = 0x7f12189f

.field public static final square_seller_agreement_url_au:I = 0x7f1218a0

.field public static final squareup_europe_limited_body:I = 0x7f1218a8

.field public static final squareup_europe_limited_title:I = 0x7f1218a9

.field public static final submit:I = 0x7f1218ce

.field public static final submit_your_information_body:I = 0x7f1218d2

.field public static final submit_your_information_title:I = 0x7f1218d3

.field public static final uppercase_header_account_type:I = 0x7f121b30

.field public static final your_information:I = 0x7f121bf4


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
