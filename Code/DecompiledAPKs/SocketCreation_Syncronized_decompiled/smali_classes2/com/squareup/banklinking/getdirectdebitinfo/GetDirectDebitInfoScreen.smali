.class public final Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;
.super Ljava/lang/Object;
.source "GetDirectDebitInfoScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001\u0007B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "content",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;",
        "(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;)V",
        "getContent",
        "()Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;",
        "Content",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final content:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;


# direct methods
.method public constructor <init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;)V
    .locals 1

    const-string v0, "content"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;->content:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    return-void
.end method


# virtual methods
.method public final getContent()Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;->content:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    return-object v0
.end method
