.class public final Lcom/squareup/banklinking/BankAccountSettingsAnalyticsKt;
.super Ljava/lang/Object;
.source "BankAccountSettingsAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "ADD_BANK_ACCOUNT_RESULT",
        "",
        "EDIT_BANK_ACCOUNT_RESULT",
        "ONBOARD_ADD_BANK_ACCOUNT_RESULT",
        "PASSWORD_ERROR",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ADD_BANK_ACCOUNT_RESULT:Ljava/lang/String; = "Settings Bank Accounts: Add Bank Account Result"

.field private static final EDIT_BANK_ACCOUNT_RESULT:Ljava/lang/String; = "Settings Bank Accounts: Edit Bank Account Result"

.field private static final ONBOARD_ADD_BANK_ACCOUNT_RESULT:Ljava/lang/String; = "Onboard: Add Bank Account Result"

.field private static final PASSWORD_ERROR:Ljava/lang/String; = "Settings Bank Account: Edit Bank Account Password Error"
