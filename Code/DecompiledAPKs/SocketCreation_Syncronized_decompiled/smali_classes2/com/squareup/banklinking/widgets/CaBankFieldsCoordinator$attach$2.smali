.class final Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;
.super Ljava/lang/Object;
.source "CaBankFieldsCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        "institutionNumber",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;

    invoke-direct {v0}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;->INSTANCE:Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/String;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
    .locals 2

    const-string v0, "institutionNumber"

    .line 48
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$NotApplicable;->INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$NotApplicable;

    check-cast p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;

    goto :goto_0

    .line 49
    :cond_0
    sget-object v0, Lcom/squareup/banklinking/InstitutionNumberUtil;->INSTANCE:Lcom/squareup/banklinking/InstitutionNumberUtil;

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/banklinking/InstitutionNumberUtil;->isPrimaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;->INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;

    check-cast p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;

    goto :goto_0

    .line 50
    :cond_1
    sget-object p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;->INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;

    check-cast p1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;->call(Ljava/lang/String;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;

    move-result-object p1

    return-object p1
.end method
