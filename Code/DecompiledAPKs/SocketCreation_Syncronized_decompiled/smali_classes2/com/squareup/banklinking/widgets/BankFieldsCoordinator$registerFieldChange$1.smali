.class final Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;
.super Ljava/lang/Object;
.source "BankFieldsCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $field:Landroid/widget/TextView;

.field final synthetic $fieldType:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

.field final synthetic this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    iput-object p2, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->$fieldType:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    iput-object p3, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->$field:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 3

    .line 73
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->this$0:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    invoke-static {p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->access$getFieldChanged$p(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    new-instance v0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;

    iget-object v1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->$fieldType:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    iget-object v2, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;->$field:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;-><init>(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
