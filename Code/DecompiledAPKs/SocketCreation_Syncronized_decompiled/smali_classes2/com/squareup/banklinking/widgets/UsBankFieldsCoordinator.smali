.class public final Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;
.super Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
.source "UsBankFieldsCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0002J\n\u0010\u000b\u001a\u0004\u0018\u00010\tH\u0016J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\rH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;",
        "()V",
        "accountField",
        "Landroid/widget/EditText;",
        "routingField",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "emptyField",
        "getField",
        "",
        "fieldType",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
        "setField",
        "fieldText",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accountField:Landroid/widget/EditText;

.field private routingField:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    sget v0, Lcom/squareup/banklinking/R$layout;->bank_account_fields_us:I

    invoke-direct {p0, v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;-><init>(I)V

    return-void
.end method

.method public static final synthetic access$getRoutingField$p(Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 14
    iget-object p0, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "routingField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setRoutingField$p(Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 48
    sget v0, Lcom/squareup/banklinking/R$id;->routing_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    .line 49
    sget v0, Lcom/squareup/banklinking/R$id;->account_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-super {p0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->attach(Landroid/view/View;)V

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->bindViews(Landroid/view/View;)V

    .line 22
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    if-nez p1, :cond_0

    const-string v0, "routingField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/widget/TextView;

    sget-object v0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ROUTING:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V

    .line 23
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez p1, :cond_1

    const-string v0, "accountField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/widget/TextView;

    sget-object v0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ACCOUNT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V

    .line 25
    sget-object p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ROUTING:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    new-instance v0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator$attach$1;-><init>(Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public emptyField()Landroid/view/View;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/TextView;

    .line 30
    iget-object v1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    if-nez v1, :cond_0

    const-string v2, "routingField"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez v2, :cond_1

    const-string v3, "accountField"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/squareup/util/Views;->getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;
    .locals 1

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const-string p1, ""

    goto :goto_0

    .line 34
    :cond_0
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez p1, :cond_1

    const-string v0, "accountField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 33
    :cond_2
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    if-nez p1, :cond_3

    const-string v0, "routingField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public setField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V
    .locals 1

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 43
    :cond_0
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez p1, :cond_1

    const-string v0, "accountField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 42
    :cond_2
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/UsBankFieldsCoordinator;->routingField:Landroid/widget/EditText;

    if-nez p1, :cond_3

    const-string v0, "routingField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
