.class public abstract Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "BankFieldsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankFieldsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankFieldsCoordinator.kt\ncom/squareup/banklinking/widgets/BankFieldsCoordinator\n*L\n1#1,142:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008 \u0018\u00002\u00020\u0001:\u0001)B\u000f\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\n\u0010\u0013\u001a\u0004\u0018\u00010\u0011H&J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0014J\u0016\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u0017H\u0004J$\u0010\u0007\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u000f0\u0019H\u0004J\u0010\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H&J/\u0010\u001b\u001a\u00020\u000f2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u00142\u0012\u0010\u001e\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020 0\u001f\"\u00020 H\u0004\u00a2\u0006\u0002\u0010!J\u0018\u0010\"\u001a\u00020\u000f2\u0006\u0010#\u001a\u00020 2\u0006\u0010\u0016\u001a\u00020\u0017H\u0004J\u0018\u0010$\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010%\u001a\u00020\u0015H&J\u0016\u0010&\u001a\u0004\u0018\u00010\'*\u00020\u00062\u0006\u0010(\u001a\u00020\u001dH\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "layoutId",
        "",
        "(I)V",
        "context",
        "Landroid/content/Context;",
        "fieldChanged",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
        "getLayoutId",
        "()I",
        "subscriptions",
        "Lrx/subscriptions/CompositeSubscription;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "detach",
        "emptyField",
        "Lrx/Observable;",
        "",
        "fieldType",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
        "listener",
        "Lkotlin/Function1;",
        "getField",
        "observeValidation",
        "validationResult",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        "destinationViews",
        "",
        "Landroid/widget/TextView;",
        "(Lrx/Observable;[Landroid/widget/TextView;)V",
        "registerFieldChange",
        "field",
        "setField",
        "fieldText",
        "getIcon",
        "Landroid/graphics/drawable/Drawable;",
        "result",
        "ValidationResult",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private final fieldChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
            ">;"
        }
    .end annotation
.end field

.field private final layoutId:I

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->layoutId:I

    .line 51
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 52
    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method public static final synthetic access$getContext$p(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;)Landroid/content/Context;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->context:Landroid/content/Context;

    return-object p0
.end method

.method public static final synthetic access$getFieldChanged$p(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getIcon(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;Landroid/content/Context;Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->getIcon(Landroid/content/Context;Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setContext$p(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;Landroid/content/Context;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->context:Landroid/content/Context;

    return-void
.end method

.method private final getIcon(Landroid/content/Context;Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 117
    invoke-virtual {p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->getIconRes()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 119
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->getIconRes()I

    move-result v0

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 120
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->getColorRes()I

    move-result p2

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p1, p2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    check-cast v1, Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    return-object v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 58
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->context:Landroid/content/Context;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1}, Lrx/subscriptions/CompositeSubscription;->clear()V

    const/4 p1, 0x0

    .line 64
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->context:Landroid/content/Context;

    return-void
.end method

.method public abstract emptyField()Landroid/view/View;
.end method

.method public final fieldChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    const-string v1, "fieldChanged.asObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 80
    new-instance v1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$1;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$1;-><init>(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 81
    sget-object v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$2;->INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$2;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v0, "fieldChanged\n        .fi\u2026\n        .map { it.text }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected final fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;

    move-result-object p1

    .line 90
    new-instance v1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$sam$rx_functions_Action1$0;

    invoke-direct {v1, p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$sam$rx_functions_Action1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const-string p2, "fieldChanged(fieldType)\n\u2026     .subscribe(listener)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {v0, p1}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    return-void
.end method

.method public abstract getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;
.end method

.method public final getLayoutId()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->layoutId:I

    return v0
.end method

.method protected final varargs observeValidation(Lrx/Observable;[Landroid/widget/TextView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
            ">;[",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "validationResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationViews"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 103
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    .line 104
    new-instance v1, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$observeValidation$1;-><init>(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;[Landroid/widget/TextView;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const-string/jumbo p2, "validationResult\n       \u2026          }\n            }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {v0, p1}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    return-void
.end method

.method protected final registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V
    .locals 3

    const-string v0, "field"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$registerFieldChange$1;-><init>(Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Landroid/widget/TextView;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const-string p2, "field.debouncedOnChanged\u2026Type, field.value))\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    return-void
.end method

.method public abstract setField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V
.end method
