.class public final Lcom/squareup/banklinking/widgets/BankAccountView;
.super Landroid/widget/LinearLayout;
.source "BankAccountView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010.\u001a\u00020,2\u0006\u0010-\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nR$\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0012\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\r\"\u0004\u0008\u0014\u0010\u000fR\u000e\u0010\u0015\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0016\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0017\u0010\r\"\u0004\u0008\u0018\u0010\u000fR\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u001b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001c\u0010\r\"\u0004\u0008\u001d\u0010\u000fR\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\"\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008#\u0010\r\"\u0004\u0008$\u0010\u000fR\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\'\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008(\u0010\r\"\u0004\u0008)\u0010\u000fR\u000e\u0010*\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankAccountView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "value",
        "",
        "accountHolderName",
        "getAccountHolderName",
        "()Ljava/lang/CharSequence;",
        "setAccountHolderName",
        "(Ljava/lang/CharSequence;)V",
        "accountHolderNameView",
        "Lcom/squareup/widgets/list/NameValueRow;",
        "accountNumber",
        "getAccountNumber",
        "setAccountNumber",
        "accountNumberView",
        "accountType",
        "getAccountType",
        "setAccountType",
        "accountTypeView",
        "Lcom/squareup/widgets/ShorteningTextView;",
        "estimatedCompletionDate",
        "getEstimatedCompletionDate",
        "setEstimatedCompletionDate",
        "estimatedCompletionDateView",
        "Lcom/squareup/widgets/MessageView;",
        "primaryInstitutionNumberView",
        "secondaryInstitutionNumberView",
        "title",
        "getTitle",
        "setTitle",
        "titleView",
        "Landroid/widget/TextView;",
        "verificationStatus",
        "getVerificationStatus",
        "setVerificationStatus",
        "verificationStatusView",
        "setPrimaryInstitutionalNumber",
        "",
        "name",
        "setSecondaryInstitutionalNumber",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountHolderNameView:Lcom/squareup/widgets/list/NameValueRow;

.field private final accountNumberView:Lcom/squareup/widgets/list/NameValueRow;

.field private final accountTypeView:Lcom/squareup/widgets/ShorteningTextView;

.field private final estimatedCompletionDateView:Lcom/squareup/widgets/MessageView;

.field private final primaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

.field private final secondaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

.field private final titleView:Landroid/widget/TextView;

.field private final verificationStatusView:Lcom/squareup/widgets/list/NameValueRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/widgets/BankAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/widgets/BankAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    sget p2, Lcom/squareup/banklinking/R$layout;->bank_account_view_contents:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p2, 0x1

    .line 24
    invoke-virtual {p0, p2}, Lcom/squareup/banklinking/widgets/BankAccountView;->setOrientation(I)V

    .line 26
    sget p2, Lcom/squareup/marin/R$drawable;->marin_divider_horizontal_light_gray:I

    .line 25
    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x2

    .line 28
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->setShowDividers(I)V

    .line 31
    sget p1, Lcom/squareup/banklinking/R$id;->title:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.title)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->titleView:Landroid/widget/TextView;

    .line 32
    sget p1, Lcom/squareup/banklinking/R$id;->account_type:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.account_type)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/ShorteningTextView;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountTypeView:Lcom/squareup/widgets/ShorteningTextView;

    .line 33
    sget p1, Lcom/squareup/banklinking/R$id;->account_holder_name:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.account_holder_name)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/list/NameValueRow;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountHolderNameView:Lcom/squareup/widgets/list/NameValueRow;

    .line 35
    sget p1, Lcom/squareup/banklinking/R$id;->primary_institution_number:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.primary_institution_number)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/list/NameValueRow;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->primaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    .line 37
    sget p1, Lcom/squareup/banklinking/R$id;->secondary_institution_number:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.secondary_institution_number)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/list/NameValueRow;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->secondaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    .line 38
    sget p1, Lcom/squareup/banklinking/R$id;->account_number:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.account_number)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/list/NameValueRow;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountNumberView:Lcom/squareup/widgets/list/NameValueRow;

    .line 39
    sget p1, Lcom/squareup/banklinking/R$id;->status:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.status)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/list/NameValueRow;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->verificationStatusView:Lcom/squareup/widgets/list/NameValueRow;

    .line 41
    sget p1, Lcom/squareup/banklinking/R$id;->estimated_completion_date:I

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.estimated_completion_date)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->estimatedCompletionDateView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 19
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 20
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/banklinking/widgets/BankAccountView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final getAccountHolderName()Ljava/lang/CharSequence;
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountHolderNameView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/NameValueRow;->getValue()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "accountHolderNameView.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getAccountNumber()Ljava/lang/CharSequence;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountNumberView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/NameValueRow;->getValue()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "accountNumberView.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getAccountType()Ljava/lang/CharSequence;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountTypeView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/ShorteningTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "accountTypeView.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getEstimatedCompletionDate()Ljava/lang/CharSequence;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->estimatedCompletionDateView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "estimatedCompletionDateView.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->titleView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getVerificationStatus()Ljava/lang/CharSequence;
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->verificationStatusView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/NameValueRow;->getValue()Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, "verificationStatusView.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setAccountHolderName(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountHolderNameView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setAccountNumber(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountNumberView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setAccountType(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->accountTypeView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setEstimatedCompletionDate(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->estimatedCompletionDateView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x6

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountView;->setShowDividers(I)V

    return-void
.end method

.method public final setPrimaryInstitutionalNumber(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->primaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->primaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setSecondaryInstitutionalNumber(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->secondaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->secondaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 98
    iget-object p2, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->secondaryInstitutionNumberView:Lcom/squareup/widgets/list/NameValueRow;

    check-cast p2, Landroid/view/View;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setVerificationStatus(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountView;->verificationStatusView:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method
