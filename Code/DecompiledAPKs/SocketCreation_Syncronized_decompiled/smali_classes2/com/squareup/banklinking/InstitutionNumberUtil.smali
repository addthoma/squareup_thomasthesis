.class public final Lcom/squareup/banklinking/InstitutionNumberUtil;
.super Ljava/lang/Object;
.source "InstitutionNumberUtil.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstitutionNumberUtil.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstitutionNumberUtil.kt\ncom/squareup/banklinking/InstitutionNumberUtil\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u000cJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u000f\u001a\u00020\u000cJ\u0016\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000cJ\u0016\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u000cR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/banklinking/InstitutionNumberUtil;",
        "",
        "()V",
        "BANK_CODE_LENGTH",
        "",
        "BRANCH_CODE_LENGTH",
        "BSB_NUMBER_LENGTH",
        "INSTITUTION_NUMBER_LENGTH",
        "SORT_CODE_LENGTH",
        "TRANSIT_NUMBER_LENGTH",
        "accountNumberMaxLength",
        "institutionNumber",
        "",
        "isAccountNumber",
        "",
        "accountNumber",
        "isPrimaryInstitutionNumber",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "primaryInstitutionNumber",
        "isSecondaryInstitutionNumber",
        "secondaryInstitutionNumber",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BANK_CODE_LENGTH:I = 0x4

.field private static final BRANCH_CODE_LENGTH:I = 0x3

.field private static final BSB_NUMBER_LENGTH:I = 0x6

.field public static final INSTANCE:Lcom/squareup/banklinking/InstitutionNumberUtil;

.field private static final INSTITUTION_NUMBER_LENGTH:I = 0x3

.field private static final SORT_CODE_LENGTH:I = 0x6

.field private static final TRANSIT_NUMBER_LENGTH:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/banklinking/InstitutionNumberUtil;

    invoke-direct {v0}, Lcom/squareup/banklinking/InstitutionNumberUtil;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/InstitutionNumberUtil;->INSTANCE:Lcom/squareup/banklinking/InstitutionNumberUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accountNumberMaxLength(Ljava/lang/String;)I
    .locals 1

    const-string v0, "institutionNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v0, "815"

    .line 73
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v0, "614"

    .line 80
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0xa

    goto :goto_2

    :sswitch_2
    const-string v0, "016"

    .line 79
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x9

    goto :goto_2

    :sswitch_3
    const-string v0, "010"

    .line 73
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_4
    const-string v0, "006"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_5
    const-string v0, "004"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_6
    const-string v0, "003"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_7
    const-string v0, "002"

    .line 81
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0xc

    goto :goto_2

    :sswitch_8
    const-string v0, "001"

    .line 73
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    :goto_0
    const/4 p1, 0x7

    goto :goto_2

    :cond_0
    :goto_1
    const p1, 0x7fffffff

    :goto_2
    return p1

    nop

    :sswitch_data_0
    .sparse-switch
        0xba31 -> :sswitch_8
        0xba32 -> :sswitch_7
        0xba33 -> :sswitch_6
        0xba34 -> :sswitch_5
        0xba36 -> :sswitch_4
        0xba4f -> :sswitch_3
        0xba55 -> :sswitch_2
        0xd0d9 -> :sswitch_1
        0xd85c -> :sswitch_0
    .end sparse-switch
.end method

.method public final isAccountNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const-string v0, "institutionNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/banklinking/InstitutionNumberUtil;->isPrimaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 96
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    .line 97
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v2, 0x7

    const/4 v3, 0x1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string v0, "815"

    .line 98
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :sswitch_1
    const-string v0, "614"

    .line 105
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/16 p1, 0xa

    if-ne p2, p1, :cond_1

    goto :goto_1

    :sswitch_2
    const-string v0, "016"

    .line 104
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/16 p1, 0x9

    if-ne p2, p1, :cond_1

    goto :goto_1

    :sswitch_3
    const-string v0, "010"

    .line 98
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :sswitch_4
    const-string v0, "006"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :sswitch_5
    const-string v0, "004"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :sswitch_6
    const-string v0, "003"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :sswitch_7
    const-string v0, "002"

    .line 106
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    if-eq p2, v2, :cond_2

    const/16 p1, 0xc

    if-ne p2, p1, :cond_1

    goto :goto_1

    :sswitch_8
    const-string v0, "001"

    .line 98
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    :goto_0
    if-ne p2, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :cond_2
    :goto_1
    return v3

    :sswitch_data_0
    .sparse-switch
        0xba31 -> :sswitch_8
        0xba32 -> :sswitch_7
        0xba33 -> :sswitch_6
        0xba34 -> :sswitch_5
        0xba36 -> :sswitch_4
        0xba4f -> :sswitch_3
        0xba55 -> :sswitch_2
        0xd0d9 -> :sswitch_1
        0xd85c -> :sswitch_0
    .end sparse-switch
.end method

.method public final isPrimaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z
    .locals 7

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "primaryInstitutionNumber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    move-object v0, p2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isNumeric(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_0

    goto :goto_1

    .line 37
    :cond_0
    sget-object v1, Lcom/squareup/banklinking/InstitutionNumberUtil$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v4

    aget v1, v1, v4

    const/4 v4, 0x6

    if-eq v1, v3, :cond_5

    const/4 v5, 0x2

    const/4 v6, 0x3

    if-eq v1, v5, :cond_4

    if-eq v1, v6, :cond_3

    const/4 v4, 0x4

    if-eq v1, v4, :cond_2

    const/4 p2, 0x5

    if-ne v1, p2, :cond_1

    .line 42
    invoke-static {v0}, Lcom/squareup/banklinking/RoutingNumberUtil;->isRoutingTransitNumber(Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_1

    .line 43
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unsupported country: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 41
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-ne p1, v4, :cond_6

    goto :goto_0

    .line 40
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-ne p1, v4, :cond_6

    goto :goto_0

    .line 39
    :cond_4
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-ne p1, v6, :cond_6

    goto :goto_0

    .line 38
    :cond_5
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-ne p1, v4, :cond_6

    :goto_0
    const/4 v2, 0x1

    :cond_6
    :goto_1
    return v2
.end method

.method public final isSecondaryInstitutionNumber(Lcom/squareup/CountryCode;Ljava/lang/String;)Z
    .locals 3

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secondaryInstitutionNumber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    move-object v0, p2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isNumeric(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    goto :goto_1

    .line 58
    :cond_0
    sget-object v0, Lcom/squareup/banklinking/InstitutionNumberUtil$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    if-eq p1, v2, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    .line 60
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    const/4 p2, 0x3

    if-ne p1, p2, :cond_3

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    const/4 p2, 0x5

    if-ne p1, p2, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    return v1
.end method
