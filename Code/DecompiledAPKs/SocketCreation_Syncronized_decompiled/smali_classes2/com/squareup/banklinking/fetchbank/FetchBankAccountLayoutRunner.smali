.class public final Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;
.super Ljava/lang/Object;
.source "FetchBankAccountLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFetchBankAccountLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FetchBankAccountLayoutRunner.kt\ncom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner\n*L\n1#1,47:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000f2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "errorMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$Companion;


# instance fields
.field private final errorMessage:Lcom/squareup/noho/NohoMessageView;

.field private final spinner:Landroid/widget/ProgressBar;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->Companion:Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->view:Landroid/view/View;

    .line 18
    iget-object p1, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->fetch_bank_account_spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    .line 19
    iget-object p1, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->fetch_bank_account_error:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$showRendering$1;-><init>(Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 26
    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;->getTitle()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_1

    .line 27
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 28
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;->getMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 29
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    new-instance v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$showRendering$2;-><init>(Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    const-string v1, "debounce { rendering.onRetry() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 30
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    new-instance v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$showRendering$3;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$showRendering$3;-><init>(Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    const-string v1, "debounce { rendering.onCancel() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 31
    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;->getRetryable()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 32
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoMessageView;->showPrimaryButton()V

    goto :goto_0

    .line 34
    :cond_0
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoMessageView;->hidePrimaryButton()V

    .line 37
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 38
    iget-object p2, p0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;->getTitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->showRendering(Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
