.class public Lcom/squareup/camerahelper/RealCameraHelper;
.super Lmortar/Presenter;
.source "RealCameraHelper.java"

# interfaces
.implements Lcom/squareup/camerahelper/CameraHelper;


# annotations
.annotation runtime Lcom/squareup/camerahelper/CameraHelperScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/camerahelper/CameraHelper$View;",
        ">;",
        "Lcom/squareup/camerahelper/CameraHelper;"
    }
.end annotation


# static fields
.field private static final NEXT_NAME_KEY:Ljava/lang/String; = "next-photo-file-name"

.field static final SQUARE_IMAGE_CAMERA_REQUEST:I = 0x1

.field static final SQUARE_IMAGE_GALLERY_REQUEST:I = 0x2


# instance fields
.field private final activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

.field private final app:Landroid/app/Application;

.field private listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

.field nextCameraFileName:Ljava/lang/String;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method constructor <init>(Lcom/squareup/log/OhSnapLogger;Landroid/app/Application;Lcom/squareup/ui/ActivityResultHandler;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 52
    iput-object p2, p0, Lcom/squareup/camerahelper/RealCameraHelper;->app:Landroid/app/Application;

    .line 53
    iput-object p3, p0, Lcom/squareup/camerahelper/RealCameraHelper;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    return-void
.end method

.method private getCameraPhotoFile()Ljava/io/File;
    .locals 4

    .line 199
    new-instance v0, Ljava/io/File;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    .line 200
    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "Register"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "failed to create %s"

    .line 203
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    .line 207
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private isCameraAvailable()Z
    .locals 2

    .line 164
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x80000

    .line 165
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 166
    iget-object v1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->app:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/pm/PackageManager;)Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$g73Nq_TQUep3dvX9Udbaj855dXw(Lcom/squareup/camerahelper/RealCameraHelper;Lcom/squareup/ui/ActivityResultHandler$IntentResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/camerahelper/RealCameraHelper;->onActivityResult(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getRequestCode()I

    move-result p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private static makeGalleryIntent()Landroid/content/Intent;
    .locals 2

    .line 170
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "image/*"

    .line 171
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    .line 174
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.category.OPENABLE"

    .line 175
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private makeNextCameraIntent()Landroid/content/Intent;
    .locals 3

    .line 181
    invoke-direct {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getCameraPhotoFile()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 185
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    .line 187
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x80000

    .line 188
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 190
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/camerahelper/CameraHelper$View;

    invoke-interface {v2}, Lcom/squareup/camerahelper/CameraHelper$View;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/util/Files;->getUriForFile(Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "output"

    .line 189
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v1
.end method

.method private onActivityResult(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)V
    .locals 5

    .line 119
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getRequestCode()I

    move-result v0

    .line 120
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getResultCode()I

    move-result v1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getData()Landroid/content/Intent;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    if-ne v0, v3, :cond_0

    .line 125
    iput-object v2, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    .line 127
    :cond_0
    iget-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v1, "photo canceled"

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper$Listener;->onImageCanceled()V

    .line 129
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/camerahelper/CameraHelper$View;

    sget v0, Lcom/squareup/camerahelper/impl/R$string;->photo_canceled:I

    invoke-interface {p1, v0}, Lcom/squareup/camerahelper/CameraHelper$View;->toast(I)V

    return-void

    :cond_1
    if-ne v0, v3, :cond_3

    .line 134
    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 135
    iput-object v2, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    .line 138
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image file picked: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/camerahelper/CameraHelper$View;

    invoke-interface {v1}, Lcom/squareup/camerahelper/CameraHelper$View;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/util/Files;->getUriForFile(Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/camerahelper/CameraHelper$Listener;->onImagePicked(Landroid/net/Uri;)V

    goto :goto_1

    .line 141
    :cond_2
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image file busted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 142
    iget-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper$Listener;->onImageCanceled()V

    .line 143
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/camerahelper/CameraHelper$View;

    sget v0, Lcom/squareup/camerahelper/impl/R$string;->photo_canceled:I

    invoke-interface {p1, v0}, Lcom/squareup/camerahelper/CameraHelper$View;->toast(I)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    if-eqz p1, :cond_5

    .line 146
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_4

    goto :goto_0

    .line 153
    :cond_4
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "image data picked"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/camerahelper/CameraHelper$Listener;->onImagePicked(Landroid/net/Uri;)V

    goto :goto_1

    .line 147
    :cond_5
    :goto_0
    iget-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v1, "image data busted"

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper$Listener;->onImageCanceled()V

    .line 149
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/camerahelper/CameraHelper$View;

    sget v0, Lcom/squareup/camerahelper/impl/R$string;->photo_canceled:I

    invoke-interface {p1, v0}, Lcom/squareup/camerahelper/CameraHelper$View;->toast(I)V

    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public dropListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    :cond_0
    return-void
.end method

.method public bridge synthetic dropView(Lcom/squareup/camerahelper/CameraHelper$View;)V
    .locals 0

    .line 26
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/camerahelper/CameraHelper$View;)Lmortar/bundler/BundleService;
    .locals 0

    .line 159
    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper$View;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/camerahelper/CameraHelper$View;

    invoke-virtual {p0, p1}, Lcom/squareup/camerahelper/RealCameraHelper;->extractBundleService(Lcom/squareup/camerahelper/CameraHelper$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public isCameraOrGalleryAvailable()Z
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->isCameraAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->isGalleryAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isGalleryAvailable()Z
    .locals 2

    .line 89
    invoke-static {}, Lcom/squareup/camerahelper/RealCameraHelper;->makeGalleryIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->app:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/pm/PackageManager;)Z

    move-result v0

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/ui/ActivityResultHandler;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/camerahelper/-$$Lambda$RealCameraHelper$2HmTx-VxX8K6qcuiFJoETbBHUMU;->INSTANCE:Lcom/squareup/camerahelper/-$$Lambda$RealCameraHelper$2HmTx-VxX8K6qcuiFJoETbBHUMU;

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/camerahelper/-$$Lambda$RealCameraHelper$g73Nq_TQUep3dvX9Udbaj855dXw;

    invoke-direct {v1, p0}, Lcom/squareup/camerahelper/-$$Lambda$RealCameraHelper$g73Nq_TQUep3dvX9Udbaj855dXw;-><init>(Lcom/squareup/camerahelper/RealCameraHelper;)V

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 57
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 69
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "next-photo-file-name"

    .line 71
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 76
    invoke-super {p0, p1}, Lmortar/Presenter;->onSave(Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    const-string v1, "next-photo-file-name"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper;->listener:Lcom/squareup/camerahelper/CameraHelper$Listener;

    return-void
.end method

.method public startCamera()V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "startCamera"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->nextCameraFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "camera was already started"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void

    .line 103
    :cond_0
    invoke-direct {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->makeNextCameraIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/camerahelper/CameraHelper$View;

    invoke-interface {v1}, Lcom/squareup/camerahelper/CameraHelper$View;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/camerahelper/CameraHelper$View;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/squareup/camerahelper/CameraHelper$View;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->startGallery()V

    :goto_0
    return-void
.end method

.method public startGallery()V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "startGallery"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/camerahelper/CameraHelper$View;

    invoke-static {}, Lcom/squareup/camerahelper/RealCameraHelper;->makeGalleryIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/squareup/camerahelper/CameraHelper$View;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public bridge synthetic takeView(Lcom/squareup/camerahelper/CameraHelper$View;)V
    .locals 0

    .line 26
    invoke-super {p0, p1}, Lmortar/Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
