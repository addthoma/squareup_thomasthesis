.class public abstract Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;
.super Ljava/lang/Object;
.source "SalesReportScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/SalesReportScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SalesReportEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CustomizeReport;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectComparisonRange;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleOverviewDetails;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExpandAllItems;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CollapseAllItems;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExpandAllCategories;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CollapseAllCategories;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$BackPress;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ReloadReport;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExportReport;,
        Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ViewFeesNote;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0013\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0013\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&\'(\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;",
        "",
        "()V",
        "BackPress",
        "ChangeCategoryViewCount",
        "ChangeItemViewCount",
        "CollapseAllCategories",
        "CollapseAllItems",
        "CustomizeReport",
        "ExpandAllCategories",
        "ExpandAllItems",
        "ExportReport",
        "ReloadReport",
        "SelectCategoryAmountCountTab",
        "SelectChartSalesType",
        "SelectComparisonRange",
        "SelectItemGrossCountTab",
        "SelectPredefinedRange",
        "ToggleOverviewDetails",
        "ToggleShowingItemsForCategory",
        "ToggleShowingVariationsForItem",
        "ViewFeesNote",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CustomizeReport;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectComparisonRange;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleOverviewDetails;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExpandAllItems;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CollapseAllItems;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExpandAllCategories;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CollapseAllCategories;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$BackPress;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ReloadReport;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExportReport;",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ViewFeesNote;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;-><init>()V

    return-void
.end method
