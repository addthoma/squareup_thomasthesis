.class public final Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideSerialFileThreadExecutorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/thread/executor/SerialExecutor;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;->fileThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;)",
            "Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSerialFileThreadExecutor(Landroid/os/HandlerThread;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/CommonAppModule;->provideSerialFileThreadExecutor(Landroid/os/HandlerThread;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/thread/executor/SerialExecutor;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/thread/executor/SerialExecutor;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;->fileThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/HandlerThread;

    invoke-static {v0}, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;->provideSerialFileThreadExecutor(Landroid/os/HandlerThread;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;->get()Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    return-object v0
.end method
