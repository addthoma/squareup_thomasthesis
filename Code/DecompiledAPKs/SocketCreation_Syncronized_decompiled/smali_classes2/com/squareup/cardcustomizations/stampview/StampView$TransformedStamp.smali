.class public final Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;
.super Ljava/lang/Object;
.source "StampView.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransformedStamp"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0017\u001a\u00020\u0018J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c2\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J1\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\t\u0010\u001e\u001a\u00020\tH\u00d6\u0001J\u0016\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u000eJ\u0013\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u00d6\u0003J\t\u0010(\u001a\u00020\tH\u00d6\u0001J\t\u0010)\u001a\u00020*H\u00d6\u0001J\u0019\u0010+\u001a\u00020 2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020\tH\u00d6\u0001R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "Landroid/os/Parcelable;",
        "renderedStamp",
        "Lcom/squareup/cardcustomizations/stampview/Stamp;",
        "transform",
        "Landroid/graphics/Matrix;",
        "minFraction",
        "",
        "minHeight",
        "",
        "(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)V",
        "getMinHeight",
        "()I",
        "paintOverride",
        "Landroid/graphics/Paint;",
        "getPaintOverride",
        "()Landroid/graphics/Paint;",
        "setPaintOverride",
        "(Landroid/graphics/Paint;)V",
        "getRenderedStamp",
        "()Lcom/squareup/cardcustomizations/stampview/Stamp;",
        "getTransform",
        "()Landroid/graphics/Matrix;",
        "bounds",
        "Landroid/graphics/RectF;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "draw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "stampPaint",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final minFraction:F

.field private final minHeight:I

.field private paintOverride:Landroid/graphics/Paint;

.field private final renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

.field private final transform:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp$Creator;

    invoke-direct {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp$Creator;-><init>()V

    sput-object v0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)V
    .locals 1

    const-string v0, "renderedStamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    iput-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    iput p3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    iput p4, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    .line 425
    invoke-virtual {p1, p2}, Lcom/squareup/cardcustomizations/stampview/Stamp;->bounds(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object p4

    invoke-virtual {p4}, Landroid/graphics/RectF;->height()F

    move-result p4

    mul-float p4, p4, p3

    float-to-int p4, p4

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;-><init>(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)V

    return-void
.end method

.method private final component3()F
    .locals 1

    iget v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FIILjava/lang/Object;)Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->copy(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final bounds()Landroid/graphics/RectF;
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->bounds(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public final component1()Lcom/squareup/cardcustomizations/stampview/Stamp;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    return-object v0
.end method

.method public final component2()Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    return v0
.end method

.method public final copy(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;
    .locals 1

    const-string v0, "renderedStamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;-><init>(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 2

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampPaint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 430
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->paintOverride:Landroid/graphics/Paint;

    if-eqz v1, :cond_0

    move-object p2, v1

    :cond_0
    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Matrix;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    iget-object v3, p1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    iget-object v3, p1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    iget v3, p1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    iget p1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    if-ne v1, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public final getMinHeight()I
    .locals 1

    .line 425
    iget v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    return v0
.end method

.method public final getPaintOverride()Landroid/graphics/Paint;
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->paintOverride:Landroid/graphics/Paint;

    return-object v0
.end method

.method public final getRenderedStamp()Lcom/squareup/cardcustomizations/stampview/Stamp;
    .locals 1

    .line 422
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    return-object v0
.end method

.method public final getTransform()Landroid/graphics/Matrix;
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final setPaintOverride(Landroid/graphics/Paint;)V
    .locals 0

    .line 427
    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->paintOverride:Landroid/graphics/Paint;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransformedStamp(renderedStamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transform="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", minFraction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", minHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->renderedStamp:Lcom/squareup/cardcustomizations/stampview/Stamp;

    sget-object v1, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;->INSTANCE:Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;

    invoke-interface {v1, v0, p1, p2}, Lkotlinx/android/parcel/Parceler;->write(Ljava/lang/Object;Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->transform:Landroid/graphics/Matrix;

    sget-object v1, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;->INSTANCE:Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;

    invoke-interface {v1, v0, p1, p2}, Lkotlinx/android/parcel/Parceler;->write(Ljava/lang/Object;Landroid/os/Parcel;I)V

    iget p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minFraction:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    iget p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->minHeight:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
