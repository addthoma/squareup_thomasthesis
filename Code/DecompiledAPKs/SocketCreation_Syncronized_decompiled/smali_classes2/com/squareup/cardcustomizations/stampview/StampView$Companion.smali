.class public final Lcom/squareup/cardcustomizations/stampview/StampView$Companion;
.super Ljava/lang/Object;
.source "StampView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001f\u0010\u0003\u001a\u0004\u0018\u0001H\u0004\"\u0004\u0008\u0000\u0010\u0004*\u0008\u0012\u0004\u0012\u0002H\u00040\u0005H\u0002\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView$Companion;",
        "",
        "()V",
        "peekOrNull",
        "T",
        "Ljava/util/ArrayDeque;",
        "(Ljava/util/ArrayDeque;)Ljava/lang/Object;",
        "pointForIndex",
        "Landroid/graphics/PointF;",
        "Landroid/view/MotionEvent;",
        "index",
        "",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 499
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$peekOrNull(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Ljava/util/ArrayDeque;)Ljava/lang/Object;
    .locals 0

    .line 499
    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->peekOrNull(Ljava/util/ArrayDeque;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$pointForIndex(Lcom/squareup/cardcustomizations/stampview/StampView$Companion;Landroid/view/MotionEvent;I)Landroid/graphics/PointF;
    .locals 0

    .line 499
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardcustomizations/stampview/StampView$Companion;->pointForIndex(Landroid/view/MotionEvent;I)Landroid/graphics/PointF;

    move-result-object p0

    return-object p0
.end method

.method private final peekOrNull(Ljava/util/ArrayDeque;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayDeque<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 502
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final pointForIndex(Landroid/view/MotionEvent;I)Landroid/graphics/PointF;
    .locals 2

    .line 500
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method
