.class public final enum Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;
.super Ljava/lang/Enum;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/signature/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignatureState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

.field public static final enum CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

.field public static final enum SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

.field public static final enum STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 35
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    const/4 v1, 0x0

    const-string v2, "CLEAR"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    const/4 v2, 0x1

    const-string v3, "STARTED_SIGNING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    const/4 v3, 0x2

    const-string v4, "SIGNED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    .line 34
    sget-object v4, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->CLEAR:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->STARTED_SIGNING:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->SIGNED:Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->$VALUES:[Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;
    .locals 1

    .line 34
    const-class v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->$VALUES:[Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    invoke-virtual {v0}, [Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;

    return-object v0
.end method
