.class public Lcom/squareup/cardcustomizations/signature/Signature;
.super Ljava/lang/Object;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/signature/Signature$Pickle;,
        Lcom/squareup/cardcustomizations/signature/Signature$Glyph;,
        Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;,
        Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;,
        Lcom/squareup/cardcustomizations/signature/Signature$SignatureState;
    }
.end annotation


# static fields
.field public static final DEFAULT_SIGNATURE_COLOR_INT:I = -0x1000000


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field public final bitmapPaint:Landroid/graphics/Paint;

.field private boundingBox:Landroid/graphics/RectF;

.field private canvas:Landroid/graphics/Canvas;

.field private currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

.field private final glyphDeque:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque<",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Signature$Glyph;",
            ">;>;"
        }
    .end annotation
.end field

.field public final height:I

.field public painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

.field private segmentCount:I

.field private final strokeWidth:F

.field public final width:I


# direct methods
.method public constructor <init>(IIFI)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 72
    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardcustomizations/signature/Signature;-><init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    return-void
.end method

.method public constructor <init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V
    .locals 2

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    .line 69
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    .line 82
    iput p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    .line 83
    iput p2, p0, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    .line 84
    iput p3, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    if-nez p5, :cond_0

    .line 86
    sget-object p1, Lcom/squareup/cardcustomizations/signature/-$$Lambda$Signature$Wy4PkdVRj0AKNRTB0SrXqBfj_mo;->INSTANCE:Lcom/squareup/cardcustomizations/signature/-$$Lambda$Signature$Wy4PkdVRj0AKNRTB0SrXqBfj_mo;

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    goto :goto_0

    .line 88
    :cond_0
    iput-object p5, p0, Lcom/squareup/cardcustomizations/signature/Signature;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    .line 91
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardcustomizations/signature/Signature;)F
    .locals 0

    .line 32
    iget p0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    return p0
.end method

.method public static convertIfNecessary(Lcom/squareup/cardcustomizations/signature/Signature;IIFILcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 7

    .line 257
    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    .line 260
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-ne v0, p4, :cond_0

    return-object p0

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->clearBitmap()V

    .line 267
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;-><init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    .line 269
    invoke-interface {p5, p1, p2}, Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 270
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    .line 271
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    .line 272
    invoke-virtual {v0, p1}, Lcom/squareup/cardcustomizations/signature/Signature;->setBitmap(Landroid/graphics/Bitmap;)V

    int-to-float p1, p2

    .line 274
    iget p2, p0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    int-to-float p2, p2

    div-float/2addr p1, p2

    int-to-float p2, p3

    .line 275
    iget p3, p0, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    int-to-float p3, p3

    div-float/2addr p2, p3

    .line 277
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->glyphs()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    .line 278
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->startGlyph()V

    .line 279
    invoke-virtual {p3}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->points()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 280
    iget p5, p4, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    mul-float p5, p5, p1

    iget p6, p4, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    mul-float p6, p6, p2

    iget-wide v1, p4, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->time:J

    invoke-virtual {v0, p5, p6, v1, v2}, Lcom/squareup/cardcustomizations/signature/Signature;->extendGlyph(FFJ)V

    goto :goto_1

    .line 282
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->finishGlyph()V

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static decode(Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 9

    .line 296
    const-class v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;

    invoke-virtual {p0, p1, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;

    .line 298
    new-instance p1, Lcom/squareup/cardcustomizations/signature/Signature;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->width:I

    iget v2, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->height:I

    iget v3, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->strokeWidth:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->color:I

    move-object v0, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardcustomizations/signature/Signature;-><init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    if-eqz p2, :cond_0

    .line 302
    iget p3, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->width:I

    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->height:I

    invoke-interface {p2, p3, v0}, Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;->createSignatureBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    .line 305
    :cond_0
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;->glyphs:[[[I

    array-length p2, p0

    const/4 p3, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_2

    aget-object v1, p0, v0

    .line 306
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Signature;->startGlyph()V

    .line 307
    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 308
    aget v5, v4, p3

    int-to-float v5, v5

    const/4 v6, 0x1

    aget v6, v4, v6

    int-to-float v6, v6

    const/4 v7, 0x2

    aget v4, v4, v7

    int-to-long v7, v4

    invoke-virtual {p1, v5, v6, v7, v8}, Lcom/squareup/cardcustomizations/signature/Signature;->extendGlyph(FFJ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 310
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Signature;->finishGlyph()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object p1
.end method

.method private getCanvas()Landroid/graphics/Canvas;
    .locals 2

    .line 328
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->canvas:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->canvas:Landroid/graphics/Canvas;

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->canvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method static synthetic lambda$new$0(Landroid/graphics/Canvas;Landroid/graphics/Paint;)Lcom/squareup/cardcustomizations/signature/GlyphPainter;
    .locals 1

    .line 86
    new-instance v0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;-><init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    return-object v0
.end method


# virtual methods
.method public bounds()Landroid/graphics/Rect;
    .locals 7

    .line 202
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    goto/16 :goto_c

    .line 206
    :cond_0
    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 207
    iget v2, p0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 208
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 209
    iget v3, p0, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v4, v0

    :goto_0
    if-gt v4, v2, :cond_3

    move v5, v1

    :goto_1
    if-gt v5, v3, :cond_2

    .line 214
    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v4, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    if-eqz v6, :cond_1

    move v0, v4

    goto :goto_2

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    move v4, v2

    :goto_3
    if-lt v4, v0, :cond_6

    move v5, v1

    :goto_4
    if-gt v5, v3, :cond_5

    .line 224
    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v4, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    if-eqz v6, :cond_4

    move v2, v4

    goto :goto_5

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_3

    :cond_6
    :goto_5
    move v4, v1

    :goto_6
    if-gt v4, v3, :cond_9

    move v5, v0

    :goto_7
    if-gt v5, v2, :cond_8

    .line 234
    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v5, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    if-eqz v6, :cond_7

    move v1, v4

    goto :goto_8

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_9
    :goto_8
    move v4, v3

    :goto_9
    if-lt v4, v1, :cond_c

    move v5, v0

    :goto_a
    if-gt v5, v2, :cond_b

    .line 244
    iget-object v6, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v5, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    if-eqz v6, :cond_a

    move v3, v4

    goto :goto_b

    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_b
    add-int/lit8 v4, v4, -0x1

    goto :goto_9

    .line 251
    :cond_c
    :goto_b
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4

    :cond_d
    :goto_c
    const/4 v0, 0x0

    return-object v0
.end method

.method public clear()V
    .locals 3

    const/4 v0, 0x0

    .line 184
    iput v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->segmentCount:I

    const/4 v0, 0x0

    .line 185
    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    .line 186
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->hasGlyphs()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 187
    :cond_0
    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    .line 188
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->clearBitmap()V

    .line 189
    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->canvas:Landroid/graphics/Canvas;

    return-void
.end method

.method public clearBitmap()V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/squareup/cardcustomizations/ui/Bitmaps;->safeRecycle(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    .line 194
    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method createPainter(Landroid/graphics/Canvas;Landroid/graphics/Paint;)Lcom/squareup/cardcustomizations/signature/GlyphPainter;
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;->createPainter(Landroid/graphics/Canvas;Landroid/graphics/Paint;)Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/google/gson/Gson;)Ljava/lang/String;
    .locals 1

    .line 290
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;

    invoke-direct {v0, p0}, Lcom/squareup/cardcustomizations/signature/Signature$Pickle;-><init>(Lcom/squareup/cardcustomizations/signature/Signature;)V

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public extendGlyph(FFJ)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->startGlyph()V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    new-instance v1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;-><init>(FFJ)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->add(Lcom/squareup/cardcustomizations/signature/Point$Timestamped;)V

    return-void
.end method

.method public finishGlyph()V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->finish()V

    .line 108
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-static {v1}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->access$000(Lcom/squareup/cardcustomizations/signature/Signature$Glyph;)Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->boundingBox()Landroid/graphics/RectF;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    .line 109
    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->segmentCount:I

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->getPointCount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->segmentCount:I

    :cond_0
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .line 170
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 173
    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->width:I

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->height:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBoundingBox()Landroid/graphics/RectF;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getSegmentCount()I
    .locals 1

    .line 161
    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->segmentCount:I

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .line 157
    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->strokeWidth:F

    return v0
.end method

.method public glyphs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Signature$Glyph;",
            ">;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public hasGlyphs()Z
    .locals 1

    .line 320
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->glyphs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public invalidateCurrentGlyph(Landroid/view/View;)V
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-virtual {v0, p1}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->invalidate(Landroid/view/View;)V

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public startGlyph()V
    .locals 2

    .line 100
    invoke-direct {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/cardcustomizations/signature/Signature;->createPainter(Landroid/graphics/Canvas;Landroid/graphics/Paint;)Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-direct {v1, v0}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;-><init>(Lcom/squareup/cardcustomizations/signature/GlyphPainter;)V

    iput-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->glyphs()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public undo()V
    .locals 4

    .line 114
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 115
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 122
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->hasGlyphs()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->glyphDeque:Ljava/util/Deque;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    :cond_2
    const/4 v1, 0x0

    .line 123
    iput v1, p0, Lcom/squareup/cardcustomizations/signature/Signature;->segmentCount:I

    const/4 v2, 0x0

    .line 124
    iput-object v2, p0, Lcom/squareup/cardcustomizations/signature/Signature;->boundingBox:Landroid/graphics/RectF;

    .line 125
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/Signature;->canvas:Landroid/graphics/Canvas;

    if-eqz v2, :cond_3

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 127
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->startGlyph()V

    .line 129
    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 130
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Signature;->currentGlyph:Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    invoke-virtual {v3, v2}, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->add(Lcom/squareup/cardcustomizations/signature/Point$Timestamped;)V

    goto :goto_2

    .line 132
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->finishGlyph()V

    goto :goto_1

    :cond_5
    return-void
.end method
