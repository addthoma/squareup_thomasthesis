.class public final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "AccessiblePinTutorialStepCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0005H\u0016J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2",
        "Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;",
        "onPageScrolled",
        "",
        "position",
        "",
        "positionOffset",
        "",
        "positionOffsetPixels",
        "onPageSelected",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 74
    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrolled(IFI)V
    .locals 1

    .line 81
    sget-object p3, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {p3}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->getTutorialPages()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    const/4 v0, 0x1

    sub-int/2addr p3, v0

    if-lt p1, p3, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    invoke-static {p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->access$getTutorialDone$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 87
    :cond_0
    iget-object p3, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    invoke-static {p3}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->access$getCrossFadeViews$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Lcom/squareup/accessibility/pin/impl/CrossFadeViews;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/squareup/accessibility/pin/impl/CrossFadeViews;->fade(IF)V

    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$2;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->access$selectPage(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;I)V

    return-void
.end method
