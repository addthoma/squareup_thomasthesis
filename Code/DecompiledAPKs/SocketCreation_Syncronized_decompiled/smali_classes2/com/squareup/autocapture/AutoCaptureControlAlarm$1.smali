.class final Lcom/squareup/autocapture/AutoCaptureControlAlarm$1;
.super Ljava/lang/Object;
.source "AutoCaptureControlAlarm.java"

# interfaces
.implements Lcom/squareup/autocapture/AutoCaptureControlAlarm;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/autocapture/AutoCaptureControlAlarm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 6

    .line 41
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->QUICK:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->start(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-void
.end method

.method public startSlowAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 6

    .line 33
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->SLOW:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->start(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-void
.end method

.method public stopQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->QUICK:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->stop(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V

    return-void
.end method

.method public stopSlowAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->SLOW:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->stop(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V

    return-void
.end method
