.class public final enum Lcom/squareup/autocapture/AutoCaptureControlAlarmType;
.super Ljava/lang/Enum;
.source "AutoCaptureControlAlarmType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/autocapture/AutoCaptureControlAlarmType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

.field public static final enum QUICK:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

.field public static final enum SLOW:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 16
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    const/4 v1, 0x0

    const-string v2, "QUICK"

    invoke-direct {v0, v2, v1}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->QUICK:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    new-instance v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    const/4 v2, 0x1

    const-string v3, "SLOW"

    invoke-direct {v0, v3, v2}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->SLOW:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    .line 15
    sget-object v3, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->QUICK:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->SLOW:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->$VALUES:[Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private isSlow()Z
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->SLOW:Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    invoke-virtual {p0, v0}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/autocapture/AutoCaptureControlAlarmType;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/autocapture/AutoCaptureControlAlarmType;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->$VALUES:[Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    invoke-virtual {v0}, [Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/autocapture/AutoCaptureControlAlarmType;

    return-object v0
.end method


# virtual methods
.method start(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 3

    .line 20
    invoke-virtual {p5}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p5

    invoke-virtual {p5}, Lcom/squareup/settings/server/PaymentSettings;->isPaymentFlowAutoCaptureEnabled()Z

    move-result p5

    if-eqz p5, :cond_1

    .line 21
    sget-object p5, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const-string v1, "start %s %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p5, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 22
    invoke-interface {p4, p0}, Lcom/squareup/autocapture/AutoCaptureControlTimer;->getIntervalMillis(Lcom/squareup/autocapture/AutoCaptureControlAlarmType;)I

    move-result p2

    int-to-long p4, p2

    .line 23
    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->isSlow()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 24
    invoke-static {p4, p5, p3}, Lcom/squareup/autocapture/AutoCaptureJob;->slowAutoCaptureRequest(JLjava/lang/String;)Lcom/evernote/android/job/JobRequest;

    move-result-object p2

    goto :goto_0

    .line 25
    :cond_0
    invoke-static {p4, p5, p3}, Lcom/squareup/autocapture/AutoCaptureJob;->quickAutoCaptureRequest(JLjava/lang/String;)Lcom/evernote/android/job/JobRequest;

    move-result-object p2

    .line 26
    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/backgroundjob/BackgroundJobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    :cond_1
    return-void
.end method

.method stop(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V
    .locals 4

    .line 31
    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "stop %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->isSlow()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/autocapture/AutoCaptureJob;->SLOW_AUTO_CAPTURE_TAG:Ljava/lang/String;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/autocapture/AutoCaptureJob;->QUICK_AUTO_CAPTURE_TAG:Ljava/lang/String;

    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/backgroundjob/BackgroundJobManager;->cancelAllForTag(Ljava/lang/String;)I

    return-void
.end method
