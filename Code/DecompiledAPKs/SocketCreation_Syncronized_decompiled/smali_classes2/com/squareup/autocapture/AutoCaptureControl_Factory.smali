.class public final Lcom/squareup/autocapture/AutoCaptureControl_Factory;
.super Ljava/lang/Object;
.source "AutoCaptureControl_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/autocapture/AutoCaptureControl;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoCaptureTimerStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureControl_Factory;->autoCaptureTimerStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/autocapture/AutoCaptureControl_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            ">;)",
            "Lcom/squareup/autocapture/AutoCaptureControl_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureControl_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/autocapture/AutoCaptureControl_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/autocapture/AutoCaptureTimerStarter;)Lcom/squareup/autocapture/AutoCaptureControl;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-direct {v0, p0}, Lcom/squareup/autocapture/AutoCaptureControl;-><init>(Lcom/squareup/autocapture/AutoCaptureTimerStarter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/autocapture/AutoCaptureControl;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureControl_Factory;->autoCaptureTimerStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    invoke-static {v0}, Lcom/squareup/autocapture/AutoCaptureControl_Factory;->newInstance(Lcom/squareup/autocapture/AutoCaptureTimerStarter;)Lcom/squareup/autocapture/AutoCaptureControl;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureControl_Factory;->get()Lcom/squareup/autocapture/AutoCaptureControl;

    move-result-object v0

    return-object v0
.end method
