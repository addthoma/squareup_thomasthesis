.class public final Lcom/squareup/common/authenticatorviews/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/authenticatorviews/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_name:I = 0x7f0a012c

.field public static final authentication_key:I = 0x7f0a01da

.field public static final button_container:I = 0x7f0a0277

.field public static final cant_scan_barcode:I = 0x7f0a02a1

.field public static final contact_support:I = 0x7f0a0398

.field public static final copy_code:I = 0x7f0a03ae

.field public static final device_code_field:I = 0x7f0a05af

.field public static final device_code_link:I = 0x7f0a05b0

.field public static final email_field:I = 0x7f0a06c0

.field public static final email_suggestion:I = 0x7f0a06cc

.field public static final forgot_password_link:I = 0x7f0a076f

.field public static final google_auth_button:I = 0x7f0a07b9

.field public static final hint:I = 0x7f0a07da

.field public static final instructions:I = 0x7f0a0861

.field public static final learn_more:I = 0x7f0a0919

.field public static final message:I = 0x7f0a09d3

.field public static final message_container:I = 0x7f0a09d6

.field public static final mobile_phone_number_field:I = 0x7f0a09e1

.field public static final password_field:I = 0x7f0a0bd3

.field public static final qr_code:I = 0x7f0a0c9b

.field public static final remember_this_device:I = 0x7f0a0d55

.field public static final row_container:I = 0x7f0a0da1

.field public static final scrollView:I = 0x7f0a0e12

.field public static final skip_enrollment_link:I = 0x7f0a0e9c

.field public static final sms_button:I = 0x7f0a0eaa

.field public static final stable_action_bar:I = 0x7f0a0f1c

.field public static final title:I = 0x7f0a103f

.field public static final title_container:I = 0x7f0a1044

.field public static final verification_code_field:I = 0x7f0a10e7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
