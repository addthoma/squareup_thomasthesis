.class public final Lcom/squareup/common/invoices/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_payment_button:I = 0x7f0a0170

.field public static final bill_to_row:I = 0x7f0a0233

.field public static final body:I = 0x7f0a023a

.field public static final bottom_buttons_container:I = 0x7f0a0240

.field public static final bottom_divider:I = 0x7f0a0241

.field public static final detail_invoice_file_attachments_container:I = 0x7f0a0598

.field public static final divider:I = 0x7f0a05e3

.field public static final due_date_row:I = 0x7f0a0608

.field public static final file_attachment_header:I = 0x7f0a074d

.field public static final header:I = 0x7f0a07c8

.field public static final invoice_additional_emails_container:I = 0x7f0a0873

.field public static final invoice_date_row:I = 0x7f0a087f

.field public static final invoice_detail_animator:I = 0x7f0a0881

.field public static final invoice_detail_content:I = 0x7f0a0882

.field public static final invoice_detail_display_state:I = 0x7f0a0883

.field public static final invoice_detail_error_message:I = 0x7f0a0884

.field public static final invoice_detail_header:I = 0x7f0a0885

.field public static final invoice_detail_legacy_status:I = 0x7f0a0886

.field public static final invoice_detail_line_items_container:I = 0x7f0a0887

.field public static final invoice_detail_line_items_divider:I = 0x7f0a0888

.field public static final invoice_detail_share_link:I = 0x7f0a0889

.field public static final invoice_detail_timeline_content:I = 0x7f0a088a

.field public static final invoice_frequency_row:I = 0x7f0a089f

.field public static final invoice_id_row:I = 0x7f0a08a3

.field public static final invoice_message:I = 0x7f0a08a6

.field public static final invoice_payment_schedule_container:I = 0x7f0a08aa

.field public static final invoice_payment_schedule_divider:I = 0x7f0a08ab

.field public static final invoice_payments_container:I = 0x7f0a08ad

.field public static final invoice_payments_divider:I = 0x7f0a08ae

.field public static final invoice_read_only_detail:I = 0x7f0a08b0

.field public static final invoice_recurring:I = 0x7f0a08b1

.field public static final invoice_timeline_button:I = 0x7f0a08be

.field public static final invoice_top_divider:I = 0x7f0a08c0

.field public static final loading_instruments:I = 0x7f0a0952

.field public static final payment_list:I = 0x7f0a0be4

.field public static final payment_schedule_list:I = 0x7f0a0bf3

.field public static final primary_container:I = 0x7f0a0c5c

.field public static final primary_header:I = 0x7f0a0c5d

.field public static final primary_value:I = 0x7f0a0c60

.field public static final progress_loading_invoice:I = 0x7f0a0c82

.field public static final second_row_container:I = 0x7f0a0e2b

.field public static final secondary_container:I = 0x7f0a0e2f

.field public static final secondary_header:I = 0x7f0a0e30

.field public static final secondary_value:I = 0x7f0a0e33

.field public static final shipping_address:I = 0x7f0a0e7b

.field public static final tertiary_container:I = 0x7f0a0f8e

.field public static final tertiary_header:I = 0x7f0a0f8f

.field public static final tertiary_value:I = 0x7f0a0f90

.field public static final title_row:I = 0x7f0a1045


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
