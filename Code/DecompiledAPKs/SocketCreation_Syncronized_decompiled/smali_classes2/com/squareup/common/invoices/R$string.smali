.class public final Lcom/squareup/common/invoices/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final additional_emails:I = 0x7f1200a1

.field public static final bill_to:I = 0x7f120182

.field public static final cc:I = 0x7f1203e1

.field public static final due_date:I = 0x7f1208e5

.field public static final estimate:I = 0x7f120a87

.field public static final invoice:I = 0x7f120c72

.field public static final invoice_amount:I = 0x7f120c79

.field public static final invoice_automatic_payment_url:I = 0x7f120c84

.field public static final invoice_charged:I = 0x7f120c95

.field public static final invoice_charged_receipt_sent:I = 0x7f120c96

.field public static final invoice_connection_error:I = 0x7f120c9d

.field public static final invoice_created:I = 0x7f120cb4

.field public static final invoice_created_subtitle:I = 0x7f120cb5

.field public static final invoice_custom:I = 0x7f120cb6

.field public static final invoice_date:I = 0x7f120cb7

.field public static final invoice_default_message:I = 0x7f120cb9

.field public static final invoice_delivery_method_email:I = 0x7f120cc0

.field public static final invoice_delivery_method_manual:I = 0x7f120cc1

.field public static final invoice_delivery_more_options:I = 0x7f120cc5

.field public static final invoice_delivery_share_body:I = 0x7f120cc6

.field public static final invoice_delivery_share_subject:I = 0x7f120cc7

.field public static final invoice_delivery_share_using:I = 0x7f120cc8

.field public static final invoice_detail_add_payment:I = 0x7f120cc9

.field public static final invoice_detail_amount_paid:I = 0x7f120cca

.field public static final invoice_detail_amount_paid_after_refund:I = 0x7f120ccb

.field public static final invoice_detail_amount_refunded:I = 0x7f120ccc

.field public static final invoice_detail_amount_remaining:I = 0x7f120ccd

.field public static final invoice_detail_charge:I = 0x7f120cce

.field public static final invoice_detail_charged:I = 0x7f120ccf

.field public static final invoice_detail_connection_error_message:I = 0x7f120cd0

.field public static final invoice_detail_customer:I = 0x7f120cd1

.field public static final invoice_detail_due:I = 0x7f120cd6

.field public static final invoice_detail_duplicate:I = 0x7f120cd7

.field public static final invoice_detail_email:I = 0x7f120cd9

.field public static final invoice_detail_frequency:I = 0x7f120cdc

.field public static final invoice_detail_id:I = 0x7f120cdd

.field public static final invoice_detail_invoice_title:I = 0x7f120cde

.field public static final invoice_detail_invoice_total:I = 0x7f120cdf

.field public static final invoice_detail_send_reminder:I = 0x7f120ce0

.field public static final invoice_detail_sent:I = 0x7f120ce1

.field public static final invoice_detail_share_link:I = 0x7f120ce3

.field public static final invoice_detail_show_full_invoice:I = 0x7f120ce4

.field public static final invoice_detail_take_payment:I = 0x7f120ce5

.field public static final invoice_detail_title:I = 0x7f120ce6

.field public static final invoice_detail_view_invoices_in_series:I = 0x7f120ce7

.field public static final invoice_detail_view_transaction:I = 0x7f120ce8

.field public static final invoice_display_state_archived:I = 0x7f120ceb

.field public static final invoice_display_state_canceled:I = 0x7f120cec

.field public static final invoice_display_state_draft:I = 0x7f120cee

.field public static final invoice_display_state_failed:I = 0x7f120cf2

.field public static final invoice_display_state_overdue:I = 0x7f120cf4

.field public static final invoice_display_state_paid:I = 0x7f120cf6

.field public static final invoice_display_state_partially_paid:I = 0x7f120cf8

.field public static final invoice_display_state_recurring:I = 0x7f120cf9

.field public static final invoice_display_state_refunded:I = 0x7f120cfb

.field public static final invoice_display_state_scheduled:I = 0x7f120cfd

.field public static final invoice_display_state_undelivered:I = 0x7f120cff

.field public static final invoice_display_state_unknown:I = 0x7f120d01

.field public static final invoice_display_state_unpaid:I = 0x7f120d03

.field public static final invoice_end_of_month:I = 0x7f120d1a

.field public static final invoice_id:I = 0x7f120d2d

.field public static final invoice_in_fourteen_days:I = 0x7f120d2f

.field public static final invoice_in_seven_days:I = 0x7f120d30

.field public static final invoice_in_thirty_days:I = 0x7f120d31

.field public static final invoice_link_copied:I = 0x7f120d32

.field public static final invoice_no_due_date:I = 0x7f120d38

.field public static final invoice_prod_url:I = 0x7f120d45

.field public static final invoice_prod_url_pay_page:I = 0x7f120d46

.field public static final invoice_recurring_period_daily:I = 0x7f120d47

.field public static final invoice_recurring_period_daily_ending:I = 0x7f120d48

.field public static final invoice_recurring_period_daily_plural:I = 0x7f120d49

.field public static final invoice_recurring_period_daily_plural_ending:I = 0x7f120d4a

.field public static final invoice_recurring_period_daily_plural_short:I = 0x7f120d4b

.field public static final invoice_recurring_period_daily_short:I = 0x7f120d4c

.field public static final invoice_recurring_period_monthly:I = 0x7f120d4d

.field public static final invoice_recurring_period_monthly_end_of_month:I = 0x7f120d4e

.field public static final invoice_recurring_period_monthly_end_of_month_ending:I = 0x7f120d4f

.field public static final invoice_recurring_period_monthly_ending:I = 0x7f120d50

.field public static final invoice_recurring_period_monthly_plural:I = 0x7f120d51

.field public static final invoice_recurring_period_monthly_plural_end_of_month:I = 0x7f120d52

.field public static final invoice_recurring_period_monthly_plural_end_of_month_ending:I = 0x7f120d53

.field public static final invoice_recurring_period_monthly_plural_ending:I = 0x7f120d54

.field public static final invoice_recurring_period_monthly_plural_short:I = 0x7f120d55

.field public static final invoice_recurring_period_monthly_short:I = 0x7f120d56

.field public static final invoice_recurring_period_weekly:I = 0x7f120d57

.field public static final invoice_recurring_period_weekly_ending:I = 0x7f120d58

.field public static final invoice_recurring_period_weekly_plural:I = 0x7f120d59

.field public static final invoice_recurring_period_weekly_plural_ending:I = 0x7f120d5a

.field public static final invoice_recurring_period_weekly_plural_short:I = 0x7f120d5b

.field public static final invoice_recurring_period_weekly_short:I = 0x7f120d5c

.field public static final invoice_recurring_period_yearly:I = 0x7f120d5d

.field public static final invoice_recurring_period_yearly_ending:I = 0x7f120d5e

.field public static final invoice_recurring_period_yearly_plural:I = 0x7f120d5f

.field public static final invoice_recurring_period_yearly_plural_ending:I = 0x7f120d60

.field public static final invoice_recurring_period_yearly_plural_short:I = 0x7f120d61

.field public static final invoice_recurring_period_yearly_short:I = 0x7f120d62

.field public static final invoice_saved:I = 0x7f120d7c

.field public static final invoice_scheduled:I = 0x7f120d80

.field public static final invoice_send_immediately:I = 0x7f120d90

.field public static final invoice_sent:I = 0x7f120d97

.field public static final invoice_share_link_confirmation:I = 0x7f120d9e

.field public static final invoice_share_link_support_url:I = 0x7f120d9f

.field public static final invoice_staging_url:I = 0x7f120da0

.field public static final invoice_staging_url_pay_page:I = 0x7f120da1

.field public static final invoice_status_canceled:I = 0x7f120da9

.field public static final invoice_status_overdue:I = 0x7f120dab

.field public static final invoice_status_paid:I = 0x7f120dac

.field public static final invoice_status_recurring:I = 0x7f120dad

.field public static final invoice_status_refunded:I = 0x7f120dae

.field public static final invoice_status_scheduled:I = 0x7f120daf

.field public static final invoice_status_unpaid:I = 0x7f120db0

.field public static final invoice_status_unpaid_no_due_date:I = 0x7f120db1

.field public static final invoice_upon_receipt:I = 0x7f120dbe

.field public static final message:I = 0x7f120fc4

.field public static final more:I = 0x7f121024

.field public static final partial_payments_amount_overdue:I = 0x7f121348

.field public static final partial_payments_balance:I = 0x7f121349

.field public static final partial_payments_card_payment_status:I = 0x7f12134a

.field public static final partial_payments_card_payment_status_no_last_four:I = 0x7f12134b

.field public static final partial_payments_cash_check_payment_status:I = 0x7f12134c

.field public static final partial_payments_deposit:I = 0x7f12134d

.field public static final partial_payments_due_date:I = 0x7f12134e

.field public static final partial_payments_due_today:I = 0x7f12134f

.field public static final partial_payments_due_upon_receipt:I = 0x7f121350

.field public static final partial_payments_due_within_future_date_plural:I = 0x7f121351

.field public static final partial_payments_due_within_future_date_singular:I = 0x7f121352

.field public static final partial_payments_due_within_past_date_plural:I = 0x7f121353

.field public static final partial_payments_due_within_past_date_singular:I = 0x7f121354

.field public static final partial_payments_installment:I = 0x7f121355

.field public static final partial_payments_other_payment_status:I = 0x7f121356

.field public static final partial_payments_overdue:I = 0x7f121357

.field public static final partial_payments_paid:I = 0x7f121358

.field public static final partial_payments_partially_paid:I = 0x7f121359

.field public static final partial_payments_unknown:I = 0x7f12135a

.field public static final partial_payments_unpaid:I = 0x7f12135b

.field public static final payment_request_in_ninety_days:I = 0x7f1213e3

.field public static final payment_request_in_one_hundred_twenty_days:I = 0x7f1213e4

.field public static final payment_request_in_sixty_days:I = 0x7f1213e5

.field public static final payment_request_in_thirty_days:I = 0x7f1213e6

.field public static final payment_request_number:I = 0x7f1213e7

.field public static final record_payment_tender_type_cash:I = 0x7f1215ed

.field public static final record_payment_tender_type_check:I = 0x7f1215ee

.field public static final record_payment_tender_type_other:I = 0x7f1215ef

.field public static final recurring:I = 0x7f1215f0

.field public static final recurring_ending_on_date:I = 0x7f1215f1

.field public static final recurring_series:I = 0x7f1215f3

.field public static final series_list_null:I = 0x7f1217c5

.field public static final series_list_title:I = 0x7f1217c6

.field public static final share:I = 0x7f1217dd

.field public static final ship_to:I = 0x7f1217e6

.field public static final time_day_ago:I = 0x7f121973

.field public static final time_days_ago:I = 0x7f121975

.field public static final title:I = 0x7f1219cd

.field public static final uppercase_customer_info:I = 0x7f121b24

.field public static final uppercase_file_attachments:I = 0x7f121b28

.field public static final uppercase_invoice_details:I = 0x7f121b44

.field public static final uppercase_invoice_options:I = 0x7f121b46

.field public static final uppercase_line_items:I = 0x7f121b4d

.field public static final uppercase_message:I = 0x7f121b4e

.field public static final uppercase_payment_schedule:I = 0x7f121b5d

.field public static final uppercase_payments:I = 0x7f121b61


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
