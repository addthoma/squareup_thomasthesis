.class public interface abstract Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;
.super Ljava/lang/Object;
.source "BuyerCheckoutLauncher.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00052\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;->$$INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;

    return-void
.end method
