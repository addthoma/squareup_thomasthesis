.class public final Lcom/squareup/buyercheckout/PaymentPromptCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "PaymentPromptCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentPromptCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentPromptCoordinator.kt\ncom/squareup/buyercheckout/PaymentPromptCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,162:1\n1103#2,7:163\n1103#2,7:170\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentPromptCoordinator.kt\ncom/squareup/buyercheckout/PaymentPromptCoordinator\n*L\n93#1,7:163\n104#1,7:170\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001-BE\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0014H\u0016J\u0010\u0010\"\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0014H\u0002J\u0010\u0010#\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0014H\u0016J\u0012\u0010$\u001a\u00020 2\u0008\u0008\u0001\u0010%\u001a\u00020&H\u0002J\u0008\u0010\'\u001a\u00020 H\u0002J\u0008\u0010(\u001a\u00020 H\u0002J0\u0010)\u001a\u00020 2\u0006\u0010!\u001a\u00020\u00142\u0016\u0010*\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010+\u001a\u00020,H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/buyercheckout/PaymentPromptCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/buyercheckout/PaymentPromptScreen;",
        "formattedTotalProvider",
        "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "transactionTypeDisplay",
        "Lcom/squareup/buyercheckout/TransactionTypeDisplay;",
        "(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)V",
        "buyerLanguageButton",
        "Landroid/widget/Button;",
        "card",
        "Landroid/view/View;",
        "cardAnimation",
        "Landroid/animation/AnimatorSet;",
        "cardSlot",
        "paymentTypeIcon",
        "Landroid/widget/ImageView;",
        "paymentTypeText",
        "Landroid/widget/TextView;",
        "transactionTotal",
        "transactionType",
        "upGlyph",
        "attach",
        "",
        "view",
        "bindViews",
        "detach",
        "maybeDisplayTransactionType",
        "typeId",
        "",
        "startAnimation",
        "stopAnimation",
        "update",
        "screen",
        "localeOverride",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "Factory",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerLanguageButton:Landroid/widget/Button;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private card:Landroid/view/View;

.field private cardAnimation:Landroid/animation/AnimatorSet;

.field private cardSlot:Landroid/view/View;

.field private final formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

.field private paymentTypeIcon:Landroid/widget/ImageView;

.field private paymentTypeText:Landroid/widget/TextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private transactionTotal:Landroid/widget/TextView;

.field private transactionType:Landroid/widget/TextView;

.field private final transactionTypeDisplay:Lcom/squareup/buyercheckout/TransactionTypeDisplay;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private upGlyph:Landroid/view/View;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;>;",
            "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/buyercheckout/TransactionTypeDisplay;",
            ")V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    iput-object p3, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p4, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p5, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionTypeDisplay:Lcom/squareup/buyercheckout/TransactionTypeDisplay;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 33
    invoke-direct/range {p0 .. p5}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)V

    return-void
.end method

.method public static final synthetic access$maybeDisplayTransactionType(Lcom/squareup/buyercheckout/PaymentPromptCoordinator;I)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->maybeDisplayTransactionType(I)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/buyercheckout/PaymentPromptCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 152
    sget v0, Lcom/squareup/tenderworkflow/R$id;->buyer_up_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->upGlyph:Landroid/view/View;

    .line 153
    sget v0, Lcom/squareup/tenderworkflow/R$id;->buyer_language_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    .line 154
    sget v0, Lcom/squareup/tenderworkflow/R$id;->transaction_total:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionTotal:Landroid/widget/TextView;

    .line 155
    sget v0, Lcom/squareup/tenderworkflow/R$id;->transaction_type_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionType:Landroid/widget/TextView;

    .line 156
    sget v0, Lcom/squareup/tenderworkflow/R$id;->card:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->card:Landroid/view/View;

    .line 157
    sget v0, Lcom/squareup/tenderworkflow/R$id;->card_slot:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardSlot:Landroid/view/View;

    .line 158
    sget v0, Lcom/squareup/tenderworkflow/R$id;->payment_type_icon:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->paymentTypeIcon:Landroid/widget/ImageView;

    .line 159
    sget v0, Lcom/squareup/tenderworkflow/R$id;->payment_type_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->paymentTypeText:Landroid/widget/TextView;

    return-void
.end method

.method private final maybeDisplayTransactionType(I)V
    .locals 2

    const-string v0, "transactionType"

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 126
    iget-object p1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionType:Landroid/widget/TextView;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionType:Landroid/widget/TextView;

    if-nez v1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private final startAnimation()V
    .locals 2

    .line 133
    invoke-static {}, Lcom/squareup/util/Views;->noAnimationsForInstrumentation()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardAnimation:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_1

    const-string v1, "cardAnimation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 136
    :cond_1
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 137
    new-instance v1, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$startAnimation$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$startAnimation$1$1;-><init>(Landroid/animation/AnimatorSet;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 142
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method private final stopAnimation()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardAnimation:Landroid/animation/AnimatorSet;

    const-string v1, "cardAnimation"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 148
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardAnimation:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ")V"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->upGlyph:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "upGlyph"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 163
    :cond_0
    new-instance v1, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p2}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    const-string v1, "buyerLanguageButton"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    iget-object v2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;->getEnableLanguageSelection()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 96
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;->getEnableLanguageSelection()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 97
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 98
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    .line 99
    sget v3, Lcom/squareup/activity/R$drawable;->buyer_language_icon_white:I

    .line 98
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    .line 97
    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/locale/LocaleFormatter;

    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/locale/LocaleFormatter;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v2}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :cond_4
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    .line 170
    new-instance v1, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$update$$inlined$onClickDebounced$2;

    invoke-direct {v1, p2}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$update$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    new-instance v0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$update$3;

    invoke-direct {v0, p2}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$update$3;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionTotal:Landroid/widget/TextView;

    if-nez p1, :cond_6

    const-string v0, "transactionTotal"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/FormattedTotalProvider;->getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object p1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;->getOfflineMode()Z

    move-result p1

    if-eqz p1, :cond_7

    sget-object p1, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->OFFLINE:Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    goto :goto_0

    :cond_7
    sget-object p1, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->ONLINE:Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    .line 112
    :goto_0
    iget-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->paymentTypeIcon:Landroid/widget/ImageView;

    if-nez p2, :cond_8

    const-string v0, "paymentTypeIcon"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->getPaymentTypeIconRes()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 113
    iget-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->paymentTypeText:Landroid/widget/TextView;

    if-nez p2, :cond_9

    const-string v0, "paymentTypeText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p3

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->getPaymentTypeTextRes()I

    move-result v0

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->card:Landroid/view/View;

    if-nez p2, :cond_a

    const-string p3, "card"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->getCardVisibility()Z

    move-result p3

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 115
    iget-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardSlot:Landroid/view/View;

    if-nez p2, :cond_b

    const-string p3, "cardSlot"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->getCardVisibility()Z

    move-result p3

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 116
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->getAnimateCard()Z

    move-result p1

    if-eqz p1, :cond_c

    invoke-direct {p0}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->startAnimation()V

    goto :goto_1

    :cond_c
    invoke-direct {p0}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->stopAnimation()V

    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->bindViews(Landroid/view/View;)V

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/tenderworkflow/R$animator;->payment_prompt_card:I

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardAnimation:Landroid/animation/AnimatorSet;

    .line 74
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->cardAnimation:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    const-string v1, "cardAnimation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->card:Landroid/view/View;

    if-nez v1, :cond_1

    const-string v2, "card"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    .line 76
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->screens:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$attach$1;-><init>(Lcom/squareup/buyercheckout/PaymentPromptCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(screens, b\u2026verrideFactory)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Shown PaymentPromptScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->transactionTypeDisplay:Lcom/squareup/buyercheckout/TransactionTypeDisplay;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/TransactionTypeDisplay;->displayText()Lio/reactivex/Single;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$attach$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$attach$2;-><init>(Lcom/squareup/buyercheckout/PaymentPromptCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "transactionTypeDisplay.d\u2026beDisplayTransactionType)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void

    .line 73
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.animation.AnimatorSet"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;->stopAnimation()V

    .line 121
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method
