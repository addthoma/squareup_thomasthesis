.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleWaitingForCancelPermission(Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBuyerCheckoutLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1\n*L\n1#1,641:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "granted",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lcom/squareup/workflow/legacy/EnterState;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            ">;"
        }
    .end annotation

    const-string v0, "granted"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 255
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;->getLog()Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getAnalytics$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 256
    :cond_0
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;->getToState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 258
    :cond_1
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;->getFromState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;->apply(Ljava/lang/Boolean;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
