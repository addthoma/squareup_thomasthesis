.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
        "+",
        "Lcom/squareup/workflow/legacyintegration/LegacyState<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;+",
        "Lkotlin/Unit;",
        "+",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        ">;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "+",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042.\u0010\u0005\u001a*\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0008\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\n0\t0\u0007\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u000b0\u0006H\n\u00a2\u0006\u0002\u0008\u000c"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutWorkflowReaction;",
        "it",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/legacy/WorkflowUpdate;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "+",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    instance-of v0, p1, Lcom/squareup/workflow/legacy/Running;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 378
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    .line 379
    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    iget-object v2, v2, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v2

    .line 380
    check-cast p1, Lcom/squareup/workflow/legacy/Running;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Running;->getHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 378
    invoke-direct {v1, v2, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    .line 377
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    goto/16 :goto_1

    .line 383
    :cond_0
    instance-of v0, p1, Lcom/squareup/workflow/legacy/Finished;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/workflow/legacy/Finished;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Finished;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/tip/BillTipResult;

    .line 384
    sget-object v0, Lcom/squareup/ui/buyer/tip/BillTipResult$TipEntered;->INSTANCE:Lcom/squareup/ui/buyer/tip/BillTipResult$TipEntered;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 385
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    iget-object v1, v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {v1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    .line 384
    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 387
    :cond_1
    sget-object v0, Lcom/squareup/ui/buyer/tip/BillTipResult$SelectingLanguage;->INSTANCE:Lcom/squareup/ui/buyer/tip/BillTipResult$SelectingLanguage;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 388
    new-instance v6, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    move-object v2, v0

    check-cast v2, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 387
    invoke-direct {p1, v6}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 390
    :cond_2
    sget-object v0, Lcom/squareup/ui/buyer/tip/BillTipResult$ExitTip;->INSTANCE:Lcom/squareup/ui/buyer/tip/BillTipResult$ExitTip;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    iget-object v0, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    .line 391
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    iget-object p1, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    move-object v1, p1

    check-cast v1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 392
    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    sget-object v2, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    check-cast v2, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-direct {p1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 390
    invoke-static/range {v0 .. v5}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->enterStateWaitingForCancelPermission$default(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    .line 383
    :goto_0
    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    :goto_1
    return-object v0

    .line 390
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 383
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowUpdate;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1$1;->invoke(Lcom/squareup/workflow/legacy/WorkflowUpdate;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
