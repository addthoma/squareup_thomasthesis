.class public final enum Lcom/squareup/buyercheckout/PaymentPromptScreenState;
.super Ljava/lang/Enum;
.source "PaymentPromptScreen.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/buyercheckout/PaymentPromptScreenState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\'\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rj\u0002\u0008\u000fj\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/PaymentPromptScreenState;",
        "",
        "paymentTypeIconRes",
        "",
        "paymentTypeTextRes",
        "cardVisibility",
        "",
        "animateCard",
        "(Ljava/lang/String;IIIZZ)V",
        "getAnimateCard",
        "()Z",
        "getCardVisibility",
        "getPaymentTypeIconRes",
        "()I",
        "getPaymentTypeTextRes",
        "ONLINE",
        "OFFLINE",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/buyercheckout/PaymentPromptScreenState;

.field public static final enum OFFLINE:Lcom/squareup/buyercheckout/PaymentPromptScreenState;

.field public static final enum ONLINE:Lcom/squareup/buyercheckout/PaymentPromptScreenState;


# instance fields
.field private final animateCard:Z

.field private final cardVisibility:Z

.field private final paymentTypeIconRes:I

.field private final paymentTypeTextRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    new-instance v8, Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    .line 16
    sget v4, Lcom/squareup/tenderworkflow/R$drawable;->nfc_icon:I

    .line 17
    sget v5, Lcom/squareup/tenderworkflow/R$string;->payment_prompt_charge_text:I

    const-string v2, "ONLINE"

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, v8

    .line 15
    invoke-direct/range {v1 .. v7}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;-><init>(Ljava/lang/String;IIIZZ)V

    sput-object v8, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->ONLINE:Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    const/4 v1, 0x0

    aput-object v8, v0, v1

    new-instance v1, Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    .line 22
    sget v12, Lcom/squareup/tenderworkflow/R$drawable;->card_swipe_icon:I

    .line 23
    sget v13, Lcom/squareup/tenderworkflow/R$string;->payment_methods_prompt_offline_mode:I

    const-string v10, "OFFLINE"

    const/4 v11, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v9, v1

    .line 21
    invoke-direct/range {v9 .. v15}, Lcom/squareup/buyercheckout/PaymentPromptScreenState;-><init>(Ljava/lang/String;IIIZZ)V

    sput-object v1, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->OFFLINE:Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->$VALUES:[Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZZ)V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->paymentTypeIconRes:I

    iput p4, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->paymentTypeTextRes:I

    iput-boolean p5, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->cardVisibility:Z

    iput-boolean p6, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->animateCard:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/buyercheckout/PaymentPromptScreenState;
    .locals 1

    const-class v0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/buyercheckout/PaymentPromptScreenState;
    .locals 1

    sget-object v0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->$VALUES:[Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    invoke-virtual {v0}, [Lcom/squareup/buyercheckout/PaymentPromptScreenState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/buyercheckout/PaymentPromptScreenState;

    return-object v0
.end method


# virtual methods
.method public final getAnimateCard()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->animateCard:Z

    return v0
.end method

.method public final getCardVisibility()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->cardVisibility:Z

    return v0
.end method

.method public final getPaymentTypeIconRes()I
    .locals 1

    .line 10
    iget v0, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->paymentTypeIconRes:I

    return v0
.end method

.method public final getPaymentTypeTextRes()I
    .locals 1

    .line 11
    iget v0, p0, Lcom/squareup/buyercheckout/PaymentPromptScreenState;->paymentTypeTextRes:I

    return v0
.end method
