.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/buyercheckout/OnCancelPaymentClicked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
        "it",
        "Lcom/squareup/buyercheckout/OnCancelPaymentClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/buyercheckout/OnCancelPaymentClicked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/OnCancelPaymentClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;

    iget-object p1, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    .line 323
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 324
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    sget-object v2, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    check-cast v2, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-direct {v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    check-cast v1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 325
    sget-object v2, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ExitPaymentPrompt;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ExitPaymentPrompt;

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 322
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->access$enterStateWaitingForCancelPermission(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/buyercheckout/OnCancelPaymentClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;->invoke(Lcom/squareup/buyercheckout/OnCancelPaymentClicked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
