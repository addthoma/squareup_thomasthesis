.class public final Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;
.super Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;
.source "BuyerCheckoutEventStreamEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewPaymentPrompt"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;",
        "()V",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;

    invoke-direct {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;-><init>()V

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 38
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CUSTOMER_CHECKOUT_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterViewName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    const-string v2, "CUSTOMER_CHECKOUT_PAYMENT_PROMPT.value"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
