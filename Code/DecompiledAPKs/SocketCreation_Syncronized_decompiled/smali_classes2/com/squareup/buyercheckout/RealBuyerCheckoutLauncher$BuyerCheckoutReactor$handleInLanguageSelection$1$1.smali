.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
        "+",
        "Lcom/squareup/workflow/legacyintegration/LegacyState<",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;+",
        "Lkotlin/Unit;",
        "+",
        "Lcom/squareup/buyer/language/Exit;",
        ">;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "+",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042<\u0010\u0005\u001a8\u0012(\u0012&\u0012\u0004\u0012\u00020\u0008\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\t0\u0007\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r0\u0006H\n\u00a2\u0006\u0002\u0008\u000e"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutWorkflowReaction;",
        "it",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/buyer/language/Exit;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/legacy/WorkflowUpdate;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lkotlin/Unit;",
            "Lcom/squareup/buyer/language/Exit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 408
    instance-of v0, p1, Lcom/squareup/workflow/legacy/Running;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 409
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    .line 410
    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;

    iget-object v2, v2, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v2

    .line 411
    iget-object v3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;

    iget-object v3, v3, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v3}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getFromState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v3

    .line 412
    check-cast p1, Lcom/squareup/workflow/legacy/Running;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Running;->getHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 409
    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    .line 408
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 415
    :cond_0
    instance-of p1, p1, Lcom/squareup/workflow/legacy/Finished;

    if-eqz p1, :cond_1

    .line 417
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getFromState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowUpdate;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1$1;->invoke(Lcom/squareup/workflow/legacy/WorkflowUpdate;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
