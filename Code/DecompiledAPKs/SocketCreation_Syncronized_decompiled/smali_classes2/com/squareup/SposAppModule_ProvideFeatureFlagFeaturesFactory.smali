.class public final Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;
.super Ljava/lang/Object;
.source "SposAppModule_ProvideFeatureFlagFeaturesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/server/FeatureFlagFeatures;",
        ">;"
    }
.end annotation


# instance fields
.field private final factoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;->factoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;",
            ">;)",
            "Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;

    invoke-direct {v0, p0}, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFeatureFlagFeatures(Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;)Lcom/squareup/settings/server/FeatureFlagFeatures;
    .locals 1

    .line 33
    invoke-static {p0}, Lcom/squareup/SposAppModule;->provideFeatureFlagFeatures(Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/FeatureFlagFeatures;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/server/FeatureFlagFeatures;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;->factoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;

    invoke-static {v0}, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;->provideFeatureFlagFeatures(Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;->get()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    return-object v0
.end method
