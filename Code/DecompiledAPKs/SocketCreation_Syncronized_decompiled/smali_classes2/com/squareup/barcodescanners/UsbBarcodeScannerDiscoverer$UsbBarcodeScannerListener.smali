.class Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;
.super Ljava/lang/Object;
.source "UsbBarcodeScannerDiscoverer.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScanner$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbBarcodeScannerListener"
.end annotation


# instance fields
.field barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

.field final synthetic this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;


# direct methods
.method constructor <init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    iput-object p2, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

    return-void
.end method


# virtual methods
.method public barcodeScannerConnected()V
    .locals 2

    .line 331
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$800(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V

    return-void
.end method

.method public barcodeScannerDisconnected()V
    .locals 2

    .line 335
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$800(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V

    return-void
.end method

.method public characterScanned(C)V
    .locals 2

    .line 339
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$800(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;->barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->characterScannedForBarcodeScanner(Lcom/squareup/barcodescanners/BarcodeScanner;C)V

    return-void
.end method
