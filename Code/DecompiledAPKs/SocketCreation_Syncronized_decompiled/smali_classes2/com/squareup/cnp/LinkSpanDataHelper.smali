.class public final Lcom/squareup/cnp/LinkSpanDataHelper;
.super Ljava/lang/Object;
.source "LinkSpanDataHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/cnp/LinkSpanDataHelper;",
        "",
        "()V",
        "formatLinkSpanData",
        "",
        "linkSpanData",
        "Lcom/squareup/cnp/LinkSpanData;",
        "context",
        "Landroid/content/Context;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final formatLinkSpanData(Lcom/squareup/cnp/LinkSpanData;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 3

    const-string v0, "linkSpanData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-virtual {p1}, Lcom/squareup/cnp/LinkSpanData;->getPattern()Lcom/squareup/cnp/LinkSpanData$PatternData;

    move-result-object v0

    .line 13
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v1, p2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 14
    invoke-virtual {v0}, Lcom/squareup/cnp/LinkSpanData$PatternData;->getPatternId()I

    move-result p2

    invoke-virtual {v0}, Lcom/squareup/cnp/LinkSpanData$PatternData;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 15
    invoke-virtual {v0}, Lcom/squareup/cnp/LinkSpanData$PatternData;->getUrlId()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 16
    invoke-virtual {v0}, Lcom/squareup/cnp/LinkSpanData$PatternData;->getClickableTextId()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 17
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 19
    invoke-virtual {p1}, Lcom/squareup/cnp/LinkSpanData;->getPut()Lcom/squareup/cnp/LinkSpanData$PutData;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 20
    invoke-virtual {p1}, Lcom/squareup/cnp/LinkSpanData$PutData;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cnp/LinkSpanData$PutData;->getValue()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "phrase.put(put.key, put.value).format()"

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "phrase.format()"

    :goto_0
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
