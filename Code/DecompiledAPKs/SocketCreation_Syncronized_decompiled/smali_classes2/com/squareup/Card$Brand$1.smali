.class final enum Lcom/squareup/Card$Brand$1;
.super Lcom/squareup/Card$Brand;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/Card$Brand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 23
    invoke-direct/range {v0 .. v7}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[ILcom/squareup/Card$1;)V

    return-void
.end method


# virtual methods
.method public checkForPanWarning(Ljava/lang/String;)Lcom/squareup/Card$PanWarning;
    .locals 2

    .line 31
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/squareup/Luhn;->validate(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    sget-object p1, Lcom/squareup/Card$PanWarning;->VISA_INVALID_16_DIGIT_PAN:Lcom/squareup/Card$PanWarning;

    return-object p1

    .line 34
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/Card$Brand;->checkForPanWarning(Ljava/lang/String;)Lcom/squareup/Card$PanWarning;

    move-result-object p1

    return-object p1
.end method

.method public validateLuhnIfRequired(Ljava/lang/String;)Z
    .locals 4

    .line 26
    invoke-static {p1}, Lcom/squareup/Luhn;->luhnSum(Ljava/lang/CharSequence;)I

    move-result v0

    .line 27
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x13

    if-ne p1, v3, :cond_0

    const/4 p1, -0x1

    if-eq v0, p1, :cond_1

    goto :goto_0

    :cond_0
    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
