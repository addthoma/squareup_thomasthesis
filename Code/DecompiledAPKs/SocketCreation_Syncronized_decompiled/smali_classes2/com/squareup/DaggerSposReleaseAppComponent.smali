.class public final Lcom/squareup/DaggerSposReleaseAppComponent;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/SposReleaseAppComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$AA_ComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    }
.end annotation


# instance fields
.field private accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private accountStatusStoreAndForwardEnabledSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;",
            ">;"
        }
    .end annotation
.end field

.field private activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private adIdGathererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdIdGatherer;",
            ">;"
        }
    .end annotation
.end field

.field private addAppNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AddAppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private androidGeoAddressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/AndroidGeoAddressProvider;",
            ">;"
        }
    .end annotation
.end field

.field private androidGeoCountryCodeGuesserProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/AndroidGeoCountryCodeGuesser;",
            ">;"
        }
    .end annotation
.end field

.field private androidGeoLocationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/AndroidGeoLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

.field private final appBootstrapModule:Lcom/squareup/AppBootstrapModule;

.field private appUpgradeDetectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AppUpgradeDetector;",
            ">;"
        }
    .end annotation
.end field

.field private appsFlyerAdAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private appsFlyerDeepLinkListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;",
            ">;"
        }
    .end annotation
.end field

.field private audioRingBufferProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/wav/AudioRingBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private authHttpInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/AuthHttpInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private authenticatedSquareDownloadManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/AuthenticatedSquareDownloadManager;",
            ">;"
        }
    .end annotation
.end field

.field private autoCaptureStatusBarNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoCaptureStatusBarNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private autoVoidStatusBarNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoidStatusBarNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private backgroundJobLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/log/BackgroundJobLogger;",
            ">;"
        }
    .end annotation
.end field

.field private barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private bindRealOnlineStoreRestrictions$impl_wiring_releaseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field

.field private bindRelinkerLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private bleConnectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleConnector;",
            ">;"
        }
    .end annotation
.end field

.field private bleEventLogFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;"
        }
    .end annotation
.end field

.field private branchHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/BranchHelper;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderHubScoperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderMessagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private clientSettingsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsCache;",
            ">;"
        }
    .end annotation
.end field

.field private corruptQueueHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ">;"
        }
    .end annotation
.end field

.field private corruptQueueRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private crashReportingLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReportingLogger;",
            ">;"
        }
    .end annotation
.end field

.field private dateAndTimeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private defaultIntentAvailabilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/DefaultIntentAvailabilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private defaultPersistentFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/DefaultPersistentFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

.field private encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;"
        }
    .end annotation
.end field

.field private environmentDiscoveryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/useragent/EnvironmentDiscovery;",
            ">;"
        }
    .end annotation
.end field

.field private es1BatchUploaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es1BatchUploader;",
            ">;"
        }
    .end annotation
.end field

.field private es2BatchUploaderProvider:Ljavax/inject/Provider;

.field private eventSinkSessionExpiredHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/EventSinkSessionExpiredHandler;",
            ">;"
        }
    .end annotation
.end field

.field private eventStreamAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EventStreamAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private factoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private fcmPushServiceAvailabilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;",
            ">;"
        }
    .end annotation
.end field

.field private fcmPushServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/fcm/FcmPushService;",
            ">;"
        }
    .end annotation
.end field

.field private featureFlagsForLogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;"
        }
    .end annotation
.end field

.field private fileBackedAuthenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/FileBackedAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private fileThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private fileThreadHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/FileThreadHolder;",
            ">;"
        }
    .end annotation
.end field

.field private firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private forwardedPaymentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private getMinesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private getMsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

.field private final globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

.field private headsetStateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private headsetStateInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private hudToasterLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/log/HudToasterLogger;",
            ">;"
        }
    .end annotation
.end field

.field private installationIdGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/InstallationIdGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private iposSkipOnboardingProductionExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private iposSkipOnboardingStagingExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private legacyAccountStatusProvider:Ljavax/inject/Provider;

.field private legacyAccountStatusResponseCacheProvider:Ljavax/inject/Provider;

.field private locationAnalyticsUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/analytics/LocationAnalyticsUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private loggedOutFeatureStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutFeatureStarter;",
            ">;"
        }
    .end annotation
.end field

.field private loggingHttpProfilerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/LoggingHttpProfiler;",
            ">;"
        }
    .end annotation
.end field

.field private macAddressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/MacAddressProvider;",
            ">;"
        }
    .end annotation
.end field

.field private mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;"
        }
    .end annotation
.end field

.field private messagesSyncNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/communications/service/sync/MessagesSyncNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private minesweeperHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperHelper;",
            ">;"
        }
    .end annotation
.end field

.field private notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private ohSnapBusBoyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapBusBoy;",
            ">;"
        }
    .end annotation
.end field

.field private paymentIncompleteStatusBarNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private pendingPreferencesCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PendingPreferencesCache;",
            ">;"
        }
    .end annotation
.end field

.field private percentageChangeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private persistentAccountServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;"
        }
    .end annotation
.end field

.field private persistentAccountStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field

.field private phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/scrubber/PhoneNumberScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private posLearnMoreTourPagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pos/loggedout/PosLearnMoreTourPager;",
            ">;"
        }
    .end annotation
.end field

.field private positiveNegativeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PositiveNegativeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private processUniqueIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;"
        }
    .end annotation
.end field

.field private final prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

.field private profilingInterceptorProvider:Ljavax/inject/Provider;

.field private provideAccessibilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideAccountServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AccountService;",
            ">;"
        }
    .end annotation
.end field

.field private provideActivationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;"
        }
    .end annotation
.end field

.field private provideActivityListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideAdIdCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private provideAddressServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/address/AddressService;",
            ">;"
        }
    .end annotation
.end field

.field private provideAllExperimentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideAndroidIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideAndroidMainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private provideAndroidSerialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideApiUrlSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector;",
            ">;"
        }
    .end annotation
.end field

.field private provideAppDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private provideApplicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private provideAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/appsflyer/AppsFlyerLib;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioHandlerThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/media/AudioManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private provideAuthenticatedRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideAuthenticationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AuthenticationService;",
            ">;"
        }
    .end annotation
.end field

.field private provideBankAccountServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bankaccount/BankAccountService;",
            ">;"
        }
    .end annotation
.end field

.field private provideBillCreationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field

.field private provideBillListServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListService;",
            ">;"
        }
    .end annotation
.end field

.field private provideBillRefundServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/BillRefundService;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleScanFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private provideBluetoothDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private provideBluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private provideBluetoothLeScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;"
        }
    .end annotation
.end field

.field private provideBluetoothStatusReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private provideBranPaymentPromptExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private provideBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private provideCacheDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInfoForRemoteReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private provideCashDrawerExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private provideCashManagementServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;"
        }
    .end annotation
.end field

.field private provideCatalogConnectV2ServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogConnectV2Service;",
            ">;"
        }
    .end annotation
.end field

.field private provideCatalogServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogService;",
            ">;"
        }
    .end annotation
.end field

.field private provideClientInvoiceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/ClientInvoiceService;",
            ">;"
        }
    .end annotation
.end field

.field private provideCogsAuthenticatedRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideCogsAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideCogsOkHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideCogsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsService;",
            ">;"
        }
    .end annotation
.end field

.field private provideCompactMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideCompactShorterMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideConnectApiServerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/api/ConnectService;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectivityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideCorruptQueuePreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private provideCountryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private provideCouponsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/coupons/CouponsService;",
            ">;"
        }
    .end annotation
.end field

.field private provideCrashReporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;"
        }
    .end annotation
.end field

.field private provideCrashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private provideCrashnadoReporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/CrashnadoReporter;",
            ">;"
        }
    .end annotation
.end field

.field private provideCrossSessionStoreAndForwardTasksQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private provideDailyLocalSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideDamagedReaderServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/DamagedReaderService;",
            ">;"
        }
    .end annotation
.end field

.field private provideDataListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$DataListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideDebuggableAccountStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private provideDefaultToLoginProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideDeferredDeepLinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideDepositScheduleServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/depositschedule/DepositScheduleService;",
            ">;"
        }
    .end annotation
.end field

.field private provideDeviceIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;"
        }
    .end annotation
.end field

.field private provideDeviceNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideDeviceNameSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideDevicePreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private provideDiagnosticsRetrofitClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;"
        }
    .end annotation
.end field

.field private provideDialogueServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/crm/DialogueService;",
            ">;"
        }
    .end annotation
.end field

.field private provideDiscountPercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideDisputesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/disputes/DisputesService;",
            ">;"
        }
    .end annotation
.end field

.field private provideDownloadManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/DownloadManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideEmailSupportLedgerEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private provideEmployeesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;"
        }
    .end annotation
.end field

.field private provideEncryptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            ">;"
        }
    .end annotation
.end field

.field private provideEnsureThumborProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventStreamProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventStreamServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EventStreamService;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventStreamV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;"
        }
    .end annotation
.end field

.field private provideExecutorServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private provideExperimentsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/FeatureFlagFeatures;",
            ">;"
        }
    .end annotation
.end field

.field private provideFelicaAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideFelicaServiceOkHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideFelicaServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaService;",
            ">;"
        }
    .end annotation
.end field

.field private provideFelicatAuthenticatedRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideFileHandlerThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/HandlerThread;",
            ">;"
        }
    .end annotation
.end field

.field private provideFileThreadSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private provideFileThreadSchedulerProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private provideFilesDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private provideFilethreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirebaseAppProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/FirebaseApp;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirebaseInstanceIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/iid/FirebaseInstanceId;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirebaseMessagingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/messaging/FirebaseMessaging;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateService;",
            ">;"
        }
    .end annotation
.end field

.field private provideForegroundServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private provideGiftCardServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/GiftCardService;",
            ">;"
        }
    .end annotation
.end field

.field private provideGsonRestAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/RestAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private provideGsonServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideHasConnectedToR6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHasCreatedItemBeforeAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHasFinishedIdVerificationBeforeAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHasSentInvoiceOrTakenPaymentBeforeAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHasSignedInBeforeAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHasSignedUpBeforeAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHasTakenItemizedPaymentBeforeAppsFlyerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideHeadsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private provideHeadsetListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private provideHeadsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset;",
            ">;"
        }
    .end annotation
.end field

.field private provideHttpProfilerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;>;"
        }
    .end annotation
.end field

.field private provideImageServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ImageService;",
            ">;"
        }
    .end annotation
.end field

.field private provideInstallationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideInstantDepositsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositsService;",
            ">;"
        }
    .end annotation
.end field

.field private provideInventoryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;"
        }
    .end annotation
.end field

.field private provideInvoiceFileAttachmentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/InvoiceFileAttachmentService;",
            ">;"
        }
    .end annotation
.end field

.field private provideIposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;"
        }
    .end annotation
.end field

.field private provideJediServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;"
        }
    .end annotation
.end field

.field private provideJobNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideLCRExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private provideLastBestLocationPersistentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/LastBestLocationStore;",
            ">;"
        }
    .end annotation
.end field

.field private provideLastEmptyStoredPaymentLoggedAtProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideLastQueueServiceStartProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideLatestApiSequenceUuidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideLinkDebitCardServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/LinkDebitCardService;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocaleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocationComparerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/comparer/LocationComparer;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/LocationManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private provideLoggedOutRetrofitQueueFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;",
            ">;"
        }
    .end annotation
.end field

.field private provideLogoutServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/LogoutService;",
            ">;"
        }
    .end annotation
.end field

.field private provideLongDateFormatNoYearProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideLongDateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideLoyaltyServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/loyalty/LoyaltyService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMediumDateFormatNoYearProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideMediumDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideMerchantProfileServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMessagehubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messagehub/MessagehubService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMessagesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messages/MessagesService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMinesweeperExecutorServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMinesweeperLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;"
        }
    .end annotation
.end field

.field private provideMinesweeperServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMinesweeperTicketProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;"
        }
    .end annotation
.end field

.field private provideMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideMoneyLocaleFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private provideMsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Ms;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultipassServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/MultipassService;",
            ">;"
        }
    .end annotation
.end field

.field private provideNativeLibraryLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;",
            ">;"
        }
    .end annotation
.end field

.field private provideNewInvoiceFeaturesTutorialTipsDismissedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private provideNfcReaderHasConnectedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideNumberFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideOkHttpClientBuilderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private provideOkHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideOnboardRedirectPathProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideOnboardingServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/onboard/OnboardingService;",
            ">;"
        }
    .end annotation
.end field

.field private provideOrderHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/orderhub/OrderHubService;",
            ">;"
        }
    .end annotation
.end field

.field private provideOrdersServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/orders/OrdersService;",
            ">;"
        }
    .end annotation
.end field

.field private providePackageManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;"
        }
    .end annotation
.end field

.field private providePairedReaderNamesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private providePaperSignatureBatchServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/papersignature/PaperSignatureBatchService;",
            ">;"
        }
    .end annotation
.end field

.field private providePasswordServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/PasswordService;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private providePercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private providePicassoMemoryCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;"
        }
    .end annotation
.end field

.field private providePicassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private providePrecogServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/precog/PrecogService;",
            ">;"
        }
    .end annotation
.end field

.field private provideProtoRestAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/RestAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private provideProtoServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private providePublicStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/PublicApiService;",
            ">;"
        }
    .end annotation
.end field

.field private providePushServiceEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private providePushServiceRegistrationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideQueueConverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideR12FirstTimeTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideR12HasConnectedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideR6FirstTimeVideoViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideRealBalanceActivityServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/service/BalanceActivityService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealBizbankServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealCapitalFlexLoanServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealCardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealCentsMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideRealCommunicationsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/communications/service/CommunicationsService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealCustomReportServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealNfcUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/NfcAdapterShim;",
            ">;"
        }
    .end annotation
.end field

.field private provideRealTransfersServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;"
        }
    .end annotation
.end field

.field private provideReceiptServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
            ">;"
        }
    .end annotation
.end field

.field private provideReferralServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/referral/ReferralService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRegisterPushNotificationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRemoteCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BranRemoteCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private provideReportCoredumpServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ReportCoredumpService;",
            ">;"
        }
    .end annotation
.end field

.field private provideReportEmailServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/reporting/ReportEmailService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRequestedPermissionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideResProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private provideResourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private provideRetrofitClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/client/Client;",
            ">;"
        }
    .end annotation
.end field

.field private provideRewardsBalanceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/RewardsBalanceService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRolodexServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/crm/RolodexService;",
            ">;"
        }
    .end annotation
.end field

.field private provideRxDevicePreferences2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private provideSafetyNetServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/SafetyNetService;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionService;",
            ">;"
        }
    .end annotation
.end field

.field private provideSerialAudioThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private provideServerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private provideSessionIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideShippingAddressServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/shipping/ShippingAddressService;",
            ">;"
        }
    .end annotation
.end field

.field private provideShortDateFormatNoYearProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideShortDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideShortDateTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideShortFractionalPercentageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideShortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideSpeedTestServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaSpeedTestService;",
            ">;"
        }
    .end annotation
.end field

.field private provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private provideStandardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private provideStartUptimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private provideStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private provideStoreAndForwardBillServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;"
        }
    .end annotation
.end field

.field private provideStoreAndForwardPaymentTaskConverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;",
            ">;"
        }
    .end annotation
.end field

.field private provideStoredPaymentsQueueCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideSystemBleScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;",
            ">;"
        }
    .end annotation
.end field

.field private provideTaxPercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideTelephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimeFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimeZoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimecardsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/TimecardsService;",
            ">;"
        }
    .end annotation
.end field

.field private provideTourEducationItemsSeenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Set<",
            "Lcom/squareup/tour/Education;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private provideTourEducationItemsSeenVerticalProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private provideTransactionLedgerOkHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideTransactionLedgerRestAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/RestAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private provideTransactionLedgerServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideTransactionLedgerServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/transaction_ledger/TransactionLedgerService;",
            ">;"
        }
    .end annotation
.end field

.field private provideTransferReportsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/reporting/TransferReportsService;",
            ">;"
        }
    .end annotation
.end field

.field private provideUnauthenticatedClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideUnauthenticatedRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideUnauthenticatedWeeblyRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideUrlRedirectSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;"
        }
    .end annotation
.end field

.field private provideUsbBarcodeScannersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private provideUsbManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserAgentIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private provideVibratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field

.field private provideViewHierarchyBuilderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private provideViewHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideWeeblyApiServerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private provideWeeblyServiceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field

.field private provideWholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideWifiManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/wifi/WifiManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideWindowManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/WindowManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideinstallmentsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
            ">;"
        }
    .end annotation
.end field

.field private providesAccountManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/accounts/AccountManager;",
            ">;"
        }
    .end annotation
.end field

.field private providesBluetoothManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;"
        }
    .end annotation
.end field

.field private providesFirstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private providesGooglePayClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesLoyaltyAdjustPointsTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private providesLoyaltyEnrollTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private providesLoyaltyRedeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private providesUUIDGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private queueJobCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueJobCreator;",
            ">;"
        }
    .end annotation
.end field

.field private readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private readerSessionIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;"
        }
    .end annotation
.end field

.field private realAccountStatusSettingsApiUrlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;",
            ">;"
        }
    .end annotation
.end field

.field private realAndroidConfigurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private realBackgroundJobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/RealBackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private realBleAutoConnectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/RealBleAutoConnector;",
            ">;"
        }
    .end annotation
.end field

.field private realConnectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/RealConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private realCurrentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/RealCurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private realCurrentTimeZoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/RealCurrentTimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private realDeviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/utilities/ui/RealDevice;",
            ">;"
        }
    .end annotation
.end field

.field private realFirebaseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/common/RealFirebase;",
            ">;"
        }
    .end annotation
.end field

.field private realHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/RealHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private realInternetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/RealInternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private realLocalNotificationAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private realLocalSettingFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/RealLocalSettingFactory;",
            ">;"
        }
    .end annotation
.end field

.field private realLocaleChangedNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/RealLocaleChangedNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private realOnboardingActivityStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter;",
            ">;"
        }
    .end annotation
.end field

.field private realOnlineStoreAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private realOnlineStoreRestrictionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field

.field private realPhoneNumberHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/phone/number/RealPhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private realPlayServicesVersionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/RealPlayServicesVersions;",
            ">;"
        }
    .end annotation
.end field

.field private realPosBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/android/util/RealPosBuild;",
            ">;"
        }
    .end annotation
.end field

.field private realPushMessageDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private realRemoteMessagesStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/communications/service/RealRemoteMessagesStore;",
            ">;"
        }
    .end annotation
.end field

.field private realSquareHeadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/RealSquareHeaders;",
            ">;"
        }
    .end annotation
.end field

.field private realToastFactoryProvider:Ljavax/inject/Provider;

.field private realUserSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/RealUserSettingsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private redundantStoredPaymentsQueueFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
            ">;"
        }
    .end annotation
.end field

.field private registerHttpInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/RegisterHttpInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private registerVersionCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private registerVersionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rx1FileSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx1FileScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private rx2FileSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx2FileScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private serverExperimentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ServerExperiments;",
            ">;"
        }
    .end annotation
.end field

.field private setOfInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lokhttp3/Interceptor;",
            ">;>;"
        }
    .end annotation
.end field

.field private sharedPreferencesBundleStoreProvider:Ljavax/inject/Provider;

.field private showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private speleoIdGeneratorProvider:Ljavax/inject/Provider;

.field private squareCardUpsellExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private starterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueService$Starter;",
            ">;"
        }
    .end annotation
.end field

.field private statusBarFirmwareUpdateNotificationServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private storeAndForwardJobCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
            ">;"
        }
    .end annotation
.end field

.field private storedCardReadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;"
        }
    .end annotation
.end field

.field private swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;"
        }
    .end annotation
.end field

.field private telephonyProvider:Ljavax/inject/Provider;

.field private timeInfoChangedMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/TimeInfoChangedMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private tmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field

.field private transactionLedgerUploaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerUploader;",
            ">;"
        }
    .end annotation
.end field

.field private urlRedirectInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/UrlRedirectInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private useFeatureFlagCatalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private userAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/useragent/UserAgentProvider;",
            ">;"
        }
    .end annotation
.end field

.field private validatedLocationCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/ValidatedLocationCacheProvider;",
            ">;"
        }
    .end annotation
.end field

.field private worldLandingScreenSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V
    .locals 0

    .line 6480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6481
    iput-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    .line 6482
    iput-object p7, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    .line 6483
    iput-object p8, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    .line 6484
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    .line 6485
    iput-object p11, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    .line 6486
    iput-object p4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

    .line 6487
    invoke-direct/range {p0 .. p11}, Lcom/squareup/DaggerSposReleaseAppComponent;->initialize(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V

    .line 6488
    invoke-direct/range {p0 .. p11}, Lcom/squareup/DaggerSposReleaseAppComponent;->initialize2(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V

    .line 6489
    invoke-direct/range {p0 .. p11}, Lcom/squareup/DaggerSposReleaseAppComponent;->initialize3(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V

    .line 6490
    invoke-direct/range {p0 .. p11}, Lcom/squareup/DaggerSposReleaseAppComponent;->initialize4(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V

    .line 6491
    invoke-direct/range {p0 .. p11}, Lcom/squareup/DaggerSposReleaseAppComponent;->initialize5(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 5635
    invoke-direct/range {p0 .. p11}, Lcom/squareup/DaggerSposReleaseAppComponent;-><init>(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePackageManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderOracleProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNfcReaderHasConnectedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasConnectedToR6Provider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideR12HasConnectedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerEventLoggerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSecureSessionServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCountryCodeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoreAndForwardBillServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$10900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoredPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSafetyNetServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePicassoMemoryCacheProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCacheDirectoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEnsureThumborProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->autoVoidStatusBarNotifierProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJobNotificationManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilethreadEnforcerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$11900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLastQueueServiceStartProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileThreadSchedulerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApiUrlSelectorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->swipeBusProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->notificationWrapperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNotificationManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMessagesServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPushMessageDelegateProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realUserSettingsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePushServiceEnabledProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$12900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePushServiceRegistrationProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->clientSettingsCacheProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRegisterPushNotificationServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceAvailabilityProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMerchantProfileServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->addAppNameFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideActivityListenerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->bleConnectorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonRestAdapterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realLocaleChangedNotifierProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$13900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderFactoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$140800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePaperSignatureBatchServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPhoneNumberHelperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceNameSettingProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBillListServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$144400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->autoCaptureStatusBarNotifierProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWeeblyServiceCreatorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortDateFormatProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortDateFormatNoYearProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMediumDateFormatNoYearProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$14900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLongDateFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$15000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDiscountPercentageFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$15100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$15200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortMoneyFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$15300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCentsMoneyFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$15400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/lang/String;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getMaybeUserTokenString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$15800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePaymentServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$15900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideImageServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$16000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReportEmailServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$16100(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/checkoutflow/installments/InstallmentsService;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealServiceInstallmentsService()Lcom/squareup/checkoutflow/installments/InstallmentsService;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$16200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->pendingPreferencesCacheProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$16300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/checkoutflow/receipt/ReceiptService;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealServiceReceiptService()Lcom/squareup/checkoutflow/receipt/ReceiptService;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$16500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLinkDebitCardServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$165700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$165800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$166100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScannerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$166200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$166300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$16900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTourEducationItemsSeenProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$17000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTourEducationItemsSeenVerticalProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$17300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBankAccountServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$17400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMultipassServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$17700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRewardsBalanceServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$17800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideActivationServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$17900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appsFlyerAdAnalyticsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$18100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->loggedOutFeatureStarterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$18200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAddressServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$18300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDepositScheduleServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->useFeatureFlagCatalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusStoreAndForwardEnabledSettingProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$20100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realToastFactoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$201300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getMediumFormNoYearDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$201700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideIposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$20200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReferralServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$20400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$20500(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getShowMultipleRewardsCopyExperiment()Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAppDelegateProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$21100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShippingAddressServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$21300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/scrubber/PhoneNumberScrubber;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getPhoneNumberScrubber()Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$22100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSpeedTestServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$221300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCatalogServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$22600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGiftCardServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$226100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOrdersServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$226900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->authenticatedSquareDownloadManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$2300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoLocationMonitorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$23300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getShortFormDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$23400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getTimeFormatDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$23500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInvoiceFileAttachmentServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$23600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getMediumFormDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$236900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTimeZoneProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$23700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLongFormDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$23800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/communications/RealMessageResolver;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealMessageResolver()Lcom/squareup/communications/RealMessageResolver;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideViewHierarchyBuilderProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$24200(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealOnlineStoreAnalytics()Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$24300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$24400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRealOnlineStoreRestrictions$impl_wiring_releaseProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$24700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$2500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$25100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realHudToasterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$25500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/lang/Object;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLegacyAccountStatusProvider()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$258900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTimecardsServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$25900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$259700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getMediumDateTimeDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$259800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDisputesServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$259900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->percentageChangeFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMinesweeperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$26000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRemoteCardReaderProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$260000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFormatterOfNumber()Lcom/squareup/text/Formatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260100(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCompactNumberFormatterOfNumber()Lcom/squareup/text/Formatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260200(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/money/AccountingFormatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAccountingFormatter()Lcom/squareup/money/AccountingFormatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCompactAccountingFormatFormatterOfMoney()Lcom/squareup/text/Formatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260400(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCompactShorterAccountingFormatFormatterOfMoney()Lcom/squareup/text/Formatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260500(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrent24HourClockMode;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealCurrent24HourClockMode()Lcom/squareup/time/RealCurrent24HourClockMode;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/RealLocalSettingFactory;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealLocalSettingFactory()Lcom/squareup/settings/RealLocalSettingFactory;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$260900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getSingleDigitDecimalNumberFormatterFormatterOfNumber()Lcom/squareup/text/Formatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$261000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realLocalSettingFactoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$261100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNumberFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$261200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeZoneProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$261400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceNameProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$26300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerSessionIdsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$263300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getShortFormNoYearDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$26400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialAudioThreadExecutorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$264400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortDateTimeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$26500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioThreadEnforcerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$26600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicaServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$266900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealPostInstallEncryptedEmail()Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$26700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->tmnTimingsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$267000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLocalSettingTrustedDeviceDetailsStore()Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$267100(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAuthenticationServiceEndpoint()Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$267200(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/location/AndroidGeoCountryCodeGuesser;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAndroidGeoCountryCodeGuesser()Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$267300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealSafetyNetRecaptchaVerifier()Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$267400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$267500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->posLearnMoreTourPagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$267600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDefaultToLoginProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$267700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->worldLandingScreenSelectorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$267800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$268500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDebuggableAccountStatusServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$268600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticationServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$268700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoCountryCodeGuesserProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$268800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesAccountManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$268900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDefaultSupportedCountriesProvider()Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realInternetStatusMonitorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$27200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransferReportsServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$27300(Lcom/squareup/DaggerSposReleaseAppComponent;)Landroid/content/res/Resources;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$27400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCapitalFlexLoanServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$27500(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/RealUserSettingsProvider;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealUserSettingsProvider()Lcom/squareup/settings/server/RealUserSettingsProvider;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$27700(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDeviceNamePIILocalSettingOfString()Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$28500(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/util/DefaultIntentAvailabilityManager;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDefaultIntentAvailabilityManager()Lcom/squareup/util/DefaultIntentAvailabilityManager;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$28900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getTourEducationItemsSeenVerticalPreferenceOfSetOfString()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$29100(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFirstInvoiceTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$29200(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLoyaltyEnrollTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$29300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLoyaltyAdjustPointsTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$29400(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLoyaltyRedeemRewardsTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$29500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/lang/String;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getPosSdkVersionNameString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$29600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCouponsServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$29900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRequestedPermissionsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealDevice()Lcom/squareup/utilities/ui/RealDevice;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealBizbankServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$30400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->squareCardUpsellExperimentProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$30700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOnboardingServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$30800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realOnboardingActivityStarterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->activityResultHandlerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$31500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealNfcUtilsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$31600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$31700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamV2Provider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$31800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLatestApiSequenceUuidProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realAndroidConfigurationChangeMonitorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$32100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->storedCardReadersProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/loggedout/LoggedOutFeatureStarter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLoggedOutFeatureStarter()Lcom/squareup/loggedout/LoggedOutFeatureStarter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$33500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$33600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderHubScoperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/lang/String;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getMaybeUserIdString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$34300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideinstallmentsServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$34400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realOnlineStoreAnalyticsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$34700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/io/File;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDataFile()Ljava/io/File;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$35000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesFirstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$35100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesLoyaltyEnrollTutorialViewedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$35200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesLoyaltyAdjustPointsTutorialViewedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$35300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesLoyaltyRedeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$35400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNewInvoiceFeaturesTutorialTipsDismissedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$35500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePrecogServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$35600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMessagehubServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->paymentIncompleteStatusBarNotifierProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDailyLocalSettingProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideR12FirstTimeTutorialViewedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideR6FirstTimeVideoViewedProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAccessibilityManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDamagedReaderServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$36700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReportCoredumpServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$37300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$37400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realLocalNotificationAnalyticsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$37500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOrderHubServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$37800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realRemoteMessagesStoreProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lio/reactivex/Scheduler;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFileThreadScheduler()Lio/reactivex/Scheduler;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$38500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->dateAndTimeFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$38600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateInitializerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$3900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getUseFeatureFlagCatalogIntegrationController()Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$39100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeferredDeepLinkProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$39400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->branchHelperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$39500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReceiptServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$39800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->defaultIntentAvailabilityManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$39900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesGooglePayClientProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$4000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAccountStatusStoreAndForwardEnabledSetting()Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$40400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrossSessionStoreAndForwardTasksQueueProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$40500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstantDepositsServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$40600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealTransfersServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$40700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealBalanceActivityServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$40800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->positiveNegativeFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$40900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInventoryServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$4100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$41900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJediServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$4200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLoyaltyServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$42000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTelephonyManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$4400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/util/Locale;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLocale()Ljava/util/Locale;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$4500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideClientInvoiceServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$4600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/receiving/StandardReceiver;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getStandardReceiver()Lcom/squareup/receiving/StandardReceiver;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$4700(Lcom/squareup/DaggerSposReleaseAppComponent;)Landroid/app/NotificationManager;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$47400(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/balance/core/RenderSignatureScheduler;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRenderSignatureScheduler()Lcom/squareup/balance/core/RenderSignatureScheduler;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$4800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/notification/NotificationWrapper;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationWrapper()Lcom/squareup/notification/NotificationWrapper;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$4900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/util/AddAppNameFormatter;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAddAppNameFormatter()Lcom/squareup/util/AddAppNameFormatter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$5100(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/phone/number/RealPhoneNumberHelper;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealPhoneNumberHelper()Lcom/squareup/text/phone/number/RealPhoneNumberHelper;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePercentageFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$52900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getHeadsetConnectionListener()Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTaxPercentageFormatterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$53100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBillRefundServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$5400(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/CountryCode;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/lang/String;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getMaybeMerchantTokenString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceIdProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$5700(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTimeZone;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealCurrentTimeZone()Lcom/squareup/time/RealCurrentTimeZone;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$5800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCustomReportServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealCurrentTime()Lcom/squareup/time/RealCurrentTime;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/AppBootstrapModule;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    return-object p0
.end method

.method static synthetic access$6000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$60700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideVibratorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideQueueConverterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserIdProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilesDirectoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCatalogConnectV2ServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$67400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLongDateFormatNoYearProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$6900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->defaultPersistentFactoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$69800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDialogueServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$70400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;
    .locals 0

    .line 5635
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLongFormNoYearDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$7100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEmployeesServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realDeviceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFactoryProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->usbDiscovererProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$7900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUsbManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesUUIDGeneratorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePicassoProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCashDrawerExecutorProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionCodeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRolodexServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBackgroundJobManagerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRetrofitClientProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$8900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExecutorServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserTokenProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMediumDateFormatProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTimeFormatProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCashManagementServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBillCreationServiceProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSessionIdProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realSquareHeadersProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$9900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;
    .locals 0

    .line 5635
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static builder()Lcom/squareup/DaggerSposReleaseAppComponent$Builder;
    .locals 2

    .line 6495
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$Builder;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method private getAccountStatusSettings()Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 8

    .line 6678
    new-instance v7, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getUseFeatureFlagCatalogIntegrationController()Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;

    move-result-object v5

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAccountStatusStoreAndForwardEnabledSetting()Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/settings/server/AccountStatusSettings;-><init>(Ldagger/Lazy;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/payment/StoreAndForwardEnabledSetting;)V

    return-object v7
.end method

.method private getAccountStatusStoreAndForwardEnabledSetting()Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;
    .locals 2

    .line 6609
    new-instance v0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    invoke-direct {v0, v1}, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method private getAccountingFormatter()Lcom/squareup/money/AccountingFormatter;
    .locals 3

    .line 6735
    new-instance v0, Lcom/squareup/money/AccountingFormatter;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2}, Lcom/squareup/money/AccountingFormatter;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getAddAppNameFormatter()Lcom/squareup/util/AddAppNameFormatter;
    .locals 2

    .line 6663
    new-instance v0, Lcom/squareup/util/AddAppNameFormatter;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-direct {v0, v1}, Lcom/squareup/util/AddAppNameFormatter;-><init>(Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getAllTrustedDeviceDetailsLocalSettingOfLinkedHashMapOfStringAndTrustedDeviceDetails()Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;"
        }
    .end annotation

    .line 6760
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->deviceSettingsModule:Lcom/squareup/settings/DeviceSettingsModule;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->provideGson()Lcom/google/gson/Gson;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceDetailsFactory;->provideDeviceDetails(Lcom/squareup/settings/DeviceSettingsModule;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getAnalyticsSwipeEventLogger()Lcom/squareup/swipe/AnalyticsSwipeEventLogger;
    .locals 2

    .line 6505
    new-instance v0, Lcom/squareup/swipe/AnalyticsSwipeEventLogger;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    invoke-direct {v0, v1}, Lcom/squareup/swipe/AnalyticsSwipeEventLogger;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method private getAndroidGeoCountryCodeGuesser()Lcom/squareup/location/AndroidGeoCountryCodeGuesser;
    .locals 5

    .line 6772
    new-instance v0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoAddressProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/core/location/providers/AddressProvider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationProvider:Ljavax/inject/Provider;

    invoke-virtual {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->telephoneManager()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;-><init>(Lcom/squareup/core/location/providers/AddressProvider;Lcom/squareup/util/Clock;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;)V

    return-object v0
.end method

.method private getAndroidHeadsetConnectionListener()Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;
    .locals 2

    .line 6687
    new-instance v0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method private getAnrChaperone()Lcom/squareup/anrchaperone/AnrChaperone;
    .locals 4

    .line 6553
    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperone;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->anrChaperoneModule:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    invoke-static {v1}, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;->provideChaperoneThread(Lcom/squareup/anrchaperone/AnrChaperoneModule;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v1

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v3}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/anrchaperone/AnrChaperone;-><init>(Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/util/Clock;Landroid/app/Application;)V

    return-object v0
.end method

.method private getAuthenticationServiceEndpoint()Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
    .locals 1

    .line 6769
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getImplementation()Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;->provideAuthenticationServiceEndpoint(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-result-object v0

    return-object v0
.end method

.method private getBluetoothAclConnectionReceiver()Lcom/squareup/logging/BluetoothAclConnectionReceiver;
    .locals 4

    .line 6586
    new-instance v0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getBluetoothDevicesCountInitializer()Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v3}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/logging/BluetoothAclConnectionReceiver;-><init>(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/logging/BluetoothDevicesCountInitializer;Landroid/app/Application;)V

    return-object v0
.end method

.method private getBluetoothDevicesCountInitializer()Lcom/squareup/logging/BluetoothDevicesCountInitializer;
    .locals 5

    .line 6583
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->prod2:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdapterProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/BluetoothUtils;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideBluetoothDevicesCountInitializerFactory;->provideBluetoothDevicesCountInitializer(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    move-result-object v0

    return-object v0
.end method

.method private getBluetoothManager()Landroid/bluetooth/BluetoothManager;
    .locals 1

    .line 6502
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;->providesBluetoothManager(Landroid/app/Application;)Landroid/bluetooth/BluetoothManager;

    move-result-object v0

    return-object v0
.end method

.method private getBugsnagAdditionalMetadataLogger()Lcom/squareup/log/BugsnagAdditionalMetadataLogger;
    .locals 1

    .line 6541
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {v0}, Lcom/squareup/log/BugsnagAdditionalMetadataLogger_Factory;->newInstance(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/log/BugsnagAdditionalMetadataLogger;

    move-result-object v0

    return-object v0
.end method

.method private getCommunicationsService()Lcom/squareup/communications/service/CommunicationsService;
    .locals 1

    .line 6657
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/communications/CommunicationsCommonModule_ProvideRealCommunicationsServiceFactory;->provideRealCommunicationsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/communications/service/CommunicationsService;

    move-result-object v0

    return-object v0
.end method

.method private getCompactAccountingFormatFormatterOfMoney()Lcom/squareup/text/Formatter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 6738
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCompactMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyModule_ProvideCompactAccountingFormatterFactory;->provideCompactAccountingFormatter(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method private getCompactNumberFormatterOfNumber()Lcom/squareup/text/Formatter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 6732
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->provideCompactNumberFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method private getCompactShorterAccountingFormatFormatterOfMoney()Lcom/squareup/text/Formatter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 6741
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCompactShorterMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyModule_ProvideCompactShorterAccountingFormatterFactory;->provideCompactShorterAccountingFormatter(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method private getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 6624
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {v0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideCountryCodeFactory;->provideCountryCode(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/CountryCode;

    move-result-object v0

    return-object v0
.end method

.method private getDataFile()Ljava/io/File;
    .locals 1

    .line 6523
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/AndroidModule_ProvideFilesDirectoryFactory;->provideFilesDirectory(Landroid/app/Application;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private getDefaultIntentAvailabilityManager()Lcom/squareup/util/DefaultIntentAvailabilityManager;
    .locals 2

    .line 6696
    new-instance v0, Lcom/squareup/util/DefaultIntentAvailabilityManager;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/util/DefaultIntentAvailabilityManager;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method private getDefaultSupportedCountriesProvider()Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;
    .locals 2

    .line 6787
    new-instance v0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;-><init>(Landroid/content/res/Resources;)V

    return-object v0
.end method

.method private getDeviceNamePIILocalSettingOfString()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 6684
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameSettingFactory;->provideDeviceNameSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceSettingsInitializedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 6556
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceInitializedFactory;->provideDeviceInitialized(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceSettingsSettingsInitializer()Lcom/squareup/settings/DeviceSettingsSettingsInitializer;
    .locals 3

    .line 6559
    new-instance v0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDeviceSettingsInitializedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;-><init>(Lcom/squareup/settings/LocalSetting;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method private getEventSinkSessionExpiredHandler()Lcom/squareup/receiving/EventSinkSessionExpiredHandler;
    .locals 2

    .line 6639
    new-instance v0, Lcom/squareup/receiving/EventSinkSessionExpiredHandler;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/badbus/BadEventSink;

    invoke-direct {v0, v1}, Lcom/squareup/receiving/EventSinkSessionExpiredHandler;-><init>(Lcom/squareup/badbus/BadEventSink;)V

    return-object v0
.end method

.method private getEventStreamCommonCrashLogger()Lcom/squareup/log/EventStreamCommonCrashLogger;
    .locals 1

    .line 6544
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/log/EventStreamCommonCrashLogger_Factory;->newInstance(Lcom/squareup/analytics/Analytics;)Lcom/squareup/log/EventStreamCommonCrashLogger;

    move-result-object v0

    return-object v0
.end method

.method private getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;
    .locals 1

    .line 6520
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeaturesFactory()Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;->provideFeatureFlagFeatures(Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    return-object v0
.end method

.method private getFeatureFlagFeaturesFactory()Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;
    .locals 4

    .line 6517
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealDevice()Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-virtual {v3}, Lcom/squareup/AppBootstrapModule;->provideEmailSupportLedgerEnabled()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/settings/server/FeatureFlagFeatures_Factory_Factory;->newInstance(Ljavax/inject/Provider;Ldagger/Lazy;Lcom/squareup/util/Device;Z)Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;

    move-result-object v0

    return-object v0
.end method

.method private getFeatureFlagsForLogs()Lcom/squareup/log/FeatureFlagsForLogs;
    .locals 3

    .line 6550
    new-instance v0, Lcom/squareup/log/FeatureFlagsForLogs;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserIdProvider:Ljavax/inject/Provider;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/log/FeatureFlagsForLogs;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method private getFileThreadScheduler()Lio/reactivex/Scheduler;
    .locals 1

    .line 6508
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->rx2FileSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/Rx2FileScheduler;

    invoke-static {v0}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;->provideFileThreadScheduler(Lcom/squareup/thread/Rx2FileScheduler;)Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method private getFirstInvoiceTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 6708
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesFirstInvoiceTutorialViewedFactory;->providesFirstInvoiceTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getForAppSetOfScoped()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    const/16 v0, 0xd

    .line 6589
    invoke-static {v0}, Ldagger/internal/SetBuilder;->newSetBuilder(I)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getProvideForAppScopedAsSet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->addAll(Ljava/util/Collection;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getProvidePushServicesAsSetForLoggedIn()Lmortar/Scoped;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->locationAnalyticsUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    new-instance v1, Lcom/squareup/noho/NightModeManager;

    invoke-direct {v1}, Lcom/squareup/noho/NightModeManager;-><init>()V

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUsbBarcodeScannersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getUsbAttachedActivityEnabler()Lcom/squareup/hardware/UsbAttachedActivityEnabler;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getScopedHeadset()Lcom/squareup/wavpool/swipe/ScopedHeadset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getScopedBluetoothReceivers()Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getBluetoothAclConnectionReceiver()Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appsFlyerAdAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->adIdGathererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ldagger/internal/SetBuilder;->build()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private getFormatterOfNumber()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 6729
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideNumberFormatterFactory;->provideNumberFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method private getHeadsetConnectionListener()Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;
    .locals 2

    .line 6690
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAndroidHeadsetConnectionListener()Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->provideHeadsetConnectionListener(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    move-result-object v0

    return-object v0
.end method

.method private getImplementation()Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;
    .locals 5

    .line 6766
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/AuthenticationService;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePasswordServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/PasswordService;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;-><init>(Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/server/account/PasswordService;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getInstallationIdGenerator()Lcom/squareup/settings/InstallationIdGenerator;
    .locals 1

    .line 6778
    invoke-static {}, Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;->provideUnique()Lcom/squareup/util/Unique;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/settings/InstallationIdGenerator_Factory;->newInstance(Lcom/squareup/util/Unique;)Lcom/squareup/settings/InstallationIdGenerator;

    move-result-object v0

    return-object v0
.end method

.method private getInstallationIdString()Ljava/lang/String;
    .locals 2

    .line 6781
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getInstallationIdGenerator()Lcom/squareup/settings/InstallationIdGenerator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->provideInstallationId(Landroid/content/SharedPreferences;Lcom/squareup/settings/InstallationIdGenerator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLastQueueServiceStartLocalSettingOfLong()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 6514
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/queue/QueueRootModule_ProvideLastQueueServiceStartFactory;->provideLastQueueServiceStart(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getLegacyAccountStatusProvider()Ljava/lang/Object;
    .locals 3

    .line 6669
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/SessionIdPIIProvider;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/account/LoggedInStatusProvider;

    invoke-static {v0, v1, v2}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->newInstance(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/account/LoggedInStatusProvider;)Lcom/squareup/accountstatus/LegacyAccountStatusProvider;

    move-result-object v0

    return-object v0
.end method

.method private getLocalSettingTrustedDeviceDetailsStore()Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;
    .locals 2

    .line 6763
    new-instance v0, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAllTrustedDeviceDetailsLocalSettingOfLinkedHashMapOfStringAndTrustedDeviceDetails()Lcom/squareup/settings/LocalSetting;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;-><init>(Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method

.method private getLocale()Ljava/util/Locale;
    .locals 1

    .line 6630
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideLocaleFactory;->provideLocale(Landroid/app/Application;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method private getLoggedOutFeatureStarter()Lcom/squareup/loggedout/LoggedOutFeatureStarter;
    .locals 1

    .line 6598
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-static {v0}, Lcom/squareup/loggedout/LoggedOutFeatureStarter_Factory;->newInstance(Lcom/squareup/account/LegacyAuthenticator;)Lcom/squareup/loggedout/LoggedOutFeatureStarter;

    move-result-object v0

    return-object v0
.end method

.method private getLoggedOutTaskWatcher()Ljava/lang/Object;
    .locals 7

    .line 6511
    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/OhSnapLogger;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->provideGson()Lcom/google/gson/Gson;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFileThreadScheduler()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskWatcherFactory;->provideLoggedOutTaskWatcher(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Landroid/content/SharedPreferences;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)Lcom/squareup/queue/TaskWatcher;

    move-result-object v0

    return-object v0
.end method

.method private getLongFormDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6654
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;->provideLongDateFormatter(Landroid/app/Application;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getLongFormNoYearDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6693
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideLongDateFormatNoYearFactory;->provideLongDateFormatNoYear(Ljava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getLoyaltyAdjustPointsTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 6714
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyAdjustPointsTutorialViewedFactory;->providesLoyaltyAdjustPointsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getLoyaltyEnrollTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 6711
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyEnrollTutorialViewedFactory;->providesLoyaltyEnrollTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getLoyaltyRedeemRewardsTutorialViewedLocalSettingOfBoolean()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 6717
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;->providesLoyaltyRedeemRewardsTutorialViewed(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method private getMaybeMerchantTokenString()Ljava/lang/String;
    .locals 1

    .line 6702
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {v0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;->provideMerchantToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMaybeUserIdString()Ljava/lang/String;
    .locals 1

    .line 6601
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {v0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideUserIdFactory;->provideUserId(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMaybeUserTokenString()Ljava/lang/String;
    .locals 1

    .line 6612
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {v0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideUserTokenFactory;->provideUserToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMediumDateTimeDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6726
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;->provideMediumDateTime(Ljava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getMediumFormDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6651
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideMediumDateFormatFactory;->provideMediumDateFormat(Landroid/app/Application;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getMediumFormNoYearDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6699
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideMediumDateFormatNoYearFactory;->provideMediumDateFormatNoYear(Ljava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getNotificationManager()Landroid/app/NotificationManager;
    .locals 1

    .line 6526
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideNotificationManagerFactory;->provideNotificationManager(Landroid/app/Application;)Landroid/app/NotificationManager;

    move-result-object v0

    return-object v0
.end method

.method private getNotificationWrapper()Lcom/squareup/notification/NotificationWrapper;
    .locals 2

    .line 6535
    new-instance v0, Lcom/squareup/notification/NotificationWrapper;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-direct {v0, v1}, Lcom/squareup/notification/NotificationWrapper;-><init>(Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getPackageManager()Landroid/content/pm/PackageManager;
    .locals 1

    .line 6571
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/AndroidModule_ProvidePackageManagerFactory;->providePackageManager(Landroid/app/Application;)Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method private getPhoneNumberScrubber()Lcom/squareup/text/scrubber/PhoneNumberScrubber;
    .locals 2

    .line 6627
    new-instance v0, Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/text/scrubber/PhoneNumberScrubber;-><init>(Lcom/squareup/CountryCode;)V

    return-object v0
.end method

.method private getPosSdkVersionNameString()Ljava/lang/String;
    .locals 1

    .line 6723
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealPosBuild()Lcom/squareup/android/util/RealPosBuild;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/PosBuildModule_RegisterVersionNameFactory;->registerVersionName(Lcom/squareup/util/PosBuild;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getProvideForAppScopedAsSet()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 6565
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->backgroundJobLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/log/BackgroundJobLogger;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/MainThreadBlockedLogger;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->queueJobCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/QueueJobCreator;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->storeAndForwardJobCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->provideForAppScopedAsSet(Lcom/squareup/backgroundjob/log/BackgroundJobLogger;Lcom/squareup/log/MainThreadBlockedLogger;Lcom/squareup/queue/QueueJobCreator;Lcom/squareup/payment/offline/StoreAndForwardJobCreator;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private getProvidePushServicesAsSetForLoggedIn()Lmortar/Scoped;
    .locals 1

    .line 6568
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPushMessageDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pushmessages/RealPushMessageDelegate;

    invoke-static {v0}, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;->providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;

    move-result-object v0

    return-object v0
.end method

.method private getRealCurrent24HourClockMode()Lcom/squareup/time/RealCurrent24HourClockMode;
    .locals 2

    .line 6744
    new-instance v0, Lcom/squareup/time/RealCurrent24HourClockMode;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->timeInfoChangedMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/time/TimeInfoChangedMonitor;

    invoke-direct {v0, v1}, Lcom/squareup/time/RealCurrent24HourClockMode;-><init>(Lcom/squareup/time/TimeInfoChangedMonitor;)V

    return-object v0
.end method

.method private getRealCurrentTime()Lcom/squareup/time/RealCurrentTime;
    .locals 2

    .line 6636
    new-instance v0, Lcom/squareup/time/RealCurrentTime;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealCurrentTimeZone()Lcom/squareup/time/RealCurrentTimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/time/RealCurrentTime;-><init>(Lcom/squareup/time/CurrentTimeZone;)V

    return-object v0
.end method

.method private getRealCurrentTimeZone()Lcom/squareup/time/RealCurrentTimeZone;
    .locals 2

    .line 6633
    new-instance v0, Lcom/squareup/time/RealCurrentTimeZone;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->timeInfoChangedMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/time/TimeInfoChangedMonitor;

    invoke-direct {v0, v1}, Lcom/squareup/time/RealCurrentTimeZone;-><init>(Lcom/squareup/time/TimeInfoChangedMonitor;)V

    return-object v0
.end method

.method private getRealDevice()Lcom/squareup/utilities/ui/RealDevice;
    .locals 3

    .line 6499
    new-instance v0, Lcom/squareup/utilities/ui/RealDevice;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realAndroidConfigurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-direct {v0, v1, v2}, Lcom/squareup/utilities/ui/RealDevice;-><init>(Landroid/app/Application;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    return-object v0
.end method

.method private getRealLocalSettingFactory()Lcom/squareup/settings/RealLocalSettingFactory;
    .locals 2

    .line 6747
    new-instance v0, Lcom/squareup/settings/RealLocalSettingFactory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    invoke-direct {v0, v1}, Lcom/squareup/settings/RealLocalSettingFactory;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method private getRealMessageResolver()Lcom/squareup/communications/RealMessageResolver;
    .locals 2

    .line 6660
    new-instance v0, Lcom/squareup/communications/RealMessageResolver;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCommunicationsService()Lcom/squareup/communications/service/CommunicationsService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/communications/RealMessageResolver;-><init>(Lcom/squareup/communications/service/CommunicationsService;)V

    return-object v0
.end method

.method private getRealOnlineStoreAnalytics()Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;
    .locals 2

    .line 6595
    new-instance v0, Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method private getRealPhoneNumberHelper()Lcom/squareup/text/phone/number/RealPhoneNumberHelper;
    .locals 2

    .line 6666
    new-instance v0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper;-><init>(Lcom/squareup/CountryCode;)V

    return-object v0
.end method

.method private getRealPlayServicesVersions()Lcom/squareup/firebase/versions/RealPlayServicesVersions;
    .locals 2

    .line 6592
    new-instance v0, Lcom/squareup/firebase/versions/RealPlayServicesVersions;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/firebase/versions/RealPlayServicesVersions;-><init>(Landroid/content/pm/PackageManager;)V

    return-object v0
.end method

.method private getRealPosBuild()Lcom/squareup/android/util/RealPosBuild;
    .locals 2

    .line 6720
    new-instance v0, Lcom/squareup/android/util/RealPosBuild;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/android/util/RealPosBuild;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method private getRealPostInstallEncryptedEmail()Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;
    .locals 5

    .line 6756
    new-instance v0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePublicStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/PublicApiService;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDefaultToLoginProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/LocalSetting;

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/server/PublicApiService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method private getRealSafetyNetRecaptchaVerifier()Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;
    .locals 7

    .line 6784
    new-instance v6, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    invoke-static {}, Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;->provideGoogleApiAvailability()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getSafetyNetClient()Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getInstallationIdString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;-><init>(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-object v6
.end method

.method private getRealServiceInstallmentsService()Lcom/squareup/checkoutflow/installments/InstallmentsService;
    .locals 1

    .line 6615
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsServiceCommonModule_ProvideinstallmentsServiceFactory;->provideinstallmentsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/checkoutflow/installments/InstallmentsService;

    move-result-object v0

    return-object v0
.end method

.method private getRealServiceReceiptService()Lcom/squareup/checkoutflow/receipt/ReceiptService;
    .locals 1

    .line 6618
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptServiceCommonModule_ProvideReceiptServiceFactory;->provideReceiptService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/checkoutflow/receipt/ReceiptService;

    move-result-object v0

    return-object v0
.end method

.method private getRealUserSettingsProvider()Lcom/squareup/settings/server/RealUserSettingsProvider;
    .locals 3

    .line 6681
    new-instance v0, Lcom/squareup/settings/server/RealUserSettingsProvider;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/LoggedInStatusProvider;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAccountStatusSettings()Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/settings/server/RealUserSettingsProvider;-><init>(Lcom/squareup/account/LoggedInStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v0
.end method

.method private getRecorderErrorReporterListener()Lcom/squareup/swipe/RecorderErrorReporterListener;
    .locals 3

    .line 6562
    invoke-virtual {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->telephoneManager()Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/wavpool/swipe/Headset;

    invoke-static {v0, v1, v2}, Lcom/squareup/swipe/RecorderErrorReporterListener_Factory;->newInstance(Landroid/telephony/TelephonyManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/swipe/RecorderErrorReporterListener;

    move-result-object v0

    return-object v0
.end method

.method private getRenderSignatureScheduler()Lcom/squareup/balance/core/RenderSignatureScheduler;
    .locals 1

    .line 6672
    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;->provideComputationScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/main/PosFeaturesModule_Real_ProvideRenderSignatureProviderFactory;->provideRenderSignatureProvider(Lio/reactivex/Scheduler;)Lcom/squareup/balance/core/RenderSignatureScheduler;

    move-result-object v0

    return-object v0
.end method

.method private getResources()Landroid/content/res/Resources;
    .locals 1

    .line 6675
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideResourcesFactory;->provideResources(Landroid/app/Application;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private getSafetyNetClient()Lcom/google/android/gms/safetynet/SafetyNetClient;
    .locals 1

    .line 6775
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;->getClient(Landroid/app/Application;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    return-object v0
.end method

.method private getScopedBluetoothReceivers()Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;
    .locals 5

    .line 6580
    new-instance v0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;-><init>(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;)V

    return-object v0
.end method

.method private getScopedHeadset()Lcom/squareup/wavpool/swipe/ScopedHeadset;
    .locals 2

    .line 6577
    new-instance v0, Lcom/squareup/wavpool/swipe/ScopedHeadset;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/Headset;

    invoke-direct {v0, v1}, Lcom/squareup/wavpool/swipe/ScopedHeadset;-><init>(Lcom/squareup/wavpool/swipe/Headset;)V

    return-object v0
.end method

.method private getSetOfCrashAdditionalLogger()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    .line 6547
    invoke-static {v0}, Ldagger/internal/SetBuilder;->newSetBuilder(I)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getBugsnagAdditionalMetadataLogger()Lcom/squareup/log/BugsnagAdditionalMetadataLogger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getEventStreamCommonCrashLogger()Lcom/squareup/log/EventStreamCommonCrashLogger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/SetBuilder;->add(Ljava/lang/Object;)Ldagger/internal/SetBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ldagger/internal/SetBuilder;->build()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private getShortFormDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6645
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideShortDateFormatFactory;->provideShortDateFormat(Landroid/app/Application;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getShortFormNoYearDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6753
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideShortDateFormatNoYearFactory;->provideShortDateFormatNoYear(Ljava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getShowMultipleRewardsCopyExperiment()Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;
    .locals 2

    .line 6621
    new-instance v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;-><init>(Ldagger/Lazy;)V

    return-object v0
.end method

.method private getSingleDigitDecimalNumberFormatterFormatterOfNumber()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 6750
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideSingleDigitDecimalNumberFormatterFactory;->provideSingleDigitDecimalNumberFormatter(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method private getStandardReceiver()Lcom/squareup/receiving/StandardReceiver;
    .locals 2

    .line 6642
    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->provideMainThreadEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getEventSinkSessionExpiredHandler()Lcom/squareup/receiving/EventSinkSessionExpiredHandler;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->provideStandardReceiver(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object v0

    return-object v0
.end method

.method private getStoredPaymentStatusBarNotifier()Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;
    .locals 4

    .line 6538
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationWrapper()Lcom/squareup/notification/NotificationWrapper;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/util/Res;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;)Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;

    move-result-object v0

    return-object v0
.end method

.method private getTimeFormatDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 6648
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideTimeFormatFactory;->provideTimeFormat(Landroid/app/Application;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method private getTourEducationItemsSeenVerticalPreferenceOfSetOfString()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 6705
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static {v0}, Lcom/squareup/PosAppComponent_Module_ProvideTourEducationItemsSeenVerticalFactory;->provideTourEducationItemsSeenVertical(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method private getTransactionLedgerManagerFactory()Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;
    .locals 8

    .line 6532
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDataFile()Ljava/io/File;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->provideWireGson()Lcom/google/gson/Gson;

    move-result-object v5

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v6

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getTransactionLedgerUploader()Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->provideFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/google/gson/Gson;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    move-result-object v0

    return-object v0
.end method

.method private getTransactionLedgerUploader()Lcom/squareup/payment/ledger/TransactionLedgerUploader;
    .locals 7

    .line 6529
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/transaction_ledger/TransactionLedgerService;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/http/Server;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lokhttp3/OkHttpClient;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->provideRpcScheduler()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->newInstance(Lcom/squareup/server/transaction_ledger/TransactionLedgerService;Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    move-result-object v0

    return-object v0
.end method

.method private getUsbAttachedActivityEnabler()Lcom/squareup/hardware/UsbAttachedActivityEnabler;
    .locals 4

    .line 6574
    new-instance v0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v3}, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->provideActivityListener(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/ActivityListener;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/hardware/UsbAttachedActivityEnabler;-><init>(Landroid/app/Application;Landroid/content/pm/PackageManager;Lcom/squareup/ActivityListener;)V

    return-object v0
.end method

.method private getUseFeatureFlagCatalogIntegrationController()Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;
    .locals 2

    .line 6605
    new-instance v0, Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;-><init>(Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method private initialize(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V
    .locals 19

    move-object/from16 v0, p0

    .line 6800
    invoke-static {}, Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor_Factory;->create()Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realAndroidConfigurationChangeMonitorProvider:Ljavax/inject/Provider;

    .line 6801
    invoke-static/range {p7 .. p7}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;->create(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 6802
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v2, p7

    invoke-static {v2, v1}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->create(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 6803
    invoke-static/range {p10 .. p10}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;->create(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    .line 6804
    invoke-static/range {p2 .. p2}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    .line 6805
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideTelephonyManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideTelephonyManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTelephonyManagerProvider:Ljavax/inject/Provider;

    .line 6806
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideDevicePreferencesFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    .line 6807
    invoke-static {}, Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;->create()Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/settings/InstallationIdGenerator_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/InstallationIdGenerator_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->installationIdGeneratorProvider:Ljavax/inject/Provider;

    .line 6808
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->installationIdGeneratorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/InstallationIdModule_ProvideInstallationIdFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    .line 6809
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTelephonyManagerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;->create()Lcom/squareup/CommonAppModule_ProvideX2SystemPropertiesFactory;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceIdProviderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceIdProvider:Ljavax/inject/Provider;

    .line 6810
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    .line 6811
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realAccountStatusSettingsApiUrlProvider:Ljavax/inject/Provider;

    .line 6812
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    .line 6813
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideUrlRedirectSettingFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUrlRedirectSettingProvider:Ljavax/inject/Provider;

    .line 6814
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realAccountStatusSettingsApiUrlProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUrlRedirectSettingProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/ProductionServerModule_ProvideServerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ProductionServerModule_ProvideServerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    .line 6815
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideResourcesFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideResourcesFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    .line 6816
    invoke-static/range {p2 .. p2}, Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;->create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideCrashReporterFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashReporterProvider:Ljavax/inject/Provider;

    .line 6817
    invoke-static {}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;->create()Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideViewHierarchyBuilderProvider:Ljavax/inject/Provider;

    .line 6818
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideViewHierarchyBuilderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/CommonAppModule_ProvideViewHierarchyFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideViewHierarchyFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideViewHierarchyProvider:Ljavax/inject/Provider;

    .line 6819
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    .line 6820
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideUserIdFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/AccountStatusModule_ProvideUserIdFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserIdProvider:Ljavax/inject/Provider;

    .line 6821
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserIdProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/FeatureFlagsForLogs_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    .line 6822
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realAndroidConfigurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/utilities/ui/RealDevice_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/utilities/ui/RealDevice_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realDeviceProvider:Ljavax/inject/Provider;

    .line 6823
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentIdProvider:Ljavax/inject/Provider;

    .line 6824
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/RealPosBuild_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/RealPosBuild_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    .line 6825
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/PosBuildModule_RegisterVersionNameFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/PosBuildModule_RegisterVersionNameFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    .line 6826
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/useragent/EnvironmentDiscovery_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/useragent/EnvironmentDiscovery_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->environmentDiscoveryProvider:Ljavax/inject/Provider;

    .line 6827
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideLocaleFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideLocaleFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    .line 6828
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentIdProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/http/useragent/NoReaderSdkBucketBinIdModule_ProvideReaderSdkBucketBinIdFactory;->create()Lcom/squareup/http/useragent/NoReaderSdkBucketBinIdModule_ProvideReaderSdkBucketBinIdFactory;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->environmentDiscoveryProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/http/RealDeviceInformationModule_ProvideDeviceInformationFactory;->create()Lcom/squareup/http/RealDeviceInformationModule_ProvideDeviceInformationFactory;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    move-object/from16 p3, v1

    move-object/from16 p4, v2

    move-object/from16 p5, v2

    move-object/from16 p6, v3

    move-object/from16 p7, v4

    move-object/from16 p8, v5

    move-object/from16 p9, v6

    move-object/from16 p10, v7

    invoke-static/range {p3 .. p10}, Lcom/squareup/http/useragent/UserAgentProvider_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/useragent/UserAgentProvider_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->userAgentProvider:Ljavax/inject/Provider;

    .line 6829
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->userAgentProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentProvider:Ljavax/inject/Provider;

    .line 6830
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/log/AppUpgradeDetector_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/log/AppUpgradeDetector_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->appUpgradeDetectorProvider:Ljavax/inject/Provider;

    .line 6831
    invoke-static/range {p2 .. p2}, Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;->create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideStartUptimeFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStartUptimeProvider:Ljavax/inject/Provider;

    .line 6832
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidSerialFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidSerialFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidSerialProvider:Ljavax/inject/Provider;

    .line 6833
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/util/AndroidModule_ProvidePackageManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvidePackageManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePackageManagerProvider:Ljavax/inject/Provider;

    .line 6834
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePackageManagerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/firebase/versions/RealPlayServicesVersions_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPlayServicesVersionsProvider:Ljavax/inject/Provider;

    .line 6835
    invoke-static {}, Lcom/squareup/analytics/ProcessUniqueId_Factory;->create()Lcom/squareup/analytics/ProcessUniqueId_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->processUniqueIdProvider:Ljavax/inject/Provider;

    .line 6836
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideCountryCodeFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/AccountStatusModule_ProvideCountryCodeFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCountryCodeProvider:Ljavax/inject/Provider;

    .line 6837
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashReporterProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/log/LogModule_ProvideMortarScopeHierarchyFactory;->create()Lcom/squareup/log/LogModule_ProvideMortarScopeHierarchyFactory;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideViewHierarchyProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realDeviceProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->appUpgradeDetectorProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStartUptimeProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidSerialProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPlayServicesVersionsProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->processUniqueIdProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCountryCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v17, v1

    invoke-static/range {v2 .. v17}, Lcom/squareup/log/CrashReportingLogger_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/CrashReportingLogger_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    .line 6838
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    .line 6839
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/analytics/EventStreamModule_Prod_ProvideEventStreamServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/analytics/EventStreamModule_Prod_ProvideEventStreamServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamServiceProvider:Ljavax/inject/Provider;

    .line 6840
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamServiceProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/analytics/Es1BatchUploader_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/analytics/Es1BatchUploader_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->es1BatchUploaderProvider:Ljavax/inject/Provider;

    .line 6841
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideWindowManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideWindowManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWindowManagerProvider:Ljavax/inject/Provider;

    .line 6842
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionCodeProvider:Ljavax/inject/Provider;

    .line 6843
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->es1BatchUploaderProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/server/analytics/EsLogger_Factory;->create()Lcom/squareup/server/analytics/EsLogger_Factory;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->create()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWindowManagerProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realDeviceProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;->create()Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;

    move-result-object v12

    iget-object v13, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionCodeProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->processUniqueIdProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    invoke-static/range {v2 .. v16}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamProvider:Ljavax/inject/Provider;

    .line 6844
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamServiceProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/analytics/Es2BatchUploader_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/analytics/Es2BatchUploader_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->es2BatchUploaderProvider:Ljavax/inject/Provider;

    .line 6845
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->es2BatchUploaderProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/server/analytics/EsLogger_Factory;->create()Lcom/squareup/server/analytics/EsLogger_Factory;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->create()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realDeviceProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;->create()Lcom/squareup/PosAppComponent_Module_ProvideIsReaderSdkFactory;

    move-result-object v11

    iget-object v12, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionNameProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionCodeProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->processUniqueIdProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v15}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamV2Provider:Ljavax/inject/Provider;

    .line 6846
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/CommonAppModule_ProvideOnboardRedirectPathFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideOnboardRedirectPathFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOnboardRedirectPathProvider:Ljavax/inject/Provider;

    .line 6847
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideAdIdCacheFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideAdIdCacheFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdIdCacheProvider:Ljavax/inject/Provider;

    .line 6848
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamV2Provider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOnboardRedirectPathProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdIdCacheProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/analytics/EventStreamAnalytics_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    .line 6849
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/analytics/LoggingHttpProfiler_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->loggingHttpProfilerProvider:Ljavax/inject/Provider;

    .line 6850
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->loggingHttpProfilerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHttpProfilerProvider:Ljavax/inject/Provider;

    .line 6851
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHttpProfilerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->profilingInterceptorProvider:Ljavax/inject/Provider;

    .line 6852
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidIdProvider:Ljavax/inject/Provider;

    .line 6853
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideWifiManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideWifiManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWifiManagerProvider:Ljavax/inject/Provider;

    .line 6854
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWifiManagerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/interceptor/MacAddressProvider_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/MacAddressProvider_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->macAddressProvider:Ljavax/inject/Provider;

    .line 6855
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameSettingFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameSettingFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceNameSettingProvider:Ljavax/inject/Provider;

    .line 6856
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceNameSettingProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDeviceNameFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceNameProvider:Ljavax/inject/Provider;

    .line 6857
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideLocationManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideLocationManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationManagerProvider:Ljavax/inject/Provider;

    .line 6858
    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;->create()Lcom/squareup/thread/MainThreadModule_ProvideAndroidMainThreadFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    .line 6859
    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationComparerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/core/location/CommonLocationModule_ProvideLocationComparerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationComparerProvider:Ljavax/inject/Provider;

    .line 6860
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationManagerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationComparerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v6

    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->create()Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;

    move-result-object v7

    move-object/from16 p3, v1

    move-object/from16 p4, v2

    move-object/from16 p5, v3

    move-object/from16 p6, v4

    move-object/from16 p7, v5

    move-object/from16 p8, v6

    move-object/from16 p9, v7

    invoke-static/range {p3 .. p9}, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoLocationMonitorProvider:Ljavax/inject/Provider;

    .line 6861
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/util/AndroidModule_ProvideFilesDirectoryFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvideFilesDirectoryFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilesDirectoryProvider:Ljavax/inject/Provider;

    .line 6862
    invoke-static {}, Lcom/squareup/thread/FileThreadHolder_Factory;->create()Lcom/squareup/thread/FileThreadHolder_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileThreadHolderProvider:Ljavax/inject/Provider;

    .line 6863
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileThreadHolderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/thread/FileThreadModule_ProvideFileHandlerThreadFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/thread/FileThreadModule_ProvideFileHandlerThreadFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileHandlerThreadProvider:Ljavax/inject/Provider;

    .line 6864
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileHandlerThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideSerialFileThreadExecutorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    .line 6865
    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/persistent/DefaultPersistentFactory_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/persistent/DefaultPersistentFactory_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->defaultPersistentFactoryProvider:Ljavax/inject/Provider;

    .line 6866
    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilesDirectoryProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->defaultPersistentFactoryProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLastBestLocationPersistentProvider:Ljavax/inject/Provider;

    .line 6867
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoLocationMonitorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLastBestLocationPersistentProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationComparerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/location/ValidatedLocationCacheProvider_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/ValidatedLocationCacheProvider_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->validatedLocationCacheProvider:Ljavax/inject/Provider;

    .line 6868
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->validatedLocationCacheProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationProvider:Ljavax/inject/Provider;

    .line 6869
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTelephonyManagerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/interceptor/Telephony_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/Telephony_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->telephonyProvider:Ljavax/inject/Provider;

    .line 6870
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideConnectivityManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectivityManagerProvider:Ljavax/inject/Provider;

    .line 6871
    invoke-static {}, Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;->create()Lcom/squareup/http/interceptor/SpeleoIdGenerator_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->speleoIdGeneratorProvider:Ljavax/inject/Provider;

    .line 6872
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidIdProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidSerialProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->macAddressProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceNameProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserAgentProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTelephonyManagerProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->telephonyProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectivityManagerProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceIdProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOnboardRedirectPathProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdIdCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->speleoIdGeneratorProvider:Ljavax/inject/Provider;

    move-object/from16 v18, v1

    invoke-static/range {v2 .. v18}, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realSquareHeadersProvider:Ljavax/inject/Provider;

    .line 6873
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realSquareHeadersProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->telephonyProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/RegisterHttpInterceptor_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerHttpInterceptorProvider:Ljavax/inject/Provider;

    .line 6874
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/ProductionServerModule_ProvideApiUrlSelectorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ProductionServerModule_ProvideApiUrlSelectorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApiUrlSelectorProvider:Ljavax/inject/Provider;

    .line 6875
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApiUrlSelectorProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUrlRedirectSettingProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->urlRedirectInterceptorProvider:Ljavax/inject/Provider;

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 6876
    invoke-static {v1, v2}, Ldagger/internal/SetFactory;->builder(II)Ldagger/internal/SetFactory$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->profilingInterceptorProvider:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, Ldagger/internal/SetFactory$Builder;->addProvider(Ljavax/inject/Provider;)Ldagger/internal/SetFactory$Builder;

    move-result-object v1

    invoke-static {}, Lcom/squareup/http/interceptor/GzipRequestInterceptor_Factory;->create()Lcom/squareup/http/interceptor/GzipRequestInterceptor_Factory;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldagger/internal/SetFactory$Builder;->addProvider(Ljavax/inject/Provider;)Ldagger/internal/SetFactory$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerHttpInterceptorProvider:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, Ldagger/internal/SetFactory$Builder;->addProvider(Ljavax/inject/Provider;)Ldagger/internal/SetFactory$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->urlRedirectInterceptorProvider:Ljavax/inject/Provider;

    invoke-virtual {v1, v2}, Ldagger/internal/SetFactory$Builder;->addProvider(Ljavax/inject/Provider;)Ldagger/internal/SetFactory$Builder;

    move-result-object v1

    invoke-virtual {v1}, Ldagger/internal/SetFactory$Builder;->build()Ldagger/internal/SetFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->setOfInterceptorProvider:Ljavax/inject/Provider;

    .line 6877
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->setOfInterceptorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientBuilderProvider:Ljavax/inject/Provider;

    .line 6878
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientBuilderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/HttpModule_ProvideUnauthenticatedClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideUnauthenticatedClientFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedClientProvider:Ljavax/inject/Provider;

    .line 6879
    invoke-static {}, Lcom/squareup/badbus/BadBusModule_ProvideBusFactory;->create()Lcom/squareup/badbus/BadBusModule_ProvideBusFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    .line 6880
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/receiving/EventSinkSessionExpiredHandler_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/receiving/EventSinkSessionExpiredHandler_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventSinkSessionExpiredHandlerProvider:Ljavax/inject/Provider;

    .line 6881
    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->create()Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventSinkSessionExpiredHandlerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    .line 6882
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v4

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    move-object/from16 p3, v1

    move-object/from16 p4, v2

    move-object/from16 p5, v3

    move-object/from16 p6, v4

    move-object/from16 p7, v5

    move-object/from16 p8, v6

    invoke-static/range {p3 .. p8}, Lcom/squareup/server/RetrofitModule_ProvideUnauthenticatedRetrofitFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideUnauthenticatedRetrofitFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedRetrofitProvider:Ljavax/inject/Provider;

    .line 6883
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedRetrofitProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/server/RetrofitModule_ProvideUnauthenticatedProtoServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideUnauthenticatedProtoServiceCreatorFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6884
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/account/AccountStatusServiceModule_ProvideDebuggableAccountStatusServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/account/AccountStatusServiceModule_ProvideDebuggableAccountStatusServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDebuggableAccountStatusServiceProvider:Ljavax/inject/Provider;

    .line 6885
    invoke-static/range {p2 .. p2}, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;->create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAppDelegateProvider:Ljavax/inject/Provider;

    .line 6886
    invoke-static {}, Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule_ProvideEncryptorFactory;->create()Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule_ProvideEncryptorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEncryptorProvider:Ljavax/inject/Provider;

    .line 6887
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAppDelegateProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilesDirectoryProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->defaultPersistentFactoryProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPosBuildProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v8

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashReporterProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEncryptorProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPlayServicesVersionsProvider:Ljavax/inject/Provider;

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    move-object/from16 p3, v3

    move-object/from16 p4, v4

    move-object/from16 p5, v5

    move-object/from16 p6, v6

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    move-object/from16 p9, v9

    move-object/from16 p10, v10

    move-object/from16 p11, v11

    invoke-static/range {p1 .. p11}, Lcom/squareup/account/PendingPreferencesCache_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/account/PendingPreferencesCache_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->pendingPreferencesCacheProvider:Ljavax/inject/Provider;

    .line 6888
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->pendingPreferencesCacheProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusResponseCacheProvider:Ljavax/inject/Provider;

    .line 6889
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSessionIdProvider:Ljavax/inject/Provider;

    .line 6890
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSessionIdProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->authHttpInterceptorProvider:Ljavax/inject/Provider;

    .line 6891
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientBuilderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->authHttpInterceptorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    .line 6892
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v4

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    move-object/from16 p3, v3

    move-object/from16 p4, v4

    move-object/from16 p5, v5

    move-object/from16 p6, v6

    invoke-static/range {p1 .. p6}, Lcom/squareup/server/RetrofitModule_ProvideAuthenticatedRetrofitFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideAuthenticatedRetrofitFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    .line 6893
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/RetrofitModule_ProvideAuthenticatedServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideAuthenticatedServiceCreatorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    .line 6894
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/account/AccountServiceModule_ProvideAccountServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/account/AccountServiceModule_ProvideAccountServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAccountServiceProvider:Ljavax/inject/Provider;

    .line 6895
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAccountServiceProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->pendingPreferencesCacheProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;->create()Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/account/PersistentAccountService_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/account/PersistentAccountService_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountServiceProvider:Ljavax/inject/Provider;

    .line 6896
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideLogoutServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideLogoutServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLogoutServiceProvider:Ljavax/inject/Provider;

    .line 6897
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideRxDevicePreferences2Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSharedPreferencesModule_ProvideRxDevicePreferences2Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    .line 6898
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/queue/QueueRootModule_ProvideCorruptQueuePreferenceFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideCorruptQueuePreferenceFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCorruptQueuePreferenceProvider:Ljavax/inject/Provider;

    .line 6899
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCorruptQueuePreferenceProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/queue/CorruptQueueRecorder_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/CorruptQueueRecorder_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private initialize2(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V
    .locals 13

    move-object v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p8

    .line 6913
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLogoutServiceProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v8}, Lcom/squareup/account/FileBackedAuthenticator_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/account/FileBackedAuthenticator_Factory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    .line 6914
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSessionIdProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-static {v4}, Lcom/squareup/account/AccountModule_Prod_ProvideSessionIdFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/account/AccountModule_Prod_ProvideSessionIdFactory;

    move-result-object v4

    invoke-static {v3, v4}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6915
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeviceIdProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDebuggableAccountStatusServiceProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusResponseCacheProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashReporterProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v9

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSessionIdProvider:Ljavax/inject/Provider;

    invoke-static/range {v4 .. v10}, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;

    move-result-object v4

    invoke-static {v4}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v3, v4}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6916
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-static {v4}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/AccountStatusModule_ProvideStatusFactory;

    move-result-object v4

    invoke-static {v3, v4}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6917
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4, v4}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    .line 6918
    invoke-static {p2}, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;->create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEmailSupportLedgerEnabledProvider:Ljavax/inject/Provider;

    .line 6919
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realDeviceProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEmailSupportLedgerEnabledProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4, v5, v6}, Lcom/squareup/settings/server/FeatureFlagFeatures_Factory_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/server/FeatureFlagFeatures_Factory_Factory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->factoryProvider:Ljavax/inject/Provider;

    .line 6920
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->factoryProvider:Ljavax/inject/Provider;

    invoke-static {v4}, Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/SposAppModule_ProvideFeatureFlagFeaturesFactory;

    move-result-object v4

    invoke-static {v3, v4}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6921
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    move-object/from16 v5, p10

    invoke-static {v5, v3, v4}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->create(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 6922
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v4, p9

    invoke-static {v4, v3}, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->create(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 6923
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashReporterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4, v5}, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashnadoReporterProvider:Ljavax/inject/Provider;

    .line 6924
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashnadoReporterProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    .line 6925
    invoke-static {}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;->create()Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLCRExecutorProvider:Ljavax/inject/Provider;

    .line 6926
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/tmn/HandlerFactory_Factory;->create()Lcom/squareup/tmn/HandlerFactory_Factory;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectivityManagerProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/squareup/tmn/TmnTimings_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tmn/TmnTimings_Factory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->tmnTimingsProvider:Ljavax/inject/Provider;

    .line 6927
    invoke-static {}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideSquidInterfaceSchedulerFactory;->create()Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideSquidInterfaceSchedulerFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    .line 6928
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesBluetoothManagerProvider:Ljavax/inject/Provider;

    .line 6929
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesBluetoothManagerProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4}, Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/blescan/BluetoothModule_Prod_ProvideUtilsFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    .line 6930
    invoke-static {}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperExecutorServiceFactory;->create()Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperExecutorServiceFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperExecutorServiceProvider:Ljavax/inject/Provider;

    .line 6931
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/ms/ReleaseMinesweeperModule_ProvideMinesweeperServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ms/ReleaseMinesweeperModule_ProvideMinesweeperServiceFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperServiceProvider:Ljavax/inject/Provider;

    .line 6932
    new-instance v3, Ldagger/internal/DelegateFactory;

    invoke-direct {v3}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMsFactoryProvider:Ljavax/inject/Provider;

    .line 6933
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMsFactoryProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMinesweeperProvider:Ljavax/inject/Provider;

    .line 6934
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMinesweeperProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperTicketProvider:Ljavax/inject/Provider;

    .line 6935
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperTicketProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v7

    invoke-static {v3, v4, v5, v6, v7}, Lcom/squareup/ms/MinesweeperHelper_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/MinesweeperHelper_Factory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->minesweeperHelperProvider:Ljavax/inject/Provider;

    .line 6936
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->minesweeperHelperProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDataListenerProvider:Ljavax/inject/Provider;

    .line 6937
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/CommonAppModule_ProvideNativeLibraryLoggerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideNativeLibraryLoggerFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNativeLibraryLoggerProvider:Ljavax/inject/Provider;

    .line 6938
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNativeLibraryLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4, v5}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRelinkerLoaderProvider:Ljavax/inject/Provider;

    .line 6939
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->minesweeperHelperProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperLoggerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperLoggerFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperLoggerProvider:Ljavax/inject/Provider;

    .line 6940
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperExecutorServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v3, v4, v5}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideMsFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMsProvider:Ljavax/inject/Provider;

    .line 6941
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMsFactoryProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperExecutorServiceProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDataListenerProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRelinkerLoaderProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperLoggerProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMsProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideNoopMinesweeperFactory;->create()Lcom/squareup/ms/SharedMinesweeperModule_ProvideNoopMinesweeperFactory;

    move-result-object v11

    invoke-static/range {v4 .. v11}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;

    move-result-object v4

    invoke-static {v4}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v3, v4}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6942
    invoke-static/range {p7 .. p7}, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;->create(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 6943
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener_Factory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    .line 6944
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    .line 6945
    new-instance v3, Ldagger/internal/DelegateFactory;

    invoke-direct {v3}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    .line 6946
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;

    .line 6947
    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRelinkerLoaderProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static/range {v4 .. v9}, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    .line 6948
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetListenerProvider:Ljavax/inject/Provider;

    .line 6949
    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetListenerProvider:Ljavax/inject/Provider;

    invoke-static {v2, v4, v5}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v3, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6950
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    .line 6951
    invoke-static/range {p6 .. p6}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 6952
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesBluetoothManagerProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/blescan/BluetoothModule_ProvideAdapterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/blescan/BluetoothModule_ProvideAdapterFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdapterProvider:Ljavax/inject/Provider;

    .line 6953
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdapterProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/blescan/BluetoothModule_ProvideBluetoothLeScannerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/blescan/BluetoothModule_ProvideBluetoothLeScannerFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothLeScannerProvider:Ljavax/inject/Provider;

    .line 6954
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScannerProvider:Ljavax/inject/Provider;

    .line 6955
    invoke-static/range {p6 .. p6}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScanFilterFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScanFilterFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScanFilterProvider:Ljavax/inject/Provider;

    .line 6956
    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdapterProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothLeScannerProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScannerProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScanFilterProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    move-object/from16 v3, p11

    invoke-static/range {v3 .. v9}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSystemBleScannerProvider:Ljavax/inject/Provider;

    .line 6957
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScannerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSystemBleScannerProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScanFilterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v3, v4, v5, v6}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 6958
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdapterProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothDiscovererProvider:Ljavax/inject/Provider;

    .line 6959
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothDiscovererProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 6960
    invoke-static {}, Lcom/squareup/squarewave/wav/AudioRingBuffer_Factory;->create()Lcom/squareup/squarewave/wav/AudioRingBuffer_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->audioRingBufferProvider:Ljavax/inject/Provider;

    .line 6961
    invoke-static {}, Lcom/squareup/wavpool/swipe/SwipeBus_Factory;->create()Lcom/squareup/wavpool/swipe/SwipeBus_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->swipeBusProvider:Ljavax/inject/Provider;

    .line 6962
    invoke-static/range {p6 .. p6}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;->create(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleExecutorProvider:Ljavax/inject/Provider;

    .line 6963
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleExecutorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleThreadProvider:Ljavax/inject/Provider;

    .line 6964
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 6965
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleExecutorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleDispatcherFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleDispatcherFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleDispatcherProvider:Ljavax/inject/Provider;

    .line 6966
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResourcesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/ResModule_ProvideResFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/ResModule_ProvideResFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    .line 6967
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/notification/NotificationWrapper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/notification/NotificationWrapper_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->notificationWrapperProvider:Ljavax/inject/Provider;

    .line 6968
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->notificationWrapperProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideForegroundServiceStarterProvider:Ljavax/inject/Provider;

    .line 6969
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideForegroundServiceStarterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/queue/QueueService_Starter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueService_Starter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    .line 6970
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectivityManagerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/internet/RealInternetStatusMonitor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/internet/RealInternetStatusMonitor_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realInternetStatusMonitorProvider:Ljavax/inject/Provider;

    .line 6971
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectivityManagerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realInternetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/connectivity/RealConnectivityMonitor_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    .line 6972
    invoke-static {}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideJobNotificationManagerFactory;->create()Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideJobNotificationManagerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJobNotificationManagerProvider:Ljavax/inject/Provider;

    .line 6973
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJobNotificationManagerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/backgroundjob/RealBackgroundJobManager_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBackgroundJobManagerProvider:Ljavax/inject/Provider;

    .line 6974
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/thread/Rx2FileScheduler_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/thread/Rx2FileScheduler_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->rx2FileSchedulerProvider:Ljavax/inject/Provider;

    .line 6975
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/queue/CorruptQueueHelper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/queue/CorruptQueueHelper_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    .line 6976
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->create()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideQueueConverterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideQueueConverterProvider:Ljavax/inject/Provider;

    .line 6977
    invoke-static {}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;->create()Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideQueueConverterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLoggedOutRetrofitQueueFactoryProvider:Ljavax/inject/Provider;

    .line 6978
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilesDirectoryProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLoggedOutRetrofitQueueFactoryProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrossSessionStoreAndForwardTasksQueueProvider:Ljavax/inject/Provider;

    .line 6979
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileHandlerThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/FileThreadEnforcer_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/FileThreadEnforcer_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 6980
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->create()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    .line 6981
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/HttpRetrofit1Module_ProvideRetrofitClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpRetrofit1Module_ProvideRetrofitClientFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRetrofitClientProvider:Ljavax/inject/Provider;

    .line 6982
    invoke-static {}, Lcom/squareup/util/AndroidModule_ProvideExecutorServiceFactory;->create()Lcom/squareup/util/AndroidModule_ProvideExecutorServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExecutorServiceProvider:Ljavax/inject/Provider;

    .line 6983
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRetrofitClientProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExecutorServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/server/RestAdapterCommonModule_ProvideProtoRestAdapterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterCommonModule_ProvideProtoRestAdapterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoRestAdapterProvider:Ljavax/inject/Provider;

    .line 6984
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoRestAdapterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/RestAdapterReleaseModule_ProvideProtoServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterReleaseModule_ProvideProtoServiceCreatorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    .line 6985
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideStoreAndForwardBillServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideStoreAndForwardBillServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoreAndForwardBillServiceProvider:Ljavax/inject/Provider;

    .line 6986
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->rx2FileSchedulerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/thread/Rx2SchedulerModule_ProvideFileThreadSchedulerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileThreadSchedulerProvider:Ljavax/inject/Provider;

    .line 6987
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileThreadHolderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/thread/FileThreadModule_ProvideFilethreadEnforcerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/thread/FileThreadModule_ProvideFilethreadEnforcerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilethreadEnforcerProvider:Ljavax/inject/Provider;

    .line 6988
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/queue/QueueRootModule_ProvideLastEmptyStoredPaymentLoggedAtFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideLastEmptyStoredPaymentLoggedAtFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLastEmptyStoredPaymentLoggedAtProvider:Ljavax/inject/Provider;

    .line 6989
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideQueueConverterProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;->create()Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLastEmptyStoredPaymentLoggedAtProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideStoreAndForwardPaymentTaskConverterFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoreAndForwardPaymentTaskConverterProvider:Ljavax/inject/Provider;

    .line 6990
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileThreadSchedulerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilethreadEnforcerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoreAndForwardPaymentTaskConverterProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->corruptQueueHelperProvider:Ljavax/inject/Provider;

    move-object/from16 p4, v1

    move-object/from16 p5, v2

    move-object/from16 p6, v3

    move-object/from16 p7, v4

    move-object/from16 p8, v5

    move-object/from16 p9, v6

    invoke-static/range {p4 .. p9}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->redundantStoredPaymentsQueueFactoryProvider:Ljavax/inject/Provider;

    .line 6991
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->redundantStoredPaymentsQueueFactoryProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoredPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    .line 6992
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/HttpModule_ProvideTransactionLedgerOkHttpClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideTransactionLedgerOkHttpClientFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerOkHttpClientProvider:Ljavax/inject/Provider;

    .line 6993
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDiagnosticsRetrofitClientProvider:Ljavax/inject/Provider;

    .line 6994
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDiagnosticsRetrofitClientProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExecutorServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/server/RestAdapterCommonModule_ProvideTransactionLedgerRestAdapterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterCommonModule_ProvideTransactionLedgerRestAdapterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerRestAdapterProvider:Ljavax/inject/Provider;

    .line 6995
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerRestAdapterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/RestAdapterCommonModule_ProvideTransactionLedgerServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterCommonModule_ProvideTransactionLedgerServiceCreatorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerServiceCreatorProvider:Ljavax/inject/Provider;

    .line 6996
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesCommonModule_ProvideTransactionLedgerServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesCommonModule_ProvideTransactionLedgerServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerServiceProvider:Ljavax/inject/Provider;

    .line 6997
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->statusBarFirmwareUpdateNotificationServiceStarterProvider:Ljavax/inject/Provider;

    .line 6998
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateModule_ProvideFirmwareUpdateServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/FirmwareUpdateModule_ProvideFirmwareUpdateServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirmwareUpdateServiceProvider:Ljavax/inject/Provider;

    .line 6999
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;->create()Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesUUIDGeneratorProvider:Ljavax/inject/Provider;

    .line 7000
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesUUIDGeneratorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/log/ReaderSessionIds_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/log/ReaderSessionIds_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerSessionIdsProvider:Ljavax/inject/Provider;

    .line 7001
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 7002
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerSessionIdsProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderOracleProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realInternetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/log/AclConnectionLoggingHelper_Factory;->create()Lcom/squareup/log/AclConnectionLoggingHelper_Factory;

    move-result-object v12

    invoke-static/range {v2 .. v12}, Lcom/squareup/log/ReaderEventLogger_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/ReaderEventLogger_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerEventLoggerProvider:Ljavax/inject/Provider;

    .line 7003
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->statusBarFirmwareUpdateNotificationServiceStarterProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirmwareUpdateServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerEventLoggerProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesUUIDGeneratorProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 p4, v1

    move-object/from16 p5, v2

    move-object/from16 p6, v3

    move-object/from16 p7, v4

    move-object/from16 p8, v5

    move-object/from16 p9, v6

    move-object/from16 p10, v7

    move-object/from16 p11, v8

    invoke-static/range {p4 .. p11}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    .line 7004
    invoke-static {}, Lcom/squareup/cardreader/RemoteCardReaderModule_ProvideCardReaderIdFactory;->create()Lcom/squareup/cardreader/RemoteCardReaderModule_ProvideCardReaderIdFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    .line 7005
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/RemoteCardReaderModule_ProvideCardReaderInfoForRemoteReaderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/RemoteCardReaderModule_ProvideCardReaderInfoForRemoteReaderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderInfoForRemoteReaderProvider:Ljavax/inject/Provider;

    .line 7006
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderInfoForRemoteReaderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRemoteCardReaderProvider:Ljavax/inject/Provider;

    .line 7007
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    .line 7008
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v2

    move-object/from16 v3, p3

    invoke-static {v3, v1, v2}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->create(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePairedReaderNamesProvider:Ljavax/inject/Provider;

    .line 7009
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePairedReaderNamesProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->storedCardReadersProvider:Ljavax/inject/Provider;

    .line 7010
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderOracleProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRemoteCardReaderProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->storedCardReadersProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v8

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v9

    move-object p1, v2

    move-object p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    invoke-static/range {p1 .. p8}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 7011
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/log/OhSnapBusBoy_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/log/OhSnapBusBoy_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->ohSnapBusBoyProvider:Ljavax/inject/Provider;

    .line 7012
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/log/MainThreadBlockedLogger_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/MainThreadBlockedLogger_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private initialize3(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V
    .locals 13

    move-object v0, p0

    .line 7026
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/analytics/BranchHelper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/analytics/BranchHelper_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->branchHelperProvider:Ljavax/inject/Provider;

    .line 7027
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/backgroundjob/log/BackgroundJobLogger_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/backgroundjob/log/BackgroundJobLogger_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->backgroundJobLoggerProvider:Ljavax/inject/Provider;

    .line 7028
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJobNotificationManagerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBackgroundJobManagerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/queue/QueueJobCreator_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueJobCreator_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->queueJobCreatorProvider:Ljavax/inject/Provider;

    .line 7029
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBackgroundJobManagerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJobNotificationManagerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrossSessionStoreAndForwardTasksQueueProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->storeAndForwardJobCreatorProvider:Ljavax/inject/Provider;

    .line 7030
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;->create()Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/firebase/common/RealFirebase_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/firebase/common/RealFirebase_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realFirebaseProvider:Ljavax/inject/Provider;

    .line 7031
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realFirebaseProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceAvailabilityProvider:Ljavax/inject/Provider;

    .line 7032
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseAppProvider:Ljavax/inject/Provider;

    .line 7033
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseAppProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseMessagingFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseMessagingFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseMessagingProvider:Ljavax/inject/Provider;

    .line 7034
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseAppProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseInstanceIdProvider:Ljavax/inject/Provider;

    .line 7035
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceAvailabilityProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseMessagingProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFirebaseInstanceIdProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/firebase/fcm/FcmPushService_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/FcmPushService_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceProvider:Ljavax/inject/Provider;

    .line 7036
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/pushmessages/RealPushMessageDelegate_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pushmessages/RealPushMessageDelegate_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPushMessageDelegateProvider:Ljavax/inject/Provider;

    .line 7037
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    .line 7038
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;

    .line 7039
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/experiments/ExperimentsModule_ProvideBranPaymentPromptExperimentFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ExperimentsModule_ProvideBranPaymentPromptExperimentFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBranPaymentPromptExperimentProvider:Ljavax/inject/Provider;

    .line 7040
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->squareCardUpsellExperimentProvider:Ljavax/inject/Provider;

    .line 7041
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->iposSkipOnboardingProductionExperimentProvider:Ljavax/inject/Provider;

    .line 7042
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->iposSkipOnboardingStagingExperimentProvider:Ljavax/inject/Provider;

    .line 7043
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->iposSkipOnboardingProductionExperimentProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->iposSkipOnboardingStagingExperimentProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ExperimentsModule_ProvideIposSkipOnboardingExperimentFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideIposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    .line 7044
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->showMultipleRewardsCopyExperimentProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBranPaymentPromptExperimentProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->squareCardUpsellExperimentProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideIposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ExperimentsModule_ProvideAllExperimentsFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAllExperimentsProvider:Ljavax/inject/Provider;

    .line 7045
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/experiments/ExperimentsModule_ProvideExperimentsCacheFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ExperimentsModule_ProvideExperimentsCacheFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExperimentsCacheProvider:Ljavax/inject/Provider;

    .line 7046
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvidePublicStatusServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvidePublicStatusServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePublicStatusServiceProvider:Ljavax/inject/Provider;

    .line 7047
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideUserTokenFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/AccountStatusModule_ProvideUserTokenFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserTokenProvider:Ljavax/inject/Provider;

    .line 7048
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->serverExperimentsProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAllExperimentsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExperimentsCacheProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePublicStatusServiceProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserTokenProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstallationIdProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;

    move-result-object v10

    invoke-static/range {v2 .. v10}, Lcom/squareup/experiments/ServerExperiments_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ServerExperiments_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 7049
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoLocationMonitorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->locationAnalyticsUpdaterProvider:Ljavax/inject/Provider;

    .line 7050
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/barcodescanners/BarcodeScannerTracker_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    .line 7051
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUsbManagerProvider:Ljavax/inject/Provider;

    .line 7052
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUsbManagerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/usb/UsbDiscoverer_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/usb/UsbDiscoverer_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->usbDiscovererProvider:Ljavax/inject/Provider;

    .line 7053
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->usbDiscovererProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUsbManagerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUsbBarcodeScannersProvider:Ljavax/inject/Provider;

    .line 7054
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/ui/main/PosIntentParserModule_ProvideDeferredDeepLinkFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/main/PosIntentParserModule_ProvideDeferredDeepLinkFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeferredDeepLinkProvider:Ljavax/inject/Provider;

    .line 7055
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDeferredDeepLinkProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->appsFlyerDeepLinkListenerProvider:Ljavax/inject/Provider;

    .line 7056
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->appsFlyerDeepLinkListenerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideAppsFlyerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideAppsFlyerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7057
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasFinishedIdVerificationBeforeAppsFlyerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasFinishedIdVerificationBeforeAppsFlyerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasFinishedIdVerificationBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7058
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasSignedUpBeforeAppsFlyerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasSignedUpBeforeAppsFlyerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasSignedUpBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7059
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasSignedInBeforeAppsFlyerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasSignedInBeforeAppsFlyerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasSignedInBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7060
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasSentInvoiceOrTakenPaymentBeforeAppsFlyerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasSentInvoiceOrTakenPaymentBeforeAppsFlyerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasSentInvoiceOrTakenPaymentBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7061
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasTakenItemizedPaymentBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7062
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasCreatedItemBeforeAppsFlyerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasCreatedItemBeforeAppsFlyerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasCreatedItemBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    .line 7063
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasFinishedIdVerificationBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasSignedUpBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasSignedInBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasSentInvoiceOrTakenPaymentBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasTakenItemizedPaymentBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasCreatedItemBeforeAppsFlyerProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v10}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AppsFlyerAdAnalytics_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->appsFlyerAdAnalyticsProvider:Ljavax/inject/Provider;

    .line 7064
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExecutorServiceProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/adanalytics/AdIdGatherer_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdIdGatherer_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->adIdGathererProvider:Ljavax/inject/Provider;

    .line 7065
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->registerVersionCodeProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v8}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->sharedPreferencesBundleStoreProvider:Ljavax/inject/Provider;

    .line 7066
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->sharedPreferencesBundleStoreProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    .line 7067
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/text/TextModule_ProvideWholeNumberPercentageFormatterFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideWholeNumberPercentageFormatterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;

    .line 7068
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;->create()Lcom/squareup/CommonAppModule_ProvideRegisterGsonFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideDefaultToLoginFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideDefaultToLoginFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDefaultToLoginProvider:Ljavax/inject/Provider;

    .line 7069
    invoke-static {}, Lcom/squareup/analytics/EncryptedEmailsFromReferrals_Factory;->create()Lcom/squareup/analytics/EncryptedEmailsFromReferrals_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    .line 7070
    invoke-static {}, Lcom/squareup/ui/ActivityResultHandler_Factory;->create()Lcom/squareup/ui/ActivityResultHandler_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->activityResultHandlerProvider:Ljavax/inject/Provider;

    .line 7071
    invoke-static {}, Lcom/squareup/ProductionServerModule_ProvideConnectApiServerFactory;->create()Lcom/squareup/ProductionServerModule_ProvideConnectApiServerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectApiServerProvider:Ljavax/inject/Provider;

    .line 7072
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectApiServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v4

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    move-object/from16 p6, v1

    move-object/from16 p7, v2

    move-object/from16 p8, v3

    move-object/from16 p9, v4

    move-object/from16 p10, v5

    move-object/from16 p11, v6

    invoke-static/range {p6 .. p11}, Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideConnectRetrofitFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectRetrofitProvider:Ljavax/inject/Provider;

    .line 7073
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectRetrofitProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/RetrofitModule_ProvideConnectServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideConnectServiceCreatorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectServiceCreatorProvider:Ljavax/inject/Provider;

    .line 7074
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/api/ConnectServiceModule_ProvideConnectServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/api/ConnectServiceModule_ProvideConnectServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideConnectServiceProvider:Ljavax/inject/Provider;

    .line 7075
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/thread/Rx1FileScheduler_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/thread/Rx1FileScheduler_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->rx1FileSchedulerProvider:Ljavax/inject/Provider;

    .line 7076
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->rx1FileSchedulerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileThreadSchedulerProvider2:Ljavax/inject/Provider;

    .line 7077
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v2

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFileThreadSchedulerProvider2:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/api/ClientSettingsCache_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ClientSettingsCache_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->clientSettingsCacheProvider:Ljavax/inject/Provider;

    .line 7078
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->useFeatureFlagCatalogIntegrationControllerProvider:Ljavax/inject/Provider;

    .line 7079
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusStoreAndForwardEnabledSettingProvider:Ljavax/inject/Provider;

    .line 7080
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsOkHttpClientProvider:Ljavax/inject/Provider;

    .line 7081
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v4

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    move-object/from16 p6, v1

    move-object/from16 p7, v2

    move-object/from16 p8, v3

    move-object/from16 p9, v4

    move-object/from16 p10, v5

    move-object/from16 p11, v6

    invoke-static/range {p6 .. p11}, Lcom/squareup/server/RetrofitModule_ProvideCogsAuthenticatedRetrofitFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideCogsAuthenticatedRetrofitFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    .line 7082
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/RetrofitModule_ProvideCogsAuthenticatedServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideCogsAuthenticatedServiceCreatorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    .line 7083
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCogsServiceProvider:Ljavax/inject/Provider;

    .line 7084
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideCatalogConnectV2ServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideCatalogConnectV2ServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCatalogConnectV2ServiceProvider:Ljavax/inject/Provider;

    .line 7085
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRetrofitClientProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideExecutorServiceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/server/RestAdapterCommonModule_ProvideGsonRestAdapterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterCommonModule_ProvideGsonRestAdapterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonRestAdapterProvider:Ljavax/inject/Provider;

    .line 7086
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonRestAdapterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonServiceCreatorProvider:Ljavax/inject/Provider;

    .line 7087
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideEmployeesServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideEmployeesServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEmployeesServiceProvider:Ljavax/inject/Provider;

    .line 7088
    invoke-static {}, Lcom/squareup/money/MoneyModule_ProvideMoneyLocaleFormatterFactory;->create()Lcom/squareup/money/MoneyModule_ProvideMoneyLocaleFormatterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyLocaleFormatterProvider:Ljavax/inject/Provider;

    .line 7089
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyLocaleFormatterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 7090
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideBillCreationServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideBillCreationServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBillCreationServiceProvider:Ljavax/inject/Provider;

    .line 7091
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerServiceProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransactionLedgerOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v5

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object v6

    move-object/from16 p6, v1

    move-object/from16 p7, v2

    move-object/from16 p8, v3

    move-object/from16 p9, v4

    move-object/from16 p10, v5

    move-object/from16 p11, v6

    invoke-static/range {p6 .. p11}, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->transactionLedgerUploaderProvider:Ljavax/inject/Provider;

    .line 7092
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFilesDirectoryProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->transactionLedgerUploaderProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v8}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFactoryProvider:Ljavax/inject/Provider;

    .line 7093
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/crm/CrmServicesMainModule_ProvideRolodexServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/crm/CrmServicesMainModule_ProvideRolodexServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRolodexServiceProvider:Ljavax/inject/Provider;

    .line 7094
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvidePaymentServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvidePaymentServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePaymentServiceProvider:Ljavax/inject/Provider;

    .line 7095
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/CommonAppModule_ProvidePicassoMemoryCacheFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvidePicassoMemoryCacheFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePicassoMemoryCacheProvider:Ljavax/inject/Provider;

    .line 7096
    invoke-static {}, Lcom/squareup/ThumborProdModule_ProvideThumborFactory;->create()Lcom/squareup/ThumborProdModule_ProvideThumborFactory;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEnsureThumborProvider:Ljavax/inject/Provider;

    .line 7097
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePicassoMemoryCacheProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEnsureThumborProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePicassoProvider:Ljavax/inject/Provider;

    .line 7098
    invoke-static/range {p5 .. p5}, Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;->create(Lcom/squareup/cashdrawer/CashDrawerModule;)Lcom/squareup/cashdrawer/CashDrawerModule_ProvideCashDrawerExecutorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCashDrawerExecutorProvider:Ljavax/inject/Provider;

    .line 7099
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideLoyaltyServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideLoyaltyServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLoyaltyServiceProvider:Ljavax/inject/Provider;

    .line 7100
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideCashManagementServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideCashManagementServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCashManagementServiceProvider:Ljavax/inject/Provider;

    .line 7101
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesCommonModule_ProvideImageServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesCommonModule_ProvideImageServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideImageServiceProvider:Ljavax/inject/Provider;

    .line 7102
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/text/TextModule_ProvideMediumDateFormatFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideMediumDateFormatFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMediumDateFormatProvider:Ljavax/inject/Provider;

    .line 7103
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/text/TextModule_ProvideTimeFormatFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideTimeFormatFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTimeFormatProvider:Ljavax/inject/Provider;

    .line 7104
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideNfcReaderHasConnectedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideNfcReaderHasConnectedFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNfcReaderHasConnectedProvider:Ljavax/inject/Provider;

    .line 7105
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideHasConnectedToR6Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideHasConnectedToR6Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHasConnectedToR6Provider:Ljavax/inject/Provider;

    .line 7106
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideR12HasConnectedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideR12HasConnectedFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideR12HasConnectedProvider:Ljavax/inject/Provider;

    .line 7107
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/SecureSessionModule_ProvideSecureSessionServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/SecureSessionModule_ProvideSecureSessionServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSecureSessionServiceProvider:Ljavax/inject/Provider;

    .line 7108
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideNotificationManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideNotificationManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNotificationManagerProvider:Ljavax/inject/Provider;

    .line 7109
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesCommonModule_ProvideSafetyNetServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesCommonModule_ProvideSafetyNetServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSafetyNetServiceProvider:Ljavax/inject/Provider;

    .line 7110
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/util/AndroidModule_ProvideCacheDirectoryFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvideCacheDirectoryFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCacheDirectoryProvider:Ljavax/inject/Provider;

    .line 7111
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->notificationWrapperProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNotificationManagerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/payment/AutoVoidStatusBarNotifier_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/AutoVoidStatusBarNotifier_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->autoVoidStatusBarNotifierProvider:Ljavax/inject/Provider;

    .line 7112
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/queue/QueueRootModule_ProvideLastQueueServiceStartFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideLastQueueServiceStartFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLastQueueServiceStartProvider:Ljavax/inject/Provider;

    .line 7113
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/ble/BleEventLogFilter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleEventLogFilter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    .line 7114
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAdapterProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderOracleProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRelinkerLoaderProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->storedCardReadersProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v12}, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBleAutoConnectorProvider:Ljavax/inject/Provider;

    .line 7115
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBleAutoConnectorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleConnector_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleConnector_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->bleConnectorProvider:Ljavax/inject/Provider;

    .line 7116
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/server/messages/MessagesServiceModule_ProvideMessagesServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/messages/MessagesServiceModule_ProvideMessagesServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMessagesServiceProvider:Ljavax/inject/Provider;

    .line 7117
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/api/ServicesReleaseModule_ProvideClientInvoiceServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideClientInvoiceServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideClientInvoiceServiceProvider:Ljavax/inject/Provider;

    .line 7118
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->legacyAccountStatusProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStatusProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->useFeatureFlagCatalogIntegrationControllerProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusStoreAndForwardEnabledSettingProvider:Ljavax/inject/Provider;

    move-object/from16 p3, v1

    move-object/from16 p4, v2

    move-object/from16 p5, v3

    move-object/from16 p6, v4

    move-object/from16 p7, v5

    move-object/from16 p8, v6

    invoke-static/range {p3 .. p8}, Lcom/squareup/settings/server/AccountStatusSettings_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/server/AccountStatusSettings_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 7119
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/settings/server/RealUserSettingsProvider_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/server/RealUserSettingsProvider_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->realUserSettingsProvider:Ljavax/inject/Provider;

    .line 7120
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServiceEnabledFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServiceEnabledFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePushServiceEnabledProvider:Ljavax/inject/Provider;

    .line 7121
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServiceRegistrationFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServiceRegistrationFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePushServiceRegistrationProvider:Ljavax/inject/Provider;

    .line 7122
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/pushmessages/register/RegisterPushNotificationServiceModule_ProvideRegisterPushNotificationServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/pushmessages/register/RegisterPushNotificationServiceModule_ProvideRegisterPushNotificationServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRegisterPushNotificationServiceProvider:Ljavax/inject/Provider;

    .line 7123
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/merchantprofile/MerchantProfileServiceModule_ProvideMerchantProfileServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/merchantprofile/MerchantProfileServiceModule_ProvideMerchantProfileServiceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMerchantProfileServiceProvider:Ljavax/inject/Provider;

    .line 7124
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/util/AddAppNameFormatter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/util/AddAppNameFormatter_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->addAppNameFormatterProvider:Ljavax/inject/Provider;

    .line 7125
    invoke-static {p2}, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideActivityListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private initialize4(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V
    .locals 0

    .line 7139
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/locale/RealLocaleChangedNotifier_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/locale/RealLocaleChangedNotifier_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realLocaleChangedNotifierProvider:Ljavax/inject/Provider;

    .line 7140
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideReportEmailServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideReportEmailServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReportEmailServiceProvider:Ljavax/inject/Provider;

    .line 7141
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/address/AddressServiceCommonModule_ProvideAddressServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/address/AddressServiceCommonModule_ProvideAddressServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAddressServiceProvider:Ljavax/inject/Provider;

    .line 7142
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/loggedout/LoggedOutFeatureStarter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/loggedout/LoggedOutFeatureStarter_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->loggedOutFeatureStarterProvider:Ljavax/inject/Provider;

    .line 7143
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/PosAppComponent_Module_ProvideTourEducationItemsSeenFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/PosAppComponent_Module_ProvideTourEducationItemsSeenFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTourEducationItemsSeenProvider:Ljavax/inject/Provider;

    .line 7144
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRxDevicePreferences2Provider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/PosAppComponent_Module_ProvideTourEducationItemsSeenVerticalFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/PosAppComponent_Module_ProvideTourEducationItemsSeenVerticalFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTourEducationItemsSeenVerticalProvider:Ljavax/inject/Provider;

    .line 7145
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideBankAccountServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideBankAccountServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBankAccountServiceProvider:Ljavax/inject/Provider;

    .line 7146
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMultipassServiceProvider:Ljavax/inject/Provider;

    .line 7147
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideRewardsBalanceServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideRewardsBalanceServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRewardsBalanceServiceProvider:Ljavax/inject/Provider;

    .line 7148
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideActivationServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideActivationServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideActivationServiceProvider:Ljavax/inject/Provider;

    .line 7149
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/debitcard/LinkDebitCardServiceMainModule_ProvideLinkDebitCardServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/debitcard/LinkDebitCardServiceMainModule_ProvideLinkDebitCardServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLinkDebitCardServiceProvider:Ljavax/inject/Provider;

    .line 7150
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/depositschedule/DepositScheduleServiceMainModule_ProvideDepositScheduleServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/depositschedule/DepositScheduleServiceMainModule_ProvideDepositScheduleServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDepositScheduleServiceProvider:Ljavax/inject/Provider;

    .line 7151
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyLocaleFormatterProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCentsMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 7152
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvidePercentageFormatterFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvidePercentageFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePercentageFormatterProvider:Ljavax/inject/Provider;

    .line 7153
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideActivityListenerProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/android/activity/RealToastFactory_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/activity/RealToastFactory_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realToastFactoryProvider:Ljavax/inject/Provider;

    .line 7154
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyLocaleFormatterProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyModule_ProvideShortMoneyFormatterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideShortMoneyFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 7155
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideReferralServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideReferralServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReferralServiceProvider:Ljavax/inject/Provider;

    .line 7156
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLongDateFormatterProvider:Ljavax/inject/Provider;

    .line 7157
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideShippingAddressServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideShippingAddressServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShippingAddressServiceProvider:Ljavax/inject/Provider;

    .line 7158
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCountryCodeProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/phone/number/RealPhoneNumberHelper_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPhoneNumberHelperProvider:Ljavax/inject/Provider;

    .line 7159
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideRequestedPermissionsFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideRequestedPermissionsFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRequestedPermissionsProvider:Ljavax/inject/Provider;

    .line 7160
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceModule_ProvideRealCapitalFlexLoanServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceModule_ProvideRealCapitalFlexLoanServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCapitalFlexLoanServiceProvider:Ljavax/inject/Provider;

    .line 7161
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/core/server/bizbank/BizbankServiceMainModule_ProvideRealBizbankServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealBizbankServiceProvider:Ljavax/inject/Provider;

    .line 7162
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realOnlineStoreRestrictionsProvider:Ljavax/inject/Provider;

    .line 7163
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realOnlineStoreRestrictionsProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRealOnlineStoreRestrictions$impl_wiring_releaseProvider:Ljavax/inject/Provider;

    .line 7164
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideOnboardingServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideOnboardingServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOnboardingServiceProvider:Ljavax/inject/Provider;

    .line 7165
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realOnboardingActivityStarterProvider:Ljavax/inject/Provider;

    .line 7166
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/hudtoaster/log/HudToasterLogger_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/hudtoaster/log/HudToasterLogger_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->hudToasterLoggerProvider:Ljavax/inject/Provider;

    .line 7167
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realToastFactoryProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->hudToasterLoggerProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/hudtoaster/RealHudToaster_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realHudToasterProvider:Ljavax/inject/Provider;

    .line 7168
    invoke-static {}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideRealNfcUtilsFactory;->create()Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideRealNfcUtilsFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealNfcUtilsProvider:Ljavax/inject/Provider;

    .line 7169
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/CommonAppModule_ProvideLatestApiSequenceUuidFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideLatestApiSequenceUuidFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLatestApiSequenceUuidProvider:Ljavax/inject/Provider;

    .line 7170
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderHubUtils_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderHubUtils_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    .line 7171
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/CardReaderHubScoper_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderHubScoperProvider:Ljavax/inject/Provider;

    .line 7172
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTaxPercentageFormatterProvider:Ljavax/inject/Provider;

    .line 7173
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideBillListServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideBillListServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBillListServiceProvider:Ljavax/inject/Provider;

    .line 7174
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;->create()Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioHandlerThreadProvider:Ljavax/inject/Provider;

    .line 7175
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioHandlerThreadProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/CommonAppModule_ProvideSerialAudioThreadExecutorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideSerialAudioThreadExecutorFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialAudioThreadExecutorProvider:Ljavax/inject/Provider;

    .line 7176
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioHandlerThreadProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/CommonAppModule_ProvideAudioThreadEnforcerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideAudioThreadEnforcerFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 7177
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAudioManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideAudioManagerFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAudioManagerProvider:Ljavax/inject/Provider;

    .line 7178
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsServiceCommonModule_ProvideinstallmentsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/installments/InstallmentsServiceCommonModule_ProvideinstallmentsServiceFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideinstallmentsServiceProvider:Ljavax/inject/Provider;

    .line 7179
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicaServiceOkHttpClientProvider:Ljavax/inject/Provider;

    .line 7180
    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicaServiceOkHttpClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object p4

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object p5

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object p6

    iget-object p7, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    invoke-static/range {p2 .. p7}, Lcom/squareup/server/RetrofitModule_ProvideFelicatAuthenticatedRetrofitFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideFelicatAuthenticatedRetrofitFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicatAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    .line 7181
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicatAuthenticatedRetrofitProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/RetrofitModule_ProvideFelicaAuthenticatedServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideFelicaAuthenticatedServiceCreatorFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicaAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    .line 7182
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicaAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideFelicaServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideFelicaServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFelicaServiceProvider:Ljavax/inject/Provider;

    .line 7183
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realOnlineStoreAnalyticsProvider:Ljavax/inject/Provider;

    .line 7184
    invoke-static {}, Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;->create()Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWeeblyApiServerProvider:Ljavax/inject/Provider;

    .line 7185
    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWeeblyApiServerProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedClientProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideRpcSchedulerFactory;

    move-result-object p4

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object p5

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object p6

    iget-object p7, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStandardReceiverProvider:Ljavax/inject/Provider;

    invoke-static/range {p2 .. p7}, Lcom/squareup/server/RetrofitModule_ProvideUnauthenticatedWeeblyRetrofitFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideUnauthenticatedWeeblyRetrofitFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedWeeblyRetrofitProvider:Ljavax/inject/Provider;

    .line 7186
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedWeeblyRetrofitProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/RetrofitModule_ProvideWeeblyServiceCreatorFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWeeblyServiceCreatorProvider:Ljavax/inject/Provider;

    .line 7187
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCountryCodeProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/scrubber/PhoneNumberScrubber_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/scrubber/PhoneNumberScrubber_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 7188
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesFirstInvoiceTutorialViewedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvidesFirstInvoiceTutorialViewedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesFirstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;

    .line 7189
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyEnrollTutorialViewedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyEnrollTutorialViewedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesLoyaltyEnrollTutorialViewedProvider:Ljavax/inject/Provider;

    .line 7190
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyAdjustPointsTutorialViewedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyAdjustPointsTutorialViewedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesLoyaltyAdjustPointsTutorialViewedProvider:Ljavax/inject/Provider;

    .line 7191
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvidesLoyaltyRedeemRewardsTutorialViewedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesLoyaltyRedeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;

    .line 7192
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->create()Lcom/squareup/gson/GsonModule_ProvideGsonFactory;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/settings/DeviceSettingsModule_ProvideNewInvoiceFeaturesTutorialTipsDismissedFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideNewInvoiceFeaturesTutorialTipsDismissedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNewInvoiceFeaturesTutorialTipsDismissedProvider:Ljavax/inject/Provider;

    .line 7193
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/precog/PrecogServiceCommonModule_ProvidePrecogServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/precog/PrecogServiceCommonModule_ProvidePrecogServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePrecogServiceProvider:Ljavax/inject/Provider;

    .line 7194
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideMessagehubServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideMessagehubServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMessagehubServiceProvider:Ljavax/inject/Provider;

    .line 7195
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNotificationManagerProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->notificationWrapperProvider:Ljavax/inject/Provider;

    iget-object p5, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->addAppNameFormatterProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->paymentIncompleteStatusBarNotifierProvider:Ljavax/inject/Provider;

    .line 7196
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAccessibilityManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideAccessibilityManagerFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAccessibilityManagerProvider:Ljavax/inject/Provider;

    .line 7197
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideDamagedReaderServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideDamagedReaderServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDamagedReaderServiceProvider:Ljavax/inject/Provider;

    .line 7198
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideDailyLocalSettingFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideDailyLocalSettingFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDailyLocalSettingProvider:Ljavax/inject/Provider;

    .line 7199
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideR12FirstTimeTutorialViewedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideR12FirstTimeTutorialViewedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideR12FirstTimeTutorialViewedProvider:Ljavax/inject/Provider;

    .line 7200
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideReportCoredumpServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideReportCoredumpServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReportCoredumpServiceProvider:Ljavax/inject/Provider;

    .line 7201
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/DeviceSettingsModule_ProvideR6FirstTimeVideoViewedFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/DeviceSettingsModule_ProvideR6FirstTimeVideoViewedFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideR6FirstTimeVideoViewedProvider:Ljavax/inject/Provider;

    .line 7202
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/HeadsetStateInitializer_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateInitializerProvider:Ljavax/inject/Provider;

    .line 7203
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/time/TimeInfoChangedMonitor_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/time/TimeInfoChangedMonitor_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->timeInfoChangedMonitorProvider:Ljavax/inject/Provider;

    .line 7204
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->timeInfoChangedMonitorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/time/RealCurrentTimeZone_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/time/RealCurrentTimeZone_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeZoneProvider:Ljavax/inject/Provider;

    .line 7205
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeZoneProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/time/RealCurrentTime_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/time/RealCurrentTime_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeProvider:Ljavax/inject/Provider;

    .line 7206
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realLocalNotificationAnalyticsProvider:Ljavax/inject/Provider;

    .line 7207
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideOrderHubServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideOrderHubServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOrderHubServiceProvider:Ljavax/inject/Provider;

    .line 7208
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/communications/CommunicationsCommonModule_ProvideRealCommunicationsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/communications/CommunicationsCommonModule_ProvideRealCommunicationsServiceFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCommunicationsServiceProvider:Ljavax/inject/Provider;

    .line 7209
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realPushMessageDelegateProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/communications/service/sync/MessagesSyncNotifier_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/communications/service/sync/MessagesSyncNotifier_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->messagesSyncNotifierProvider:Ljavax/inject/Provider;

    .line 7210
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCommunicationsServiceProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->messagesSyncNotifierProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/communications/service/RealRemoteMessagesStore_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/communications/service/RealRemoteMessagesStore_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realRemoteMessagesStoreProvider:Ljavax/inject/Provider;

    .line 7211
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeZoneProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->dateAndTimeFormatterProvider:Ljavax/inject/Provider;

    .line 7212
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideSpeedTestServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideSpeedTestServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSpeedTestServiceProvider:Ljavax/inject/Provider;

    .line 7213
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideGiftCardServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideGiftCardServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGiftCardServiceProvider:Ljavax/inject/Provider;

    .line 7214
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptServiceCommonModule_ProvideReceiptServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/ReceiptServiceCommonModule_ProvideReceiptServiceFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideReceiptServiceProvider:Ljavax/inject/Provider;

    .line 7215
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/util/DefaultIntentAvailabilityManager_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/util/DefaultIntentAvailabilityManager_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->defaultIntentAvailabilityManagerProvider:Ljavax/inject/Provider;

    .line 7216
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/googlepay/client/GooglePayClientModule_ProvidesGooglePayClientFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/googlepay/client/GooglePayClientModule_ProvidesGooglePayClientFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesGooglePayClientProvider:Ljavax/inject/Provider;

    .line 7217
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideShortDateFormatFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideShortDateFormatFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortDateFormatProvider:Ljavax/inject/Provider;

    .line 7218
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideInvoiceFileAttachmentServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideInvoiceFileAttachmentServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInvoiceFileAttachmentServiceProvider:Ljavax/inject/Provider;

    .line 7219
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInstantDepositsServiceProvider:Ljavax/inject/Provider;

    .line 7220
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/balance/core/server/transfers/TransfersServiceMainModule_ProvideRealTransfersServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/core/server/transfers/TransfersServiceMainModule_ProvideRealTransfersServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealTransfersServiceProvider:Ljavax/inject/Provider;

    .line 7221
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/balance/activity/service/BalanceActivityServiceMainModule_ProvideRealBalanceActivityServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/activity/service/BalanceActivityServiceMainModule_ProvideRealBalanceActivityServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealBalanceActivityServiceProvider:Ljavax/inject/Provider;

    .line 7222
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideTransferReportsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideTransferReportsServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTransferReportsServiceProvider:Ljavax/inject/Provider;

    .line 7223
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideMediumDateFormatNoYearFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideMediumDateFormatNoYearFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMediumDateFormatNoYearProvider:Ljavax/inject/Provider;

    .line 7224
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/money/PositiveNegativeFormatter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/money/PositiveNegativeFormatter_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->positiveNegativeFormatterProvider:Ljavax/inject/Provider;

    .line 7225
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideInventoryServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideInventoryServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideInventoryServiceProvider:Ljavax/inject/Provider;

    .line 7226
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideBillRefundServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideBillRefundServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBillRefundServiceProvider:Ljavax/inject/Provider;

    .line 7227
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideVibratorFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideVibratorFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideVibratorProvider:Ljavax/inject/Provider;

    .line 7228
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideLongDateFormatNoYearFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideLongDateFormatNoYearFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLongDateFormatNoYearProvider:Ljavax/inject/Provider;

    .line 7229
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/crm/CrmServicesMainModule_ProvideDialogueServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/crm/CrmServicesMainModule_ProvideDialogueServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDialogueServiceProvider:Ljavax/inject/Provider;

    .line 7230
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideShortDateFormatNoYearFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideShortDateFormatNoYearFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortDateFormatNoYearProvider:Ljavax/inject/Provider;

    .line 7231
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideDiscountPercentageFormatterFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideDiscountPercentageFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDiscountPercentageFormatterProvider:Ljavax/inject/Provider;

    .line 7232
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvidePaperSignatureBatchServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvidePaperSignatureBatchServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePaperSignatureBatchServiceProvider:Ljavax/inject/Provider;

    .line 7233
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMoneyFormatterProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNotificationManagerProvider:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object p5, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->autoCaptureStatusBarNotifierProvider:Ljavax/inject/Provider;

    .line 7234
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideCatalogServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideCatalogServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCatalogServiceProvider:Ljavax/inject/Provider;

    .line 7235
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideJediServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideJediServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideJediServiceProvider:Ljavax/inject/Provider;

    .line 7236
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideOrdersServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideOrdersServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideOrdersServiceProvider:Ljavax/inject/Provider;

    .line 7237
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/android/util/AndroidUtilModule_ProvideDownloadManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideDownloadManagerFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDownloadManagerProvider:Ljavax/inject/Provider;

    .line 7238
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDownloadManagerProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSessionIdProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideServerProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->authenticatedSquareDownloadManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private initialize5(Lcom/squareup/anrchaperone/AnrChaperoneModule;Lcom/squareup/AppBootstrapModule;Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Lcom/squareup/settings/DeviceSettingsModule;Lcom/squareup/cashdrawer/CashDrawerModule;Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;)V
    .locals 0

    .line 7252
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realCurrentTimeZoneProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTimeZoneProvider:Ljavax/inject/Provider;

    .line 7253
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideCouponsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideCouponsServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCouponsServiceProvider:Ljavax/inject/Provider;

    .line 7254
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideGsonServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideTimecardsServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideTimecardsServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTimecardsServiceProvider:Ljavax/inject/Provider;

    .line 7255
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/api/ServicesReleaseModule_ProvideDisputesServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/api/ServicesReleaseModule_ProvideDisputesServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDisputesServiceProvider:Ljavax/inject/Provider;

    .line 7256
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortFractionalPercentageProvider:Ljavax/inject/Provider;

    .line 7257
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortFractionalPercentageProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->percentageChangeFormatterProvider:Ljavax/inject/Provider;

    .line 7258
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyModule_ProvideCompactMoneyFormatterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideCompactMoneyFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCompactMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 7259
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyModule_ProvideCompactShorterMoneyFormatterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideCompactShorterMoneyFormatterFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCompactShorterMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 7260
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDevicePreferencesProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/settings/RealLocalSettingFactory_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/settings/RealLocalSettingFactory_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realLocalSettingFactoryProvider:Ljavax/inject/Provider;

    .line 7261
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/text/TextModule_ProvideNumberFormatterFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideNumberFormatterFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideNumberFormatterProvider:Ljavax/inject/Provider;

    .line 7262
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticatedServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/customreport/data/service/CustomReportServiceCommonModule_ProvideRealCustomReportServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/customreport/data/service/CustomReportServiceCommonModule_ProvideRealCustomReportServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCustomReportServiceProvider:Ljavax/inject/Provider;

    .line 7263
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocaleProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideShortDateTimeProvider:Ljavax/inject/Provider;

    .line 7264
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/location/AndroidGeoAddressProvider_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/AndroidGeoAddressProvider_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoAddressProvider:Ljavax/inject/Provider;

    .line 7265
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoAddressProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLocationProvider:Ljavax/inject/Provider;

    iget-object p4, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideTelephonyManagerProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3, p4}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/AndroidGeoCountryCodeGuesser_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoCountryCodeGuesserProvider:Ljavax/inject/Provider;

    .line 7266
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideFeatureFlagFeaturesProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDefaultToLoginProvider:Ljavax/inject/Provider;

    iget-object p3, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoCountryCodeGuesserProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->worldLandingScreenSelectorProvider:Ljavax/inject/Provider;

    .line 7267
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->androidGeoCountryCodeGuesserProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/pos/loggedout/PosLearnMoreTourPager_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/pos/loggedout/PosLearnMoreTourPager_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->posLearnMoreTourPagerProvider:Ljavax/inject/Provider;

    .line 7268
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/account/AuthenticationModule_ProvideAuthenticationServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/account/AuthenticationModule_ProvideAuthenticationServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAuthenticationServiceProvider:Ljavax/inject/Provider;

    .line 7269
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUnauthenticatedProtoServiceCreatorProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/server/account/AuthenticationModule_ProvidePasswordServiceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/server/account/AuthenticationModule_ProvidePasswordServiceFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providePasswordServiceProvider:Ljavax/inject/Provider;

    .line 7270
    iget-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideApplicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/android/util/AndroidUtilModule_ProvidesAccountManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvidesAccountManagerFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->providesAccountManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectBootReceiver(Lcom/squareup/queue/QueueService$BootReceiver;)Lcom/squareup/queue/QueueService$BootReceiver;
    .locals 1

    .line 7526
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_BootReceiver_MembersInjector;->injectQueueServiceStarter(Lcom/squareup/queue/QueueService$BootReceiver;Lcom/squareup/queue/QueueServiceStarter;)V

    return-object p1
.end method

.method private injectFcmService(Lcom/squareup/firebase/fcm/FcmService;)Lcom/squareup/firebase/fcm/FcmService;
    .locals 1

    .line 7632
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fcmPushServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {p1, v0}, Lcom/squareup/firebase/fcm/FcmService_MembersInjector;->injectFcmPushService(Lcom/squareup/firebase/fcm/FcmService;Lcom/squareup/firebase/fcm/FcmPushService;)V

    return-object p1
.end method

.method private injectFirmwareUpdateNotificationService(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;)Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;
    .locals 1

    .line 7616
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectRes(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/util/Res;)V

    .line 7617
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideWholeNumberPercentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectPercentageFormatter(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/text/Formatter;)V

    .line 7618
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectFirmwareUpdateDispatcher(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;)V

    .line 7619
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationWrapper()Lcom/squareup/notification/NotificationWrapper;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectNotificationWrapper(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/notification/NotificationWrapper;)V

    return-object p1
.end method

.method private injectQueueService(Lcom/squareup/queue/QueueService;)Lcom/squareup/queue/QueueService;
    .locals 1

    .line 7531
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectAuthenticator(Lcom/squareup/queue/QueueService;Lcom/squareup/account/LegacyAuthenticator;)V

    .line 7532
    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectClock(Lcom/squareup/queue/QueueService;Lcom/squareup/util/Clock;)V

    .line 7533
    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->provideGson()Lcom/google/gson/Gson;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectGson(Lcom/squareup/queue/QueueService;Lcom/google/gson/Gson;)V

    .line 7534
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectEventSink(Lcom/squareup/queue/QueueService;Lcom/squareup/badbus/BadEventSink;)V

    .line 7535
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectConnectivityMonitor(Lcom/squareup/queue/QueueService;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    .line 7536
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBackgroundJobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectJobManager(Lcom/squareup/queue/QueueService;Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 7537
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLoggedOutTaskWatcher()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectLoggedOutTaskWatcher(Lcom/squareup/queue/QueueService;Ljava/lang/Object;)V

    .line 7538
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrossSessionStoreAndForwardTasksQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectStoreAndForwardQueue(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 7539
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectOhSnapLogger(Lcom/squareup/queue/QueueService;Lcom/squareup/log/OhSnapLogger;)V

    .line 7540
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/logging/RemoteLogger;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectRemoteLogger(Lcom/squareup/queue/QueueService;Lcom/squareup/logging/RemoteLogger;)V

    .line 7541
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideForegroundServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectForegroundServiceStarter(Lcom/squareup/queue/QueueService;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V

    .line 7542
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectQueueServiceStarter(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueServiceStarter;)V

    .line 7543
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getLastQueueServiceStartLocalSettingOfLong()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectLastQueueServiceStart(Lcom/squareup/queue/QueueService;Lcom/squareup/settings/LocalSetting;)V

    .line 7544
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagFeatures()Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectFeatures(Lcom/squareup/queue/QueueService;Lcom/squareup/settings/server/Features;)V

    return-object p1
.end method

.method private injectRedirectTrackingReceiver(Lcom/squareup/analytics/RedirectTrackingReceiver;)Lcom/squareup/analytics/RedirectTrackingReceiver;
    .locals 1

    .line 7625
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectAnalytics(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/analytics/Analytics;)V

    .line 7626
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideDefaultToLoginProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectPostInstallLoginSetting(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/settings/LocalSetting;)V

    .line 7627
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-static {p1, v0}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectEncryptedEmailsFromReferrals(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    return-object p1
.end method

.method private injectRegisterAppDelegate(Lcom/squareup/RegisterAppDelegate;)Lcom/squareup/RegisterAppDelegate;
    .locals 1

    .line 7572
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->eventStreamAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAnalytics(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/analytics/Analytics;)V

    .line 7573
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAnrChaperone()Lcom/squareup/anrchaperone/AnrChaperone;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAnrChaperone(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/anrchaperone/AnrChaperone;)V

    .line 7574
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileBackedAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAuthenticator(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/account/LegacyAuthenticator;)V

    .line 7575
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadBus;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectBus(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/badbus/BadBus;)V

    .line 7576
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectCardReaderHub(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/CardReaderHub;)V

    .line 7577
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectCardReaderOracle(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    .line 7578
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDeviceSettingsSettingsInitializer()Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectDeviceSettingsSettingsInitializer(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/settings/DeviceSettingsSettingsInitializer;)V

    .line 7579
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectOhSnapLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/OhSnapLogger;)V

    .line 7580
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideEventStreamV2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectEventStreamV2(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/eventstream/v2/EventstreamV2;)V

    .line 7581
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRecorderErrorReporterListener()Lcom/squareup/swipe/RecorderErrorReporterListener;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectRecorderErrorReporterListener(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/swipe/RecorderErrorReporterListener;)V

    .line 7582
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRelinkerLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectLibraryLoader(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/loader/LibraryLoader;)V

    .line 7583
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MsFactory;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectMsFactory(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/ms/MsFactory;)V

    .line 7584
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->ohSnapBusBoyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapBusBoy;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectOhSnapBusBoy(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/OhSnapBusBoy;)V

    .line 7585
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideForegroundServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectForegroundServiceStarter(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V

    .line 7586
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectQueueServiceStarter(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/queue/QueueServiceStarter;)V

    .line 7587
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/MainThreadBlockedLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectMainThreadBlockedLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/MainThreadBlockedLogger;)V

    .line 7588
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerSessionIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderSessionIds;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectReaderSessionIds(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/ReaderSessionIds;)V

    .line 7589
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->crashReportingLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/logging/RemoteLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectRemoteLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/logging/RemoteLogger;)V

    .line 7590
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->branchHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/DeepLinkHelper;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectDeepLinkHelper(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/analytics/DeepLinkHelper;)V

    .line 7591
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectCardReaderListeners(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/cardreader/CardReaderListeners;)V

    .line 7592
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/ReaderEventLogger;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectReaderEventLogger(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/log/ReaderEventLogger;)V

    .line 7593
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationWrapper()Lcom/squareup/notification/NotificationWrapper;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectNotificationWrapper(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/notification/NotificationWrapper;)V

    .line 7594
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getForAppSetOfScoped()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectAppScopedItems(Lcom/squareup/RegisterAppDelegate;Ljava/util/Set;)V

    .line 7595
    invoke-static {}, Lcom/squareup/PosAppComponent$Module;->provideIsReaderSdk()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectIsReaderSdk(Lcom/squareup/RegisterAppDelegate;Z)V

    .line 7596
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentAccountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectPersistentAccountStatusService(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/accountstatus/PersistentAccountStatusService;)V

    .line 7597
    invoke-static {}, Lcom/squareup/x2/NoX2AppModule_ProvideMaybeSquareDeviceFactory;->provideMaybeSquareDevice()Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectBadMaybeSquareDeviceCheck(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    .line 7598
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealPlayServicesVersions()Lcom/squareup/firebase/versions/RealPlayServicesVersions;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/RegisterAppDelegate_MembersInjector;->injectPlayServicesVersions(Lcom/squareup/RegisterAppDelegate;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    return-object p1
.end method

.method private injectShareSheetSelectedAppReceiver(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;)Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;
    .locals 1

    .line 7638
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealOnlineStoreAnalytics()Lcom/squareup/onlinestore/analytics/RealOnlineStoreAnalytics;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver_MembersInjector;->injectAnalytics(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V

    return-object p1
.end method

.method private injectStoreAndForwardTask(Lcom/squareup/payment/offline/StoreAndForwardTask;)Lcom/squareup/payment/offline/StoreAndForwardTask;
    .locals 1

    .line 7549
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectApplication(Lcom/squareup/payment/offline/StoreAndForwardTask;Landroid/app/Application;)V

    .line 7550
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectBus(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/badbus/BadEventSink;)V

    .line 7551
    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectClock(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/util/Clock;)V

    .line 7552
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUserIdProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectCurrentUserId(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljavax/inject/Provider;)V

    .line 7553
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getDataFile()Ljava/io/File;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectDataDirectory(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljava/io/File;)V

    .line 7554
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSerialFileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectFileExecutor(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljava/util/concurrent/Executor;)V

    .line 7555
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/FileThreadEnforcer;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectFileThreadEnforcer(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/FileThreadEnforcer;)V

    .line 7556
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectForwardedPaymentsProvider(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;)V

    .line 7557
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realConnectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectConnectivityMonitor(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    .line 7558
    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectMainScheduler(Lcom/squareup/payment/offline/StoreAndForwardTask;Lrx/Scheduler;)V

    .line 7559
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectMainThread(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/thread/executor/MainThread;)V

    .line 7560
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->realBackgroundJobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectJobManager(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 7561
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectNotificationManager(Lcom/squareup/payment/offline/StoreAndForwardTask;Landroid/app/NotificationManager;)V

    .line 7562
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideResProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectRes(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/util/Res;)V

    .line 7563
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoreAndForwardBillServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/StoreAndForwardBillService;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectStoreAndForwardBillService(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/bills/StoreAndForwardBillService;)V

    .line 7564
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrossSessionStoreAndForwardTasksQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectCrossSessionQueue(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 7565
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideStoredPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectQueueCache(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/retrofit/QueueCache;)V

    .line 7566
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getTransactionLedgerManagerFactory()Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectTransactionLedgerManagerFactory(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;)V

    .line 7567
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getStoredPaymentStatusBarNotifier()Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectStoredPaymentNotifier(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/notifications/StoredPaymentNotifier;)V

    return-object p1
.end method

.method private injectUsbAttachedActivity(Lcom/squareup/hardware/UsbAttachedActivity;)Lcom/squareup/hardware/UsbAttachedActivity;
    .locals 1

    .line 7603
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1, v0}, Lcom/squareup/hardware/UsbAttachedActivity_MembersInjector;->injectUsbDiscoverer(Lcom/squareup/hardware/UsbAttachedActivity;Lcom/squareup/usb/UsbDiscoverer;)V

    .line 7604
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/hardware/UsbAttachedActivity_MembersInjector;->injectEventSink(Lcom/squareup/hardware/UsbAttachedActivity;Lcom/squareup/badbus/BadEventSink;)V

    return-object p1
.end method

.method private injectUsbDetachedReceiver(Lcom/squareup/hardware/UsbDetachedReceiver;)Lcom/squareup/hardware/UsbDetachedReceiver;
    .locals 1

    .line 7609
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1, v0}, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->injectUsbDiscoverer(Lcom/squareup/hardware/UsbDetachedReceiver;Lcom/squareup/usb/UsbDiscoverer;)V

    .line 7610
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->injectEventSink(Lcom/squareup/hardware/UsbDetachedReceiver;Lcom/squareup/badbus/BadEventSink;)V

    return-object p1
.end method


# virtual methods
.method public accessibilityManager()Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .line 7423
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAccessibilityManagerFactory;->provideAccessibilityManager(Landroid/app/Application;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    return-object v0
.end method

.method public apiActivityComponent()Lcom/squareup/ui/ApiActivity$Component;
    .locals 2

    .line 7507
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$AA_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$AA_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public application()Landroid/app/Application;
    .locals 1

    .line 7299
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method

.method public audioManager()Landroid/media/AudioManager;
    .locals 1

    .line 7427
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAudioManagerFactory;->provideAudioManager(Landroid/app/Application;)Landroid/media/AudioManager;

    move-result-object v0

    return-object v0
.end method

.method public audioRingBuffer()Lcom/squareup/squarewave/wav/AudioRingBuffer;
    .locals 1

    .line 7431
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->audioRingBufferProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/wav/AudioRingBuffer;

    return-object v0
.end method

.method public badBus()Lcom/squareup/badbus/BadBus;
    .locals 1

    .line 7403
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadBus;

    return-object v0
.end method

.method public badEventSink()Lcom/squareup/badbus/BadEventSink;
    .locals 1

    .line 7411
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    return-object v0
.end method

.method public bleDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 7451
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public bleExecutor()Landroid/os/Handler;
    .locals 1

    .line 7439
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    return-object v0
.end method

.method public bleThread()Ljava/lang/Thread;
    .locals 1

    .line 7443
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    return-object v0
.end method

.method public bleThreadEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 1

    .line 7447
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object v0
.end method

.method public bluetoothAdapter()Landroid/bluetooth/BluetoothAdapter;
    .locals 2

    .line 7371
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/BluetoothUtils;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getBluetoothManager()Landroid/bluetooth/BluetoothManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/blescan/BluetoothModule_ProvideAdapterFactory;->provideAdapter(Lcom/squareup/cardreader/BluetoothUtils;Landroid/bluetooth/BluetoothManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    return-object v0
.end method

.method public bluetoothStatusReceiver()Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;
    .locals 1

    .line 7367
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    return-object v0
.end method

.method public bluetoothUtils()Lcom/squareup/cardreader/BluetoothUtils;
    .locals 1

    .line 7339
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/BluetoothUtils;

    return-object v0
.end method

.method public cardReaderFactory()Lcom/squareup/cardreader/CardReaderFactory;
    .locals 1

    .line 7287
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderFactory;

    return-object v0
.end method

.method public cardReaderHub()Lcom/squareup/cardreader/CardReaderHub;
    .locals 1

    .line 7359
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub;

    return-object v0
.end method

.method public cardReaderListeners()Lcom/squareup/cardreader/CardReaderListeners;
    .locals 1

    .line 7283
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderListeners;

    return-object v0
.end method

.method public cardReaderPauseAndResumer()Lcom/squareup/cardreader/CardReaderPauseAndResumer;
    .locals 1

    .line 7355
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    return-object v0
.end method

.method public componentByActivity()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x7

    .line 7483
    invoke-static {v0}, Ldagger/internal/MapBuilder;->newMapBuilder(I)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/MainActivity;

    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule_ProvideMainActivityComponentFactory;->provideMainActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/ApiMainActivity;

    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory;->provideApiMainActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/loggedout/LoggedOutActivity;

    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule_ProvideLoggedOutActivityComponentFactory;->provideLoggedOutActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/onboarding/OnboardingActivity;

    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule_ProvideOnboardingActivityComponentFactory;->provideOnboardingActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/ApiActivity;

    invoke-static {}, Lcom/squareup/ActivityComponentModule_ProvideApiActivityComponentFactory;->provideApiActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/LocationActivity;

    invoke-static {}, Lcom/squareup/ActivityComponentModule_ProvideLocationActivityComponentFactory;->provideLocationActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/PaymentActivity;

    invoke-static {}, Lcom/squareup/PosAppComponent_Module_ProvidePaymentActivityComponentFactory;->providePaymentActivityComponent()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldagger/internal/MapBuilder;->put(Ljava/lang/Object;Ljava/lang/Object;)Ldagger/internal/MapBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ldagger/internal/MapBuilder;->build()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public componentFactory()Lcom/squareup/ui/component/ComponentFactory;
    .locals 1

    .line 7279
    invoke-static {}, Lcom/squareup/ui/component/PosComponentFactory_Factory;->newInstance()Lcom/squareup/ui/component/PosComponentFactory;

    move-result-object v0

    return-object v0
.end method

.method public crashnado()Lcom/squareup/crashnado/Crashnado;
    .locals 1

    .line 7295
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideCrashnadoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crashnado/Crashnado;

    return-object v0
.end method

.method public device()Lcom/squareup/util/Device;
    .locals 1

    .line 7275
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getRealDevice()Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    return-object v0
.end method

.method public exceptionHandlerDependencies()Lcom/squareup/log/RegisterExceptionHandler$Deps;
    .locals 3

    .line 7467
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getSetOfCrashAdditionalLogger()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getFeatureFlagsForLogs()Lcom/squareup/log/FeatureFlagsForLogs;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAnrChaperone()Lcom/squareup/anrchaperone/AnrChaperone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->newInstance(Ljava/util/Set;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/log/RegisterExceptionHandler$Deps;

    move-result-object v0

    return-object v0
.end method

.method public headset()Lcom/squareup/wavpool/swipe/Headset;
    .locals 1

    .line 7363
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Headset;

    return-object v0
.end method

.method public inject(Lcom/squareup/RegisterAppDelegate;)V
    .locals 0

    .line 7471
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectRegisterAppDelegate(Lcom/squareup/RegisterAppDelegate;)Lcom/squareup/RegisterAppDelegate;

    return-void
.end method

.method public inject(Lcom/squareup/analytics/RedirectTrackingReceiver;)V
    .locals 0

    .line 7495
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectRedirectTrackingReceiver(Lcom/squareup/analytics/RedirectTrackingReceiver;)Lcom/squareup/analytics/RedirectTrackingReceiver;

    return-void
.end method

.method public inject(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;)V
    .locals 0

    .line 7491
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectFirmwareUpdateNotificationService(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;)Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;

    return-void
.end method

.method public inject(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;)V
    .locals 0

    .line 7503
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectShareSheetSelectedAppReceiver(Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;)Lcom/squareup/checkoutlink/sharesheet/ShareSheetSelectedAppReceiver;

    return-void
.end method

.method public inject(Lcom/squareup/firebase/fcm/FcmService;)V
    .locals 0

    .line 7499
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectFcmService(Lcom/squareup/firebase/fcm/FcmService;)Lcom/squareup/firebase/fcm/FcmService;

    return-void
.end method

.method public inject(Lcom/squareup/hardware/UsbAttachedActivity;)V
    .locals 0

    .line 7475
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectUsbAttachedActivity(Lcom/squareup/hardware/UsbAttachedActivity;)Lcom/squareup/hardware/UsbAttachedActivity;

    return-void
.end method

.method public inject(Lcom/squareup/hardware/UsbDetachedReceiver;)V
    .locals 0

    .line 7479
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectUsbDetachedReceiver(Lcom/squareup/hardware/UsbDetachedReceiver;)Lcom/squareup/hardware/UsbDetachedReceiver;

    return-void
.end method

.method public inject(Lcom/squareup/payment/offline/StoreAndForwardTask;)V
    .locals 0

    .line 7463
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectStoreAndForwardTask(Lcom/squareup/payment/offline/StoreAndForwardTask;)Lcom/squareup/payment/offline/StoreAndForwardTask;

    return-void
.end method

.method public inject(Lcom/squareup/queue/QueueService$BootReceiver;)V
    .locals 0

    .line 7455
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectBootReceiver(Lcom/squareup/queue/QueueService$BootReceiver;)Lcom/squareup/queue/QueueService$BootReceiver;

    return-void
.end method

.method public inject(Lcom/squareup/queue/QueueService;)V
    .locals 0

    .line 7459
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent;->injectQueueService(Lcom/squareup/queue/QueueService;)Lcom/squareup/queue/QueueService;

    return-void
.end method

.method public isReaderSdk()Z
    .locals 1

    .line 7303
    invoke-static {}, Lcom/squareup/PosAppComponent$Module;->provideIsReaderSdk()Z

    move-result v0

    return v0
.end method

.method public lcrExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 7319
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideLCRExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public libraryLoader()Lcom/squareup/cardreader/loader/LibraryLoader;
    .locals 1

    .line 7375
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->bindRelinkerLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/loader/LibraryLoader;

    return-object v0
.end method

.method public bridge synthetic loggedInComponent()Lcom/squareup/CommonLoggedInComponent;
    .locals 1

    .line 5631
    invoke-virtual {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->loggedInComponent()Lcom/squareup/SposReleaseLoggedInComponent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic loggedInComponent()Lcom/squareup/RegisterAppDelegateLoggedInComponent;
    .locals 1

    .line 5631
    invoke-virtual {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->loggedInComponent()Lcom/squareup/SposReleaseLoggedInComponent;

    move-result-object v0

    return-object v0
.end method

.method public loggedInComponent()Lcom/squareup/SposReleaseLoggedInComponent;
    .locals 2

    .line 7517
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public bridge synthetic loggedOutActivityComponent()Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;
    .locals 1

    .line 5631
    invoke-virtual {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->loggedOutActivityComponent()Lcom/squareup/ui/root/SposReleaseLoggedOutActivityComponent;

    move-result-object v0

    return-object v0
.end method

.method public loggedOutActivityComponent()Lcom/squareup/ui/root/SposReleaseLoggedOutActivityComponent;
    .locals 2

    .line 7522
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public magSwipeFailureFilter()Lcom/squareup/cardreader/MagSwipeFailureFilter;
    .locals 1

    .line 7315
    invoke-static {}, Lcom/squareup/cardreader/MagSwipeFailureFilter_NeverFilterMagSwipeFailures_Factory;->newInstance()Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailures;

    move-result-object v0

    return-object v0
.end method

.method public mainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 7407
    invoke-static {}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideMainDispatcherFactory;->provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public mainThread()Lcom/squareup/thread/executor/MainThread;
    .locals 1

    .line 7335
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideAndroidMainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    return-object v0
.end method

.method public minesweeperTicket()Lcom/squareup/ms/MinesweeperTicket;
    .locals 1

    .line 7347
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideMinesweeperTicketProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MinesweeperTicket;

    return-object v0
.end method

.method public nativeLoggingEnabled()Z
    .locals 1

    .line 7291
    invoke-static {}, Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory;->provideNativeLoggingEnabled()Z

    move-result v0

    return v0
.end method

.method public paymentActivityComponent()Lcom/squareup/ui/PaymentActivity$Component;
    .locals 2

    .line 7512
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public persistentBundleManager()Lcom/squareup/persistentbundle/PersistentBundleManager;
    .locals 1

    .line 7487
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    return-object v0
.end method

.method public provideBleBondingBroadcastReceiver()Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;
    .locals 1

    .line 7383
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    return-object v0
.end method

.method public provideBleScanner()Lcom/squareup/cardreader/ble/BleScanner;
    .locals 1

    .line 7395
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBleScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleScanner;

    return-object v0
.end method

.method public provideBluetoothDiscoveryBroadcastReceiver()Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;
    .locals 1

    .line 7399
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideBluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    return-object v0
.end method

.method public provideHeadsetConnectionState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 2

    .line 7387
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->globalHeadsetModule:Lcom/squareup/cardreader/GlobalHeadsetModule;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideHeadsetProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/Headset;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionStateFactory;->provideHeadsetConnectionState(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object v0

    return-object v0
.end method

.method public provideHeadsetStateDispatcher()Lcom/squareup/cardreader/HeadsetStateDispatcher;
    .locals 1

    .line 7379
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/HeadsetStateDispatcher;

    return-object v0
.end method

.method public provideMinesweeper()Lcom/squareup/ms/Minesweeper;
    .locals 1

    .line 7391
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MsFactory;

    invoke-static {v0}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;->getMinesweeper(Lcom/squareup/ms/MsFactory;)Lcom/squareup/ms/Minesweeper;

    move-result-object v0

    return-object v0
.end method

.method public provideMsFactory()Lcom/squareup/ms/MsFactory;
    .locals 1

    .line 7343
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->getMsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MsFactory;

    return-object v0
.end method

.method public provideNativeBinaries()Lcom/squareup/cardreader/NativeBinaries;
    .locals 1

    .line 7323
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    invoke-static {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideNativeBinariesFactory;->provideNativeBinaries(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/NativeBinaries;

    move-result-object v0

    return-object v0
.end method

.method public provideSquidInterfaceScheduler()Lio/reactivex/Scheduler;
    .locals 1

    .line 7331
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    return-object v0
.end method

.method public provideTmnTimings()Lcom/squareup/tmn/TmnTimings;
    .locals 1

    .line 7327
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->tmnTimingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tmn/TmnTimings;

    return-object v0
.end method

.method public realCardReaderListeners()Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 1

    .line 7307
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->provideRealCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/RealCardReaderListeners;

    return-object v0
.end method

.method public running()Z
    .locals 2

    .line 7351
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->globalCardReaderModule:Lcom/squareup/cardreader/GlobalCardReaderModule;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRunningFactory;->provideRunning(Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public swipeBus()Lcom/squareup/wavpool/swipe/SwipeBus;
    .locals 1

    .line 7435
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->swipeBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/SwipeBus;

    return-object v0
.end method

.method public swipeEventLogger()Lcom/squareup/logging/SwipeEventLogger;
    .locals 1

    .line 7415
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent;->getAnalyticsSwipeEventLogger()Lcom/squareup/swipe/AnalyticsSwipeEventLogger;

    move-result-object v0

    return-object v0
.end method

.method public telephoneManager()Landroid/telephony/TelephonyManager;
    .locals 1

    .line 7419
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent;->appBootstrapModule:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideTelephonyManagerFactory;->provideTelephonyManager(Landroid/app/Application;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    return-object v0
.end method

.method public touchReporting()Lcom/squareup/securetouch/TouchReporting;
    .locals 1

    .line 7311
    invoke-static {}, Lcom/squareup/securetouch/NoTouchReportingModule_ProvideNoTouchReportingFactory;->provideNoTouchReporting()Lcom/squareup/securetouch/TouchReporting;

    move-result-object v0

    return-object v0
.end method
