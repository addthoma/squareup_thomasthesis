.class public final Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;
.super Ljava/lang/Object;
.source "CommonLoggedInModule_ProvideDiscountBundleFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory$InstanceHolder;->access$000()Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideDiscountBundleFactory()Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonLoggedInModule;->provideDiscountBundleFactory()Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;->provideDiscountBundleFactory()Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;->get()Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    move-result-object v0

    return-object v0
.end method
