.class public final enum Lcom/squareup/billhistory/model/TenderHistory$Type;
.super Ljava/lang/Enum;
.source "TenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/TenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/billhistory/model/TenderHistory$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum ADJUSTMENT:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum NO_SALE:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum OTHER:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum TAB:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

.field public static final enum ZERO_AMOUNT:Lcom/squareup/billhistory/model/TenderHistory$Type;


# instance fields
.field public final buyerFacingResourceId:I

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final refundableFromAccount:Z

.field public final resourceId:I

.field public final tenderClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation
.end field

.field public final uppercaseResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 56
    new-instance v8, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v4, Lcom/squareup/billhistory/model/CashTenderHistory;

    sget v6, Lcom/squareup/billhistory/R$string;->cash:I

    sget v7, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_tender_cash:I

    const-string v1, "CASH"

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v8, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 58
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v12, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_BACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v13, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    sget v15, Lcom/squareup/billhistory/R$string;->card:I

    sget v16, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_tender_card:I

    const-string v10, "CARD"

    const/4 v11, 0x1

    const/4 v14, 0x1

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 60
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_WALLET_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v5, Lcom/squareup/billhistory/model/TabTenderHistory;

    sget v7, Lcom/squareup/billhistory/R$string;->receipt_detail_paid_cardcase:I

    sget v8, Lcom/squareup/billhistory/R$string;->receipt_detail_paid_cardcase_uppercase:I

    const-string v2, "TAB"

    const/4 v3, 0x2

    const/4 v6, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->TAB:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 62
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v12, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v13, Lcom/squareup/billhistory/model/OtherTenderHistory;

    sget v15, Lcom/squareup/billhistory/R$string;->payment_type_other:I

    sget v16, Lcom/squareup/billhistory/R$string;->payment_type_other_uppercase:I

    const-string v10, "OTHER"

    const/4 v11, 0x3

    const/4 v14, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->OTHER:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 66
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v5, Lcom/squareup/billhistory/model/ZeroTenderHistory;

    sget v7, Lcom/squareup/billhistory/R$string;->payment_type_zero_amount:I

    sget v8, Lcom/squareup/billhistory/R$string;->payment_type_zero_amount_uppercase:I

    const-string v2, "ZERO_AMOUNT"

    const/4 v3, 0x4

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->ZERO_AMOUNT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 70
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v12, Lcom/squareup/glyph/GlyphTypeface$Glyph;->UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v13, Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    sget v15, Lcom/squareup/billhistory/R$string;->no_sale:I

    sget v16, Lcom/squareup/billhistory/R$string;->no_sale_uppercase:I

    const-string v10, "NO_SALE"

    const/4 v11, 0x5

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->NO_SALE:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 74
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v5, Lcom/squareup/billhistory/model/AdjustmentTenderHistory;

    sget v7, Lcom/squareup/billhistory/R$string;->tender_adjustment:I

    sget v8, Lcom/squareup/billhistory/R$string;->tender_adjustment_uppercase:I

    const-string v2, "ADJUSTMENT"

    const/4 v3, 0x6

    const/4 v6, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->ADJUSTMENT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 78
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v12, Lcom/squareup/glyph/GlyphTypeface$Glyph;->UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-class v13, Lcom/squareup/billhistory/model/UnknownTenderHistory;

    sget v15, Lcom/squareup/billhistory/R$string;->tender_unknown:I

    sget v16, Lcom/squareup/billhistory/R$string;->tender_unknown_uppercase:I

    const-string v10, "UNKNOWN"

    const/4 v11, 0x7

    const/4 v14, 0x1

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 55
    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->TAB:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->OTHER:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->ZERO_AMOUNT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->NO_SALE:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->ADJUSTMENT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->$VALUES:[Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZII)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;ZII)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move v8, p6

    .line 95
    invoke-direct/range {v0 .. v8}, Lcom/squareup/billhistory/model/TenderHistory$Type;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZIII)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/Class;ZIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;ZIII)V"
        }
    .end annotation

    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 101
    iput-object p3, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 102
    iput-object p4, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->tenderClass:Ljava/lang/Class;

    .line 103
    iput-boolean p5, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->refundableFromAccount:Z

    .line 104
    iput p6, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->resourceId:I

    .line 105
    iput p7, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->uppercaseResourceId:I

    .line 106
    iput p8, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->buyerFacingResourceId:I

    return-void
.end method

.method static synthetic access$1500(Lcom/squareup/billhistory/model/TenderHistory$Type;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/billhistory/model/TenderHistory$Type;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Type;
    .locals 1

    .line 55
    const-class v0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/billhistory/model/TenderHistory$Type;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->$VALUES:[Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-virtual {v0}, [Lcom/squareup/billhistory/model/TenderHistory$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-object v0
.end method
