.class public Lcom/squareup/billhistory/model/NoSaleTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "NoSaleTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;Lcom/squareup/billhistory/model/NoSaleTenderHistory$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory;-><init>(Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/NoSaleTenderHistory;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/NoSaleTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getUnbrandedTenderGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/billhistory/model/NoSaleTenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/util/ProtoGlyphs;->cash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method
