.class public Lcom/squareup/billhistory/Bills;
.super Ljava/lang/Object;
.source "Bills.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;,
        Lcom/squareup/billhistory/Bills$ItemizationAdapter;,
        Lcom/squareup/billhistory/Bills$BillItemAdapter;,
        Lcom/squareup/billhistory/Bills$BillIdChanged;
    }
.end annotation


# static fields
.field public static final DOWN_CONVERTED_SUFFIX:Ljava/lang/String; = "-downconverted"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static accumulateTenderState(Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/protos/client/bills/Tender$State;
    .locals 1

    if-eqz p0, :cond_4

    .line 630
    sget-object v0, Lcom/squareup/billhistory/Bills$1;->$SwitchMap$com$squareup$protos$client$bills$Tender$State:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$State;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    goto :goto_0

    .line 651
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    if-ne p1, p0, :cond_4

    .line 652
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 643
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-eq p1, p0, :cond_4

    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_CUSTOMER_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    if-eq p1, p0, :cond_4

    .line 646
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 637
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-eq p1, p0, :cond_4

    .line 638
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_CUSTOMER_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 634
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    :cond_4
    :goto_0
    return-object p1
.end method

.method private static addOrConsolidate(Ljava/util/LinkedHashMap;Lcom/squareup/protos/client/bills/DiscountLineItem;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ")V"
        }
    .end annotation

    .line 494
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 495
    invoke-virtual {p0, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiscountLineItem;

    if-eqz v0, :cond_0

    .line 498
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    .line 499
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 500
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 501
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v3, v5

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 500
    invoke-static {v3, v4, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    move-result-object v0

    .line 504
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    move-result-object v0

    .line 499
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 507
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static addOrConsolidate(Ljava/util/LinkedHashMap;Lcom/squareup/protos/client/bills/FeeLineItem;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ")V"
        }
    .end annotation

    .line 520
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 521
    invoke-virtual {p0, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/FeeLineItem;

    if-eqz v0, :cond_0

    .line 524
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    .line 525
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 526
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, v0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 527
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v3, v5

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 526
    invoke-static {v3, v4, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    move-result-object v0

    .line 525
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v0

    .line 531
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 533
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static addTenders(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 587
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 588
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Tender;

    .line 589
    iget-object v3, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v2, v3}, Lcom/squareup/billhistory/model/TenderHistory;->fromHistoricalTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static attributeOrderIfPossible(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/CreatorDetails;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 552
    iget-object v0, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v0, v0, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order;->setEmployeeToken(Ljava/lang/String;)V

    .line 554
    iget-object v0, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v0, v0, Lcom/squareup/protos/client/Employee;->first_name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order;->setEmployeeFirstName(Ljava/lang/String;)V

    .line 555
    iget-object p1, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object p1, p1, Lcom/squareup/protos/client/Employee;->last_name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->setEmployeeLastName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static calculatePaymentState(Ljava/util/List;)Lcom/squareup/protos/client/bills/Tender$State;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Tender$State;"
        }
    .end annotation

    .line 619
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    .line 620
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 621
    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-static {v1, v0}, Lcom/squareup/billhistory/Bills;->accumulateTenderState(Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/protos/client/bills/Tender$State;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static createBillNote(Ljava/util/List;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/Itemization;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 95
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/server/payment/Itemization;

    invoke-interface {p0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/server/payment/Itemization;

    goto :goto_0

    :cond_0
    move-object p0, v0

    .line 97
    :goto_0
    new-instance v1, Lcom/squareup/billhistory/Bills$ItemizationAdapter;

    invoke-direct {v1, v0}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;-><init>(Lcom/squareup/billhistory/Bills$1;)V

    invoke-static {p0, v1, p1}, Lcom/squareup/billhistory/Bills;->createBillNote([Ljava/lang/Object;Lcom/squareup/billhistory/Bills$BillItemAdapter;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object p0

    return-object p0
.end method

.method private static createBillNote([Ljava/lang/Object;Lcom/squareup/billhistory/Bills$BillItemAdapter;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Lcom/squareup/billhistory/Bills$BillItemAdapter<",
            "TT;>;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;"
        }
    .end annotation

    if-eqz p0, :cond_3

    .line 128
    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_1

    .line 132
    :cond_0
    array-length v0, p0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    aget-object v0, p0, v2

    .line 133
    invoke-interface {p1, v0}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->isCustomAmount(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    aget-object v0, p0, v2

    .line 134
    invoke-interface {p1, v0}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getNotes(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    aget-object v0, p0, v2

    .line 135
    invoke-interface {p1, v0}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    sget-object p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->EMPTY:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    return-object p0

    .line 139
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 140
    array-length v1, p0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p0, v2

    .line 141
    new-instance v12, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;

    .line 142
    invoke-interface {p1, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 143
    invoke-interface {p1, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getNotes(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 144
    invoke-interface {p1, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getQuantity(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object v7

    .line 145
    invoke-interface {p1, p2, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getQuantityPrecision(Lcom/squareup/util/Res;Ljava/lang/Object;)I

    move-result v8

    .line 146
    invoke-interface {p1, p2, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->isUnitPriced(Lcom/squareup/util/Res;Ljava/lang/Object;)Z

    move-result v9

    .line 147
    invoke-interface {p1, p2, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->getUnitAbbreviation(Lcom/squareup/util/Res;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 148
    invoke-interface {p1, v3}, Lcom/squareup/billhistory/Bills$BillItemAdapter;->isCustomAmount(Ljava/lang/Object;)Z

    move-result v11

    move-object v4, v12

    invoke-direct/range {v4 .. v11}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;IZLjava/lang/String;Z)V

    .line 141
    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 150
    :cond_2
    new-instance p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    invoke-direct {p0, p2, v0}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;-><init>(Lcom/squareup/util/Res;Ljava/util/List;)V

    return-object p0

    .line 129
    :cond_3
    :goto_1
    sget-object p0, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->EMPTY:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    return-object p0
.end method

.method public static createBillNoteFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 104
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    .line 106
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Itemization;

    .line 105
    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/protos/client/bills/Itemization;

    goto :goto_0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 108
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/billhistory/Bills;->itemizationsFromRefund(Ljava/util/List;)[Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p0

    goto :goto_0

    :cond_1
    move-object p0, v1

    .line 110
    :goto_0
    new-instance v0, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;

    invoke-direct {v0, v1}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;-><init>(Lcom/squareup/billhistory/Bills$1;)V

    invoke-static {p0, v0, p1}, Lcom/squareup/billhistory/Bills;->createBillNote([Ljava/lang/Object;Lcom/squareup/billhistory/Bills$BillItemAdapter;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object p0

    return-object p0
.end method

.method public static formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 665
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 666
    sget p0, Lcom/squareup/billhistory/R$string;->no_sale:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 667
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v0, :cond_2

    .line 668
    sget p0, Lcom/squareup/billhistory/R$string;->voided_sale:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 669
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/billhistory/model/BillHistory;->isRefund:Z

    const-string v1, "amount"

    if-eqz v0, :cond_3

    .line 670
    sget v0, Lcom/squareup/billhistory/R$string;->refund_caption:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->refundAmount:Lcom/squareup/protos/common/Money;

    .line 671
    invoke-interface {p2, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 672
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 674
    :cond_3
    sget v0, Lcom/squareup/billhistory/R$string;->payment_caption:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    .line 675
    invoke-interface {p2, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 676
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 684
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v0

    const-string v1, "time"

    const-string v2, "date"

    if-eqz v0, :cond_1

    .line 685
    sget p2, Lcom/squareup/billhistory/R$string;->no_sale_on_date_time_format:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 686
    invoke-virtual {p3, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 687
    invoke-virtual {p4, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 688
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0

    .line 689
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v0, :cond_2

    .line 690
    sget p2, Lcom/squareup/billhistory/R$string;->voided_sale_on_date_time_format:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 691
    invoke-virtual {p3, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 692
    invoke-virtual {p4, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 693
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0

    .line 694
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/billhistory/model/BillHistory;->isRefund:Z

    const-string v3, "amount"

    if-eqz v0, :cond_3

    .line 695
    sget v0, Lcom/squareup/billhistory/R$string;->refund_caption_on_date_time_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->refundAmount:Lcom/squareup/protos/common/Money;

    .line 696
    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 697
    invoke-virtual {p3, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 698
    invoke-virtual {p4, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 699
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 701
    :cond_3
    sget v0, Lcom/squareup/billhistory/R$string;->payment_caption_on_date_time_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    .line 702
    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, v3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 703
    invoke-virtual {p3, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    .line 704
    invoke-virtual {p4, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 705
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private static getCompletedAt(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Date;
    .locals 1

    .line 336
    :try_start_0
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string v0, "Cannot parse bill completion date"

    .line 338
    invoke-static {p0, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 339
    new-instance p0, Ljava/util/Date;

    invoke-direct {p0}, Ljava/util/Date;-><init>()V

    return-object p0
.end method

.method private static getCreatorDetails(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/CreatorDetails;
    .locals 2

    .line 539
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    .line 541
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Tender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    goto :goto_0

    .line 542
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 544
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Refund;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Refund;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private static getCreatorName(Lcom/squareup/protos/client/CreatorDetails;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    if-eqz v0, :cond_0

    .line 562
    iget-object p0, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    iget-object p0, p0, Lcom/squareup/protos/client/MobileStaff;->full_name:Ljava/lang/String;

    return-object p0

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_1

    .line 566
    iget-object p0, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object p0, p0, Lcom/squareup/protos/client/Employee;->read_only_full_name:Ljava/lang/String;

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getCreatorName(Lcom/squareup/protos/client/bills/Bill;)Ljava/lang/String;
    .locals 0

    .line 573
    invoke-static {p0}, Lcom/squareup/billhistory/Bills;->getCreatorDetails(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/billhistory/Bills;->getCreatorName(Lcom/squareup/protos/client/CreatorDetails;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getElectronicSignature(Lcom/squareup/protos/client/bills/Bill;)Ljava/lang/String;
    .locals 1

    .line 580
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    if-eqz v0, :cond_0

    .line 581
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getFirstSaleBillForFamily(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lcom/squareup/protos/client/bills/Bill;
    .locals 4

    .line 323
    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Bill;

    if-eqz v0, :cond_2

    .line 325
    invoke-static {v1}, Lcom/squareup/billhistory/Bills;->getCompletedAt(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Date;

    move-result-object v2

    invoke-static {v0}, Lcom/squareup/billhistory/Bills;->getCompletedAt(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v2

    if-gez v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_0

    .line 327
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private static getReceiptNumbers(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 819
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 820
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Tender;

    .line 821
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static getRelatedBills(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 358
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 359
    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Bill;

    .line 363
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 364
    invoke-static {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->fromBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 366
    :cond_0
    invoke-static {v1}, Lcom/squareup/billhistory/Bills;->getUniqueRefunds(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 369
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static getUniqueRefunds(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 373
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 374
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Refund;

    .line 376
    invoke-static {v2, p0}, Lcom/squareup/server/payment/RelatedBillHistory;->fromRefund(Lcom/squareup/protos/client/bills/Refund;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v3

    .line 380
    iget-object v4, v2, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 381
    iget-object v4, v2, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    .line 382
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 383
    invoke-virtual {v4, v3}, Lcom/squareup/server/payment/RelatedBillHistory;->merge(Lcom/squareup/server/payment/RelatedBillHistory;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v3

    .line 385
    :cond_0
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 387
    :cond_1
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    return-object p0
.end method

.method private static getV1Refunds(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 347
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/RefundV1;

    .line 349
    iget-object v3, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v2, v3}, Lcom/squareup/server/payment/RelatedBillHistory;->fromRefundV1(Lcom/squareup/protos/client/bills/RefundV1;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static idsMatch(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    if-nez p1, :cond_0

    goto :goto_0

    .line 607
    :cond_0
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    iget-object v2, p1, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    if-eq v1, v2, :cond_1

    return v0

    .line 610
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    .line 611
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return v2

    .line 614
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    if-eqz p0, :cond_3

    .line 615
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    :cond_3
    :goto_0
    return v0
.end method

.method private static itemizationsFromRefund(Ljava/util/List;)[Lcom/squareup/protos/client/bills/Itemization;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;)[",
            "Lcom/squareup/protos/client/bills/Itemization;"
        }
    .end annotation

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 116
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 117
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 118
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 123
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    new-array p0, p0, [Lcom/squareup/protos/client/bills/Itemization;

    .line 122
    invoke-interface {v0, p0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/protos/client/bills/Itemization;

    return-object p0
.end method

.method private static refundsToRefundHistory(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 3

    .line 395
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Must have at least one refund."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 403
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Refund;

    invoke-static {v0, p1}, Lcom/squareup/server/payment/RelatedBillHistory;->fromRefund(Lcom/squareup/protos/client/bills/Refund;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    .line 405
    invoke-static {v1, p0}, Lcom/squareup/billhistory/Bills;->returnLineItemsWithDisplayDetails(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    .line 404
    invoke-virtual {v0, p0}, Lcom/squareup/server/payment/RelatedBillHistory;->copyWithReturnLineItems(Ljava/util/List;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object p0

    .line 410
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Refund;

    .line 411
    invoke-virtual {p0}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_tender_server_token:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method private static returnLineItemsWithDisplayDetails(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;"
        }
    .end annotation

    .line 428
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 429
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Itemization;

    .line 430
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 433
    :cond_0
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 434
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 435
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 437
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 438
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 439
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 440
    iget-object v6, v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 441
    iget-object v8, v7, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 442
    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    .line 444
    iget-object v9, v7, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v9, v9, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v9, v9, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v9, :cond_3

    .line 450
    iget-object v10, v9, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 451
    iget-object v12, v11, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v12, v12, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object v13, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne v12, v13, :cond_1

    .line 453
    invoke-static {p1, v11}, Lcom/squareup/billhistory/Bills;->addOrConsolidate(Ljava/util/LinkedHashMap;Lcom/squareup/protos/client/bills/DiscountLineItem;)V

    goto :goto_3

    .line 455
    :cond_1
    invoke-static {v1, v11}, Lcom/squareup/billhistory/Bills;->addOrConsolidate(Ljava/util/LinkedHashMap;Lcom/squareup/protos/client/bills/DiscountLineItem;)V

    goto :goto_3

    .line 461
    :cond_2
    iget-object v9, v9, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 462
    invoke-static {v2, v10}, Lcom/squareup/billhistory/Bills;->addOrConsolidate(Ljava/util/LinkedHashMap;Lcom/squareup/protos/client/bills/FeeLineItem;)V

    goto :goto_4

    .line 465
    :cond_3
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    move-result-object v9

    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    .line 466
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v7

    .line 467
    invoke-virtual {v7, v8}, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v7

    .line 468
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v7

    .line 466
    invoke-virtual {v9, v7}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    move-result-object v7

    .line 469
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object v7

    .line 465
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 472
    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 473
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 474
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 475
    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 476
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v4

    .line 477
    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v4

    .line 478
    invoke-virtual {v4, v6}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v4

    .line 479
    invoke-virtual {v4, v7}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v4

    .line 480
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object v4

    .line 476
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_5
    return-object v3
.end method

.method public static toBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory;
    .locals 0

    .line 160
    invoke-static {p0, p1, p2}, Lcom/squareup/billhistory/Bills;->toRefundV1BillBuilder(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 162
    invoke-static {p0}, Lcom/squareup/billhistory/Bills;->getV1Refunds(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setRelatedBills(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    .line 163
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p0

    return-object p0
.end method

.method public static toBill(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory;
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    .line 176
    invoke-static {p0}, Lcom/squareup/billhistory/Bills;->getFirstSaleBillForFamily(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lcom/squareup/protos/client/bills/Bill;

    move-result-object v1

    invoke-static {v0, v1, p1, p2, p3}, Lcom/squareup/billhistory/Bills;->toBillBuilder(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 180
    iget-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x1

    .line 181
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setIsRefund(Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 184
    iget-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 185
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 187
    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 191
    :cond_1
    invoke-static {p0}, Lcom/squareup/billhistory/Bills;->getRelatedBills(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setRelatedBills(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    .line 192
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p0

    return-object p0
.end method

.method static toBillBuilder(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 254
    invoke-static/range {p0 .. p0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v3

    .line 255
    invoke-static/range {p0 .. p0}, Lcom/squareup/billhistory/Bills;->getCompletedAt(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Date;

    move-result-object v8

    .line 256
    iget-object v4, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v4, v2}, Lcom/squareup/billhistory/Bills;->createBillNoteFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v9

    .line 257
    invoke-static/range {p0 .. p0}, Lcom/squareup/billhistory/Bills;->getCreatorDetails(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v4

    .line 258
    invoke-static {v4}, Lcom/squareup/billhistory/Bills;->getCreatorName(Lcom/squareup/protos/client/CreatorDetails;)Ljava/lang/String;

    move-result-object v12

    .line 260
    iget-object v13, v0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 264
    iget-object v5, v0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    const/4 v6, 0x0

    const-string v7, "Sale bill must have a cart."

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 265
    iget-object v10, v1, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 266
    invoke-static/range {p1 .. p4}, Lcom/squareup/payment/OrderSnapshot;->forBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/payment/Order;

    move-result-object v5

    .line 267
    invoke-static {v5, v4}, Lcom/squareup/billhistory/Bills;->attributeOrderIfPossible(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/CreatorDetails;)V

    .line 268
    iget-object v0, v1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v0, v7}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/protos/client/bills/Cart;

    .line 269
    iget-object v0, v1, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/billhistory/Bills;->getReceiptNumbers(Ljava/util/List;)Ljava/util/List;

    move-result-object v15

    .line 270
    invoke-static/range {p1 .. p1}, Lcom/squareup/billhistory/Bills;->addTenders(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;

    move-result-object v16

    .line 274
    iget-object v0, v1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    move-object v6, v0

    .line 277
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/squareup/billhistory/Bills;->getElectronicSignature(Lcom/squareup/protos/client/bills/Bill;)Ljava/lang/String;

    move-result-object v7

    .line 278
    new-instance v4, Lcom/squareup/billhistory/model/BillHistory$Builder;

    const/16 v17, 0x0

    move-object v0, v4

    move-object v1, v3

    move-object v2, v10

    move-object/from16 v3, v16

    move-object v14, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v8

    move-object v8, v7

    move-object v7, v9

    move-object v9, v8

    move-object v8, v15

    move-object v15, v9

    move/from16 v9, v17

    invoke-direct/range {v0 .. v9}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    .line 280
    invoke-static/range {v16 .. v16}, Lcom/squareup/billhistory/Bills;->calculatePaymentState(Ljava/util/List;)Lcom/squareup/protos/client/bills/Tender$State;

    move-result-object v0

    invoke-virtual {v14, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 281
    invoke-virtual {v0, v15}, Lcom/squareup/billhistory/model/BillHistory$Builder;->signature(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 282
    invoke-virtual {v0, v12}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCreatorName(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 283
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setIsRefund(Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 284
    invoke-virtual {v0, v11}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 285
    invoke-virtual {v0, v13}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setLoyaltyDetails(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 286
    invoke-virtual {v0, v10}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setFirstBillId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    return-object v0

    .line 290
    :cond_1
    iget-object v5, v0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    move/from16 v10, p3

    move/from16 v11, p4

    .line 291
    invoke-static {v0, v2, v10, v11}, Lcom/squareup/payment/OrderSnapshot;->forBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/payment/Order;

    move-result-object v14

    .line 292
    invoke-static/range {p1 .. p4}, Lcom/squareup/payment/OrderSnapshot;->forBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/payment/Order;

    move-result-object v15

    .line 293
    invoke-static {v14, v4}, Lcom/squareup/billhistory/Bills;->attributeOrderIfPossible(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/CreatorDetails;)V

    .line 294
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v2, v7}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/squareup/protos/client/bills/Cart;

    .line 295
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v2, v7}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/squareup/protos/client/bills/Cart;

    .line 296
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-static {v2}, Lcom/squareup/billhistory/Bills;->getReceiptNumbers(Ljava/util/List;)Ljava/util/List;

    move-result-object v16

    .line 297
    invoke-static/range {p0 .. p0}, Lcom/squareup/billhistory/Bills;->addTenders(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;

    move-result-object v17

    .line 298
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    move-object v7, v2

    goto :goto_0

    :cond_2
    move-object v7, v6

    .line 301
    :goto_0
    invoke-static/range {p0 .. p0}, Lcom/squareup/billhistory/Bills;->getElectronicSignature(Lcom/squareup/protos/client/bills/Bill;)Ljava/lang/String;

    move-result-object v0

    .line 302
    new-instance v6, Lcom/squareup/billhistory/model/BillHistory$Builder;

    const/16 v18, 0x0

    move-object v2, v6

    move-object v4, v5

    move-object/from16 v5, v17

    move-object/from16 v19, v6

    move-object v6, v14

    move-object v14, v10

    move-object/from16 v10, v16

    move-object/from16 v16, v14

    move-object v14, v11

    move/from16 v11, v18

    invoke-direct/range {v2 .. v11}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    .line 304
    invoke-static/range {v17 .. v17}, Lcom/squareup/billhistory/Bills;->calculatePaymentState(Ljava/util/List;)Lcom/squareup/protos/client/bills/Tender$State;

    move-result-object v2

    move-object/from16 v3, v19

    invoke-virtual {v3, v2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v2

    .line 305
    invoke-virtual {v2, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->signature(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 306
    invoke-virtual {v0, v12}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCreatorName(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 307
    invoke-virtual {v0, v2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setIsRefund(Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 308
    invoke-virtual {v0, v14}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 309
    invoke-virtual {v0, v13}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setLoyaltyDetails(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 310
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setFirstBillId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 311
    invoke-virtual {v0, v15}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setFirstOrder(Lcom/squareup/payment/Order;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    move-object/from16 v2, v16

    .line 312
    invoke-virtual {v0, v2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setFirstCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static toRefundBill(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 6

    .line 203
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "Must have refund."

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 204
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Refund;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Refund;->write_only_linked_bill_server_token:Ljava/lang/String;

    .line 205
    invoke-virtual {p0, v0}, Lcom/squareup/billhistory/model/BillHistory;->containsBillWithId(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Source bill family must contain the refund\'s source bill."

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 209
    invoke-static {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->forBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    .line 210
    invoke-static {p1}, Lcom/squareup/billhistory/Bills;->getCompletedAt(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Date;

    move-result-object v2

    .line 211
    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v3, p2}, Lcom/squareup/billhistory/Bills;->createBillNoteFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object p2

    .line 212
    invoke-static {p1}, Lcom/squareup/billhistory/Bills;->getCreatorName(Lcom/squareup/protos/client/bills/Bill;)Ljava/lang/String;

    move-result-object v3

    .line 215
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 216
    invoke-static {p0, p1}, Lcom/squareup/billhistory/Bills;->refundsToRefundHistory(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    new-instance v5, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v5, p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 219
    invoke-virtual {v5, v0, v2, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->asNewBill(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    .line 220
    invoke-virtual {p0, v3}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCreatorName(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    .line 221
    invoke-virtual {p0, v4}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setRelatedBills(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    .line 222
    invoke-virtual {p0, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setIsRefund(Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    .line 224
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p0

    return-object p0
.end method

.method private static toRefundV1BillBuilder(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 236
    invoke-static {p0, p0, p1, p2, v0}, Lcom/squareup/billhistory/Bills;->toBillBuilder(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p0

    return-object p0
.end method
