.class public abstract Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "BuyerLanguageSelectionViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00040\u0003\"\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "_bindings",
        "",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public varargs constructor <init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "**>;)V"
        }
    .end annotation

    const-string v0, "_bindings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    invoke-direct {p0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
