.class public final Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;
.super Ljava/lang/Object;
.source "AppletsDrawerActionBarNavigationHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;)",
            "Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/applet/AppletsDrawerRunner;)Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;

    invoke-direct {v0, p0}, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;-><init>(Lcom/squareup/applet/AppletsDrawerRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerRunner;

    invoke-static {v0}, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;->newInstance(Lcom/squareup/applet/AppletsDrawerRunner;)Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper_Factory;->get()Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;

    move-result-object v0

    return-object v0
.end method
