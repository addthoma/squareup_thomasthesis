.class public final Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;
.super Ljava/lang/Object;
.source "AppletsDrawerLayout_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/applet/AppletsDrawerLayout;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletsDrawerPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;->appletsDrawerPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/applet/AppletsDrawerLayout;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAppletsDrawerPresenter(Lcom/squareup/applet/AppletsDrawerLayout;Lcom/squareup/applet/AppletsDrawerPresenter;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout;->appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/applet/AppletsDrawerLayout;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;->appletsDrawerPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;->injectAppletsDrawerPresenter(Lcom/squareup/applet/AppletsDrawerLayout;Lcom/squareup/applet/AppletsDrawerPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/applet/AppletsDrawerLayout;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletsDrawerLayout_MembersInjector;->injectMembers(Lcom/squareup/applet/AppletsDrawerLayout;)V

    return-void
.end method
