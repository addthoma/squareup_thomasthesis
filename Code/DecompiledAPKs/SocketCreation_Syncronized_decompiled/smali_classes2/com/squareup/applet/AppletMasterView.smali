.class public Lcom/squareup/applet/AppletMasterView;
.super Landroid/widget/LinearLayout;
.source "AppletMasterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/applet/AppletMasterView$Component;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field badgePresenter:Lcom/squareup/applet/BadgePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/applet/AppletMasterViewPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const-class p2, Lcom/squareup/applet/AppletMasterView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletMasterView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/applet/AppletMasterView$Component;->inject(Lcom/squareup/applet/AppletMasterView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 55
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/applet/AppletMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 40
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/applet/AppletMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView;->presenter:Lcom/squareup/applet/AppletMasterViewPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/AppletMasterViewPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/applet/AppletMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView;->presenter:Lcom/squareup/applet/AppletMasterViewPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/AppletMasterViewPresenter;->dropView(Ljava/lang/Object;)V

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 34
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 35
    invoke-direct {p0}, Lcom/squareup/applet/AppletMasterView;->bindViews()V

    return-void
.end method
