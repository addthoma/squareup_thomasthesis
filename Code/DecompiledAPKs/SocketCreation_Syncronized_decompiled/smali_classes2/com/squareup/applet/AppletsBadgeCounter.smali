.class public Lcom/squareup/applet/AppletsBadgeCounter;
.super Ljava/lang/Object;
.source "AppletsBadgeCounter.java"


# static fields
.field public static final MAX_DISPLAYED_BADGE_COUNT_RATE_MS:I = 0x64

.field private static final MAX_DISPLAYED_BADGE_COUNT_VALUE:I = 0x63


# instance fields
.field private final sampledTotalBadge:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/applet/Applets;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)V
    .locals 3
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 36
    invoke-interface {p1}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/applet/Applet;

    .line 37
    invoke-virtual {p2}, Lcom/squareup/applet/Applet;->isNotificationCenterApplet()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 38
    invoke-virtual {p2}, Lcom/squareup/applet/Applet;->badge()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/applet/AppletsBadgeCounter;->sampledTotalBadge:Lio/reactivex/Observable;

    return-void

    .line 45
    :cond_1
    sget-object p1, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/applet/AppletsBadgeCounter;->sampledTotalBadge:Lio/reactivex/Observable;

    return-void

    .line 50
    :cond_2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 52
    invoke-interface {p1}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/Applet;

    .line 56
    invoke-virtual {v0}, Lcom/squareup/applet/Applet;->badge()Lio/reactivex/Observable;

    move-result-object v1

    .line 57
    invoke-virtual {v0}, Lcom/squareup/applet/Applet;->visibility()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v2, Lcom/squareup/applet/-$$Lambda$AppletsBadgeCounter$Rbkh5rDYuF1nzk-ziJ72EnsI_t0;->INSTANCE:Lcom/squareup/applet/-$$Lambda$AppletsBadgeCounter$Rbkh5rDYuF1nzk-ziJ72EnsI_t0;

    .line 55
    invoke-static {v1, v0, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    :cond_3
    sget-object p1, Lcom/squareup/applet/-$$Lambda$AppletsBadgeCounter$WAaiR1EAFPahHsOWdOecE6xs8xM;->INSTANCE:Lcom/squareup/applet/-$$Lambda$AppletsBadgeCounter$WAaiR1EAFPahHsOWdOecE6xs8xM;

    invoke-static {p2, p1}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x64

    .line 90
    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 93
    invoke-static {v0, v1, p2, p3}, Lcom/squareup/util/rx2/Rx2TransformersKt;->adaptiveSample(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object p2

    .line 92
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/applet/AppletsBadgeCounter;->sampledTotalBadge:Lio/reactivex/Observable;

    return-void
.end method

.method public static getBadgeRowValueText(Lcom/squareup/util/Res;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 113
    sget p1, Lcom/squareup/pos/container/R$string;->new_message_one:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    if-le p1, v0, :cond_1

    .line 115
    sget v0, Lcom/squareup/pos/container/R$string;->new_message_two_or_more:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "count"

    .line 116
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 117
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 118
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method public static getBadgeText(I)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x63

    .line 104
    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/applet/Applet$Badge;Ljava/lang/Boolean;)Lcom/squareup/applet/Applet$Badge;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$new$1([Ljava/lang/Object;)Lcom/squareup/applet/Applet$Badge;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/squareup/applet/Applet$Badge$Priority;->NORMAL:Lcom/squareup/applet/Applet$Badge$Priority;

    .line 72
    array-length v1, p0

    const/4 v2, 0x0

    move-object v3, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v4, p0, v2

    .line 74
    instance-of v5, v4, Lcom/squareup/applet/Applet$Badge$Visible;

    if-eqz v5, :cond_0

    .line 75
    check-cast v4, Lcom/squareup/applet/Applet$Badge$Visible;

    .line 76
    invoke-virtual {v4}, Lcom/squareup/applet/Applet$Badge$Visible;->getCount()I

    move-result v5

    add-int/2addr v0, v5

    .line 77
    invoke-virtual {v4}, Lcom/squareup/applet/Applet$Badge$Visible;->getPriority()Lcom/squareup/applet/Applet$Badge$Priority;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/comparisons/ComparisonsKt;->maxOf(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v3

    check-cast v3, Lcom/squareup/applet/Applet$Badge$Priority;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    .line 81
    sget-object p0, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    return-object p0

    .line 83
    :cond_2
    new-instance p0, Lcom/squareup/applet/Applet$Badge$Visible;

    invoke-static {v0}, Lcom/squareup/applet/AppletsBadgeCounter;->getBadgeText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/squareup/applet/Applet$Badge$Visible;-><init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    return-object p0
.end method


# virtual methods
.method public sampledTotalBadge()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/applet/AppletsBadgeCounter;->sampledTotalBadge:Lio/reactivex/Observable;

    return-object v0
.end method
