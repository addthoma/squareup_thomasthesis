.class final Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;
.super Ljava/lang/Object;
.source "AppletsDrawerPresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/applet/Applet$Badge;",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/applet/Applet$Badge;",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 44
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/applet/Applet$Badge;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/Applet$Badge;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 101
    invoke-static {v1, v2, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 104
    sget-object v1, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$item:Lcom/squareup/applet/AppletsDrawerItem;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerItem;->hideBadge()V

    goto :goto_0

    .line 105
    :cond_0
    instance-of v1, v0, Lcom/squareup/applet/Applet$Badge$Visible;

    if-eqz v1, :cond_4

    if-eqz p1, :cond_3

    .line 107
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$applet:Lcom/squareup/applet/Applet;

    invoke-virtual {p1}, Lcom/squareup/applet/Applet;->isNotificationCenterApplet()Z

    move-result p1

    if-eqz p1, :cond_1

    move-object p1, v0

    check-cast p1, Lcom/squareup/applet/Applet$Badge$Visible;

    invoke-virtual {p1}, Lcom/squareup/applet/Applet$Badge$Visible;->getPriority()Lcom/squareup/applet/Applet$Badge$Priority;

    move-result-object p1

    sget-object v1, Lcom/squareup/applet/Applet$Badge$Priority;->FATAL:Lcom/squareup/applet/Applet$Badge$Priority;

    if-ne p1, v1, :cond_1

    .line 108
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$item:Lcom/squareup/applet/AppletsDrawerItem;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerItem;->showNotificationCenterWarningBadge()V

    goto :goto_0

    .line 109
    :cond_1
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$applet:Lcom/squareup/applet/Applet;

    invoke-virtual {p1}, Lcom/squareup/applet/Applet;->isNotificationCenterApplet()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 110
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$item:Lcom/squareup/applet/AppletsDrawerItem;

    check-cast v0, Lcom/squareup/applet/Applet$Badge$Visible;

    invoke-virtual {v0}, Lcom/squareup/applet/Applet$Badge$Visible;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/applet/AppletsDrawerItem;->showNotificationCenterBadge(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 112
    :cond_2
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$item:Lcom/squareup/applet/AppletsDrawerItem;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerItem;->hideBadge()V

    goto :goto_0

    .line 115
    :cond_3
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2$1;->this$0:Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;

    iget-object p1, p1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$2;->$item:Lcom/squareup/applet/AppletsDrawerItem;

    check-cast v0, Lcom/squareup/applet/Applet$Badge$Visible;

    invoke-virtual {v0}, Lcom/squareup/applet/Applet$Badge$Visible;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/applet/Applet$Badge$Visible;->getPriority()Lcom/squareup/applet/Applet$Badge$Priority;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/squareup/applet/AppletsDrawerItem;->showBadge(Ljava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    :cond_4
    :goto_0
    return-void
.end method
