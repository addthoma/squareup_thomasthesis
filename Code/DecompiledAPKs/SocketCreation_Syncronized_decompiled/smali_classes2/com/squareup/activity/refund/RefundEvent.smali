.class public abstract Lcom/squareup/activity/refund/RefundEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u000e\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V",
        "getName",
        "()Lcom/squareup/eventstream/v1/EventStream$Name;",
        "getValue",
        "()Ljava/lang/String;",
        "Companion",
        "Lcom/squareup/activity/refund/RefundSelectedTendersEvent;",
        "Lcom/squareup/activity/refund/RefundReasonEvent;",
        "Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;",
        "Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;",
        "Lcom/squareup/activity/refund/RefundErrorEvent;",
        "Lcom/squareup/activity/refund/RefundChooseRefundEvent;",
        "Lcom/squareup/activity/refund/RefundReturnItemsEvent;",
        "Lcom/squareup/activity/refund/RefundReturnAmountEvent;",
        "Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;",
        "Lcom/squareup/activity/refund/RefundProcessingEvent;",
        "Lcom/squareup/activity/refund/RefundCancelEvent;",
        "Lcom/squareup/activity/refund/RefundRetryEvent;",
        "Lcom/squareup/activity/refund/RefundDoneEvent;",
        "Lcom/squareup/activity/refund/RefundCancelAfterErrorEvent;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/RefundEvent$Companion;


# instance fields
.field private final name:Lcom/squareup/eventstream/v1/EventStream$Name;

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/RefundEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RefundEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/RefundEvent;->Companion:Lcom/squareup/activity/refund/RefundEvent$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundEvent;->value:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/refund/RefundEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public static final issueRefundEvent(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundEvent;->Companion:Lcom/squareup/activity/refund/RefundEvent$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/activity/refund/RefundEvent$Companion;->issueRefundEvent(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundEvent;

    move-result-object p0

    return-object p0
.end method

.method public static final refundErrorEvent(Ljava/lang/String;)Lcom/squareup/activity/refund/RefundErrorEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundEvent;->Companion:Lcom/squareup/activity/refund/RefundEvent$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/activity/refund/RefundEvent$Companion;->refundErrorEvent(Ljava/lang/String;)Lcom/squareup/activity/refund/RefundErrorEvent;

    move-result-object p0

    return-object p0
.end method

.method public static final refundModeEvent(Lcom/squareup/activity/refund/RefundMode;)Lcom/squareup/activity/refund/RefundEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundEvent;->Companion:Lcom/squareup/activity/refund/RefundEvent$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/activity/refund/RefundEvent$Companion;->refundModeEvent(Lcom/squareup/activity/refund/RefundMode;)Lcom/squareup/activity/refund/RefundEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getName()Lcom/squareup/eventstream/v1/EventStream$Name;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundEvent;->value:Ljava/lang/String;

    return-object v0
.end method
