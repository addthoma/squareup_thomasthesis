.class public final Lcom/squareup/activity/refund/CardPresentRefundScreenData;
.super Ljava/lang/Object;
.source "CardPresentRefundScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 .2\u00020\u0001:\u0001.B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\nH\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u000cH\u00c6\u0003JG\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00c6\u0001J\u0016\u0010 \u001a\u00020\u00002\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$J\u0016\u0010%\u001a\u00020\u00002\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$J\u0006\u0010&\u001a\u00020\u0000J\u0013\u0010\'\u001a\u00020\n2\u0008\u0010(\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010)\u001a\u00020*H\u00d6\u0001J\u0006\u0010+\u001a\u00020,J\t\u0010-\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0013R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "title",
        "",
        "message",
        "cardPresentRefundErrorState",
        "Lcom/squareup/ui/activity/CardPresentRefundErrorState;",
        "stopListeningToScreenDataNow",
        "",
        "total",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;)V",
        "getCardPresentRefundErrorState",
        "()Lcom/squareup/ui/activity/CardPresentRefundErrorState;",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getMessage",
        "()Ljava/lang/String;",
        "getStopListeningToScreenDataNow",
        "()Z",
        "getTitle",
        "getTotal",
        "()Lcom/squareup/protos/common/Money;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "copyForFatalError",
        "messageResources",
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "res",
        "Lcom/squareup/util/Res;",
        "copyForRetryableError",
        "copyForScreenComplete",
        "equals",
        "other",
        "hashCode",
        "",
        "toFailureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "toString",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;


# instance fields
.field private final cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final message:Ljava/lang/String;

.field private final stopListeningToScreenDataNow:Z

.field private final title:Ljava/lang/String;

.field private final total:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->Companion:Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "glyph"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardPresentRefundErrorState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iput-boolean p5, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    iput-object p6, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x4

    if-eqz p8, :cond_0

    const-string p3, ""

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p7, 0x8

    if-eqz p3, :cond_1

    .line 20
    sget-object p4, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->NONE:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p7, 0x10

    if-eqz p3, :cond_2

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    move v5, p5

    :goto_0
    and-int/lit8 p3, p7, 0x20

    if-eqz p3, :cond_3

    const/4 p3, 0x0

    .line 22
    move-object p6, p3

    check-cast p6, Lcom/squareup/protos/common/Money;

    :cond_3
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static final awaitingCardRemoval(Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->Companion:Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;->awaitingCardRemoval(Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic copy$default(Lcom/squareup/activity/refund/CardPresentRefundScreenData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copy(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRefundData(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->Companion:Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;->fromRefundData(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->Companion:Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;->fromRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/activity/CardPresentRefundErrorState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    return v0
.end method

.method public final component6()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 8

    const-string v0, "glyph"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardPresentRefundErrorState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public final copyForFatalError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 10

    const-string v0, "messageResources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getTitleId()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 88
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getMessageId()I

    move-result p1

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 89
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 90
    sget-object v5, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->FATAL:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p0

    .line 86
    invoke-static/range {v1 .. v9}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copy$default(Lcom/squareup/activity/refund/CardPresentRefundScreenData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p1

    return-object p1
.end method

.method public final copyForRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 10

    const-string v0, "messageResources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getTitleId()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getMessageId()I

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getMessageId()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    :goto_0
    move-object v4, p2

    goto :goto_1

    :cond_0
    const-string p2, ""

    goto :goto_0

    .line 79
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x21

    const/4 v9, 0x0

    const/4 v2, 0x0

    move-object v1, p0

    .line 73
    invoke-static/range {v1 .. v9}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copy$default(Lcom/squareup/activity/refund/CardPresentRefundScreenData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p1

    return-object p1
.end method

.method public final copyForScreenComplete()Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x2f

    const/4 v8, 0x0

    move-object v0, p0

    .line 98
    invoke-static/range {v0 .. v8}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copy$default(Lcom/squareup/activity/refund/CardPresentRefundScreenData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iget-object v1, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCardPresentRefundErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    return-object v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getStopListeningToScreenDataNow()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final toFailureMessage()Lcom/squareup/receiving/FailureMessage;
    .locals 4

    .line 100
    new-instance v0, Lcom/squareup/receiving/FailureMessage;

    .line 101
    iget-object v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    .line 102
    iget-object v2, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    const/4 v3, 0x0

    .line 100
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardPresentRefundScreenData(glyph="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardPresentRefundErrorState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->cardPresentRefundErrorState:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stopListeningToScreenDataNow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->stopListeningToScreenDataNow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->total:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
