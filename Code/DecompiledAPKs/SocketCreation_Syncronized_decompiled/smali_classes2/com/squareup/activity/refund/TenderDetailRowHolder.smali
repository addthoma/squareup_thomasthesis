.class final Lcom/squareup/activity/refund/TenderDetailRowHolder;
.super Ljava/lang/Object;
.source "IssueRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIssueRefundCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IssueRefundCoordinator.kt\ncom/squareup/activity/refund/TenderDetailRowHolder\n*L\n1#1,668:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ&\u00102\u001a\u0002032\u0006\u00104\u001a\u00020\u001f2\u0006\u00105\u001a\u00020\u00082\u0006\u00106\u001a\u0002072\u0006\u00108\u001a\u000209J\u0008\u0010:\u001a\u000209H\u0002R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u00020\u001fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#R\u0011\u0010$\u001a\u00020%\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0011\u0010(\u001a\u00020)\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+R\u0011\u0010,\u001a\u00020-\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010/R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u00101\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/activity/refund/TenderDetailRowHolder;",
        "",
        "view",
        "Landroid/view/ViewGroup;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "eventHandler",
        "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
        "(Landroid/view/ViewGroup;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;)V",
        "getEventHandler",
        "()Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
        "helpText",
        "Lcom/squareup/widgets/MessageView;",
        "getHelpText",
        "()Lcom/squareup/widgets/MessageView;",
        "inlineAmount",
        "Lcom/squareup/widgets/OnScreenRectangleEditText;",
        "getInlineAmount",
        "()Lcom/squareup/widgets/OnScreenRectangleEditText;",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "moneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "getMoneyScrubber",
        "()Lcom/squareup/money/MaxMoneyScrubber;",
        "getPriceLocaleHelper",
        "()Lcom/squareup/money/PriceLocaleHelper;",
        "tenderDetail",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "getTenderDetail",
        "()Lcom/squareup/activity/refund/TenderDetails;",
        "setTenderDetail",
        "(Lcom/squareup/activity/refund/TenderDetails;)V",
        "tenderGlyph",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "getTenderGlyph",
        "()Lcom/squareup/glyph/SquareGlyphView;",
        "tenderName",
        "Lcom/squareup/marketfont/MarketTextView;",
        "getTenderName",
        "()Lcom/squareup/marketfont/MarketTextView;",
        "textWatcher",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "getTextWatcher",
        "()Lcom/squareup/text/EmptyTextWatcher;",
        "getView",
        "()Landroid/view/ViewGroup;",
        "bind",
        "",
        "data",
        "remainingAmount",
        "taxInformationLocation",
        "Lcom/squareup/activity/refund/TaxesInformation;",
        "hasInsufficientCashDrawer",
        "",
        "isCashTenderWithRefundMoney",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

.field private final helpText:Lcom/squareup/widgets/MessageView;

.field private final inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field public tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

.field private final tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private final tenderName:Lcom/squareup/marketfont/MarketTextView;

.field private final textWatcher:Lcom/squareup/text/EmptyTextWatcher;

.field private final view:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->view:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p3, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    .line 580
    iget-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->view:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    sget p2, Lcom/squareup/activity/R$id;->tender_type:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 581
    iget-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->view:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    sget p2, Lcom/squareup/activity/R$id;->tender_name:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderName:Lcom/squareup/marketfont/MarketTextView;

    .line 582
    iget-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->view:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    sget p2, Lcom/squareup/activity/R$id;->inline_amount:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/OnScreenRectangleEditText;

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    .line 583
    iget-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->view:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    sget p2, Lcom/squareup/activity/R$id;->help_text:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    .line 584
    new-instance p1, Lcom/squareup/money/MaxMoneyScrubber;

    .line 585
    iget-object p2, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p2, Lcom/squareup/money/MoneyExtractor;

    iget-object p3, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 586
    new-instance p4, Lcom/squareup/protos/common/Money;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/common/Money;->DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p4, v0, v1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 584
    invoke-direct {p1, p2, p3, p4}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    .line 588
    new-instance p1, Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;

    invoke-direct {p1, p0}, Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;-><init>(Lcom/squareup/activity/refund/TenderDetailRowHolder;)V

    check-cast p1, Lcom/squareup/text/EmptyTextWatcher;

    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->textWatcher:Lcom/squareup/text/EmptyTextWatcher;

    .line 600
    iget-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object p2, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    check-cast p2, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {p1, p2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    .line 601
    iget-object p2, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    check-cast p2, Lcom/squareup/text/Scrubber;

    invoke-virtual {p1, p2}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    return-void
.end method

.method private final isCashTenderWithRefundMoney()Z
    .locals 3

    .line 665
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    const-string v1, "tenderDetail"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v0

    sget-object v2, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public final bind(Lcom/squareup/activity/refund/TenderDetails;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/TaxesInformation;Z)V
    .locals 6

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remainingAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxInformationLocation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 610
    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    const-string p1, "tenderDetail"

    if-eqz p4, :cond_0

    .line 611
    invoke-direct {p0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->isCashTenderWithRefundMoney()Z

    move-result p4

    if-eqz p4, :cond_0

    .line 612
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p4, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 613
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget v0, Lcom/squareup/marin/R$color;->marin_orange:I

    invoke-virtual {p4, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    goto :goto_0

    .line 615
    :cond_0
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_1

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 616
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p4, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    .line 618
    :goto_0
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderName:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_2

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 620
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->textWatcher:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p4, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 623
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p4, :cond_3

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p4}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object p4

    .line 624
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_4

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 622
    invoke-static {p4, p2}, Lcom/squareup/money/MoneyMath;->min(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 627
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    const-string v0, "maxMoney"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_5

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-static {p4, p2, v0}, Lcom/squareup/activity/refund/TenderDetailsKt;->setMaxForRefundTender(Lcom/squareup/money/MaxMoneyScrubber;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/TenderDetails;)V

    .line 629
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_6

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getEditable()Z

    move-result v0

    invoke-virtual {p4, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setEnabled(Z)V

    .line 630
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    new-instance v1, Lcom/squareup/protos/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v5, :cond_7

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v5}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v1, v4, v5}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 631
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p4, :cond_8

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p4}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object p4

    iget-object p4, p4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez p4, :cond_9

    goto :goto_1

    :cond_9
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long p4, v0, v2

    if-eqz p4, :cond_c

    :goto_1
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p4, :cond_a

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p4}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object p4

    iget-object p4, p4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez p4, :cond_b

    goto :goto_2

    :cond_b
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long p4, v0, v2

    if-eqz p4, :cond_e

    .line 632
    :cond_c
    :goto_2
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_d

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p4

    .line 633
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v0, p4}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    .line 636
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result p4

    invoke-virtual {v0, p4}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setSelection(I)V

    .line 639
    :cond_e
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->textWatcher:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p4, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 641
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p4, :cond_f

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p4}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object p4

    iget-object p4, p4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez p4, :cond_10

    goto :goto_3

    :cond_10
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long p4, v0, v2

    if-eqz p4, :cond_17

    :goto_3
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p4, :cond_11

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {p4}, Lcom/squareup/activity/refund/TenderDetails;->getEditable()Z

    move-result p4

    if-nez p4, :cond_12

    goto/16 :goto_6

    .line 643
    :cond_12
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p4}, Lcom/squareup/widgets/MessageView;->getText()Ljava/lang/CharSequence;

    move-result-object p4

    const-string v0, "helpText.text"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result p4

    const/4 v0, 0x1

    if-nez p4, :cond_13

    const/4 p4, 0x1

    goto :goto_4

    :cond_13
    const/4 p4, 0x0

    :goto_4
    if-eqz p4, :cond_18

    .line 644
    iget-object p4, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    sget-object v1, Lcom/squareup/activity/refund/TenderDetailRowHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/activity/refund/TaxesInformation;->ordinal()I

    move-result p3

    aget p3, v1, p3

    const-string v1, "tender"

    const-string v2, "amount"

    if-eq p3, v0, :cond_15

    .line 653
    iget-object p3, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p3}, Lcom/squareup/widgets/MessageView;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/activity/R$string;->refund_with_max_amount_help:I

    invoke-static {p3, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 654
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p3, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 655
    iget-object p3, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p3, :cond_14

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    invoke-virtual {p3}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 656
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_5

    .line 645
    :cond_15
    new-instance p3, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 646
    sget v0, Lcom/squareup/activity/R$string;->refund_with_max_amount_with_tax_info_help:I

    const-string v3, "learn_more"

    invoke-virtual {p3, v0, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p3

    .line 647
    sget v0, Lcom/squareup/activity/R$string;->refund_help_text_url:I

    invoke-virtual {p3, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p3

    .line 648
    sget v0, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-virtual {p3, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p3

    .line 649
    invoke-virtual {p3}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 650
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p3, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 651
    iget-object p3, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez p3, :cond_16

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p3}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 652
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 644
    :goto_5
    invoke-virtual {p4, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 642
    :cond_17
    :goto_6
    iget-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :cond_18
    :goto_7
    return-void
.end method

.method public final getEventHandler()Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;
    .locals 1

    .line 578
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->eventHandler:Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    return-object v0
.end method

.method public final getHelpText()Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 583
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->helpText:Lcom/squareup/widgets/MessageView;

    return-object v0
.end method

.method public final getInlineAmount()Lcom/squareup/widgets/OnScreenRectangleEditText;
    .locals 1

    .line 582
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->inlineAmount:Lcom/squareup/widgets/OnScreenRectangleEditText;

    return-object v0
.end method

.method public final getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 577
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getMoneyScrubber()Lcom/squareup/money/MaxMoneyScrubber;
    .locals 1

    .line 584
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-object v0
.end method

.method public final getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 576
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object v0
.end method

.method public final getTenderDetail()Lcom/squareup/activity/refund/TenderDetails;
    .locals 2

    .line 597
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    if-nez v0, :cond_0

    const-string v1, "tenderDetail"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getTenderGlyph()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    .line 580
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderGlyph:Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method public final getTenderName()Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    .line 581
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderName:Lcom/squareup/marketfont/MarketTextView;

    return-object v0
.end method

.method public final getTextWatcher()Lcom/squareup/text/EmptyTextWatcher;
    .locals 1

    .line 588
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->textWatcher:Lcom/squareup/text/EmptyTextWatcher;

    return-object v0
.end method

.method public final getView()Landroid/view/ViewGroup;
    .locals 1

    .line 575
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->view:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final setTenderDetail(Lcom/squareup/activity/refund/TenderDetails;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 597
    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder;->tenderDetail:Lcom/squareup/activity/refund/TenderDetails;

    return-void
.end method
