.class public final Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;
.super Ljava/lang/Object;
.source "CardPresentRefundScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/CardPresentRefundScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardPresentRefundScreenData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardPresentRefundScreenData.kt\ncom/squareup/activity/refund/CardPresentRefundScreenData$Companion\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J \u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J \u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;",
        "",
        "()V",
        "awaitingCardRemoval",
        "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
        "res",
        "Lcom/squareup/util/Res;",
        "fromRefundData",
        "refundData",
        "Lcom/squareup/activity/refund/RefundData;",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "fromRetryableError",
        "messageResources",
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/activity/refund/CardPresentRefundScreenData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final awaitingCardRemoval(Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    .line 65
    sget v1, Lcom/squareup/cardreader/R$string;->please_remove_card_title:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 66
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3c

    const/4 v9, 0x0

    move-object v1, v0

    .line 64
    invoke-direct/range {v1 .. v9}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final fromRefundData(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "refundData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glyph"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getFirstTenderRequiringCardAuthorization()Lcom/squareup/activity/refund/TenderDetails;

    move-result-object p1

    .line 34
    new-instance v9, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    .line 35
    sget v0, Lcom/squareup/activity/R$string;->refund_tap_dip_title:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 36
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getCardBrand()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "brand"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 37
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getCardLastFour()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "last_four"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 40
    sget v0, Lcom/squareup/activity/R$string;->refund_tap_dip_message:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 43
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p2

    .line 34
    invoke-direct/range {v0 .. v8}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v9
.end method

.method public final fromRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "messageResources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glyph"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getTitleId()I

    move-result v1

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getMessageId()I

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getMessageId()I

    move-result p1

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object v4, p1

    goto :goto_1

    :cond_0
    const-string p1, ""

    goto :goto_0

    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x38

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p2

    .line 52
    invoke-direct/range {v1 .. v9}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/activity/CardPresentRefundErrorState;ZLcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
