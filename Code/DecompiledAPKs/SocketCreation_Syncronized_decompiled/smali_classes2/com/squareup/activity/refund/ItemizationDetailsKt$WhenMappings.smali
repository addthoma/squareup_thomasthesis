.class public final synthetic Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->values()[Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1

    return-void
.end method
