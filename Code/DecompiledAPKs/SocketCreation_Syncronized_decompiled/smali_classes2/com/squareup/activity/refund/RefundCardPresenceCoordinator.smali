.class public final Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RefundCardPresenceCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;,
        Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0002\u0019\u001aB1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0004\u001a\u00020\u0006H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
        "eventHandler",
        "Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;Lcom/squareup/text/Formatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "message",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "total",
        "Landroid/widget/TextView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "updateView",
        "Factory",
        "RefundCardPresenceEventHandler",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final eventHandler:Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;

.field private message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private total:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
            ">;",
            "Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->data:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;

    iput-object p4, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static final synthetic access$getData$p(Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;)Lio/reactivex/Observable;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->data:Lio/reactivex/Observable;

    return-object p0
.end method

.method public static final synthetic access$updateView(Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;Lcom/squareup/activity/refund/CardPresentRefundScreenData;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->updateView(Lcom/squareup/activity/refund/CardPresentRefundScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 81
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 82
    sget v0, Lcom/squareup/activity/R$id;->card_present_refund_message_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 83
    sget v0, Lcom/squareup/activity/R$id;->refund_total:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->total:Landroid/widget/TextView;

    return-void
.end method

.method private final updateView(Lcom/squareup/activity/refund/CardPresentRefundScreenData;)V
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v1, "message"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->getMessage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "total"

    if-eqz v0, :cond_5

    .line 70
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->total:Landroid/widget/TextView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 74
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->total:Landroid/widget/TextView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 71
    :cond_4
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/activity/R$string;->refund_amount:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 72
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "amount"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 76
    :cond_5
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->total:Landroid/widget/TextView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->bindViews(Landroid/view/View;)V

    .line 52
    new-instance v0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$attach$1;-><init>(Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p1, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string v0, "actionBar.presenter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 59
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cardreader/R$string;->cancel_refund:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 60
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$attach$2;

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$attach$2;-><init>(Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
