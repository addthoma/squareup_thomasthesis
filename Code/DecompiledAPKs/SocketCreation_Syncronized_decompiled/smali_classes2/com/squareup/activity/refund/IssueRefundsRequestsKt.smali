.class public final Lcom/squareup/activity/refund/IssueRefundsRequestsKt;
.super Ljava/lang/Object;
.source "IssueRefundsRequests.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIssueRefundsRequests.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IssueRefundsRequests.kt\ncom/squareup/activity/refund/IssueRefundsRequestsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,568:1\n704#2:569\n777#2,2:570\n1360#2:572\n1429#2,3:573\n1360#2:576\n1429#2,3:577\n1587#2,3:580\n1360#2:583\n1429#2,3:584\n1587#2,3:587\n1360#2:590\n1429#2,3:591\n1587#2,3:594\n704#2:597\n777#2,2:598\n1360#2:600\n1429#2,3:601\n1587#2,3:604\n1360#2:607\n1429#2,3:608\n1587#2,3:611\n1360#2:614\n1429#2,3:615\n1587#2,3:618\n1360#2:621\n1429#2,3:622\n1360#2:625\n1429#2,3:626\n1360#2:629\n1429#2,3:630\n*E\n*S KotlinDebug\n*F\n+ 1 IssueRefundsRequests.kt\ncom/squareup/activity/refund/IssueRefundsRequestsKt\n*L\n150#1:569\n150#1,2:570\n151#1:572\n151#1,3:573\n282#1:576\n282#1,3:577\n283#1,3:580\n285#1:583\n285#1,3:584\n286#1,3:587\n288#1:590\n288#1,3:591\n289#1,3:594\n367#1:597\n367#1,2:598\n368#1:600\n368#1,3:601\n369#1,3:604\n383#1:607\n383#1,3:608\n384#1,3:611\n398#1:614\n398#1,3:615\n399#1,3:618\n433#1:621\n433#1,3:622\n433#1:625\n433#1,3:626\n433#1:629\n433#1,3:630\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a \u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\u001a\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u0002\u001a,\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0011H\u0002\u001a:\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\n\u0008\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u001cH\u0002\u001a \u0010\u001d\u001a\u00020\u00012\u000e\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a \u0010 \u001a\u00020\u00012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\u001a\u0010!\u001a\u0010\u0012\u000c\u0012\n #*\u0004\u0018\u00010\"0\"0\u0011*\u00020$H\u0002\u001a\u000e\u0010%\u001a\u0004\u0018\u00010&*\u00020$H\u0002\u001a\u001a\u0010\'\u001a\u00020\u0012*\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,\u001a\n\u0010-\u001a\u00020\u0014*\u00020.\u001a\u000c\u0010/\u001a\u00020\u000e*\u00020\u000eH\u0002\u001a\u001e\u00100\u001a\u000201*\u0002012\u0006\u0010)\u001a\u00020*2\u0008\u00102\u001a\u0004\u0018\u000103H\u0002\u001a$\u00100\u001a\u000204*\u0002042\u0006\u0010)\u001a\u00020*2\u0006\u00105\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0002\u00a8\u00066"
    }
    d2 = {
        "additiveFeesMoneySum",
        "Lcom/squareup/protos/common/Money;",
        "feeLineItems",
        "",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "currency_code",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "buildCardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "readerType",
        "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "encryptedReaderData",
        "Lokio/ByteString;",
        "calculateReturnAmounts",
        "Lcom/squareup/protos/client/bills/Cart$Amounts;",
        "totalRefundMoney",
        "returnItemizations",
        "",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        "returnTip",
        "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
        "constructPresentedCardDetails",
        "Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;",
        "entryMethod",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "keyedPan",
        "",
        "isRefundDestination",
        "",
        "discountMoneySum",
        "discountLineItems",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "feesMoneySum",
        "createRefundsFromTenderDetails",
        "Lcom/squareup/protos/client/bills/RefundRequest;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/activity/refund/RefundData;",
        "createReturnCartDetailsOrNull",
        "Lcom/squareup/protos/client/bills/ReturnCartDetails;",
        "createReturnItemization",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
        "returnQuantity",
        "Ljava/math/BigDecimal;",
        "roundingMode",
        "Ljava/math/RoundingMode;",
        "createReturnTip",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
        "negate",
        "prorateForReturn",
        "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
        "selectedOptions",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
        "returnRatio",
        "activity_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$createRefundsFromTenderDetails(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->createRefundsFromTenderDetails(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createReturnCartDetailsOrNull(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/bills/ReturnCartDetails;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->createReturnCartDetailsOrNull(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/bills/ReturnCartDetails;

    move-result-object p0

    return-object p0
.end method

.method private static final additiveFeesMoneySum(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 365
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    if-eqz p0, :cond_5

    .line 369
    check-cast p0, Ljava/lang/Iterable;

    .line 597
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 598
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 367
    iget-object v2, v2, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v2, v2, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v3, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 599
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 600
    new-instance p0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 601
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 602
    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 368
    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 603
    :cond_3
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 605
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    move-object p1, v0

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 369
    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_3

    :cond_4
    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    move-object p1, v0

    :goto_4
    return-object p1
.end method

.method private static final buildCardData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData;
    .locals 3

    .line 547
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 549
    sget-object v1, Lcom/squareup/activity/refund/IssueRefundsRequestsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 560
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p1

    .line 559
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 562
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can\'t handle readerType \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p0, 0x27

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 557
    :cond_1
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object p1

    .line 556
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card(Lcom/squareup/protos/client/bills/CardData$T2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 554
    :cond_2
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p1

    .line 553
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 551
    :cond_3
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object p1

    .line 550
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card(Lcom/squareup/protos/client/bills/CardData$R6Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    .line 565
    :goto_0
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 566
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    const-string p1, "CardData.Builder()\n     \u2026eaderType)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method static synthetic buildCardData$default(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/protos/client/bills/CardData;
    .locals 0

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_0

    .line 545
    sget-object p1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->buildCardData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    return-object p0
.end method

.method private static final calculateReturnAmounts(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$Amounts;"
        }
    .end annotation

    .line 280
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 281
    check-cast p1, Ljava/lang/Iterable;

    .line 576
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 577
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 578
    check-cast v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 282
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Amounts;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 579
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 581
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v3, v0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/Money;

    .line 283
    invoke-static {v3, v4}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    goto :goto_1

    .line 583
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 584
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 585
    check-cast v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 285
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Amounts;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 586
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 588
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-object v1, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/Money;

    .line 286
    invoke-static {v1, v4}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    goto :goto_3

    .line 287
    :cond_3
    check-cast p2, Ljava/lang/Iterable;

    .line 590
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {p2, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 591
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 592
    check-cast v2, Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    .line 288
    iget-object v2, v2, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 593
    :cond_4
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 595
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-object p2, v0

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 289
    invoke-static {p2, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    goto :goto_5

    .line 290
    :cond_5
    new-instance p1, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 292
    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 293
    invoke-virtual {p1, v3}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 294
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 295
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 296
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 298
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p0

    const-string p1, "Amounts.Builder()\n      \u2026o)\n      }\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final constructPresentedCardDetails(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Ljava/lang/String;Z)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;
    .locals 2

    .line 516
    sget-object v0, Lcom/squareup/activity/refund/IssueRefundsRequestsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p3, 0x2

    if-eq v0, p3, :cond_0

    .line 533
    invoke-static {p1, p2}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->buildCardData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    goto :goto_0

    .line 525
    :cond_0
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 527
    new-instance p3, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;-><init>()V

    .line 528
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;

    move-result-object p2

    .line 529
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R4Card;

    move-result-object p2

    .line 526
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card(Lcom/squareup/protos/client/bills/CardData$R4Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 531
    sget-object p2, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 532
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    goto :goto_0

    .line 517
    :cond_1
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 519
    new-instance p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;-><init>()V

    .line 520
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object p2

    .line 521
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p2

    .line 518
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 523
    sget-object p2, Lcom/squareup/protos/client/bills/CardData$ReaderType;->KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 524
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    .line 536
    :goto_0
    new-instance p2, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;-><init>()V

    .line 537
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    move-result-object p1

    .line 538
    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    move-result-object p0

    .line 539
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->is_refund_destination(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    move-result-object p0

    .line 540
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->build()Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object p0

    const-string p1, "PresentedCardDetails.Bui\u2026stination)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method static synthetic constructPresentedCardDetails$default(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    .line 511
    sget-object p1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 512
    sget-object p2, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_1
    and-int/lit8 p6, p5, 0x8

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    .line 513
    check-cast p3, Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x10

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 514
    :cond_3
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->constructPresentedCardDetails(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Ljava/lang/String;Z)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object p0

    return-object p0
.end method

.method private static final createRefundsFromTenderDetails(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/activity/refund/RefundData;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundRequest;",
            ">;"
        }
    .end annotation

    .line 149
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 569
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 570
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/activity/refund/TenderDetails;

    .line 150
    invoke-virtual {p0, v3}, Lcom/squareup/activity/refund/RefundData;->canRefundTender(Lcom/squareup/activity/refund/TenderDetails;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 571
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 572
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 573
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 574
    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 152
    new-instance v3, Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;-><init>()V

    .line 154
    new-instance v4, Lcom/squareup/protos/client/bills/Refund$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/bills/Refund$Builder;-><init>()V

    .line 155
    sget-object v5, Lcom/squareup/protos/client/bills/Refund$State;->NEW:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->state(Lcom/squareup/protos/client/bills/Refund$State;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 156
    new-instance v5, Lcom/squareup/protos/client/IdPair;

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->refund_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedBillServerToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 158
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getSourceTenderToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 159
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundReason()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getReasonOption()Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->reason_option(Lcom/squareup/protos/client/bills/Refund$ReasonOption;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 161
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 162
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_requested_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Refund$Builder;

    .line 164
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->hasCardAuthorizationData()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 167
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getCardEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v6

    .line 168
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getCardReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v7

    .line 169
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getCardAuthorizationData()Lokio/ByteString;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x18

    const/4 v12, 0x0

    .line 166
    invoke-static/range {v6 .. v12}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->constructPresentedCardDetails$default(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object v2

    .line 165
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->presented_card_details(Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    goto :goto_2

    .line 172
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 173
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/Card;->getInputType()Lcom/squareup/Card$InputType;

    move-result-object v2

    sget-object v5, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    if-ne v2, v5, :cond_3

    .line 177
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 178
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x6

    const/4 v12, 0x0

    .line 176
    invoke-static/range {v6 .. v12}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->constructPresentedCardDetails$default(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object v2

    .line 175
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->presented_card_details(Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    goto :goto_2

    .line 186
    :cond_3
    sget-object v5, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v6, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCardSwipe()Lokio/ByteString;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/16 v10, 0xa

    const/4 v11, 0x0

    .line 185
    invoke-static/range {v5 .. v11}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->constructPresentedCardDetails$default(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object v2

    .line 184
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->presented_card_details(Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    .line 193
    :cond_4
    :goto_2
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/Refund$Builder;->build()Lcom/squareup/protos/client/bills/Refund;

    move-result-object v2

    .line 154
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->refund(Lcom/squareup/protos/client/bills/Refund;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    .line 195
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->build()Lcom/squareup/protos/client/bills/RefundRequest;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 575
    :cond_5
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static final createReturnCartDetailsOrNull(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/bills/ReturnCartDetails;
    .locals 7

    .line 199
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/activity/refund/RefundData;->getReturnItemizations(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 204
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/activity/refund/RefundData;->getReturnTips(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 205
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getDisplayRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->calculateReturnAmounts(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object v2

    .line 206
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRoundingAdjustment()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object v3

    .line 208
    new-instance v4, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;-><init>()V

    .line 210
    new-instance v5, Lcom/squareup/protos/client/bills/Cart$Builder;

    invoke-direct {v5}, Lcom/squareup/protos/client/bills/Cart$Builder;-><init>()V

    .line 211
    new-instance v6, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;-><init>()V

    .line 212
    invoke-virtual {v6, v2}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    .line 213
    invoke-static {v2}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->negate(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    .line 214
    invoke-virtual {v6}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object v2

    .line 211
    invoke-virtual {v5, v2}, Lcom/squareup/protos/client/bills/Cart$Builder;->amount_details(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    .line 215
    new-instance v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;-><init>()V

    .line 216
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedBillServerToken()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->source_bill_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    .line 217
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    .line 218
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    .line 219
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    .line 220
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object p0

    .line 215
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v5, p0}, Lcom/squareup/protos/client/bills/Cart$Builder;->return_line_items(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$Builder;

    .line 221
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p0

    .line 210
    invoke-virtual {v4, p0}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;

    .line 223
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->build()Lcom/squareup/protos/client/bills/ReturnCartDetails;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final createReturnItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
    .locals 5

    const-string v0, "$this$createReturnItemization"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roundingMode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v1, "itemization.quantity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p1, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v2, "Return quantity must not be greater than residual quantity"

    .line 232
    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 240
    sget-object v0, Ljava/math/MathContext;->DECIMAL128:Ljava/math/MathContext;

    invoke-virtual {p1, v1, v0}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 242
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;-><init>()V

    .line 244
    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    .line 245
    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p0

    .line 246
    new-instance v2, Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    .line 247
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    .line 250
    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    const-string v3, "configuration"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "returnRatio"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p1, v0, p2}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->prorateForReturn(Lcom/squareup/protos/client/bills/Itemization$Configuration;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p2

    .line 248
    invoke-virtual {p0, p2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    .line 254
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    const-string v0, "amounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-static {p2, p1, v0}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->prorateForReturn(Lcom/squareup/protos/client/bills/Itemization$Amounts;Ljava/math/BigDecimal;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    .line 252
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p0

    .line 245
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    .line 258
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object p0

    const-string p1, "ReturnItemization.Builde\u2026))\n      }\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final createReturnTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)Lcom/squareup/protos/client/bills/ReturnTipLineItem;
    .locals 4

    const-string v0, "$this$createReturnTip"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    new-instance v0, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;-><init>()V

    .line 265
    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem;->newBuilder()Lcom/squareup/protos/client/bills/TipLineItem$Builder;

    move-result-object p0

    .line 266
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tip_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;

    move-result-object p0

    .line 267
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem;

    move-result-object p0

    .line 264
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->tip_line_item(Lcom/squareup/protos/client/bills/TipLineItem;)Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;

    move-result-object p0

    .line 269
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    move-result-object p0

    const-string v0, "ReturnTipLineItem.Builde\u2026()\n      )\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final discountMoneySum(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 396
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    if-eqz p0, :cond_2

    .line 399
    check-cast p0, Ljava/lang/Iterable;

    .line 614
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 615
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 616
    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 398
    iget-object v1, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 617
    :cond_0
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 619
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    move-object p1, v0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 399
    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    move-object p1, v0

    :goto_2
    return-object p1
.end method

.method private static final feesMoneySum(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 381
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    if-eqz p0, :cond_2

    .line 384
    check-cast p0, Ljava/lang/Iterable;

    .line 607
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 608
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 609
    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 383
    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 610
    :cond_0
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 612
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    move-object p1, v0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 384
    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    move-object p1, v0

    :goto_2
    return-object p1
.end method

.method private static final negate(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 1

    .line 301
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p0

    .line 302
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 303
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 304
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 305
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 306
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    .line 307
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p0

    const-string v0, "newBuilder().apply {\n  t\u2026rcharge_money))\n}.build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final prorateForReturn(Lcom/squareup/protos/client/bills/Itemization$Amounts;Ljava/math/BigDecimal;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Amounts;
    .locals 4

    .line 326
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object p0

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 328
    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "tax_money.currency_code"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->feesMoneySum(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    if-eqz p2, :cond_1

    .line 329
    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "discount_money.currency_code"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->discountMoneySum(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    const-string v2, "item_variation_price_money"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    invoke-static {v1, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->itemVariationPriceTimesQuantityMoney(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 332
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/checkout/ModifiersKt;->asAppliedModifiers(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/squareup/quantity/SharedCalculationsKt;->itemBaseAmount(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/util/Map;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    .line 343
    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    if-eqz p2, :cond_2

    .line 346
    iget-object v0, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->fee:Ljava/util/List;

    :cond_2
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "total_money.currency_code"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->additiveFeesMoneySum(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    aput-object p2, v1, v2

    .line 341
    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->sumAll([Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 340
    invoke-virtual {p0, p2}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    .line 350
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz p2, :cond_3

    .line 353
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    .line 355
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p0

    const-string p1, "newBuilder().apply {\n  /\u2026mountMoney)\n  }\n}.build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final prorateForReturn(Lcom/squareup/protos/client/bills/Itemization$Configuration;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/client/bills/Itemization$Configuration;
    .locals 10

    .line 433
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p0

    .line 496
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 434
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 439
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    const-string v3, "modifier_option"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 621
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 622
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 623
    check-cast v5, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    .line 440
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object v6

    .line 442
    new-instance v7, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v1, v8}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    .line 443
    iget-object v7, v6, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    move-result-object v7

    .line 451
    iget-object v8, v5, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v8, :cond_0

    iget-object v8, v8, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    if-eqz v8, :cond_0

    iget-object v8, v8, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    if-eqz v8, :cond_0

    invoke-static {v8}, Lkotlin/text/StringsKt;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_1

    :cond_0
    move-object v8, v1

    :goto_1
    if-eqz v8, :cond_1

    .line 452
    move-object v9, v8

    check-cast v9, Ljava/lang/Number;

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    .line 453
    iget-object v5, v5, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v5

    .line 456
    invoke-static {v9, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierQuantityTimesItemizationQuantity(ILjava/math/BigDecimal;)Ljava/lang/String;

    move-result-object v9

    .line 455
    invoke-virtual {v5, v9}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity_times_itemization_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    .line 467
    :cond_1
    iget-object v5, v7, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_money:Lcom/squareup/protos/common/Money;

    const-string v9, "modifier_option_money"

    invoke-static {v5, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_2

    :cond_2
    const/4 v8, 0x1

    .line 466
    :goto_2
    invoke-static {v5, v8, p1}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierOptionTimesQuantityMoney(Lcom/squareup/protos/common/Money;ILjava/math/BigDecimal;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 465
    invoke-virtual {v7, v5}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    .line 470
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object v5

    .line 443
    invoke-virtual {v6, v5}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    .line 472
    invoke-virtual {v6}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 624
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 439
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p1

    .line 473
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 475
    iget-object p1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    const-string v2, "discount"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 625
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 626
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 627
    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 476
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/DiscountLineItem;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object v3

    .line 478
    new-instance v5, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v1, v6}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    .line 479
    iget-object v5, v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    move-result-object v5

    .line 480
    iget-object v6, v5, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {v6, p2, p3}, Lcom/squareup/money/MoneyMath;->multiply(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;

    .line 481
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    move-result-object v5

    .line 479
    invoke-virtual {v3, v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    .line 483
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 628
    :cond_4
    check-cast v2, Ljava/util/List;

    .line 475
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    .line 486
    iget-object p1, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    const-string v2, "fee"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 629
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 630
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 631
    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 487
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/FeeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v3

    .line 489
    new-instance v4, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    .line 490
    iget-object v4, v3, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object v4

    .line 491
    iget-object v5, v4, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {v5, p2, p3}, Lcom/squareup/money/MoneyMath;->multiply(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    .line 492
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    move-result-object v4

    .line 490
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    .line 494
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 632
    :cond_5
    check-cast v2, Ljava/util/List;

    .line 486
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    if-eqz v0, :cond_6

    .line 496
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object v1

    .line 434
    :cond_6
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    .line 497
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p0

    const-string p1, "newBuilder().apply {\n  s\u2026)\n  }?.build())\n}.build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
