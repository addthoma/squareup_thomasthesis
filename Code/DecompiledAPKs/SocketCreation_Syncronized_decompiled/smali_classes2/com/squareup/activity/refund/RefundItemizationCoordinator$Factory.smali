.class public final Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;
.super Ljava/lang/Object;
.source "RefundItemizationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundItemizationCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u001c\u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u0019\u001a\u00020\u001aR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "getCurrencyCode",
        "()Lcom/squareup/protos/common/CurrencyCode;",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "getPriceLocaleHelper",
        "()Lcom/squareup/money/PriceLocaleHelper;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "create",
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "eventHandler",
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)Lcom/squareup/activity/refund/RefundItemizationCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;",
            "Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;",
            ")",
            "Lcom/squareup/activity/refund/RefundItemizationCoordinator;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    new-instance v0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    .line 97
    iget-object v4, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v5, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v6, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v7, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->res:Lcom/squareup/util/Res;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    .line 96
    invoke-direct/range {v1 .. v7}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method public final getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public final getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-object v0
.end method
