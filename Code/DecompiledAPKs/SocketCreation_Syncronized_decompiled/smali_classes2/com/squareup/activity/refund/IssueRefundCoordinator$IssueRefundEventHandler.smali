.class public interface abstract Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;
.super Ljava/lang/Object;
.source "IssueRefundCoordinator.kt"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnPanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/IssueRefundCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IssueRefundEventHandler"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0005H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH&J\"\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H&J\u0010\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016H&J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0018H&\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
        "Lcom/squareup/register/widgets/card/OnPanListener;",
        "clearCard",
        "",
        "isRefundToGiftCard",
        "",
        "hasCard",
        "onBackPressed",
        "onRefundPressed",
        "onRefundReasonSelected",
        "reasonText",
        "",
        "reason",
        "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "onRefundToGiftCard",
        "isGiftCard",
        "destinationGiftCard",
        "Lcom/squareup/Card;",
        "encryptedSwipe",
        "Lokio/ByteString;",
        "onTenderDetailChanged",
        "tenderDetails",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "swipedCard",
        "Lio/reactivex/Observable;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clearCard(Z)V
.end method

.method public abstract hasCard()Z
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onRefundPressed()V
.end method

.method public abstract onRefundReasonSelected(Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;)V
.end method

.method public abstract onRefundToGiftCard(ZLcom/squareup/Card;Lokio/ByteString;)V
.end method

.method public abstract onTenderDetailChanged(Lcom/squareup/activity/refund/TenderDetails;)V
.end method

.method public abstract swipedCard()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end method
