.class public final Lcom/squareup/activity/refund/RefundEvent$Companion;
.super Ljava/lang/Object;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundEvent$Companion;",
        "",
        "()V",
        "issueRefundEvent",
        "Lcom/squareup/activity/refund/RefundEvent;",
        "refundData",
        "Lcom/squareup/activity/refund/RefundData;",
        "refundErrorEvent",
        "Lcom/squareup/activity/refund/RefundErrorEvent;",
        "localizedDescription",
        "",
        "refundModeEvent",
        "refundMode",
        "Lcom/squareup/activity/refund/RefundMode;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundEvent$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final issueRefundEvent(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundEvent;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "refundData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/refund/RefundEvent$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 26
    sget-object v0, Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;->Companion:Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion;->of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;

    move-result-object p1

    check-cast p1, Lcom/squareup/activity/refund/RefundEvent;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 25
    :cond_1
    sget-object v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->Companion:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;->of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    move-result-object p1

    check-cast p1, Lcom/squareup/activity/refund/RefundEvent;

    :goto_0
    return-object p1
.end method

.method public final refundErrorEvent(Ljava/lang/String;)Lcom/squareup/activity/refund/RefundErrorEvent;
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "localizedDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/activity/refund/RefundErrorEvent;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, v0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/activity/refund/RefundErrorEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final refundModeEvent(Lcom/squareup/activity/refund/RefundMode;)Lcom/squareup/activity/refund/RefundEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "refundMode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/squareup/activity/refund/RefundEvent$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 32
    sget-object p1, Lcom/squareup/activity/refund/RefundReturnAmountEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundReturnAmountEvent;

    check-cast p1, Lcom/squareup/activity/refund/RefundEvent;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 31
    :cond_1
    sget-object p1, Lcom/squareup/activity/refund/RefundReturnItemsEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundReturnItemsEvent;

    check-cast p1, Lcom/squareup/activity/refund/RefundEvent;

    :goto_0
    return-object p1
.end method
