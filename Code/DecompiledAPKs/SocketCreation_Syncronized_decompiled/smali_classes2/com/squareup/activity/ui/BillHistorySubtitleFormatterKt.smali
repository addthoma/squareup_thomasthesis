.class public final Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;
.super Ljava/lang/Object;
.source "BillHistorySubtitleFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillHistorySubtitleFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillHistorySubtitleFormatter.kt\ncom/squareup/activity/ui/BillHistorySubtitleFormatterKt\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,187:1\n37#2,2:188\n*E\n*S KotlinDebug\n*F\n+ 1 BillHistorySubtitleFormatter.kt\ncom/squareup/activity/ui/BillHistorySubtitleFormatterKt\n*L\n59#1,2:188\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010\r\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a.\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0002\u001a\u001e\u0010\u000b\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0002\u001a\u001e\u0010\u000c\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0002\u001a.\u0010\r\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0002\u001a\u001e\u0010\u000e\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0002\u001a\u001e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u001a6\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00142\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016\u001a(\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\u0010H\u0002\u00a8\u0006\u001d"
    }
    d2 = {
        "addModifiersToSubtitle",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "subtitle",
        "",
        "",
        "addNoteToSubtitle",
        "addSkuToSubtitle",
        "addVariationPriceAndQuantityToSubtitle",
        "addVariationToSubtitle",
        "createBillHistoryItemizationSubtitle",
        "",
        "itemization",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "quantityOverride",
        "Ljava/math/BigDecimal;",
        "seats",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "toModifierString",
        "modifier",
        "Lcom/squareup/checkout/OrderModifier;",
        "nonEmptyModifierName",
        "unitAbbreviation",
        "activity_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final addModifiersToSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 145
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedMap;

    .line 146
    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/OrderModifier;

    .line 147
    invoke-virtual {v2, p0}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    .line 148
    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "modifier"

    .line 150
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v5, "item.selectedVariation"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v4

    const-string v5, "item.selectedVariation.unitAbbreviation"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-static {p1, v2, v3, v4}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->toModifierString(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/OrderModifier;Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 152
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static final addNoteToSubtitle(Lcom/squareup/checkout/CartItem;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 182
    iget-object p0, p0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    .line 183
    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "note"

    .line 184
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private static final addSkuToSubtitle(Lcom/squareup/checkout/CartItem;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "item.selectedVariation"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getDisplayDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;->sku:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 132
    :cond_0
    iget-object p0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, ""

    .line 134
    :goto_1
    move-object p0, v0

    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 135
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method private static final addVariationPriceAndQuantityToSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 92
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "selectedVariation"

    .line 93
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 97
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "item.unitPriceOrNull() ?: return"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1, v1}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 102
    iget-object v1, p2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 103
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    const-string v1, "selectedVariation.unitAbbreviation"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 105
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iget-object p1, p2, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-static {p1, p0}, Lcom/squareup/quantity/ItemQuantities;->toQuantityEntrySuffix(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 108
    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private static final addVariationToSubtitle(Lcom/squareup/checkout/CartItem;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 119
    :cond_0
    iget-object p0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v0, "item.selectedVariation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object p0

    .line 120
    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    const-string v0, "name"

    .line 124
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static final createBillHistoryItemizationSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 51
    iget-object v1, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz v1, :cond_0

    .line 52
    move-object v1, v0

    check-cast v1, Ljava/util/List;

    invoke-static {p0, p1, p2, v1}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->addVariationPriceAndQuantityToSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;Ljava/util/List;)V

    .line 53
    invoke-static {p2, v1}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->addVariationToSubtitle(Lcom/squareup/checkout/CartItem;Ljava/util/List;)V

    .line 54
    invoke-static {p2, v1}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->addSkuToSubtitle(Lcom/squareup/checkout/CartItem;Ljava/util/List;)V

    .line 55
    invoke-static {p0, p1, p2, v1}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->addModifiersToSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;Ljava/util/List;)V

    .line 57
    :cond_0
    move-object p0, v0

    check-cast p0, Ljava/util/List;

    invoke-static {p2, p0}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->addNoteToSubtitle(Lcom/squareup/checkout/CartItem;Ljava/util/List;)V

    .line 59
    check-cast v0, Ljava/util/Collection;

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/CharSequence;

    .line 189
    invoke-interface {v0, p0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_2

    const-string p1, ", "

    .line 59
    invoke-static {p0, p1}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const-string p0, ""

    :goto_0
    return-object p0

    .line 189
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final createBillHistoryItemizationSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Ljava/math/BigDecimal;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemization"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quantityOverride"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "seats"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v0, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 80
    sget-object v1, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    check-cast v1, Lcom/squareup/checkout/OrderVariationNamer;

    invoke-virtual {v0, p2, p0, v1, p4}, Lcom/squareup/checkout/CartItem$Builder;->fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 81
    invoke-virtual {p2, p3}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 82
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    const-string p3, "item"

    .line 83
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->createBillHistoryItemizationSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic createBillHistoryItemizationSubtitle$default(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/util/List;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    .line 76
    iget-object p3, p2, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string p5, "itemization.quantity"

    invoke-static {p3, p5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p5, Ljava/math/BigDecimal;

    invoke-direct {p5, p3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object p3, p5

    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->createBillHistoryItemizationSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final toModifierString(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/OrderModifier;Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5

    .line 164
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 165
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    goto :goto_1

    .line 170
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p0

    .line 171
    invoke-virtual {p0}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p0

    .line 172
    invoke-virtual {p0, p3}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p0

    .line 173
    invoke-virtual {p0}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 175
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    return-object p0

    :cond_2
    :goto_1
    return-object p2
.end method
