.class public final Lcom/squareup/activity/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final activity_applet_issue_refund_view:I = 0x7f0a0153

.field public static final activity_applet_issue_refund_view_glyph_view:I = 0x7f0a0154

.field public static final activity_applet_restock_on_itemized_refund_view:I = 0x7f0a015d

.field public static final amount:I = 0x7f0a01b7

.field public static final amount_button:I = 0x7f0a01b8

.field public static final amount_container:I = 0x7f0a01bc

.field public static final bottom_panel:I = 0x7f0a0246

.field public static final card_container:I = 0x7f0a02b4

.field public static final card_present_refund_message_panel:I = 0x7f0a02bf

.field public static final card_present_refund_view:I = 0x7f0a02c0

.field public static final cash_drawer_warning:I = 0x7f0a02ea

.field public static final checkbox:I = 0x7f0a0318

.field public static final content_view:I = 0x7f0a03a7

.field public static final email_disclaimer:I = 0x7f0a06be

.field public static final email_receipt_field:I = 0x7f0a06c4

.field public static final error_message_panel:I = 0x7f0a071f

.field public static final fees_help:I = 0x7f0a074a

.field public static final frame:I = 0x7f0a0773

.field public static final gift_card_brand_with_unmasked_digits:I = 0x7f0a078e

.field public static final gift_card_figure:I = 0x7f0a0797

.field public static final gift_card_number:I = 0x7f0a0799

.field public static final gift_card_refund_amount:I = 0x7f0a07a0

.field public static final glyph:I = 0x7f0a07ae

.field public static final help_text:I = 0x7f0a07d2

.field public static final inline_amount:I = 0x7f0a0832

.field public static final issue_receipt_subtitle:I = 0x7f0a08c4

.field public static final issue_refund_errors_bar:I = 0x7f0a08c6

.field public static final issue_refund_view:I = 0x7f0a08c7

.field public static final items:I = 0x7f0a08e8

.field public static final items_button:I = 0x7f0a08e9

.field public static final items_container:I = 0x7f0a08ea

.field public static final items_taxes_and_discounts_help:I = 0x7f0a08ed

.field public static final name:I = 0x7f0a0a0c

.field public static final note:I = 0x7f0a0a47

.field public static final notification_payment:I = 0x7f0a0a63

.field public static final print_formal_receipt_button:I = 0x7f0a0c66

.field public static final print_receipt_button:I = 0x7f0a0c6e

.field public static final reasons:I = 0x7f0a0cfd

.field public static final receipt_digital_hint:I = 0x7f0a0d07

.field public static final refund_amount_editor:I = 0x7f0a0d39

.field public static final refund_amount_help:I = 0x7f0a0d3a

.field public static final refund_amount_max:I = 0x7f0a0d3b

.field public static final refund_amount_remaining:I = 0x7f0a0d3c

.field public static final refund_cards_required:I = 0x7f0a0d3d

.field public static final refund_error_action_button:I = 0x7f0a0d3e

.field public static final refund_legal:I = 0x7f0a0d3f

.field public static final refund_tax_information:I = 0x7f0a0d44

.field public static final refund_to_gc_button:I = 0x7f0a0d46

.field public static final refund_to_gc_title:I = 0x7f0a0d47

.field public static final refund_to_gift_card_helper_text:I = 0x7f0a0d48

.field public static final refund_total:I = 0x7f0a0d49

.field public static final refund_type_radio_group:I = 0x7f0a0d4a

.field public static final refunded_items:I = 0x7f0a0d4b

.field public static final refunded_items_container:I = 0x7f0a0d4c

.field public static final refunded_items_title:I = 0x7f0a0d4d

.field public static final restockable_items:I = 0x7f0a0d75

.field public static final sms_disclaimer:I = 0x7f0a0eac

.field public static final sms_receipt_field:I = 0x7f0a0eb1

.field public static final switch_language_button:I = 0x7f0a0f58

.field public static final tender_name:I = 0x7f0a0f86

.field public static final tender_type:I = 0x7f0a0f8a

.field public static final tenders:I = 0x7f0a0f8b

.field public static final title:I = 0x7f0a103f

.field public static final top_panel:I = 0x7f0a1053


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
