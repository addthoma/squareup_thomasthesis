.class public abstract Lcom/squareup/charts/piechart/SliceColor;
.super Ljava/lang/Object;
.source "SliceColor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/charts/piechart/SliceColor$SolidColor;,
        Lcom/squareup/charts/piechart/SliceColor$GradientColor;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSliceColor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SliceColor.kt\ncom/squareup/charts/piechart/SliceColor\n*L\n1#1,53:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0001\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/SliceColor;",
        "",
        "()V",
        "paint",
        "Landroid/graphics/Paint;",
        "obtainPaint",
        "GradientColor",
        "SolidColor",
        "Lcom/squareup/charts/piechart/SliceColor$SolidColor;",
        "Lcom/squareup/charts/piechart/SliceColor$GradientColor;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final paint:Landroid/graphics/Paint;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/squareup/charts/piechart/SliceColor;->paint:Landroid/graphics/Paint;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/charts/piechart/SliceColor;-><init>()V

    return-void
.end method


# virtual methods
.method public final obtainPaint()Landroid/graphics/Paint;
    .locals 3

    .line 40
    instance-of v0, p0, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/charts/piechart/SliceColor;->paint:Landroid/graphics/Paint;

    .line 41
    move-object v2, p0

    check-cast v2, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    invoke-virtual {v2}, Lcom/squareup/charts/piechart/SliceColor$SolidColor;->getColor()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 43
    :cond_0
    instance-of v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/charts/piechart/SliceColor;->paint:Landroid/graphics/Paint;

    .line 44
    move-object v2, p0

    check-cast v2, Lcom/squareup/charts/piechart/SliceColor$GradientColor;

    invoke-virtual {v2}, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->getShader$public_release()Landroid/graphics/LinearGradient;

    move-result-object v2

    check-cast v2, Landroid/graphics/Shader;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 45
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 48
    :goto_0
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-object v0

    .line 43
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
