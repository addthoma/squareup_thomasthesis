.class public final Lcom/squareup/checkoutapplet/RealCheckoutApplet;
.super Lcom/squareup/checkoutapplet/CheckoutApplet;
.source "RealCheckoutApplet.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\'\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0018\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0014J\u0010\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0016R\u0014\u0010\n\u001a\u00020\u000bX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/checkoutapplet/RealCheckoutApplet;",
        "Lcom/squareup/checkoutapplet/CheckoutApplet;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "isEnabled",
        "Lio/reactivex/Observable;",
        "",
        "getHomeScreens",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "currentHistory",
        "Lflow/History;",
        "getText",
        "resources",
        "Landroid/content/res/Resources;",
        "visibility",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analyticsName:Ljava/lang/String;

.field private final isEnabled:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/checkoutapplet/CheckoutApplet;-><init>(Ldagger/Lazy;)V

    iput-object p3, p0, Lcom/squareup/checkoutapplet/RealCheckoutApplet;->mainScheduler:Lio/reactivex/Scheduler;

    .line 25
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CHECKOUT_APPLET_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, p1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "features.featureEnabled(CHECKOUT_APPLET_V2)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutapplet/RealCheckoutApplet;->isEnabled:Lio/reactivex/Observable;

    const-string p1, "checkout-v2"

    .line 39
    iput-object p1, p0, Lcom/squareup/checkoutapplet/RealCheckoutApplet;->analyticsName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/checkoutapplet/RealCheckoutApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance p1, Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;

    .line 33
    sget-object v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope;->INSTANCE:Lcom/squareup/checkoutapplet/CheckoutAppletScope;

    check-cast v0, Lcom/squareup/ui/main/InMainActivityScope;

    .line 32
    invoke-direct {p1, v0}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;-><init>(Lcom/squareup/ui/main/InMainActivityScope;)V

    .line 31
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget v0, Lcom/squareup/checkoutapplet/impl/R$string;->checkout_applet_name:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.string.checkout_applet_name)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/checkoutapplet/RealCheckoutApplet;->isEnabled:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/checkoutapplet/RealCheckoutApplet;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "isEnabled.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
