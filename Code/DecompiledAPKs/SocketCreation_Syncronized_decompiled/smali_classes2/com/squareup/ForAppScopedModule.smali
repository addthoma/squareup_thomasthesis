.class public abstract Lcom/squareup/ForAppScopedModule;
.super Ljava/lang/Object;
.source "ForAppScopedModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideForAppScopedAsSet(Lcom/squareup/backgroundjob/log/BackgroundJobLogger;Lcom/squareup/log/MainThreadBlockedLogger;Lcom/squareup/queue/QueueJobCreator;Lcom/squareup/payment/offline/StoreAndForwardJobCreator;)Ljava/util/Set;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/ElementsIntoSet;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/backgroundjob/log/BackgroundJobLogger;",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            "Lcom/squareup/queue/QueueJobCreator;",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Lmortar/Scoped;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 p0, 0x1

    aput-object p1, v1, p0

    const/4 p0, 0x2

    aput-object p2, v1, p0

    const/4 p0, 0x3

    aput-object p3, v1, p0

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
