.class public final Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;
.super Ljava/lang/Object;
.source "RedirectTrackingReceiver_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/analytics/RedirectTrackingReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;"
        }
    .end annotation
.end field

.field private final postInstallLoginSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->postInstallLoginSettingProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/analytics/RedirectTrackingReceiver;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectEncryptedEmailsFromReferrals(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    return-void
.end method

.method public static injectPostInstallLoginSetting(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/RedirectTrackingReceiver;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;)V"
        }
    .end annotation

    .line 54
    iput-object p1, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->postInstallLoginSetting:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/analytics/RedirectTrackingReceiver;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectAnalytics(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/analytics/Analytics;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->postInstallLoginSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectPostInstallLoginSetting(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/settings/LocalSetting;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-static {p1, v0}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectEncryptedEmailsFromReferrals(Lcom/squareup/analytics/RedirectTrackingReceiver;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/analytics/RedirectTrackingReceiver;

    invoke-virtual {p0, p1}, Lcom/squareup/analytics/RedirectTrackingReceiver_MembersInjector;->injectMembers(Lcom/squareup/analytics/RedirectTrackingReceiver;)V

    return-void
.end method
