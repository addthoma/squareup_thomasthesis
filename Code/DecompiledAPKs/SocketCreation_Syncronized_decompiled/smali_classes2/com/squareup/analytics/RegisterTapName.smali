.class public final enum Lcom/squareup/analytics/RegisterTapName;
.super Ljava/lang/Enum;
.source "RegisterTapName.java"

# interfaces
.implements Lcom/squareup/analytics/EventNamedTap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterTapName;",
        ">;",
        "Lcom/squareup/analytics/EventNamedTap;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterTapName;

.field public static final enum APPOINTMENT_ADD_SERVICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum AUTOMATIC_REMINDERS_DISABLED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum AUTOMATIC_REMINDERS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CASH_DRAWER_TEST:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_CREATE_NEW:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_EMAIL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_KEYPAD_TAB:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_LIBRARY_TAB:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_REDEEM_REWARDS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_REWARDS_CHOOSE_COUPON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_REWARDS_USE_CODE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CHECKOUT_SMS_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum COLLECT_CASH_DIALOG_CANCEL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum COLLECT_CASH_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_DONE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_DONE_EDITING:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_ITEMS_NAVIGATION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_SKIP_ITEM_DRAG:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_SKIP_ITEM_PRICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_SKIP_MODAL_CONTINUE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_SKIP_MODAL_END_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_SKIP_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_START_VIA_APPLET:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CREATE_ITEM_TUTORIAL_START_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CUSTOMER_CHECKOUT_CONFIRM_AND_PAY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CUSTOMER_CHECKOUT_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CUSTOMER_CHECKOUT_X_BUYER_CART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CUSTOMER_CHECKOUT_X_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum CUSTOM_CASH_TENDER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum DINING_OPTION_CART_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum DINING_OPTION_ITEM_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum EMONEY_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum EMPLOYEE_JOBS_LIST_JOBS_SELECT_JOB:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum EMPLOYEE_MANAGEMENT_TRACK_TIME_UPSELL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum GAP_TIME_TOGGLE_CART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum GAP_TIME_TOGGLE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum GIFT_CARD_TENDER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HARDWARE_PRINTER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HARDWARE_PRINTER_NONE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_ORDER_HISTORY_TRACK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_ORDER_READER_ALL_HARDWARE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_ORDER_READER_R12:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_ORDER_READER_R4:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_R4_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum HELP_R4_ORDER_READER_EXIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_ENTER_CARD_NUMBER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_QR_CODE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_START_APPLICATION_LINK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_TEXT_LINK_INSTEAD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_TEXT_MESSAGE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTALLMENTS_TEXT_SEND_CLICKED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_HEADER_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_LINK_DIFFERENT_DEBIT_CARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_TRY_TO_LINK_CARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_TRY_TO_RESEND_EMAIL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_UPSELL_DISMISSED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum INSTANT_DEPOSIT_UPSELL_TAPPED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_ALL_SERVICES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CANCEL_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CATEGORIES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CONFIRM_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_ITEM_IN_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_CREATE_UNIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_DISCOUNTS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_EDIT_SERVICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_MODIFIER_SETS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_TAP_SERVICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_APPLET_UNITS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEMS_CHECKOUT_ADD_ITEM_TO_CART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ITEM_LIBRARY_FILTER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum MESSAGE_CENTER_MESSAGE_LINK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_BANK_ACCOUNT_TYPE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_BANK_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_BANK_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_BANK_LATER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_BIRTH_DATE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_BUSINESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONFIRM_IDENTITY_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONFIRM_IDENTITY_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONFIRM_IDENTITY_RADIO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONFIRM_IDENTITY_RADIO_LAST:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_PAYMENT_ORDER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_R12_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_R12_SKIP:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_R4_MAIL_READER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_R4_SKIP_READER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_UPSELL_BUY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_CONTACTLESS_UPSELL_SKIP:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_EIN_RADIO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_INCOME_RADIO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_MCC_LATER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_PASSWORD_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_PERSONAL_INFO_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_PHONE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_SSN:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_READER_EDIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_READER_SEND:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_READER_SKIP:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_SIGN_UP:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_SUB_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_SUB_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_SUB_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONBOARDING_VERTICAL_SELECTION_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum OPEN_TICKETS_VIEW_DELETE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum OPEN_TICKETS_VIEW_MERGE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum OPEN_TICKETS_VIEW_MOVE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum OPEN_TICKETS_VIEW_REPRINT_TICKET:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum OPEN_TICKETS_VIEW_TRANSFER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_ADDRESS_CANCEL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_ADDRESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_PAYMENT_CANCEL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_PAYMENT_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_SUMMARY_ADDRESS_EDIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_SUMMARY_PAYMENT_EDIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_CONTACTLESS_SUMMARY_PLACE_ORDER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_ENTRY_ITEM_SUGGESTION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_ENTRY_ITEM_SUGGESTION_DELETE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_ENTRY_ITEM_SUGGESTION_SAVE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum ORDER_HUB_APPLET_ACTIVE_ORDERS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum OTHER_TENDER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_FLOW_CASH_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_FLOW_OTHER_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_FLOW_OTHER_TENDER_TYPE_CHANGED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_OPTIONS_MORE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_PAD_CHARGE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_PAD_SHOW_CART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_CARD_NOT_PRESENT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_CARD_ON_FILE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_CUSTOM_CASH:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_GIFT_CARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_INVOICE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_NO_SALE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_OTHER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYMENT_TYPE_QUICK_CASH:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYROLL_LEARN_MORE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PAYROLL_UPSELL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINTER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINTER_STATION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINTER_STATION_CREATE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINTER_STATION_DELETE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINTER_TEST_PRINT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINT_FORMAL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum PRINT_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum R12_MEET_THE_READER_MULTIPAGE_WALKTHROUGH:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum R12_MEET_THE_READER_WATCH_VIDEO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum R12_MULTIPAGE_WALKTHROUGH_COMPLETED_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum R12_MULTIPAGE_WALKTHROUGH_INTRO_VIDEO:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_COMPLETE_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_COMPLETE_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_COMPLETE_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_COMPLETE_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_COMPLETE_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_INPUT_EMAIL_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_INPUT_EMAIL_SEND:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_INPUT_TEXT_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_INPUT_TEXT_SEND:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_INVALID_INPUT_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_DECLINE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_EMAIL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_PRINT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_PRINT_FORMAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_TEXT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SELECTION_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SMS_MARKETING_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SMS_MARKETING_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SMS_MARKETING_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SMS_MARKETING_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RECEIPT_SMS_MARKETING_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum REFERRAL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum REPORTS_EMAIL_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum REPORTS_PRINT_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum REPORTS_RETRY_LOAD_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum RETAIL_MAP_PAGE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_CHANGE_COMPARISON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_COLLAPSE_ALL_DETAILS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_CUSTOMIZE_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_EMAIL_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_EXPAND_ALL_DETAILS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_HIDE_ITEMS_FOR_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_HIDE_VARIATIONS_FOR_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_PRINT_REPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SELECT_QUICK_TIMERANGE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SHOW_ALL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SHOW_ITEMS_FOR_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SHOW_TOP_10:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SHOW_TOP_5:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SHOW_VARIATIONS_FOR_ITEM:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_COUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_DETAILS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_GROSS_SALES_CHART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_NET_SALES_CHART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_OVERVIEW:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_SWITCH_TO_SALES_COUNT_CHART:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SALES_REPORT_VIEW_IN_DASHBOARD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SCALE_ITEMIZATION_ADD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SCALE_ITEMIZATION_CANCEL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CART_DISCOUNTS_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CART_DISCOUNTS_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CART_TAXES_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CART_TAXES_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_DETAIL_NOT_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_DETAIL_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_DETAIL_VARIABLE_PRICE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_CONVERT_ITEMS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_CREATE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_EMPLOYEE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_EXTRA_TIME:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_PRICE_TYPE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_REMOVE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SERVICES_TAP_VARIATION:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SHARED_SETTINGS_DASHBOARD_LINK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SPLIT_TENDER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_ABOUT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_AD:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_COMMUNITIES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_CREATE_ITEM_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_DEVICE_TOUR_T2:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_EMAIL_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_FEATURE_TOUR:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_FIRST_PAYMENT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_HELP:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_HELP_CONTENT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_INVOICE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_IS_SQUARE_UP:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_LICENSES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_LOYALTY_ADJUST_POINTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_LOYALTY_ENROLL_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_LOYALTY_REDEEM_REWARDS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_MESSAGES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_PRIVACY_POLICY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_QUICK_AMOUNTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_R12_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_R6_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_READER_ORDERS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_REFERRAL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_REORDER_READER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_SELLER_AGREEMENT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_SETUP_GUIDE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_TWITTER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_UNDERSTANDING_REGISTER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_UPLOAD_DIAGNOSTICS_DATA:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_UPLOAD_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_WEB_SUPPORT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SUPPORT_WHATS_NEW:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum SWIPE_CHIP_CARDS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TAXES_SERVICES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TAXES_SERVICES_EXEMPT_ALL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TAXES_SERVICES_TAX_ALL:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_ADD_NOTES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_BACK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_EDIT_NOTES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_EXIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_FINISH:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_LIST_BREAKS_SELECT_BREAK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_NOTES_SAVED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_PASSCODE_EXIT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_PASSCODE_SUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_PASSCODE_UNSUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_REMINDER_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_REMINDER_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_REMINDER_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_REMINDER_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_START_BREAK_OR_CLOCK_OUT_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_START_BREAK_OR_CLOCK_OUT_START_BREAK:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_START_BREAK_OR_CLOCK_OUT_SWITCH_JOBS:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_SUCCESS_CLOCK_IN_OUT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIMECARDS_VIEW_NOTES:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIP_CUSTOM_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TIP_SELECTED_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum TOUR_START:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum WELCOME_FLOW_TOS_CHECKBOX_SELECTED:Lcom/squareup/analytics/RegisterTapName;

.field public static final enum WELCOME_FLOW_TOS_CHECKBOX_UNSELECTED:Lcom/squareup/analytics/RegisterTapName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v1, 0x0

    const-string v2, "APPOINTMENT_ADD_SERVICE"

    const-string v3, "Appointment: Tap Add Service"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->APPOINTMENT_ADD_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v2, 0x1

    const-string v3, "AUTOMATIC_REMINDERS_ENABLED"

    const-string v4, "Invoices: Enable Automatic Reminders"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v3, 0x2

    const-string v4, "AUTOMATIC_REMINDERS_DISABLED"

    const-string v5, "Invoices: Disable Automatic Reminders"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_DISABLED:Lcom/squareup/analytics/RegisterTapName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v4, 0x3

    const-string v5, "CASH_DRAWER_TEST"

    const-string v6, "Cash Drawers: Test Open Cash Drawers"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CASH_DRAWER_TEST:Lcom/squareup/analytics/RegisterTapName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v5, 0x4

    const-string v6, "COLLECT_CASH_DIALOG_CANCEL"

    const-string v7, "Accidental Cash Modal: Cancel"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->COLLECT_CASH_DIALOG_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    .line 14
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v6, 0x5

    const-string v7, "COLLECT_CASH_DIALOG_OKAY"

    const-string v8, "Accidental Cash Modal: Okay"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->COLLECT_CASH_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

    .line 15
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v7, 0x6

    const-string v8, "CUSTOM_CASH_TENDER"

    const-string v9, "Custom Cash Tender"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CUSTOM_CASH_TENDER:Lcom/squareup/analytics/RegisterTapName;

    .line 16
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/4 v8, 0x7

    const-string v9, "CUSTOMER_CHECKOUT_CONFIRM_AND_PAY"

    const-string v10, "Customer Checkout: Confirm and Pay Clicked"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_CONFIRM_AND_PAY:Lcom/squareup/analytics/RegisterTapName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v9, 0x8

    const-string v10, "CUSTOMER_CHECKOUT_X_BUYER_CART"

    const-string v11, "Customer Checkout: Exit Buyer Cart Screen"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_X_BUYER_CART:Lcom/squareup/analytics/RegisterTapName;

    .line 18
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v10, 0x9

    const-string v11, "CUSTOMER_CHECKOUT_X_PAYMENT_PROMPT"

    const-string v12, "Customer Checkout: Exit Payment Prompt Screen"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_X_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterTapName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v11, 0xa

    const-string v12, "CHECKOUT_CREATE_NEW"

    const-string v13, "Checkout: Tap Create Item"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_CREATE_NEW:Lcom/squareup/analytics/RegisterTapName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v12, 0xb

    const-string v13, "CHECKOUT_SAVE_ITEM"

    const-string v14, "Checkout: Save Item"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 21
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v13, 0xc

    const-string v14, "CHECKOUT_SMS_RECEIPT"

    const-string v15, "Checkout: SMS Receipt Requested"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SMS_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v14, 0xd

    const-string v15, "CHECKOUT_EMAIL_RECEIPT"

    const-string v13, "Checkout: Email Receipt Requested"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_EMAIL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    .line 23
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const/16 v13, 0xe

    const-string v15, "CHECKOUT_KEYPAD_TAB"

    const-string v14, "Checkout: Keypad Tab Tapped"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_KEYPAD_TAB:Lcom/squareup/analytics/RegisterTapName;

    .line 24
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v14, "CHECKOUT_LIBRARY_TAB"

    const/16 v15, 0xf

    const-string v13, "Checkout: Library Tab Tapped"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_LIBRARY_TAB:Lcom/squareup/analytics/RegisterTapName;

    .line 25
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CHECKOUT_REDEEM_REWARDS"

    const/16 v14, 0x10

    const-string v15, "Checkout: Redeem Rewards In Library Tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REDEEM_REWARDS:Lcom/squareup/analytics/RegisterTapName;

    .line 26
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CHECKOUT_REWARDS_CHOOSE_COUPON"

    const/16 v14, 0x11

    const-string v15, "Checkout: Apply Reward To Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REWARDS_CHOOSE_COUPON:Lcom/squareup/analytics/RegisterTapName;

    .line 27
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CHECKOUT_REWARDS_USE_CODE"

    const/16 v14, 0x12

    const-string v15, "Checkout: Use Rewards Code Tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REWARDS_USE_CODE:Lcom/squareup/analytics/RegisterTapName;

    .line 28
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_ALL_ITEMS"

    const/16 v14, 0x13

    const-string v15, "Items Tutorial: All Items"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    .line 29
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_CREATE_ITEM"

    const/16 v14, 0x14

    const-string v15, "Items Tutorial: Create Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 30
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_DONE"

    const/16 v14, 0x15

    const-string v15, "Items Tutorial: Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE:Lcom/squareup/analytics/RegisterTapName;

    .line 31
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_DONE_EDITING"

    const/16 v14, 0x16

    const-string v15, "Items Tutorial: Done Editing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE_EDITING:Lcom/squareup/analytics/RegisterTapName;

    .line 32
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_ITEMS_NAVIGATION"

    const/16 v14, 0x17

    const-string v15, "Items Tutorial: Items Navigation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_ITEMS_NAVIGATION:Lcom/squareup/analytics/RegisterTapName;

    .line 33
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SAVE_ITEM"

    const/16 v14, 0x18

    const-string v15, "Items Tutorial: Save Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 34
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SKIP_ITEM_DRAG"

    const/16 v14, 0x19

    const-string v15, "Items Tutorial: Skip Item Drag"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_ITEM_DRAG:Lcom/squareup/analytics/RegisterTapName;

    .line 35
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SKIP_ITEM_PRICE"

    const/16 v14, 0x1a

    const-string v15, "Items Tutorial: Skip Item Price"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_ITEM_PRICE:Lcom/squareup/analytics/RegisterTapName;

    .line 36
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SKIP_MODAL_CONTINUE_TUTORIAL"

    const/16 v14, 0x1b

    const-string v15, "Items Tutorial: Exit Tutorial Modal: Continue"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_MODAL_CONTINUE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 38
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SKIP_MODAL_END_TUTORIAL"

    const/16 v14, 0x1c

    const-string v15, "Items Tutorial: Exit Tutorial Modal: End Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_MODAL_END_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 40
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SKIP_TUTORIAL"

    const/16 v14, 0x1d

    const-string v15, "Items Tutorial: Skip Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 41
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_START_VIA_APPLET"

    const/16 v14, 0x1e

    const-string v15, "Items Tutorial: Start Tutorial Via Support Applet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_START_VIA_APPLET:Lcom/squareup/analytics/RegisterTapName;

    .line 42
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CREATE_ITEM_TUTORIAL_START_VIA_ONBOARDING"

    const/16 v14, 0x1f

    const-string v15, "Items Tutorial: Start Tutorial Via Onboarding"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_START_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    .line 43
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "CUSTOMER_CHECKOUT_ROW_SELECTED"

    const/16 v14, 0x20

    const-string v15, "Customer Checkout: CTO Row Selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 44
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "DINING_OPTION_CART_LEVEL_CHANGED"

    const/16 v14, 0x21

    const-string v15, "Dining Option: Cart Level Changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->DINING_OPTION_CART_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    .line 45
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "DINING_OPTION_ITEM_LEVEL_CHANGED"

    const/16 v14, 0x22

    const-string v15, "Dining Option: Item Level Changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->DINING_OPTION_ITEM_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    .line 46
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "EMONEY_ROW_SELECTED"

    const/16 v14, 0x23

    const-string v15, "Emoney: CTO Row Selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->EMONEY_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 47
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "EMPLOYEE_JOBS_LIST_JOBS_SELECT_JOB"

    const/16 v14, 0x24

    const-string v15, "Employee Jobs: List Jobs: Select Job"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_JOBS_LIST_JOBS_SELECT_JOB:Lcom/squareup/analytics/RegisterTapName;

    .line 48
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "EMPLOYEE_MANAGEMENT_TRACK_TIME_UPSELL"

    const/16 v14, 0x25

    const-string v15, "Employee Management: Track Time on Register Upsell"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_MANAGEMENT_TRACK_TIME_UPSELL:Lcom/squareup/analytics/RegisterTapName;

    .line 49
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "GAP_TIME_TOGGLE_CART"

    const/16 v14, 0x26

    const-string v15, "Gap Time Toggle Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_CART:Lcom/squareup/analytics/RegisterTapName;

    .line 50
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "GAP_TIME_TOGGLE_SERVICE"

    const/16 v14, 0x27

    const-string v15, "Gap Time Toggle Service"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    .line 51
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "GIFT_CARD_TENDER"

    const/16 v14, 0x28

    const-string v15, "Gift Card Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->GIFT_CARD_TENDER:Lcom/squareup/analytics/RegisterTapName;

    .line 52
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HARDWARE_PRINTER"

    const/16 v14, 0x29

    const-string v15, "Hardware Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HARDWARE_PRINTER:Lcom/squareup/analytics/RegisterTapName;

    .line 53
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HARDWARE_PRINTER_NONE"

    const/16 v14, 0x2a

    const-string v15, "No Hardware Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HARDWARE_PRINTER_NONE:Lcom/squareup/analytics/RegisterTapName;

    .line 54
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_FEE_TUTORIAL"

    const/16 v14, 0x2b

    const-string v15, "Fee Tutorial: Help"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 55
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_ORDER_HISTORY_TRACK"

    const/16 v14, 0x2c

    const-string v15, "Help Flow: Order History Track Delivery"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_HISTORY_TRACK:Lcom/squareup/analytics/RegisterTapName;

    .line 56
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_ORDER_READER_ALL_HARDWARE"

    const/16 v14, 0x2d

    const-string v15, "Help Flow: Order Reader All Hardware"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_ALL_HARDWARE:Lcom/squareup/analytics/RegisterTapName;

    .line 57
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_ORDER_READER_R12"

    const/16 v14, 0x2e

    const-string v15, "Help Flow: Order Reader R12"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_R12:Lcom/squareup/analytics/RegisterTapName;

    .line 58
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_ORDER_READER_R4"

    const/16 v14, 0x2f

    const-string v15, "Help Flow: Order Reader R4"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_R4:Lcom/squareup/analytics/RegisterTapName;

    .line 59
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_R4_ORDER_READER"

    const/16 v14, 0x30

    const-string v15, "Help Flow: Order Reader tap continue"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_R4_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    .line 60
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "HELP_R4_ORDER_READER_EXIT"

    const/16 v14, 0x31

    const-string v15, "Help Flow: Order Reader exit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_R4_ORDER_READER_EXIT:Lcom/squareup/analytics/RegisterTapName;

    .line 61
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_ENTER_CARD_NUMBER"

    const/16 v14, 0x32

    const-string v15, "Installments: Enter Card Number"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_ENTER_CARD_NUMBER:Lcom/squareup/analytics/RegisterTapName;

    .line 62
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_QR_CODE"

    const/16 v14, 0x33

    const-string v15, "Installments: QR Code"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_QR_CODE:Lcom/squareup/analytics/RegisterTapName;

    .line 63
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_ROW_SELECTED"

    const/16 v14, 0x34

    const-string v15, "Installments: CTO Row Selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 64
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_START_APPLICATION_LINK"

    const/16 v14, 0x35

    const-string v15, "Installments: Start Application Link"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_START_APPLICATION_LINK:Lcom/squareup/analytics/RegisterTapName;

    .line 65
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_TEXT_LINK_INSTEAD"

    const/16 v14, 0x36

    const-string v15, "Installments: Text Link Instead"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_LINK_INSTEAD:Lcom/squareup/analytics/RegisterTapName;

    .line 66
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_TEXT_MESSAGE"

    const/16 v14, 0x37

    const-string v15, "Installments: Text Message"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_MESSAGE:Lcom/squareup/analytics/RegisterTapName;

    .line 67
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTALLMENTS_TEXT_SEND_CLICKED"

    const/16 v14, 0x38

    const-string v15, "Installments: Text Message Send Clicked"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_SEND_CLICKED:Lcom/squareup/analytics/RegisterTapName;

    .line 68
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS"

    const/16 v14, 0x39

    const-string v15, "Instant Deposit: Deposits Report Deposit Available Funds"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

    .line 70
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_HEADER_DEPOSIT_AVAILABLE_FUNDS"

    const/16 v14, 0x3a

    const-string v15, "Instant Deposit: Header Deposit Available Funds"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_HEADER_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

    .line 71
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_LINK_DIFFERENT_DEBIT_CARD"

    const/16 v14, 0x3b

    const-string v15, "Instant Deposit: Link Different Debit Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_LINK_DIFFERENT_DEBIT_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 72
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_TRY_TO_LINK_CARD"

    const/16 v14, 0x3c

    const-string v15, "Instant Deposit: Try to Link Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_TRY_TO_LINK_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 73
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_TRY_TO_RESEND_EMAIL"

    const/16 v14, 0x3d

    const-string v15, "Instant Deposit: Try to Resend Email"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_TRY_TO_RESEND_EMAIL:Lcom/squareup/analytics/RegisterTapName;

    .line 74
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_UPSELL_TAPPED"

    const/16 v14, 0x3e

    const-string v15, "Instant Deposit: Deposits Report Instant Deposit upsell tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_UPSELL_TAPPED:Lcom/squareup/analytics/RegisterTapName;

    .line 75
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "INSTANT_DEPOSIT_UPSELL_DISMISSED"

    const/16 v14, 0x3f

    const-string v15, "Instant Deposit: Deposits Report Instant Deposit upsell dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_UPSELL_DISMISSED:Lcom/squareup/analytics/RegisterTapName;

    .line 77
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEM_LIBRARY_FILTER"

    const/16 v14, 0x40

    const-string v15, "Item Library: Filter"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEM_LIBRARY_FILTER:Lcom/squareup/analytics/RegisterTapName;

    .line 78
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_ALL_ITEMS"

    const/16 v14, 0x41

    const-string v15, "Items Applet: All Items"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    .line 79
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_ALL_SERVICES"

    const/16 v14, 0x42

    const-string v15, "Items Applet: Tap All Services"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_ALL_SERVICES:Lcom/squareup/analytics/RegisterTapName;

    .line 80
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CANCEL_DISCARD_CHANGES"

    const/16 v14, 0x43

    const-string v15, "Items Applet: Cancel Discard Changes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CANCEL_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    .line 81
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CATEGORIES"

    const/16 v14, 0x44

    const-string v15, "Items Applet: Categories"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CATEGORIES:Lcom/squareup/analytics/RegisterTapName;

    .line 82
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_UNITS"

    const/16 v14, 0x45

    const-string v15, "Items Applet: Units"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_UNITS:Lcom/squareup/analytics/RegisterTapName;

    .line 83
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CONFIRM_DISCARD_CHANGES"

    const/16 v14, 0x46

    const-string v15, "Items Applet: Confirm Discard Changes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CONFIRM_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    .line 84
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_DISCOUNTS"

    const/16 v14, 0x47

    const-string v15, "Items Applet: Discounts"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_DISCOUNTS:Lcom/squareup/analytics/RegisterTapName;

    .line 85
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_MODIFIER_SETS"

    const/16 v14, 0x48

    const-string v15, "Items Applet: Modifier Sets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_MODIFIER_SETS:Lcom/squareup/analytics/RegisterTapName;

    .line 86
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_ITEM"

    const/16 v14, 0x49

    const-string v15, "Items Applet: Create Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 87
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_ITEM_IN_CATEGORY"

    const/16 v14, 0x4a

    const-string v15, "Items Applet: Create Item In Category"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM_IN_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    .line 88
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_CATEGORY"

    const/16 v14, 0x4b

    const-string v15, "Items Applet: Create Category"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    .line 89
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_DISCOUNT"

    const/16 v14, 0x4c

    const-string v15, "Items Applet: Create Discount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 90
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_UNIT"

    const/16 v14, 0x4d

    const-string v15, "Items Applet: Create Unit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_UNIT:Lcom/squareup/analytics/RegisterTapName;

    .line 91
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_MODIFIER_SET"

    const/16 v14, 0x4e

    const-string v15, "Items Applet: Create Modifier Set"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

    .line 92
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_CREATE_SERVICE"

    const/16 v14, 0x4f

    const-string v15, "Items Applet: Services: Tap Create Service"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    .line 93
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_EDIT_ITEM"

    const/16 v14, 0x50

    const-string v15, "Items Applet: Edit Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 94
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_EDIT_CATEGORY"

    const/16 v14, 0x51

    const-string v15, "Items Applet: Edit Category"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    .line 95
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_EDIT_DISCOUNT"

    const/16 v14, 0x52

    const-string v15, "Items Applet: Edit Discount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 96
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_EDIT_MODIFIER_SET"

    const/16 v14, 0x53

    const-string v15, "Items Applet: Edit Modifier Set"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

    .line 97
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_EDIT_SERVICE"

    const/16 v14, 0x54

    const-string v15, "Items Applet: Edit Service"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    .line 98
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_APPLET_TAP_SERVICE"

    const/16 v14, 0x55

    const-string v15, "Service Selected: Items Applet: Services"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_TAP_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    .line 99
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ITEMS_CHECKOUT_ADD_ITEM_TO_CART"

    const/16 v14, 0x56

    const-string v15, "Checkout: Tap Item to Add to Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_CHECKOUT_ADD_ITEM_TO_CART:Lcom/squareup/analytics/RegisterTapName;

    .line 100
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "MESSAGE_CENTER_MESSAGE_LINK"

    const/16 v14, 0x57

    const-string v15, "Message Center Message Link"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->MESSAGE_CENTER_MESSAGE_LINK:Lcom/squareup/analytics/RegisterTapName;

    .line 101
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_BANK_ACCOUNT_TYPE"

    const/16 v14, 0x58

    const-string v15, "Onboarding: Account type button in bank page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_ACCOUNT_TYPE:Lcom/squareup/analytics/RegisterTapName;

    .line 102
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_BANK_CONTINUE"

    const/16 v14, 0x59

    const-string v15, "Onboarding: Continue button in bank page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 103
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_BANK_CONTINUE_INVALID"

    const/16 v14, 0x5a

    const-string v15, "Onboarding: Continue button in bank page tapped with invalid info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

    .line 105
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_BANK_LATER"

    const/16 v14, 0x5b

    const-string v15, "Onboarding: Later button in bank page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_LATER:Lcom/squareup/analytics/RegisterTapName;

    .line 106
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_BIRTH_DATE"

    const/16 v14, 0x5c

    const-string v15, "Onboarding: Birth date in personal info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BIRTH_DATE:Lcom/squareup/analytics/RegisterTapName;

    .line 107
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_BUSINESS_CONTINUE"

    const/16 v14, 0x5d

    const-string v15, "Onboarding: Continue button in business info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BUSINESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 108
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONFIRM_IDENTITY_CONTINUE"

    const/16 v14, 0x5e

    const-string v15, "Onboarding: Continue on confirm identity page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 109
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONFIRM_IDENTITY_CONTINUE_MISSING"

    const/16 v14, 0x5f

    const-string v15, "Onboarding: Continue on confirm identity page tapped with missing info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    .line 111
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONFIRM_IDENTITY_RADIO"

    const/16 v14, 0x60

    const-string v15, "Onboarding: Answer radio button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_RADIO:Lcom/squareup/analytics/RegisterTapName;

    .line 112
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONFIRM_IDENTITY_RADIO_LAST"

    const/16 v14, 0x61

    const-string v15, "Onboarding: Last answer radio button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_RADIO_LAST:Lcom/squareup/analytics/RegisterTapName;

    .line 113
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_R4_MAIL_READER"

    const/16 v14, 0x62

    const-string v15, "Onboarding: Mail reader on R4 reader shipping page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R4_MAIL_READER:Lcom/squareup/analytics/RegisterTapName;

    .line 114
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_R4_SKIP_READER"

    const/16 v14, 0x63

    const-string v15, "Onboarding: Skip reader on R4 reader shipping page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R4_SKIP_READER:Lcom/squareup/analytics/RegisterTapName;

    .line 115
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_UPSELL_BUY"

    const/16 v14, 0x64

    const-string v15, "Onboarding: Buy button on R12 upsell page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_UPSELL_BUY:Lcom/squareup/analytics/RegisterTapName;

    .line 116
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_UPSELL_SKIP"

    const/16 v14, 0x65

    const-string v15, "Onboarding: Skip button on R12 upsell page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_UPSELL_SKIP:Lcom/squareup/analytics/RegisterTapName;

    .line 117
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_R12_CONTINUE"

    const/16 v14, 0x66

    const-string v15, "Onboarding: Continue button on R12 shipping page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R12_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 118
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_R12_SKIP"

    const/16 v14, 0x67

    const-string v15, "Onboarding: Skip button on R12 shipping page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R12_SKIP:Lcom/squareup/analytics/RegisterTapName;

    .line 119
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_CONTACTLESS_PAYMENT_ORDER"

    const/16 v14, 0x68

    const-string v15, "Onboarding: Order button on R12 payment page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_PAYMENT_ORDER:Lcom/squareup/analytics/RegisterTapName;

    .line 120
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_EIN_RADIO"

    const/16 v14, 0x69

    const-string v15, "Onboarding: Ein radio button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_EIN_RADIO:Lcom/squareup/analytics/RegisterTapName;

    .line 121
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_INCOME_RADIO"

    const/16 v14, 0x6a

    const-string v15, "Onboarding: Income radio"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_INCOME_RADIO:Lcom/squareup/analytics/RegisterTapName;

    .line 122
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_MCC_CONTINUE"

    const/16 v14, 0x6b

    const-string v15, "Onboarding: Continue button in MCC screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 123
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_MCC_CONTINUE_NON_SELECTED"

    const/16 v14, 0x6c

    const-string v15, "Onboarding: Continue button in MCC screen without any MCC selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 125
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_MCC_LATER"

    const/16 v14, 0x6d

    const-string v15, "Onboarding: Later button in MCC screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_LATER:Lcom/squareup/analytics/RegisterTapName;

    .line 126
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_MCC_RADIO"

    const/16 v14, 0x6e

    const-string v15, "Onboarding: MCC radio button selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

    .line 127
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_PERSONAL_INFO_CONTINUE"

    const/16 v14, 0x6f

    const-string v15, "Onboarding: Continue button in personal info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 128
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID"

    const/16 v14, 0x70

    const-string v15, "Onboarding: Continue clicked in personal info page with invalid info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

    .line 130
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_PHONE"

    const/16 v14, 0x71

    const-string v15, "Onboarding: Continue clicked in personal info page with invalid phone number"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_PHONE:Lcom/squareup/analytics/RegisterTapName;

    .line 132
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_SSN"

    const/16 v14, 0x72

    const-string v15, "Onboarding: Continue clicked in personal info page with invalid SSN"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_SSN:Lcom/squareup/analytics/RegisterTapName;

    .line 134
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING"

    const/16 v14, 0x73

    const-string v15, "Onboarding: Continue clicked in personal info page with missing info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    .line 136
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_READER_EDIT"

    const/16 v14, 0x74

    const-string v15, "Onboarding: Edit button in send reader page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_READER_EDIT:Lcom/squareup/analytics/RegisterTapName;

    .line 137
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_READER_SEND"

    const/16 v14, 0x75

    const-string v15, "Onboarding: Mail reader button in reader page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_READER_SEND:Lcom/squareup/analytics/RegisterTapName;

    .line 138
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_READER_SKIP"

    const/16 v14, 0x76

    const-string v15, "Onboarding: Skip button in reader page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_READER_SKIP:Lcom/squareup/analytics/RegisterTapName;

    .line 139
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_PASSWORD_CONTINUE"

    const/16 v14, 0x77

    const-string v15, "Onboarding: Sign Up Password Continue"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PASSWORD_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 140
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_SIGN_UP"

    const/16 v14, 0x78

    const-string v15, "Onboarding: Sign Up"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SIGN_UP:Lcom/squareup/analytics/RegisterTapName;

    .line 141
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_SUB_MCC_CONTINUE"

    const/16 v14, 0x79

    const-string v15, "Onboarding: Continue button in Sub-MCC screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 142
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_SUB_MCC_CONTINUE_NON_SELECTED"

    const/16 v14, 0x7a

    const-string v15, "Onboarding: Continue button in MCC screen without any Sub-MCC selected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 144
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_SUB_MCC_RADIO"

    const/16 v14, 0x7b

    const-string v15, "Onboarding: Sub-MCC radio button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

    .line 145
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONBOARDING_VERTICAL_SELECTION_CONTINUE"

    const/16 v14, 0x7c

    const-string v15, "Onboarding: Continue button in Vertical Selection screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_VERTICAL_SELECTION_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 147
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ONLINE_CHECKOUT"

    const/16 v14, 0x7d

    const-string v15, "SPOS Checkout: Online Checkout: Select Payment Type Buy"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterTapName;

    .line 148
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "OPEN_TICKETS_VIEW_DELETE"

    const/16 v14, 0x7e

    const-string v15, "Open Tickets View: Delete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_DELETE:Lcom/squareup/analytics/RegisterTapName;

    .line 149
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "OPEN_TICKETS_VIEW_MERGE"

    const/16 v14, 0x7f

    const-string v15, "Open Tickets View: Merge"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MERGE:Lcom/squareup/analytics/RegisterTapName;

    .line 150
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "OPEN_TICKETS_VIEW_MOVE"

    const/16 v14, 0x80

    const-string v15, "Open Tickets View: Move"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MOVE:Lcom/squareup/analytics/RegisterTapName;

    .line 151
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "OPEN_TICKETS_VIEW_REPRINT_TICKET"

    const/16 v14, 0x81

    const-string v15, "Open Tickets View: Reprint Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_REPRINT_TICKET:Lcom/squareup/analytics/RegisterTapName;

    .line 152
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "OPEN_TICKETS_VIEW_TRANSFER"

    const/16 v14, 0x82

    const-string v15, "Open Tickets View: Transfer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_TRANSFER:Lcom/squareup/analytics/RegisterTapName;

    .line 153
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_ADDRESS_CONTINUE"

    const/16 v14, 0x83

    const-string v15, "Order Contactless: Continue button in Address screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_ADDRESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 154
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_ADDRESS_CANCEL"

    const/16 v14, 0x84

    const-string v15, "Order Contactless: Cancel button in Address screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_ADDRESS_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    .line 155
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_SUMMARY_ADDRESS_EDIT"

    const/16 v14, 0x85

    const-string v15, "Order Contactless: Edit Address in Summary screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_SUMMARY_ADDRESS_EDIT:Lcom/squareup/analytics/RegisterTapName;

    .line 156
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_SUMMARY_PAYMENT_EDIT"

    const/16 v14, 0x86

    const-string v15, "Order Contactless: Edit Payment in Summary screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_SUMMARY_PAYMENT_EDIT:Lcom/squareup/analytics/RegisterTapName;

    .line 157
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_SUMMARY_PLACE_ORDER"

    const/16 v14, 0x87

    const-string v15, "Order Contactless: Place Order in Summary screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_SUMMARY_PLACE_ORDER:Lcom/squareup/analytics/RegisterTapName;

    .line 158
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_PAYMENT_CONTINUE"

    const/16 v14, 0x88

    const-string v15, "Order Contactless: Continue button in Payment screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_PAYMENT_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 159
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_CONTACTLESS_PAYMENT_CANCEL"

    const/16 v14, 0x89

    const-string v15, "Order Contactless: Cancel button in Payment screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_PAYMENT_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    .line 160
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_ENTRY_ITEM_SUGGESTION"

    const/16 v14, 0x8a

    const-string v15, "Item Suggestions: Tap Item Suggestion"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION:Lcom/squareup/analytics/RegisterTapName;

    .line 161
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_ENTRY_ITEM_SUGGESTION_SAVE"

    const/16 v14, 0x8b

    const-string v15, "Item Suggestions: Save Item Suggestion"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION_SAVE:Lcom/squareup/analytics/RegisterTapName;

    .line 162
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_ENTRY_ITEM_SUGGESTION_DELETE"

    const/16 v14, 0x8c

    const-string v15, "Item Suggestions: Delete Item Suggestion"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION_DELETE:Lcom/squareup/analytics/RegisterTapName;

    .line 163
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "ORDER_HUB_APPLET_ACTIVE_ORDERS"

    const/16 v14, 0x8d

    const-string v15, "Order Hub Applet: Active Orders"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->ORDER_HUB_APPLET_ACTIVE_ORDERS:Lcom/squareup/analytics/RegisterTapName;

    .line 164
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "OTHER_TENDER"

    const/16 v14, 0x8e

    const-string v15, "Other Tender Done Selecting Other Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->OTHER_TENDER:Lcom/squareup/analytics/RegisterTapName;

    .line 165
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_FLOW_CASH_TENDER_SELECTED"

    const/16 v14, 0x8f

    const-string v15, "Payment Flow: Cash Payment Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_CASH_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 166
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_FLOW_OTHER_TENDER_SELECTED"

    const/16 v14, 0x90

    const-string v15, "Other Tender Done Selecting Other Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_OTHER_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 167
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_FLOW_OTHER_TENDER_TYPE_CHANGED"

    const/16 v14, 0x91

    const-string v15, "Other Tender Type"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_OTHER_TENDER_TYPE_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    .line 168
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_OPTIONS_MORE"

    const/16 v14, 0x92

    const-string v15, "More Payment Options"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_OPTIONS_MORE:Lcom/squareup/analytics/RegisterTapName;

    .line 169
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_PAD_CHARGE_BUTTON"

    const/16 v14, 0x93

    const-string v15, "Payment Pad Charge Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_CHARGE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 170
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_PAD_SHOW_CART"

    const/16 v14, 0x94

    const-string v15, "Payment Pad Show Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_SHOW_CART:Lcom/squareup/analytics/RegisterTapName;

    .line 171
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_CARD_NOT_PRESENT"

    const/16 v14, 0x95

    const-string v15, "Payment Type: CNP"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_CARD_NOT_PRESENT:Lcom/squareup/analytics/RegisterTapName;

    .line 172
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_CARD_ON_FILE"

    const/16 v14, 0x96

    const-string v15, "Payment Type: Card On File"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_CARD_ON_FILE:Lcom/squareup/analytics/RegisterTapName;

    .line 173
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_CUSTOM_CASH"

    const/16 v14, 0x97

    const-string v15, "Payment Type: Custom Cash"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_CUSTOM_CASH:Lcom/squareup/analytics/RegisterTapName;

    .line 174
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_GIFT_CARD"

    const/16 v14, 0x98

    const-string v15, "Payment Type: Gift Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_GIFT_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 175
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_INVOICE"

    const/16 v14, 0x99

    const-string v15, "Payment Type: Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_INVOICE:Lcom/squareup/analytics/RegisterTapName;

    .line 176
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_NO_SALE"

    const/16 v14, 0x9a

    const-string v15, "Payment Type: No Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_NO_SALE:Lcom/squareup/analytics/RegisterTapName;

    .line 177
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_OTHER"

    const/16 v14, 0x9b

    const-string v15, "Payment Type: Other Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_OTHER:Lcom/squareup/analytics/RegisterTapName;

    .line 178
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYMENT_TYPE_QUICK_CASH"

    const/16 v14, 0x9c

    const-string v15, "Payment Type: Quick Cash"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_QUICK_CASH:Lcom/squareup/analytics/RegisterTapName;

    .line 179
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYROLL_LEARN_MORE"

    const/16 v14, 0x9d

    const-string v15, "Employee Management: Timecards and Payroll"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYROLL_LEARN_MORE:Lcom/squareup/analytics/RegisterTapName;

    .line 180
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PAYROLL_UPSELL_BUTTON"

    const/16 v14, 0x9e

    const-string v15, "Timecards and Payroll: Learn More about Payroll"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PAYROLL_UPSELL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 181
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINT_FORMAL_RECEIPT"

    const/16 v14, 0x9f

    const-string v15, "Print Formal Receipt Tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINT_FORMAL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    .line 182
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINT_RECEIPT"

    const/16 v14, 0xa0

    const-string v15, "Print Receipt Tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    .line 183
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINTER"

    const/16 v14, 0xa1

    const-string v15, "Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINTER:Lcom/squareup/analytics/RegisterTapName;

    .line 184
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINTER_STATION"

    const/16 v14, 0xa2

    const-string v15, "Printer Station"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION:Lcom/squareup/analytics/RegisterTapName;

    .line 185
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINTER_STATION_CREATE"

    const/16 v14, 0xa3

    const-string v15, "Create Printer Station"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION_CREATE:Lcom/squareup/analytics/RegisterTapName;

    .line 186
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINTER_STATION_DELETE"

    const/16 v14, 0xa4

    const-string v15, "Delete Printer Station"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION_DELETE:Lcom/squareup/analytics/RegisterTapName;

    .line 187
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "PRINTER_TEST_PRINT"

    const/16 v14, 0xa5

    const-string v15, "Printer Settings Print Test"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINTER_TEST_PRINT:Lcom/squareup/analytics/RegisterTapName;

    .line 188
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "R12_MEET_THE_READER_WATCH_VIDEO"

    const/16 v14, 0xa6

    const-string v15, "Meet the Reader Modal - Watch Video Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->R12_MEET_THE_READER_WATCH_VIDEO:Lcom/squareup/analytics/RegisterTapName;

    .line 189
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "R12_MEET_THE_READER_MULTIPAGE_WALKTHROUGH"

    const/16 v14, 0xa7

    const-string v15, "Meet the Reader Modal - Multipage Walkthrough Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->R12_MEET_THE_READER_MULTIPAGE_WALKTHROUGH:Lcom/squareup/analytics/RegisterTapName;

    .line 190
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "R12_MULTIPAGE_WALKTHROUGH_INTRO_VIDEO"

    const/16 v14, 0xa8

    const-string v15, "Multipage Walkthrough - Introductory Video Link"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->R12_MULTIPAGE_WALKTHROUGH_INTRO_VIDEO:Lcom/squareup/analytics/RegisterTapName;

    .line 191
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "R12_MULTIPAGE_WALKTHROUGH_COMPLETED_TUTORIAL"

    const/16 v14, 0xa9

    const-string v15, "Multipage Walkthrough - Completed Tutorial Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->R12_MULTIPAGE_WALKTHROUGH_COMPLETED_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 192
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_COMPLETE_NEW_SALE"

    const/16 v14, 0xaa

    const-string v15, "Receipt: New Sale from Receipt Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

    .line 193
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_COMPLETE_ADD_CARD"

    const/16 v14, 0xab

    const-string v15, "Receipt: Add Card from Receipt Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 194
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_COMPLETE_ADD_CUSTOMER"

    const/16 v14, 0xac

    const-string v15, "Receipt: New Customer from Receipt Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 195
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_COMPLETE_LANGUAGE_SELECTION"

    const/16 v14, 0xad

    const-string v15, "Receipt: Language Selection from Receipt Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 196
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_COMPLETE_VIEW_CUSTOMER"

    const/16 v14, 0xae

    const-string v15, "Receipt: View Customer from Receipt Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 197
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_INPUT_EMAIL_BACK_BUTTON"

    const/16 v14, 0xaf

    const-string v15, "Receipt: Back Button from Receipt Email Input"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_EMAIL_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 198
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_INPUT_EMAIL_SEND"

    const/16 v14, 0xb0

    const-string v15, "Receipt: Send for Email"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_EMAIL_SEND:Lcom/squareup/analytics/RegisterTapName;

    .line 199
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_INPUT_TEXT_BACK_BUTTON"

    const/16 v14, 0xb1

    const-string v15, "Receipt: Back Button from Receipt Text Input"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_TEXT_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 200
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_INPUT_TEXT_SEND"

    const/16 v14, 0xb2

    const-string v15, "Receipt: Send for Text"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_TEXT_SEND:Lcom/squareup/analytics/RegisterTapName;

    .line 201
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_INVALID_INPUT_DIALOG_OKAY"

    const/16 v14, 0xb3

    const-string v15, "Receipt: Okay for Receipt Invalid Input"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INVALID_INPUT_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

    .line 202
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_ADD_CUSTOMER"

    const/16 v14, 0xb4

    const-string v15, "Receipt: Add Customer from Receipt Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 203
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_ADD_CARD"

    const/16 v14, 0xb5

    const-string v15, "Receipt: Add Card from Receipt Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 204
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_EMAIL"

    const/16 v14, 0xb6

    const-string v15, "Receipt: Email Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_EMAIL:Lcom/squareup/analytics/RegisterTapName;

    .line 205
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_DECLINE"

    const/16 v14, 0xb7

    const-string v15, "Receipt: Decline Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_DECLINE:Lcom/squareup/analytics/RegisterTapName;

    .line 206
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_LANGUAGE_SELECTION"

    const/16 v14, 0xb8

    const-string v15, "Receipt: Language Selection from Receipt Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 207
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_NEW_SALE"

    const/16 v14, 0xb9

    const-string v15, "Receipt: New Sale from Receipt Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

    .line 208
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_PRINT"

    const/16 v14, 0xba

    const-string v15, "Receipt: Print Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_PRINT:Lcom/squareup/analytics/RegisterTapName;

    .line 209
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_PRINT_FORMAL"

    const/16 v14, 0xbb

    const-string v15, "Receipt: Print Formal Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_PRINT_FORMAL:Lcom/squareup/analytics/RegisterTapName;

    .line 210
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_TEXT"

    const/16 v14, 0xbc

    const-string v15, "Receipt: Text Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_TEXT:Lcom/squareup/analytics/RegisterTapName;

    .line 211
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SELECTION_VIEW_CUSTOMER"

    const/16 v14, 0xbd

    const-string v15, "Receipt: View Customer from Receipt Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 212
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SMS_MARKETING_ADD_CARD"

    const/16 v14, 0xbe

    const-string v15, "Receipt: Add Card from Receipt SMS Marketing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 213
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SMS_MARKETING_ADD_CUSTOMER"

    const/16 v14, 0xbf

    const-string v15, "Receipt: Add Customer from Receipt SMS Marketing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 214
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SMS_MARKETING_LANGUAGE_SELECTION"

    const/16 v14, 0xc0

    const-string v15, "Receipt: Language Selection from Receipt SMS Marketing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 216
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SMS_MARKETING_NEW_SALE"

    const/16 v14, 0xc1

    const-string v15, "Receipt: New Sale from Receipt SMS Marketing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

    .line 217
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RECEIPT_SMS_MARKETING_VIEW_CUSTOMER"

    const/16 v14, 0xc2

    const-string v15, "Receipt: View Customer from Receipt SMS Marketing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 218
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "REFERRAL_BUTTON"

    const/16 v14, 0xc3

    const-string v15, "Referral Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->REFERRAL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 219
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "REPORTS_CUSTOM_SALES_REPORT"

    const/16 v14, 0xc4

    const-string v15, "Reports: Custom Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 220
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "REPORTS_EMAIL_SALES_REPORT"

    const/16 v14, 0xc5

    const-string v15, "Reports: Email Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->REPORTS_EMAIL_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 221
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "REPORTS_PRINT_SALES_REPORT"

    const/16 v14, 0xc6

    const-string v15, "Reports: Print Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->REPORTS_PRINT_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 222
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "REPORTS_RETRY_LOAD_REPORT"

    const/16 v14, 0xc7

    const-string v15, "Reports: Retry Load Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->REPORTS_RETRY_LOAD_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 223
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "RETAIL_MAP_PAGE"

    const/16 v14, 0xc8

    const-string v15, "Retail Map Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->RETAIL_MAP_PAGE:Lcom/squareup/analytics/RegisterTapName;

    .line 224
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_CHANGE_COMPARISON"

    const/16 v14, 0xc9

    const-string v15, "Sales Report: Change Comparison"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_CHANGE_COMPARISON:Lcom/squareup/analytics/RegisterTapName;

    .line 225
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_COLLAPSE_ALL_DETAILS"

    const/16 v14, 0xca

    const-string v15, "Sales Report: Collapse All Details"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_COLLAPSE_ALL_DETAILS:Lcom/squareup/analytics/RegisterTapName;

    .line 226
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_CUSTOMIZE_REPORT"

    const/16 v14, 0xcb

    const-string v15, "Sales Report: Customize Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_CUSTOMIZE_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 227
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_EMAIL_REPORT"

    const/16 v14, 0xcc

    const-string v15, "Sales Report: Email Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_EMAIL_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 228
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_EXPAND_ALL_DETAILS"

    const/16 v14, 0xcd

    const-string v15, "Sales Report: Expand All Details"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_EXPAND_ALL_DETAILS:Lcom/squareup/analytics/RegisterTapName;

    .line 229
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_HIDE_ITEMS_FOR_CATEGORY"

    const/16 v14, 0xce

    const-string v15, "Sales Report: Hide Items for Category"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_HIDE_ITEMS_FOR_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    .line 230
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_HIDE_VARIATIONS_FOR_ITEM"

    const/16 v14, 0xcf

    const-string v15, "Sales Report: Hide Variations for Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_HIDE_VARIATIONS_FOR_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 231
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_PRINT_REPORT"

    const/16 v14, 0xd0

    const-string v15, "Sales Report: Print Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_PRINT_REPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 232
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SELECT_QUICK_TIMERANGE"

    const/16 v14, 0xd1

    const-string v15, "Sales Report: Select Quick Timerange"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SELECT_QUICK_TIMERANGE:Lcom/squareup/analytics/RegisterTapName;

    .line 233
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SHOW_ALL"

    const/16 v14, 0xd2

    const-string v15, "Sales Report: Show All"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_ALL:Lcom/squareup/analytics/RegisterTapName;

    .line 234
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SHOW_ITEMS_FOR_CATEGORY"

    const/16 v14, 0xd3

    const-string v15, "Sales Report: Show Items for Category"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_ITEMS_FOR_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    .line 235
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SHOW_TOP_10"

    const/16 v14, 0xd4

    const-string v15, "Sales Report: Show Top 10"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_TOP_10:Lcom/squareup/analytics/RegisterTapName;

    .line 236
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SHOW_TOP_5"

    const/16 v14, 0xd5

    const-string v15, "Sales Report: Show Top 5"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_TOP_5:Lcom/squareup/analytics/RegisterTapName;

    .line 237
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SHOW_VARIATIONS_FOR_ITEM"

    const/16 v14, 0xd6

    const-string v15, "Sales Report: Show Variations for Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_VARIATIONS_FOR_ITEM:Lcom/squareup/analytics/RegisterTapName;

    .line 238
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_AMOUNT"

    const/16 v14, 0xd7

    const-string v15, "Sales Report: Switch to Amount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 239
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_COUNT"

    const/16 v14, 0xd8

    const-string v15, "Sales Report: Switch to Count"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_COUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 240
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_DETAILS"

    const/16 v14, 0xd9

    const-string v15, "Sales Report: Switch to Details"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_DETAILS:Lcom/squareup/analytics/RegisterTapName;

    .line 241
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_GROSS_SALES_CHART"

    const/16 v14, 0xda

    const-string v15, "Sales Report: Switch to Gross Sales Chart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_GROSS_SALES_CHART:Lcom/squareup/analytics/RegisterTapName;

    .line 242
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_NET_SALES_CHART"

    const/16 v14, 0xdb

    const-string v15, "Sales Report: Switch to Net Sales Chart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_NET_SALES_CHART:Lcom/squareup/analytics/RegisterTapName;

    .line 243
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_OVERVIEW"

    const/16 v14, 0xdc

    const-string v15, "Sales Report: Switch to Overview"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_OVERVIEW:Lcom/squareup/analytics/RegisterTapName;

    .line 244
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_SWITCH_TO_SALES_COUNT_CHART"

    const/16 v14, 0xdd

    const-string v15, "Sales Report: Switch to Sales Count Chart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_SALES_COUNT_CHART:Lcom/squareup/analytics/RegisterTapName;

    .line 245
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SALES_REPORT_VIEW_IN_DASHBOARD"

    const/16 v14, 0xde

    const-string v15, "Sales Report: View in Dashboard"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_VIEW_IN_DASHBOARD:Lcom/squareup/analytics/RegisterTapName;

    .line 246
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SCALE_ITEMIZATION_ADD"

    const/16 v14, 0xdf

    const-string v15, "Itemization Editor: Add"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SCALE_ITEMIZATION_ADD:Lcom/squareup/analytics/RegisterTapName;

    .line 247
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SCALE_ITEMIZATION_CANCEL"

    const/16 v14, 0xe0

    const-string v15, "Itemization Editor: Cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SCALE_ITEMIZATION_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    .line 248
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_DETAIL_VARIABLE_PRICE_BUTTON"

    const/16 v14, 0xe1

    const-string v15, "Seller Flow: Configure Item Detail: Variable Price Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_VARIABLE_PRICE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 250
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_DETAIL_SELECTED_VARIATION_BUTTON"

    const/16 v14, 0xe2

    const-string v15, "Seller Flow: Configure Item Detail: Selected Variation Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 252
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_DETAIL_NOT_SELECTED_VARIATION_BUTTON"

    const/16 v14, 0xe3

    const-string v15, "Seller Flow: Configure Item Detail: Not Selected Variation Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_NOT_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 254
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CART_DISCOUNTS_UP_BUTTON"

    const/16 v14, 0xe4

    const-string v15, "Seller Flow: Cart Discounts: Up Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_DISCOUNTS_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 255
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CART_DISCOUNTS_BACK_BUTTON"

    const/16 v14, 0xe5

    const-string v15, "Seller Flow: Cart Discounts: Back Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_DISCOUNTS_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 256
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CART_TAXES_UP_BUTTON"

    const/16 v14, 0xe6

    const-string v15, "Seller Flow: Cart Taxes: Up Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_TAXES_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 257
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SELLER_FLOW_CART_TAXES_BACK_BUTTON"

    const/16 v14, 0xe7

    const-string v15, "Seller Flow: Cart Taxes: Back Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_TAXES_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    .line 258
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_CONVERT_ITEMS"

    const/16 v14, 0xe8

    const-string v15, "Items Applet: Services: Tap Convert Items"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_CONVERT_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    .line 259
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_CREATE_VARIATION"

    const/16 v14, 0xe9

    const-string v15, "Service: Tap Add Variation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_CREATE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    .line 260
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_EMPLOYEE_SELECTION"

    const/16 v14, 0xea

    const-string v15, "Service: Tap Assigned Employees"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_EMPLOYEE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 261
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_EXTRA_TIME"

    const/16 v14, 0xeb

    const-string v15, "Service: Tap Block Extra Time"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_EXTRA_TIME:Lcom/squareup/analytics/RegisterTapName;

    .line 262
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_PRICE_TYPE"

    const/16 v14, 0xec

    const-string v15, "Service: Tap Price Type"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_PRICE_TYPE:Lcom/squareup/analytics/RegisterTapName;

    .line 263
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_REMOVE_VARIATION"

    const/16 v14, 0xed

    const-string v15, "Variation: Tap Remove Variation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_REMOVE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    .line 264
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SERVICES_TAP_VARIATION"

    const/16 v14, 0xee

    const-string v15, "Service: Tap Existing Variation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_TAP_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    .line 265
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SHARED_SETTINGS_DASHBOARD_LINK"

    const/16 v14, 0xef

    const-string v15, "Shared Settings: Dashboard link clicked"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SHARED_SETTINGS_DASHBOARD_LINK:Lcom/squareup/analytics/RegisterTapName;

    .line 266
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SPLIT_TENDER"

    const/16 v14, 0xf0

    const-string v15, "Split Tender Split/Edit Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SPLIT_TENDER:Lcom/squareup/analytics/RegisterTapName;

    .line 267
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_ABOUT"

    const/16 v14, 0xf1

    const-string v15, "Support Applet: About"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ABOUT:Lcom/squareup/analytics/RegisterTapName;

    .line 268
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_AD"

    const/16 v14, 0xf2

    const-string v15, "Support Applet: Ad"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_AD:Lcom/squareup/analytics/RegisterTapName;

    .line 269
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_ANNOUNCEMENTS"

    const/16 v14, 0xf3

    const-string v15, "Support Applet: Announcements"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterTapName;

    .line 270
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_BANK_ACCOUNT"

    const/16 v14, 0xf4

    const-string v15, "Support Applet: Bank Account"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 271
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_COMMUNITIES"

    const/16 v14, 0xf5

    const-string v15, "Support Applet: Seller Communities"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_COMMUNITIES:Lcom/squareup/analytics/RegisterTapName;

    .line 272
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_CONTACT"

    const/16 v14, 0xf6

    const-string v15, "Support Applet: Contact"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterTapName;

    .line 273
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_CREATE_ITEM_TUTORIAL"

    const/16 v14, 0xf7

    const-string v15, "Support Applet: Create Item Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_CREATE_ITEM_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 274
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_DEVICE_TOUR_T2"

    const/16 v14, 0xf8

    const-string v15, "Support Applet: Device Tour on T2"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_DEVICE_TOUR_T2:Lcom/squareup/analytics/RegisterTapName;

    .line 275
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_EMAIL_TRANSACTION_LEDGER"

    const/16 v14, 0xf9

    const-string v15, "Support Applet: Email Transaction Ledger"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_EMAIL_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

    .line 276
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_FEE_TUTORIAL"

    const/16 v14, 0xfa

    const-string v15, "Support Applet: Fee Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 277
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_FIRST_PAYMENT"

    const/16 v14, 0xfb

    const-string v15, "Support Applet: First Payment Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FIRST_PAYMENT:Lcom/squareup/analytics/RegisterTapName;

    .line 278
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_HELP"

    const/16 v14, 0xfc

    const-string v15, "Support Applet: Help"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_HELP:Lcom/squareup/analytics/RegisterTapName;

    .line 279
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_HELP_CONTENT"

    const/16 v14, 0xfd

    const-string v15, "Support Applet: Help Content"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_HELP_CONTENT:Lcom/squareup/analytics/RegisterTapName;

    .line 280
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_INVOICE_TUTORIAL"

    const/16 v14, 0xfe

    const-string v15, "Support Applet: Create an Invoice Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_INVOICE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 281
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_IS_SQUARE_UP"

    const/16 v14, 0xff

    const-string v15, "Support Applet: Issquareup Status Link"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_IS_SQUARE_UP:Lcom/squareup/analytics/RegisterTapName;

    .line 282
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_LICENSES"

    const/16 v14, 0x100

    const-string v15, "Support Applet: Licenses"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LICENSES:Lcom/squareup/analytics/RegisterTapName;

    .line 283
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_LEGAL"

    const/16 v14, 0x101

    const-string v15, "Support Applet: Legal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterTapName;

    .line 284
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_LOYALTY_ADJUST_POINTS_TUTORIAL"

    const/16 v14, 0x102

    const-string v15, "Support Applet: Adjust Points Loyalty Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ADJUST_POINTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 285
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_LOYALTY_ENROLL_TUTORIAL"

    const/16 v14, 0x103

    const-string v15, "Support Applet: Enroll a Loyalty Customer Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ENROLL_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 286
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_LOYALTY_REDEEM_REWARDS_TUTORIAL"

    const/16 v14, 0x104

    const-string v15, "Support Applet: Redeem a Reward Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_REDEEM_REWARDS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 287
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_MESSAGES"

    const/16 v14, 0x105

    const-string v15, "Support Applet: Messages"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_MESSAGES:Lcom/squareup/analytics/RegisterTapName;

    .line 288
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_ONBOARDING"

    const/16 v14, 0x106

    const-string v15, "Support Applet: Onboarding Restart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    .line 289
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_ORDER_READER"

    const/16 v14, 0x107

    const-string v15, "Support Applet: Order Reader Request"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    .line 290
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_PRIVACY_POLICY"

    const/16 v14, 0x108

    const-string v15, "Support Applet: Privacy Policy"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_PRIVACY_POLICY:Lcom/squareup/analytics/RegisterTapName;

    .line 291
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_QUICK_AMOUNTS_TUTORIAL"

    const/16 v14, 0x109

    const-string v15, "Support Applet: Quick Amounts Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_QUICK_AMOUNTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 292
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_R6_TUTORIAL"

    const/16 v14, 0x10a

    const-string v15, "Support Applet: R6 Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_R6_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 293
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_R12_TUTORIAL"

    const/16 v14, 0x10b

    const-string v15, "Support Applet: R12 Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_R12_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    .line 294
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_READER_ORDERS"

    const/16 v14, 0x10c

    const-string v15, "Support Applet: Reader Orders"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_READER_ORDERS:Lcom/squareup/analytics/RegisterTapName;

    .line 295
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_REFERRAL"

    const/16 v14, 0x10d

    const-string v15, "Support Applet: Get Free Processing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_REFERRAL:Lcom/squareup/analytics/RegisterTapName;

    .line 296
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_REORDER_READER"

    const/16 v14, 0x10e

    const-string v15, "Support Applet: Reader Request"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_REORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    .line 297
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_SELLER_AGREEMENT"

    const/16 v14, 0x10f

    const-string v15, "Support Applet: Seller Agreement"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_SELLER_AGREEMENT:Lcom/squareup/analytics/RegisterTapName;

    .line 298
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_SETUP_GUIDE"

    const/16 v14, 0x110

    const-string v15, "Support Applet: Setup Guide"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_SETUP_GUIDE:Lcom/squareup/analytics/RegisterTapName;

    .line 299
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_TROUBLESHOOTING"

    const/16 v14, 0x111

    const-string v15, "Support Applet: Troubleshooting"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterTapName;

    .line 300
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_TUTORIALS"

    const/16 v14, 0x112

    const-string v15, "Support Applet: Tutorials"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterTapName;

    .line 301
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_TWITTER"

    const/16 v14, 0x113

    const-string v15, "Support Applet: Twitter"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TWITTER:Lcom/squareup/analytics/RegisterTapName;

    .line 302
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_UNDERSTANDING_REGISTER"

    const/16 v14, 0x114

    const-string v15, "Support Applet: Understanding your Point of Sale X2 Tutorial"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UNDERSTANDING_REGISTER:Lcom/squareup/analytics/RegisterTapName;

    .line 303
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_FEATURE_TOUR"

    const/16 v14, 0x115

    const-string v15, "Support Applet: Feature Tour on X2"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FEATURE_TOUR:Lcom/squareup/analytics/RegisterTapName;

    .line 304
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_UPLOAD_DIAGNOSTICS_DATA"

    const/16 v14, 0x116

    const-string v15, "Support Applet: Upload Diagnostics Data"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UPLOAD_DIAGNOSTICS_DATA:Lcom/squareup/analytics/RegisterTapName;

    .line 305
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_UPLOAD_TRANSACTION_LEDGER"

    const/16 v14, 0x117

    const-string v15, "Support Applet: Upload Transaction Ledger"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UPLOAD_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

    .line 306
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_WEB_SUPPORT"

    const/16 v14, 0x118

    const-string v15, "Support Applet: Web Support"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_WEB_SUPPORT:Lcom/squareup/analytics/RegisterTapName;

    .line 307
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SUPPORT_WHATS_NEW"

    const/16 v14, 0x119

    const-string v15, "Support Applet: What\'s New in Square Point of Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_WHATS_NEW:Lcom/squareup/analytics/RegisterTapName;

    .line 308
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "SWIPE_CHIP_CARDS_ENABLED"

    const/16 v14, 0x11a

    const-string v15, "Swipe Chip Cards: Enabled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->SWIPE_CHIP_CARDS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    .line 309
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TAXES_SERVICES"

    const/16 v14, 0x11b

    const-string v15, "Tax: Tap Apply Tax To Services"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES:Lcom/squareup/analytics/RegisterTapName;

    .line 310
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TAXES_SERVICES_EXEMPT_ALL"

    const/16 v14, 0x11c

    const-string v15, "Tax: Applicable Services: Exempt All"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES_EXEMPT_ALL:Lcom/squareup/analytics/RegisterTapName;

    .line 311
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TAXES_SERVICES_TAX_ALL"

    const/16 v14, 0x11d

    const-string v15, "Tax: Applicable Services: Tap Tax All"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES_TAX_ALL:Lcom/squareup/analytics/RegisterTapName;

    .line 312
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_ADD_NOTES"

    const/16 v14, 0x11e

    const-string v15, "Timecards: Add Notes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_ADD_NOTES:Lcom/squareup/analytics/RegisterTapName;

    .line 313
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_BACK"

    const/16 v14, 0x11f

    const-string v15, "Timecards: Back"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_BACK:Lcom/squareup/analytics/RegisterTapName;

    .line 314
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_CLOCK_IN"

    const/16 v14, 0x120

    const-string v15, "Timecards: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

    .line 315
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_CLOCK_OUT"

    const/16 v14, 0x121

    const-string v15, "Timecards: Clock Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

    .line 316
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_EDIT_NOTES"

    const/16 v14, 0x122

    const-string v15, "Timecards: Edit Notes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_EDIT_NOTES:Lcom/squareup/analytics/RegisterTapName;

    .line 317
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_END_BREAK"

    const/16 v14, 0x123

    const-string v15, "Timecards: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

    .line 318
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_END_BREAK_EARLY"

    const/16 v14, 0x124

    const-string v15, "Timecards: End break early"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

    .line 319
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_EXIT"

    const/16 v14, 0x125

    const-string v15, "Timecards: Exit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_EXIT:Lcom/squareup/analytics/RegisterTapName;

    .line 320
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_FINISH"

    const/16 v14, 0x126

    const-string v15, "Timecards: Finish"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_FINISH:Lcom/squareup/analytics/RegisterTapName;

    .line 321
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_LIST_BREAKS_SELECT_BREAK"

    const/16 v14, 0x127

    const-string v15, "Timecards: List Breaks: Select Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_LIST_BREAKS_SELECT_BREAK:Lcom/squareup/analytics/RegisterTapName;

    .line 322
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_NOTES_SAVED"

    const/16 v14, 0x128

    const-string v15, "Timecards: Notes saved"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_NOTES_SAVED:Lcom/squareup/analytics/RegisterTapName;

    .line 323
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_PASSCODE_EXIT"

    const/16 v14, 0x129

    const-string v15, "Timecards: Passcode: Exit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_EXIT:Lcom/squareup/analytics/RegisterTapName;

    .line 324
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_PASSCODE_SUCCESSFUL_ATTEMPT"

    const/16 v14, 0x12a

    const-string v15, "Timecards: Passcode: Successful Attempt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_SUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

    .line 325
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_PASSCODE_UNSUCCESSFUL_ATTEMPT"

    const/16 v14, 0x12b

    const-string v15, "Timecards: Passcode: Unsuccessful Attempt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_UNSUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

    .line 326
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_REMINDER_CLOCK_IN"

    const/16 v14, 0x12c

    const-string v15, "Timecards Reminder: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

    .line 327
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_REMINDER_CONTINUE"

    const/16 v14, 0x12d

    const-string v15, "Timecards Reminder: Continue"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    .line 328
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_REMINDER_END_BREAK"

    const/16 v14, 0x12e

    const-string v15, "Timecards Reminder: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

    .line 329
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_REMINDER_END_BREAK_EARLY"

    const/16 v14, 0x12f

    const-string v15, "Timecards Reminder: End break early"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

    .line 330
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_START_BREAK_OR_CLOCK_OUT_CLOCK_OUT"

    const/16 v14, 0x130

    const-string v15, "Timecards: Start Break Or Clock Out: Clock Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

    .line 331
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_START_BREAK_OR_CLOCK_OUT_START_BREAK"

    const/16 v14, 0x131

    const-string v15, "Timecards: Start Break Or Clock Out: Start Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_START_BREAK:Lcom/squareup/analytics/RegisterTapName;

    .line 333
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_START_BREAK_OR_CLOCK_OUT_SWITCH_JOBS"

    const/16 v14, 0x132

    const-string v15, "Timecards: Start Break Or Clock Out: Switch Jobs"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_SWITCH_JOBS:Lcom/squareup/analytics/RegisterTapName;

    .line 335
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_SUCCESS_CLOCK_IN_OUT"

    const/16 v14, 0x133

    const-string v15, "Timecards: Success: Clock In/Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_SUCCESS_CLOCK_IN_OUT:Lcom/squareup/analytics/RegisterTapName;

    .line 336
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIMECARDS_VIEW_NOTES"

    const/16 v14, 0x134

    const-string v15, "Timecards: View Notes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_VIEW_NOTES:Lcom/squareup/analytics/RegisterTapName;

    .line 337
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIP_CUSTOM_AMOUNT"

    const/16 v14, 0x135

    const-string v15, "Entered Custom Tip Amount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIP_CUSTOM_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 338
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TIP_SELECTED_AMOUNT"

    const/16 v14, 0x136

    const-string v15, "Selected Suggested Tip Amount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TIP_SELECTED_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    .line 339
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "TOUR_START"

    const/16 v14, 0x137

    const-string v15, "Start Tour"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->TOUR_START:Lcom/squareup/analytics/RegisterTapName;

    .line 340
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "WELCOME_FLOW_TOS_CHECKBOX_SELECTED"

    const/16 v14, 0x138

    const-string v15, "Welcome Flow Create Account: TOS Checkbox Checked"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->WELCOME_FLOW_TOS_CHECKBOX_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 341
    new-instance v0, Lcom/squareup/analytics/RegisterTapName;

    const-string v13, "WELCOME_FLOW_TOS_CHECKBOX_UNSELECTED"

    const/16 v14, 0x139

    const-string v15, "Welcome Flow Create Account: TOS Checkbox Unchecked"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->WELCOME_FLOW_TOS_CHECKBOX_UNSELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v0, 0x13a

    new-array v0, v0, [Lcom/squareup/analytics/RegisterTapName;

    .line 8
    sget-object v13, Lcom/squareup/analytics/RegisterTapName;->APPOINTMENT_ADD_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_DISABLED:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CASH_DRAWER_TEST:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->COLLECT_CASH_DIALOG_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->COLLECT_CASH_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CUSTOM_CASH_TENDER:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_CONFIRM_AND_PAY:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_X_BUYER_CART:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_X_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_CREATE_NEW:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SMS_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_EMAIL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_KEYPAD_TAB:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_LIBRARY_TAB:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REDEEM_REWARDS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REWARDS_CHOOSE_COUPON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REWARDS_USE_CODE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE_EDITING:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_ITEMS_NAVIGATION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_ITEM_DRAG:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_ITEM_PRICE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_MODAL_CONTINUE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_MODAL_END_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_START_VIA_APPLET:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_START_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->DINING_OPTION_CART_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->DINING_OPTION_ITEM_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->EMONEY_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_JOBS_LIST_JOBS_SELECT_JOB:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->EMPLOYEE_MANAGEMENT_TRACK_TIME_UPSELL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_CART:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->GIFT_CARD_TENDER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HARDWARE_PRINTER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HARDWARE_PRINTER_NONE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_HISTORY_TRACK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_ALL_HARDWARE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_R12:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_R4:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_R4_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_R4_ORDER_READER_EXIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_ENTER_CARD_NUMBER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_QR_CODE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_START_APPLICATION_LINK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_LINK_INSTEAD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_MESSAGE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_TEXT_SEND_CLICKED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_HEADER_DEPOSIT_AVAILABLE_FUNDS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_LINK_DIFFERENT_DEBIT_CARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_TRY_TO_LINK_CARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_TRY_TO_RESEND_EMAIL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_UPSELL_TAPPED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_UPSELL_DISMISSED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEM_LIBRARY_FILTER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_ALL_SERVICES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CANCEL_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CATEGORIES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_UNITS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CONFIRM_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_DISCOUNTS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_MODIFIER_SETS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM_IN_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_UNIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_TAP_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_CHECKOUT_ADD_ITEM_TO_CART:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->MESSAGE_CENTER_MESSAGE_LINK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_ACCOUNT_TYPE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_LATER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BIRTH_DATE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BUSINESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_RADIO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_RADIO_LAST:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R4_MAIL_READER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R4_SKIP_READER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_UPSELL_BUY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_UPSELL_SKIP:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R12_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R12_SKIP:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_PAYMENT_ORDER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_EIN_RADIO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_INCOME_RADIO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_LATER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_PHONE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_SSN:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_READER_EDIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_READER_SEND:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_READER_SKIP:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PASSWORD_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SIGN_UP:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_VERTICAL_SELECTION_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_DELETE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MERGE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MOVE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_REPRINT_TICKET:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_TRANSFER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_ADDRESS_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_ADDRESS_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_SUMMARY_ADDRESS_EDIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_SUMMARY_PAYMENT_EDIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_SUMMARY_PLACE_ORDER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_PAYMENT_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_CONTACTLESS_PAYMENT_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION_SAVE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION_DELETE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_HUB_APPLET_ACTIVE_ORDERS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OTHER_TENDER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_CASH_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_OTHER_TENDER_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_FLOW_OTHER_TENDER_TYPE_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_OPTIONS_MORE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_CHARGE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_SHOW_CART:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_CARD_NOT_PRESENT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_CARD_ON_FILE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_CUSTOM_CASH:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_GIFT_CARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_INVOICE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_NO_SALE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_OTHER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_QUICK_CASH:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYROLL_LEARN_MORE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYROLL_UPSELL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINT_FORMAL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION_CREATE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION_DELETE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_TEST_PRINT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->R12_MEET_THE_READER_WATCH_VIDEO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->R12_MEET_THE_READER_MULTIPAGE_WALKTHROUGH:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->R12_MULTIPAGE_WALKTHROUGH_INTRO_VIDEO:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->R12_MULTIPAGE_WALKTHROUGH_COMPLETED_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_EMAIL_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_EMAIL_SEND:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_TEXT_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INPUT_TEXT_SEND:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_INVALID_INPUT_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_EMAIL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_DECLINE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_PRINT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_PRINT_FORMAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_TEXT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_NEW_SALE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REFERRAL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_EMAIL_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_PRINT_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_RETRY_LOAD_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RETAIL_MAP_PAGE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_CHANGE_COMPARISON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_COLLAPSE_ALL_DETAILS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_CUSTOMIZE_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_EMAIL_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_EXPAND_ALL_DETAILS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_HIDE_ITEMS_FOR_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_HIDE_VARIATIONS_FOR_ITEM:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_PRINT_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SELECT_QUICK_TIMERANGE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_ALL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_ITEMS_FOR_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_TOP_10:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_TOP_5:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SHOW_VARIATIONS_FOR_ITEM:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_COUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_DETAILS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_GROSS_SALES_CHART:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_NET_SALES_CHART:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_OVERVIEW:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_SWITCH_TO_SALES_COUNT_CHART:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_VIEW_IN_DASHBOARD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SCALE_ITEMIZATION_ADD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SCALE_ITEMIZATION_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_VARIABLE_PRICE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL_NOT_SELECTED_VARIATION_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_DISCOUNTS_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_DISCOUNTS_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_TAXES_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_TAXES_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_CONVERT_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_CREATE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_EMPLOYEE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_EXTRA_TIME:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_PRICE_TYPE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_REMOVE_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_TAP_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SHARED_SETTINGS_DASHBOARD_LINK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SPLIT_TENDER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ABOUT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_AD:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_COMMUNITIES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_CREATE_ITEM_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_DEVICE_TOUR_T2:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_EMAIL_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FIRST_PAYMENT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_HELP:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_HELP_CONTENT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_INVOICE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_IS_SQUARE_UP:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LICENSES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ADJUST_POINTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ENROLL_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_REDEEM_REWARDS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_MESSAGES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_PRIVACY_POLICY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_QUICK_AMOUNTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_R6_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_R12_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_READER_ORDERS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_REFERRAL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_REORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_SELLER_AGREEMENT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_SETUP_GUIDE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TWITTER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UNDERSTANDING_REGISTER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FEATURE_TOUR:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UPLOAD_DIAGNOSTICS_DATA:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UPLOAD_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_WEB_SUPPORT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_WHATS_NEW:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SWIPE_CHIP_CARDS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES_EXEMPT_ALL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TAXES_SERVICES_TAX_ALL:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_ADD_NOTES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_BACK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_EDIT_NOTES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_EXIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_FINISH:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_LIST_BREAKS_SELECT_BREAK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_NOTES_SAVED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_EXIT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_SUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_PASSCODE_UNSUCCESSFUL_ATTEMPT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_CLOCK_IN:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_END_BREAK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_REMINDER_END_BREAK_EARLY:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_CLOCK_OUT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_START_BREAK:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_START_BREAK_OR_CLOCK_OUT_SWITCH_JOBS:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_SUCCESS_CLOCK_IN_OUT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIMECARDS_VIEW_NOTES:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIP_CUSTOM_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIP_SELECTED_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TOUR_START:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->WELCOME_FLOW_TOS_CHECKBOX_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x138

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->WELCOME_FLOW_TOS_CHECKBOX_UNSELECTED:Lcom/squareup/analytics/RegisterTapName;

    const/16 v2, 0x139

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/RegisterTapName;->$VALUES:[Lcom/squareup/analytics/RegisterTapName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 346
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 347
    iput-object p3, p0, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterTapName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterTapName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->$VALUES:[Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterTapName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    return-object v0
.end method
