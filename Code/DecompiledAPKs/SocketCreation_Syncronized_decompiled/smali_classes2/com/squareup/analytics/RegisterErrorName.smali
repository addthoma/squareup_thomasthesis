.class public final enum Lcom/squareup/analytics/RegisterErrorName;
.super Ljava/lang/Enum;
.source "RegisterErrorName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterErrorName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum APPLICATION_NOT_RESPONDING:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum CORRUPT_QUEUE:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum DEPOSITS_REPORT_DETAIL_LOAD_BILL_ENTRIES_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum DEPOSITS_REPORT_DETAIL_LOAD_DEPOSIT_DETAILS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum FLIPPER_INVALID_SAVED_TICKET:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTALLMENTS_QR_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTALLMENTS_QR_FAILED_IMAGE_CONVERSION:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTALLMENTS_SMS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTANT_DEPOSIT_DEPOSITS_REPORT_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTANT_DEPOSIT_HEADER_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTANT_DEPOSIT_HEADER_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTANT_DEPOSIT_LINK_CARD_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INSTANT_DEPOSIT_RESEND_EMAIL_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INVOICE_DISCOUNT_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INVOICE_GIFT_CARD_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum INVOICE_MODIFIER_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum ITEMS_APPLET_SYNC_FAILED_AFTER_V3_PUT:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum LOGIN_RESPONSE_ENCRYPTION_ERROR:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum REMOTE_NOTIFICATIONS_LOADING_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum REPORTS_LOADING_REPORT_FAILED:Lcom/squareup/analytics/RegisterErrorName;

.field public static final enum WELCOME_FLOW_TOS_NOT_AGREED:Lcom/squareup/analytics/RegisterErrorName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v1, 0x0

    const-string v2, "APPLICATION_NOT_RESPONDING"

    const-string v3, "Application Not Responding: Main Thread Blocked"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->APPLICATION_NOT_RESPONDING:Lcom/squareup/analytics/RegisterErrorName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v2, 0x1

    const-string v3, "CORRUPT_QUEUE"

    const-string v4, "Corrupt Queue"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->CORRUPT_QUEUE:Lcom/squareup/analytics/RegisterErrorName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v3, 0x2

    const-string v4, "DEPOSITS_REPORT_DETAIL_LOAD_BILL_ENTRIES_FAILED"

    const-string v5, "Deposits Report Detail: Load bill entries failed"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->DEPOSITS_REPORT_DETAIL_LOAD_BILL_ENTRIES_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v4, 0x3

    const-string v5, "DEPOSITS_REPORT_DETAIL_LOAD_DEPOSIT_DETAILS_FAILED"

    const-string v6, "Deposits Report Detail: Load deposit details failed"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->DEPOSITS_REPORT_DETAIL_LOAD_DEPOSIT_DETAILS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 15
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v5, 0x4

    const-string v6, "FLIPPER_INVALID_SAVED_TICKET"

    const-string v7, "Flipper: invalid saved ticket"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->FLIPPER_INVALID_SAVED_TICKET:Lcom/squareup/analytics/RegisterErrorName;

    .line 16
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v6, 0x5

    const-string v7, "INSTALLMENTS_QR_FAILED"

    const-string v8, "Installments: QR Code Failed to Load"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_QR_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v7, 0x6

    const-string v8, "INSTALLMENTS_QR_FAILED_IMAGE_CONVERSION"

    const-string v9, "Installments: QR Code Failed to Load (Image Conversion Failed)"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_QR_FAILED_IMAGE_CONVERSION:Lcom/squareup/analytics/RegisterErrorName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/4 v8, 0x7

    const-string v9, "INSTALLMENTS_SMS_FAILED"

    const-string v10, "Installments: Failed to Send SMS"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_SMS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v9, 0x8

    const-string v10, "INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_ERROR"

    const-string v11, "Instant Deposit: Deposits Report Depositing Error"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v10, 0x9

    const-string v11, "INSTANT_DEPOSIT_DEPOSITS_REPORT_LEARN_MORE"

    const-string v12, "Instant Deposit: Deposits Report Learn More"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

    .line 23
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v11, 0xa

    const-string v12, "INSTANT_DEPOSIT_HEADER_DEPOSITING_ERROR"

    const-string v13, "Instant Deposit: Header Depositing Error"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_HEADER_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    .line 24
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v12, 0xb

    const-string v13, "INSTANT_DEPOSIT_HEADER_LEARN_MORE"

    const-string v14, "Instant Deposit: Header Learn More"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_HEADER_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

    .line 25
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v13, 0xc

    const-string v14, "INSTANT_DEPOSIT_RESEND_EMAIL_FAILED"

    const-string v15, "Instant Deposit: Resend Email Failed"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_RESEND_EMAIL_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 26
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v14, 0xd

    const-string v15, "INSTANT_DEPOSIT_LINK_CARD_FAILED"

    const-string v13, "Instant Deposit: Link Card Failed"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_LINK_CARD_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 27
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const/16 v13, 0xe

    const-string v15, "INVOICE_DISCOUNT_UNSUPPORTED"

    const-string v14, "Invoice: Discount Unsupported"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_DISCOUNT_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    .line 28
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v14, "INVOICE_GIFT_CARD_UNSUPPORTED"

    const/16 v15, 0xf

    const-string v13, "Invoice: Gift Card Unsupported"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_GIFT_CARD_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    .line 29
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v13, "INVOICE_MODIFIER_UNSUPPORTED"

    const/16 v14, 0x10

    const-string v15, "Invoice: Modifier Unsupported"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_MODIFIER_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    .line 30
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v13, "ITEMS_APPLET_SYNC_FAILED_AFTER_V3_PUT"

    const/16 v14, 0x11

    const-string v15, "Items Applet: Sync Failed After V3 Put"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->ITEMS_APPLET_SYNC_FAILED_AFTER_V3_PUT:Lcom/squareup/analytics/RegisterErrorName;

    .line 31
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v13, "LOGIN_RESPONSE_ENCRYPTION_ERROR"

    const/16 v14, 0x12

    const-string v15, "Login Response: Encryption Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->LOGIN_RESPONSE_ENCRYPTION_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    .line 32
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v13, "REMOTE_NOTIFICATIONS_LOADING_FAILED"

    const/16 v14, 0x13

    const-string v15, "Notifications: Loading Remote Notifications Failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->REMOTE_NOTIFICATIONS_LOADING_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 33
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v13, "REPORTS_LOADING_REPORT_FAILED"

    const/16 v14, 0x14

    const-string v15, "Reports: loading report failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->REPORTS_LOADING_REPORT_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    .line 34
    new-instance v0, Lcom/squareup/analytics/RegisterErrorName;

    const-string v13, "WELCOME_FLOW_TOS_NOT_AGREED"

    const/16 v14, 0x15

    const-string v15, "Welcome Flow Create Account: TOS Not Agreed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterErrorName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->WELCOME_FLOW_TOS_NOT_AGREED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v0, 0x16

    new-array v0, v0, [Lcom/squareup/analytics/RegisterErrorName;

    .line 8
    sget-object v13, Lcom/squareup/analytics/RegisterErrorName;->APPLICATION_NOT_RESPONDING:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->CORRUPT_QUEUE:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->DEPOSITS_REPORT_DETAIL_LOAD_BILL_ENTRIES_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->DEPOSITS_REPORT_DETAIL_LOAD_DEPOSIT_DETAILS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->FLIPPER_INVALID_SAVED_TICKET:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_QR_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_QR_FAILED_IMAGE_CONVERSION:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_SMS_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_HEADER_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_HEADER_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_RESEND_EMAIL_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_LINK_CARD_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_DISCOUNT_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_GIFT_CARD_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_MODIFIER_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->ITEMS_APPLET_SYNC_FAILED_AFTER_V3_PUT:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->LOGIN_RESPONSE_ENCRYPTION_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->REMOTE_NOTIFICATIONS_LOADING_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->REPORTS_LOADING_REPORT_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->WELCOME_FLOW_TOS_NOT_AGREED:Lcom/squareup/analytics/RegisterErrorName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/RegisterErrorName;->$VALUES:[Lcom/squareup/analytics/RegisterErrorName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput-object p3, p0, Lcom/squareup/analytics/RegisterErrorName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterErrorName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterErrorName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterErrorName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterErrorName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterErrorName;->$VALUES:[Lcom/squareup/analytics/RegisterErrorName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterErrorName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterErrorName;

    return-object v0
.end method
