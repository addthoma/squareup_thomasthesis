.class public abstract Lcom/squareup/analytics/event/v1/ActionEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ActionEvent.java"


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/EventNamedAction;)V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-interface {p1}, Lcom/squareup/analytics/EventNamedAction;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/EventNamedAction;Lcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-interface {p1}, Lcom/squareup/analytics/EventNamedAction;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method
