.class public final Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "AudioBlockingDialogEvent.java"


# instance fields
.field public final showing:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->AUDIO_BLOCKING_DIALOG:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 21
    iput-boolean p1, p0, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;->showing:Z

    return-void
.end method

.method public static dismissing()Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;
    .locals 2

    .line 13
    new-instance v0, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;-><init>(Z)V

    return-object v0
.end method

.method public static showing()Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;
    .locals 2

    .line 9
    new-instance v0, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;-><init>(Z)V

    return-object v0
.end method
