.class public Lcom/squareup/analytics/event/v1/CrashEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "CrashEvent.java"


# static fields
.field protected static final IS_BRAN:Z = true

.field protected static final IS_NATIVE_CRASH:Z = true


# instance fields
.field public final error_class:Ljava/lang/String;

.field public final error_message:Ljava/lang/String;

.field public final is_bran:Z

.field public final is_native_crash:Z

.field public final is_oom:Z

.field public final root_cause:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/Throwable;ZZ)V
    .locals 2

    .line 40
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->CRASH:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Crash Event: Uncaught Exception"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/event/v1/CrashEvent;->error_class:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/event/v1/CrashEvent;->error_message:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/squareup/util/Throwables;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/event/v1/CrashEvent;->root_cause:Ljava/lang/String;

    .line 44
    const-class v0, Ljava/lang/OutOfMemoryError;

    invoke-static {v0, p1}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/analytics/event/v1/CrashEvent;->is_oom:Z

    .line 45
    iput-boolean p2, p0, Lcom/squareup/analytics/event/v1/CrashEvent;->is_bran:Z

    .line 46
    iput-boolean p3, p0, Lcom/squareup/analytics/event/v1/CrashEvent;->is_native_crash:Z

    return-void
.end method

.method public static branJavaCrash(Ljava/lang/Throwable;)Lcom/squareup/analytics/event/v1/CrashEvent;
    .locals 3

    .line 23
    new-instance v0, Lcom/squareup/analytics/event/v1/CrashEvent;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/analytics/event/v1/CrashEvent;-><init>(Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method

.method public static branNativeCrash(Ljava/lang/Throwable;)Lcom/squareup/analytics/event/v1/CrashEvent;
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/analytics/event/v1/CrashEvent;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, v1}, Lcom/squareup/analytics/event/v1/CrashEvent;-><init>(Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method

.method public static posJavaCrash(Ljava/lang/Throwable;)Lcom/squareup/analytics/event/v1/CrashEvent;
    .locals 2

    .line 15
    new-instance v0, Lcom/squareup/analytics/event/v1/CrashEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1}, Lcom/squareup/analytics/event/v1/CrashEvent;-><init>(Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method

.method public static posNativeCrash(Ljava/lang/Throwable;)Lcom/squareup/analytics/event/v1/CrashEvent;
    .locals 3

    .line 19
    new-instance v0, Lcom/squareup/analytics/event/v1/CrashEvent;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/analytics/event/v1/CrashEvent;-><init>(Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method
