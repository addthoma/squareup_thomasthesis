.class public Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "R12FeatureTourEvent.java"


# static fields
.field public static final prependedEventString:Ljava/lang/String; = "R12 Feature Tour: "


# instance fields
.field public final detail:Ljava/lang/String;

.field public final eventDurationMsec:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;)V
    .locals 3

    .line 30
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R12 Feature Tour: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 31
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->detail:Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->eventDurationMsec:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V
    .locals 3

    .line 36
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R12 Feature Tour: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 37
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->detail:Ljava/lang/String;

    const/4 p1, 0x0

    .line 38
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->eventDurationMsec:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterTapName;)V
    .locals 3

    .line 24
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R12 Feature Tour: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 25
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->detail:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->eventDurationMsec:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;)V
    .locals 3

    .line 18
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R12 Feature Tour: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 19
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->detail:Ljava/lang/String;

    const/4 p1, 0x0

    .line 20
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->eventDurationMsec:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 3

    .line 43
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R12 Feature Tour: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 44
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->detail:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;->eventDurationMsec:Ljava/lang/Long;

    return-void
.end method
