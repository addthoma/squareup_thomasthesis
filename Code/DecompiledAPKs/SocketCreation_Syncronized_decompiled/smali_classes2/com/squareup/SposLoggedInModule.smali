.class public abstract Lcom/squareup/SposLoggedInModule;
.super Ljava/lang/Object;
.source "SposLoggedInModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/disputes/RealHandlesDisputesModule;,
        Lcom/squareup/pos/help/SposJumbotronModule;,
        Lcom/squareup/features/cogs/CogsModule;,
        Lcom/squareup/opentickets/TicketsModule;,
        Lcom/squareup/settings/server/passcode/PasscodeEnabledDefaultModule;,
        Lcom/squareup/payment/tender/DefaultTenderProtoFactoryModule;,
        Lcom/squareup/loyalty/impl/wiring/LoyaltySettingsModule;,
        Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;,
        Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProviderModule;,
        Lcom/squareup/print/OrderTicketPrintingEnabledModule;,
        Lcom/squareup/tickets/RealOpenTicketsSettingsModule;,
        Lcom/squareup/signout/ResetPaymentActivityOnSignOutModule;,
        Lcom/squareup/printer/epson/EpsonPrinterModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideJailScreenDialogConfiguration(Lcom/squareup/jailkeeper/JailScreenConfiguration$NoNetworkSettingsButton;)Lcom/squareup/jailkeeper/JailScreenConfiguration;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideShouldPreloadOrderEntryPages(Lcom/squareup/orderentry/pages/AlwaysPreLoadOrderEntryPages;)Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransactionLockModeDefault(Lcom/squareup/settings/server/passcode/TransactionLockModeDefault$DoNotLockAfterEachSale;)Lcom/squareup/settings/server/passcode/TransactionLockModeDefault;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
