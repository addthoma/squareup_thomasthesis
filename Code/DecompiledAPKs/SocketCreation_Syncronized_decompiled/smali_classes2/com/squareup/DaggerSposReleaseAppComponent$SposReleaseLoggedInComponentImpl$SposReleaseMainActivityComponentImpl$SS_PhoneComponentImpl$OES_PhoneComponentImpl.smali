.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OES_PhoneComponentImpl"
.end annotation


# instance fields
.field private addNoteScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private cartHeaderPhonePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private categoryDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CategoryDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private chargeAndTicketButtonFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private chargeAndTicketButtonsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private checkoutLibraryListAssistantProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
            ">;"
        }
    .end annotation
.end field

.field private checkoutLibraryListPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private clockSkewProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ClockSkew;",
            ">;"
        }
    .end annotation
.end field

.field private employeeLockButtonPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private keypadPanelPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadPanelPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private libraryBarPhonePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/LibraryBarPhonePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private lockOrClockButtonHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LockOrClockButtonHelper;",
            ">;"
        }
    .end annotation
.end field

.field private o1ReminderLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private orderEntryScreenConfiguratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenConfigurator;",
            ">;"
        }
    .end annotation
.end field

.field private orderEntryScreenEmvCardStatusProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private paymentPadPhonePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/PaymentPadPhonePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private phoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;",
            ">;"
        }
    .end annotation
.end field

.field private presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private startCashDrawerShiftPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;)V
    .locals 0

    .line 27406
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27408
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 27363
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 30

    move-object/from16 v0, p0

    .line 27413
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$81100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->checkoutLibraryListAssistantProvider:Ljavax/inject/Provider;

    .line 27414
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$30600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->access$80800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$101100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$53500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$60900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v13

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v14

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v16

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$37100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v17

    invoke-static {}, Lcom/squareup/CommonLoggedInModule_ProvideDiscountRulesLibraryFactoryFactory;->create()Lcom/squareup/CommonLoggedInModule_ProvideDiscountRulesLibraryFactoryFactory;

    move-result-object v18

    invoke-static {}, Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;->create()Lcom/squareup/CommonLoggedInModule_ProvideDiscountBundleFactoryFactory;

    move-result-object v19

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v20

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$101200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v21

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->checkoutLibraryListAssistantProvider:Ljavax/inject/Provider;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$101300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v23

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v24

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v25

    invoke-static/range {v2 .. v25}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->checkoutLibraryListPresenterProvider:Ljavax/inject/Provider;

    .line 27415
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$24800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$32200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$53300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    invoke-static/range {v2 .. v10}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryScreenEmvCardStatusProcessorProvider:Ljavax/inject/Provider;

    .line 27416
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$24800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->chargeAndTicketButtonFormatterProvider:Ljavax/inject/Provider;

    .line 27417
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->chargeAndTicketButtonFormatterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$24800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$54900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    invoke-static {}, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideGlassConfirmControllerFactory;->create()Lcom/squareup/ui/main/CommonMainActivityModule_ProvideGlassConfirmControllerFactory;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$31300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v13

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$33900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v14

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$33400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v16

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$53600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v17

    invoke-static {}, Lcom/squareup/x2/NoX2MainActivityModule_ProvideHodorScreenRunnerFactory;->create()Lcom/squareup/x2/NoX2MainActivityModule_ProvideHodorScreenRunnerFactory;

    move-result-object v18

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v19

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$34100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v20

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v21

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$38700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v22

    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;->create()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;

    move-result-object v23

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->access$100800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;)Ljavax/inject/Provider;

    move-result-object v24

    invoke-static {}, Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltySellerCartBannerFormatter_Factory;->create()Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltySellerCartBannerFormatter_Factory;

    move-result-object v25

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v26

    invoke-static/range {v2 .. v26}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->chargeAndTicketButtonsPresenterProvider:Ljavax/inject/Provider;

    .line 27418
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$51300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/applet/BadgePresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/applet/BadgePresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 27419
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$31000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$81100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$80900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryViewPagerPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;

    .line 27420
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$101100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v5}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/orderentry/CategoryDropDownPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/CategoryDropDownPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->categoryDropDownPresenterProvider:Ljavax/inject/Provider;

    .line 27421
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$101100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->categoryDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->phoneProvider:Ljavax/inject/Provider;

    .line 27422
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v5}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/orderentry/ClockSkew_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/ClockSkew_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->clockSkewProvider:Ljavax/inject/Provider;

    .line 27423
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$101900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$102000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {}, Lcom/squareup/x2/NoX2AppModule_ProvideMaybeSquareDeviceFactory;->create()Lcom/squareup/x2/NoX2AppModule_ProvideMaybeSquareDeviceFactory;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->o1ReminderLauncherProvider:Ljavax/inject/Provider;

    .line 27424
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$24800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$102100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$102200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$102300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$34600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$40000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v11, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v13, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->phoneProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$102400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v14

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->clockSkewProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$100900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v17

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$56600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v18

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v19

    invoke-static {}, Lcom/squareup/x2/NoX2MainActivityModule_ProvideHodorScreenRunnerFactory;->create()Lcom/squareup/x2/NoX2MainActivityModule_ProvideHodorScreenRunnerFactory;

    move-result-object v20

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$8200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v21

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v22

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$34000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v23

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->o1ReminderLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v25

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v26

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$80900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v27

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->access$100800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;)Ljavax/inject/Provider;

    move-result-object v28

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v29

    invoke-static/range {v2 .. v29}, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    .line 27425
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/main/AddNoteScreenRunner_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/main/AddNoteScreenRunner_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->addNoteScreenRunnerProvider:Ljavax/inject/Provider;

    .line 27426
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->access$100800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->addNoteScreenRunnerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$24800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$30600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$60700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$31300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$102500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v13

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$80900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v14

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v15

    invoke-static/range {v2 .. v15}, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->keypadPanelPresenterProvider:Ljavax/inject/Provider;

    .line 27427
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$36900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$26100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v5}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->startCashDrawerShiftPresenterProvider:Ljavax/inject/Provider;

    .line 27428
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$25700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {}, Lcom/squareup/x2/NoX2AppModule_ProvideMaybeSquareDeviceFactory;->create()Lcom/squareup/x2/NoX2AppModule_ProvideMaybeSquareDeviceFactory;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$21900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$83700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->lockOrClockButtonHelperProvider:Ljavax/inject/Provider;

    .line 27429
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$21900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$25700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->lockOrClockButtonHelperProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v5, v5, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v5}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->employeeLockButtonPresenterProvider:Ljavax/inject/Provider;

    .line 27430
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$24800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$102200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$31300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->cartHeaderPhonePresenterProvider:Ljavax/inject/Provider;

    .line 27431
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/orderentry/OrderEntryScreenConfigurator_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreenConfigurator_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryScreenConfiguratorProvider:Ljavax/inject/Provider;

    .line 27432
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$31000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$80900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryScreenConfiguratorProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    invoke-static/range {v2 .. v8}, Lcom/squareup/orderentry/PaymentPadPhonePresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/PaymentPadPhonePresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->paymentPadPhonePresenterProvider:Ljavax/inject/Provider;

    .line 27433
    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->checkoutLibraryListPresenterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->categoryDropDownPresenterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$101100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/squareup/orderentry/LibraryBarPhonePresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/LibraryBarPhonePresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->libraryBarPhonePresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectCartHeaderPhoneView(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;)Lcom/squareup/ui/cart/header/CartHeaderPhoneView;
    .locals 1

    .line 27557
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->cartHeaderPhonePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView_MembersInjector;->injectPresenter(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;)V

    return-object p1
.end method

.method private injectCategoryDropDownView(Lcom/squareup/orderentry/CategoryDropDownView;)Lcom/squareup/orderentry/CategoryDropDownView;
    .locals 1

    .line 27562
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->categoryDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/CategoryDropDownView_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/CategoryDropDownView;Lcom/squareup/orderentry/CategoryDropDownPresenter;)V

    .line 27563
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/CategoryDropDownView_MembersInjector;->injectCatalogIntegrationController(Lcom/squareup/orderentry/CategoryDropDownView;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object p1
.end method

.method private injectChargeAndTicketsButtons(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Lcom/squareup/orderentry/ChargeAndTicketsButtons;
    .locals 1

    .line 27503
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->chargeAndTicketButtonsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/ChargeAndTicketsButtons;Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    return-object p1
.end method

.method private injectCheckoutLibraryListView(Lcom/squareup/orderentry/CheckoutLibraryListView;)Lcom/squareup/orderentry/CheckoutLibraryListView;
    .locals 1

    .line 27539
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->checkoutLibraryListPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/librarylist/LibraryListView_MembersInjector;->injectPresenter(Lcom/squareup/librarylist/LibraryListView;Lcom/squareup/librarylist/LibraryListPresenter;)V

    return-object p1
.end method

.method private injectEmployeeLockButton(Lcom/squareup/ui/permissions/EmployeeLockButton;)Lcom/squareup/ui/permissions/EmployeeLockButton;
    .locals 1

    .line 27551
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->employeeLockButtonPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/permissions/EmployeeLockButton_MembersInjector;->injectPresenter(Lcom/squareup/ui/permissions/EmployeeLockButton;Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;)V

    .line 27552
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/permissions/EmployeeLockButton_MembersInjector;->injectDevice(Lcom/squareup/ui/permissions/EmployeeLockButton;Lcom/squareup/util/Device;)V

    return-object p1
.end method

.method private injectKeypadPanel(Lcom/squareup/orderentry/KeypadPanel;)Lcom/squareup/orderentry/KeypadPanel;
    .locals 1

    .line 27532
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->injectDevice(Lcom/squareup/orderentry/KeypadPanel;Lcom/squareup/util/Device;)V

    .line 27533
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->keypadPanelPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/KeypadPanel_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/KeypadPanel;Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    return-object p1
.end method

.method private injectLibraryBarPhone(Lcom/squareup/orderentry/LibraryBarPhone;)Lcom/squareup/orderentry/LibraryBarPhone;
    .locals 1

    .line 27573
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->libraryBarPhonePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/LibraryBarPhone_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/LibraryBarPhone;Lcom/squareup/orderentry/LibraryBarPhonePresenter;)V

    return-object p1
.end method

.method private injectLibraryListView(Lcom/squareup/librarylist/LibraryListView;)Lcom/squareup/librarylist/LibraryListView;
    .locals 1

    .line 27497
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->checkoutLibraryListPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/librarylist/LibraryListView_MembersInjector;->injectPresenter(Lcom/squareup/librarylist/LibraryListView;Lcom/squareup/librarylist/LibraryListPresenter;)V

    return-object p1
.end method

.method private injectOrderEntryBadgeView(Lcom/squareup/orderentry/OrderEntryBadgeView;)Lcom/squareup/orderentry/OrderEntryBadgeView;
    .locals 1

    .line 27508
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;->injectDevice(Lcom/squareup/orderentry/OrderEntryBadgeView;Lcom/squareup/util/Device;)V

    return-object p1
.end method

.method private injectOrderEntryDrawerButton(Lcom/squareup/orderentry/OrderEntryDrawerButton;)Lcom/squareup/orderentry/OrderEntryDrawerButton;
    .locals 1

    .line 27514
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->injectBadgePresenter(Lcom/squareup/orderentry/OrderEntryDrawerButton;Lcom/squareup/applet/BadgePresenter;)V

    .line 27515
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$56600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryDrawerButton_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/OrderEntryDrawerButton;Lcom/squareup/applet/AppletsDrawerPresenter;)V

    return-object p1
.end method

.method private injectOrderEntryView(Lcom/squareup/orderentry/OrderEntryView;)Lcom/squareup/orderentry/OrderEntryView;
    .locals 1

    .line 27526
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryView_MembersInjector;->injectDevice(Lcom/squareup/orderentry/OrderEntryView;Lcom/squareup/util/Device;)V

    .line 27527
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryView_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/OrderEntryView;Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)V

    return-object p1
.end method

.method private injectOrderEntryViewPager(Lcom/squareup/orderentry/OrderEntryViewPager;)Lcom/squareup/orderentry/OrderEntryViewPager;
    .locals 1

    .line 27520
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/OrderEntryViewPager;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V

    .line 27521
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$80900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->injectTutorialCore(Lcom/squareup/orderentry/OrderEntryViewPager;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object p1
.end method

.method private injectPaymentPadPhoneView(Lcom/squareup/orderentry/PaymentPadPhoneView;)Lcom/squareup/orderentry/PaymentPadPhoneView;
    .locals 1

    .line 27568
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->paymentPadPhonePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/PaymentPadPhoneView_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/PaymentPadPhoneView;Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V

    return-object p1
.end method

.method private injectStartCashDrawerShiftLayout(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;
    .locals 1

    .line 27545
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->startCashDrawerShiftPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout_MembersInjector;->injectPresenter(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;)V

    .line 27546
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;Lcom/squareup/money/PriceLocaleHelper;)V

    return-object p1
.end method


# virtual methods
.method public homeScreenEmvCardStatusProcessor()Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;
    .locals 1

    .line 27442
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->orderEntryScreenEmvCardStatusProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;

    return-object v0
.end method

.method public inject(Lcom/squareup/librarylist/LibraryListView;)V
    .locals 0

    .line 27438
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectLibraryListView(Lcom/squareup/librarylist/LibraryListView;)Lcom/squareup/librarylist/LibraryListView;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/CategoryDropDownView;)V
    .locals 0

    .line 27486
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectCategoryDropDownView(Lcom/squareup/orderentry/CategoryDropDownView;)Lcom/squareup/orderentry/CategoryDropDownView;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V
    .locals 0

    .line 27446
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectChargeAndTicketsButtons(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/CheckoutLibraryListView;)V
    .locals 0

    .line 27470
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectCheckoutLibraryListView(Lcom/squareup/orderentry/CheckoutLibraryListView;)Lcom/squareup/orderentry/CheckoutLibraryListView;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/KeypadPanel;)V
    .locals 0

    .line 27466
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectKeypadPanel(Lcom/squareup/orderentry/KeypadPanel;)Lcom/squareup/orderentry/KeypadPanel;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/LibraryBarPhone;)V
    .locals 0

    .line 27494
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectLibraryBarPhone(Lcom/squareup/orderentry/LibraryBarPhone;)Lcom/squareup/orderentry/LibraryBarPhone;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/OrderEntryBadgeView;)V
    .locals 0

    .line 27450
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectOrderEntryBadgeView(Lcom/squareup/orderentry/OrderEntryBadgeView;)Lcom/squareup/orderentry/OrderEntryBadgeView;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/OrderEntryDrawerButton;)V
    .locals 0

    .line 27454
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectOrderEntryDrawerButton(Lcom/squareup/orderentry/OrderEntryDrawerButton;)Lcom/squareup/orderentry/OrderEntryDrawerButton;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/OrderEntryView;)V
    .locals 0

    .line 27462
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectOrderEntryView(Lcom/squareup/orderentry/OrderEntryView;)Lcom/squareup/orderentry/OrderEntryView;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/OrderEntryViewPager;)V
    .locals 0

    .line 27458
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectOrderEntryViewPager(Lcom/squareup/orderentry/OrderEntryViewPager;)Lcom/squareup/orderentry/OrderEntryViewPager;

    return-void
.end method

.method public inject(Lcom/squareup/orderentry/PaymentPadPhoneView;)V
    .locals 0

    .line 27490
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectPaymentPadPhoneView(Lcom/squareup/orderentry/PaymentPadPhoneView;)Lcom/squareup/orderentry/PaymentPadPhoneView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;)V
    .locals 0

    .line 27482
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectCartHeaderPhoneView(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;)Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/permissions/EmployeeLockButton;)V
    .locals 0

    .line 27478
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectEmployeeLockButton(Lcom/squareup/ui/permissions/EmployeeLockButton;)Lcom/squareup/ui/permissions/EmployeeLockButton;

    return-void
.end method

.method public inject(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V
    .locals 0

    .line 27474
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_PhoneComponentImpl$OES_PhoneComponentImpl;->injectStartCashDrawerShiftLayout(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    return-void
.end method
