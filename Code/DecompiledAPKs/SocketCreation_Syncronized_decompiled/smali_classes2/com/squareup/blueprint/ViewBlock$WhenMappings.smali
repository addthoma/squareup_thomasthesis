.class public final synthetic Lcom/squareup/blueprint/ViewBlock$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/blueprint/HorizontalAlign;->values()[Lcom/squareup/blueprint/HorizontalAlign;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->START:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v1}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v1}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->END:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v1}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/blueprint/VerticalAlign;->values()[Lcom/squareup/blueprint/VerticalAlign;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v1}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v1}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/blueprint/VerticalAlign;->BOTTOM:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v1}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
