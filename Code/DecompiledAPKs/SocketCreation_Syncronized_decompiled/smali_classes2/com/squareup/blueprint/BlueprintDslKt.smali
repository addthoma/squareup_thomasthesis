.class public final Lcom/squareup/blueprint/BlueprintDslKt;
.super Ljava/lang/Object;
.source "BlueprintDsl.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBlueprintDsl.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt\n*L\n1#1,165:1\n117#1,5:166\n119#1,4:171\n129#1,2:175\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a9\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u00042#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00010\u0007\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t\u001aM\u0010\u0000\u001a\u00020\u0001*\u00020\n2\u0008\u0008\u0003\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c2#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00010\u0007\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t\u001aC\u0010\u0000\u001a\u00020\u0001*\u00020\u000e2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c2#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00010\u0007\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t\u001aw\u0010\u000f\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0011\"\u0008\u0008\u0001\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u00072\u0019\u0008\u0002\u0010\u0014\u001a\u0013\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t2\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00162#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u0017\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001a*\u0010\u0018\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00120\u00072\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u001aR\u0010\u0019\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0011\"\u0008\u0008\u0001\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u00072#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u001a\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001af\u0010\u001b\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0011\"\u0008\u0008\u0001\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u00072\u0008\u0008\u0002\u0010\u001c\u001a\u00020\u001d2\u0008\u0008\u0002\u0010\u001e\u001a\u00020\u001d2#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u001f\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001a?\u0010\u0014\u001a\u0002H\u0012\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0013*\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u00120 2\u0017\u0010\u0005\u001a\u0013\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u00a2\u0006\u0002\u0010!\u001aM\u0010\"\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00120\u00072\u0017\u0010#\u001a\u0013\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t2\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020&0\u0006\u001aH\u0010\"\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00120\u00072\u0008\u0008\u0002\u0010\'\u001a\u00020\u000c2\u0008\u0008\u0002\u0010(\u001a\u00020\u000c2\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020&0\u0006\u001aR\u0010)\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0011\"\u0008\u0008\u0001\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u00072#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120*\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001aR\u0010+\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0011\"\u0008\u0008\u0001\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u00072#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120,\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001aR\u0010-\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0011\"\u0008\u0008\u0001\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120\u00072#\u0010\u0005\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00120.\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001aX\u0010/\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00120\u00072\u0017\u0010\u0014\u001a\u0013\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t2\u001d\u0010\u0005\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u001200\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t\u001a?\u0010/\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0013*\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00120\u00072\u001d\u0010\u0005\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u001200\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\t\u00a8\u00061"
    }
    d2 = {
        "blueprint",
        "",
        "Landroid/app/Activity;",
        "id",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/ViewsUpdateContext;",
        "Lkotlin/ExtensionFunctionType;",
        "Landroid/view/ViewGroup;",
        "extendHorizontally",
        "",
        "extendVertically",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "center",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "params",
        "type",
        "Lcom/squareup/blueprint/CenterBlock$Type;",
        "Lcom/squareup/blueprint/CenterBlock;",
        "existing",
        "horizontal",
        "Lcom/squareup/blueprint/HorizontalBlock;",
        "inset",
        "horizontalInset",
        "Lcom/squareup/resources/DimenModel;",
        "verticalInset",
        "Lcom/squareup/blueprint/InsetBlock;",
        "Lcom/squareup/blueprint/Block;",
        "(Lcom/squareup/blueprint/Block;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "simple",
        "paramsBlock",
        "viewFactory",
        "Landroid/content/Context;",
        "Landroid/view/View;",
        "wrapHorizontally",
        "wrapVertically",
        "spreadHorizontal",
        "Lcom/squareup/blueprint/SpreadHorizontalBlock;",
        "spreadVertical",
        "Lcom/squareup/blueprint/SpreadVerticalBlock;",
        "vertical",
        "Lcom/squareup/blueprint/VerticalBlock;",
        "view",
        "Lcom/squareup/blueprint/CreateViewBlock;",
        "blueprint-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final blueprint(Landroid/app/Activity;ILkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$blueprint"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 23
    new-instance p1, Lcom/squareup/blueprint/OneElementContext;

    sget-object v0, Lcom/squareup/blueprint/BlueprintDslKt$blueprint$context$1;->INSTANCE:Lcom/squareup/blueprint/BlueprintDslKt$blueprint$context$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v0}, Lcom/squareup/blueprint/OneElementContext;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v1, Lcom/squareup/blueprint/Blueprint;

    new-instance p2, Lcom/squareup/blueprint/ViewsUpdateContext;

    const-string v0, "root"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p0}, Lcom/squareup/blueprint/ViewsUpdateContext;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    invoke-direct {v1, p2}, Lcom/squareup/blueprint/Blueprint;-><init>(Lcom/squareup/blueprint/ViewsUpdateContext;)V

    invoke-virtual {p1}, Lcom/squareup/blueprint/OneElementContext;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/blueprint/Blueprint;->update$default(Lcom/squareup/blueprint/Blueprint;Lcom/squareup/blueprint/Block;ZZILjava/lang/Object;)V

    return-void
.end method

.method public static final blueprint(Landroid/view/ViewGroup;IZZLkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "IZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$blueprint"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 47
    new-instance p1, Lcom/squareup/blueprint/OneElementContext;

    sget-object v0, Lcom/squareup/blueprint/BlueprintDslKt$blueprint$context$3;->INSTANCE:Lcom/squareup/blueprint/BlueprintDslKt$blueprint$context$3;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v0}, Lcom/squareup/blueprint/OneElementContext;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-interface {p4, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    new-instance p4, Lcom/squareup/blueprint/Blueprint;

    new-instance v0, Lcom/squareup/blueprint/ViewsUpdateContext;

    const-string v1, "root"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0}, Lcom/squareup/blueprint/ViewsUpdateContext;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    invoke-direct {p4, v0}, Lcom/squareup/blueprint/Blueprint;-><init>(Lcom/squareup/blueprint/ViewsUpdateContext;)V

    .line 49
    invoke-virtual {p1}, Lcom/squareup/blueprint/OneElementContext;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object p0

    .line 48
    invoke-virtual {p4, p0, p2, p3}, Lcom/squareup/blueprint/Blueprint;->update(Lcom/squareup/blueprint/Block;ZZ)V

    return-void
.end method

.method public static final blueprint(Landroidx/constraintlayout/widget/ConstraintLayout;ZZLkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/constraintlayout/widget/ConstraintLayout;",
            "ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$blueprint"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/blueprint/OneElementContext;

    sget-object v1, Lcom/squareup/blueprint/BlueprintDslKt$blueprint$context$2;->INSTANCE:Lcom/squareup/blueprint/BlueprintDslKt$blueprint$context$2;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/OneElementContext;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-interface {p3, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance p3, Lcom/squareup/blueprint/Blueprint;

    new-instance v1, Lcom/squareup/blueprint/ViewsUpdateContext;

    invoke-direct {v1, p0}, Lcom/squareup/blueprint/ViewsUpdateContext;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/Blueprint;-><init>(Lcom/squareup/blueprint/ViewsUpdateContext;)V

    .line 34
    invoke-virtual {v0}, Lcom/squareup/blueprint/OneElementContext;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object p0

    .line 33
    invoke-virtual {p3, p0, p1, p2}, Lcom/squareup/blueprint/Blueprint;->update(Lcom/squareup/blueprint/Block;ZZ)V

    return-void
.end method

.method public static synthetic blueprint$default(Landroid/view/ViewGroup;IZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const/4 p1, -0x1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x1

    if-eqz p6, :cond_1

    const/4 p2, 0x1

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    const/4 p3, 0x1

    .line 43
    :cond_2
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/BlueprintDslKt;->blueprint(Landroid/view/ViewGroup;IZZLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static synthetic blueprint$default(Landroidx/constraintlayout/widget/ConstraintLayout;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    const/4 p2, 0x1

    .line 29
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/blueprint/BlueprintDslKt;->blueprint(Landroidx/constraintlayout/widget/ConstraintLayout;ZZLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final center(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;Lcom/squareup/blueprint/CenterBlock$Type;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TP;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/blueprint/CenterBlock$Type;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/CenterBlock<",
            "TC;TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$center"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    new-instance p1, Lcom/squareup/blueprint/CenterBlock;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 121
    invoke-interface {p3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/squareup/blueprint/Block;

    .line 117
    invoke-interface {p0, p1}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static synthetic center$default(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;Lcom/squareup/blueprint/CenterBlock$Type;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 6

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    .line 113
    sget-object p1, Lcom/squareup/blueprint/BlueprintDslKt$center$1;->INSTANCE:Lcom/squareup/blueprint/BlueprintDslKt$center$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    .line 114
    sget-object p2, Lcom/squareup/blueprint/CenterBlock$Type;->BOTH:Lcom/squareup/blueprint/CenterBlock$Type;

    :cond_1
    move-object v2, p2

    const-string p2, "$this$center"

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "params"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "type"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "block"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 167
    new-instance p1, Lcom/squareup/blueprint/CenterBlock;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 170
    invoke-interface {p3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/squareup/blueprint/Block;

    .line 166
    invoke-interface {p0, p1}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final existing(Lcom/squareup/blueprint/BlueprintContext;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "TP;>;I)V"
        }
    .end annotation

    const-string v0, "$this$existing"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/blueprint/ExistingViewBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/blueprint/ExistingViewBlock;-><init>(Ljava/lang/Object;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 106
    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/ExistingViewBlock;->setId(I)V

    const/4 p1, 0x1

    .line 107
    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/ExistingViewBlock;->setWrapHorizontally(Z)V

    .line 108
    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/ExistingViewBlock;->setWrapVertically(Z)V

    .line 109
    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final horizontal(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/HorizontalBlock<",
            "TC;TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$horizontal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    new-instance v0, Lcom/squareup/blueprint/HorizontalBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/blueprint/HorizontalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final inset(Lcom/squareup/blueprint/BlueprintContext;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lkotlin/jvm/functions/Function1;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;TP;>;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/InsetBlock<",
            "TC;TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$inset"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "horizontalInset"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verticalInset"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v0, Lcom/squareup/blueprint/InsetBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf8

    const/4 v11, 0x0

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v11}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p3, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static synthetic inset$default(Lcom/squareup/blueprint/BlueprintContext;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p3

    and-int/lit8 v2, p4, 0x1

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 125
    invoke-static {v3}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v2

    check-cast v2, Lcom/squareup/resources/DimenModel;

    move-object v6, v2

    goto :goto_0

    :cond_0
    move-object/from16 v6, p1

    :goto_0
    and-int/lit8 v2, p4, 0x2

    if-eqz v2, :cond_1

    .line 126
    invoke-static {v3}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v2

    check-cast v2, Lcom/squareup/resources/DimenModel;

    move-object v7, v2

    goto :goto_1

    :cond_1
    move-object/from16 v7, p2

    :goto_1
    const-string v2, "$this$inset"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "horizontalInset"

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v2, "verticalInset"

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "block"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    new-instance v2, Lcom/squareup/blueprint/InsetBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xf8

    const/4 v14, 0x0

    move-object v4, v2

    invoke-direct/range {v4 .. v14}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v2}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final params(Lcom/squareup/blueprint/Block;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/Block<",
            "*TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TP;",
            "Lkotlin/Unit;",
            ">;)TP;"
        }
    .end annotation

    const-string v0, "$this$params"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object p0

    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public static final simple(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TP;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$simple"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paramsBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/squareup/blueprint/CreateViewBlock;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/CreateViewBlock;-><init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v0, 0x1

    .line 85
    invoke-virtual {p1, v0}, Lcom/squareup/blueprint/CreateViewBlock;->setWrapHorizontally(Z)V

    .line 86
    invoke-virtual {p1, v0}, Lcom/squareup/blueprint/CreateViewBlock;->setWrapVertically(Z)V

    .line 87
    invoke-virtual {p1, p2}, Lcom/squareup/blueprint/CreateViewBlock;->setViewFactory$blueprint_core_release(Lkotlin/jvm/functions/Function1;)V

    .line 88
    check-cast p1, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, p1}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final simple(Lcom/squareup/blueprint/BlueprintContext;ZZLkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "TP;>;ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$simple"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/squareup/blueprint/CreateViewBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/CreateViewBlock;-><init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 74
    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/CreateViewBlock;->setWrapHorizontally(Z)V

    .line 75
    invoke-virtual {v0, p2}, Lcom/squareup/blueprint/CreateViewBlock;->setWrapVertically(Z)V

    .line 76
    invoke-virtual {v0, p3}, Lcom/squareup/blueprint/CreateViewBlock;->setViewFactory$blueprint_core_release(Lkotlin/jvm/functions/Function1;)V

    .line 77
    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static synthetic simple$default(Lcom/squareup/blueprint/BlueprintContext;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    const/4 p2, 0x1

    .line 71
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/blueprint/BlueprintDslKt;->simple(Lcom/squareup/blueprint/BlueprintContext;ZZLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final spreadHorizontal(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/SpreadHorizontalBlock<",
            "TC;TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$spreadHorizontal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/squareup/blueprint/SpreadHorizontalBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/blueprint/SpreadHorizontalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final spreadVertical(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/SpreadVerticalBlock<",
            "TC;TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$spreadVertical"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    new-instance v0, Lcom/squareup/blueprint/SpreadVerticalBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/blueprint/SpreadVerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final vertical(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/VerticalBlock<",
            "TC;TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$vertical"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    new-instance v0, Lcom/squareup/blueprint/VerticalBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/blueprint/VerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final view(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/CreateViewBlock<",
            "TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$view"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/squareup/blueprint/CreateViewBlock;

    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/CreateViewBlock;-><init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final view(Lcom/squareup/blueprint/BlueprintContext;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/ViewsUpdateContext;",
            "TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TP;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/CreateViewBlock<",
            "TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$view"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/squareup/blueprint/CreateViewBlock;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/CreateViewBlock;-><init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/squareup/blueprint/Block;

    invoke-interface {p0, p1}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method
