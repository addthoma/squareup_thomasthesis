.class public final Lcom/squareup/blueprint/mosaic/BlueprintUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "BlueprintUiModel.kt"

# interfaces
.implements Lcom/squareup/blueprint/BlueprintContext;


# annotations
.annotation runtime Lcom/squareup/mosaic/core/MosaicDslMarker;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "TP;>;",
        "Lcom/squareup/blueprint/BlueprintContext<",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u00032\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005B5\u0012\u0006\u0010\u0008\u001a\u00028\u0000\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0016\u0008\u0002\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\r\u00a2\u0006\u0002\u0010\u000eJ\u001c\u0010\u001c\u001a\u00020\u00072\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\rH\u0016J\u0008\u0010\u001e\u001a\u00020\u0007H\u0016J\u0018\u0010\u001f\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030 2\u0006\u0010!\u001a\u00020\"H\u0016R(\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001a\u0010\u000b\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0014\"\u0004\u0008\u0018\u0010\u0016R\u0016\u0010\u0008\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u001b\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/blueprint/mosaic/BlueprintUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "",
        "params",
        "extendHorizontally",
        "",
        "extendVertically",
        "blueprintModel",
        "Lcom/squareup/blueprint/Block;",
        "(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;)V",
        "getBlueprintModel",
        "()Lcom/squareup/blueprint/Block;",
        "setBlueprintModel",
        "(Lcom/squareup/blueprint/Block;)V",
        "getExtendHorizontally",
        "()Z",
        "setExtendHorizontally",
        "(Z)V",
        "getExtendVertically",
        "setExtendVertically",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "add",
        "element",
        "createParams",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private blueprintModel:Lcom/squareup/blueprint/Block;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private extendHorizontally:Z

.field private extendVertically:Z

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;ZZ",
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->params:Ljava/lang/Object;

    iput-boolean p2, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->extendHorizontally:Z

    iput-boolean p3, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->extendVertically:Z

    iput-object p4, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->blueprintModel:Lcom/squareup/blueprint/Block;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 40
    check-cast p4, Lcom/squareup/blueprint/Block;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;)V

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->blueprintModel:Lcom/squareup/blueprint/Block;

    return-void
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->createParams()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public createParams()V
    .locals 0

    return-void
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public final getBlueprintModel()Lcom/squareup/blueprint/Block;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->blueprintModel:Lcom/squareup/blueprint/Block;

    return-object v0
.end method

.method public final getExtendHorizontally()Z
    .locals 1

    .line 38
    iget-boolean v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->extendHorizontally:Z

    return v0
.end method

.method public final getExtendVertically()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->extendVertically:Z

    return v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final setBlueprintModel(Lcom/squareup/blueprint/Block;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 40
    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->blueprintModel:Lcom/squareup/blueprint/Block;

    return-void
.end method

.method public final setExtendHorizontally(Z)V
    .locals 0

    .line 38
    iput-boolean p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->extendHorizontally:Z

    return-void
.end method

.method public final setExtendVertically(Z)V
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->extendVertically:Z

    return-void
.end method
