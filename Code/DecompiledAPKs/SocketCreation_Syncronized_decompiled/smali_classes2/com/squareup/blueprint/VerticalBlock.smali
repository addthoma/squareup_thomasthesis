.class public final Lcom/squareup/blueprint/VerticalBlock;
.super Lcom/squareup/blueprint/LinearBlock;
.source "VerticalBlock.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/LinearBlock<",
        "TC;TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalBlock.kt\ncom/squareup/blueprint/VerticalBlock\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,288:1\n1499#2,3:289\n300#2,7:292\n1313#2:299\n1382#2,3:300\n1600#2,3:303\n1591#2,2:306\n1591#2,2:308\n1591#2,2:310\n1591#2,2:312\n1600#2,3:314\n1600#2,3:317\n1600#2,3:320\n1591#2,2:323\n1084#3,2:325\n1084#3,2:327\n1084#3,2:329\n*E\n*S KotlinDebug\n*F\n+ 1 VerticalBlock.kt\ncom/squareup/blueprint/VerticalBlock\n*L\n60#1,3:289\n65#1,7:292\n67#1:299\n67#1,3:300\n69#1,3:303\n100#1,2:306\n120#1,2:308\n128#1,2:310\n143#1,2:312\n182#1,3:314\n194#1,3:317\n220#1,3:320\n234#1,2:323\n252#1,2:325\n256#1,2:327\n282#1,2:329\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005B3\u0012\u0006\u0010\u0006\u001a\u00028\u0001\u0012\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ%\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00028\u00002\u0006\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\u000cH\u0016\u00a2\u0006\u0002\u0010 J \u0010!\u001a\u00020\u000c2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000cH\u0016J \u0010&\u001a\u00020\u000c2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000cH\u0016J\u000e\u0010\'\u001a\u00028\u0001H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0017J\u001b\u0010(\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008H\u00c4\u0003J\t\u0010)\u001a\u00020\u000cH\u00c2\u0003J \u0010*\u001a\u00020+2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010,\u001a\u00020+2\u0006\u0010-\u001a\u00020.H\u0016J \u0010/\u001a\u00020+2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010,\u001a\u00020+2\u0006\u0010-\u001a\u000200H\u0016JJ\u00101\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00028\u00012\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u00082\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001\u00a2\u0006\u0002\u00102J\u0013\u00103\u001a\u00020\u000f2\u0008\u00104\u001a\u0004\u0018\u00010\u0004H\u00d6\u0003J\t\u00105\u001a\u00020\u000cH\u00d6\u0001J\u0010\u00106\u001a\u0002072\u0006\u0010\"\u001a\u00020\u0002H\u0016J\t\u00108\u001a\u000209H\u00d6\u0001J\u0010\u0010:\u001a\u00020+2\u0006\u0010\"\u001a\u00020\u0002H\u0016R\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0011R&\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u00028\u0001X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0018\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/blueprint/VerticalBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/LinearBlock;",
        "params",
        "elements",
        "",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "extendIndex",
        "",
        "(Ljava/lang/Object;Ljava/util/List;I)V",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "getElements",
        "()Ljava/util/List;",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "spacing",
        "",
        "buildViews",
        "",
        "updateContext",
        "width",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "component1",
        "component2",
        "component3",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "copy",
        "(Ljava/lang/Object;Ljava/util/List;I)Lcom/squareup/blueprint/VerticalBlock;",
        "equals",
        "other",
        "hashCode",
        "startIds",
        "Lcom/squareup/blueprint/IdAndMarginCollection;",
        "toString",
        "",
        "topIds",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation
.end field

.field private extendIndex:I

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private spacing:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;I)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/blueprint/LinearBlock;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/VerticalBlock;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/blueprint/VerticalBlock;->elements:Ljava/util/List;

    iput p3, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 52
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/List;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, -0x1

    .line 53
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/blueprint/VerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;I)V

    return-void
.end method

.method private final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/VerticalBlock;Ljava/lang/Object;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/blueprint/VerticalBlock;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/VerticalBlock;->copy(Ljava/lang/Object;Ljava/util/List;I)Lcom/squareup/blueprint/VerticalBlock;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    .line 293
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 294
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 65
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {v3}, Lcom/squareup/blueprint/LinearBlock$Params;->getExtend()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    .line 298
    :goto_1
    iput v2, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    .line 66
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 299
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 300
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 301
    check-cast v4, Lcom/squareup/blueprint/Block;

    .line 67
    invoke-virtual {v4}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {v4}, Lcom/squareup/blueprint/LinearBlock$Params;->getSpacing$blueprint_core_release()Lcom/squareup/resources/DimenModel;

    move-result-object v4

    const-string v5, "context"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 302
    :cond_2
    check-cast v3, Ljava/util/List;

    iput-object v3, p0, Lcom/squareup/blueprint/VerticalBlock;->spacing:Ljava/util/List;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 304
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_3

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 71
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getDependableHorizontally()Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, p2

    goto :goto_4

    :cond_4
    const/4 v5, 0x0

    .line 74
    :goto_4
    iget v6, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    if-ne v2, v6, :cond_5

    move v2, p3

    goto :goto_5

    :cond_5
    const/4 v2, -0x2

    .line 75
    :goto_5
    invoke-virtual {v3, p1, v5, v2}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    move v2, v4

    goto :goto_3

    :cond_6
    return-void
.end method

.method public chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    invoke-virtual {p2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v0

    .line 245
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/blueprint/Block;->chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result v1

    .line 248
    invoke-virtual {p2, v0}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v2

    sub-int/2addr v2, p3

    .line 249
    new-instance p3, Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p2, v0}, Lcom/squareup/blueprint/ChainInfo;->idAt(I)I

    move-result v0

    const/4 v3, 0x6

    invoke-direct {p3, v0, v3, v2}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 250
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p2}, Lcom/squareup/blueprint/ChainInfo;->lastId()I

    move-result p2

    const/4 v2, 0x7

    invoke-direct {v0, p2, v2, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 251
    new-instance p2, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p2, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 252
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lkotlin/sequences/SequencesKt;->drop(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object v3

    .line 325
    invoke-interface {v3}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/blueprint/Block;

    .line 252
    invoke-virtual {v5, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 253
    :cond_0
    check-cast p2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 255
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 256
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/sequences/SequencesKt;->drop(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object v2

    .line 327
    invoke-interface {v2}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 258
    move-object v4, p2

    check-cast v4, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v5, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v3, p1, v4, v5}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    .line 257
    invoke-virtual {p3, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_1

    .line 260
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 261
    move-object p2, p3

    check-cast p2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 262
    invoke-virtual {p3, p2, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    :cond_2
    return v1
.end method

.method public chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getDependableVertically()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 282
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/blueprint/VerticalBlock;->spacing:Ljava/util/List;

    if-nez v1, :cond_0

    const-string v2, "spacing"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 329
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    invoke-virtual {v1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    add-int/2addr v1, p3

    .line 283
    invoke-virtual {v2, p1, p2, v1}, Lcom/squareup/blueprint/Block;->chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result p3

    goto :goto_0

    :cond_1
    return p3

    .line 273
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot chain inside VerticalBlock if there\'s a \'remaining\' item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    sget-object v0, Lcom/squareup/blueprint/VerticalBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    const/4 v1, 0x7

    if-eq p3, v0, :cond_8

    const/4 v0, 0x2

    if-eq p3, v0, :cond_6

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    .line 126
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v0, 0x6

    invoke-direct {p3, v0}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 127
    new-instance v2, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v2, v0}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 310
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 129
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getDependableHorizontally()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 130
    invoke-virtual {v3, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {v3, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 135
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138
    invoke-virtual {p3, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 139
    invoke-interface {p2, p3, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 140
    check-cast v2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p3, p1, v2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 141
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 142
    new-instance v1, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-interface {p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result p2

    invoke-direct {v1, p2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 143
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 312
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 144
    invoke-virtual {v2}, Lcom/squareup/blueprint/Block;->getDependableHorizontally()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 146
    move-object v3, v1

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/HorizontalAlign;->END:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    goto :goto_2

    .line 148
    :cond_2
    move-object v3, p3

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    .line 144
    :goto_2
    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_1

    .line 151
    :cond_3
    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    return-object v0

    .line 135
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "VerticalBlock aligned RIGHT need at least one dependable item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 151
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 118
    :cond_6
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 119
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 308
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    .line 121
    move-object v2, p2

    check-cast v2, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v3, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_3

    .line 123
    :cond_7
    check-cast p3, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p3

    .line 97
    :cond_8
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 98
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 99
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 306
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 101
    invoke-virtual {v2}, Lcom/squareup/blueprint/Block;->getDependableHorizontally()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 102
    move-object v3, p2

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/HorizontalAlign;->START:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_4

    .line 104
    :cond_9
    move-object v3, p2

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_4

    .line 107
    :cond_a
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_c

    .line 110
    invoke-virtual {v0}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_b

    .line 111
    invoke-virtual {p3, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 112
    move-object v1, v0

    check-cast v1, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p2, p1, v1}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 113
    invoke-virtual {v0, p2, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 115
    :cond_b
    check-cast p3, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p3

    .line 107
    :cond_c
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "VerticalBlock aligned LEFT need at least one dependable item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    sget-object v0, Lcom/squareup/blueprint/VerticalBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x0

    const-string v1, "spacing"

    const/4 v2, 0x1

    if-eq p3, v2, :cond_b

    const/4 v2, 0x2

    if-eq p3, v2, :cond_5

    const/4 v2, 0x3

    if-ne p3, v2, :cond_4

    .line 217
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getDependableVertically()Z

    move-result p3

    if-eqz p3, :cond_3

    .line 220
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/lang/Iterable;

    .line 321
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    if-gez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 223
    iget-object v4, p0, Lcom/squareup/blueprint/VerticalBlock;->spacing:Ljava/util/List;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 224
    sget-object v0, Lcom/squareup/blueprint/VerticalAlign;->BOTTOM:Lcom/squareup/blueprint/VerticalAlign;

    .line 221
    invoke-virtual {v2, p1, p2, v0}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    move v0, v3

    goto :goto_0

    :cond_2
    return-object p2

    .line 217
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "VerticalBlock aligned BOTTOM cannot have a \'remaining\' block."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 227
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 193
    :cond_5
    sget-object p3, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    .line 194
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 318
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    if-gez v0, :cond_6

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_6
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 195
    iget v5, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    if-eq v5, v0, :cond_8

    .line 199
    iget-object v5, p0, Lcom/squareup/blueprint/VerticalBlock;->spacing:Ljava/util/List;

    if-nez v5, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 197
    invoke-virtual {v3, p1, p2, p3}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    goto :goto_2

    .line 206
    :cond_8
    iget-object p3, p0, Lcom/squareup/blueprint/VerticalBlock;->spacing:Ljava/util/List;

    if-nez p3, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    invoke-interface {p2, p3}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 207
    sget-object p3, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    .line 204
    invoke-virtual {v3, p1, p2, p3}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 210
    sget-object p3, Lcom/squareup/blueprint/VerticalAlign;->BOTTOM:Lcom/squareup/blueprint/VerticalAlign;

    :goto_2
    move v0, v4

    goto :goto_1

    :cond_a
    return-object p2

    .line 179
    :cond_b
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getDependableVertically()Z

    move-result p3

    if-eqz p3, :cond_f

    .line 182
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/lang/Iterable;

    .line 315
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    if-gez v0, :cond_c

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_c
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 185
    iget-object v4, p0, Lcom/squareup/blueprint/VerticalBlock;->spacing:Ljava/util/List;

    if-nez v4, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 186
    sget-object v0, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    .line 183
    invoke-virtual {v2, p1, p2, v0}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    move v0, v3

    goto :goto_3

    :cond_e
    return-object p2

    .line 179
    :cond_f
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "VerticalBlock aligned TOP cannot have a \'remaining\' block."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final copy(Ljava/lang/Object;Ljava/util/List;I)Lcom/squareup/blueprint/VerticalBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;I)",
            "Lcom/squareup/blueprint/VerticalBlock<",
            "TC;TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/VerticalBlock;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blueprint/VerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/VerticalBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/VerticalBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/VerticalBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    iget p1, p1, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDependableHorizontally()Z
    .locals 3

    .line 60
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 289
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 290
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    .line 60
    invoke-virtual {v1}, Lcom/squareup/blueprint/Block;->getDependableHorizontally()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    return v2
.end method

.method public getDependableVertically()Z
    .locals 1

    .line 61
    iget v0, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/blueprint/VerticalBlock;->elements:Ljava/util/List;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/blueprint/VerticalBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    add-int/2addr v0, v1

    return v0
.end method

.method public startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 235
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 323
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 235
    invoke-virtual {v2, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/VerticalBlock;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VerticalBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", elements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", extendIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blueprint/VerticalBlock;->extendIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/squareup/blueprint/VerticalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method
