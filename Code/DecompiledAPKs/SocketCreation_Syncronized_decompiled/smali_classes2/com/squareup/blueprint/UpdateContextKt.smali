.class public final Lcom/squareup/blueprint/UpdateContextKt;
.super Ljava/lang/Object;
.source "UpdateContext.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u001a2\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\u0004\u001a\"\u0010\t\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\u0004\u00a8\u0006\u000c"
    }
    d2 = {
        "connectAndLog",
        "",
        "Landroidx/constraintlayout/widget/ConstraintSet;",
        "fromId",
        "",
        "fromSide",
        "toId",
        "toSide",
        "margin",
        "setMarginAndLog",
        "id",
        "side",
        "blueprint-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final connectAndLog(Landroidx/constraintlayout/widget/ConstraintSet;IIIII)V
    .locals 1

    const-string v0, "$this$connectAndLog"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual/range {p0 .. p5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    return-void
.end method

.method public static final setMarginAndLog(Landroidx/constraintlayout/widget/ConstraintSet;III)V
    .locals 1

    const-string v0, "$this$setMarginAndLog"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintSet;->setMargin(III)V

    return-void
.end method
