.class public interface abstract Lcom/squareup/PosLoggedInComponent;
.super Ljava/lang/Object;
.source "PosLoggedInComponent.java"

# interfaces
.implements Lcom/squareup/CommonLoggedInComponent;
.implements Lcom/squareup/ui/onboarding/OnboardingLoggedInComponent;
.implements Lcom/squareup/ui/PaymentActivity$LoggedInComponent;
.implements Lcom/squareup/account/PendingPreferencesCache$LoggedInDependenciesInjector;
.implements Lcom/squareup/reports/applet/ui/report/sales/ReportLoggedInComponent;
.implements Lcom/squareup/checkoutflow/installments/InstallmentsComponent;
.implements Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;
.implements Lcom/squareup/account/UpdatePreferencesTask$Component;
.implements Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/PosLoggedInComponent$Module;
    }
.end annotation


# virtual methods
.method public abstract handlesDisputes()Lcom/squareup/disputes/api/HandlesDisputes;
.end method

.method public abstract onboardingActivity()Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;
.end method
