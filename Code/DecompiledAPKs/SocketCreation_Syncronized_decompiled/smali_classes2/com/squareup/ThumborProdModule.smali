.class public Lcom/squareup/ThumborProdModule;
.super Ljava/lang/Object;
.source "ThumborProdModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field public static final PRODUCTION_THUMBOR_KEY:Ljava/lang/String; = "7_VkmxXz8TVgTTxW"

.field public static final PRODUCTION_THUMBOR_URL:Ljava/lang/String; = "https://images-production-s.squarecdn.com"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideThumbor()Lcom/squareup/pollexor/Thumbor;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "https://images-production-s.squarecdn.com"

    const-string v1, "7_VkmxXz8TVgTTxW"

    .line 13
    invoke-static {v0, v1}, Lcom/squareup/pollexor/Thumbor;->create(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/pollexor/Thumbor;

    move-result-object v0

    return-object v0
.end method
