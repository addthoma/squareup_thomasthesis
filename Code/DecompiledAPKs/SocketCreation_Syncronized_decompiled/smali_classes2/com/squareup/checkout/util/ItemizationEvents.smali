.class public Lcom/squareup/checkout/util/ItemizationEvents;
.super Ljava/lang/Object;
.source "ItemizationEvents.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->COMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    const-string v0, "comp-reason"

    .line 54
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static creationEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static creationEvent(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 38
    invoke-static {p1}, Lcom/squareup/checkout/util/ISO8601Dates;->tryBuildISO8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static deleteEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DELETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 65
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static discountRemoveEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->REMOVE_DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 71
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method private static eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 2

    .line 94
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;-><init>()V

    .line 95
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_type(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 96
    invoke-static {}, Lcom/squareup/checkout/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 98
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 97
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    if-eqz p1, :cond_0

    .line 101
    sget-object v0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY_EMPLOYEE_PROTO:Lcom/squareup/protos/client/Employee;

    if-eq p1, v0, :cond_0

    .line 102
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 103
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p1

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    :cond_0
    return-object p0
.end method

.method public static itemWithEvent(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem;
    .locals 0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 89
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 90
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    return-object p0
.end method

.method private static splitChildItemization(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
    .locals 2

    .line 110
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 112
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    .line 111
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    move-result-object p1

    .line 114
    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;

    move-result-object p0

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p0

    return-object p0
.end method

.method public static splitEvent(Lcom/squareup/protos/client/Employee;Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 3

    .line 77
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 80
    invoke-static {p1, p3}, Lcom/squareup/checkout/util/ItemizationEvents;->splitChildItemization(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget-object p1, p2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 81
    invoke-static {p1, p3}, Lcom/squareup/checkout/util/ItemizationEvents;->splitChildItemization(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v1, p2

    .line 79
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->child_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    move-result-object p1

    .line 78
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->split_event_details(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static uncompEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNCOMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method

.method public static voidEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-static {v0, p0}, Lcom/squareup/checkout/util/ItemizationEvents;->eventBuilder(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    const-string/jumbo v0, "void-reason"

    .line 48
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object p0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p0

    return-object p0
.end method
