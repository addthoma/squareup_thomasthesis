.class public abstract Lcom/squareup/checkout/Adjustment;
.super Ljava/lang/Object;
.source "Adjustment.java"

# interfaces
.implements Lcom/squareup/calc/order/Adjustment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Adjustment$Builder;,
        Lcom/squareup/checkout/Adjustment$JsonAdapter;,
        Lcom/squareup/checkout/Adjustment$DisplayComparator;
    }
.end annotation


# static fields
.field public static final DISPLAY_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/checkout/Adjustment;",
            ">;"
        }
    .end annotation
.end field

.field private static final collator:Ljava/text/Collator;


# instance fields
.field public final amount:Lcom/squareup/protos/common/Money;

.field private final className:Ljava/lang/String;

.field public final createdAt:Ljava/util/Date;

.field public final enabled:Z

.field public final id:Ljava/lang/String;

.field public final idPair:Lcom/squareup/protos/client/IdPair;

.field public final inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

.field public final maxAmount:Lcom/squareup/protos/common/Money;

.field public final name:Ljava/lang/String;

.field public final percentage:Lcom/squareup/util/Percentage;

.field public final phase:Lcom/squareup/api/items/CalculationPhase;

.field public final priority:Lcom/squareup/calc/constants/CalculationPriority;

.field public final rate:Ljava/math/BigDecimal;

.field public final taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/checkout/Adjustment$DisplayComparator;

    invoke-direct {v0}, Lcom/squareup/checkout/Adjustment$DisplayComparator;-><init>()V

    sput-object v0, Lcom/squareup/checkout/Adjustment;->DISPLAY_COMPARATOR:Ljava/util/Comparator;

    .line 55
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkout/Adjustment;->collator:Ljava/text/Collator;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/checkout/Adjustment$Builder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Adjustment$Builder<",
            "*>;)V"
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->className:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->id:Ljava/lang/String;

    const-string v1, "id"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->percentage:Lcom/squareup/util/Percentage;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    .line 120
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->createdAt:Ljava/util/Date;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->createdAt:Ljava/util/Date;

    .line 121
    iget-object v0, p0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->bigDecimalRate()Ljava/math/BigDecimal;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    .line 122
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->amount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    .line 123
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->maxAmount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->maxAmount:Lcom/squareup/protos/common/Money;

    .line 124
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    .line 125
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->phase:Lcom/squareup/api/items/CalculationPhase;

    const-string v2, "phase"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/CalculationPhase;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->phase:Lcom/squareup/api/items/CalculationPhase;

    .line 126
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->priority:Lcom/squareup/calc/constants/CalculationPriority;

    const-string v2, "priority"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/calc/constants/CalculationPriority;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->priority:Lcom/squareup/calc/constants/CalculationPriority;

    .line 127
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    goto :goto_1

    :cond_1
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    :goto_1
    iput-object v0, p0, Lcom/squareup/checkout/Adjustment;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 130
    iget-boolean v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->enabled:Z

    iput-boolean v0, p0, Lcom/squareup/checkout/Adjustment;->enabled:Z

    .line 131
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    if-nez v0, :cond_2

    new-instance p1, Lcom/squareup/protos/client/IdPair;

    .line 132
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    iget-object p1, p1, Lcom/squareup/checkout/Adjustment$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    :goto_2
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment;->idPair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method

.method static synthetic access$000()Ljava/text/Collator;
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/checkout/Adjustment;->collator:Ljava/text/Collator;

    return-object v0
.end method

.method public static sortToDisplayOrder(Ljava/util/Map;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/checkout/Adjustment;",
            ">(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 278
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 279
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 280
    sget-object p0, Lcom/squareup/checkout/Adjustment;->DISPLAY_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 281
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Adjustment;

    .line 282
    iget-object v2, v1, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public amount()Ljava/lang/Long;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 209
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    return-object v0
.end method

.method public final asRequestAdjustment(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/value/Adjustment;
    .locals 1

    const/4 v0, 0x0

    .line 289
    invoke-virtual {p0, p1, v0}, Lcom/squareup/checkout/Adjustment;->asRequestAdjustment(Lcom/squareup/protos/common/Money;Ljava/util/List;)Lcom/squareup/server/payment/value/Adjustment;

    move-result-object p1

    return-object p1
.end method

.method public abstract asRequestAdjustment(Lcom/squareup/protos/common/Money;Ljava/util/List;)Lcom/squareup/server/payment/value/Adjustment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;)",
            "Lcom/squareup/server/payment/value/Adjustment;"
        }
    .end annotation
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 225
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 227
    :cond_1
    check-cast p1, Lcom/squareup/checkout/Adjustment;

    .line 229
    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    .line 230
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    .line 231
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->maxAmount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->maxAmount:Lcom/squareup/protos/common/Money;

    .line 232
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    .line 233
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    .line 234
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->phase:Lcom/squareup/api/items/CalculationPhase;

    .line 235
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    iget-object v3, p1, Lcom/squareup/checkout/Adjustment;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 236
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/squareup/checkout/Adjustment;->enabled:Z

    iget-boolean p1, p1, Lcom/squareup/checkout/Adjustment;->enabled:Z

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    .line 241
    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->maxAmount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->phase:Lcom/squareup/api/items/CalculationPhase;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/checkout/Adjustment;->enabled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public id()Ljava/lang/String;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    return-object v0
.end method

.method public inclusionType()Lcom/squareup/calc/constants/InclusionType;
    .locals 2

    .line 138
    sget-object v0, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Fee$InclusionType:[I

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee$InclusionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 142
    sget-object v0, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    return-object v0

    .line 144
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown Type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_1
    sget-object v0, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    return-object v0
.end method

.method public maxAmount()Ljava/lang/Long;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/checkout/Adjustment;->maxAmount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 220
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    return-object v0
.end method

.method public phase()Lcom/squareup/calc/constants/CalculationPhase;
    .locals 3

    .line 153
    sget-object v0, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v1}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 169
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown phase "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :pswitch_0
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    .line 165
    :pswitch_1
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    .line 163
    :pswitch_2
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->TIP_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    .line 161
    :pswitch_3
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    .line 159
    :pswitch_4
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    .line 157
    :pswitch_5
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    .line 155
    :pswitch_6
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public priority()Lcom/squareup/calc/constants/CalculationPriority;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/checkout/Adjustment;->priority:Lcom/squareup/calc/constants/CalculationPriority;

    return-object v0
.end method

.method public rate()Ljava/math/BigDecimal;
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;
    .locals 3

    .line 178
    sget-object v0, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Discount$ModifyTaxBasis:[I

    iget-object v1, p0, Lcom/squareup/checkout/Adjustment;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 182
    sget-object v0, Lcom/squareup/calc/constants/ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    return-object v0

    .line 184
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown taxBasis "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Adjustment;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_1
    sget-object v0, Lcom/squareup/calc/constants/ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    return-object v0
.end method
