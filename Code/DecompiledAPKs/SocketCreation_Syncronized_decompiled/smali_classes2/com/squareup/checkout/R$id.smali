.class public final Lcom/squareup/checkout/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final buyer_action_bar:I = 0x7f0a027c

.field public static final buyer_action_options:I = 0x7f0a027e

.field public static final cart_entry_row_amount:I = 0x7f0a02ce

.field public static final cart_entry_row_name_and_quantity:I = 0x7f0a02cf

.field public static final cart_entry_row_sub_label:I = 0x7f0a02d0

.field public static final confirm_button:I = 0x7f0a038b

.field public static final glyph_container:I = 0x7f0a07af

.field public static final glyph_message:I = 0x7f0a07b1

.field public static final glyph_subtitle:I = 0x7f0a07b6

.field public static final glyph_title:I = 0x7f0a07b7

.field public static final helper_text:I = 0x7f0a07d3

.field public static final input_field:I = 0x7f0a0834

.field public static final merchant_image:I = 0x7f0a09c9

.field public static final noho_buyer_action_bar_left_button:I = 0x7f0a0a32

.field public static final noho_buyer_action_bar_left_glyph_button:I = 0x7f0a0a33

.field public static final noho_buyer_action_bar_right_button:I = 0x7f0a0a34

.field public static final noho_buyer_action_bar_subtitle:I = 0x7f0a0a35

.field public static final noho_buyer_action_bar_ticket_name:I = 0x7f0a0a36

.field public static final noho_buyer_action_bar_title:I = 0x7f0a0a37

.field public static final noho_buyer_action_bar_up_button:I = 0x7f0a0a38

.field public static final noho_buyer_action_container_call_to_action:I = 0x7f0a0a3a

.field public static final payment_processing_action_container:I = 0x7f0a0bed

.field public static final payment_processing_action_hint:I = 0x7f0a0bee

.field public static final payment_processing_primary_button:I = 0x7f0a0bef

.field public static final payment_processing_secondary_button:I = 0x7f0a0bf0

.field public static final percentage_hint:I = 0x7f0a0c0d

.field public static final primary_action_container:I = 0x7f0a0c5a

.field public static final receipt_content:I = 0x7f0a0cfe

.field public static final secondary_action_container:I = 0x7f0a0e2d

.field public static final spinner_glyph:I = 0x7f0a0ebd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
