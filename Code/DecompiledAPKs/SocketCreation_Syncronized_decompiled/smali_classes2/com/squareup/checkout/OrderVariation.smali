.class public Lcom/squareup/checkout/OrderVariation;
.super Ljava/lang/Object;
.source "OrderVariation.java"


# static fields
.field public static final CUSTOM_ITEM_ITEM_VARIATION:Lcom/squareup/api/items/ItemVariation;

.field public static final CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;


# instance fields
.field private final cachedPrice:Lcom/squareup/protos/common/Money;

.field private final displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

.field private final displayName:Ljava/lang/String;

.field private duration:Lorg/threeten/bp/Duration;

.field private giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

.field private intermissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation
.end field

.field private final itemVariation:Lcom/squareup/api/items/ItemVariation;

.field private final quantityUnit:Lcom/squareup/orders/model/Order$QuantityUnit;

.field private quantityUnitOverride:Lcom/squareup/orders/model/Order$QuantityUnit;

.field private transitionTime:Lorg/threeten/bp/Duration;

.field private final unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 102
    new-instance v0, Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemVariation$Builder;-><init>()V

    sget-object v1, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    .line 103
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 104
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 105
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/squareup/api/items/ItemVariation$Builder;->build()Lcom/squareup/api/items/ItemVariation;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_ITEM_VARIATION:Lcom/squareup/api/items/ItemVariation;

    .line 111
    new-instance v0, Lcom/squareup/checkout/OrderVariation;

    sget-object v2, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_ITEM_VARIATION:Lcom/squareup/api/items/ItemVariation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/OrderVariation;-><init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    return-void
.end method

.method constructor <init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)V
    .locals 2

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "At least one of itemVariation or displayDetails must be non-null"

    .line 185
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 187
    iput-object p1, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    .line 188
    iput-object p2, p0, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    .line 189
    iput-object p3, p0, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    .line 190
    iput-object p4, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 191
    iput-object p6, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    const-wide/16 p3, 0x0

    if-eqz p1, :cond_3

    .line 193
    iget-object p6, p1, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    if-nez p6, :cond_2

    goto :goto_2

    .line 194
    :cond_2
    iget-object p6, p1, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    .line 195
    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p6

    goto :goto_3

    .line 194
    :cond_3
    :goto_2
    invoke-static {p3, p4}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p6

    .line 195
    :goto_3
    iput-object p6, p0, Lcom/squareup/checkout/OrderVariation;->duration:Lorg/threeten/bp/Duration;

    if-eqz p1, :cond_5

    .line 197
    iget-object p6, p1, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    if-nez p6, :cond_4

    goto :goto_4

    .line 198
    :cond_4
    iget-object p3, p1, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    .line 199
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    invoke-static {p3, p4}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p3

    goto :goto_5

    .line 198
    :cond_5
    :goto_4
    invoke-static {p3, p4}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p3

    .line 199
    :goto_5
    iput-object p3, p0, Lcom/squareup/checkout/OrderVariation;->transitionTime:Lorg/threeten/bp/Duration;

    if-nez p1, :cond_6

    .line 201
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p3

    goto :goto_6

    :cond_6
    iget-object p3, p1, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    :goto_6
    iput-object p3, p0, Lcom/squareup/checkout/OrderVariation;->intermissions:Ljava/util/List;

    if-eqz p1, :cond_7

    .line 203
    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 204
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_7

    :cond_7
    iget-object p1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;->price_money:Lcom/squareup/protos/common/Money;

    :goto_7
    iput-object p1, p0, Lcom/squareup/checkout/OrderVariation;->cachedPrice:Lcom/squareup/protos/common/Money;

    if-eqz p5, :cond_8

    goto :goto_8

    .line 209
    :cond_8
    invoke-static {}, Lcom/squareup/quantity/UnitDisplayData;->empty()Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p5

    :goto_8
    iput-object p5, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    return-void
.end method

.method public static forReadOnly(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/checkout/OrderVariation;
    .locals 1

    const/4 v0, 0x0

    .line 88
    invoke-static {p0, v0}, Lcom/squareup/checkout/OrderVariation;->forReadOnly(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;

    move-result-object p0

    return-object p0
.end method

.method public static forReadOnly(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;
    .locals 8

    const-string/jumbo v0, "variation"

    .line 97
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 98
    new-instance v0, Lcom/squareup/checkout/OrderVariation;

    iget-object v7, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/OrderVariation;-><init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)V

    return-object v0
.end method

.method public static of(Lcom/squareup/api/items/ItemVariation;Ljava/lang/String;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;
    .locals 8

    const-string/jumbo v0, "variation"

    .line 64
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 65
    new-instance v0, Lcom/squareup/checkout/OrderVariation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/OrderVariation;-><init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)V

    return-object v0
.end method

.method public static of(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Ljava/lang/String;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;
    .locals 8

    const-string/jumbo v0, "variationDetails"

    .line 44
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    .line 47
    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    .line 48
    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->gift_card_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    const/4 p0, 0x0

    if-eqz v0, :cond_0

    .line 54
    iget-object p0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    .line 55
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    move-object v2, p0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v2, p0

    move-object v5, v2

    .line 58
    :goto_0
    new-instance p0, Lcom/squareup/checkout/OrderVariation;

    move-object v1, p0

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/OrderVariation;-><init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)V

    return-object p0
.end method

.method public static of(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)Lcom/squareup/checkout/OrderVariation;
    .locals 8

    const-string v0, "catalogVariation"

    .line 37
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 38
    new-instance v0, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object p0

    move-object v2, p0

    check-cast v2, Lcom/squareup/api/items/ItemVariation;

    invoke-static {p1}, Lcom/squareup/quantity/ItemQuantities;->toQuantityUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, v0

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/OrderVariation;-><init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)V

    return-object v0
.end method

.method public static ofFixedForReadOnly(Lcom/squareup/server/payment/Itemization;)Lcom/squareup/checkout/OrderVariation;
    .locals 3

    .line 70
    new-instance v0, Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemVariation$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    const-string v2, "id"

    .line 71
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    const-string v2, ""

    .line 72
    invoke-static {v1, v2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getPriceMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v2, "price"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/PricingType;->FIXED_PRICING:Lcom/squareup/api/items/PricingType;

    .line 74
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 75
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/squareup/api/items/ItemVariation$Builder;->build()Lcom/squareup/api/items/ItemVariation;

    move-result-object v0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result p0

    .line 78
    invoke-static {v1, v2, p0}, Lcom/squareup/quantity/UnitDisplayData;->of(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    .line 80
    invoke-static {v0, p0}, Lcom/squareup/checkout/OrderVariation;->forReadOnly(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public clearQuantityUnitOverride()V
    .locals 1

    const/4 v0, 0x0

    .line 302
    iput-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnitOverride:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_c

    .line 398
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_5

    .line 400
    :cond_1
    check-cast p1, Lcom/squareup/checkout/OrderVariation;

    .line 402
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 406
    :cond_3
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 410
    :cond_5
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v2, v3}, Lcom/squareup/api/items/ItemVariation;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 414
    :cond_7
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 418
    :cond_9
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    iget-object p1, p1, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    if-eqz v2, :cond_a

    invoke-virtual {v2, p1}, Lcom/squareup/quantity/UnitDisplayData;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_b

    goto :goto_4

    :cond_a
    if-eqz p1, :cond_b

    :goto_4
    return v1

    :cond_b
    return v0

    :cond_c
    :goto_5
    return v1
.end method

.method public getDisplayDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayNameOrDefault()Lcom/squareup/resources/TextModel;
    .locals 2

    .line 236
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/resources/FixedText;

    invoke-direct {v1, v0}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v0, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-direct {v1, v0}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    :goto_0
    return-object v1
.end method

.method public getDuration()Lorg/threeten/bp/Duration;
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->duration:Lorg/threeten/bp/Duration;

    return-object v0
.end method

.method public getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 3

    .line 380
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    return-object v0

    .line 381
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Insufficient data to determine id, variation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " only backed by a DisplayDetail!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getIdOrNull()Ljava/lang/String;
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getIntermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation

    .line 339
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->intermissions:Ljava/util/List;

    return-object v0
.end method

.method public getItemVariation()Lcom/squareup/api/items/ItemVariation;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    return-object v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 247
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->cachedPrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getQuantityPrecision()I
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnitOverride:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, v0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnitOverride:Lcom/squareup/orders/model/Order$QuantityUnit;

    iget-object v0, v0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    .line 266
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecision()I

    move-result v0

    return v0
.end method

.method public getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnit:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-object v0
.end method

.method public getQuantityUnitOverride()Lcom/squareup/orders/model/Order$QuantityUnit;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnitOverride:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-object v0
.end method

.method public getTransitionTime()Lorg/threeten/bp/Duration;
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->transitionTime:Lorg/threeten/bp/Duration;

    return-object v0
.end method

.method public getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnitName()Ljava/lang/String;
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasUserConfiguredName()Z
    .locals 3

    .line 214
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    .line 215
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Insufficient data to determine hasUserConfiguredName, variation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " only backed by a DisplayDetail! Why do you even care?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    .line 427
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/api/items/ItemVariation;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 428
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 429
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 430
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 431
    iget-object v2, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/squareup/quantity/UnitDisplayData;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public isFixedPriced()Z
    .locals 1

    .line 358
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isUnitPriced()Z
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isVariablePriced()Z
    .locals 4

    .line 352
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->displayDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;->price_money:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 353
    :cond_1
    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    sget-object v3, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->itemVariation:Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method public setDuration(Lorg/threeten/bp/Duration;)V
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/squareup/checkout/OrderVariation;->duration:Lorg/threeten/bp/Duration;

    return-void
.end method

.method public setGiftCardDetails(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;)V
    .locals 0

    .line 366
    iput-object p1, p0, Lcom/squareup/checkout/OrderVariation;->giftCardDetails:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    return-void
.end method

.method public setIntermissions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)V"
        }
    .end annotation

    .line 347
    iput-object p1, p0, Lcom/squareup/checkout/OrderVariation;->intermissions:Ljava/util/List;

    return-void
.end method

.method public setQuantityPrecisionOverride(I)V
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "QuantityUnit is null. We won\'t create an override if there\'s nothing to override."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$QuantityUnit;->newBuilder()Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->precision(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->build()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    return-void
.end method

.method public setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/checkout/OrderVariation;->quantityUnitOverride:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderModifier{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", price="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/OrderVariation;->unitDisplayData:Lcom/squareup/quantity/UnitDisplayData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
