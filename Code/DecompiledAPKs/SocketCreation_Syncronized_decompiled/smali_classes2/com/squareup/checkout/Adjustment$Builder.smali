.class public abstract Lcom/squareup/checkout/Adjustment$Builder;
.super Ljava/lang/Object;
.source "Adjustment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Adjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/checkout/Adjustment$Builder<",
        "TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field amount:Lcom/squareup/protos/common/Money;

.field createdAt:Ljava/util/Date;

.field enabled:Z

.field id:Ljava/lang/String;

.field idPair:Lcom/squareup/protos/client/IdPair;

.field inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

.field maxAmount:Lcom/squareup/protos/common/Money;

.field name:Ljava/lang/String;

.field percentage:Lcom/squareup/util/Percentage;

.field phase:Lcom/squareup/api/items/CalculationPhase;

.field priority:Lcom/squareup/calc/constants/CalculationPriority;

.field taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 323
    iput-boolean v0, p0, Lcom/squareup/checkout/Adjustment$Builder;->enabled:Z

    .line 326
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment$Builder;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    .line 330
    sget-object v0, Lcom/squareup/calc/constants/CalculationPriority;->NORMAL:Lcom/squareup/calc/constants/CalculationPriority;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment$Builder;->priority:Lcom/squareup/calc/constants/CalculationPriority;

    .line 331
    sget-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    iput-object v0, p0, Lcom/squareup/checkout/Adjustment$Builder;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")TT;"
        }
    .end annotation

    .line 364
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 365
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public createdAt(Ljava/util/Date;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")TT;"
        }
    .end annotation

    .line 339
    invoke-static {p1}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->createdAt:Ljava/util/Date;

    .line 340
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public enabled(Z)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .line 394
    iput-boolean p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->enabled:Z

    .line 395
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method protected getThis()Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 349
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->id:Ljava/lang/String;

    .line 350
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            ")TT;"
        }
    .end annotation

    .line 334
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 335
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Fee$InclusionType;",
            ")TT;"
        }
    .end annotation

    .line 374
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    .line 375
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public maxAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")TT;"
        }
    .end annotation

    .line 369
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->maxAmount:Lcom/squareup/protos/common/Money;

    .line 370
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 354
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->name:Ljava/lang/String;

    .line 355
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Percentage;",
            ")TT;"
        }
    .end annotation

    .line 359
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->percentage:Lcom/squareup/util/Percentage;

    .line 360
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/CalculationPhase;",
            ")TT;"
        }
    .end annotation

    .line 379
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->phase:Lcom/squareup/api/items/CalculationPhase;

    .line 380
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public priority(Lcom/squareup/calc/constants/CalculationPriority;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPriority;",
            ")TT;"
        }
    .end annotation

    .line 384
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->priority:Lcom/squareup/calc/constants/CalculationPriority;

    .line 385
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method public taxBasis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Discount$ModifyTaxBasis;",
            ")TT;"
        }
    .end annotation

    .line 389
    iput-object p1, p0, Lcom/squareup/checkout/Adjustment$Builder;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 390
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method

.method protected tryParseCreateAt(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/ISO8601Date;",
            ")TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 344
    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Adjustment$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 345
    invoke-virtual {p0}, Lcom/squareup/checkout/Adjustment$Builder;->getThis()Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p1

    return-object p1
.end method
