.class public abstract Lcom/squareup/checkout/Discount$Matches;
.super Ljava/lang/Object;
.source "Discount.kt"


# annotations
.annotation runtime Lcom/google/gson/annotations/JsonAdapter;
    value = Lcom/squareup/checkout/Discount$Matches$MatchesJsonAdapter;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Matches"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Discount$Matches$AllItems;,
        Lcom/squareup/checkout/Discount$Matches$Categories;,
        Lcom/squareup/checkout/Discount$Matches$Items;,
        Lcom/squareup/checkout/Discount$Matches$MatchesJsonAdapter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00087\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0007\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkout/Discount$Matches;",
        "",
        "()V",
        "AllItems",
        "Categories",
        "Items",
        "MatchesJsonAdapter",
        "Lcom/squareup/checkout/Discount$Matches$AllItems;",
        "Lcom/squareup/checkout/Discount$Matches$Categories;",
        "Lcom/squareup/checkout/Discount$Matches$Items;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 147
    invoke-direct {p0}, Lcom/squareup/checkout/Discount$Matches;-><init>()V

    return-void
.end method
