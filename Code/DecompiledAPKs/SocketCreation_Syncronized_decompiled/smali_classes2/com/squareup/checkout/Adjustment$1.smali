.class synthetic Lcom/squareup/checkout/Adjustment$1;
.super Ljava/lang/Object;
.source "Adjustment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Adjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$api$items$CalculationPhase:[I

.field static final synthetic $SwitchMap$com$squareup$api$items$Discount$ModifyTaxBasis:[I

.field static final synthetic $SwitchMap$com$squareup$api$items$Fee$InclusionType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 178
    invoke-static {}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->values()[Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Discount$ModifyTaxBasis:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Discount$ModifyTaxBasis:[I

    sget-object v2, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v2}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Discount$ModifyTaxBasis:[I

    sget-object v3, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v3}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 153
    :catch_1
    invoke-static {}, Lcom/squareup/api/items/CalculationPhase;->values()[Lcom/squareup/api/items/CalculationPhase;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    :try_start_2
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    const/4 v4, 0x3

    aput v4, v2, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    const/4 v4, 0x4

    aput v4, v2, v3
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->TIP_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    const/4 v4, 0x5

    aput v4, v2, v3
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    const/4 v4, 0x6

    aput v4, v2, v3
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$CalculationPhase:[I

    sget-object v3, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v3}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v3

    const/4 v4, 0x7

    aput v4, v2, v3
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    .line 138
    :catch_8
    invoke-static {}, Lcom/squareup/api/items/Fee$InclusionType;->values()[Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Fee$InclusionType:[I

    :try_start_9
    sget-object v2, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Fee$InclusionType:[I

    sget-object v3, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v3}, Lcom/squareup/api/items/Fee$InclusionType;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/checkout/Adjustment$1;->$SwitchMap$com$squareup$api$items$Fee$InclusionType:[I

    sget-object v2, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v2}, Lcom/squareup/api/items/Fee$InclusionType;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    return-void
.end method
