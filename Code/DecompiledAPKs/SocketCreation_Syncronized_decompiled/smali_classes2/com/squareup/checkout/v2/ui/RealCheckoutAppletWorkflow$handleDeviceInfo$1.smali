.class final Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckoutAppletWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->handleDeviceInfo(Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "-",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;

    invoke-direct {v0}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;-><init>()V

    sput-object v0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;->INSTANCE:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "-",
            "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;

    sget-object v1, Lcom/squareup/checkout/v2/ui/DeviceFormat$Phone;->INSTANCE:Lcom/squareup/checkout/v2/ui/DeviceFormat$Phone;

    check-cast v1, Lcom/squareup/checkout/v2/ui/DeviceFormat;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;->copy(Lcom/squareup/checkout/v2/ui/DeviceFormat;)Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
