.class final Lcom/squareup/BundleKey$3;
.super Lcom/squareup/BundleKey;
.source "BundleKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/BundleKey;->forEnum(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/BundleKey<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final enumConstants:[Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field final synthetic val$type:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0

    .line 119
    iput-object p2, p0, Lcom/squareup/BundleKey$3;->val$type:Ljava/lang/Class;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/squareup/BundleKey;-><init>(Ljava/lang/String;Lcom/squareup/BundleKey$1;)V

    .line 120
    iget-object p1, p0, Lcom/squareup/BundleKey$3;->val$type:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Enum;

    iput-object p1, p0, Lcom/squareup/BundleKey$3;->enumConstants:[Ljava/lang/Enum;

    return-void
.end method


# virtual methods
.method public get(Landroid/content/Intent;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")TT;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/BundleKey$3;->key:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    if-eq p1, v1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/squareup/BundleKey$3;->enumConstants:[Ljava/lang/Enum;

    aget-object p1, v0, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public get(Landroid/os/Bundle;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/BundleKey$3;->key:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-le p1, v1, :cond_0

    .line 124
    iget-object v0, p0, Lcom/squareup/BundleKey$3;->enumConstants:[Ljava/lang/Enum;

    aget-object p1, v0, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public bridge synthetic get(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 0

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey$3;->get(Landroid/content/Intent;)Ljava/lang/Enum;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 0

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey$3;->get(Landroid/os/Bundle;)Ljava/lang/Enum;

    move-result-object p1

    return-object p1
.end method

.method public put(Landroid/content/Intent;Ljava/lang/Enum;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/BundleKey$3;->key:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public bridge synthetic put(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .line 119
    check-cast p2, Ljava/lang/Enum;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/BundleKey$3;->put(Landroid/content/Intent;Ljava/lang/Enum;)V

    return-void
.end method

.method public put(Landroid/os/Bundle;Ljava/lang/Enum;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 134
    iget-object v0, p0, Lcom/squareup/BundleKey$3;->key:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic put(Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 0

    .line 119
    check-cast p2, Ljava/lang/Enum;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/BundleKey$3;->put(Landroid/os/Bundle;Ljava/lang/Enum;)V

    return-void
.end method
