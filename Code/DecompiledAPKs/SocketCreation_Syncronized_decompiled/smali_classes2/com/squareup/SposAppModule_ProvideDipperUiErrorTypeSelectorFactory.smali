.class public final Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory;
.super Ljava/lang/Object;
.source "SposAppModule_ProvideDipperUiErrorTypeSelectorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory$InstanceHolder;->access$000()Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideDipperUiErrorTypeSelector()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;
    .locals 2

    .line 23
    invoke-static {}, Lcom/squareup/SposAppModule;->provideDipperUiErrorTypeSelector()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;
    .locals 1

    .line 15
    invoke-static {}, Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory;->provideDipperUiErrorTypeSelector()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/SposAppModule_ProvideDipperUiErrorTypeSelectorFactory;->get()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    move-result-object v0

    return-object v0
.end method
