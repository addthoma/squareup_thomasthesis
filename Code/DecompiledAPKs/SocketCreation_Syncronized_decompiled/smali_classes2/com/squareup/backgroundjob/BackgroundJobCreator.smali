.class public abstract Lcom/squareup/backgroundjob/BackgroundJobCreator;
.super Ljava/lang/Object;
.source "BackgroundJobCreator.java"

# interfaces
.implements Lcom/evernote/android/job/JobCreator;
.implements Lmortar/Scoped;


# instance fields
.field private final jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;


# direct methods
.method public constructor <init>(Lcom/squareup/backgroundjob/BackgroundJobManager;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/backgroundjob/BackgroundJobCreator;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 24
    iget-object p1, p0, Lcom/squareup/backgroundjob/BackgroundJobCreator;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-interface {p1, p0}, Lcom/squareup/backgroundjob/BackgroundJobManager;->addJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/backgroundjob/BackgroundJobCreator;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-interface {v0, p0}, Lcom/squareup/backgroundjob/BackgroundJobManager;->removeJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V

    return-void
.end method
