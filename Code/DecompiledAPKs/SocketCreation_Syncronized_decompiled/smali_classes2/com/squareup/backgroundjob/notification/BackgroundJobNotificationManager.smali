.class public interface abstract Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;
.super Ljava/lang/Object;
.source "BackgroundJobNotificationManager.java"


# virtual methods
.method public abstract enableNotifications(Z)V
.end method

.method public abstract hideAllNotifications()V
.end method

.method public abstract hideAllNotificationsForTag(Ljava/lang/String;)V
.end method

.method public abstract hideNotificationFor(Lcom/evernote/android/job/JobRequest;)V
.end method

.method public abstract hideNotificationFor(Lcom/squareup/backgroundjob/JobParams;)V
.end method

.method public abstract showNotificationFor(Lcom/evernote/android/job/JobRequest;)V
.end method
