.class public abstract Lcom/squareup/backgroundjob/ExecutorBackgroundJob;
.super Lcom/squareup/backgroundjob/BackgroundJob;
.source "ExecutorBackgroundJob.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final executor:Ljava/util/concurrent/Executor;

.field private final wakeLockTag:Ljava/lang/String;

.field private final wakeLockTimeoutMs:J


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Ljava/lang/String;)V
    .locals 7

    .line 32
    sget-wide v5, Lcom/squareup/backgroundjob/BackgroundJobs;->DEFAULT_WAKE_LOCK_TIMEOUT_MS:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Ljava/lang/String;J)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Ljava/lang/String;J)V
    .locals 0

    .line 44
    invoke-direct {p0, p3}, Lcom/squareup/backgroundjob/BackgroundJob;-><init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    .line 45
    iput-object p1, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->context:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->executor:Ljava/util/concurrent/Executor;

    .line 47
    iput-object p4, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->wakeLockTag:Ljava/lang/String;

    .line 48
    iput-wide p5, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->wakeLockTimeoutMs:J

    return-void
.end method


# virtual methods
.method public synthetic lambda$onRunJob$0$ExecutorBackgroundJob(Lcom/squareup/backgroundjob/JobParams;)V
    .locals 0

    .line 56
    invoke-virtual {p0, p1}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->onFinishRunJob(Lcom/squareup/backgroundjob/JobParams;)V

    return-void
.end method

.method protected final onRunJob(Lcom/evernote/android/job/Job$Params;)Lcom/evernote/android/job/Job$Result;
    .locals 7

    .line 52
    new-instance v0, Lcom/squareup/backgroundjob/RealParams;

    invoke-direct {v0, p1}, Lcom/squareup/backgroundjob/RealParams;-><init>(Lcom/evernote/android/job/Job$Params;)V

    .line 53
    invoke-virtual {p0, v0}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->onStartRunJob(Lcom/squareup/backgroundjob/JobParams;)V

    .line 54
    iget-object v1, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->executor:Ljava/util/concurrent/Executor;

    iget-wide v3, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->wakeLockTimeoutMs:J

    iget-object v5, p0, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;->wakeLockTag:Ljava/lang/String;

    new-instance v6, Lcom/squareup/backgroundjob/-$$Lambda$ExecutorBackgroundJob$b4RdTC9qTFmhZgCAfUA2lt-DmN0;

    invoke-direct {v6, p0, v0}, Lcom/squareup/backgroundjob/-$$Lambda$ExecutorBackgroundJob$b4RdTC9qTFmhZgCAfUA2lt-DmN0;-><init>(Lcom/squareup/backgroundjob/ExecutorBackgroundJob;Lcom/squareup/backgroundjob/JobParams;)V

    invoke-static/range {v1 .. v6}, Lcom/squareup/backgroundjob/BackgroundJobs;->executeWithWakeLock(Landroid/content/Context;Ljava/util/concurrent/Executor;JLjava/lang/String;Lrx/functions/Action0;)V

    .line 59
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method
