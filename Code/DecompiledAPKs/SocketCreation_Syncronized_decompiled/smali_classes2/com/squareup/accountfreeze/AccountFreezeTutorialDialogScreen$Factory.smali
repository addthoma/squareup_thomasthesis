.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory;
.super Ljava/lang/Object;
.source "AccountFreezeTutorialDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccountFreezeTutorialDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccountFreezeTutorialDialogScreen.kt\ncom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,56:1\n52#2:57\n52#2:58\n*E\n*S KotlinDebug\n*F\n+ 1 AccountFreezeTutorialDialogScreen.kt\ncom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory\n*L\n21#1:57\n23#1:58\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J \u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "runner",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)Landroid/app/Dialog;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 34
    sget p1, Lcom/squareup/accountfreeze/impl/R$string;->alert_title:I

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 35
    sget v0, Lcom/squareup/accountfreeze/impl/R$string;->alert_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    sget v0, Lcom/squareup/accountfreeze/impl/R$string;->alert_verify:I

    new-instance v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory$createDialog$1;

    invoke-direct {v1, p3, p2}, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory$createDialog$1;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    sget v0, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 40
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 44
    sget v0, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 43
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 47
    sget v0, Lcom/squareup/accountfreeze/impl/R$string;->alert_later:I

    new-instance v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory$createDialog$2;

    invoke-direct {v1, p3, p2}, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory$createDialog$2;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    new-instance p3, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory$createDialog$3;

    invoke-direct {p3, p2}, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory$createDialog$3;-><init>(Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;)V

    check-cast p3, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026n() }\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    const-class v0, Lcom/squareup/accountfreeze/AccountFreezeParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountfreeze/AccountFreezeParentComponent;

    .line 22
    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreezeParentComponent;->runner()Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;

    move-result-object v0

    .line 58
    const-class v1, Lcom/squareup/accountfreeze/AccountFreezeParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/accountfreeze/AccountFreezeParentComponent;

    .line 24
    invoke-interface {v1}, Lcom/squareup/accountfreeze/AccountFreezeParentComponent;->analytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    .line 25
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(createDialog\u2026text, runner, analytics))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
