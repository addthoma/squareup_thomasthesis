.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorial;
.super Ljava/lang/Object;
.source "AccountFreezeTutorial.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/AccountFreezeTutorial$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u0000 #2\u00020\u0001:\u0001#B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0008\u0010\u0011\u001a\u00020\u0010H\u0002J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0010H\u0016J\u0008\u0010\u0016\u001a\u00020\u0010H\u0016J\u001a\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0010H\u0002J\u0018\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020\u0010H\u0002J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\r0\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "accountFreeze",
        "Lcom/squareup/accountfreeze/AccountFreeze;",
        "creator",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
        "runner",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)V",
        "output",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "kotlin.jvm.PlatformType",
        "endAndGoToDeposits",
        "",
        "hideBanner",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitRequested",
        "onExitScope",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialOverride",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "showBanner",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BANNER_STATE:Lcom/squareup/tutorialv2/TutorialState;

.field public static final Companion:Lcom/squareup/accountfreeze/AccountFreezeTutorial$Companion;


# instance fields
.field private final accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final creator:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

.field private final output:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeTutorial$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accountfreeze/AccountFreezeTutorial$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->Companion:Lcom/squareup/accountfreeze/AccountFreezeTutorial$Companion;

    .line 111
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 112
    sget v1, Lcom/squareup/accountfreeze/impl/R$string;->banner_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 113
    sget v1, Lcom/squareup/accountfreeze/impl/R$color;->freeze_banner_background:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->backgroundColor(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    sput-object v0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->BANNER_STATE:Lcom/squareup/tutorialv2/TutorialState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountFreeze"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->creator:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->runner:Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;

    iput-object p4, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    .line 31
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<TutorialState>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/accountfreeze/AccountFreezeTutorial;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getBANNER_STATE$cp()Lcom/squareup/tutorialv2/TutorialState;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->BANNER_STATE:Lcom/squareup/tutorialv2/TutorialState;

    return-object v0
.end method

.method private final endAndGoToDeposits()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->onClickedBanner(Lcom/squareup/analytics/Analytics;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreeze;->markBannerDismissed()V

    .line 106
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->runner:Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;

    invoke-virtual {v0}, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->jumpToDeposits()V

    return-void
.end method

.method private final hideBanner()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final onTutorialOverride()V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->creator:Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    invoke-virtual {v0}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->onFreezeTutorialAborted()V

    return-void
.end method

.method private final showBanner()V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->BANNER_STATE:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 37
    sget-object v1, Lcom/squareup/accountfreeze/AccountFreezeTutorial$onEnterScope$1;->INSTANCE:Lcom/squareup/accountfreeze/AccountFreezeTutorial$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 38
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "output\n        .filter {\u2026_STATE }\n        .take(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v1, Lcom/squareup/accountfreeze/AccountFreezeTutorial$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial$onEnterScope$2;-><init>(Lcom/squareup/accountfreeze/AccountFreezeTutorial;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitRequested()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->onClosedBanner(Lcom/squareup/analytics/Analytics;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreeze;->markBannerDismissed()V

    .line 82
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->runner:Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;

    invoke-virtual {v0}, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;->showPromptScreen()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_3

    :sswitch_0
    const-string p2, "Applet Selected"

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_1

    :sswitch_1
    const-string p2, "Shown SelectMethodScreen, above max card amount"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto/16 :goto_1

    :sswitch_2
    const-string p2, "Tutorial content tapped"

    .line 66
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->endAndGoToDeposits()V

    goto/16 :goto_3

    :sswitch_3
    const-string p2, "Shown SelectMethodScreen"

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :sswitch_4
    const-string p2, "Shown BuyerCartScreen"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :sswitch_5
    const-string v0, "Shown OrderEntryScreen"

    .line 49
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :sswitch_6
    const-string p2, "Shown SelectMethodScreen, below min card amount"

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :sswitch_7
    const-string v0, "Leaving SelectMethodScreen"

    .line 61
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_2

    :sswitch_8
    const-string p2, "Tutorial being replaced by one of a higher priority"

    .line 75
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->onTutorialOverride()V

    goto :goto_3

    :sswitch_9
    const-string v0, "On OrderEntryScreen and ready for card"

    .line 49
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :goto_0
    const-string p1, "Home visible"

    .line 54
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->showBanner()V

    goto :goto_3

    :cond_0
    const-string p1, "Home not visible"

    .line 55
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->hideBanner()V

    goto :goto_3

    :sswitch_a
    const-string p2, "Shown PaymentPromptScreen"

    .line 68
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 73
    :goto_1
    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->hideBanner()V

    goto :goto_3

    :sswitch_b
    const-string v0, "Leaving SelectMethodScreen, card amount in range"

    .line 61
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 63
    :goto_2
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->showBanner()V

    goto :goto_3

    :sswitch_c
    const-string p2, "Dismissed EnterPasscodeToUnlockScreen"

    .line 59
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->showBanner()V

    :cond_1
    :goto_3
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7fe47a36 -> :sswitch_c
        -0x74bd0c1d -> :sswitch_b
        -0x7176a1b9 -> :sswitch_a
        -0x69b9f3a4 -> :sswitch_9
        -0x6999a91d -> :sswitch_8
        -0x43d36e0b -> :sswitch_7
        -0x435292c1 -> :sswitch_6
        -0x2f75f541 -> :sswitch_5
        -0x5eb0f90 -> :sswitch_4
        0x1fbd2f78 -> :sswitch_3
        0x3d22a0f5 -> :sswitch_2
        0x5869a0bd -> :sswitch_1
        0x7cf2cba1 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
