.class public final enum Lcom/squareup/bugreport/BatteryStatus$Plugged;
.super Ljava/lang/Enum;
.source "BatteryStatus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/bugreport/BatteryStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Plugged"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/bugreport/BatteryStatus$Plugged;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u0000 \u00072\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/bugreport/BatteryStatus$Plugged;",
        "",
        "(Ljava/lang/String;I)V",
        "AC",
        "USB",
        "WIRELESS",
        "UNKNOWN",
        "Companion",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/bugreport/BatteryStatus$Plugged;

.field public static final enum AC:Lcom/squareup/bugreport/BatteryStatus$Plugged;

.field public static final Companion:Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;

.field public static final enum UNKNOWN:Lcom/squareup/bugreport/BatteryStatus$Plugged;

.field public static final enum USB:Lcom/squareup/bugreport/BatteryStatus$Plugged;

.field public static final enum WIRELESS:Lcom/squareup/bugreport/BatteryStatus$Plugged;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/bugreport/BatteryStatus$Plugged;

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;

    const/4 v2, 0x0

    const-string v3, "AC"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Plugged;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->AC:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;

    const/4 v2, 0x1

    const-string v3, "USB"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Plugged;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->USB:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;

    const/4 v2, 0x2

    const-string v3, "WIRELESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Plugged;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->WIRELESS:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;

    const/4 v2, 0x3

    const-string v3, "UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Plugged;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->UNKNOWN:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/bugreport/BatteryStatus$Plugged;->$VALUES:[Lcom/squareup/bugreport/BatteryStatus$Plugged;

    new-instance v0, Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/bugreport/BatteryStatus$Plugged;->Companion:Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/bugreport/BatteryStatus$Plugged;
    .locals 1

    const-class v0, Lcom/squareup/bugreport/BatteryStatus$Plugged;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/bugreport/BatteryStatus$Plugged;

    return-object p0
.end method

.method public static values()[Lcom/squareup/bugreport/BatteryStatus$Plugged;
    .locals 1

    sget-object v0, Lcom/squareup/bugreport/BatteryStatus$Plugged;->$VALUES:[Lcom/squareup/bugreport/BatteryStatus$Plugged;

    invoke-virtual {v0}, [Lcom/squareup/bugreport/BatteryStatus$Plugged;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/bugreport/BatteryStatus$Plugged;

    return-object v0
.end method
