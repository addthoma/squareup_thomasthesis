.class public final Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;
.super Landroid/app/FragmentManager$FragmentLifecycleCallbacks;
.source "AndroidOFragmentDestroyWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lleakcanary/internal/AndroidOFragmentDestroyWatcher;-><init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "leakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1",
        "Landroid/app/FragmentManager$FragmentLifecycleCallbacks;",
        "onFragmentDestroyed",
        "",
        "fm",
        "Landroid/app/FragmentManager;",
        "fragment",
        "Landroid/app/Fragment;",
        "onFragmentViewDestroyed",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lleakcanary/internal/AndroidOFragmentDestroyWatcher;


# direct methods
.method constructor <init>(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;->this$0:Lleakcanary/internal/AndroidOFragmentDestroyWatcher;

    invoke-direct {p0}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public onFragmentDestroyed(Landroid/app/FragmentManager;Landroid/app/Fragment;)V
    .locals 2

    const-string v0, "fm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "fragment"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object p1, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;->this$0:Lleakcanary/internal/AndroidOFragmentDestroyWatcher;

    invoke-static {p1}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->access$getConfigProvider$p(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lleakcanary/AppWatcher$Config;

    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getWatchFragments()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 52
    iget-object p1, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;->this$0:Lleakcanary/internal/AndroidOFragmentDestroyWatcher;

    invoke-static {p1}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->access$getObjectWatcher$p(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)Lleakcanary/ObjectWatcher;

    move-result-object p1

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " received Fragment#onDestroy() callback"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-virtual {p1, p2, v0}, Lleakcanary/ObjectWatcher;->watch(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onFragmentViewDestroyed(Landroid/app/FragmentManager;Landroid/app/Fragment;)V
    .locals 2

    const-string v0, "fm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "fragment"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 39
    iget-object v0, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;->this$0:Lleakcanary/internal/AndroidOFragmentDestroyWatcher;

    invoke-static {v0}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->access$getConfigProvider$p(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lleakcanary/AppWatcher$Config;

    invoke-virtual {v0}, Lleakcanary/AppWatcher$Config;->getWatchFragmentViews()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;->this$0:Lleakcanary/internal/AndroidOFragmentDestroyWatcher;

    invoke-static {v0}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->access$getObjectWatcher$p(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)Lleakcanary/ObjectWatcher;

    move-result-object v0

    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " received Fragment#onDestroyView() callback "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "(references to its views should be cleared to prevent leaks)"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 40
    invoke-virtual {v0, p1, p2}, Lleakcanary/ObjectWatcher;->watch(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
