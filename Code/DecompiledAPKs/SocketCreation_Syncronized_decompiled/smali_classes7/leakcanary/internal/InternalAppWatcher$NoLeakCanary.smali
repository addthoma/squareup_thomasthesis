.class public final Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;
.super Ljava/lang/Object;
.source "InternalAppWatcher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;
.implements Lleakcanary/OnObjectRetainedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/internal/InternalAppWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoLeakCanary"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/app/Application;",
        "Lkotlin/Unit;",
        ">;",
        "Lleakcanary/OnObjectRetainedListener;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0002H\u0096\u0002J\u0008\u0010\u0008\u001a\u00020\u0003H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;",
        "Lkotlin/Function1;",
        "Landroid/app/Application;",
        "",
        "Lleakcanary/OnObjectRetainedListener;",
        "()V",
        "invoke",
        "application",
        "onObjectRetained",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 97
    new-instance v0, Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;

    invoke-direct {v0}, Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;-><init>()V

    sput-object v0, Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Landroid/app/Application;

    invoke-virtual {p0, p1}, Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;->invoke(Landroid/app/Application;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public invoke(Landroid/app/Application;)V
    .locals 1

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onObjectRetained()V
    .locals 0

    return-void
.end method
