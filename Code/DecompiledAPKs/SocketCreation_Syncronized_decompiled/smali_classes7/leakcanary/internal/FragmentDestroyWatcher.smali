.class public final Lleakcanary/internal/FragmentDestroyWatcher;
.super Ljava/lang/Object;
.source "FragmentDestroyWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFragmentDestroyWatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FragmentDestroyWatcher.kt\nleakcanary/internal/FragmentDestroyWatcher\n*L\n1#1,124:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004H\u0002J<\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c2\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0002J$\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lleakcanary/internal/FragmentDestroyWatcher;",
        "",
        "()V",
        "ANDROIDX_FRAGMENT_CLASS_NAME",
        "",
        "ANDROIDX_FRAGMENT_DESTROY_WATCHER_CLASS_NAME",
        "ANDROID_SUPPORT_FRAGMENT_CLASS_NAME",
        "ANDROID_SUPPORT_FRAGMENT_DESTROY_WATCHER_CLASS_NAME",
        "classAvailable",
        "",
        "className",
        "getWatcherIfAvailable",
        "Lkotlin/Function1;",
        "Landroid/app/Activity;",
        "",
        "fragmentClassName",
        "watcherClassName",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "configProvider",
        "Lkotlin/Function0;",
        "Lleakcanary/AppWatcher$Config;",
        "install",
        "application",
        "Landroid/app/Application;",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ANDROIDX_FRAGMENT_CLASS_NAME:Ljava/lang/String; = "androidx.fragment.app.Fragment"

.field private static final ANDROIDX_FRAGMENT_DESTROY_WATCHER_CLASS_NAME:Ljava/lang/String; = "leakcanary.internal.AndroidXFragmentDestroyWatcher"

.field private static final ANDROID_SUPPORT_FRAGMENT_CLASS_NAME:Ljava/lang/String; = "androidx.fragment.app.Fragment"

.field private static final ANDROID_SUPPORT_FRAGMENT_DESTROY_WATCHER_CLASS_NAME:Ljava/lang/String; = "leakcanary.internal.AndroidSupportFragmentDestroyWatcher"

.field public static final INSTANCE:Lleakcanary/internal/FragmentDestroyWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lleakcanary/internal/FragmentDestroyWatcher;

    invoke-direct {v0}, Lleakcanary/internal/FragmentDestroyWatcher;-><init>()V

    sput-object v0, Lleakcanary/internal/FragmentDestroyWatcher;->INSTANCE:Lleakcanary/internal/FragmentDestroyWatcher;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final classAvailable(Ljava/lang/String;)Z
    .locals 0

    .line 109
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x1

    goto :goto_0

    :catchall_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final getWatcherIfAvailable(Ljava/lang/String;Ljava/lang/String;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)Lkotlin/jvm/functions/Function1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lleakcanary/ObjectWatcher;",
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;)",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Activity;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 94
    invoke-direct {p0, p1}, Lleakcanary/internal/FragmentDestroyWatcher;->classAvailable(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 95
    invoke-direct {p0, p2}, Lleakcanary/internal/FragmentDestroyWatcher;->classAvailable(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 97
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    const/4 p2, 0x2

    new-array v0, p2, [Ljava/lang/Class;

    .line 98
    const-class v1, Lleakcanary/ObjectWatcher;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lkotlin/jvm/functions/Function0;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p1

    const-string v0, "Class.forName(watcherCla\u2026a, Function0::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array p2, p2, [Ljava/lang/Object;

    aput-object p3, p2, v2

    aput-object p4, p2, v3

    .line 100
    invoke-virtual {p1, p2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1, v3}, Lkotlin/jvm/internal/TypeIntrinsics;->beforeCheckcastToFunctionOfArity(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/jvm/functions/Function1;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type (android.app.Activity) -> kotlin.Unit"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final install(Landroid/app/Application;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lleakcanary/ObjectWatcher;",
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;)V"
        }
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "objectWatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 47
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    .line 49
    new-instance v1, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;

    invoke-direct {v1, p2, p3}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;-><init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V

    .line 48
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v1, "androidx.fragment.app.Fragment"

    const-string v2, "leakcanary.internal.AndroidXFragmentDestroyWatcher"

    .line 53
    invoke-direct {p0, v1, v2, p2, p3}, Lleakcanary/internal/FragmentDestroyWatcher;->getWatcherIfAvailable(Ljava/lang/String;Ljava/lang/String;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 59
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const-string v2, "leakcanary.internal.AndroidSupportFragmentDestroyWatcher"

    .line 62
    invoke-direct {p0, v1, v2, p2, p3}, Lleakcanary/internal/FragmentDestroyWatcher;->getWatcherIfAvailable(Ljava/lang/String;Ljava/lang/String;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 68
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    if-nez p2, :cond_3

    return-void

    .line 75
    :cond_3
    new-instance p2, Lleakcanary/internal/FragmentDestroyWatcher$install$3;

    invoke-direct {p2, v0}, Lleakcanary/internal/FragmentDestroyWatcher$install$3;-><init>(Ljava/util/List;)V

    check-cast p2, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, p2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method
