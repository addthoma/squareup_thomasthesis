.class public final Lleakcanary/internal/InternalAppWatcher;
.super Ljava/lang/Object;
.source "InternalAppWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInternalAppWatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InternalAppWatcher.kt\nleakcanary/internal/InternalAppWatcher\n+ 2 SharkLog.kt\nshark/SharkLog\n*L\n1#1,104:1\n31#2,3:105\n*E\n*S KotlinDebug\n*F\n+ 1 InternalAppWatcher.kt\nleakcanary/internal/InternalAppWatcher\n*L\n66#1,3:105\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000E\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007*\u0001\u000c\u0008\u00c0\u0002\u0018\u00002\u00020\u0001:\u0001%B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010 \u001a\u00020\u001fH\u0002J\u000e\u0010!\u001a\u00020\u001f2\u0006\u0010\u0003\u001a\u00020\u0004J\u001a\u0010\"\u001a\u0002H#\"\n\u0008\u0000\u0010#\u0018\u0001*\u00020\u0001H\u0086\u0008\u00a2\u0006\u0002\u0010$R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\rR\u001b\u0010\u000e\u001a\u00020\u000f8FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0011\u0010\u0012\u001a\u0004\u0008\u000e\u0010\u0010R\u0011\u0010\u0013\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0010R\u001b\u0010\u0014\u001a\u00020\u00158BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0018\u0010\u0012\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u001a\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u001f0\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lleakcanary/internal/InternalAppWatcher;",
        "",
        "()V",
        "application",
        "Landroid/app/Application;",
        "getApplication",
        "()Landroid/app/Application;",
        "setApplication",
        "(Landroid/app/Application;)V",
        "checkRetainedExecutor",
        "Ljava/util/concurrent/Executor;",
        "clock",
        "leakcanary/internal/InternalAppWatcher$clock$1",
        "Lleakcanary/internal/InternalAppWatcher$clock$1;",
        "isDebuggableBuild",
        "",
        "()Z",
        "isDebuggableBuild$delegate",
        "Lkotlin/Lazy;",
        "isInstalled",
        "mainHandler",
        "Landroid/os/Handler;",
        "getMainHandler",
        "()Landroid/os/Handler;",
        "mainHandler$delegate",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "getObjectWatcher",
        "()Lleakcanary/ObjectWatcher;",
        "onAppWatcherInstalled",
        "Lkotlin/Function1;",
        "",
        "checkMainThread",
        "install",
        "noOpDelegate",
        "T",
        "()Ljava/lang/Object;",
        "NoLeakCanary",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final INSTANCE:Lleakcanary/internal/InternalAppWatcher;

.field public static application:Landroid/app/Application;

.field private static final checkRetainedExecutor:Ljava/util/concurrent/Executor;

.field private static final clock:Lleakcanary/internal/InternalAppWatcher$clock$1;

.field private static final isDebuggableBuild$delegate:Lkotlin/Lazy;

.field private static final mainHandler$delegate:Lkotlin/Lazy;

.field private static final objectWatcher:Lleakcanary/ObjectWatcher;

.field private static final onAppWatcherInstalled:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Application;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lleakcanary/internal/InternalAppWatcher;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "isDebuggableBuild"

    const-string v5, "isDebuggableBuild()Z"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "mainHandler"

    const-string v4, "getMainHandler()Landroid/os/Handler;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lleakcanary/internal/InternalAppWatcher;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    .line 20
    new-instance v0, Lleakcanary/internal/InternalAppWatcher;

    invoke-direct {v0}, Lleakcanary/internal/InternalAppWatcher;-><init>()V

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    .line 27
    sget-object v0, Lleakcanary/internal/InternalAppWatcher$isDebuggableBuild$2;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$isDebuggableBuild$2;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->isDebuggableBuild$delegate:Lkotlin/Lazy;

    .line 33
    new-instance v0, Lleakcanary/internal/InternalAppWatcher$clock$1;

    invoke-direct {v0}, Lleakcanary/internal/InternalAppWatcher$clock$1;-><init>()V

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->clock:Lleakcanary/internal/InternalAppWatcher$clock$1;

    .line 39
    sget-object v0, Lleakcanary/internal/InternalAppWatcher$mainHandler$2;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$mainHandler$2;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->mainHandler$delegate:Lkotlin/Lazy;

    :try_start_0
    const-string v0, "leakcanary.internal.InternalLeakCanary"

    .line 45
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(\"leakcanar\u2026rnal.InternalLeakCanary\")"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "INSTANCE"

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    sget-object v0, Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$NoLeakCanary;

    :goto_0
    if-eqz v0, :cond_0

    .line 52
    invoke-static {v0, v2}, Lkotlin/jvm/internal/TypeIntrinsics;->beforeCheckcastToFunctionOfArity(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function1;

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->onAppWatcherInstalled:Lkotlin/jvm/functions/Function1;

    .line 55
    sget-object v0, Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;

    check-cast v0, Ljava/util/concurrent/Executor;

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->checkRetainedExecutor:Ljava/util/concurrent/Executor;

    .line 58
    new-instance v0, Lleakcanary/ObjectWatcher;

    .line 59
    sget-object v1, Lleakcanary/internal/InternalAppWatcher;->clock:Lleakcanary/internal/InternalAppWatcher$clock$1;

    check-cast v1, Lleakcanary/Clock;

    .line 60
    sget-object v2, Lleakcanary/internal/InternalAppWatcher;->checkRetainedExecutor:Ljava/util/concurrent/Executor;

    .line 61
    sget-object v3, Lleakcanary/internal/InternalAppWatcher$objectWatcher$1;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$objectWatcher$1;

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 58
    invoke-direct {v0, v1, v2, v3}, Lleakcanary/ObjectWatcher;-><init>(Lleakcanary/Clock;Ljava/util/concurrent/Executor;Lkotlin/jvm/functions/Function0;)V

    sput-object v0, Lleakcanary/internal/InternalAppWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    return-void

    .line 52
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type (android.app.Application) -> kotlin.Unit"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getMainHandler$p(Lleakcanary/internal/InternalAppWatcher;)Landroid/os/Handler;
    .locals 0

    .line 20
    invoke-direct {p0}, Lleakcanary/internal/InternalAppWatcher;->getMainHandler()Landroid/os/Handler;

    move-result-object p0

    return-object p0
.end method

.method private final checkMainThread()V
    .locals 3

    .line 90
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    const-string v1, "Looper.getMainLooper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    return-void

    .line 91
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Should be called from the main thread, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final getMainHandler()Landroid/os/Handler;
    .locals 3

    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->mainHandler$delegate:Lkotlin/Lazy;

    sget-object v1, Lleakcanary/internal/InternalAppWatcher;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final getApplication()Landroid/app/Application;
    .locals 2

    .line 31
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->application:Landroid/app/Application;

    if-nez v0, :cond_0

    const-string v1, "application"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getObjectWatcher()Lleakcanary/ObjectWatcher;
    .locals 1

    .line 58
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    return-object v0
.end method

.method public final install(Landroid/app/Application;)V
    .locals 3

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    sget-object v0, Lshark/SharkLog;->INSTANCE:Lshark/SharkLog;

    new-instance v1, Lleakcanary/internal/DefaultCanaryLog;

    invoke-direct {v1}, Lleakcanary/internal/DefaultCanaryLog;-><init>()V

    check-cast v1, Lshark/SharkLog$Logger;

    invoke-virtual {v0, v1}, Lshark/SharkLog;->setLogger(Lshark/SharkLog$Logger;)V

    .line 66
    sget-object v0, Lshark/SharkLog;->INSTANCE:Lshark/SharkLog;

    .line 105
    invoke-virtual {v0}, Lshark/SharkLog;->getLogger()Lshark/SharkLog$Logger;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Installing AppWatcher"

    .line 66
    invoke-interface {v0, v1}, Lshark/SharkLog$Logger;->d(Ljava/lang/String;)V

    .line 67
    :cond_0
    invoke-direct {p0}, Lleakcanary/internal/InternalAppWatcher;->checkMainThread()V

    .line 68
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->application:Landroid/app/Application;

    if-eqz v0, :cond_1

    return-void

    .line 71
    :cond_1
    sput-object p1, Lleakcanary/internal/InternalAppWatcher;->application:Landroid/app/Application;

    .line 73
    sget-object v0, Lleakcanary/internal/InternalAppWatcher$install$configProvider$1;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$install$configProvider$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 74
    sget-object v1, Lleakcanary/internal/ActivityDestroyWatcher;->Companion:Lleakcanary/internal/ActivityDestroyWatcher$Companion;

    sget-object v2, Lleakcanary/internal/InternalAppWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    invoke-virtual {v1, p1, v2, v0}, Lleakcanary/internal/ActivityDestroyWatcher$Companion;->install(Landroid/app/Application;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V

    .line 75
    sget-object v1, Lleakcanary/internal/FragmentDestroyWatcher;->INSTANCE:Lleakcanary/internal/FragmentDestroyWatcher;

    sget-object v2, Lleakcanary/internal/InternalAppWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    invoke-virtual {v1, p1, v2, v0}, Lleakcanary/internal/FragmentDestroyWatcher;->install(Landroid/app/Application;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V

    .line 76
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->onAppWatcherInstalled:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final isDebuggableBuild()Z
    .locals 3

    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->isDebuggableBuild$delegate:Lkotlin/Lazy;

    sget-object v1, Lleakcanary/internal/InternalAppWatcher;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final isInstalled()Z
    .locals 1

    .line 23
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->application:Landroid/app/Application;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final synthetic noOpDelegate()Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    const-string v0, "T"

    const/4 v1, 0x4

    .line 80
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    .line 81
    sget-object v2, Lleakcanary/internal/InternalAppWatcher$noOpDelegate$noOpHandler$1;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$noOpDelegate$noOpHandler$1;

    check-cast v2, Ljava/lang/reflect/InvocationHandler;

    .line 85
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    .line 84
    invoke-static {v3, v5, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    check-cast v1, Ljava/lang/Object;

    return-object v1
.end method

.method public final setApplication(Landroid/app/Application;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sput-object p1, Lleakcanary/internal/InternalAppWatcher;->application:Landroid/app/Application;

    return-void
.end method
