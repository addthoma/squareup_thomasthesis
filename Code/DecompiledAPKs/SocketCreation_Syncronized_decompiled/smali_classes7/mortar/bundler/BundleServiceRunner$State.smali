.class final enum Lmortar/bundler/BundleServiceRunner$State;
.super Ljava/lang/Enum;
.source "BundleServiceRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/bundler/BundleServiceRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmortar/bundler/BundleServiceRunner$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmortar/bundler/BundleServiceRunner$State;

.field public static final enum IDLE:Lmortar/bundler/BundleServiceRunner$State;

.field public static final enum LOADING:Lmortar/bundler/BundleServiceRunner$State;

.field public static final enum SAVING:Lmortar/bundler/BundleServiceRunner$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 33
    new-instance v0, Lmortar/bundler/BundleServiceRunner$State;

    const/4 v1, 0x0

    const-string v2, "IDLE"

    invoke-direct {v0, v2, v1}, Lmortar/bundler/BundleServiceRunner$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    new-instance v0, Lmortar/bundler/BundleServiceRunner$State;

    const/4 v2, 0x1

    const-string v3, "LOADING"

    invoke-direct {v0, v3, v2}, Lmortar/bundler/BundleServiceRunner$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmortar/bundler/BundleServiceRunner$State;->LOADING:Lmortar/bundler/BundleServiceRunner$State;

    new-instance v0, Lmortar/bundler/BundleServiceRunner$State;

    const/4 v3, 0x2

    const-string v4, "SAVING"

    invoke-direct {v0, v4, v3}, Lmortar/bundler/BundleServiceRunner$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmortar/bundler/BundleServiceRunner$State;->SAVING:Lmortar/bundler/BundleServiceRunner$State;

    const/4 v0, 0x3

    new-array v0, v0, [Lmortar/bundler/BundleServiceRunner$State;

    .line 32
    sget-object v4, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    aput-object v4, v0, v1

    sget-object v1, Lmortar/bundler/BundleServiceRunner$State;->LOADING:Lmortar/bundler/BundleServiceRunner$State;

    aput-object v1, v0, v2

    sget-object v1, Lmortar/bundler/BundleServiceRunner$State;->SAVING:Lmortar/bundler/BundleServiceRunner$State;

    aput-object v1, v0, v3

    sput-object v0, Lmortar/bundler/BundleServiceRunner$State;->$VALUES:[Lmortar/bundler/BundleServiceRunner$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmortar/bundler/BundleServiceRunner$State;
    .locals 1

    .line 32
    const-class v0, Lmortar/bundler/BundleServiceRunner$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lmortar/bundler/BundleServiceRunner$State;

    return-object p0
.end method

.method public static values()[Lmortar/bundler/BundleServiceRunner$State;
    .locals 1

    .line 32
    sget-object v0, Lmortar/bundler/BundleServiceRunner$State;->$VALUES:[Lmortar/bundler/BundleServiceRunner$State;

    invoke-virtual {v0}, [Lmortar/bundler/BundleServiceRunner$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmortar/bundler/BundleServiceRunner$State;

    return-object v0
.end method
