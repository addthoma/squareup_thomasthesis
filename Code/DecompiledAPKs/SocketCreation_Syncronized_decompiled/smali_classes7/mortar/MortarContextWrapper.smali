.class Lmortar/MortarContextWrapper;
.super Landroid/content/ContextWrapper;
.source "MortarContextWrapper.java"


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field private final scope:Lmortar/MortarScope;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmortar/MortarScope;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Lmortar/MortarContextWrapper;->scope:Lmortar/MortarScope;

    return-void
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const-string v0, "layout_inflater"

    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    iget-object p1, p0, Lmortar/MortarContextWrapper;->inflater:Landroid/view/LayoutInflater;

    if-nez p1, :cond_0

    .line 35
    invoke-virtual {p0}, Lmortar/MortarContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lmortar/MortarContextWrapper;->inflater:Landroid/view/LayoutInflater;

    .line 37
    :cond_0
    iget-object p1, p0, Lmortar/MortarContextWrapper;->inflater:Landroid/view/LayoutInflater;

    return-object p1

    .line 39
    :cond_1
    iget-object v0, p0, Lmortar/MortarContextWrapper;->scope:Lmortar/MortarScope;

    invoke-virtual {v0, p1}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmortar/MortarContextWrapper;->scope:Lmortar/MortarScope;

    invoke-virtual {v0, p1}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1
.end method
