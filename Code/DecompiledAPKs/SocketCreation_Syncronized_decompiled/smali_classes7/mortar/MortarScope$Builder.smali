.class public final Lmortar/MortarScope$Builder;
.super Ljava/lang/Object;
.source "MortarScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final parent:Lmortar/MortarScope;

.field private final serviceProviders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lmortar/MortarScope;)V
    .locals 1

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lmortar/MortarScope$Builder;->serviceProviders:Ljava/util/Map;

    .line 272
    iput-object p1, p0, Lmortar/MortarScope$Builder;->parent:Lmortar/MortarScope;

    return-void
.end method

.method private doWithService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;
    .locals 4

    .line 321
    iget-object v0, p0, Lmortar/MortarScope$Builder;->serviceProviders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz p2, :cond_1

    if-nez v0, :cond_0

    return-object p0

    .line 326
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, p1

    const/4 p1, 0x2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, p1

    const-string p1, "Scope builder already bound \"%s\" to service \"%s\", cannot be rebound to \"%s\""

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 323
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "service == null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public build(Ljava/lang/String;)Lmortar/MortarScope;
    .locals 6

    const-string v0, ">>>"

    .line 299
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-nez v1, :cond_4

    .line 304
    new-instance v0, Lmortar/MortarScope;

    iget-object v1, p0, Lmortar/MortarScope$Builder;->parent:Lmortar/MortarScope;

    iget-object v5, p0, Lmortar/MortarScope$Builder;->serviceProviders:Ljava/util/Map;

    invoke-direct {v0, p1, v1, v5}, Lmortar/MortarScope;-><init>(Ljava/lang/String;Lmortar/MortarScope;Ljava/util/Map;)V

    .line 305
    iget-object v1, p0, Lmortar/MortarScope$Builder;->parent:Lmortar/MortarScope;

    if-eqz v1, :cond_1

    .line 306
    iget-object v1, v1, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    iget-object v1, p0, Lmortar/MortarScope$Builder;->parent:Lmortar/MortarScope;

    iget-object v1, v1, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 307
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p1, v1, v2

    const-string p1, "Scope \"%s\" already has a child named \"%s\""

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_1
    :goto_0
    iget-object p1, p0, Lmortar/MortarScope$Builder;->serviceProviders:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 315
    instance-of v2, v1, Lmortar/Scoped;

    if-eqz v2, :cond_2

    check-cast v1, Lmortar/Scoped;

    invoke-virtual {v0, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_1

    :cond_3
    return-object v0

    .line 300
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v3

    aput-object v0, v4, v2

    const-string p1, "Name \"%s\" must not contain \'%s\'"

    invoke-static {p1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;
    .locals 3

    .line 280
    instance-of v0, p2, Lmortar/Scoped;

    if-nez v0, :cond_0

    .line 285
    invoke-direct {p0, p1, p2}, Lmortar/MortarScope$Builder;->doWithService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    move-result-object p1

    return-object p1

    .line 281
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const/4 p1, 0x2

    const-class p2, Lmortar/Scoped;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, p1

    const-string p1, "For service %s, %s must not be an instance of %s, use \"withScopedService\" instead."

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public withService(Ljava/lang/String;Lmortar/Scoped;)Lmortar/MortarScope$Builder;
    .locals 0

    .line 294
    invoke-direct {p0, p1, p2}, Lmortar/MortarScope$Builder;->doWithService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    move-result-object p1

    return-object p1
.end method
