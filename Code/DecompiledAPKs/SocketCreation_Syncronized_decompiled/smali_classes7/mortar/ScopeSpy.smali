.class public Lmortar/ScopeSpy;
.super Ljava/lang/Object;
.source "ScopeSpy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addLeafName(Lmortar/MortarScope;Ljava/lang/StringBuilder;)V
    .locals 3

    .line 45
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, " | "

    .line 46
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    :cond_0
    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x2d

    .line 50
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x0

    .line 52
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    const/16 v0, 0x2e

    .line 55
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 57
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 59
    :cond_2
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static addLeafScopes(Lmortar/MortarScope;Ljava/lang/StringBuilder;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 35
    invoke-static {p0, p1}, Lmortar/ScopeSpy;->addLeafName(Lmortar/MortarScope;Ljava/lang/StringBuilder;)V

    return-void

    .line 39
    :cond_0
    iget-object p0, p0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/MortarScope;

    .line 40
    invoke-static {v0, p1}, Lmortar/ScopeSpy;->addLeafScopes(Lmortar/MortarScope;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static getRootScope(Lmortar/MortarScope;)Lmortar/MortarScope;
    .locals 1

    .line 27
    :goto_0
    iget-object v0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    .line 28
    iget-object p0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public static hasChildren(Lmortar/MortarScope;)Z
    .locals 0

    .line 23
    iget-object p0, p0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public static parentScope(Lmortar/MortarScope;)Lmortar/MortarScope;
    .locals 0

    .line 19
    iget-object p0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    return-object p0
.end method

.method public static visibleScreens(Lmortar/MortarScope;)Ljava/lang/String;
    .locals 1

    .line 12
    invoke-static {p0}, Lmortar/ScopeSpy;->getRootScope(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object p0

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    invoke-static {p0, v0}, Lmortar/ScopeSpy;->addLeafScopes(Lmortar/MortarScope;Ljava/lang/StringBuilder;)V

    .line 15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
