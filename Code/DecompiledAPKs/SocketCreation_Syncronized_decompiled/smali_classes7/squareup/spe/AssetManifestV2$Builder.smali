.class public final Lsquareup/spe/AssetManifestV2$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AssetManifestV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/AssetManifestV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/AssetManifestV2;",
        "Lsquareup/spe/AssetManifestV2$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public percent_complete:Ljava/lang/Integer;

.field public valid:Ljava/lang/Boolean;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lsquareup/spe/AssetManifestV2$Builder;->build()Lsquareup/spe/AssetManifestV2;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/AssetManifestV2;
    .locals 5

    .line 135
    new-instance v0, Lsquareup/spe/AssetManifestV2;

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2$Builder;->version:Ljava/lang/Integer;

    iget-object v2, p0, Lsquareup/spe/AssetManifestV2$Builder;->valid:Ljava/lang/Boolean;

    iget-object v3, p0, Lsquareup/spe/AssetManifestV2$Builder;->percent_complete:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lsquareup/spe/AssetManifestV2;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public percent_complete(Ljava/lang/Integer;)Lsquareup/spe/AssetManifestV2$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lsquareup/spe/AssetManifestV2$Builder;->percent_complete:Ljava/lang/Integer;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lsquareup/spe/AssetManifestV2$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lsquareup/spe/AssetManifestV2$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lsquareup/spe/AssetManifestV2$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lsquareup/spe/AssetManifestV2$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
