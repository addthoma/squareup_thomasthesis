.class public final Lsquareup/spe/K450Manifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "K450Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/K450Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/K450Manifest;",
        "Lsquareup/spe/K450Manifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public chip_revision:Lokio/ByteString;

.field public chipid:Lokio/ByteString;

.field public configuration:Lsquareup/spe/UnitConfiguration;

.field public cpu0_firmware_a:Lsquareup/spe/AssetManifest;

.field public cpu0_firmware_b:Lsquareup/spe/AssetManifest;

.field public cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

.field public cpu1_firmware_a:Lsquareup/spe/AssetManifest;

.field public cpu1_firmware_b:Lsquareup/spe/AssetManifest;

.field public cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

.field public rak_select:Lsquareup/spe/RAKSelect;

.field public reader_authority_fingerprint:Lokio/ByteString;

.field public signer_fingerprint:Lokio/ByteString;

.field public tms_capk_a:Lsquareup/spe/AssetManifest;

.field public tms_capk_b:Lsquareup/spe/AssetManifest;

.field public tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 287
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 256
    invoke-virtual {p0}, Lsquareup/spe/K450Manifest$Builder;->build()Lsquareup/spe/K450Manifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/K450Manifest;
    .locals 20

    move-object/from16 v0, p0

    .line 367
    new-instance v18, Lsquareup/spe/K450Manifest;

    move-object/from16 v1, v18

    iget-object v2, v0, Lsquareup/spe/K450Manifest$Builder;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v3, v0, Lsquareup/spe/K450Manifest$Builder;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v4, v0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v5, v0, Lsquareup/spe/K450Manifest$Builder;->chipid:Lokio/ByteString;

    iget-object v6, v0, Lsquareup/spe/K450Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    iget-object v7, v0, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    iget-object v8, v0, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    iget-object v9, v0, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    iget-object v10, v0, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    iget-object v11, v0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a:Lsquareup/spe/AssetManifest;

    iget-object v12, v0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b:Lsquareup/spe/AssetManifest;

    iget-object v13, v0, Lsquareup/spe/K450Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    iget-object v14, v0, Lsquareup/spe/K450Manifest$Builder;->reader_authority_fingerprint:Lokio/ByteString;

    iget-object v15, v0, Lsquareup/spe/K450Manifest$Builder;->rak_select:Lsquareup/spe/RAKSelect;

    move-object/from16 v19, v1

    iget-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->chip_revision:Lokio/ByteString;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lsquareup/spe/K450Manifest;-><init>(Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Lsquareup/spe/RAKSelect;Lokio/ByteString;Lokio/ByteString;)V

    return-object v18
.end method

.method public chip_revision(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 361
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->chip_revision:Lokio/ByteString;

    return-object p0
.end method

.method public chipid(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 306
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->chipid:Lokio/ByteString;

    return-object p0
.end method

.method public configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    return-object p0
.end method

.method public cpu0_firmware_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu0_firmware_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu0_running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    return-object p0
.end method

.method public cpu1_firmware_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu1_firmware_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu1_running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 296
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    return-object p0
.end method

.method public rak_select(Lsquareup/spe/RAKSelect;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->rak_select:Lsquareup/spe/RAKSelect;

    return-object p0
.end method

.method public reader_authority_fingerprint(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->reader_authority_fingerprint:Lokio/ByteString;

    return-object p0
.end method

.method public signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    return-object p0
.end method

.method public tms_capk_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public tms_capk_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public tms_capk_running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K450Manifest$Builder;
    .locals 0

    .line 301
    iput-object p1, p0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    return-object p0
.end method
