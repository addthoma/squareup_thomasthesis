.class public final enum Lsquareup/spe/RAKSelect;
.super Ljava/lang/Enum;
.source "RAKSelect.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/RAKSelect$ProtoAdapter_RAKSelect;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/spe/RAKSelect;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/spe/RAKSelect;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/RAKSelect;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INVALID_1:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_2:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_3:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_4:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_5:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_6:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_7:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_8:Lsquareup/spe/RAKSelect;

.field public static final enum INVALID_9:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_FUSE1:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_FUSE2:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_FUSE3:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_ROM1:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_ROM2:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_ROM3:Lsquareup/spe/RAKSelect;

.field public static final enum RAK_HASH_IN_ROM4:Lsquareup/spe/RAKSelect;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 14
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "RAK_HASH_IN_FUSE1"

    invoke-direct {v0, v3, v2, v1}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE1:Lsquareup/spe/RAKSelect;

    .line 16
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/4 v3, 0x2

    const-string v4, "RAK_HASH_IN_FUSE2"

    invoke-direct {v0, v4, v1, v3}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE2:Lsquareup/spe/RAKSelect;

    .line 18
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/4 v4, 0x3

    const-string v5, "RAK_HASH_IN_FUSE3"

    invoke-direct {v0, v5, v3, v4}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE3:Lsquareup/spe/RAKSelect;

    .line 20
    new-instance v0, Lsquareup/spe/RAKSelect;

    const-string v5, "RAK_HASH_IN_ROM1"

    invoke-direct {v0, v5, v4, v2}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM1:Lsquareup/spe/RAKSelect;

    .line 22
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/4 v5, 0x4

    const-string v6, "RAK_HASH_IN_ROM2"

    invoke-direct {v0, v6, v5, v5}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM2:Lsquareup/spe/RAKSelect;

    .line 24
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v6, 0x8

    const/4 v7, 0x5

    const-string v8, "RAK_HASH_IN_ROM3"

    invoke-direct {v0, v8, v7, v6}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM3:Lsquareup/spe/RAKSelect;

    .line 26
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v8, 0xc

    const/4 v9, 0x6

    const-string v10, "RAK_HASH_IN_ROM4"

    invoke-direct {v0, v10, v9, v8}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM4:Lsquareup/spe/RAKSelect;

    .line 28
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/4 v10, 0x7

    const-string v11, "INVALID_1"

    invoke-direct {v0, v11, v10, v7}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_1:Lsquareup/spe/RAKSelect;

    .line 30
    new-instance v0, Lsquareup/spe/RAKSelect;

    const-string v11, "INVALID_2"

    invoke-direct {v0, v11, v6, v9}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_2:Lsquareup/spe/RAKSelect;

    .line 32
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v11, 0x9

    const-string v12, "INVALID_3"

    invoke-direct {v0, v12, v11, v10}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_3:Lsquareup/spe/RAKSelect;

    .line 34
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v12, 0xa

    const-string v13, "INVALID_4"

    invoke-direct {v0, v13, v12, v11}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_4:Lsquareup/spe/RAKSelect;

    .line 36
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v13, 0xb

    const-string v14, "INVALID_5"

    invoke-direct {v0, v14, v13, v12}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_5:Lsquareup/spe/RAKSelect;

    .line 38
    new-instance v0, Lsquareup/spe/RAKSelect;

    const-string v14, "INVALID_6"

    invoke-direct {v0, v14, v8, v13}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_6:Lsquareup/spe/RAKSelect;

    .line 40
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v14, 0xd

    const-string v15, "INVALID_7"

    invoke-direct {v0, v15, v14, v14}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_7:Lsquareup/spe/RAKSelect;

    .line 42
    new-instance v0, Lsquareup/spe/RAKSelect;

    const/16 v15, 0xe

    const-string v14, "INVALID_8"

    invoke-direct {v0, v14, v15, v15}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_8:Lsquareup/spe/RAKSelect;

    .line 44
    new-instance v0, Lsquareup/spe/RAKSelect;

    const-string v14, "INVALID_9"

    const/16 v15, 0xf

    const/16 v8, 0xf

    invoke-direct {v0, v14, v15, v8}, Lsquareup/spe/RAKSelect;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/RAKSelect;->INVALID_9:Lsquareup/spe/RAKSelect;

    const/16 v0, 0x10

    new-array v0, v0, [Lsquareup/spe/RAKSelect;

    .line 13
    sget-object v8, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE1:Lsquareup/spe/RAKSelect;

    aput-object v8, v0, v2

    sget-object v2, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE2:Lsquareup/spe/RAKSelect;

    aput-object v2, v0, v1

    sget-object v1, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE3:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM1:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v4

    sget-object v1, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM2:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v5

    sget-object v1, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM3:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v7

    sget-object v1, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM4:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v9

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_1:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v10

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_2:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v6

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_3:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v11

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_4:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v12

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_5:Lsquareup/spe/RAKSelect;

    aput-object v1, v0, v13

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_6:Lsquareup/spe/RAKSelect;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_7:Lsquareup/spe/RAKSelect;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_8:Lsquareup/spe/RAKSelect;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/RAKSelect;->INVALID_9:Lsquareup/spe/RAKSelect;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lsquareup/spe/RAKSelect;->$VALUES:[Lsquareup/spe/RAKSelect;

    .line 46
    new-instance v0, Lsquareup/spe/RAKSelect$ProtoAdapter_RAKSelect;

    invoke-direct {v0}, Lsquareup/spe/RAKSelect$ProtoAdapter_RAKSelect;-><init>()V

    sput-object v0, Lsquareup/spe/RAKSelect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lsquareup/spe/RAKSelect;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/spe/RAKSelect;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 74
    :pswitch_0
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_9:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 73
    :pswitch_1
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_8:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 72
    :pswitch_2
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_7:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 65
    :pswitch_3
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM4:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 71
    :pswitch_4
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_6:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 70
    :pswitch_5
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_5:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 69
    :pswitch_6
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_4:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 64
    :pswitch_7
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM3:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 68
    :pswitch_8
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_3:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 67
    :pswitch_9
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_2:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 66
    :pswitch_a
    sget-object p0, Lsquareup/spe/RAKSelect;->INVALID_1:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 63
    :pswitch_b
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM2:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 61
    :pswitch_c
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE3:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 60
    :pswitch_d
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE2:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 59
    :pswitch_e
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE1:Lsquareup/spe/RAKSelect;

    return-object p0

    .line 62
    :pswitch_f
    sget-object p0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_ROM1:Lsquareup/spe/RAKSelect;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/spe/RAKSelect;
    .locals 1

    .line 13
    const-class v0, Lsquareup/spe/RAKSelect;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/spe/RAKSelect;

    return-object p0
.end method

.method public static values()[Lsquareup/spe/RAKSelect;
    .locals 1

    .line 13
    sget-object v0, Lsquareup/spe/RAKSelect;->$VALUES:[Lsquareup/spe/RAKSelect;

    invoke-virtual {v0}, [Lsquareup/spe/RAKSelect;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/spe/RAKSelect;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 81
    iget v0, p0, Lsquareup/spe/RAKSelect;->value:I

    return v0
.end method
