.class public final Lsquareup/items/merchant/CogsId$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CogsId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/CogsId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/CogsId;",
        "Lsquareup/items/merchant/CogsId$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public object_id:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lsquareup/items/merchant/CogsId$Builder;->build()Lsquareup/items/merchant/CogsId;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/CogsId;
    .locals 4

    .line 115
    new-instance v0, Lsquareup/items/merchant/CogsId;

    iget-object v1, p0, Lsquareup/items/merchant/CogsId$Builder;->object_id:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/CogsId$Builder;->unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/items/merchant/CogsId;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public object_id(Ljava/lang/String;)Lsquareup/items/merchant/CogsId$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lsquareup/items/merchant/CogsId$Builder;->object_id:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lsquareup/items/merchant/CogsId$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lsquareup/items/merchant/CogsId$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
