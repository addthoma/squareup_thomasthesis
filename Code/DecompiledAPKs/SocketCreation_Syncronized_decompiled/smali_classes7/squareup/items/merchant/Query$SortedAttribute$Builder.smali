.class public final Lsquareup/items/merchant/Query$SortedAttribute$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$SortedAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Query$SortedAttribute;",
        "Lsquareup/items/merchant/Query$SortedAttribute$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public initial_attribute_value:Ljava/lang/String;

.field public sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 636
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 631
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->build()Lsquareup/items/merchant/Query$SortedAttribute;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Query$SortedAttribute;
    .locals 4

    .line 651
    new-instance v0, Lsquareup/items/merchant/Query$SortedAttribute;

    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->initial_attribute_value:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/items/merchant/Query$SortedAttribute;-><init>(Ljava/lang/String;Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;Lokio/ByteString;)V

    return-object v0
.end method

.method public initial_attribute_value(Ljava/lang/String;)Lsquareup/items/merchant/Query$SortedAttribute$Builder;
    .locals 0

    .line 640
    iput-object p1, p0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->initial_attribute_value:Ljava/lang/String;

    return-object p0
.end method

.method public sort_order(Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;)Lsquareup/items/merchant/Query$SortedAttribute$Builder;
    .locals 0

    .line 645
    iput-object p1, p0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-object p0
.end method
