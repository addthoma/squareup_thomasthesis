.class public final Lsquareup/items/merchant/Query$NeighborType;
.super Lcom/squareup/wire/Message;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NeighborType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;,
        Lsquareup/items/merchant/Query$NeighborType$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Query$NeighborType;",
        "Lsquareup/items/merchant/Query$NeighborType$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$NeighborType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OBJECT_TYPE:Lsquareup/items/merchant/CatalogObjectType;

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_definition_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final object_type:Lsquareup/items/merchant/CatalogObjectType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CatalogObjectType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1068
    new-instance v0, Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$NeighborType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1072
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->DO_NOT_USE:Lsquareup/items/merchant/CatalogObjectType;

    sput-object v0, Lsquareup/items/merchant/Query$NeighborType;->DEFAULT_OBJECT_TYPE:Lsquareup/items/merchant/CatalogObjectType;

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/CatalogObjectType;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1094
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lsquareup/items/merchant/Query$NeighborType;-><init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/CatalogObjectType;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 1099
    sget-object v0, Lsquareup/items/merchant/Query$NeighborType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1100
    iput-object p1, p0, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    const-string p1, "attribute_definition_tokens"

    .line 1101
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1116
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Query$NeighborType;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1117
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Query$NeighborType;

    .line 1118
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$NeighborType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Query$NeighborType;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    iget-object v3, p1, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    .line 1119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    iget-object p1, p1, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    .line 1120
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 1125
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 1127
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$NeighborType;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1128
    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/items/merchant/CatalogObjectType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1129
    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1130
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1067
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$NeighborType;->newBuilder()Lsquareup/items/merchant/Query$NeighborType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Query$NeighborType$Builder;
    .locals 2

    .line 1106
    new-instance v0, Lsquareup/items/merchant/Query$NeighborType$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$NeighborType$Builder;-><init>()V

    .line 1107
    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    iput-object v1, v0, Lsquareup/items/merchant/Query$NeighborType$Builder;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    .line 1108
    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/Query$NeighborType$Builder;->attribute_definition_tokens:Ljava/util/List;

    .line 1109
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$NeighborType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Query$NeighborType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1138
    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    if-eqz v1, :cond_0

    const-string v1, ", object_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1139
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", attribute_definition_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "NeighborType{"

    .line 1140
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
