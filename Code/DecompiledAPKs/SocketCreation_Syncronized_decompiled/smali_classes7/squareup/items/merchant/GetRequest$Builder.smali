.class public final Lsquareup/items/merchant/GetRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/GetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/GetRequest;",
        "Lsquareup/items/merchant/GetRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enabled_unit_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public include_deleted:Ljava/lang/Boolean;

.field public limit:Ljava/lang/Integer;

.field public merchant_token:Ljava/lang/String;

.field public modified_after:Ljava/lang/Long;

.field public pagination_token:Ljava/lang/String;

.field public query:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query;",
            ">;"
        }
    .end annotation
.end field

.field public type:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public unit_token:Ljava/lang/String;

.field public version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 256
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 257
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/GetRequest$Builder;->query:Ljava/util/List;

    .line 258
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/GetRequest$Builder;->type:Ljava/util/List;

    .line 259
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/GetRequest$Builder;->enabled_unit_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 235
    invoke-virtual {p0}, Lsquareup/items/merchant/GetRequest$Builder;->build()Lsquareup/items/merchant/GetRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/GetRequest;
    .locals 13

    .line 353
    new-instance v12, Lsquareup/items/merchant/GetRequest;

    iget-object v1, p0, Lsquareup/items/merchant/GetRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/GetRequest$Builder;->query:Ljava/util/List;

    iget-object v3, p0, Lsquareup/items/merchant/GetRequest$Builder;->type:Ljava/util/List;

    iget-object v4, p0, Lsquareup/items/merchant/GetRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v5, p0, Lsquareup/items/merchant/GetRequest$Builder;->pagination_token:Ljava/lang/String;

    iget-object v6, p0, Lsquareup/items/merchant/GetRequest$Builder;->modified_after:Ljava/lang/Long;

    iget-object v7, p0, Lsquareup/items/merchant/GetRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v8, p0, Lsquareup/items/merchant/GetRequest$Builder;->version:Ljava/lang/Long;

    iget-object v9, p0, Lsquareup/items/merchant/GetRequest$Builder;->enabled_unit_tokens:Ljava/util/List;

    iget-object v10, p0, Lsquareup/items/merchant/GetRequest$Builder;->include_deleted:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lsquareup/items/merchant/GetRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v12
.end method

.method public enabled_unit_tokens(Ljava/util/List;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lsquareup/items/merchant/GetRequest$Builder;"
        }
    .end annotation

    .line 336
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 337
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->enabled_unit_tokens:Ljava/util/List;

    return-object p0
.end method

.method public include_deleted(Ljava/lang/Boolean;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 347
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->include_deleted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public modified_after(Ljava/lang/Long;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->modified_after:Ljava/lang/Long;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public query(Ljava/util/List;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query;",
            ">;)",
            "Lsquareup/items/merchant/GetRequest$Builder;"
        }
    .end annotation

    .line 277
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 278
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->query:Ljava/util/List;

    return-object p0
.end method

.method public type(Ljava/util/List;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;)",
            "Lsquareup/items/merchant/GetRequest$Builder;"
        }
    .end annotation

    .line 283
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 284
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->type:Ljava/util/List;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lsquareup/items/merchant/GetRequest$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lsquareup/items/merchant/GetRequest$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method
