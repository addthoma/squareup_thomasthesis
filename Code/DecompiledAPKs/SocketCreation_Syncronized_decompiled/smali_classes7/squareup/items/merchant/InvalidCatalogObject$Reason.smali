.class public final enum Lsquareup/items/merchant/InvalidCatalogObject$Reason;
.super Ljava/lang/Enum;
.source "InvalidCatalogObject.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/InvalidCatalogObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/InvalidCatalogObject$Reason$ProtoAdapter_Reason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/InvalidCatalogObject$Reason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/InvalidCatalogObject$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATTRIBUTE_DEFINITION_TOKEN_UNKNOWN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_IMMUTABLE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_TYPE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_UNIT_OVERRIDE_ONLY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_VALUE_OVER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ATTRIBUTE_VALUE_UNDER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum BROKEN_MEMBERSHIP:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum BROKEN_OBJECT_REFERENCE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum DO_NOT_USE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum DUPLICATE_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum DUPLICATE_OBJECT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum DUPLICATE_TEMPORARY_OBJECT_TOKEN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum FIXED_DISCOUNT_ONLY_FOR_PRICING_RULE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum FORBIDDEN_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum GIFT_CARD_ONE_VARIATION:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ILLEGAL_UNIT_OVERRIDE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum INT_COMPARISON_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum INVALID_CURRENCY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum INVALID_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum INVALID_STRING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum INVALID_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum INVALID_UTF8:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ITEM_OPTION_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum ITEM_OPTION_VAL_NOT_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum NULL_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum OBJECT_DELETED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum OBJECT_DISABLED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum OBJECT_ENABLED_AT_MULTIPLE_UNITS:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum OBJECT_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum PRODUCT_SET_FORMS_CYCLIC_LINK:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum REQUIRED_ATTRIBUTE_MISSING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum REQUIRED_SHARED_ATTRIBUTE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum TOO_MANY_ATTRIBUTES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum TOO_MANY_OBJECTS_OF_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum TOO_MANY_REFERENCES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum UNDEFINED_ERROR:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum UNEXPECTED_ATTRIBUTE_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum UNKNOWN_ITEM_OPTION_VAL:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum UNKNOWN_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum V1_OBJECT_VALIDATION_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum VARIATION_ITEM_OPTIONS_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum VARIATION_ITEM_OPTION_VALUE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum VERSION_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

.field public static final enum WRONG_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 164
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DO_NOT_USE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 169
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v2, 0x1

    const-string v3, "VERSION_MISMATCH"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VERSION_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 174
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v3, 0x3

    const-string v4, "ATTRIBUTE_TYPE_MISMATCH"

    const/4 v5, 0x2

    invoke-direct {v0, v4, v5, v3}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_TYPE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 179
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v4, 0x4

    const-string v5, "ATTRIBUTE_NOT_UNIQUE"

    invoke-direct {v0, v5, v3, v4}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 184
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v5, 0x5

    const-string v6, "ATTRIBUTE_VALUE_OVER_LIMIT"

    invoke-direct {v0, v6, v4, v5}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_VALUE_OVER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 189
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v6, 0x6

    const-string v7, "ATTRIBUTE_DEFINITION_TOKEN_UNKNOWN"

    invoke-direct {v0, v7, v5, v6}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_DEFINITION_TOKEN_UNKNOWN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 195
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v7, 0x7

    const-string v8, "OBJECT_NONEXISTENT"

    invoke-direct {v0, v8, v6, v7}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 201
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v8, 0x8

    const-string v9, "ATTRIBUTE_NONEXISTENT"

    invoke-direct {v0, v9, v7, v8}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 206
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v9, 0x9

    const-string v10, "V1_OBJECT_VALIDATION_FAILED"

    invoke-direct {v0, v10, v8, v9}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->V1_OBJECT_VALIDATION_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 211
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v10, 0xa

    const-string v11, "DUPLICATE_TEMPORARY_OBJECT_TOKEN"

    invoke-direct {v0, v11, v9, v10}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_TEMPORARY_OBJECT_TOKEN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 216
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v11, 0xb

    const-string v12, "REQUIRED_ATTRIBUTE_MISSING"

    invoke-direct {v0, v12, v10, v11}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->REQUIRED_ATTRIBUTE_MISSING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 221
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v12, 0xc

    const-string v13, "BROKEN_OBJECT_REFERENCE"

    invoke-direct {v0, v13, v11, v12}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_OBJECT_REFERENCE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 226
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v13, 0xd

    const-string v14, "ATTRIBUTE_VALUE_UNDER_LIMIT"

    invoke-direct {v0, v14, v12, v13}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_VALUE_UNDER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 231
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v14, 0xe

    const-string v15, "NULL_ATTRIBUTE"

    invoke-direct {v0, v15, v13, v14}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->NULL_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 236
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v15, 0xf

    const-string v13, "INVALID_OBJECT_TYPE"

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 244
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "INVALID_UNIT"

    const/16 v14, 0x10

    invoke-direct {v0, v13, v15, v14}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 249
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "OBJECT_DELETED"

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_DELETED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 256
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "ATTRIBUTE_IMMUTABLE"

    const/16 v14, 0x11

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_IMMUTABLE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 261
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "DUPLICATE_ATTRIBUTE"

    const/16 v14, 0x12

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 266
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "DUPLICATE_OBJECT"

    const/16 v14, 0x13

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_OBJECT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 273
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "BROKEN_MEMBERSHIP"

    const/16 v14, 0x14

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_MEMBERSHIP:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 280
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "ILLEGAL_UNIT_OVERRIDE"

    const/16 v14, 0x15

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ILLEGAL_UNIT_OVERRIDE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 285
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "OBJECT_ENABLED_AT_MULTIPLE_UNITS"

    const/16 v14, 0x16

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_ENABLED_AT_MULTIPLE_UNITS:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 290
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "FORBIDDEN_ATTRIBUTE"

    const/16 v14, 0x17

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->FORBIDDEN_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 295
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "OBJECT_DISABLED"

    const/16 v14, 0x18

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_DISABLED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 300
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "GIFT_CARD_ONE_VARIATION"

    const/16 v14, 0x19

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->GIFT_CARD_ONE_VARIATION:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 305
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "UNKNOWN_UNIT"

    const/16 v14, 0x1a

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNKNOWN_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 310
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "TOO_MANY_ATTRIBUTES"

    const/16 v14, 0x1b

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_ATTRIBUTES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 315
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "INVALID_STRING"

    const/16 v14, 0x1c

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_STRING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 320
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "WRONG_OBJECT_TYPE"

    const/16 v14, 0x1d

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->WRONG_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 325
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "INVALID_CURRENCY"

    const/16 v14, 0x1e

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_CURRENCY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 330
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "ATTRIBUTE_UNIT_OVERRIDE_ONLY"

    const/16 v14, 0x1f

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_UNIT_OVERRIDE_ONLY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 335
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "UNEXPECTED_ATTRIBUTE_FOUND"

    const/16 v14, 0x20

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNEXPECTED_ATTRIBUTE_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 340
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "ITEM_OPTION_NOT_UNIQUE"

    const/16 v14, 0x21

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ITEM_OPTION_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 345
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "UNKNOWN_ITEM_OPTION_VAL"

    const/16 v14, 0x22

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNKNOWN_ITEM_OPTION_VAL:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 350
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "PRODUCT_SET_FORMS_CYCLIC_LINK"

    const/16 v14, 0x23

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->PRODUCT_SET_FORMS_CYCLIC_LINK:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 355
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "INT_COMPARISON_FAILED"

    const/16 v14, 0x24

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INT_COMPARISON_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 360
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "TOO_MANY_REFERENCES"

    const/16 v14, 0x25

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_REFERENCES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 365
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "TOO_MANY_OBJECTS_OF_TYPE"

    const/16 v14, 0x26

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_OBJECTS_OF_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 370
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "ITEM_OPTION_VAL_NOT_FOUND"

    const/16 v14, 0x27

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ITEM_OPTION_VAL_NOT_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 375
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "INVALID_UTF8"

    const/16 v14, 0x28

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_UTF8:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 380
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "VARIATION_ITEM_OPTIONS_MISMATCH"

    const/16 v14, 0x29

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VARIATION_ITEM_OPTIONS_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 385
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "VARIATION_ITEM_OPTION_VALUE_MISMATCH"

    const/16 v14, 0x2a

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VARIATION_ITEM_OPTION_VALUE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 390
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "FIXED_DISCOUNT_ONLY_FOR_PRICING_RULE"

    const/16 v14, 0x2b

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->FIXED_DISCOUNT_ONLY_FOR_PRICING_RULE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 395
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "REQUIRED_SHARED_ATTRIBUTE_MISMATCH"

    const/16 v14, 0x2c

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->REQUIRED_SHARED_ATTRIBUTE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 400
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const-string v13, "UNDEFINED_ERROR"

    const/16 v14, 0x2d

    const/16 v15, 0x64

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/InvalidCatalogObject$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNDEFINED_ERROR:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v0, 0x2e

    new-array v0, v0, [Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 163
    sget-object v13, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DO_NOT_USE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v13, v0, v1

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VERSION_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_TYPE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_VALUE_OVER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v4

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_DEFINITION_TOKEN_UNKNOWN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v5

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v6

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v7

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->V1_OBJECT_VALIDATION_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v8

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_TEMPORARY_OBJECT_TOKEN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v9

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->REQUIRED_ATTRIBUTE_MISSING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v10

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_OBJECT_REFERENCE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v11

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_VALUE_UNDER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    aput-object v1, v0, v12

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->NULL_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_DELETED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_IMMUTABLE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_OBJECT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_MEMBERSHIP:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ILLEGAL_UNIT_OVERRIDE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_ENABLED_AT_MULTIPLE_UNITS:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->FORBIDDEN_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_DISABLED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->GIFT_CARD_ONE_VARIATION:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNKNOWN_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_ATTRIBUTES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_STRING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->WRONG_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_CURRENCY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_UNIT_OVERRIDE_ONLY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNEXPECTED_ATTRIBUTE_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ITEM_OPTION_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNKNOWN_ITEM_OPTION_VAL:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->PRODUCT_SET_FORMS_CYCLIC_LINK:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INT_COMPARISON_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_REFERENCES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_OBJECTS_OF_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ITEM_OPTION_VAL_NOT_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_UTF8:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VARIATION_ITEM_OPTIONS_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VARIATION_ITEM_OPTION_VALUE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->FIXED_DISCOUNT_ONLY_FOR_PRICING_RULE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->REQUIRED_SHARED_ATTRIBUTE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNDEFINED_ERROR:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->$VALUES:[Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    .line 402
    new-instance v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason$ProtoAdapter_Reason;

    invoke-direct {v0}, Lsquareup/items/merchant/InvalidCatalogObject$Reason$ProtoAdapter_Reason;-><init>()V

    sput-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 406
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 407
    iput p3, p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/InvalidCatalogObject$Reason;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/16 v0, 0x64

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    const/4 p0, 0x0

    return-object p0

    .line 459
    :pswitch_0
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->REQUIRED_SHARED_ATTRIBUTE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 458
    :pswitch_1
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->FIXED_DISCOUNT_ONLY_FOR_PRICING_RULE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 457
    :pswitch_2
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VARIATION_ITEM_OPTION_VALUE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 456
    :pswitch_3
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VARIATION_ITEM_OPTIONS_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 455
    :pswitch_4
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_UTF8:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 454
    :pswitch_5
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ITEM_OPTION_VAL_NOT_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 453
    :pswitch_6
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_OBJECTS_OF_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 452
    :pswitch_7
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_REFERENCES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 451
    :pswitch_8
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INT_COMPARISON_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 450
    :pswitch_9
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->PRODUCT_SET_FORMS_CYCLIC_LINK:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 449
    :pswitch_a
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNKNOWN_ITEM_OPTION_VAL:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 448
    :pswitch_b
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ITEM_OPTION_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 447
    :pswitch_c
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNEXPECTED_ATTRIBUTE_FOUND:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 446
    :pswitch_d
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_UNIT_OVERRIDE_ONLY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 445
    :pswitch_e
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_CURRENCY:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 444
    :pswitch_f
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->WRONG_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 443
    :pswitch_10
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_STRING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 442
    :pswitch_11
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->TOO_MANY_ATTRIBUTES:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 441
    :pswitch_12
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNKNOWN_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 440
    :pswitch_13
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->GIFT_CARD_ONE_VARIATION:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 439
    :pswitch_14
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_DISABLED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 438
    :pswitch_15
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->FORBIDDEN_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 437
    :pswitch_16
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_ENABLED_AT_MULTIPLE_UNITS:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 436
    :pswitch_17
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ILLEGAL_UNIT_OVERRIDE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 435
    :pswitch_18
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_MEMBERSHIP:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 434
    :pswitch_19
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_OBJECT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 433
    :pswitch_1a
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 432
    :pswitch_1b
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_IMMUTABLE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 431
    :pswitch_1c
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_DELETED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 430
    :pswitch_1d
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_UNIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 429
    :pswitch_1e
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->INVALID_OBJECT_TYPE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 428
    :pswitch_1f
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->NULL_ATTRIBUTE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 427
    :pswitch_20
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_VALUE_UNDER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 426
    :pswitch_21
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->BROKEN_OBJECT_REFERENCE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 425
    :pswitch_22
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->REQUIRED_ATTRIBUTE_MISSING:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 424
    :pswitch_23
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DUPLICATE_TEMPORARY_OBJECT_TOKEN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 423
    :pswitch_24
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->V1_OBJECT_VALIDATION_FAILED:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 422
    :pswitch_25
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 421
    :pswitch_26
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->OBJECT_NONEXISTENT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 420
    :pswitch_27
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_DEFINITION_TOKEN_UNKNOWN:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 419
    :pswitch_28
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_VALUE_OVER_LIMIT:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 418
    :pswitch_29
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_NOT_UNIQUE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 417
    :pswitch_2a
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->ATTRIBUTE_TYPE_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 460
    :cond_0
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->UNDEFINED_ERROR:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 416
    :cond_1
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->VERSION_MISMATCH:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    .line 415
    :cond_2
    sget-object p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->DO_NOT_USE:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x26
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/InvalidCatalogObject$Reason;
    .locals 1

    .line 163
    const-class v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/InvalidCatalogObject$Reason;
    .locals 1

    .line 163
    sget-object v0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->$VALUES:[Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    invoke-virtual {v0}, [Lsquareup/items/merchant/InvalidCatalogObject$Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 467
    iget v0, p0, Lsquareup/items/merchant/InvalidCatalogObject$Reason;->value:I

    return v0
.end method
