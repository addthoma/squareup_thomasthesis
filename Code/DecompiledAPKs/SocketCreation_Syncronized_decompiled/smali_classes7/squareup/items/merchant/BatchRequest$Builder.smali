.class public final Lsquareup/items/merchant/BatchRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/BatchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/BatchRequest;",
        "Lsquareup/items/merchant/BatchRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public MerchantToken:Ljava/lang/String;

.field public request:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Request;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 109
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/BatchRequest$Builder;->request:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public MerchantToken(Ljava/lang/String;)Lsquareup/items/merchant/BatchRequest$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lsquareup/items/merchant/BatchRequest$Builder;->MerchantToken:Ljava/lang/String;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lsquareup/items/merchant/BatchRequest$Builder;->build()Lsquareup/items/merchant/BatchRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/BatchRequest;
    .locals 4

    .line 128
    new-instance v0, Lsquareup/items/merchant/BatchRequest;

    iget-object v1, p0, Lsquareup/items/merchant/BatchRequest$Builder;->request:Ljava/util/List;

    iget-object v2, p0, Lsquareup/items/merchant/BatchRequest$Builder;->MerchantToken:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/items/merchant/BatchRequest;-><init>(Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public request(Ljava/util/List;)Lsquareup/items/merchant/BatchRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Request;",
            ">;)",
            "Lsquareup/items/merchant/BatchRequest$Builder;"
        }
    .end annotation

    .line 113
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 114
    iput-object p1, p0, Lsquareup/items/merchant/BatchRequest$Builder;->request:Ljava/util/List;

    return-object p0
.end method
