.class final Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AttributeDefinition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/AttributeDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 580
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/AttributeDefinition;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 578
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/AttributeDefinition;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/AttributeDefinition;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 613
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$Builder;-><init>()V

    .line 614
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 615
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_9

    const/4 v4, 0x1

    if-eq v3, v4, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    const/4 v4, 0x4

    if-eq v3, v4, :cond_5

    const/4 v4, 0x6

    if-eq v3, v4, :cond_4

    const/16 v4, 0x8

    if-eq v3, v4, :cond_3

    const/16 v4, 0x1b

    if-eq v3, v4, :cond_2

    const/16 v4, 0x18

    if-eq v3, v4, :cond_1

    const/16 v4, 0x19

    if-eq v3, v4, :cond_0

    .line 648
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 633
    :cond_0
    :try_start_0
    sget-object v4, Lsquareup/items/merchant/AttributeDefinition$Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/AttributeDefinition$Scope;

    invoke-virtual {v0, v4}, Lsquareup/items/merchant/AttributeDefinition$Builder;->scope(Lsquareup/items/merchant/AttributeDefinition$Scope;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 635
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/items/merchant/AttributeDefinition$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 630
    :cond_1
    sget-object v3, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility(Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)Lsquareup/items/merchant/AttributeDefinition$Builder;

    goto :goto_0

    .line 641
    :cond_2
    :try_start_1
    sget-object v4, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    invoke-virtual {v0, v4}, Lsquareup/items/merchant/AttributeDefinition$Builder;->attribute_class(Lsquareup/items/merchant/AttributeDefinition$AttributeClass;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 643
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/items/merchant/AttributeDefinition$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 629
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/AttributeDefinition$Builder;->display_name(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;

    goto :goto_0

    .line 628
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/AttributeDefinition$Builder;->searchable(Ljava/lang/Boolean;)Lsquareup/items/merchant/AttributeDefinition$Builder;

    goto/16 :goto_0

    .line 627
    :cond_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/AttributeDefinition$Builder;->legacy_scope(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;

    goto/16 :goto_0

    .line 621
    :cond_6
    :try_start_2
    sget-object v4, Lsquareup/items/merchant/AttributeDefinition$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/AttributeDefinition$Type;

    invoke-virtual {v0, v4}, Lsquareup/items/merchant/AttributeDefinition$Builder;->type(Lsquareup/items/merchant/AttributeDefinition$Type;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 623
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/items/merchant/AttributeDefinition$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 618
    :cond_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/AttributeDefinition$Builder;->name(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;

    goto/16 :goto_0

    .line 617
    :cond_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/AttributeDefinition$Builder;->token(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;

    goto/16 :goto_0

    .line 652
    :cond_9
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/AttributeDefinition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 653
    invoke-virtual {v0}, Lsquareup/items/merchant/AttributeDefinition$Builder;->build()Lsquareup/items/merchant/AttributeDefinition;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 578
    check-cast p2, Lsquareup/items/merchant/AttributeDefinition;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/AttributeDefinition;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/AttributeDefinition;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 599
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 600
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 601
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 602
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 603
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 604
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 605
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 606
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 607
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 608
    invoke-virtual {p2}, Lsquareup/items/merchant/AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 578
    check-cast p1, Lsquareup/items/merchant/AttributeDefinition;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;->encodedSize(Lsquareup/items/merchant/AttributeDefinition;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/AttributeDefinition;)I
    .locals 4

    .line 585
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 586
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v3, 0x3

    .line 587
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    const/4 v3, 0x4

    .line 588
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/16 v3, 0x19

    .line 589
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 590
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    const/16 v3, 0x8

    .line 591
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    const/16 v3, 0x1b

    .line 592
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    const/16 v3, 0x18

    .line 593
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 594
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 578
    check-cast p1, Lsquareup/items/merchant/AttributeDefinition;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;->redact(Lsquareup/items/merchant/AttributeDefinition;)Lsquareup/items/merchant/AttributeDefinition;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/AttributeDefinition;)Lsquareup/items/merchant/AttributeDefinition;
    .locals 2

    .line 658
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition;->newBuilder()Lsquareup/items/merchant/AttributeDefinition$Builder;

    move-result-object p1

    .line 659
    iget-object v0, p1, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    iput-object v0, p1, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    .line 660
    :cond_0
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 661
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$Builder;->build()Lsquareup/items/merchant/AttributeDefinition;

    move-result-object p1

    return-object p1
.end method
