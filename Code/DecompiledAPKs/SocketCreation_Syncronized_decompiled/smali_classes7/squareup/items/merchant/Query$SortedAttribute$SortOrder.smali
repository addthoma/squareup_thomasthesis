.class public final enum Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;
.super Ljava/lang/Enum;
.source "Query.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$SortedAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SortOrder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$SortedAttribute$SortOrder$ProtoAdapter_SortOrder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ASCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

.field public static final enum DESCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 656
    new-instance v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "ASCENDING"

    invoke-direct {v0, v3, v1, v2}, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->ASCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    .line 658
    new-instance v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    const/4 v3, 0x2

    const-string v4, "DESCENDING"

    invoke-direct {v0, v4, v2, v3}, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->DESCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    new-array v0, v3, [Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    .line 655
    sget-object v3, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->ASCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    aput-object v3, v0, v1

    sget-object v1, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->DESCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    aput-object v1, v0, v2

    sput-object v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->$VALUES:[Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    .line 660
    new-instance v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder$ProtoAdapter_SortOrder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder$ProtoAdapter_SortOrder;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 664
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 665
    iput p3, p0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 674
    :cond_0
    sget-object p0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->DESCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-object p0

    .line 673
    :cond_1
    sget-object p0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->ASCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;
    .locals 1

    .line 655
    const-class v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;
    .locals 1

    .line 655
    sget-object v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->$VALUES:[Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    invoke-virtual {v0}, [Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 681
    iget v0, p0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->value:I

    return v0
.end method
