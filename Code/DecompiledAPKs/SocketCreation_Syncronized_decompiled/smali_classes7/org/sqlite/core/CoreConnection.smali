.class public abstract Lorg/sqlite/core/CoreConnection;
.super Ljava/lang/Object;
.source "CoreConnection.java"


# static fields
.field private static final RESOURCE_NAME_PREFIX:Ljava/lang/String; = ":resource:"

.field protected static final beginCommandMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/sqlite/SQLiteConfig$TransactionMode;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final pragmaSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected autoCommit:Z

.field private busyTimeout:I

.field public final dateClass:Lorg/sqlite/SQLiteConfig$DateClass;

.field public final dateFormat:Lorg/sqlite/date/FastDateFormat;

.field public final dateMultiplier:J

.field public final datePrecision:Lorg/sqlite/SQLiteConfig$DatePrecision;

.field public final dateStringFormat:Ljava/lang/String;

.field protected db:Lorg/sqlite/core/DB;

.field private fileName:Ljava/lang/String;

.field protected meta:Lorg/sqlite/core/CoreDatabaseMetaData;

.field protected final openModeFlags:I

.field protected transactionIsolation:I

.field protected transactionMode:Lorg/sqlite/SQLiteConfig$TransactionMode;

.field private final url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 41
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lorg/sqlite/SQLiteConfig$TransactionMode;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lorg/sqlite/core/CoreConnection;->beginCommandMap:Ljava/util/Map;

    .line 44
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    sput-object v0, Lorg/sqlite/core/CoreConnection;->pragmaSet:Ljava/util/Set;

    .line 46
    sget-object v0, Lorg/sqlite/core/CoreConnection;->beginCommandMap:Ljava/util/Map;

    sget-object v1, Lorg/sqlite/SQLiteConfig$TransactionMode;->DEFFERED:Lorg/sqlite/SQLiteConfig$TransactionMode;

    const-string v2, "begin;"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lorg/sqlite/core/CoreConnection;->beginCommandMap:Ljava/util/Map;

    sget-object v1, Lorg/sqlite/SQLiteConfig$TransactionMode;->IMMEDIATE:Lorg/sqlite/SQLiteConfig$TransactionMode;

    const-string v2, "begin immediate;"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lorg/sqlite/core/CoreConnection;->beginCommandMap:Ljava/util/Map;

    sget-object v1, Lorg/sqlite/SQLiteConfig$TransactionMode;->EXCLUSIVE:Lorg/sqlite/SQLiteConfig$TransactionMode;

    const-string v2, "begin exclusive;"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-static {}, Lorg/sqlite/SQLiteConfig$Pragma;->values()[Lorg/sqlite/SQLiteConfig$Pragma;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 51
    sget-object v4, Lorg/sqlite/core/CoreConnection;->pragmaSet:Ljava/util/Set;

    iget-object v3, v3, Lorg/sqlite/SQLiteConfig$Pragma;->pragmaName:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    .line 34
    iput-object v0, p0, Lorg/sqlite/core/CoreConnection;->meta:Lorg/sqlite/core/CoreDatabaseMetaData;

    const/4 v0, 0x1

    .line 35
    iput-boolean v0, p0, Lorg/sqlite/core/CoreConnection;->autoCommit:Z

    const/16 v0, 0x8

    .line 36
    iput v0, p0, Lorg/sqlite/core/CoreConnection;->transactionIsolation:I

    const/4 v0, 0x0

    .line 37
    iput v0, p0, Lorg/sqlite/core/CoreConnection;->busyTimeout:I

    .line 39
    sget-object v0, Lorg/sqlite/SQLiteConfig$TransactionMode;->DEFFERED:Lorg/sqlite/SQLiteConfig$TransactionMode;

    iput-object v0, p0, Lorg/sqlite/core/CoreConnection;->transactionMode:Lorg/sqlite/SQLiteConfig$TransactionMode;

    .line 64
    iput-object p1, p0, Lorg/sqlite/core/CoreConnection;->url:Ljava/lang/String;

    .line 65
    invoke-direct {p0, p2, p3}, Lorg/sqlite/core/CoreConnection;->extractPragmasFromFilename(Ljava/lang/String;Ljava/util/Properties;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    .line 67
    new-instance p1, Lorg/sqlite/SQLiteConfig;

    invoke-direct {p1, p3}, Lorg/sqlite/SQLiteConfig;-><init>(Ljava/util/Properties;)V

    .line 68
    iget-object p3, p1, Lorg/sqlite/SQLiteConfig;->dateClass:Lorg/sqlite/SQLiteConfig$DateClass;

    iput-object p3, p0, Lorg/sqlite/core/CoreConnection;->dateClass:Lorg/sqlite/SQLiteConfig$DateClass;

    .line 69
    iget-wide v0, p1, Lorg/sqlite/SQLiteConfig;->dateMultiplier:J

    iput-wide v0, p0, Lorg/sqlite/core/CoreConnection;->dateMultiplier:J

    .line 70
    iget-object p3, p1, Lorg/sqlite/SQLiteConfig;->dateStringFormat:Ljava/lang/String;

    invoke-static {p3}, Lorg/sqlite/date/FastDateFormat;->getInstance(Ljava/lang/String;)Lorg/sqlite/date/FastDateFormat;

    move-result-object p3

    iput-object p3, p0, Lorg/sqlite/core/CoreConnection;->dateFormat:Lorg/sqlite/date/FastDateFormat;

    .line 71
    iget-object p3, p1, Lorg/sqlite/SQLiteConfig;->dateStringFormat:Ljava/lang/String;

    iput-object p3, p0, Lorg/sqlite/core/CoreConnection;->dateStringFormat:Ljava/lang/String;

    .line 72
    iget-object p3, p1, Lorg/sqlite/SQLiteConfig;->datePrecision:Lorg/sqlite/SQLiteConfig$DatePrecision;

    iput-object p3, p0, Lorg/sqlite/core/CoreConnection;->datePrecision:Lorg/sqlite/SQLiteConfig$DatePrecision;

    .line 73
    invoke-virtual {p1}, Lorg/sqlite/SQLiteConfig;->getTransactionMode()Lorg/sqlite/SQLiteConfig$TransactionMode;

    move-result-object p3

    iput-object p3, p0, Lorg/sqlite/core/CoreConnection;->transactionMode:Lorg/sqlite/SQLiteConfig$TransactionMode;

    .line 74
    invoke-virtual {p1}, Lorg/sqlite/SQLiteConfig;->getOpenModeFlags()I

    move-result p3

    iput p3, p0, Lorg/sqlite/core/CoreConnection;->openModeFlags:I

    .line 76
    iget p3, p0, Lorg/sqlite/core/CoreConnection;->openModeFlags:I

    iget v0, p1, Lorg/sqlite/SQLiteConfig;->busyTimeout:I

    invoke-direct {p0, p3, v0}, Lorg/sqlite/core/CoreConnection;->open(II)V

    const-string p3, "file:"

    .line 78
    invoke-virtual {p2, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_0

    const-string p3, "cache="

    invoke-virtual {p2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 80
    iget-object p2, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    invoke-virtual {p1}, Lorg/sqlite/SQLiteConfig;->isEnabledSharedCache()Z

    move-result p3

    invoke-virtual {p2, p3}, Lorg/sqlite/core/DB;->shared_cache(Z)I

    .line 82
    :cond_0
    iget-object p2, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    invoke-virtual {p1}, Lorg/sqlite/SQLiteConfig;->isEnabledLoadExtension()Z

    move-result p3

    invoke-virtual {p2, p3}, Lorg/sqlite/core/DB;->enable_load_extension(Z)I

    .line 85
    move-object p2, p0

    check-cast p2, Ljava/sql/Connection;

    invoke-virtual {p1, p2}, Lorg/sqlite/SQLiteConfig;->apply(Ljava/sql/Connection;)V

    return-void
.end method

.method private extractPragmasFromFilename(Ljava/lang/String;Ljava/util/Properties;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    const/16 v0, 0x3f

    .line 100
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    return-object p1

    .line 106
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    .line 107
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    add-int/2addr v1, v4

    .line 110
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "&"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 111
    :goto_0
    array-length v6, p1

    if-ge v1, v6, :cond_7

    .line 113
    array-length v6, p1

    sub-int/2addr v6, v4

    sub-int/2addr v6, v1

    aget-object v6, p1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 115
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    goto :goto_2

    :cond_1
    const-string v7, "="

    .line 120
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 121
    aget-object v8, v7, v3

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 122
    sget-object v9, Lorg/sqlite/core/CoreConnection;->pragmaSet:Ljava/util/Set;

    invoke-interface {v9, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 123
    array-length v6, v7

    if-eq v6, v4, :cond_3

    .line 126
    aget-object v6, v7, v4

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 127
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_6

    .line 128
    invoke-virtual {p2, v8}, Ljava/util/Properties;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_2

    .line 138
    :cond_2
    invoke-virtual {p2, v8, v6}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_2

    .line 124
    :cond_3
    new-instance p1, Ljava/sql/SQLException;

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    aput-object v8, p2, v3

    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->url:Ljava/lang/String;

    aput-object v0, p2, v4

    const-string v0, "Please specify a value for PRAGMA %s in URL %s"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    if-nez v5, :cond_5

    const/16 v7, 0x3f

    goto :goto_1

    :cond_5
    const/16 v7, 0x26

    .line 143
    :goto_1
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    :cond_6
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private extractResource(Ljava/net/URL;)Ljava/io/File;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 231
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    .line 236
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {p1}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "java.io.tmpdir"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 241
    invoke-virtual {p1}, Ljava/net/URL;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "sqlite-jdbc-tmp-%d.db"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 242
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 245
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URLConnection;->getLastModified()J

    move-result-wide v0

    .line 246
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v6, v0, v4

    if-gez v6, :cond_1

    return-object v2

    .line 252
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 254
    :cond_2
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "failed to remove existing DB file: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 269
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 270
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object p1

    .line 273
    :goto_1
    :try_start_1
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    .line 274
    invoke-virtual {v1, v0, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 279
    :cond_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 280
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v2

    :catchall_0
    move-exception v0

    .line 279
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 280
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private open(II)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    const-string v1, ":memory:"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    const-string v1, "mode=memory"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 162
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    const-string v1, ":resource:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 167
    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-nez v1, :cond_0

    .line 170
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 173
    new-instance p2, Ljava/sql/SQLException;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v0, v1, v3

    aput-object p1, v1, v2

    const-string p1, "resource %s not found: %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 178
    :cond_0
    :goto_0
    :try_start_1
    invoke-direct {p0, v1}, Lorg/sqlite/core/CoreConnection;->extractResource(Ljava/net/URL;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception p1

    .line 181
    new-instance p2, Ljava/sql/SQLException;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v0, v1, v3

    aput-object p1, v1, v2

    const-string p1, "failed to load %s: %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 185
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 187
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    move-object p1, v1

    :goto_1
    if-eqz v1, :cond_2

    .line 188
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p2

    if-nez p2, :cond_2

    .line 190
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    move-object v5, v1

    move-object v1, p1

    move-object p1, v5

    goto :goto_1

    .line 192
    :cond_2
    new-instance p2, Ljava/sql/SQLException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "path to \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\': \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\' does not exist"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 199
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 200
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 205
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    goto :goto_2

    :catch_2
    move-exception p1

    .line 203
    new-instance p2, Ljava/sql/SQLException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "opening db: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\': "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 211
    :cond_5
    :goto_2
    :try_start_3
    invoke-static {}, Lorg/sqlite/core/NativeDB;->load()Z

    .line 212
    new-instance v0, Lorg/sqlite/core/NativeDB;

    invoke-direct {v0}, Lorg/sqlite/core/NativeDB;-><init>()V

    iput-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 220
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    move-object v1, p0

    check-cast v1, Lorg/sqlite/SQLiteConnection;

    iget-object v2, p0, Lorg/sqlite/core/CoreConnection;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lorg/sqlite/core/DB;->open(Lorg/sqlite/SQLiteConnection;Ljava/lang/String;I)V

    .line 221
    invoke-virtual {p0, p2}, Lorg/sqlite/core/CoreConnection;->setBusyTimeout(I)V

    return-void

    :catch_3
    move-exception p1

    .line 215
    new-instance p2, Ljava/sql/SQLException;

    const-string v0, "Error opening connection"

    invoke-direct {p2, v0}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2, p1}, Ljava/sql/SQLException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 217
    throw p2
.end method


# virtual methods
.method protected checkCursor(III)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    const/16 v0, 0x3eb

    if-ne p1, v0, :cond_2

    const/16 p1, 0x3ef

    if-ne p2, p1, :cond_1

    const/4 p1, 0x2

    if-ne p3, p1, :cond_0

    return-void

    .line 357
    :cond_0
    new-instance p1, Ljava/sql/SQLException;

    const-string p2, "SQLite only supports closing cursors at commit"

    invoke-direct {p1, p2}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 355
    :cond_1
    new-instance p1, Ljava/sql/SQLException;

    const-string p2, "SQLite only supports CONCUR_READ_ONLY cursors"

    invoke-direct {p1, p2}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 353
    :cond_2
    new-instance p1, Ljava/sql/SQLException;

    const-string p2, "SQLite only supports TYPE_FORWARD_ONLY cursors"

    invoke-direct {p1, p2}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected checkOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 335
    invoke-virtual {p0}, Lorg/sqlite/core/CoreConnection;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 336
    :cond_0
    new-instance v0, Ljava/sql/SQLException;

    const-string v1, "database connection closed"

    invoke-direct {v0, v1}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 385
    invoke-virtual {p0}, Lorg/sqlite/core/CoreConnection;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 387
    :cond_0
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->meta:Lorg/sqlite/core/CoreDatabaseMetaData;

    if-eqz v0, :cond_1

    .line 388
    invoke-virtual {v0}, Lorg/sqlite/core/CoreDatabaseMetaData;->close()V

    .line 390
    :cond_1
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    invoke-virtual {v0}, Lorg/sqlite/core/DB;->close()V

    const/4 v0, 0x0

    .line 391
    iput-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    return-void
.end method

.method public db()Lorg/sqlite/core/DB;
    .locals 1

    .line 327
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    return-object v0
.end method

.method public finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 378
    invoke-virtual {p0}, Lorg/sqlite/core/CoreConnection;->close()V

    return-void
.end method

.method public getBusyTimeout()I
    .locals 1

    .line 290
    iget v0, p0, Lorg/sqlite/core/CoreConnection;->busyTimeout:I

    return v0
.end method

.method protected isClosed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 370
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public libversion()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 318
    invoke-virtual {p0}, Lorg/sqlite/core/CoreConnection;->checkOpen()V

    .line 320
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    invoke-virtual {v0}, Lorg/sqlite/core/DB;->libversion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setBusyTimeout(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 301
    iput p1, p0, Lorg/sqlite/core/CoreConnection;->busyTimeout:I

    .line 302
    iget-object p1, p0, Lorg/sqlite/core/CoreConnection;->db:Lorg/sqlite/core/DB;

    iget v0, p0, Lorg/sqlite/core/CoreConnection;->busyTimeout:I

    invoke-virtual {p1, v0}, Lorg/sqlite/core/DB;->busy_timeout(I)V

    return-void
.end method

.method protected setTransactionMode(Lorg/sqlite/SQLiteConfig$TransactionMode;)V
    .locals 0

    .line 366
    iput-object p1, p0, Lorg/sqlite/core/CoreConnection;->transactionMode:Lorg/sqlite/SQLiteConfig$TransactionMode;

    return-void
.end method

.method public url()Ljava/lang/String;
    .locals 1

    .line 309
    iget-object v0, p0, Lorg/sqlite/core/CoreConnection;->url:Ljava/lang/String;

    return-object v0
.end method
