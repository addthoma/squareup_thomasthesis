.class final Lcom/squareup/time/RealCurrentTime$RealClock;
.super Lorg/threeten/bp/Clock;
.source "RealCurrentTime.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/time/RealCurrentTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RealClock"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0004H\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/time/RealCurrentTime$RealClock;",
        "Lorg/threeten/bp/Clock;",
        "currentTimeZone",
        "Lkotlin/Function0;",
        "Lorg/threeten/bp/ZoneId;",
        "(Lkotlin/jvm/functions/Function0;)V",
        "getZone",
        "instant",
        "Lorg/threeten/bp/Instant;",
        "withZone",
        "zone",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTimeZone:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lorg/threeten/bp/ZoneId;",
            ">;)V"
        }
    .end annotation

    const-string v0, "currentTimeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lorg/threeten/bp/Clock;-><init>()V

    iput-object p1, p0, Lcom/squareup/time/RealCurrentTime$RealClock;->currentTimeZone:Lkotlin/jvm/functions/Function0;

    return-void
.end method


# virtual methods
.method public getZone()Lorg/threeten/bp/ZoneId;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/time/RealCurrentTime$RealClock;->currentTimeZone:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/ZoneId;

    return-object v0
.end method

.method public instant()Lorg/threeten/bp/Instant;
    .locals 2

    .line 30
    invoke-static {}, Lorg/threeten/bp/Instant;->now()Lorg/threeten/bp/Instant;

    move-result-object v0

    const-string v1, "Instant.now()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public withZone(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/Clock;
    .locals 2

    const-string/jumbo v0, "zone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/time/RealCurrentTime$RealClock;

    new-instance v1, Lcom/squareup/time/RealCurrentTime$RealClock$withZone$1;

    invoke-direct {v1, p1}, Lcom/squareup/time/RealCurrentTime$RealClock$withZone$1;-><init>(Lorg/threeten/bp/ZoneId;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {v0, v1}, Lcom/squareup/time/RealCurrentTime$RealClock;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lorg/threeten/bp/Clock;

    return-object v0
.end method
