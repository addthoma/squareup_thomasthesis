.class public final Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
.super Lcom/squareup/wire/AndroidMessage;
.source "EmployeesEntity.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/EmployeesEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PasscodeOnlyCredential"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;,
        Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HASHED_PASSCODE:Ljava/lang/String; = ""

.field public static final DEFAULT_ITERATIONS:Ljava/lang/Integer;

.field public static final DEFAULT_SALT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final hashed_passcode:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final iterations:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final salt:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 410
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$ProtoAdapter_PasscodeOnlyCredential;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 412
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 420
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->DEFAULT_ITERATIONS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .line 445
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 450
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 451
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    .line 452
    iput-object p2, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    .line 453
    iput-object p3, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 516
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 469
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 470
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    .line 471
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    .line 472
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    .line 473
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    .line 474
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 479
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 481
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 482
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 483
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 484
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 485
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
    .locals 2

    .line 458
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;-><init>()V

    .line 459
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode:Ljava/lang/String;

    .line 460
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt:Ljava/lang/String;

    .line 461
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations:Ljava/lang/Integer;

    .line 462
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 409
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
    .locals 2

    .line 509
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    .line 510
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    .line 511
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 512
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 409
    check-cast p1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->overlay(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
    .locals 2

    .line 502
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->requireBuilder(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->DEFAULT_ITERATIONS:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 503
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 409
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 493
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", hashed_passcode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", salt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", iterations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PasscodeOnlyCredential{"

    .line 496
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
