.class public final Lcom/squareup/server/account/protos/TaxId$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TaxId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/TaxId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/TaxId;",
        "Lcom/squareup/server/account/protos/TaxId$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 144
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 145
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/TaxId;
    .locals 5

    .line 166
    new-instance v0, Lcom/squareup/server/account/protos/TaxId;

    iget-object v1, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->types:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/TaxId;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/TaxId$Builder;->build()Lcom/squareup/server/account/protos/TaxId;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/server/account/protos/TaxId$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/squareup/server/account/protos/TaxId$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->number:Ljava/lang/String;

    return-object p0
.end method

.method public types(Ljava/util/List;)Lcom/squareup/server/account/protos/TaxId$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/TaxId$Builder;"
        }
    .end annotation

    .line 149
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/server/account/protos/TaxId$Builder;->types:Ljava/util/List;

    return-object p0
.end method
