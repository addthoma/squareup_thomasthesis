.class public final Lcom/squareup/server/account/protos/GiftCards;
.super Lcom/squareup/wire/AndroidMessage;
.source "GiftCards.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/GiftCards$ProtoAdapter_GiftCards;,
        Lcom/squareup/server/account/protos/GiftCards$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/GiftCards;",
        "Lcom/squareup/server/account/protos/GiftCards$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/GiftCards;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/GiftCards;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISABLE_GIFT_CARD_CREATION:Ljava/lang/Boolean;

.field public static final DEFAULT_REFUND_TO_ANY_GIFT_CARD:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final bins:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final disable_gift_card_creation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final refund_to_any_gift_card:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final track1_data:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/server/account/protos/GiftCards$ProtoAdapter_GiftCards;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/GiftCards$ProtoAdapter_GiftCards;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/GiftCards;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/GiftCards;->DEFAULT_REFUND_TO_ANY_GIFT_CARD:Ljava/lang/Boolean;

    .line 38
    sput-object v0, Lcom/squareup/server/account/protos/GiftCards;->DEFAULT_DISABLE_GIFT_CARD_CREATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 82
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/GiftCards;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 88
    sget-object v0, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p5, "bins"

    .line 89
    invoke-static {p5, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    const-string/jumbo p1, "track1_data"

    .line 90
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    .line 91
    iput-object p3, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    .line 92
    iput-object p4, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards;->newBuilder()Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 109
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/GiftCards;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 110
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/GiftCards;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/GiftCards;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    .line 112
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    .line 113
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 122
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 127
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/GiftCards$Builder;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/server/account/protos/GiftCards$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/GiftCards$Builder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/GiftCards$Builder;->bins:Ljava/util/List;

    .line 99
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/GiftCards$Builder;->track1_data:Ljava/util/List;

    .line 100
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/GiftCards$Builder;->refund_to_any_gift_card:Ljava/lang/Boolean;

    .line 101
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/GiftCards$Builder;->disable_gift_card_creation:Ljava/lang/Boolean;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards;->newBuilder()Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/GiftCards;
    .locals 2

    .line 153
    iget-object v0, p1, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/GiftCards;->requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->bins(Ljava/util/List;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v1

    .line 154
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/GiftCards;->requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->track1_data(Ljava/util/List;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v1

    .line 155
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/GiftCards;->requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->refund_to_any_gift_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v1

    .line 156
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/GiftCards;->requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->disable_gift_card_creation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 157
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->build()Lcom/squareup/server/account/protos/GiftCards;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/server/account/protos/GiftCards;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/GiftCards;->overlay(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/GiftCards;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/GiftCards;
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/GiftCards;->requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/GiftCards;->DEFAULT_REFUND_TO_ANY_GIFT_CARD:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->refund_to_any_gift_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v1

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/GiftCards;->requireBuilder(Lcom/squareup/server/account/protos/GiftCards$Builder;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/GiftCards;->DEFAULT_DISABLE_GIFT_CARD_CREATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->disable_gift_card_creation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/GiftCards$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/GiftCards$Builder;->build()Lcom/squareup/server/account/protos/GiftCards;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/GiftCards;->populateDefaults()Lcom/squareup/server/account/protos/GiftCards;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", bins="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->bins:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", track1_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->track1_data:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", refund_to_any_gift_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->refund_to_any_gift_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", disable_gift_card_creation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/GiftCards;->disable_gift_card_creation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GiftCards{"

    .line 139
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
