.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Employee"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$ProtoAdapter_Employee;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TIME_TRACKING_DEVICE_LEVEL:Ljava/lang/Boolean;

.field public static final DEFAULT_UPDATED_GUEST_ACCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_REPORTS_GRANULAR_PERMISSIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SETTINGS_GRANULAR_PERMISSIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final time_tracking_device_level:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final updated_guest_access:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final use_reports_granular_permissions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final use_settings_granular_permissions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11767
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$ProtoAdapter_Employee;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$ProtoAdapter_Employee;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11769
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 11773
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_UPDATED_GUEST_ACCESS:Ljava/lang/Boolean;

    .line 11775
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_TIME_TRACKING_DEVICE_LEVEL:Ljava/lang/Boolean;

    .line 11777
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_USE_REPORTS_GRANULAR_PERMISSIONS:Ljava/lang/Boolean;

    .line 11779
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_USE_SETTINGS_GRANULAR_PERMISSIONS:Ljava/lang/Boolean;

    .line 11781
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7

    .line 11826
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 11834
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 11835
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    .line 11836
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    .line 11837
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    .line 11838
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    .line 11839
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 11916
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 11857
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 11858
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    .line 11859
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    .line 11860
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    .line 11861
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    .line 11862
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    .line 11863
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    .line 11864
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 11869
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_5

    .line 11871
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 11872
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11873
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11874
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11875
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11876
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 11877
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;
    .locals 2

    .line 11844
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;-><init>()V

    .line 11845
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->updated_guest_access:Ljava/lang/Boolean;

    .line 11846
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->time_tracking_device_level:Ljava/lang/Boolean;

    .line 11847
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_reports_granular_permissions:Ljava/lang/Boolean;

    .line 11848
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_settings_granular_permissions:Ljava/lang/Boolean;

    .line 11849
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    .line 11850
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 11766
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
    .locals 2

    .line 11907
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->updated_guest_access(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11908
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->time_tracking_device_level(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11909
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_reports_granular_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11910
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_settings_granular_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11911
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_view_detailed_sales_reports_permission(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    move-object p1, p0

    goto :goto_0

    .line 11912
    :cond_5
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 11766
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
    .locals 2

    .line 11896
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_UPDATED_GUEST_ACCESS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->updated_guest_access(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11897
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_TIME_TRACKING_DEVICE_LEVEL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->time_tracking_device_level(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11898
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_USE_REPORTS_GRANULAR_PERMISSIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_reports_granular_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11899
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_USE_SETTINGS_GRANULAR_PERMISSIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_settings_granular_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    .line 11900
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->DEFAULT_USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->use_view_detailed_sales_reports_permission(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    move-object v0, p0

    goto :goto_0

    .line 11901
    :cond_5
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 11766
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 11884
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11885
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", updated_guest_access="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->updated_guest_access:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11886
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", time_tracking_device_level="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->time_tracking_device_level:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11887
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_reports_granular_permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_reports_granular_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11888
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", use_settings_granular_permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_settings_granular_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11889
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", use_view_detailed_sales_reports_permission="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->use_view_detailed_sales_reports_permission:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Employee{"

    .line 11890
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
