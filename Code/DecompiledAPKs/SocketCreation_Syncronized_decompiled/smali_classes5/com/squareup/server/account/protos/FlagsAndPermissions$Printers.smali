.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Printers"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$ProtoAdapter_Printers;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_PRINT_COMPACT_TICKETS:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_EPSON_PRINTERS:Ljava/lang/Boolean;

.field public static final DEFAULT_EPSON_DEBUGGING:Ljava/lang/Boolean;

.field public static final DEFAULT_RETAIN_PRINTER_CONNECTION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final can_print_compact_tickets:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final enable_epson_printers:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final epson_debugging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final retain_printer_connection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19015
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$ProtoAdapter_Printers;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$ProtoAdapter_Printers;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 19017
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 19021
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_ENABLE_EPSON_PRINTERS:Ljava/lang/Boolean;

    .line 19023
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_EPSON_DEBUGGING:Ljava/lang/Boolean;

    .line 19025
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_CAN_PRINT_COMPACT_TICKETS:Ljava/lang/Boolean;

    .line 19027
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_RETAIN_PRINTER_CONNECTION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 6

    .line 19059
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 19065
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 19066
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    .line 19067
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    .line 19068
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    .line 19069
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 19140
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 19086
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 19087
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    .line 19088
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    .line 19089
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    .line 19090
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    .line 19091
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    .line 19092
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 19097
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 19099
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 19100
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 19101
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 19102
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 19103
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 19104
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    .locals 2

    .line 19074
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;-><init>()V

    .line 19075
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->enable_epson_printers:Ljava/lang/Boolean;

    .line 19076
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->epson_debugging:Ljava/lang/Boolean;

    .line 19077
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->can_print_compact_tickets:Ljava/lang/Boolean;

    .line 19078
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->retain_printer_connection:Ljava/lang/Boolean;

    .line 19079
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19014
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
    .locals 2

    .line 19132
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->enable_epson_printers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    .line 19133
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->epson_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    .line 19134
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->can_print_compact_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    .line 19135
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->retain_printer_connection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 19136
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 19014
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
    .locals 2

    .line 19122
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_ENABLE_EPSON_PRINTERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->enable_epson_printers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    .line 19123
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_EPSON_DEBUGGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->epson_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    .line 19124
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_CAN_PRINT_COMPACT_TICKETS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->can_print_compact_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    .line 19125
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->DEFAULT_RETAIN_PRINTER_CONNECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->retain_printer_connection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object v0, p0

    goto :goto_0

    .line 19126
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 19014
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 19111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19112
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", enable_epson_printers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->enable_epson_printers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19113
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", epson_debugging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->epson_debugging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19114
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_print_compact_tickets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->can_print_compact_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19115
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", retain_printer_connection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->retain_printer_connection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Printers{"

    .line 19116
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
