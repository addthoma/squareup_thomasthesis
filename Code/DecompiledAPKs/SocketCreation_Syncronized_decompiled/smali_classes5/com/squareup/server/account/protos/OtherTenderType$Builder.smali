.class public final Lcom/squareup/server/account/protos/OtherTenderType$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OtherTenderType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/OtherTenderType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        "Lcom/squareup/server/account/protos/OtherTenderType$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accepts_tips:Ljava/lang/Boolean;

.field public tender_name:Ljava/lang/String;

.field public tender_note_prompt:Ljava/lang/String;

.field public tender_opens_cash_drawer:Ljava/lang/Boolean;

.field public tender_type:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 189
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public accepts_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->accepts_tips:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 8

    .line 222
    new-instance v7, Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_type:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_note_prompt:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->accepts_tips:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/account/protos/OtherTenderType;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->build()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v0

    return-object v0
.end method

.method public tender_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_name:Ljava/lang/String;

    return-object p0
.end method

.method public tender_note_prompt(Ljava/lang/String;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_note_prompt:Ljava/lang/String;

    return-object p0
.end method

.method public tender_opens_cash_drawer(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tender_type(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_type:Ljava/lang/Integer;

    return-object p0
.end method
