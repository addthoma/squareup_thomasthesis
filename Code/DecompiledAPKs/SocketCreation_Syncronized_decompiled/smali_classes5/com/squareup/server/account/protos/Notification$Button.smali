.class public final Lcom/squareup/server/account/protos/Notification$Button;
.super Lcom/squareup/wire/AndroidMessage;
.source "Notification.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Button"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Notification$Button$ProtoAdapter_Button;,
        Lcom/squareup/server/account/protos/Notification$Button$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        "Lcom/squareup/server/account/protos/Notification$Button$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 248
    new-instance v0, Lcom/squareup/server/account/protos/Notification$Button$ProtoAdapter_Button;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Notification$Button$ProtoAdapter_Button;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 250
    sget-object v0, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Notification$Button;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 273
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/Notification$Button;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 277
    sget-object v0, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 278
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    .line 279
    iput-object p2, p0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Notification$Button$Builder;)Lcom/squareup/server/account/protos/Notification$Button$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button;->newBuilder()Lcom/squareup/server/account/protos/Notification$Button$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 294
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Notification$Button;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 295
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Notification$Button;

    .line 296
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Notification$Button;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    .line 297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    .line 298
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 303
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 305
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 306
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 307
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 308
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Notification$Button$Builder;
    .locals 2

    .line 284
    new-instance v0, Lcom/squareup/server/account/protos/Notification$Button$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Notification$Button$Builder;-><init>()V

    .line 285
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Button$Builder;->url:Ljava/lang/String;

    .line 286
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Button$Builder;->label:Ljava/lang/String;

    .line 287
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Button$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 247
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button;->newBuilder()Lcom/squareup/server/account/protos/Notification$Button$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Notification$Button;)Lcom/squareup/server/account/protos/Notification$Button;
    .locals 2

    .line 330
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification$Button;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Button$Builder;)Lcom/squareup/server/account/protos/Notification$Button$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Button$Builder;->url(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Button$Builder;

    move-result-object v1

    .line 331
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification$Button;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Button$Builder;)Lcom/squareup/server/account/protos/Notification$Button$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Notification$Button$Builder;->label(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Button$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 332
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Notification$Button$Builder;->build()Lcom/squareup/server/account/protos/Notification$Button;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 247
    check-cast p1, Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Notification$Button;->overlay(Lcom/squareup/server/account/protos/Notification$Button;)Lcom/squareup/server/account/protos/Notification$Button;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Notification$Button;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 247
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button;->populateDefaults()Lcom/squareup/server/account/protos/Notification$Button;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Button{"

    .line 318
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
