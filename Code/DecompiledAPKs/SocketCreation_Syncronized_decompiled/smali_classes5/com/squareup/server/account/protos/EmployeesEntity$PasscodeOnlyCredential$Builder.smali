.class public final Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmployeesEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;",
        "Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public hashed_passcode:Ljava/lang/String;

.field public iterations:Ljava/lang/Integer;

.field public salt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 526
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;
    .locals 5

    .line 546
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 519
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object v0

    return-object v0
.end method

.method public hashed_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
    .locals 0

    .line 530
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode:Ljava/lang/String;

    return-object p0
.end method

.method public iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations:Ljava/lang/Integer;

    return-object p0
.end method

.method public salt(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;
    .locals 0

    .line 535
    iput-object p1, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt:Ljava/lang/String;

    return-object p0
.end method
