.class final Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RegEx"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7302
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7413
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;-><init>()V

    .line 7414
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 7415
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/16 v4, 0xa5

    if-eq v3, v4, :cond_1

    const/16 v4, 0xa6

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    packed-switch v3, :pswitch_data_2

    packed-switch v3, :pswitch_data_3

    packed-switch v3, :pswitch_data_4

    sparse-switch v3, :sswitch_data_0

    packed-switch v3, :pswitch_data_5

    packed-switch v3, :pswitch_data_6

    packed-switch v3, :pswitch_data_7

    .line 7466
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7463
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_high_contrast_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto :goto_0

    .line 7462
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto :goto_0

    .line 7461
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enable_verbose_login_response_cache_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto :goto_0

    .line 7460
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->delete_session_token_plaintext(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto :goto_0

    .line 7459
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enhanced_transaction_search_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto :goto_0

    .line 7458
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_accessibility_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto :goto_0

    .line 7457
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_print_single_ticket_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7456
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_item_suggestions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7441
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_jail_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7440
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->resilient_bus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7439
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_predefined_tickets_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7438
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->separated_printouts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7464
    :sswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->checkout_applet_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7455
    :sswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_order_entry_screen_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7454
    :sswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_hide_giftcards_rewards_if_no_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7453
    :sswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->show_fee_breakdown_table(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7452
    :sswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_create_new_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7451
    :sswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->receipts_print_disposition(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7450
    :sswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->buyer_checkout_display_transaction_type(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7449
    :sswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_v2_only_use_ip_location_with_no_wps(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7448
    :sswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_6_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7447
    :sswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_3_hour_check_interval(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7446
    :sswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_x2_use_24_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7445
    :sswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_integration_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7444
    :sswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->wait_for_bran_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7437
    :sswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7436
    :sswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->bundle_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7435
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7434
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_device_settings_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7433
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->intercept_magswipe_events_during_printing_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7432
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->restart_app_after_crash(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7431
    :pswitch_10
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->printer_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7430
    :pswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_feature_tour_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7429
    :pswitch_12
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->print_itemized_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7428
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_ledger_and_diagnostics_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7427
    :pswitch_14
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7426
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7425
    :pswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->dip_tap_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7424
    :pswitch_17
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_persistent_bundle_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7423
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7422
    :pswitch_19
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_clock_skew_lockout_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7421
    :pswitch_1a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->loyalty_checkout_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7420
    :pswitch_1b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_x2_wifi_events(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7419
    :pswitch_1c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->ignore_pii_accessibility_scrubber_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7418
    :pswitch_1d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_api_url_list_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7417
    :pswitch_1e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7443
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_close_remote_cash_drawers_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7442
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    goto/16 :goto_0

    .line 7470
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7471
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xc
        :pswitch_18
        :pswitch_17
        :pswitch_16
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x10
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x15
        :pswitch_11
        :pswitch_10
        :pswitch_f
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1a
        :pswitch_e
        :pswitch_d
        :pswitch_c
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x90 -> :sswitch_e
        0x9b -> :sswitch_d
        0xa8 -> :sswitch_c
        0xa9 -> :sswitch_b
        0xaa -> :sswitch_a
        0xab -> :sswitch_9
        0xac -> :sswitch_8
        0xad -> :sswitch_7
        0xae -> :sswitch_6
        0xaf -> :sswitch_5
        0xb0 -> :sswitch_4
        0xb1 -> :sswitch_3
        0xb2 -> :sswitch_2
        0xb5 -> :sswitch_1
        0xc3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_5
    .packed-switch 0x9d
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0xb7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0xbc
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7300
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7360
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    const/16 v2, 0x90

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7361
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7362
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7363
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7364
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7365
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7366
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7367
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7368
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7369
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7370
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7371
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7372
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7373
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7374
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7375
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7376
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7377
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7378
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7379
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7380
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    const/16 v2, 0x9b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7381
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    const/16 v2, 0x9d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7382
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    const/16 v2, 0x9e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7383
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    const/16 v2, 0x9f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7384
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    const/16 v2, 0xa0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7385
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    const/16 v2, 0xa5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7386
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    const/16 v2, 0xa6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7387
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    const/16 v2, 0xa8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7388
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    const/16 v2, 0xa9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7389
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    const/16 v2, 0xaa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7390
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    const/16 v2, 0xab

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7391
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    const/16 v2, 0xac

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7392
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    const/16 v2, 0xad

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7393
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    const/16 v2, 0xae

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7394
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    const/16 v2, 0xaf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7395
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    const/16 v2, 0xb0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7396
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    const/16 v2, 0xb1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7397
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    const/16 v2, 0xb2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7398
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    const/16 v2, 0xb5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7399
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    const/16 v2, 0xb7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7400
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    const/16 v2, 0xb8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7401
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    const/16 v2, 0xb9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    const/16 v2, 0xba

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7403
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    const/16 v2, 0xbc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7404
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    const/16 v2, 0xbd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7405
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    const/16 v2, 0xbe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7406
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    const/16 v2, 0xbf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7407
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    const/16 v2, 0xc3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7408
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7300
    check-cast p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)I
    .locals 4

    .line 7307
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    const/16 v2, 0x90

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    const/4 v3, 0x1

    .line 7308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 7309
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 7310
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 7311
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 7312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 7313
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    const/16 v3, 0xc

    .line 7314
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    const/16 v3, 0xd

    .line 7315
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    const/16 v3, 0xe

    .line 7316
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    const/16 v3, 0x10

    .line 7317
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    const/16 v3, 0x11

    .line 7318
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    const/16 v3, 0x12

    .line 7319
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    const/16 v3, 0x13

    .line 7320
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    const/16 v3, 0x15

    .line 7321
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    const/16 v3, 0x16

    .line 7322
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    const/16 v3, 0x17

    .line 7323
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    const/16 v3, 0x1a

    .line 7324
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    const/16 v3, 0x1b

    .line 7325
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    const/16 v3, 0x1c

    .line 7326
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    const/16 v3, 0x9b

    .line 7327
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    const/16 v3, 0x9d

    .line 7328
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    const/16 v3, 0x9e

    .line 7329
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    const/16 v3, 0x9f

    .line 7330
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    const/16 v3, 0xa0

    .line 7331
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    const/16 v3, 0xa5

    .line 7332
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    const/16 v3, 0xa6

    .line 7333
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    const/16 v3, 0xa8

    .line 7334
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    const/16 v3, 0xa9

    .line 7335
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    const/16 v3, 0xaa

    .line 7336
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    const/16 v3, 0xab

    .line 7337
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    const/16 v3, 0xac

    .line 7338
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    const/16 v3, 0xad

    .line 7339
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    const/16 v3, 0xae

    .line 7340
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    const/16 v3, 0xaf

    .line 7341
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    const/16 v3, 0xb0

    .line 7342
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    const/16 v3, 0xb1

    .line 7343
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    const/16 v3, 0xb2

    .line 7344
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    const/16 v3, 0xb5

    .line 7345
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    const/16 v3, 0xb7

    .line 7346
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    const/16 v3, 0xb8

    .line 7347
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    const/16 v3, 0xb9

    .line 7348
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    const/16 v3, 0xba

    .line 7349
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    const/16 v3, 0xbc

    .line 7350
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    const/16 v3, 0xbd

    .line 7351
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    const/16 v3, 0xbe

    .line 7352
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    const/16 v3, 0xbf

    .line 7353
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    const/16 v3, 0xc3

    .line 7354
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7355
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 7300
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;->encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .locals 0

    .line 7476
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object p1

    .line 7477
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7478
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7300
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;->redact(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    return-object p1
.end method
